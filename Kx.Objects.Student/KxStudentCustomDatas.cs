using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Exceptions;
using Kx.Core.Utility;

namespace Kx.Objects.Student
{
    public class KxStudentCustomDatas : KxBaseClass
    {
        private int kxStudentId;
        private List<KxStudentCustomData> items = new List<KxStudentCustomData>();

        public int KxStudentId
        {
            get
            { return kxStudentId; }
            set
            { kxStudentId = value; }
        }

        public List<KxStudentCustomData> Items
        {
            get
            { return items; }
        }

        public KxStudentCustomData this[int index]
        {
            get
            { return items[index]; }
            set
            { items[index] = value; }
        }

        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = null;

            if (actiontype == ActionType.Select)
            {
                result = new KxData("studentcustomdata", "st2studentcustomdata", TargetType.Table);

                result.KeyFields.Add(new KxDataValue("kxStudentId", kxStudentId, KxValueType.Int));
            }
            return result;
        }

        public KxStudentCustomDatas(KxBaseClass parent, int kxStudentId) : base(parent)
        {
            this.kxStudentId = kxStudentId;
            Load();
        }

        public override bool LoadFromXml(string xml, string xpath)
        {
            items.Clear();
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xml);

                XmlNodeList nodes = doc.SelectNodes(@"//results/" + xpath + "/datarow");

                foreach (XmlNode node in nodes)
                {
                    if (node != null)
                    {
                        KxStudentCustomData sc = new KxStudentCustomData(this, node);
                        Items.Add(sc);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "LoadFromXml", ex.Message, xpath);
                LastError = ex.Message;
                return false;
            }
        }

        public KxStudentCustomData GetCustomData(String datafield)
        {
            KxStudentCustomData result = null;
			String sDataField = datafield.ToUpper();

			foreach (KxStudentCustomData scd in Items)
            {
				if (scd.DataField.ToUpper() == sDataField)
                {
                    result = scd;
                    break;
                }
            }
			return result;
        }

        public string GetCustomDataValue(string datafield)
        {
            string result = "";

            KxStudentCustomData cd = GetCustomData(datafield);

            if (cd != null)
            {
                result = cd.DataValue;
            }
            return result;
        }

    }
}
