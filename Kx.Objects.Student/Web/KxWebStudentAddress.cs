using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Configuration;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;
using Kx.Objects.Student;
using Kx.Objects.Common;

namespace Kx.Objects.Student.Web
{
    [Serializable]
    public class KxWebStudentAddress : KxBaseClass
    {
        #region Attributes & Properties

        protected Guid     applicationGuid  = Guid.Empty;
        protected DateTime ?creationDate = DateTime.Now;
        protected int      addressTypeId;
        protected string   address1;
        protected string   address2;
        protected string   address3;
        protected string   address4;
        protected string   postcode;
        protected string   contactName;
        protected string   telephoneNumber;
        protected string   mobileNumber;
        protected string   faxNumber;
        protected string   emailAddress;

        private static string _hasNameFields;

        protected string _title = String.Empty;
        protected string _foreName = String.Empty;
        protected string _surName = String.Empty;

        protected string _relationship;

        protected KxAddressType addressType = null;

        public int      AddressId
        {
            get
            { return base.dBIdentifier;}
        }
        public Guid     ApplicationGuid
        {
            get
            {
                return applicationGuid;
            }
        }
        public DateTime ?CreationDate
        {
            get
            { return creationDate;}
            set
            { 
                AddToAudit("CreationDate", creationDate, value);
                creationDate = value;
            }
        }
        public int      AddressTypeId
        {
            get
            { return addressTypeId;}
            set
            { 
                AddToAudit("AddressTypeId", addressTypeId, value);
                addressTypeId = value;
            }
        }
        public string   Address1
        {
            get
            { return address1;}
            set
            { 
                AddToAudit("Address1", address1, value);
                address1 = value;
            }
        }
        public string   Address2
        {
            get
            { return address2;}
            set
            { 
                AddToAudit("Address2", address2, value);
                address2 = value;
            }
        }
        public string   Address3
        {
            get
            { return address3;}
            set
            { 
                AddToAudit("Address3", address3, value);
                address3 = value;
            }
        }
        public string   Address4
        {
            get
            { return address4;}
            set
            { 
                AddToAudit("Address4", address4, value);
                address4 = value;
            }
        }
        public string   Postcode
        {
            get
            { return postcode;}
            set
            { 
                AddToAudit("Postcode", postcode, value);
                postcode = value;
            }
        }
        public string   ContactName
        {
            get
            { return contactName;}
            set
            { 
                AddToAudit("ContactName", contactName, value);
                contactName = value;
            }
        }
        public string   TelephoneNumber
        {
            get
            { return telephoneNumber;}
            set
            { 
                AddToAudit("TelephoneNumber", telephoneNumber, value);
                telephoneNumber = value;
            }
        }
        public string   MobileNumber
        {
            get
            { return mobileNumber;}
            set
            { 
                AddToAudit("MobileNumber", mobileNumber, value);
                mobileNumber = value;
            }
        }
        public string   FaxNumber
        {
            get
            { return faxNumber;}
            set
            { 
                AddToAudit("FaxNumber", faxNumber, value);
                faxNumber = value;
            }
        }
        public string   EmailAddress
        {
            get
            { return emailAddress;}
            set
            { 
                AddToAudit("EmailAddress", emailAddress, value);
                emailAddress = value;
            }
        }

        public string Title
        {
            get
            {
                return _title;
            }
            set
            {
                AddToAudit("Title", _title, value);
                _title = value;
            }
        }
        public string ForeName
        {
            get
            {
                return _foreName;
            }
            set
            {
                AddToAudit("Forename", _foreName, value);
                _foreName = value;
            }
        }
        public string SurName
        {
            get
            {
                return _surName;
            }
            set
            {
                AddToAudit("Surname", _surName, value);
                _surName = value;
            }
        }

        public string Relationship
        {
            get { return _relationship; }
            set
            {
                AddToAudit("Relationship", _relationship, value);
                _relationship = value;
            }
        }


        public KxAddressType AddressType
        {
            get
            {
                if (addressType == null)
                {
                    if (addressTypeId != 0)
                    {
                        addressType = new KxAddressType(this,addressTypeId);
                    }
                }                
                return addressType; 
            }
        }
        public string FormattedAddressSL
        {
            get
            { return GetFormattedAddressSL(); }
        }
        public bool IsPopulated
        {
            get
            {
                return GetIsPopulated();
            }
        }

        public bool UseProperAddressFormat
        {
            get
            {
                return GetProperAddressFormatSetting();
            }
        }

        /// <remarks>
        /// Internal method made visible to the unit test project via the InternalsVisibleTo attribute in AssemblyInfo.cs
        /// </remarks>
        internal void UNITTESTResetHasColumns()
        {
            _hasNameFields = null;
        }


#endregion
        public KxWebStudentAddress(KxBaseClass parent): base(parent)
        {
        }

        public KxWebStudentAddress(KxBaseClass parent, int addressid) : base(parent)
        {
             base.dBIdentifier = addressid;
             Load();
        }

        public KxWebStudentAddress(KxBaseClass parent, int addressid, XmlNode node) :base(parent)
        {
            base.dBIdentifier = addressid;
            LoadFromXml("",node);
        }


        public override KxData ConfigureData(ActionType actiontype)
        {
            if (parent != null)
            {
                applicationGuid = parent.GuidIdentifier;
            }

            KxData result = new KxData("webstudentaddress", "WebStudentAddress", TargetType.Table);

            result.AutoKeyField = new KxDataValue("AddressId",AddressId,KxValueType.Int);


            if ((actiontype == ActionType.Update) && (AddressId == 0))
            {
                result.Action = ActionType.Insert;
            }
            else
            {
                result.Action = actiontype;
            }

            if ((actiontype == ActionType.Insert)  ||  (actiontype == ActionType.Update))
            {
                result.AddParameter("applicationGuid", applicationGuid, KxValueType.Guid, false);
                result.AddParameter("creationDate", creationDate, KxValueType.DateTime, false);
                result.AddParameter("addressTypeId", addressTypeId, KxValueType.Int, false);
                result.AddParameter("address1", address1, KxValueType.String, 255, true);
                result.AddParameter("address2", address2, KxValueType.String, 255, true);
                result.AddParameter("address3", address3, KxValueType.String, 255, true);
                result.AddParameter("address4", address4, KxValueType.String, 255, true);
                result.AddParameter("postcode", postcode, KxValueType.String, 20, true);
                result.AddParameter("contactName", contactName, KxValueType.String, 255, true);
                result.AddParameter("telephoneNumber", telephoneNumber, KxValueType.String, 50, true);
                result.AddParameter("mobileNumber", mobileNumber, KxValueType.String, 50, true);
                result.AddParameter("faxNumber", faxNumber, KxValueType.String, 50, true);
                result.AddParameter("emailAddress", emailAddress, KxValueType.String, 255, true);

                const string STR_No = "No";
                const string STR_Yes = "Yes";
                try
                {
                    if (string.IsNullOrEmpty(_hasNameFields))
                    {
                        KxData data = new KxData("dd", "SELECT TOP 1 Relationship FROM WebStudentAddress", TargetType.Static);
                        if (data.Execute())
                        {
                            _hasNameFields = STR_Yes;
                        }
                        else
                        {
                            _hasNameFields = STR_No;
                        }
                    }
                }
                catch (Exception ex)
                {
                    KxLogger.LogActivity(this, "Finding name fields column", ex.Message, "");
                    _hasNameFields = STR_No;
                }

                if (_hasNameFields == STR_Yes)
                {
                    result.AddParameter("title", _title, KxValueType.String, 50, true);
                    result.AddParameter("forename", _foreName, KxValueType.String, 255, true);
                    result.AddParameter("surname", _surName, KxValueType.String, 255, true);
                    result.AddParameter("relationship", _relationship, KxValueType.String, 50, true);
                }                

                if (KxDbConfiguration.GetConfigurationBool("PreventWebAddressUpdate",false))
                {
                    result = null;
                    Changed = false;
                }

            }
            return result;
        }

        public override void LoadFromXml(string xml,XmlNode node)
        {
            Children.Remove(addressType);
            addressType = null;

            applicationGuid = KxXmlUtils.GetNodeValue(node, "applicationguid", Guid.Empty);
            creationDate = KxXmlUtils.GetNodeValueDT(node, "creationdate", null);
            addressTypeId = KxXmlUtils.GetNodeValue(node, "addresstypeid", 0);
            address1 = KxXmlUtils.GetNodeValue(node, "address1", string.Empty);
            address2 = KxXmlUtils.GetNodeValue(node, "address2", string.Empty);
            address3 = KxXmlUtils.GetNodeValue(node, "address3", string.Empty);
            address4 = KxXmlUtils.GetNodeValue(node, "address4", string.Empty);
            postcode = KxXmlUtils.GetNodeValue(node, "postcode", string.Empty);
            contactName = KxXmlUtils.GetNodeValue(node, "contactname", string.Empty);
            telephoneNumber = KxXmlUtils.GetNodeValue(node, "telephonenumber", string.Empty);
            mobileNumber = KxXmlUtils.GetNodeValue(node, "mobilenumber", string.Empty);
            faxNumber = KxXmlUtils.GetNodeValue(node, "faxnumber", string.Empty);
            emailAddress = KxXmlUtils.GetNodeValue(node, "emailaddress", string.Empty);

            _title = KxXmlUtils.GetNodeValue(node, "title", string.Empty);
            _foreName = KxXmlUtils.GetNodeValue(node, "forename", string.Empty);
            _surName = KxXmlUtils.GetNodeValue(node, "surname", string.Empty);
            _relationship = KxXmlUtils.GetNodeValue(node, "relationship", string.Empty);

            if (!string.IsNullOrEmpty(contactName) && string.IsNullOrWhiteSpace(string.Concat(_title, _foreName, _surName)))
            {
                String[] names = contactName.Split(new char[]{' '});

                if (names.Length == 3)
                {
                    _title = names[0];
                    _foreName = names[1];
                    _surName = names[2];
                }
                else if (names.Length == 2)
                {
                    _foreName = names[0];
                    _surName = names[1];
                }
            }

        }

        public string GetFormattedAddressSL()
        {
            string result = address1 + ", " + address2 + ", " + address3 + ", " + address4 + ", " + postcode;
            result = result.Replace(", , , , ", ", ");
            result = result.Replace(", , , ", ", ");
            result = result.Replace(", , ", ", ");
            result = result.TrimEnd(' ', ',');
            return result;
        }

        public bool GetIsPopulated()
        {
            return !String.IsNullOrEmpty(address1)  ||  !String.IsNullOrEmpty(address2)   || !String.IsNullOrEmpty(address3)   ||
            !String.IsNullOrEmpty(address4)         ||  !String.IsNullOrEmpty(postcode)   || !String.IsNullOrEmpty(contactName)  || 
            !String.IsNullOrEmpty(telephoneNumber)  || !String.IsNullOrEmpty(mobileNumber)  ||  !String.IsNullOrEmpty(faxNumber)   ||
            !String.IsNullOrEmpty(emailAddress);
        }

        public void CopyStudentAddress(KxStudentAddress source)
        {
            if (source.Address.IndexOf('\r') > -1 && !UseProperAddressFormat)
            {
                ProcessAddress(source);
            }
            else
            {
                if (source.Address.Length > 255)
                {
                    Address1 = source.Address.Substring(1, 250);
                }
                else
                {
                    Address1 = source.Address;
                }

                Address2 = source.Town;
                Address3 = source.County;
                Address4 = source.Country;
               
            }
            Postcode = source.Postcode;
            ForeName = source.ForeName;
            SurName = source.SurName;
            Title = source.Title;
            Relationship = source.Relationship;
            ContactName = source.ForeName+" "+source.SurName;
            ContactName = contactName.Trim();

            TelephoneNumber = source.Telephone;
            MobileNumber = source.Mobile;
            FaxNumber = source.Fax;
            EmailAddress = source.EmailAddress;
        }

        private void ProcessAddress(KxStudentAddress source)
        {
            string[] addressLines = source.Address.Split(new char[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);

            if (addressLines.Length > 0)
            {
                Address1 = addressLines[0];
            }
            if (addressLines.Length > 1)
            {
                Address2 = addressLines[1];
            }
            if (addressLines.Length > 2)
            {
                Address3 = addressLines[2];
            }
            if (addressLines.Length > 3)
            {
                Address4 = addressLines[3];
            }
           
            if (source.Town  != "")
            {
                Address4 = source.Town;
            }
        }

        public void CopyWebStudentAddress(KxWebStudentAddress source)
        {
            try
            {
                address1 = source.Address1;
                address2 = source.Address2;
                address3 = source.Address3;
                address4 = source.Address4;
                postcode = source.Postcode;
                contactName = source.ContactName;
                telephoneNumber = source.TelephoneNumber;
                mobileNumber = source.MobileNumber;
                faxNumber = source.FaxNumber;
                emailAddress = source.EmailAddress;
                _title = source.Title;
                _foreName = source.ForeName;
                _surName = source.SurName;
                Changed = true;
            }
            catch { }
        }

        public bool AddressIsidentical(KxWebStudentAddress source)
        {
            if (source == null)
            {
                return false;
            }
            else
            {
                return address1 == source.Address1 && address2 == source.Address2 &&
                       address3 == source.Address3 && address4 == source.Address4 &&
                       postcode == source.Postcode;
            }
        }

        public bool StudentAddressIsidentical(KxStudentAddress source)
        {
            if (source == null)
            {
                return false;
            }
            else
            {
            return address1 == source.Address && address2 == source.Town &&
                   address3 == source.County && address4 == source.Country &&
                   postcode == source.Postcode;
             }
        }

        public bool PostCodePrefixMatch(string postcodes)
        {
            return Kx.Objects.Student.Helpers.PostcodeHelper.PostCodeOutcodeMatch(Postcode, postcodes);
        }

        private bool GetProperAddressFormatSetting()
        {
            bool result = false;

            try
            {
                result = KxDbConfiguration.GetConfigurationBool("ProperAddressFormat",false);
            }
            catch
            {}
            return result;
        }

        public void ConvertNameToContact()
        {
            contactName = String.Format("{0} {1}", _title, _foreName);
            ContactName = String.Format("{0} {1}", contactName.Trim(), _surName);
        }
    }
}

