using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;
using Kx.Objects.Common;

namespace Kx.Objects.Student.Web
{
    [Serializable]
    public class KxWebAdjustedDueDate : KxBaseClass
    {
        #region Attributes & Properties

        protected KxArea area = null;

        protected int      areaId = 0;
        protected int      paymentScheduleTypeId = 0;
        protected DateTime ?fromDate;
        protected DateTime ?toDate;
        protected DateTime ?dueDate;

        protected bool loaded = false;

        public bool Loaded
        {
            get
            {
                return loaded;
            }
            set
            {
                loaded = value;
            }
        }

        public bool NotLoaded
        {
            get
            {
                return !loaded;
            }
        }

        public int      AreaId
        {
            get
            { return areaId;}
            set
            { 
                AddToAudit("AreaId", areaId, value);
                areaId = value;
            }
        }

        public KxArea Area
        {
            get
            {
                if (area == null)
                {
                    area = KxStudentObjectManager.Areas.GetAreaById(areaId);
                }
                return area;
            }
        }

        public string AreaName
        {
            get
            {
                return Area.Description;
            }
        }

        public int      PaymentScheduleTypeId
        {
            get
            { return paymentScheduleTypeId;}
            set
            { 
                AddToAudit("PaymentScheduleTypeId", paymentScheduleTypeId, value);
                paymentScheduleTypeId = value;
            }
        }

        public DateTime ?FromDate
        {
            get
            { return fromDate;}
            set
            { 
                AddToAudit("FromDate", fromDate, value);
                fromDate = value;
            }
        }

        public string FromDateString
        {
            get
            {
                try
                {
                    return FromDate.Value.ToString("yyyyMMdd");
                }
                catch
                {
                    return "18991230";
                }
            }
        }

        public DateTime ?ToDate
        {
            get
            { return toDate;}
            set
            { 
                AddToAudit("ToDate", toDate, value);
                toDate = value;
            }
        }

            public string ToDateString
        {
            get
            {
                try
                {
                    return ToDate.Value.ToString("yyyyMMdd");
                }
                catch
                {
                    return "18991230";
                }
            }
        }

        public DateTime ?DueDate
        {
            get
            { return dueDate;}
            set
            { 
                AddToAudit("DueDate", dueDate, value);
                dueDate = value;
            }
        }

        public string UniqueId
        {
            get
            {
                return String.Format("{0}A{1}A{2}A{3}", AreaId, PaymentScheduleTypeId, FromDateString, ToDateString);
            }
        }


        #endregion 

        public KxWebAdjustedDueDate(KxBaseClass parent)
            : base(parent)
        {
        }

        public KxWebAdjustedDueDate(KxBaseClass parent, XmlNode node)
            : base(parent)
        {
            LoadFromXml("",node);
        }


        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("webadjustedduedate", "WebAdjustedDueDate", TargetType.Table);

            result.AddKeyField("areaId",areaId,KxValueType.Int);
            result.AddKeyField("paymentScheduleTypeId",paymentScheduleTypeId,KxValueType.Int);
            result.AddKeyField("fromDate",fromDate,KxValueType.DateTime);
            result.AddKeyField("toDate", toDate, KxValueType.DateTime);


            if ((actiontype == ActionType.Update) && ( !loaded))
            {
                result.Action = ActionType.Insert;
            }
            else
            {
                result.Action = actiontype;
            }

            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {
                   loaded = true;
                   result.AddParameter("dueDate", dueDate, KxValueType.DateTime, true);
            }
            return result;
        }


        public override  void LoadFromXml(string xml,XmlNode node)
        {
            areaId  = KxXmlUtils.GetNodeValue(node,"areaid",0);
            paymentScheduleTypeId  = KxXmlUtils.GetNodeValue(node,"paymentscheduletypeid",0);
            fromDate  = KxXmlUtils.GetNodeValueDT(node,"fromdate",null);
            toDate  = KxXmlUtils.GetNodeValueDT(node,"todate",null);
            dueDate  = KxXmlUtils.GetNodeValueDT(node,"duedate",null);

            loaded = true;
        }
    }
}

