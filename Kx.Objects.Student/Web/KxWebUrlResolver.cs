using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;

namespace Kx.Objects.Student.Web
{
    [Serializable]
    public class KxWebUrlResolver : KxBaseClass
    {
        #region Attributes & Properties

        protected int      academicYearId;
        protected int wordTemplateId;
        protected int      areaId;
        protected string   blockIds;
        protected string   distanceSellingUrl;
        protected string   inventoryUrl;
        protected string   handbookUrl;
        protected string   arrivalInstructionsUrl;
        protected string   url1;
        protected string   url2;

        public int      UrlResolverId
        {
            get
            { return base.dBIdentifier;}
        }

        public int      AcademicYearId
        {
            get
            { return academicYearId;}
            set
            { 
                AddToAudit("AcademicYearId", academicYearId, value);
                academicYearId = value;
            }
        }

        public int WordTemplateId
        {
            get
            { return wordTemplateId; }
            set
            {
                AddToAudit("WordTemplateId", wordTemplateId, value);
                wordTemplateId = value;
            }
        }

        public int      AreaId
        {
            get
            { return areaId;}
            set
            { 
                AddToAudit("AreaId", areaId, value);
                areaId = value;
            }
        }

        public string      BlockIds
        {
            get
            { return blockIds; }
            set
            {
                AddToAudit("BlockIds", blockIds, value);
                blockIds = value;
            }
        }

        public string BlockIdArrays
        {
            get
            { return String.Format(",{0},", blockIds); }
           
        }

       
        public string   DistanceSellingUrl
        {
            get
            { return distanceSellingUrl;}
            set
            { 
                AddToAudit("DistanceSellingUrl", distanceSellingUrl, value);
                distanceSellingUrl = value;
            }
        }

        public string   InventoryUrl
        {
            get
            { return inventoryUrl;}
            set
            { 
                AddToAudit("InventoryUrl", inventoryUrl, value);
                inventoryUrl = value;
            }
        }

        public string   HandbookUrl
        {
            get
            { return handbookUrl;}
            set
            { 
                AddToAudit("HandbookUrl", handbookUrl, value);
                handbookUrl = value;
            }
        }

        public string   ArrivalInstructionsUrl
        {
            get
            { return arrivalInstructionsUrl;}
            set
            { 
                AddToAudit("ArrivalInstructionsUrl", arrivalInstructionsUrl, value);
                arrivalInstructionsUrl = value;
            }
        }

        public string Url1
        {
            get
            { return url1; }
            set
            {
                AddToAudit("url1", url1, value);
                url1 = value;
            }
        }

        public string Url2
        {
            get
            { return url2; }
            set
            {
                AddToAudit("url2", url2, value);
                url2 = value;
            }
        }


        #endregion 


        public KxWebUrlResolver(KxBaseClass parent):base(parent)
        {}


        public KxWebUrlResolver(KxBaseClass parent, XmlNode node):base(parent)
        {
             LoadFromXml("", node);
        }

        public KxWebUrlResolver(KxBaseClass parent,int      urlresolverid):base(parent)
        {
             base.dBIdentifier = urlresolverid;
             Load();
        }


        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("weburlresolver", "WebUrlResolver", TargetType.Table);

            result.AutoKeyField = new KxDataValue("UrlResolverId",UrlResolverId,KxValueType.Int);
            
            if ((actiontype == ActionType.Update) && (UrlResolverId == 0))
            {
                result.Action = ActionType.Insert;
            }
            else
            {
                result.Action = actiontype;
            }

            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {
                result.AddParameter("wordtemplateid", wordTemplateId, KxValueType.Int, true);
                result.AddParameter("areaId", areaId, KxValueType.Int, true);
                result.AddParameter("Blockids", BlockIds, KxValueType.String,500, true);
                result.AddParameter("distanceSellingUrl", distanceSellingUrl, KxValueType.String, 300, true);
                result.AddParameter("inventoryUrl", inventoryUrl, KxValueType.String, 300, true);
                result.AddParameter("handbookUrl", handbookUrl, KxValueType.String, 300, true);
                result.AddParameter("arrivalInstructionsUrl", arrivalInstructionsUrl, KxValueType.String, 300, true);
                result.AddParameter("url1", url1, KxValueType.String, 300, true);
                result.AddParameter("url2", url2, KxValueType.String, 300, true);
            }
            return result;
        }


        public override  void LoadFromXml(string xml,XmlNode node)
        {
            dBIdentifier  = KxXmlUtils.GetNodeValue(node,"urlresolverid",0);
            academicYearId  = KxXmlUtils.GetNodeValue(node,"academicyearid",0);
            wordTemplateId = KxXmlUtils.GetNodeValue(node, "wordtemplateid", 0);
            areaId  = KxXmlUtils.GetNodeValue(node,"areaid",0);
            blockIds = KxXmlUtils.GetNodeValue(node, "blockids", "");
            distanceSellingUrl  = KxXmlUtils.GetNodeValue(node,"distancesellingurl","");
            inventoryUrl  = KxXmlUtils.GetNodeValue(node,"inventoryurl","");
            handbookUrl  = KxXmlUtils.GetNodeValue(node,"handbookurl","");
            arrivalInstructionsUrl  = KxXmlUtils.GetNodeValue(node,"arrivalinstructionsurl","");
            url1 = KxXmlUtils.GetNodeValue(node, "url1", "");
            url2 = KxXmlUtils.GetNodeValue(node, "url2", "");
        }
    }
}

