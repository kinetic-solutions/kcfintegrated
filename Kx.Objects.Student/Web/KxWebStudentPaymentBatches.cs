using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Exceptions;
using Kx.Core.Utility;

namespace Kx.Objects.Student.Web
{
    public class KxWebStudentPaymentBatches : KxBaseClass
    {
        
        private List<KxWebStudentPaymentBatch> items = new List<KxWebStudentPaymentBatch>();

        public List<KxWebStudentPaymentBatch> Items
        {
            get
            { return items; }
        }

        public KxWebStudentPaymentBatch this[int index]
        {
            get
            { return items[index]; }
            set
            { items[index] = value; }
        }

        public override KxData ConfigureData(ActionType actiontype)
        {
        
            KxData result = new KxData("webstudentpaymentbatch", "WebStudentPaymentBatch", TargetType.Table, ActionType.Select);

            result.AddKeyField("webstudentpaymentid",-1,KxValueType.Int);

            return result;
        }

        public override bool LoadFromXml(string xml, string xpath)
        {
            items.Clear();
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xml);

                XmlNodeList nodes = doc.SelectNodes(@"//results/" + xpath + "/datarow");

                foreach (XmlNode node in nodes)
                {
                    if (node != null)
                    {
                        KxWebStudentPaymentBatch item = new KxWebStudentPaymentBatch(this, node);
                        items.Add(item);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "LoadFromXml", ex.Message, xpath);
                LastError = ex.Message;
                return false;
            }
        }
    }
}

