using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Exceptions;
using Kx.Core.Utility;
using Kx.Objects.Student.DataProviders;

namespace Kx.Objects.Student.Web
{
    public class KxWebStudentApplications : KxBaseClass
    {
        private int kxStudentId;
        // This is a safety hack to be removed if sites remain stable longer term
        private static string _enableOldAppSaveBehaviour;

        private List<KxWebStudentApplication> items = new List<KxWebStudentApplication>();

        public Guid UserGuid
        {
            get
            {
                return guidIdentifier;
            }
        }

        /// <summary>
        /// Bool which determines if the old applications saving behaviour is still used in the GetWebApplicationsForYearAndSession method
        /// This is a safety hack to be removed if sites remain stable longer term
        /// </summary>
        protected bool enableOldAppSaveBehaviour
        {
            get
            {
                if (_enableOldAppSaveBehaviour == null)
                {
                    _enableOldAppSaveBehaviour = ConfigurationManager.AppSettings["enableOldAppSaveBehaviour"];
                }
                return _enableOldAppSaveBehaviour == "True";
            }
        }

        public int KxStudentId
        {
            get
            { return kxStudentId; }
            set
            {
                kxStudentId = value;

                foreach (KxWebStudentApplication app in items)
                {
                    app.KxStudentId = kxStudentId;
                }
            }
        }

        public List<KxWebStudentApplication> Items
        {
            get
            { return items; }
        }

        public KxWebStudentApplication this[int index]
        {
            get
            { return items[index]; }
            set
            { items[index] = value; }
        }

        public KxWebStudentApplication AddApplication()
        {
            KxWebStudentApplication result = new KxWebStudentApplication(this);
            Items.Add(result);
            return result;
        }

        public override KxData ConfigureData(ActionType actiontype)
        {
            if (parent != null)
            {
                guidIdentifier = parent.GuidIdentifier;
            }

            KxData result = null;

            if (actiontype == ActionType.Select)
            {
                result = new KxData("webstudentapplication", "webstudentapplication", TargetType.Table, ActionType.Select);

                result.KeyFields.Add(new KxDataValue("userGuid", UserGuid, KxValueType.Guid));

                result.AddOrderField("applicationid", KxDataOrderType.Descending); 
            }
            return result;
        }

        public KxWebStudentApplications(KxBaseClass parent, Guid userGuid) : this(parent)
        {
            guidIdentifier = userGuid;
            Load();
        }

        public KxWebStudentApplications(KxBaseClass parent) : base(parent)
        {
        }

        public override bool LoadFromXml(string xml, string xpath)
        {
            items.Clear();
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xml);

                XmlNodeList nodes = doc.SelectNodes(@"//results/" + xpath + "/datarow");

                foreach (XmlNode node in nodes)
                {
                    if (node != null)
                    {
                        KxWebStudentApplication application = new KxWebStudentApplication(this, node);
                        if (application.Changed)
                            System.Diagnostics.Debugger.Break();
                        Items.Add(application);
                        /*
                        Guid applicationId = KxXmlUtils.GetNodeValue(node, "applicationguid", Guid.Empty);
                        if (applicationId != Guid.Empty)
                        {
                            KxWebStudentApplication application = new KxWebStudentApplication(this, applicationId);
                            Items.Add(application);
                        }
                         * */
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "LoadFromXml", ex.Message, xpath);
                LastError = ex.Message;
                return false;
            }
        }

        public KxWebStudentApplication GetWebApplicationForYearAndSession(int AcademicYear, int sessionId)
        {
            return GetWebApplicationsForYearAndSession(AcademicYear, sessionId, false).FirstOrDefault();
        }

        public IEnumerable<KxWebStudentApplication> GetWebApplicationsForYearAndSession(int AcademicYear, int sessionId, bool expired)
        {
            KxAcademicYearSession session = StudentCoreDataProvider.GetSession(AcademicYear, sessionId);

            List<KxWebStudentApplication> result = new List<KxWebStudentApplication>();

            KxWebStudentApplication application = null;

            foreach (KxWebStudentApplication app in Items)
            {
                if (app.AcademicYearId == AcademicYear && app.SessionID == sessionId && (!app.Expired || expired))
                {
                    application = app;
                    result.Add(app);

                    if (app.ApplicationId != 0 && !session.AllowMultipleApplications)
                    {
                        break;
                    }
                }
            }

            if ((application == null && !expired && sessionId > -1) || (session.AllowMultipleApplications && result.FirstOrDefault(s => s.SessionID == sessionId && sessionId > 0 && s.ApplicationId == 0) == null))
            {
                // pha - why is this firing at all????
                application = AddApplication();
                application.AcademicYearId = AcademicYear;
                application.SessionID = sessionId;
                if (enableOldAppSaveBehaviour) // This is a safety hack to be removed if sites remain stable longer term
                {
                    application.Save();
                }
                else
                {
                    // Don't save the application to database unless it is linked to a WebUser which is in the WebUsers table (registered). 
                    if (application.WebUser.IsInWebUsers)
                    {
                        application.Save();
                    }
                }
                result.Add(application);
            }
            return result;
        }

        public KxWebStudentApplication GetApplicationByid(int applicationId)
        {
            KxWebStudentApplication result = null;

            foreach (KxWebStudentApplication app in Items)
            {
                if (app.ApplicationId == applicationId)
                {
                    result = app;
                    break;
                }
            }

            return result;
        }

        public override void NotifyDelete(KxBaseClass child)
        {
            KxWebStudentApplication member = (KxWebStudentApplication)child;
            if (Items.Contains(member))
            {
                Items.Remove(member);
            }
        }
    }
}
