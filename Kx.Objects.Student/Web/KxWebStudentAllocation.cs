#region copyright

// =======================================================
// Product:   KCF.Student
// Project:   Kx.Objects.Student
// File:      KxWebStudentAllocation.cs
// Created:   08/07/2016
// By:        Steve Ashley
// Modified:  21/12/2016
// 
// Copyright � Kinetic Solutions Ltd 2016
// All rights reserved.
// 
// No portion of this file may be re-created, copied or otherwise
// used without the express permission of Kinetic Solutions Ltd.
// =======================================================

#endregion

#region usings

using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Configuration;
using Kx.Core.Data;
using Kx.Core.Exceptions;
using Kx.Core.Merge;
using Kx.Core.Utility;
using Kx.Core.Web.Configuration;
using Kx.Objects.Student.DataProviders;
using Winnovative;

#endregion

namespace Kx.Objects.Student.Web
{
    [Serializable]
    public class KxWebStudentAllocation : KxBaseClass, IMergeObject
    {
        #region Fields and Properties

        protected KxLocationContent _locationContent = null;

        private KxMergeTagProviderFactory _mergeTagProviderFactory;
        protected string acceptanceDocument;
        protected string acceptancePage;
        protected DateTime? acceptedDate = null;
        protected string acceptedUserData;
        protected int allocationId;
        protected decimal depositAmount;
        protected bool depositPaid;
        protected string hostIPAddress;
        protected bool processed;
        protected KxStudentReason reason = null;
        protected DateTime? rejectedDate = null;
        protected int rejectedReason;
        protected string rejectedUserData;
        protected KxWebStudentAllocationSchedule schedule = null;

        protected KxStudentAllocation studentAllocation = null;
        //protected Guid webAllocationGuid;
        protected Guid userGuid;

        public Guid WebAllocationGuid
        {
            get { return guidIdentifier; }
        }

        public Guid UserGuid
        {
            get { return userGuid; }
            set
            {
                AddToAudit("UserGuid", userGuid, value);
                userGuid = value;
            }
        }

        public int AllocationId
        {
            get { return allocationId; }
            set
            {
                AddToAudit("AllocationId", allocationId, value);
                allocationId = value;
            }
        }

        public DateTime? AcceptedDate
        {
            get { return acceptedDate; }
            set
            {
                AddToAudit("AcceptedDate", acceptedDate, value);
                acceptedDate = value;
            }
        }

        public string AcceptedUserData
        {
            get { return acceptedUserData; }
            set
            {
                AddToAudit("AcceptedUserData", acceptedUserData, value);
                acceptedUserData = value;
            }
        }

        public DateTime? RejectedDate
        {
            get { return rejectedDate; }
            set
            {
                AddToAudit("RejectedDate", rejectedDate, value);
                rejectedDate = value;
            }
        }

        public int RejectedReason
        {
            get { return rejectedReason; }
            set
            {
                reason = null;
                AddToAudit("RejectedReason", rejectedReason, value);
                rejectedReason = value;
            }
        }

        public string RejectedUserData
        {
            get { return rejectedUserData; }
            set
            {
                AddToAudit("RejectedUserData", rejectedUserData, value);
                rejectedUserData = value;
            }
        }

        public bool HasAcceptancePage
        {
            get { return !String.IsNullOrEmpty(AcceptancePage); }
        }

        public string AcceptancePage
        {
            get { return acceptancePage; }
            set
            {
                if (value != acceptancePage)
                {
                    Changed = true;
                }
                acceptancePage = value;
            }
        }

        public string AcceptanceDocument
        {
            get { return acceptanceDocument; }
            set
            {
                AddToAudit("AcceptanceDocument", acceptanceDocument, value);
                acceptanceDocument = value;
            }
        }

        public string HostIPAddress
        {
            get { return hostIPAddress; }
            set
            {
                AddToAudit("HostIPAddress", hostIPAddress, value);
                hostIPAddress = value;
            }
        }

        public bool DepositPaid
        {
            get { 
                return depositPaid; 
            }
            set
            {
                AddToAudit("DepositPaid", depositPaid, value);
                depositPaid = value;
            }
        }

        public decimal DepositAmount
        {
            get 
            { 
                return depositAmount; 
            }
            set
            {
                AddToAudit("DepositAmount", depositAmount, value);
                depositAmount = value;
            }
        }

        public bool Processed
        {
            get { return processed; }
            set
            {
                AddToAudit("Processed", processed, value);
                processed = value;
            }
        }

        public KxStudentAllocation StudentAllocation
        {
            get
            {
                if (studentAllocation == null)
                {
                    studentAllocation = new KxStudentAllocation(this, allocationId);
                }
                return studentAllocation;
            }
        }

        public KxWebStudentAllocationSchedule Schedule
        {
            get
            {
                if (schedule == null)
                {
                    schedule = new KxWebStudentAllocationSchedule(this);
                }
                return schedule;
            }
        }

        public KxStudentReason Reason
        {
            get
            {
                if (rejectedReason > 0 && reason == null)
                {
                    reason = new KxStudentReason(this, rejectedReason);
                }
                return reason;
            }
        }

        private KxMergeTagProviderFactory MergeTagProviderFactory
        {
            get
            {
                if (_mergeTagProviderFactory == null)
                {
                    _mergeTagProviderFactory = new KxMergeTagProviderFactory();
                }

                return _mergeTagProviderFactory;
            }
        }

        public KxMergeTagProvider MergeTags
        {
            get
            {
                if (StudentAllocation != null)
                {
                    return StudentAllocation.MergeTags;
                }

                return MergeTagProviderFactory.GetMergeTagProvider(KxMergeTagSetName.GetST2StudentAllocations, 0, AllocationId);
            }
        }

        public KxLocationContent LocationContent
        {
            get
            {
                if (_locationContent == null)
                {
                    _locationContent =
                        KxLocationContent.GetLocationContent(StudentAllocation.ResidentialRoom.Block.SiteId,
                            StudentAllocation.ResidentialRoom.Block.AreaId,
                            StudentAllocation.ResidentialRoom.BlockId,
                            StudentAllocation.ResidentialRoom.BedroomTypeId).FirstOrDefault();
                }

                return _locationContent;
            }
        }


        public string ReasonText
        {
            get
            {
                if (Reason != null)
                {
                    return Reason.Description;
                }
                else
                {
                    return "N/A";
                }
            }
        }

        public bool SpansChristmasAndEaster
        {
            get { return SpansChristmas && SpansEaster; }
        }

        public bool SpansChristmasNotEaster
        {
            get { return SpansChristmas && !SpansEaster; }
        }

        public bool SpansEasterNotChristmas
        {
            get { return SpansEaster && !SpansChristmas; }
        }

        public bool SpansChristmasOrEaster
        {
            get { return SpansChristmas || SpansEaster; }
        }

        public bool SpansChristmas
        {
            get
            {
                if (Schedule != null)
                {
                    return GetAllocationSpan(new DateTime(Schedule.ActualArrivalYear, 12, 25));
                }
                else
                {
                    return true;
                }
            }
        }

        public bool SpansEaster
        {
            get
            {
                if (Schedule != null)
                {
                    return GetAllocationSpan(KxDateTime.EasterSunday(Schedule.ActualArrivalYear)) || GetAllocationSpan(KxDateTime.EasterSunday(Schedule.ActualDepartureYear));
                }
                else
                {
                    return true;
                }
            }
        }

        public bool AllocatedAtEaster
        {
            get
            {
                if (StudentAllocation != null && StudentAllocation.AllocationDates != null)
                {
                    return StudentAllocation.AllocationDates.AllocatedOnDate(KxDateTime.EasterSunday(Schedule.ActualArrivalYear)) ||
                           StudentAllocation.AllocationDates.AllocatedOnDate(KxDateTime.EasterSunday(Schedule.ActualDepartureYear));
                }
                else
                {
                    return false;
                }
            }
        }

        public bool NotAllocatedAtEaster
        {
            get { return !AllocatedAtEaster; }
        }

        public string AcceptancePdf
        {
            get { return GetAcceptancePdf(); }
        }

        public int DaysToStart
        {
            get { return GetDaysToStart(); }
        }

        public KxPropertyDetail PropertyDetail
        {
            get { return KxPropertyDetail.GetPropertyDetail(StudentAllocation.ResidentialRoom.Block.SiteId, StudentAllocation.ResidentialRoom.Block.AreaId, StudentAllocation.ResidentialRoom.Block.BlockId); }
        }

        #endregion

        #region Constructors

        public KxWebStudentAllocation(KxBaseClass parent) : base(parent)
        {
        }

        public KxWebStudentAllocation(KxBaseClass parent, Guid allocationGuid)
            : this(parent)
        {
            guidIdentifier = allocationGuid;
            Load();
        }

        public KxWebStudentAllocation(KxBaseClass parent, Guid userguid, int allocationId)
            : this(parent)
        {
            userGuid = userguid;
            AllocationId = allocationId;
        }

        public KxWebStudentAllocation(KxBaseClass parent, XmlNode node)
            : this(parent)
        {
            LoadFromXml("", node);
        }

        #endregion

        #region Public Methods

        public void SnapshotMergeTags(bool reCreateExisting)
        {
            if (StudentAllocation != null)
            {
                StudentAllocation.SnapshotMergeTags(reCreateExisting);
            }
            else
            {
                MergeTagProviderFactory.SnapshotMergeTags(KxMergeTagSetName.GetST2StudentAllocations, 0,
                    AllocationId, reCreateExisting);
            }
        }

        public override KxData ConfigureData(ActionType actiontype)
        {
            if (KxConfigurationManager.Profiling)
            {
                KxLogger.LogActivity(this, "KxWebUserStudent.ConfigureData", "Started", "");
            }
            KxData result = new KxData("webstudentallocation", "WebStudentAllocation", TargetType.Table);
            try
            {
                if ((actiontype == ActionType.Update) && (guidIdentifier == Guid.Empty))
                {
                    if (changed)
                    {
                        guidIdentifier = Guid.NewGuid();
                    }
                    result.Action = ActionType.Insert;
                }
                else
                {
                    result.Action = actiontype;
                }

                result.KeyFields.Add(new KxDataValue("webAllocationGuid", guidIdentifier, KxValueType.Guid));

                if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
                {
                    result.AddParameter("userGuid", userGuid, KxValueType.Guid, true);
                    result.AddParameter("allocationId", allocationId, KxValueType.Int, true);
                    result.AddParameter("acceptedDate", acceptedDate, KxValueType.DateTime, true);
                    result.AddParameter("acceptedUserData", acceptedUserData, KxValueType.String, 2000, true);
                    result.AddParameter("rejectedDate", rejectedDate, KxValueType.DateTime, true);
                    result.AddParameter("rejectedReason", rejectedReason, KxValueType.Int, true);
                    result.AddParameter("rejectedUserData", rejectedUserData, KxValueType.String, 2000, true);

                    if (!String.IsNullOrEmpty(acceptancePage))
                    {
                        result.AddParameter("acceptancePage", Encoding.UTF8.GetBytes(acceptancePage), KxValueType.Image);
                    }

                    result.AddParameter("acceptanceDocument", acceptanceDocument, KxValueType.String, 2147483647, true);
                    result.AddParameter("hostIPAddress", hostIPAddress, KxValueType.String, 50, true);
                    result.AddParameter("depositPaid", depositPaid, KxValueType.Boolean, true);
                    result.AddParameter("processed", processed, KxValueType.Boolean, true);
                    result.AddParameter("depositAmount", depositAmount, KxValueType.Decimal, true);
                }
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "KxWebUserStudent.ConfigureData", ex.Message, "");
            }
            if (KxConfigurationManager.Profiling)
            {
                KxLogger.LogActivity(this, "KxWebUserStudent.ConfigureData", "Ended", "");
            }
            return result;
        }

        public override void LoadFromXml(string xml, XmlNode node)
        {
            guidIdentifier = KxXmlUtils.GetNodeValue(node, "weballocationguid", Guid.Empty);
            userGuid = KxXmlUtils.GetNodeValue(node, "userguid", Guid.Empty);
            allocationId = KxXmlUtils.GetNodeValue(node, "allocationid", 0);
            acceptedDate = KxXmlUtils.GetNodeValueDT(node, "accepteddate", null);
            acceptedUserData = KxXmlUtils.GetNodeValue(node, "accepteduserdata", "");
            rejectedDate = KxXmlUtils.GetNodeValueDT(node, "rejecteddate", null);
            rejectedReason = KxXmlUtils.GetNodeValue(node, "rejectedreason", 0);
            rejectedUserData = KxXmlUtils.GetNodeValue(node, "rejecteduserdata", "");

            acceptancePage = KxBase64.Base64Decode(KxXmlUtils.GetNodeValue(node, "acceptancepage", ""));

            acceptanceDocument = KxXmlUtils.GetNodeValue(node, "acceptancedocument", "");
            hostIPAddress = KxXmlUtils.GetNodeValue(node, "hostipaddress", "");
            depositPaid = KxXmlUtils.GetNodeValue(node, "depositpaid", false);
            processed = KxXmlUtils.GetNodeValue(node, "processed", false);
            depositAmount = KxXmlUtils.GetNodeValue(node, "depositamount", (decimal) 0.00M);
        }

        public void ResetMergeTags()
        {
            _mergeTagProviderFactory = null;
        }

        public bool AcceptAllocation(string UserDetails, string UserIPAddress)
        {
            bool result = true;

            if (KxConfigurationManager.Profiling)
            {
                KxLogger.LogActivity(this, "KxWebStudentAllocation.AcceptAllocation", UserDetails, "");
            }
            if (!StudentAllocation.OfferHasExpired)
            {
                if (KxConfigurationManager.Profiling)
                {
                    KxLogger.LogActivity(this, "KxWebUserStudent.AcceptAllocation", "Not Expired", "");
                }

                try
                {
                    AcceptedUserData = UserDetails;
                    HostIPAddress = UserIPAddress;
                    bool convertAcceptancePage = KxConfigurationManager.DontConvertAcceptancePage;
                    if (!convertAcceptancePage)
                    {
                        AcceptanceDocument = KxStringUtils.PageToDocument(AcceptancePage);
                    }
                    else
                    {
                        AcceptanceDocument = AcceptancePage;
                    }
                    AcceptedDate = DateTime.Now;
                }
                catch (Exception ex)
                {
                    KxLogger.LogActivity(this, "KxWebUserStudent.AcceptAllocation PageToDocument Error", ex.Message, "");
                }
                if (KxConfigurationManager.Profiling)
                {
                    KxLogger.LogActivity(this, "KxWebUserStudent.AcceptAllocation", "Before Save", "");
                }
                try
                {
                    result = Save();
                }
                catch (Exception ex)
                {
                    KxLogger.LogActivity(this, "KxWebUserStudent.AcceptAllocation Save Error", ex.Message, "");
                    result = false;
                }
                if (KxConfigurationManager.Profiling)
                {
                    if (result)
                    {
                        KxLogger.LogActivity(this, "KxWebUserStudent.AcceptAllocation", "Save - True", "");
                    }
                    else
                    {
                        KxLogger.LogActivity(this, "KxWebUserStudent.AcceptAllocation", "Save - False", "");
                    }
                }
            }
            return result;
        }

        public bool RejectAllocation(string UserDetails, string UserIPAddress)
        {
            bool result = true;

            if (!StudentAllocation.OfferHasExpired)
            {
                RejectedUserData = UserDetails;
                HostIPAddress = UserIPAddress;
                RejectedDate = DateTime.Now;
                result = Save();
            }
            return result;
        }

        public bool GetAllocationSpan(DateTime date)
        {
            bool result = false;

            if (Schedule != null)
            {
                result = Schedule.ActualArrivalDate < date && Schedule.ActualDepartureDate > date;
            }
            return result;
        }

        public string GetAcceptancePdf()
        {
            string result = "";

            try
            {
                PdfConverter pdfConverter = new PdfConverter();

                pdfConverter.PdfDocumentOptions.PdfPageSize = PdfPageSize.A4;
                pdfConverter.PdfDocumentOptions.PdfCompressionLevel = PdfCompressionLevel.NoCompression;
                pdfConverter.PdfDocumentOptions.ShowHeader = false;
                pdfConverter.PdfDocumentOptions.ShowFooter = false;
                pdfConverter.PdfDocumentOptions.LeftMargin = 0;
                pdfConverter.PdfDocumentOptions.RightMargin = 0;
                pdfConverter.PdfDocumentOptions.TopMargin = 0;
                pdfConverter.PdfDocumentOptions.BottomMargin = 0;
                //pdfConverter.PdfDocumentOptions.Width = 1024;                
                pdfConverter.PdfDocumentOptions.PdfPageSize = PdfPageSize.A4;
                pdfConverter.PdfDocumentOptions.AvoidTextBreak = true;
                //pdfConverter.HtmlViewerWidth = 1024;
                pdfConverter.PdfDocumentOptions.FitWidth = true;
                //pdfConverter.LicenseKey = "8tnA0sPSysTSxNzC0sHD3MPA3MvLy8s=";
                pdfConverter.LicenseKey = "Nbuouqu6oqmvuqurtKq6qau0q6i0o6Ojow==";

                string doc = AcceptanceDocument.Replace("\r", "<br>");

                string tempfilepath = KxDbConfiguration.GetConfiguration("TempFileLocation");
                string filename = Path.Combine(tempfilepath, "Offer_" + AllocationId.ToString() + ".pdf");

                if (!File.Exists(filename))
                {
                    pdfConverter.SavePdfFromHtmlStringToFile(doc, filename);
                }
                result = filename;
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "GetAcceptancePdf", ex.Message, "");
                result = "";
            }
            return result;
        }

        public override bool Save()
        {
            if (KxConfigurationManager.Profiling)
            {
                KxLogger.LogActivity(this, "KxWebStudentAllocation.Save", "Start", "");
            }

            bool result = base.Save();

            if (KxConfigurationManager.Profiling)
            {
                KxLogger.LogActivity(this, "KxWebStudentAllocation.Save", "End", "");
            }

            return result;
        }

        #endregion

        #region Private Methods

        private int GetDaysToStart()
        {
            int result = 0;

            if (StudentAllocation.AllocationStartDate.CompareTo(DateTime.Today) > 0)
            {
                TimeSpan ts = StudentAllocation.AllocationStartDate.Subtract(DateTime.Today);
                result = Convert.ToInt32(ts.TotalDays);
            }

            return result;
        }

        #endregion
    }
}