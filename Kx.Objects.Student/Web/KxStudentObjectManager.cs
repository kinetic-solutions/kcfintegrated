﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using Kx.Objects.Common;
using Kx.Objects.Common.Web;

namespace Kx.Objects.Student.Web
{
    public class KxStudentObjectManager
    {
        public static KxStudentObjectProvider ObjectProvider
        {
            get
            {
                if (HttpContext.Current.Application["StudentObject"] == null)
                {
                    HttpContext.Current.Application["StudentObject"] = new KxStudentObjectProvider();
                }
                return ((KxStudentObjectProvider)HttpContext.Current.Application["StudentObject"]);
            }
        }

        public static KxAreas Areas
        {
            get
            {
                return ObjectProvider.Areas;
            }
        }


        public static KxSites Sites
        {
            get
            {
                return ObjectProvider.Sites;
            }
        }

        public static KxSiteBlocks SiteBlocks
        {
            get
            {
                return ObjectProvider.SiteBlocks;
            }
        }

        public static KxBedroomTypes BedroomTypes
        {
            get
            {
                return ObjectProvider.BedroomTypes;
            }
        }

        public static KxAllocationModelCache AllocationModels
        {
            get
            {
                return ObjectProvider.AllocationModels;
            }
        }
        public static KxAllocationModelDateCache AllocationModelDates
        {
            get
            {
                return ObjectProvider.AllocationModelDates;
            }
        }
        public static KxStudentChargeTypes StudentChargeTypes
        {
            get
            {
                return ObjectProvider.StudentChargeTypes;
            }
        }

        public static KxUdfMappings UdfMappings
        {
            get
            {
                return ObjectProvider.UdfMappings;
            }
        }

        public static KxeInductionQuestions InductionQuestions
        {
            get
            {
                return ObjectProvider.InductionQuestions;
            }
        }

        public static KxeInductionArrivalLocations InductionArrivalLocations
        {
            get
            {
                return ObjectProvider.InductionArrivalLocations;
            }
        }

        public static KxeInductionArrivalSlotCache ArrivalSlotCache
        {
            get
            {
                return ObjectProvider.ArrivalSlotCache;
            }
        }

        public static KxWebAllocationPricingList WebAllocationPricingList
        {
            get
            {
                return ObjectProvider.WebAllocationPricingList;
            }
        }

        public static KxWebOpalAllocationPricingList WebOpalAllocationPricingList
        {
            get
            {
                return ObjectProvider.WebOpalAllocationPricingList;
            }
        }

        public static KxBookingEligibilityRuleList BookingEligibilityRuleList
        {
            get
            {
                return ObjectProvider.BookingEligibilityRuleList;
            }
        }

        public static KxSiteAvailabilityLookups SiteAvailabilityLookups
        {
            get
            {
                return ObjectProvider.SiteAvailabilityLookups;
            }
        }

        public static KxWebAdjustedDueDates AdjustedDueDates
        {
            get
            {
                return ObjectProvider.AdjustedDueDates;
            }
        }

        public static KxPricingModels PricingModels
        {
            get
            {
                return ObjectProvider.PricingModels;
            }
        }

        
    }
}
