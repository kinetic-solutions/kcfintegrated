#region copyright

// =======================================================
// Product:   KCF.Student
// Project:   Kx.Objects.Student
// File:      KxWebStudentSocialProfile.cs
// Created:   08/07/2016
// By:        Steve Ashley
// Modified:  21/12/2016
// 
// Copyright � Kinetic Solutions Ltd 2016
// All rights reserved.
// 
// No portion of this file may be re-created, copied or otherwise
// used without the express permission of Kinetic Solutions Ltd.
// =======================================================

#endregion

#region usings

using System;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Exceptions;
using Kx.Core.Utility;

#endregion

namespace Kx.Objects.Student.Web
{
    [Serializable]
    public class KxWebStudentSocialProfile : KxBaseClass
    {
        #region Fields and Properties

        protected string _briefDescription;
        protected DateTime? _creationDate;
        protected string _field1Value;
        protected string _field2Value;
        protected string _field3Value;
        protected string _field4Value;
        protected string _field5Value;

        protected int _kxStudentId;
        private bool _newRow = false;
        protected string _profile1;
        protected string _profile2;
        protected string _profile3;
        protected string _profile4;
        protected string _profile5;
        protected string _profile6;
        protected string _profile7;
        protected string _profile8;
        protected string _profile9;
        protected bool _profileIsPublic;
        protected string _screenName;

        public int KxStudentId
        {
            get { return _kxStudentId; }
            set
            {
                AddToAudit("KxStudentId", _kxStudentId, value);
                _kxStudentId = value;
            }
        }

        public bool ProfileIsPublic
        {
            get { return _profileIsPublic; }
            set
            {
                AddToAudit("ProfileIsPublic", _profileIsPublic, value);
                _profileIsPublic = value;
            }
        }

        public DateTime? CreationDate
        {
            get { return _creationDate; }
            set
            {
                AddToAudit("CreationDate", _creationDate, value);
                _creationDate = value;
            }
        }

        public string ScreenName
        {
            get { return _screenName; }
            set
            {
                AddToAudit("ScreenName", _screenName, value);
                _screenName = value;
            }
        }

        public string BriefDescription
        {
            get { return _briefDescription; }
            set
            {
                AddToAudit("BriefDescription", _briefDescription, value);
                _briefDescription = value;
            }
        }

        public string Field1Value
        {
            get { return _field1Value; }
            set
            {
                AddToAudit("Field1Value", _field1Value, value);
                _field1Value = value;
            }
        }

        public string Field2Value
        {
            get { return _field2Value; }
            set
            {
                AddToAudit("Field2Value", _field2Value, value);
                _field2Value = value;
            }
        }

        public string Field3Value
        {
            get { return _field3Value; }
            set
            {
                AddToAudit("Field3Value", _field3Value, value);
                _field3Value = value;
            }
        }

        public string Field4Value
        {
            get { return _field4Value; }
            set
            {
                AddToAudit("Field4Value", _field4Value, value);
                _field4Value = value;
            }
        }

        public string Field5Value
        {
            get { return _field5Value; }
            set
            {
                AddToAudit("Field5Value", _field5Value, value);
                _field5Value = value;
            }
        }

        public string Profile1
        {
            get { return _profile1; }
            set
            {
                AddToAudit("Profile1", _profile1, value);
                _profile1 = value;
            }
        }

        public string Profile2
        {
            get { return _profile2; }
            set
            {
                AddToAudit("Profile2", _profile2, value);
                _profile2 = value;
            }
        }

        public string Profile3
        {
            get { return _profile3; }
            set
            {
                AddToAudit("Profile3", _profile3, value);
                _profile3 = value;
            }
        }

        public string Profile4
        {
            get { return _profile4; }
            set
            {
                AddToAudit("Profile4", _profile4, value);
                _profile4 = value;
            }
        }

        public string Profile5
        {
            get { return _profile5; }
            set
            {
                AddToAudit("Profile5", _profile5, value);
                _profile5 = value;
            }
        }

        public string Profile6
        {
            get { return _profile6; }
            set
            {
                AddToAudit("Profile6", _profile6, value);
                _profile6 = value;
            }
        }

        public string Profile7
        {
            get { return _profile7; }
            set
            {
                AddToAudit("Profile7", _profile7, value);
                _profile7 = value;
            }
        }

        public string Profile8
        {
            get { return _profile8; }
            set
            {
                AddToAudit("Profile8", _profile8, value);
                _profile8 = value;
            }
        }

        public string Profile9
        {
            get { return _profile9; }
            set
            {
                AddToAudit("Profile9", _profile9, value);
                _profile9 = value;
            }
        }

        #endregion

        #region Constructors

        public KxWebStudentSocialProfile(KxBaseClass parent, XmlNode node)
            : base(parent)
        {
            LoadFromXml("", node);
        }

        public KxWebStudentSocialProfile(KxBaseClass parent, int kxStudentID)
            : base(parent)
        {
            _kxStudentId = kxStudentID;
            _newRow = true;
        }

        public KxWebStudentSocialProfile()
        {
        }

        #endregion

        #region Public Methods

        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("webstudentsocialprofile", "WebStudentSocialProfile", TargetType.Table);

            if (_newRow && actiontype == ActionType.Update && !_creationDate.HasValue)
            {
                result.Action = ActionType.Insert;
                result.AddParameter("kxStudentId", _kxStudentId, KxValueType.Int, false);
                CreationDate = DateTime.Now;
            }
            else
            {
                result.Action = actiontype;
                result.AddKeyField("kxstudentid", _kxStudentId, KxValueType.Int);
            }

            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {
                result.AddParameter("profileIsPublic", _profileIsPublic, KxValueType.Boolean, false);
                result.AddParameter("creationDate", _creationDate, KxValueType.DateTime, false);
                result.AddParameter("screenName", _screenName, KxValueType.String, 50, false);
                result.AddParameter("briefDescription", _briefDescription, KxValueType.String, 5000, true);
                result.AddParameter("field1Value", _field1Value, KxValueType.String, 50, true);
                result.AddParameter("field2Value", _field2Value, KxValueType.String, 50, true);
                result.AddParameter("field3Value", _field3Value, KxValueType.String, 50, true);
                result.AddParameter("field4Value", _field4Value, KxValueType.String, 50, true);
                result.AddParameter("field5Value", _field5Value, KxValueType.String, 50, true);
                result.AddParameter("profile1", _profile1, KxValueType.String, 255, true);
                result.AddParameter("profile2", _profile2, KxValueType.String, 255, true);
                result.AddParameter("profile3", _profile3, KxValueType.String, 255, true);
                result.AddParameter("profile4", _profile4, KxValueType.String, 255, true);
                result.AddParameter("profile5", _profile5, KxValueType.String, 255, true);
                result.AddParameter("profile6", _profile6, KxValueType.String, 255, true);
                result.AddParameter("profile7", _profile7, KxValueType.String, 255, true);
                result.AddParameter("profile8", _profile8, KxValueType.String, 255, true);
                result.AddParameter("profile9", _profile9, KxValueType.String, 255, true);
            }
            return result;
        }


        public override void LoadFromXml(string xml, XmlNode node)
        {
            if (KxXmlUtils.GetNodeValue(node, "kxstudentid", 0) == 0)
            {
                _newRow = true;
            }
            else
            {
                _kxStudentId = KxXmlUtils.GetNodeValue(node, "kxstudentid", 0);
            }
            _profileIsPublic = KxXmlUtils.GetNodeValue(node, "profileispublic", false);
            _creationDate = KxXmlUtils.GetNodeValueDT(node, "creationdate", null);
            _screenName = KxXmlUtils.GetNodeValue(node, "screenname", string.Empty);
            _briefDescription = KxXmlUtils.GetNodeValue(node, "briefdescription", string.Empty);
            _field1Value = KxXmlUtils.GetNodeValue(node, "field1value", string.Empty);
            _field2Value = KxXmlUtils.GetNodeValue(node, "field2value", string.Empty);
            _field3Value = KxXmlUtils.GetNodeValue(node, "field3value", string.Empty);
            _field4Value = KxXmlUtils.GetNodeValue(node, "field4value", string.Empty);
            _field5Value = KxXmlUtils.GetNodeValue(node, "field5value", string.Empty);
            _profile1 = KxXmlUtils.GetNodeValue(node, "profile1", string.Empty);
            _profile2 = KxXmlUtils.GetNodeValue(node, "profile2", string.Empty);
            _profile3 = KxXmlUtils.GetNodeValue(node, "profile3", string.Empty);
            _profile4 = KxXmlUtils.GetNodeValue(node, "profile4", string.Empty);
            _profile5 = KxXmlUtils.GetNodeValue(node, "profile5", string.Empty);
            _profile6 = KxXmlUtils.GetNodeValue(node, "profile6", string.Empty);
            _profile7 = KxXmlUtils.GetNodeValue(node, "profile7", string.Empty);
            _profile8 = KxXmlUtils.GetNodeValue(node, "profile8", string.Empty);
            _profile9 = KxXmlUtils.GetNodeValue(node, "profile9", string.Empty);
        }

        public static KxWebStudentSocialProfile GetWebStudentSocialProfile(int kxStudentId, KxBaseClass parent)
        {
            KxData data = new KxData("webstudentsocialprofile", "WebStudentSocialProfile", TargetType.Table);
            data.AddKeyField("kxstudentid", kxStudentId, KxValueType.Int);
            if (data.Execute())
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(data.XmlResults);
                XmlNodeList nodes = doc.SelectNodes(@"//webstudentsocialprofile/datarow");
                foreach (XmlNode node in nodes)
                {
                    return new KxWebStudentSocialProfile(parent, node);
                }
            }
            else
            {
                KxLogger.LogActivity("KxWebStudentSocialProfile", "GetWebStudentSocialProfile", data.LastError, "KXStudentID: " + kxStudentId.ToString());
            }

            return new KxWebStudentSocialProfile(parent, kxStudentId);
        }

        #endregion
    }
}