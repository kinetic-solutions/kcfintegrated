﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Exceptions;
using Kx.Core.Utility;

namespace Kx.Objects.Student.Web
{
    public class KxBookingEligibilityRuleList : KxBaseClass
    {
        #region Properties & Attributes
        private int academicYearId = 0;
        private int facultyId = 0;
        private List<KxBookingEligibilityRule> items = new List<KxBookingEligibilityRule>();

        public int AcademicYearId
        {
            get
            {
                return academicYearId;
            }
            set
            {
                if (value != null && value != academicYearId)
                {
                    academicYearId = value;
                    Load();
                }
            }
        }
        public int FacultyId
        {
            get
            {
                return facultyId;
            }
            set
            {
                if (value != null && value != facultyId)
                {
                    facultyId = value;
                    Load();
                }
            }
        }

        public string[] BookingEligibilityRuleIds
        {
            get
            {
                return GetBookingEligibilityRuleIds();
            }
        }

        public List<KxBookingEligibilityRule> Items
        {
            get
            { return items; }
        }
        public KxBookingEligibilityRule this[int index]
        {
            get { return items[index]; }
            set { items[index] = value; }
        }

        #endregion

        #region Contructors
        public KxBookingEligibilityRuleList(KxBaseClass parent, int academicYearId)
            : base(parent)
        {
             this.academicYearId = academicYearId;
             this.facultyId = 0;
        }

        public KxBookingEligibilityRuleList(KxBaseClass parent)
            : base(parent)
        {
        }


        #endregion

        #region Public Methods

        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = null;

            if (actiontype == ActionType.Select)
            {
                result = new KxData("bookingeligibilityrules", "BookingEligibilityRules", TargetType.Table, ActionType.Select);

                if (academicYearId > 0)
                {
                    result.KeyFields.Add(new KxDataValue("academicyearid", academicYearId, KxValueType.Int));
                }

                if (facultyId > 0)
                {
                    result.KeyFields.Add(new KxDataValue("facultyid", facultyId, KxValueType.Int));
                }
            }

            return result;
        }

        public override bool LoadFromXml(string xml, string xpath)
        {
            items.Clear();
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xml);

                XmlNodeList nodes = doc.SelectNodes(@"//results/" + xpath + "/datarow");

                foreach (XmlNode node in nodes)
                {
                    if (node != null)
                    {
                        KxBookingEligibilityRule item = new KxBookingEligibilityRule(this, node);
                        items.Add(item);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "LoadFromXml", ex.Message, xpath);
                LastError = ex.Message;
                return false;
            }
        }

        public string[] GetBookingEligibilityRuleIds()
        {
            List<string> list = new List<string>();

            foreach (KxBookingEligibilityRule i in Items)
            {
                list.Add(i.UniqueId);
            }
            return list.ToArray();
        }


        public KxBookingEligibilityRule GetBookingEligibilityRule(string id)
        {
            KxBookingEligibilityRule result = null;

            foreach (KxBookingEligibilityRule i in Items)
            {
                if (i.UniqueId == id)
                {
                    result = i;
                    break;
                }
            }
            return result;
        }

        public KxBookingEligibilityRule AddBookingEligibilityRule()
        {
            KxBookingEligibilityRule result = new KxBookingEligibilityRule(this);

            items.Add(result);

            return result;
        }

        public void RemoveBookingEligibilityRule(KxBookingEligibilityRule rule)
        {
            foreach (KxBookingEligibilityRule i in Items)
            {
                if (i.UniqueId == rule.UniqueId)
                {
                    Items.Remove(i);
                    break;
                }
            }

        }

        public void SaveBookingEligibilityRules(string savedByUsername)
        {
            foreach (KxBookingEligibilityRule i in Items)
            {
                if (i.AcademicYearId != 0 && i.FacultyId != 0 && i.StudentYearId != 0)
                {
                    if (i.Changed)
                    {
                        i.LastUpdatedDate = DateTime.Now;
                        i.LastUpdatedByUser = savedByUsername;
                    }
                    

                    KxData result = i.ConfigureData(ActionType.Update);

                    result.Execute();
                }
            }

        }

        public bool RuleIsUnique(KxBookingEligibilityRule rule)
        {
            int count = 0;
            foreach (KxBookingEligibilityRule i in Items)
            {
                if (i.UniqueId == rule.UniqueId)
                {
                    count++;
                }
            }

            if (count > 1)
            {
                return false;
            }

            return true;

        }

        #endregion
    }
}
