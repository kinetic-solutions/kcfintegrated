﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Utility;

namespace Kx.Objects.Student.Web
{
    public class KxWebStudentSocialProfileFlatmate : KxWebStudentSocialProfile
    {
        private int _loadingKxStudentId;
        public string RoomName { get; private set; }
        public string StudentId { get; private set; }

        public bool IsLoadedStudent
        {
            get { return this.KxStudentId == _loadingKxStudentId; }
        }

        private KxStudentPhoto _photo = null;
        public KxStudentPhoto Photo
        {
            get
            {
                if (_photo == null && !string.IsNullOrWhiteSpace(StudentId))
                {
                    _photo = new KxStudentPhoto(StudentId);
                }
                else if (_photo == null)
                {
                    _photo = new KxStudentPhoto("UNKNOWN");
                }

                return _photo;
            }
        }

        public bool IsPhotoUploaded
        {
            get { return Photo.isPhotoUploaded; }
        }

        public string PhotoImageBase64
        {
            get { return KxBase64.Base64Encode(Photo.imageBytes).Replace("B64:", ""); }
        }

        public string PhotoFileName
        {
            get { return Photo.PhotoFileName; }
        }



        public KxWebStudentSocialProfileFlatmate(KxBaseClass parent, XmlNode node, int loadingKxStudentId) 
            : base(parent, node)
        {
            _loadingKxStudentId = loadingKxStudentId;
            LoadFromXml("", node);
        }

        public KxWebStudentSocialProfileFlatmate()
        {
        }

        public override void LoadFromXml(string xml, XmlNode node)
        {
            base.LoadFromXml(xml, node);

            this.RoomName = KxXmlUtils.GetNodeValue(node, "roomname", string.Empty);
            this.StudentId = KxXmlUtils.GetNodeValue(node, "studentid", string.Empty);

            if (this.KxStudentId != _loadingKxStudentId && !ProfileIsPublic)
            {
                _screenName = string.Empty;
                _briefDescription = string.Empty;
                _field1Value = string.Empty;
                _field2Value = string.Empty;
                _field3Value = string.Empty;
                _field4Value = string.Empty;
                _field5Value = string.Empty;
                _profile1 = string.Empty;
                _profile2 = string.Empty;
                _profile3 = string.Empty;
                _profile4 = string.Empty;
                _profile5 = string.Empty;
                _profile6 = string.Empty;
                _profile7 = string.Empty;
                _profile8 = string.Empty;
                _profile9 = string.Empty;
                this.StudentId = string.Empty;
            }
        }
    }
}
