using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;

namespace Kx.Objects.Student
{
    [Serializable]
    public class KxWebStudentApplicationInduction : KxBaseClass
    {
        #region Attributes & Properties

        protected Guid _applicationGuid;
        protected int _allocationId;
        protected DateTime? _inductionStarted;
        protected DateTime? _inductionCompleted;
        protected string _inductionAudit;

        public int InductionId
        {
            get { return base.dBIdentifier; }
        }

        public Guid ApplicationGuId
        {
            get { return _applicationGuid; }
            set
            { 
                AddToAudit("ApplicationGuId", _applicationGuid, value);
                _applicationGuid = value;
            }
        }

        public int AllocationId
        {
            get { return _allocationId; }
            set
            { 
                AddToAudit("AllocationId", _allocationId, value);
                _allocationId = value;
            }
        }

        public DateTime? InductionStarted
        {
            get { return _inductionStarted; }
            set
            { 
                AddToAudit("InductionStarted", _inductionStarted, value);
                _inductionStarted = value;
            }
        }

        public DateTime? InductionCompleted
        {
            get { return _inductionCompleted; }
            set
            { 
                AddToAudit("InductionCompleted", _inductionCompleted, value);
                _inductionCompleted = value;
            }
        }

        public string InductionAudit
        {
            get { return _inductionAudit; }
            set
            { 
                AddToAudit("InductionAudit", _inductionAudit, value);
                _inductionAudit = value;
            }
        }


        #endregion 


        public KxWebStudentApplicationInduction(KxBaseClass parent):base(parent)
        { }


        public KxWebStudentApplicationInduction(KxBaseClass parent, XmlNode node):base(parent)
        {
             LoadFromXml("", node);
        }

        public KxWebStudentApplicationInduction(KxBaseClass parent,int  inductionid):base(parent)
        {
             base.dBIdentifier = inductionid;
             Load();
        }


        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("webstudentapplicationinduction", "dbo.WebStudentApplicationInduction", TargetType.Table);


            if ((actiontype == ActionType.Update) && (dBIdentifier == 0))
            {
                result.Action = ActionType.Insert;
            }
            else
            {
                result.Action = actiontype;
            }

            result.AutoKeyField = new KxDataValue("inductionid",dBIdentifier,KxValueType.Int);


            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {
                result.AddParameter("applicationGuid", _applicationGuid, KxValueType.Guid, false);
                result.AddParameter("allocationId", _allocationId, KxValueType.Int, true);
                result.AddParameter("inductionStarted", _inductionStarted, KxValueType.DateTime, false);
                result.AddParameter("inductionCompleted", _inductionCompleted, KxValueType.DateTime, true);
                result.AddParameter("inductionAudit", _inductionAudit, KxValueType.String, 2147483647, true);
            }
            return result;
        }

        public void AddAudit(string auditText)
        {
            this.InductionAudit = string.Format("{0}{1}{2:G} : {3}", InductionAudit, Environment.NewLine, DateTime.Now,
                auditText);
            this.Save();
        }


        public override  void LoadFromXml(string xml,XmlNode node)
        {
            dBIdentifier  = KxXmlUtils.GetNodeValue(node,"inductionid",0);
            _applicationGuid  = KxXmlUtils.GetNodeValue(node,"applicationguid",Guid.Empty);
            _allocationId  = KxXmlUtils.GetNodeValue(node,"allocationid",0);
            _inductionStarted  = KxXmlUtils.GetNodeValueDT(node,"inductionstarted",null);
            _inductionCompleted  = KxXmlUtils.GetNodeValueDT(node,"inductioncompleted",null);
            _inductionAudit  = KxXmlUtils.GetNodeValue(node,"inductionaudit",string.Empty);
        }

        public static KxWebStudentApplicationInduction GetWebStudentApplicationInduction (Guid applicationGuid)
        {
            KxData data = new KxData("webstudentapplicationinduction", "dbo.WebStudentApplicationInduction", TargetType.Table);
            data.AddKeyField("applicationguid", applicationGuid, KxValueType.Guid);
            if (data.Execute ())
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(data.XmlResults);
                XmlNodeList nodes = doc.SelectNodes(@"//webstudentapplicationinduction/datarow");
                foreach(XmlNode node in nodes)
                {
                     return new KxWebStudentApplicationInduction(null, node);
                }
            }

            return new KxWebStudentApplicationInduction(null) {ApplicationGuId = applicationGuid};
        }
    }
}

