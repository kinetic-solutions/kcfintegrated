﻿using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using System;
using System.Collections.Generic;
using System.Web;
using System.Xml;

namespace Kx.Objects.Student.Web
{
    public enum EnDiscountType
    {
        Fixed = 'F',
        Percentage = 'P'
    }

    public enum EnAppliesTo
    {
        First = 'F',
        Last = 'L'
    }

    public class KxDiscountCode : KxBaseClass
    {
        string _discountType;
        string _appliesTo;
        public KxDiscountCode(KxBaseClass parent) : base(parent)
        { }


        public KxDiscountCode(KxBaseClass parent, XmlNode node) : base(parent)
        {
            LoadFromXml("", node);
        }

        public KxDiscountCode(KxBaseClass parent, int propertydetailid) : base(parent)
        {
            Load();
        }


        public int DiscountCodeID { get; set; }
        public string DiscountCode { get; set; }
        public string Description { get; set; }
        public bool Inactive { get; set; }
        public EnDiscountType DiscountType { get => string.IsNullOrWhiteSpace(_discountType) ? EnDiscountType.Fixed : (EnDiscountType)_discountType[0]; set => _discountType = ((char)value).ToString(); }
        public string DiscountTypeStr { get => _discountType; }
        public string AppliesToStr { get => _appliesTo; }
        public EnAppliesTo AppliesTo { get => string.IsNullOrWhiteSpace(_appliesTo) ? EnAppliesTo.First : (EnAppliesTo)_appliesTo[0]; set => _appliesTo = ((char)value).ToString(); }
        public decimal Value { get; set; }
        public int SiteID { get; private set; }
        public int AreaID { get; private set; }
        public DateTime StartDate { get; private set; }
        public DateTime EndDate { get; private set; }
        public int MaxAvailability { get; private set; }
        public int UsedCount { get; private set; }

        public override void LoadFromXml(string xml, XmlNode node)
        {
            DiscountCodeID = KxXmlUtils.GetNodeValue(node, "discountcodeid", 0);
            DiscountCode = KxXmlUtils.GetNodeValue(node, "DiscountCode", string.Empty);
            Description = KxXmlUtils.GetNodeValue(node, "description", string.Empty);
            Inactive = KxXmlUtils.GetNodeValue(node, "inactive", false);
            _discountType = KxXmlUtils.GetNodeValue(node, "discounttype", string.Empty);
            Value = KxXmlUtils.GetNodeValue(node, "value", (decimal)0.00M);
            _appliesTo = KxXmlUtils.GetNodeValue(node, "appliesto", string.Empty);
            SiteID = KxXmlUtils.GetNodeValue(node, "siteid", 0);
            AreaID = KxXmlUtils.GetNodeValue(node, "areaid", 0);
            StartDate = KxXmlUtils.GetNodeValueDT(node, "startdate", KxDateTime.KxNullDate) ?? KxDateTime.KxNullDate;
            EndDate = KxXmlUtils.GetNodeValueDT(node, "enddate", KxDateTime.KxNullDate) ?? KxDateTime.KxNullDate;
            MaxAvailability = KxXmlUtils.GetNodeValue(node, "maxavailability", 0);
            UsedCount = KxXmlUtils.GetNodeValue(node, "usedcount", 0);
            DiscountCode = KxXmlUtils.GetNodeValue(node, "discountcode", string.Empty);
        }

        public static List<KxDiscountCode> GetAllDiscountCodes(int kxStudentId)
        {
            
            List<KxDiscountCode> list = new List<KxDiscountCode>();
            KxData data = new KxData("discountcodes", "kcf.DiscountCodes", TargetType.Table);
            if (data.Execute())
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(data.XmlResults);
                XmlNodeList nodes = doc.SelectNodes(@"//discountcodes/datarow");
                foreach (XmlNode node in nodes)
                {
                    list.Add(new KxDiscountCode(null, node));
                }
            }

            return list;
        }

        public static KxDiscountCode GetDiscountCode(string discountcode)
        {
            string sql = @"SELECT dc.DiscountCodeID, dc.DiscountCode, dc.Description, dc.DiscountType, dc.Value,
                               dc.Inactive, SiteID, AreaID, StartDate, EndDate, MaxAvailability,
                                    (SELECT COUNT(sdc.DiscountCodeID) from ST2StudentDiscountCodes sdc where sdc.DiscountCodeID = dc.DiscountCodeID) as UsedCount
                               FROM dbo.ST2DiscountCodes dc 
                               JOIN dbo.ST2DiscountCodeLocations dcl ON dc.DiscountCodeID = dcl.DiscountCodeID 
                               JOIN dbo.ST2DiscountCodeDates dcd ON dcd.DiscountCodeID = dc.DiscountCodeID
                               WHERE COALESCE(DiscountCode, @DiscountCode) = @DiscountCode";

            KxData data = new KxData("st2discountcodes", sql, TargetType.Static);
            data.AddParameter("discountcode", discountcode, KxValueType.String);
            if (data.Execute())
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(data.XmlResults);
                XmlNodeList nodes = doc.SelectNodes(@"//st2discountcodes/datarow");
                foreach (XmlNode node in nodes)
                {
                    return new KxDiscountCode(null, node);
                }
            }

            return null;
        }

        public static KxDiscountCode GetDiscountCodeById(int discountcodeid)
        {
            string sql = @"SELECT dc.DiscountCodeID, dc.DiscountCode, dc.Description, dc.DiscountType, dc.Value,
                               dc.Inactive, SiteID, AreaID, StartDate, EndDate, MaxAvailability,
                                    (SELECT COUNT(sdc.DiscountCodeID) from ST2StudentDiscountCodes sdc where sdc.DiscountCodeID = dc.DiscountCodeID) as UsedCount
                               FROM dbo.ST2DiscountCodes dc 
                               JOIN dbo.ST2DiscountCodeLocations dcl ON dc.DiscountCodeID = dcl.DiscountCodeID 
                               JOIN dbo.ST2DiscountCodeDates dcd ON dcd.DiscountCodeID = dc.DiscountCodeID
                               WHERE COALESCE(dc.DiscountCodeID, @DiscountCodeID) = @DiscountCodeID";

            KxData data = new KxData("st2discountcodes", sql, TargetType.Static);
            data.AddParameter("discountcodeid", discountcodeid, KxValueType.String);
            if (data.Execute())
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(data.XmlResults);
                XmlNodeList nodes = doc.SelectNodes(@"//st2discountcodes/datarow");
                foreach (XmlNode node in nodes)
                {
                    return new KxDiscountCode(null, node);
                }
            }

            return null;
        }

        public static bool IsDiscountCodeValid(string discountcode)
        {
            var discountCode = GetDiscountCode(discountcode);
            return IsDiscountCodeValid(discountCode);
        }

        /// <summary>
        /// Performs the overall validations for the discount code
        /// - validates if code is valid
        /// - validates if in range of Start date and end date
        /// - validates if already used
        /// </summary>
        /// <param name="discountCode"></param>
        /// <returns></returns>
        public static bool IsDiscountCodeValid(KxDiscountCode discountCode)
        {
            var todayDate = DateTime.Now;
            if (discountCode == null || discountCode.Inactive)
                return false;

            var isValid = false;

            if (discountCode.StartDate == KxDateTime.KxNullDate)
            {
                if (discountCode.EndDate == KxDateTime.KxNullDate)
                    isValid = true;
                else if (todayDate <= discountCode.EndDate)
                    isValid = true;
            }
            else if (discountCode.EndDate == KxDateTime.KxNullDate)
            {
                if (todayDate >= discountCode.StartDate)
                    isValid = true;
            }
            else if (todayDate >= discountCode.StartDate && todayDate <= discountCode.EndDate)
            {
                isValid = true;
            }


            if (isValid && discountCode.UsedCount >= discountCode.MaxAvailability)
            {
                isValid = false;
            }

            return isValid;
        }

        /// <summary>
        /// gets the list of the applied discount codes on an allocation
        /// </summary>
        /// <param name="userGuid"></param>
        /// <param name="applicationGuid"></param>
        /// <returns></returns>
        public static List<KxDiscountCode> GetAppliedDiscountCodes(Guid userGuid, Guid applicationGuid)
        {
            List<KxDiscountCode> lstDiscountCode = new List<KxDiscountCode>();
            KxData data = new KxData("st2studentdiscountcodes ", "kcf.WebStudentDiscountCodes ", TargetType.Table);
            if (userGuid != Guid.Empty)
                data.AddKeyField("UserGuid", userGuid, KxValueType.Guid);

            if (applicationGuid != Guid.Empty)
                data.AddKeyField("ApplicationGuid", applicationGuid, KxValueType.Guid);

            if (data.Execute())
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(data.XmlResults);
                XmlNodeList nodes = doc.SelectNodes(@"//st2studentdiscountcodes/datarow");
                foreach (XmlNode node in nodes)
                {
                    var discountCodeID = KxXmlUtils.GetNodeValue(node, "discountcodeid", 0);
                    var discountCodeObj = GetDiscountCodeById(discountCodeID);
                    lstDiscountCode.Add(discountCodeObj);
                }
            }

            return lstDiscountCode;
        }

        public static bool AddAppliedDiscountCode(Guid userGuid, Guid allocationGuid, Guid applicationGuid, KxDiscountCode discountCode)
        {
            KxData kxData = new KxData("studentdiscountcodes", "kcf.WebStudentDiscountCodes", TargetType.Table, ActionType.Insert);
            kxData.AddParameter("DiscountCodeID", discountCode.DiscountCodeID, KxValueType.String);
            kxData.AddParameter("userGuid", userGuid, KxValueType.Guid);
            kxData.AddParameter("applicationGuid", applicationGuid, KxValueType.Guid, true);
            kxData.AddParameter("allocationGuid", allocationGuid, KxValueType.Guid, true);
            kxData.AddParameter("DiscountAmount", discountCode.Value, KxValueType.Decimal);
            kxData.AddParameter("DiscountType", discountCode.DiscountTypeStr, KxValueType.String);
            kxData.AddParameter("DiscountValue", discountCode.Value, KxValueType.Decimal);

            kxData.AddParameter("CreationDateTime", DateTime.Now, KxValueType.DateTime);
            if (kxData.Execute())
            {
                return true;
            }

            return false;
        }
        //public static bool AddAppliedDiscountCode(int kxStudentId, int allocationid, int applicationid, KxDiscountCode discountCode)
        //{
        //    KxData kxData = new KxData("studentdiscountcodes", "kcf.WebStudentDiscountCodes", TargetType.Table, ActionType.Insert);
        //    kxData.AddParameter("DiscountCodeID", discountCode.DiscountCodeID, KxValueType.String);
        //    kxData.AddParameter("KxStudentID", kxStudentId, KxValueType.Int);
        //    kxData.AddParameter("ApplicationID", applicationid, KxValueType.Int, true);
        //    kxData.AddParameter("AllocationID", allocationid, KxValueType.Int, true);
        //    kxData.AddParameter("DiscountAmount", discountCode.Value, KxValueType.Decimal);
        //    kxData.AddParameter("DiscountType", discountCode.DiscountTypeStr, KxValueType.String);
        //    kxData.AddParameter("DiscountValue", discountCode.Value, KxValueType.Decimal);

        //    kxData.AddParameter("CreationDateTime", DateTime.Now, KxValueType.DateTime);
        //    if (kxData.Execute())
        //    {
        //        return true;
        //    }

        //    return false;
        //}

        /// <summary>
        /// Validates the preference and discount code and returns the result true or false based on the records found.
        /// If result has got records greater than 0 then preference would be considered valid else not.
        /// </summary>
        /// <param name="preferenceAvailabilityId"></param>
        /// <param name="discountCodeID"></param>
        /// <returns></returns>
        public static bool IsPreferenceValid(int preferenceAvailabilityId, int discountCodeID)
        {
            KxData kxData = new KxData("preferenceavailabilitydiscountcodes", "PreferenceAvailabilityDiscountCodes", TargetType.Table, ActionType.Select);
            kxData.AddParameter("preferenceAvailabilityId", preferenceAvailabilityId, KxValueType.String);
            kxData.AddParameter("discountCodeID", discountCodeID, KxValueType.Int);

            if (kxData.Execute())
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(kxData.XmlResults);
                XmlNodeList nodes = doc.SelectNodes(@"//preferenceavailabilitydiscountcodes/datarow");
                if (nodes.Count > 0)
                {
                    return true;
                }

                return false;
            }

            return false;
        }
    }
}
