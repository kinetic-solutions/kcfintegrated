using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Exceptions;
using Kx.Core.Utility;


namespace Kx.Objects.Student.Web
{
    public class KxWebStudentPayments : KxBaseClass
    {
        private Guid applicationGuid;
        private List<KxWebStudentPayment> items = new List<KxWebStudentPayment>();

        public Guid ApplicationGuid
        {
            get
            { return applicationGuid; }
            set
            { applicationGuid = value; }
        }

        public List<KxWebStudentPayment> Items
        {
            get
            { return items; }
        }

        public KxWebStudentPayment this[int index]
        {
            get
            { return items[index]; }
            set
            { items[index] = value; }
        }

        public DateTime? PaymentDate
        {
            get
            {
                return GetPaymentDate();
            }
        }

        public DateTime? DepositDate
        {
            get
            {
                return GetDepositDate();
            }
        }

        public Decimal TotalPayments
        {
            get
            {
                return GetTotalPayments();
            }
        }

        public Decimal TotalDeposits
        {
            get
            {
                return GetTotalDeposits();
            }
        }

        public Decimal TotalAncillaries
        {
            get
            {
                return GetTotalAncillaries();
            }
        }

        public bool HasWebPayment
        {
            get
            {
                return Items.Count > 0;
            }
        }

        public bool HasWebInstalment
        {
            get
            {
                return GetHasWebInstalment();
            }
        }

        public Decimal LatestTransactionAmount
        {
            get
            {
                return GetLatestTransactionAmount();
            }
        }

        public string LatestTransactionReference
        {
            get
            {
                return GetLatestTransactionReference();
            }
        }

        public string LatestTransactionGateway
        {
            get
            {
                return GetLatestTransactionGateway();
            }
        }

        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = null;

            if (actiontype == ActionType.Select)
            {
                result = new KxData("webstudentpayments", "webstudentpayments", TargetType.StoredProc, ActionType.Select);

                if (applicationGuid == Guid.Empty)
                {
                    result.KeyFields.Add(new KxDataValue("ApplicationGuid", Guid.NewGuid(), KxValueType.Guid));
                }
                else
                {
                    result.KeyFields.Add(new KxDataValue("ApplicationGuid", applicationGuid, KxValueType.Guid));
                }
            }
            return result;
        }

        public KxWebStudentPayments(KxBaseClass parent, Guid applicationGuid) : this(parent)
        {
            this.applicationGuid = applicationGuid;
            Load();
        }

        public KxWebStudentPayments(KxBaseClass parent) : base(parent)
        {
        }

        public override bool LoadFromXml(string xml, string xpath)
        {
            items.Clear();
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xml);

                XmlNodeList nodes = doc.SelectNodes(@"//results/" + xpath + "/datarow");

                foreach (XmlNode node in nodes)
                {
                    if (node != null)
                    {
                        int webpaymentid = KxXmlUtils.GetNodeValue(node, "webpaymentid", 0);
                        if (webpaymentid != 0)
                        {
                            KxWebStudentPayment paym = new KxWebStudentPayment(this, webpaymentid);
                            Items.Add(paym);
                        }
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "LoadFromXml", ex.Message, xpath);
                LastError = ex.Message;
                return false;
            }
        }

        public override void NotifyDelete(KxBaseClass child)
        {
            KxWebStudentPayment member = (KxWebStudentPayment)child;
            if (Items.Contains(member))
            {
                Items.Remove(member);
            }
        }

        private bool GetHasWebInstalment()
        {
            bool Result = false;

            foreach (KxWebStudentPayment payment in Items)
            {
                if (payment.PaymentType == KxPaymentType.Instalment)
                {
                    Result = true;
                    break;
                }
            }

            return Result;
        }

        public Decimal GetTotalPayments()
        {
            Decimal Result = 0.00M;

            foreach (KxWebStudentPayment payment in Items)
            {
                if (payment.PaymentType == KxPaymentType.Payment)
                {
                    Result += payment.TransactionAmount;
                }
            }

            return Result;
        }

        public DateTime? GetPaymentDate()
        {
            DateTime? Result = null;

            foreach (KxWebStudentPayment payment in Items)
            {
                if (payment.PaymentType == KxPaymentType.Payment)
                {
                    Result = payment.PaymentDate;
                }
            }

            return Result;
        }

        public Decimal GetTotalDeposits()
        {
            Decimal Result = 0.00M;

            foreach (KxWebStudentPayment payment in Items)
            {
                if (payment.PaymentType == KxPaymentType.Deposit)
                {
                    Result += payment.TransactionAmount;
                }
            }

            return Result;
        }

        public DateTime? GetDepositDate()
        {
            DateTime? Result = null;

            foreach (KxWebStudentPayment payment in Items)
            {
                if (payment.PaymentType == KxPaymentType.Deposit)
                {
                    Result = payment.PaymentDate;
                }
            }

            return Result;
        }

        public Decimal GetTotalAncillaries()
        {
            Decimal Result = 0.00M;

            foreach (KxWebStudentPayment payment in Items)
            {
                if (payment.PaymentType == KxPaymentType.Ancillary)
                {
                    Result += payment.TransactionAmount;
                }
            }

            return Result;
        }


        public bool ReferenceExists(string reference)
        {
            bool result = false;

            foreach (KxWebStudentPayment pay in Items)
            {
                if (reference != "" && pay.TransactionReference == reference)
                {
                    result = true;
                    break;
                }
            }

            return result;
        }

        private string GetLatestTransactionReference()
        {
            string result = "";

            DateTime date = DateTime.Now.AddYears(-1);

            foreach (KxWebStudentPayment pay in Items)
            {
                if (pay.PaymentDate != null && pay.PaymentDate.Value > date)
                {
                    result = pay.TransactionReference;
                    date = pay.PaymentDate.Value;
                }
            }

            return result;
        }

        private string GetLatestTransactionGateway()
        {
            string result = "";

            DateTime date = DateTime.Now.AddYears(-1);

            foreach (KxWebStudentPayment pay in Items)
            {
                if (pay.PaymentDate != null && pay.PaymentDate.Value > date)
                {
                    result = pay.GatewayName;
                    date = pay.PaymentDate.Value;
                }
            }

            return result;
        }

        private Decimal GetLatestTransactionAmount()
        {
            Decimal result = 0.00M;

            DateTime date = DateTime.Now.AddYears(-1);

            foreach (KxWebStudentPayment pay in Items)
            {
                if (pay.PaymentDate != null && pay.PaymentDate.Value > date)
                {
                    result = pay.TransactionAmount;
                    date = pay.PaymentDate.Value;
                }
            }

            return result;
        }


        public bool GetHasNoInstalments()
        {
            bool result = true;

            foreach (KxWebStudentPayment pay in Items)
            {
                if (pay.PaymentType == KxPaymentType.Instalment)
                {
                    result = false;
                    break;
                }
            }

            return result;
        }

        public KxWebStudentPayment GetPaymentById(int id)
        {
            KxWebStudentPayment result = null;

            foreach (KxWebStudentPayment pay in Items)
            {
                if (pay.WebPaymentId == id)
                {
                    result = pay;
                    break;
                }
            }

            return result;
        }

        public KxWebStudentPayment GetPaymentByReference(string reference)
        {
            KxWebStudentPayment result = null;

            foreach (KxWebStudentPayment pay in Items)
            {
                if (pay.TransactionReference == reference)
                {
                    result = pay;
                    break;
                }
            }

            return result;
        }

        public void ReloadPayments()
        {
            Load();
        }

    }
}
