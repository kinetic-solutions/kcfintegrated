using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Exceptions;
using Kx.Core.Utility;


namespace Kx.Objects.Student
{
    public class KxSiteAvailabilityLookups : KxBaseClass
    {
        private List<KxSiteAvailabilityLookup> items = new List<KxSiteAvailabilityLookup>();

        protected string[] siteLookups = null;
        protected string[] allSiteLookups = null;

        public List<KxSiteAvailabilityLookup> Items
        {
            get
            { return items; }
        }

        public KxSiteAvailabilityLookup this[int index]
        {
            get
            { return items[index]; }
            set
            { items[index] = value; }
        }

        public string[] SiteLookups
        {
            get
            {
                if (siteLookups == null)
                {
                    siteLookups = GetSiteLookups();
                }
                return siteLookups;
            }
        }

        public string[] AllSiteLookups
        {
            get
            {
                if (allSiteLookups == null)
                {
                    allSiteLookups = GetAllSiteLookups();
                }
                return allSiteLookups;
            }
            set
            {
                allSiteLookups = value;
            }
        }

         public KxSiteAvailabilityLookups(KxBaseClass parent)
            : base(parent)
        {
            Load();
        }

        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = null;

            if (actiontype == ActionType.Select)
            {
                result = new KxData("siteavailabilitylookup", "SiteAvailabilityLookup", TargetType.Table, ActionType.Select);
                result.AddOrderField("cityname", KxDataOrderType.Ascending);
                result.AddOrderField("propertyname", KxDataOrderType.Ascending);
                
            }
            return result;
        }

        public override bool LoadFromXml(string xml, string xpath)
        {
            items.Clear();
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xml);

                XmlNodeList nodes = doc.SelectNodes(@"//results/" + xpath + "/datarow");

                foreach (XmlNode node in nodes)
                {
                    if (node != null)
                    {
                        KxSiteAvailabilityLookup item = new KxSiteAvailabilityLookup(this, node);
                        item.Index = items.Count+1;
                        items.Add(item);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "LoadFromXml", ex.Message, xpath);
                LastError = ex.Message;
                return false;
            }
        }

        public KxSiteAvailabilityLookup GetSiteAvailabilityLookup(int siteId,int areaId, int blockId)
        {
            KxSiteAvailabilityLookup result = null;

            foreach (KxSiteAvailabilityLookup i in Items)
            {
                if (i.SiteId == siteId && i.AreaId == areaId && i.BlockId == blockId)
                {
                    result = i;
                    break;
                }
            }
            return result;
        }

        public KxSiteAvailabilityLookup GetSiteAvailabilityLookup(string id)
        {
            KxSiteAvailabilityLookup result = null;

            foreach (KxSiteAvailabilityLookup i in Items)
            {
                if (i.UniqueId == id)
                {
                    result = i;
                    break;
                }
            }
            return result;
        }

        public KxSiteAvailabilityLookup GetSiteAvailabilityLookupByIndex(int idx)
        {
            KxSiteAvailabilityLookup result = null;

            foreach (String str in SiteLookups)
            {
                KxSiteAvailabilityLookup temp = GetSiteAvailabilityLookup(SiteLookups[idx]);

                if (temp != null)
                {
                    result = temp;
                    break;
                }
            }

            return result;
        }

        public string[] GetSiteLookups()
        {
            List<string> result = new List<string>();

            int index = 0;
            foreach (KxSiteAvailabilityLookup i in Items)
            {
                if (i.RoomsAvailable)
                {
                    result.Add(i.UniqueId);
                    i.Index = index;
                    index++;
                }
            }
            
            return result.ToArray();
        }

        public string[] GetAllSiteLookups()
        {
            List<string> result = new List<string>();

            int index = 0;
            foreach (KxSiteAvailabilityLookup i in Items)
            {
                result.Add(i.UniqueId);
                i.Index = index;
                index++;
            }

            return result.ToArray();
        }

        public KxSiteAvailabilityLookup AddSiteAvailability()
        {
            KxSiteAvailabilityLookup result = new KxSiteAvailabilityLookup(this);

            items.Add(result);

            return result;
        }
    }
}

