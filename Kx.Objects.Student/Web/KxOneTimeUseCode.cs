using System;
using System.CodeDom;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;

namespace Kx.Objects.Student.Web
{
    public enum OneTimeUseCodeResponses
    {
        Valid,
        Invalid,
        Used
    }

    [Serializable]
    public class KxOneTimeUseCode : KxBaseClass
    {
        #region Attributes & Properties

        protected string _oneTimeUseCode;
        protected DateTime? _creationDate;
        protected string _createdBy;
        protected DateTime? _usedOn;
        protected Guid _usedByApplicationGuid;
        protected Guid _usedByUserGuid;
        protected string _usedByAdditional;
        private bool _adding = false;
        private string _issuedTo;

        public string OneTimeUseCode
        {
            get { return _oneTimeUseCode; }
            set
            { 
                AddToAudit("OneTimeUseCode", _oneTimeUseCode, value);
                _oneTimeUseCode = value;
                _adding = true;
            }
        }

        public DateTime? CreationDate
        {
            get { return _creationDate; }
            set
            { 
                AddToAudit("CreationDate", _creationDate, value);
                _creationDate = value;
            }
        }

        public string CreatedBy
        {
            get { return _createdBy; }
            set
            { 
                AddToAudit("CreatedBy", _createdBy, value);
                _createdBy = value;
            }
        }

        public DateTime? UsedOn
        {
            get { return _usedOn; }
            set
            { 
                AddToAudit("UsedOn", _usedOn, value);
                _usedOn = value;
            }
        }

        public Guid UsedByApplicationGuId
        {
            get { return _usedByApplicationGuid; }
            set
            { 
                AddToAudit("UsedByApplicationGuId", _usedByApplicationGuid, value);
                _usedByApplicationGuid = value;
            }
        }

        public Guid UsedByUserGuId
        {
            get { return _usedByUserGuid; }
            set
            { 
                AddToAudit("UsedByUserGuId", _usedByUserGuid, value);
                _usedByUserGuid = value;
            }
        }

        public string UsedByAdditional
        {
            get { return _usedByAdditional; }
            set
            { 
                AddToAudit("UsedByAdditional", _usedByAdditional, value);
                _usedByAdditional = value;
            }
        }

        public string IssuedTo
        {
            get { return _issuedTo; }
            set
            {
                AddToAudit("IssuedTo", _issuedTo, value);
                _issuedTo = value;
            }
        }

        #endregion

        public KxOneTimeUseCode()
            : base(null)
        {
            
        }

        public KxOneTimeUseCode(string oneTimeUseCode)
            : this()
        {
            _oneTimeUseCode = oneTimeUseCode;
            Load();
        }

        public KxOneTimeUseCode(XmlNode node)
            : this()
        {
            LoadFromXml("", node);
        }

        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("onetimeusecodes", "kcf.OneTimeUseCodes", TargetType.Table);


             if ((actiontype == ActionType.Update) && (_adding))
             {
                 result.Action = ActionType.Insert;
                 result.AddParameter("oneTimeUseCode", _oneTimeUseCode, KxValueType.String, 20, false);
             }
             else
             {
                 result.Action = actiontype;
                 result.AddKeyField("onetimeusecode", _oneTimeUseCode, KxValueType.String);
             }

            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {
                result.AddParameter("creationDate", _creationDate, KxValueType.DateTime, false);
                result.AddParameter("createdBy", _createdBy, KxValueType.String, 50, false);
                result.AddParameter("usedOn", _usedOn, KxValueType.DateTime, true);
                result.AddParameter("usedByApplicationGuid", _usedByApplicationGuid, KxValueType.Guid, true);
                result.AddParameter("usedByUserGuid", _usedByUserGuid, KxValueType.Guid, true);
                result.AddParameter("usedByAdditional", _usedByAdditional, KxValueType.String, 2147483647, true);
                result.AddParameter("issuedto", _issuedTo, KxValueType.String, 50, true);
            }
            return result;
        }


        public override  void LoadFromXml(string xml,XmlNode node)
        {
            _oneTimeUseCode  = KxXmlUtils.GetNodeValue(node,"onetimeusecode",string.Empty);
            _creationDate  = KxXmlUtils.GetNodeValueDT(node,"creationdate",null);
            _createdBy  = KxXmlUtils.GetNodeValue(node,"createdby",string.Empty);
            _usedOn  = KxXmlUtils.GetNodeValueDT(node,"usedon",null);
            _usedByApplicationGuid  = KxXmlUtils.GetNodeValue(node,"usedbyapplicationguid",Guid.Empty);
            _usedByUserGuid  = KxXmlUtils.GetNodeValue(node,"usedbyuserguid",Guid.Empty);
            _usedByAdditional  = KxXmlUtils.GetNodeValue(node,"usedbyadditional",string.Empty);
            _issuedTo = KxXmlUtils.GetNodeValue(node, "issuedto", string.Empty);
        }

        public static IEnumerable<KxOneTimeUseCode> GetAllOneTimeUseCodes ()
        {
            KxData data = new KxData("onetimeusecodes", "kcf.OneTimeUseCodes", TargetType.Table);
            if (data.Execute ())
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(data.XmlResults);
                XmlNodeList nodes = doc.SelectNodes(@"//onetimeusecodes/datarow");
                foreach(XmlNode node in nodes)
                {
                     yield return new KxOneTimeUseCode(node);
                }
            }
        }

        public static OneTimeUseCodeResponses ValidateMultipleUseCode(string code)
        {
            OneTimeUseCodeResponses result = OneTimeUseCodeResponses.Invalid;
            KxOneTimeUseCode codeObj = new KxOneTimeUseCode(code);
            if (codeObj.CreationDate.HasValue)
            {
                if (!codeObj.UsedOn.HasValue)
                {
                    result = OneTimeUseCodeResponses.Valid;
                }
                else if (codeObj.UsedOn.HasValue)
                {
                    result = OneTimeUseCodeResponses.Used;
                }
            }

            return result;
        }

        public static OneTimeUseCodeResponses ValidateOneTimeUseCode(string code, Guid applicationGuid, Guid userGuid, HttpContextBase context)
        {
            OneTimeUseCodeResponses result = OneTimeUseCodeResponses.Invalid;
            KxOneTimeUseCode codeObj = new KxOneTimeUseCode(code);
            if (codeObj.CreationDate.HasValue)
            {
                if (!codeObj.UsedOn.HasValue)
                {
                    result = OneTimeUseCodeResponses.Valid;
                    codeObj.UsedOn = DateTime.Now;
                    codeObj.UsedByApplicationGuId = applicationGuid;
                    codeObj.UsedByUserGuId = userGuid;
                    codeObj.UsedByAdditional = string.Format("SourceIP: {0};Browser: {1}", context.Request.UserHostAddress, string.Format("{0} {1}", context.Request.Browser.Browser, context.Request.Browser.Version));
                    if (!codeObj.Save())
                    {
                        KxLogger.LogActivity("OnetimeUseCodes", "ValidateCode", codeObj.LastError, "");
                    }
                }
                else if (codeObj.UsedOn.HasValue)
                {
                    if (codeObj.UsedByApplicationGuId == applicationGuid)
                    {
                        result = OneTimeUseCodeResponses.Valid;
                        KxLogger.LogActivity("OneTimeUseCodes", "ValidateCode", string.Format("Code {0} has been re-validated against the same application {1}", code, applicationGuid), "");
                    }
                    else
                    {
                        result = OneTimeUseCodeResponses.Used;
                    }
                }
            }

            return result;
        }
    }
}

