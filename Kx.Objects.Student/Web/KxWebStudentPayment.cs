using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;

namespace Kx.Objects.Student.Web
{
    public enum KxPaymentType {Deposit,Payment,Instalment,Ancillary,Unknown,Surcharge,Statement};


    [Serializable]
    public class KxWebStudentPayment : KxBaseClass
    {
        #region Attributes & Properties

        protected Guid     applicationGuid;
        protected DateTime ?paymentDate;
        protected string   transactionReference;
        protected decimal     transactionAmount;
        protected string   gatewayName;
        protected KxPaymentType paymentType = KxPaymentType.Unknown;

        public int      WebPaymentId
        {
            get
            { return base.dBIdentifier;}
        }

        public Guid     ApplicationGuid
        {
            get
            { return applicationGuid;}
            set
            { 
                AddToAudit("ApplicationGuid", applicationGuid, value);
                applicationGuid = value;
            }
        }

        public DateTime ?PaymentDate
        {
            get
            { return paymentDate;}
            set
            { 
                AddToAudit("PaymentDate", paymentDate, value);
                paymentDate = value;
            }
        }

        public string   TransactionReference
        {
            get
            { return transactionReference;}
            set
            { 
                AddToAudit("TransactionReference", transactionReference, value);
                transactionReference = value;
            }
        }

        public decimal     TransactionAmount
        {
            get
            { return transactionAmount;}
            set
            { 
                AddToAudit("TransactionAmount", transactionAmount, value);
                transactionAmount = value;
            }
        }

        public string   GatewayName
        {
            get
            { return gatewayName;}
            set
            { 
                AddToAudit("GatewayName", gatewayName, value);
                gatewayName = value;
            }
        }

        public KxPaymentType PaymentType
        {
            get
            { return paymentType;}
            set
            { 
                AddToAudit("PaymentType", paymentType, value);
                paymentType = value;
            }
        }


        #endregion 

        public KxWebStudentPayment(KxBaseClass parent) : base(parent)
        {
        }

        public KxWebStudentPayment(KxBaseClass parent, int webpaymentid)
            : this(parent)
        {
             base.dBIdentifier = webpaymentid;
             Load();
        }


        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("webstudentpayment", "WebStudentPayment", TargetType.Table);

            result.AutoKeyField = new KxDataValue("WebPaymentId",WebPaymentId,KxValueType.Int);


            if ((actiontype == ActionType.Update) && (WebPaymentId == 0))
            {
                result.Action = ActionType.Insert;
            }
            else
            {
                result.Action = actiontype;
            }

            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {
                result.AddParameter("applicationGuid", applicationGuid, KxValueType.Guid, false);
                result.AddParameter("paymentDate", paymentDate, KxValueType.DateTime, true);
                result.AddParameter("transactionReference", transactionReference, KxValueType.String, 255, true);
                result.AddParameter("transactionAmount", transactionAmount, KxValueType.Decimal, true);
                result.AddParameter("gatewayName", gatewayName, KxValueType.String, 50, true);
                result.AddParameter("paymentType", PaymentTypeToString(paymentType), KxValueType.String, 50, true);
            }
            return result;
        }

        public override void LoadFromXml(string xml,XmlNode node)
        {
            applicationGuid = KxXmlUtils.GetNodeValue(node, "applicationguid", Guid.Empty);
            paymentDate = KxXmlUtils.GetNodeValueDT(node, "paymentdate", null);
            transactionReference = KxXmlUtils.GetNodeValue(node, "transactionreference", "");
            transactionAmount = KxXmlUtils.GetNodeValue(node, "transactionamount", (decimal)0);
            gatewayName = KxXmlUtils.GetNodeValue(node, "gatewayname", "");
            paymentType = PaymentTypeFromString(KxXmlUtils.GetNodeValue(node, "paymenttype", ""));
        }

        public static string PaymentTypeToString(KxPaymentType value)
        {
            switch (value)
            {
                case KxPaymentType.Ancillary: return "ANCILLARY";
                case KxPaymentType.Deposit: return "DEPOSIT"; 
                case KxPaymentType.Payment: return "PAYMENT"; 
                case KxPaymentType.Instalment: return "INSTALMENT";
                case KxPaymentType.Surcharge: return "SURCHARGE";
                case KxPaymentType.Statement: return "STATEMENT"; 
                default : return "UNKNOWN";
            }
        }

        public static KxPaymentType PaymentTypeFromString(string value)
        {
             switch (value.ToUpper())
             {
                 case "INSTALMENT": return KxPaymentType.Instalment;
                 case "ANCILLARY": return KxPaymentType.Ancillary;
                 case "DEPOSIT": return KxPaymentType.Deposit;
                 case "PAYMENT": return KxPaymentType.Payment;
                 case "SURCHARGE": return KxPaymentType.Surcharge;
                 case "STATEMENT": return KxPaymentType.Statement;
                 default: return KxPaymentType.Unknown;
             }
        }


    }
}

