using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Exceptions;
using Kx.Core.Utility;

namespace Kx.Objects.Student.Web
{
    public class KxWebOpalAvailabilityList : KxBaseClass
    {
        protected List<KxWebOpalAvailability> items = new List<KxWebOpalAvailability>();

        protected bool loadRequired = false;

        protected int academicYearId = 0;
        protected string cityName = "";
        protected int areaId = 0;
        protected int bedroomTypeId = 0;
        protected string letLength = "";
        protected int paymentScheduleType = 0;

        public string CityName
        {
            get
            {
                return cityName;
            }
            set
            {
                if (value != null && cityName != value)
                {
                    loadRequired = true;
                }
                cityName = value;
            }
        }
        public int AreaId
        {
            get
            {
                return areaId;
            }
            set
            {
                if (value != null && areaId != value)
                {
                    loadRequired = true;
                }
                areaId = value;
            }
        }
        public int BedroomTypeId
        {
            get
            {
                return bedroomTypeId;
            }
            set
            {
                if (value != null && bedroomTypeId != value)
                {
                    loadRequired = true;
                }
                bedroomTypeId = value;
            }
        }
        public string LetLength
        {
            get
            {
                return letLength;
            }
            set
            {
                if (value != null && letLength != value)
                {
                    loadRequired = true;
                }
                letLength = value;
            }
        }
        public int PaymentScheduleType
        {
            get
            {
                return paymentScheduleType;
            }
            set
            {
                if (value != null && paymentScheduleType != value)
                {
                    loadRequired = true;
                }
                paymentScheduleType = value;
            }
        }

        public bool ShowAvailability
        {
            get
            {
                return GetShowAvailability();
            }
        }


        public bool HasDefaultAvailability
        {
            get
            {
                return DefaultAvailability != null;
            }
        }

        public KxWebOpalAvailability DefaultAvailability
        {
            get
            {
                if (loadRequired)
                {
                    Load();
                }
                if (Items.Count == 1)
                {
                    return Items[0];
                }
                else
                {
                    return null;
                }
            }
        }


        public List<KxWebOpalAvailability> Items
        {
            get
            { return items; }
        }

        public KxWebOpalAvailability this[int index]
        {
            get
            { return items[index]; }
            set
            { items[index] = value; }
        }

        public string[] CurrentItemsArray
        {
            get
            {
                return GetCurrentItemsArray();
            }
        }



        public KxWebOpalAvailabilityList(KxBaseClass parent, int academicyearid)
            : base(parent)
        {
            academicYearId = academicyearid;
        }

        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = null;

            if (actiontype == ActionType.Select && cityName != "")
            {
                result = new KxData("woa", "GetOpalAvailability", TargetType.StoredProc, ActionType.ExecuteProcedure);

                result.AddParameter("academicyearid", academicYearId, KxValueType.Int);

                result.AddParameter("cityname", cityName, KxValueType.String);
                

                if (letLength != "")
                {
                    result.AddParameter("letLength", letLength, KxValueType.String);
                }

                if (areaId > 0)
                {
                    result.AddParameter("areaId", areaId, KxValueType.Int);
                }

                if (bedroomTypeId > 0)
                {
                    result.AddParameter("bedroomtypeId", bedroomTypeId, KxValueType.Int);
                }

                if (paymentScheduleType == 1)
                {
                    result.AddParameter("AnnualPayments", 1, KxValueType.Int);
                }
                else if (paymentScheduleType == 2)
                {
                    result.AddParameter("InstalmentPayments", 1, KxValueType.Int);
                }
                
            }
            return result;
        }

        public override bool LoadFromXml(string xml, string xpath)
        {
            items.Clear();
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xml);

                XmlNodeList nodes = doc.SelectNodes(@"//results/" + xpath + "/datarow");

                foreach (XmlNode node in nodes)
                {
                    if (node != null)
                    {
                        KxWebOpalAvailability item = new KxWebOpalAvailability(this, node);
                        items.Add(item);
                    }
                }
                loadRequired = false;
                return true;
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "LoadFromXml", ex.Message, xpath);
                LastError = ex.Message;
                return false;
            }
            
        }

        public KxWebOpalAvailability GetWebOpalAvailability(string id)
        {
            KxWebOpalAvailability result = null;

            foreach (KxWebOpalAvailability i in Items)
            {
                if (i.UniqueId == id)
                {
                    result = i;
                    break;
                }
            }
            return result;
        }

        private string[] GetCurrentItemsArray()
        {
            if (loadRequired)
            {
                Load();
            }

            List<string> values = new List<string>();

            foreach (KxWebOpalAvailability avail in Items)
            {
                values.Add(KxStringUtils.StringToAttributeSafeString( avail.UniqueId));
            }

            return values.ToArray();
        }

        private bool GetShowAvailability()
        {
            return HasDefaultAvailability && cityName != String.Empty && areaId > 0 && bedroomTypeId > 0 && letLength != String.Empty && PaymentScheduleType > 0;
        }


   
    }
}

