using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;
using System.Threading;
using System.Globalization;

namespace Kx.Objects.Student
{
    [Serializable]
    public class KxWebStudentRoomCityRate : KxBaseClass
    {
        #region Attributes & Properties

        protected int allocationModelId;
        protected int defaultPricingModelId;
        protected int bedroomTypeId;
        protected string roomType;
        protected string rentPerWeek;
        protected int noWeeks;
        protected string singlePayment;
        protected string installments;
        protected string monthly;
        protected int unavailable;
        protected string comment;
        protected int sortType;
        protected decimal weeklyAmount;
		protected decimal fourWeeklyAmount;
        protected DateTime? dueDate;
        protected DateTime? startDate;
        protected DateTime? endDate;

        public int AllocationModelId
        {
            get
            { return allocationModelId; }
            set
            {
                AddToAudit("AllocationModelId", allocationModelId, value);
                allocationModelId = value;
            }
        }

        public int DefaultPricingModelId
        {
            get
            { return defaultPricingModelId; }
            set
            {
                AddToAudit("DefaultPricingModelId", defaultPricingModelId, value);
                defaultPricingModelId = value;
            }
        }

        public int BedroomTypeId
        {
            get
            { return bedroomTypeId; }
            set
            {
                AddToAudit("BedroomTypeId", bedroomTypeId, value);
                bedroomTypeId = value;
            }
        }

        public string RoomType
        {
            get
            { return roomType; }
            set
            {
                AddToAudit("RoomType", roomType, value);
                roomType = value;
            }
        }

        public string RentPerWeek
        {
            get
            { return rentPerWeek; }
            set
            {
                AddToAudit("RentPerWeek", rentPerWeek, value);
                rentPerWeek = value;
            }
        }

        public int NoWeeks
        {
            get
            { return noWeeks; }
            set
            {
                AddToAudit("NoWeeks", noWeeks, value);
                noWeeks = value;
            }
        }

        public string SinglePayment
        {
            get
            { return singlePayment; }
            set
            {
                AddToAudit("SinglePayment", singlePayment, value);
                singlePayment = value;
            }
        }

        public string Installments
        {
            get
            { return installments; }
            set
            {
                AddToAudit("Installments", installments, value);
                installments = value;
            }
        }

        public string Monthly
        {
            get
            { return GetMonthly(); }
            set
            {
                AddToAudit("Monthly", monthly, value);
                monthly = value;
            }
        }

        public int Unavailable
        {
            get
            { return unavailable; }
            set
            {
                AddToAudit("Unavailable", unavailable, value);
                unavailable = value;
            }
        }

        public string Comment
        {
            get
            { return comment; }
            set
            {
                AddToAudit("Comment", comment, value);
                comment = value;
            }
        }

        public int SortType
        {
            get
            { return sortType; }
            set
            {
                AddToAudit("SortType", sortType, value);
                sortType = value;
            }
        }
		private string ValueToString(decimal dValue)
		{
			string sResult;
			NumberFormatInfo numberInfo = Thread.CurrentThread.CurrentCulture.NumberFormat;

			sResult = dValue.ToString("G", numberInfo);
			//KxLogger.LogActivity(this, "KxWebStudentRoomCityRate.FourWeeklyAmountString", sResult, "");
			return sResult;
		}

		public decimal WeeklyAmount
		{
			get
			{ return weeklyAmount; }
			set
			{
				AddToAudit("WeeklyAmount", weeklyAmount, value);
				weeklyAmount = value;
			}
		}

		public string WeeklyAmountString
		{
			get
			{
				return ValueToString(weeklyAmount);
			}
		}

		public decimal FourWeeklyAmount
		{
			get
			{
				return fourWeeklyAmount;
			}
			set
			{
				AddToAudit("FourWeeklyAmount", fourWeeklyAmount, value);
				fourWeeklyAmount = value;
			}
		}

		public string FourWeeklyAmountString
		{
			get
			{	return ValueToString(fourWeeklyAmount);
			}
		}

		public DateTime? DueDate
        {
            get
            { return dueDate; }
            set
            {
                AddToAudit("DueDate", dueDate, value);
                dueDate = value;
            }
        }

        public DateTime? StartDate
        {
            get
            { return startDate; }
            set
            {
                AddToAudit("StartDate", startDate, value);
                startDate = value;
            }
        }

        public DateTime? EndDate
        {
            get
            { return endDate; }
            set
            {
                AddToAudit("EndDate", endDate, value);
                endDate = value;
            }
        }

        public string PeriodDates
        {
            get
            {
                try
                {
                    string result = String.Format("{0:dd MMM yyyy}<br/>to<br/>{1:dd MMM yyyy}", StartDate.Value, EndDate.Value);
                    return result.Replace(" ", "&nbsp;");
                }
                catch
                {
                    return "";
                }
            }
        }

        public string PeriodDates2
        {
            get
            {
                try
                {
                    string result = String.Format("{0:dd MMM yyyy} to {1:dd MMM yyyy}", StartDate.Value, EndDate.Value);
                    return result.Replace(" ", "&nbsp;");
                }
                catch
                {
                    return "";
                }
            }
        }

        public string UniqueId
        {
            get
            {
                return String.Format("{0}A{1}A{2}", AllocationModelId, DefaultPricingModelId, BedroomTypeId);
            }
        }

        #endregion 

        public KxWebStudentRoomCityRate(KxBaseClass parent, XmlNode node)
            : base(parent)
        {
             LoadFromXml("", node);
        }

        protected string GetMonthly()
        {
            decimal result = 0.00M;

            Decimal dec = WeeklyAmount * NoWeeks;

            switch (NoWeeks)
            {
                case 13: result = dec / 3.00M;break;
                case 26: result = dec / 6.00M; break;
                case 43: result = dec / 10.00M; break;
                default: result = dec / 12.00M; break;
            }

            return String.Format("&euro;{0:N02}", result);
        }
        
        public override  void LoadFromXml(string xml,XmlNode node)
        {
            allocationModelId = KxXmlUtils.GetNodeValue(node, "allocationmodelid", 0);
            defaultPricingModelId = KxXmlUtils.GetNodeValue(node, "defaultpricingmodelid", 0);
            bedroomTypeId = KxXmlUtils.GetNodeValue(node, "bedroomtypeid", 0);
            roomType = KxXmlUtils.GetNodeValue(node, "roomtype", "");
            rentPerWeek = KxXmlUtils.GetNodeValue(node, "rentperweek", "");
            noWeeks = KxXmlUtils.GetNodeValue(node, "noweeks", 0);
            singlePayment = KxXmlUtils.GetNodeValue(node, "singlepayment", "");
            installments = KxXmlUtils.GetNodeValue(node, "installments", "");
            monthly = KxXmlUtils.GetNodeValue(node, "monthly", "");
            unavailable = KxXmlUtils.GetNodeValue(node, "unavailable", 0);
            comment = KxXmlUtils.GetNodeValue(node, "comment", "");
            sortType = KxXmlUtils.GetNodeValue(node, "sorttype", 0);
            weeklyAmount = KxXmlUtils.GetNodeValue(node, "weeklyamount", (decimal)0.00M);
            dueDate = KxXmlUtils.GetNodeValueDT(node, "duedate", null);
            startDate = KxXmlUtils.GetNodeValueDT(node, "startdate", null);
            endDate = KxXmlUtils.GetNodeValueDT(node, "enddate", null);
			fourWeeklyAmount = KxXmlUtils.GetNodeValue(node, "fourweeklyamount", (decimal)0.00M);
        }
    }
}

