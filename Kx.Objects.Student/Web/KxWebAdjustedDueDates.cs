using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Exceptions;
using Kx.Core.Utility;

namespace Kx.Objects.Student.Web
{
    public class KxWebAdjustedDueDates : KxBaseClass
    {
        private List<KxWebAdjustedDueDate> items = new List<KxWebAdjustedDueDate>();

        protected int areaIdFilter = 0;
        protected int paymentScheduleTypeIdFilter = 0;

        public int AreaIdFilter
        {
            get
            {
                return areaIdFilter;
            }
            set
            {
                areaIdFilter = value;
            }
        }
        public int PaymentScheduleTypeIdFilter
        {
            get
            {
                return paymentScheduleTypeIdFilter;
            }
            set
            {
                paymentScheduleTypeIdFilter = value;
            }
        }

        public List<KxWebAdjustedDueDate> Items
        {
            get
            { return items; }
        }

        public string[] FilteredDueDatesList
        {
            get
            {
                return GetFilteredDueDatesList();
            }
        }

        public KxWebAdjustedDueDate this[int index]
        {
            get
            { return items[index]; }
            set
            { items[index] = value; }
        }

        public KxWebAdjustedDueDates(KxBaseClass parent)
            : base(parent)
        {
            Load();
        }

        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = null;

            if (actiontype == ActionType.Select)
            {
                result = new KxData("webadjustedduedate", "WebAdjustedDueDate", TargetType.Table, ActionType.Select);

                result.AddOrderField("fromDate", KxDataOrderType.Ascending);
                result.AddOrderField("toDate", KxDataOrderType.Ascending);
            }
            return result;
        }

        public override bool LoadFromXml(string xml, string xpath)
        {
            items.Clear();
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xml);

                XmlNodeList nodes = doc.SelectNodes(@"//results/" + xpath + "/datarow");

                foreach (XmlNode node in nodes)
                {
                    if (node != null)
                    {
                        KxWebAdjustedDueDate item = new KxWebAdjustedDueDate(this, node);
                        items.Add(item);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "LoadFromXml", ex.Message, xpath);
                LastError = ex.Message;
                return false;
            }
        }

        public KxWebAdjustedDueDate GetAdjustedDueDate(int areaId, int paymentScheduleTypeId, DateTime startDate)
        {
            KxWebAdjustedDueDate result = null;

            foreach (KxWebAdjustedDueDate i in Items)
            {
                if (i.AreaId == areaId && i.PaymentScheduleTypeId == paymentScheduleTypeId)
                {
                    if (i.FromDate <= startDate && i.ToDate >= startDate)
                    {
                        result = i;
                        break;
                    }
                }
            }
            return result;
        }

        public KxWebAdjustedDueDate GetAdjustedDueDate(string uniqueId)
        {
            KxWebAdjustedDueDate result = null;

            foreach (KxWebAdjustedDueDate i in Items)
            {
                if (i.UniqueId == uniqueId)
                {
                    result = i;
                    break;
                }
            }
            return result;
        }

        public KxWebAdjustedDueDate AddDueDate()
        {
            KxWebAdjustedDueDate result = new KxWebAdjustedDueDate(this);

            items.Add(result);

            return result;
        }

        public string[] GetFilteredDueDatesList()
        {
            List<string> result = new List<string>();
            if (areaIdFilter > 0 && PaymentScheduleTypeIdFilter > 0)
            {
                foreach (KxWebAdjustedDueDate i in Items)
                {
                    if (i.AreaId == areaIdFilter && i.PaymentScheduleTypeId == paymentScheduleTypeIdFilter)
                    {
                        result.Add(i.UniqueId);
                    }
                }
            }

            return result.ToArray();
        }

       

    }
}

