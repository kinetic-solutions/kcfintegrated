using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;

namespace Kx.Objects.Student.Web
{
    [Serializable]
    public class KxOpalTransfer : KxBaseClass
    {
        #region Attributes & Properties
        protected bool loaded = false;

        protected string referenceId;
        protected string   emailAddress;
        protected string   title;
        protected string foreName;
        protected string   surName;
        protected DateTime ?dob;
        protected string   gender;
        protected string   address1;
        protected string   address2;
        protected string   postcode;
        protected string   town;
        protected string   country;
        protected string   mobileNumber;
        protected string telephoneNumber;
        protected string   university;
        protected string   course;
        protected string   yearStart;
        protected int courseTypeID;

        public bool Loaded
        {
            get
            {
                return loaded;
            }
            set
            {
                loaded = value;
            }
        }

        public string ReferenceId
        {
            get
            { return referenceId; }
            set
            {
                AddToAudit("ReferenceId", referenceId, value);
                referenceId = value;
            }
        }

        public string   EmailAddress
        {
            get
            { return emailAddress;}
            set
            { 
                AddToAudit("EmailAddress", emailAddress, value);
                emailAddress = value;
            }
        }

        public string   Title
        {
            get
            { return title;}
            set
            { 
                AddToAudit("Title", title, value);
                title = value;
            }
        }

        public string ForeName
        {
            get
            { return foreName; }
            set
            {
                AddToAudit("ForeName", foreName, value);
                foreName = value;
            }
        }

        public string   SurName
        {
            get
            { return surName;}
            set
            { 
                AddToAudit("SurName", surName, value);
                surName = value;
            }
        }

        public DateTime ?Dob
        {
            get
            { return dob;}
            set
            { 
                AddToAudit("Dob", dob, value);
                dob = value;
            }
        }

        public string   Gender
        {
            get
            { return gender;}
            set
            { 
                AddToAudit("Gender", gender, value);
                gender = value;
            }
        }

        public string   Address1
        {
            get
            { return address1;}
            set
            { 
                AddToAudit("Address1", address1, value);
                address1 = value;
            }
        }

        public string   Address2
        {
            get
            { return address2;}
            set
            { 
                AddToAudit("Address2", address2, value);
                address2 = value;
            }
        }

        public string   Postcode
        {
            get
            { return postcode;}
            set
            { 
                AddToAudit("Postcode", postcode, value);
                postcode = value;
            }
        }

        public string   Town
        {
            get
            { return town;}
            set
            { 
                AddToAudit("Town", town, value);
                town = value;
            }
        }

        public string   Country
        {
            get
            { return country;}
            set
            { 
                AddToAudit("Country", country, value);
                country = value;
            }
        }

        public string   MobileNumber
        {
            get
            { return mobileNumber;}
            set
            { 
                AddToAudit("MobileNumber", mobileNumber, value);
                mobileNumber = value;
            }
        }

        public string TelephoneNumber
        {
            get
            {
                return telephoneNumber;
            }
            set
            {
                AddToAudit("TelephoneNumber", telephoneNumber, value);
            	telephoneNumber = value;
            }
        }

        public string   University
        {
            get
            { return university;}
            set
            { 
                AddToAudit("University", university, value);
                university = value;
            }
        }

        public string   Course
        {
            get
            { return course;}
            set
            { 
                AddToAudit("Course", course, value);
                course = value;
            }
        }

        public string   YearStart
        {
            get
            { return yearStart;}
            set
            { 
                AddToAudit("YearStart", yearStart, value);
                yearStart = value;
            }
        }

        public int CourseTypeID
        {
            get
            {
                return courseTypeID;
            }
            set
            {
                AddToAudit("CourseTypeID", courseTypeID, value);
                courseTypeID = value;
            }
        }

        #endregion 

        public KxOpalTransfer(KxBaseClass parent): base(parent)
        {
        }

        public KxOpalTransfer(KxBaseClass parent, string reference)
            : base(parent)
        {
            referenceId = reference;
            Load();
        }

        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("opaltransfer", "OpalTransfer", TargetType.Table);

            result.KeyFields.Add(new KxDataValue("reference_id",referenceId,KxValueType.String));


            if ((actiontype == ActionType.Update) && (!loaded))
            {
                result.Action = ActionType.Insert;
            }
            else
            {
                result.Action = actiontype;
            }

            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {
                result.AddParameter("emailAddress", emailAddress, KxValueType.String, 50, true);
                   result.AddParameter("title", title, KxValueType.String, 10, true);
                   result.AddParameter("foreName", foreName, KxValueType.String, 50, true);
                   result.AddParameter("surName", surName, KxValueType.String, 50, true);
                   result.AddParameter("dob", dob, KxValueType.DateTime, true);
                   result.AddParameter("gender", gender, KxValueType.String, 1, true);
                   result.AddParameter("address1", address1, KxValueType.String, 120, true);
                   result.AddParameter("address2", address2, KxValueType.String, 120, true);
                   result.AddParameter("postCode", postcode, KxValueType.String, 30, true);
                   result.AddParameter("town", town, KxValueType.String, 10, true);
                   result.AddParameter("country", country, KxValueType.String, 10, true);
                   result.AddParameter("mobileNumber", mobileNumber, KxValueType.String, 25, true);
                   result.AddParameter("telephoneNumber", telephoneNumber, KxValueType.String, 25, true);
                   result.AddParameter("university", university, KxValueType.String, 50, true);
                   result.AddParameter("course", course, KxValueType.String, 50, true);
                   result.AddParameter("yearStart", yearStart, KxValueType.String, 10, true);
            }
            return result;
        }


        public override  void LoadFromXml(string xml,XmlNode node)
        {
            referenceId = KxXmlUtils.GetNodeValue(node, "reference_id", "");
            emailAddress  = KxXmlUtils.GetNodeValue(node,"emailaddress","");
            title  = KxXmlUtils.GetNodeValue(node,"title","");
            foreName = KxXmlUtils.GetNodeValue(node, "forename", "");
            surName  = KxXmlUtils.GetNodeValue(node,"surname","");
            dob  = KxXmlUtils.GetNodeValueDT(node,"dob",null);
            gender  = KxXmlUtils.GetNodeValue(node,"gender","");
            address1  = KxXmlUtils.GetNodeValue(node,"address1","");
            address2  = KxXmlUtils.GetNodeValue(node,"address2","");
            postcode  = KxXmlUtils.GetNodeValue(node,"postcode","");
            town  = KxXmlUtils.GetNodeValue(node,"town","");
            country  = KxXmlUtils.GetNodeValue(node,"country","");
            mobileNumber  = KxXmlUtils.GetNodeValue(node,"mobilenumber","");
            telephoneNumber = KxXmlUtils.GetNodeValue(node, "telephonenumber", "");
            university = KxXmlUtils.GetNodeValue(node, "university", "");
            course  = KxXmlUtils.GetNodeValue(node,"course","");
            yearStart  = KxXmlUtils.GetNodeValue(node,"yearstart","");
            courseTypeID = KxXmlUtils.GetNodeValue(node, "coursetypeid", 0);

            loaded = !String.IsNullOrEmpty(foreName);
        }
    }
}

