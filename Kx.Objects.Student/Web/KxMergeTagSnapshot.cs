using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Merge;
using Kx.Core.Utility;
using Kx.Core.Exceptions;

namespace Kx.Objects.Student.Web
{
    [Serializable]
    public class KxMergeTagSnapshot : KxBaseClass
    {
        #region Attributes & Properties

        protected string _mergeTagSet;
        protected int _allocationId;
        protected int _kxStudentId;
        protected DateTime? _creationDate;
        protected string _mergeTag;
        protected string _mergeData;

        public int SnapshotId
        {
            get { return base.dBIdentifier; }
        }

        public string MergeTagSet
        {
            get { return _mergeTagSet; }
            set
            { 
                AddToAudit("MergeTagSet", _mergeTagSet, value);
                _mergeTagSet = value;
            }
        }

        public int AllocationId
        {
            get { return _allocationId; }
            set
            { 
                AddToAudit("AllocationId", _allocationId, value);
                _allocationId = value;
            }
        }

        public int KxStudentId
        {
            get { return _kxStudentId; }
            set
            { 
                AddToAudit("KxStudentId", _kxStudentId, value);
                _kxStudentId = value;
            }
        }

        public DateTime? CreationDate
        {
            get { return _creationDate; }
            set
            { 
                AddToAudit("CreationDate", _creationDate, value);
                _creationDate = value;
            }
        }

        public string MergeTag
        {
            get { return _mergeTag; }
            set
            { 
                AddToAudit("MergeTag", _mergeTag, value);
                _mergeTag = value;
            }
        }

        public string MergeData
        {
            get { return _mergeData; }
            set
            { 
                AddToAudit("MergeData", _mergeData, value);
                _mergeData = value;
            }
        }


        #endregion 


        public KxMergeTagSnapshot(KxBaseClass parent):base(parent)
        { }


        public KxMergeTagSnapshot(KxBaseClass parent, XmlNode node):base(parent)
        {
             LoadFromXml("", node);
        }

        public KxMergeTagSnapshot(KxBaseClass parent,int  snapshotid):base(parent)
        {
             base.dBIdentifier = snapshotid;
             Load();
        }


        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("mergetagsnapshot", "kcf.MergeTagSnapshot", TargetType.Table);

            result.AutoKeyField = new KxDataValue("SnapshotId",SnapshotId,KxValueType.Int);


            if ((actiontype == ActionType.Update) && (SnapshotId == 0))
            {
                result.Action = ActionType.Insert;
            }
            else
            {
                result.Action = actiontype;
            }

            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {
                result.AddParameter("mergeTagSet", _mergeTagSet, KxValueType.String, 50, false);
                result.AddParameter("allocationId", _allocationId, KxValueType.Int, false);
                result.AddParameter("kxStudentId", _kxStudentId, KxValueType.Int, false);
                result.AddParameter("creationDate", _creationDate, KxValueType.DateTime, false);
                result.AddParameter("mergeTag", _mergeTag, KxValueType.String, 255, false);
                result.AddParameter("mergeData", _mergeData, KxValueType.String, 2147483647, false);
            }
            return result;
        }


        public override  void LoadFromXml(string xml,XmlNode node)
        {
            dBIdentifier  = KxXmlUtils.GetNodeValue(node,"snapshotid",0);
            _mergeTagSet  = KxXmlUtils.GetNodeValue(node,"mergetagset",string.Empty);
            _allocationId  = KxXmlUtils.GetNodeValue(node,"allocationid",0);
            _kxStudentId  = KxXmlUtils.GetNodeValue(node,"kxstudentid",0);
            _creationDate  = KxXmlUtils.GetNodeValueDT(node,"creationdate",null);
            _mergeTag  = KxXmlUtils.GetNodeValue(node,"mergetag",string.Empty);
            _mergeData  = KxXmlUtils.GetNodeValue(node,"mergedata",string.Empty);
        }

        public static IEnumerable<KxMergeTagSnapshot> GetMergeTagSnapshot(KxMergeTagSetName mergeTagSet, int allocationId, int kxStudentId)
        {
            KxData data = new KxData("mergetagsnapshot", "kcf.MergeTagSnapshot", TargetType.Table);
            data.AddKeyField("mergetagset", mergeTagSet.ToString(), KxValueType.String);
            data.AddKeyField("allocationid", allocationId, KxValueType.Int);
            data.AddKeyField("kxstudentid", kxStudentId, KxValueType.Int);
            if (data.Execute ())
            {
                foreach(XmlNode node in data.GetNodesFromData())
                {
                     yield return new KxMergeTagSnapshot(null, node);
                }
            }
            else
            {
                KxLogger.LogActivity("KxMergeTagSnapshot", "GetMergeTagSnapshot", data.LastError, string.Format("{0}, Alloc:{1}, KxStuID{2}", mergeTagSet, allocationId, kxStudentId));
            }
        }
    }
}

