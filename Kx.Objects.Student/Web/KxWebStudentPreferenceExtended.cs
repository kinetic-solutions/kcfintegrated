using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;
using Kx.Objects.Common;

namespace Kx.Objects.Student.Web
{
    public partial class KxWebStudentPreference : KxBaseClass, IComparable<KxWebStudentPreference>
    {
        #region Attributes & Properties

        protected bool siteIdChanged = false;
        protected bool areaIdChanged = false;
        protected bool blockIdChanged = false;
        protected bool bedroomTypeIdChanged = false;
        protected bool floorChanged = false;
        protected bool singleSexAreaChanged = false;
        protected bool smokingChanged = false;
        protected bool customField1Changed = false;
        protected bool customField2Changed = false;
        protected bool customValue1Changed = false;
        protected bool customValue2Changed = false;
        protected bool customValue3Changed = false;
        protected bool customValue4Changed = false;
        protected bool customValue5Changed = false;
        protected bool customValue6Changed = false;
        protected bool customValue7Changed = false;
        protected bool customValue8Changed = false;

        public int      SiteIdExtended
        {
            get
            {
                return siteId;}
            set
            { 
                SiteId = value;
                siteIdChanged = true;
            }
        }
        public int AreaIdExtended
        {
            get
            { return areaId;}
            set
            { 
                AreaId = value;
                areaIdChanged = true;
            }
        }

        public int BlockIdExtended
        {
            get
            { return blockId;}
            set
            { 
                BlockId = value;
                blockIdChanged = true;
            }
        }

        public int BedroomTypeIdExtended
        {
            get
            { return bedroomTypeId;}
            set
            { 
                BedroomTypeId = value;
                bedroomTypeIdChanged = true;
            }
        }
        public int FloorExtended
        {
            get
            { return floor;}
            set
            { 
                Floor = value;
                floorChanged = true;
            }
        }
        public int SingleSexAreaExtended
        {
            get
            { return singleSexArea;}
            set
            { 
                SingleSexArea = value;
                singleSexAreaChanged = true;
            }
        }


        public int SmokingExtended
        {
            get
            { return smoking;}
            set
            { 
                Smoking = value;
                smokingChanged = true;
            }
        }

        public int CustomField1Extended
        {
            get
            { return customField1;}
            set
            { 
                CustomField1 = value;
                customField1Changed = true;
            }
        }

        public int CustomField2Extended
        {
            get
            { return customField2;}
            set
            { 
                CustomField2 = value;
                customField2Changed = true;
            }
        }

        public string CustomValue1Extended
        {
            get
            { return customValue1;}
            set
            { 
                CustomValue1 = value;
                customValue1Changed = true;
            }
        }
        public string CustomValue2Extended
        {
            get
            { return customValue2;}
            set
            { 
                CustomValue2 = value;
                customValue2Changed = true;
            }
        }
        public string CustomValue3Extended
        {
            get
            { return customValue3;}
            set
            { 
                CustomValue3 = value;
                customValue3Changed = true;
            }
        }
        public string CustomValue4Extended
        {
            get
            { return customValue4;}
            set
            { 
                CustomValue4 = value;
                customValue4Changed = true;
            }
        }
        public string CustomValue5Extended
        {
            get
            { return customValue5; }
            set
            {
                CustomValue5 = value;
                customValue5Changed = true;
            }
        }
        public string CustomValue6Extended
        {
            get
            { return customValue6; }
            set
            {
                CustomValue6 = value;
                customValue6Changed = true;
            }
        }
        public string CustomValue7Extended
        {
            get
            { return customValue7; }
            set
            {
                CustomValue7 = value;
                customValue7Changed = true;
            }
        }
        public string CustomValue8Extended
        {
            get
            { return customValue8; }
            set
            {
                CustomValue8 = value;
                customValue8Changed = true;
            }
        }

        public string ExtendedPreference
        {
            get
            {
                return GetExtendedPreference();
            }
            set
            {
                SetExtendedPreference(value);
            }
        }

     

        #endregion 

     

        protected string GetExtendedPreference()
        {
            StringBuilder sb = new StringBuilder();

            if (siteIdChanged)
            {
                sb.Append(SiteId.ToString());
            }
            sb.Append("|");
            if (areaIdChanged)
            {
                sb.Append(AreaId.ToString());
            }
            sb.Append("|");
            if (blockIdChanged)
            {
                sb.Append(BlockId.ToString());
            }
            sb.Append("|");
            if (bedroomTypeIdChanged)
            {
                sb.Append(BedroomTypeId.ToString());
            }
            sb.Append("|");
            if (floorChanged)
            {
                sb.Append(Floor.ToString());
            }
            sb.Append("|");
            if (singleSexAreaChanged)
            {
                sb.Append(SingleSexArea.ToString());
            }
            sb.Append("|");
            if (smokingChanged)
            {
                sb.Append(Smoking.ToString());
            }
            sb.Append("|");
            if (customField1Changed)
            {
                sb.Append(CustomField1.ToString());
            }
            sb.Append("|");
            if (customField2Changed)
            {
                sb.Append(CustomField2.ToString());
            }
            sb.Append("|");
            if (customValue1Changed)
            {
                sb.Append(CustomValue1.ToString());
            }
            else
            {
                sb.Append("XXX");
            }
            sb.Append("|");
            if (customValue2Changed)
            {
                sb.Append(CustomValue2.ToString());
            }
            else
            {
                sb.Append("XXX");
            }
            sb.Append("|");
            if (customValue3Changed)
            {
                sb.Append(CustomValue3.ToString());
            }
            else
            {
                sb.Append("XXX");
            }
            sb.Append("|");
            if (customValue4Changed)
            {
                sb.Append(CustomValue4.ToString());
            }
            else
            {
                sb.Append("XXX");
            }
            sb.Append("|");
            if (customValue4Changed)
            {
                sb.Append(CustomValue5.ToString());
            }
            else
            {
                sb.Append("XXX");
            }
            sb.Append("|");
            if (customValue6Changed)
            {
                sb.Append(CustomValue6.ToString());
            }
            else
            {
                sb.Append("XXX");
            }
            sb.Append("|");
            if (customValue7Changed)
            {
                sb.Append(CustomValue7.ToString());
            }
            else
            {
                sb.Append("XXX");
            }
            sb.Append("|");
            if (customValue8Changed)
            {
                sb.Append(CustomValue8.ToString());
            }
            else
            {
                sb.Append("XXX");
            }
            sb.Append("|");
            sb.Append(Ensuite ? "1" : "0");
            


            //sb.AppendFormat("{0}|{1}|{2}|{3}|{4}|{5}|{6}",SiteId,AreaId,BlockId,BedroomTypeId,Floor,SingleSexArea,Smoking);
            //sb.AppendFormat("|{0}|{1}|{2}|{3}|{4}|{5}|{6}",CustomField1,CustomField2,CustomValue1,CustomValue2,CustomValue3,CustomValue4, Ensuite ? 1 : 0);

            return sb.ToString();
        }

        protected void SetExtendedPreference(string value)
        {
            string[] values = value.Split(new char[]{'|'});

            if (values.Length == 14)
            {
                if (values[0] != String.Empty)
                {
                    SiteIdExtended = KxStringUtils.ConvertToInt(values[0], 0);
                }

                if (values[1] != String.Empty)
                {
                    AreaIdExtended = KxStringUtils.ConvertToInt(values[1], 0);
                }

                if (values[2] != String.Empty)
                {
                    BlockIdExtended = KxStringUtils.ConvertToInt(values[2], 0);
                }

                if (values[3] != String.Empty)
                {
                    BedroomTypeIdExtended = KxStringUtils.ConvertToInt(values[3], 0);
                }

                if (values[4] != String.Empty)
                {
                    FloorExtended = KxStringUtils.ConvertToInt(values[4], 0);
                }

                if (values[5] != String.Empty)
                {
                    SingleSexAreaExtended = KxStringUtils.ConvertToInt(values[5], 0);
                }

                if (values[6] != String.Empty)
                {
                    SmokingExtended = KxStringUtils.ConvertToInt(values[6], 0);
                }

                if (values[7] != String.Empty)
                {
                    CustomField1Extended = KxStringUtils.ConvertToInt(values[7], 0);
                }

                if (values[8] != String.Empty)
                {
                    CustomField2Extended = KxStringUtils.ConvertToInt(values[8], 0);
                }

                if (values[9] != "XXX")
                {
                    CustomValue1Extended = values[9];
                }

                if (values[10]  != "XXX")
                {
                    CustomValue2Extended = values[10];
                }

                if (values[11] != "XXX")
                {
                    CustomValue3Extended = values[11];
                }

                if (values[12] != "XXX")
                {
                    CustomValue4Extended = values[12];
                }
                if (values[13] != "XXX")
                {
                    CustomValue5Extended = values[13];
                }
                if (values[14] != "XXX")
                {
                    CustomValue6Extended = values[14];
                }
                if (values[15] != "XXX")
                {
                    CustomValue7Extended = values[15];
                }
                if (values[16] != "XXX")
                {
                    CustomValue8Extended = values[16];
                }
                Ensuite = values[17] == "1";
            }
            else if (value == String.Empty)
            {
                if (siteIdChanged)
                {
                    siteId = 0;
                }

                if (areaIdChanged)
                {
                    areaId = 0;
                }
                if (blockIdChanged)
                {
                    blockId = 0;
                }
                if (bedroomTypeIdChanged)
                {
                    bedroomTypeId = 0;
                }
                if (floorChanged)
                {
                    floor = 0;
                }
                if (singleSexAreaChanged)
                {
                    singleSexArea = 0;
                }
                if (smokingChanged)
                {
                    smoking = 0;
                }
                if (customField1Changed)
                {
                    customField1 = 0;
                }
                if (customField2Changed)
                {
                    customField2 = 0;
                }

                if (customValue1Changed)
                {
                    CustomValue1 = "";
                }

                if (customValue2Changed)
                {
                    CustomValue2 = "";
                }

                if (customValue3Changed)
                {
                    CustomValue3 = "";
                }

                if (customValue4Changed)
                {
                    CustomValue4 = "";
                }
                if (customValue5Changed)
                {
                    CustomValue5 = "";
                }
                if (customValue6Changed)
                {
                    CustomValue6 = "";
                }
                if (customValue7Changed)
                {
                    CustomValue7 = "";
                }
                if (customValue8Changed)
                {
                    CustomValue8 = "";
                }
            }

        }
    }
}

