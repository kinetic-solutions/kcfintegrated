﻿using System;
using System.Collections.Generic;
using System.Text;
using Kx.Objects.Common;
using Kx.Objects.Common.Web;

namespace Kx.Objects.Student.Web
{
    public class KxStudentObjectProvider
    {
        protected KxAreas areas = null;
        protected KxSites sites = null;
        protected KxSiteBlocks siteBlocks = null;
        protected KxBedroomTypes bedroomTypes = null;
        protected KxAllocationModelCache allocationModels = null;
        protected KxAllocationModelDateCache allocationModelDates = null;
        protected KxStudentChargeTypes studentChargeTypes = null;
        protected KxUdfMappings udfMappings = null;
        protected KxeInductionQuestions inductionQuestions = null;
        protected KxeInductionArrivalLocations inductionArrivalLocations = null;
        protected KxeInductionArrivalSlotCache arrivalSlotCache = null;
        protected KxSiteAvailabilityLookups siteAvailabilityLookups = null;
        protected KxWebAllocationPricingList webAllocationPricingList = null;
        protected KxWebOpalAllocationPricingList webOpalAllocationPricingList = null;
        protected KxWebAdjustedDueDates adjustedDueDates = null;
        protected KxPricingModels pricingModels = null;
        protected KxBookingEligibilityRuleList bookingEligibilityRuleList = null;

        public KxWebAdjustedDueDates AdjustedDueDates
        {
            get
            {
                if (adjustedDueDates == null)
                {
                    adjustedDueDates = new KxWebAdjustedDueDates(null);
                }
                return adjustedDueDates;
            }
            set
            {
                adjustedDueDates = value;
            }
        }

        public KxAreas Areas
        {
            get
            {
                if (areas == null)
                {
                    areas = new KxAreas(null);
                }
                return areas;
            }
            set
            {
                areas = value;
            }
        }

        public KxSites Sites
        {
            get
            {
                if (sites == null)
                {
                    sites = new KxSites(null);
                    sites.Load();
                }
                return sites;
            }
            set
            {
                sites = value;
            }
        }

        public KxSiteBlocks SiteBlocks
        {
            get
            {
                if (siteBlocks == null)
                {
                    siteBlocks = new KxSiteBlocks(null);
                    siteBlocks.Load();
                }
                return siteBlocks;
            }
            set
            {
                siteBlocks = value;
            }
        }

        public KxBedroomTypes BedroomTypes
        {
            get
            {
                if (bedroomTypes == null)
                {
                    bedroomTypes = new KxBedroomTypes(null);
                }
                return bedroomTypes;
            }
            set
            {
                bedroomTypes = value;
            }
        }

        public KxAllocationModelCache AllocationModels
        {
            get
            {
                if (allocationModels == null)
                {
                    allocationModels = new KxAllocationModelCache(null);
                }
                return allocationModels;
            }
            set
            {
              
                allocationModels = value;
            }
        }
        public KxAllocationModelDateCache AllocationModelDates
        {
            get
            {
                if (allocationModelDates == null)
                {
                    allocationModelDates = new KxAllocationModelDateCache(null);
                }
                return allocationModelDates;
            }
            set
            {

                allocationModelDates = value;
            }
        }

        public KxSiteAvailabilityLookups SiteAvailabilityLookups
        {
            get
            {
                if (siteAvailabilityLookups == null)
                {
                    siteAvailabilityLookups = new KxSiteAvailabilityLookups(null);
                }
                return siteAvailabilityLookups;
            }
            set
            {
                siteAvailabilityLookups = value;
            }
        }

        public KxWebAllocationPricingList WebAllocationPricingList
        {
            get
            {
                if (webAllocationPricingList == null)
                {
                    webAllocationPricingList = new KxWebAllocationPricingList(null);
                }
                return webAllocationPricingList;
            }
            set
            {
                webAllocationPricingList = value;
            }
        }

        public KxStudentChargeTypes StudentChargeTypes
        {
            get
            {
                if (studentChargeTypes == null)
                {
                    studentChargeTypes = new KxStudentChargeTypes(null);
                }
                return studentChargeTypes;
            }
            set
            {
                studentChargeTypes = value;
            }
        }

        public KxUdfMappings UdfMappings
        {
            get
            {
                if (udfMappings == null)
                {
                    udfMappings = new KxUdfMappings(null);
                }
                return udfMappings;
            }
            set
            {
                udfMappings = value;
            }
        }

        public KxeInductionQuestions InductionQuestions
        {
            get
            {
                if (inductionQuestions == null)
                {
                    inductionQuestions = new KxeInductionQuestions(null);
                }
                return inductionQuestions;
            }
            set
            {
                inductionQuestions = value;
            }
        }

        public KxeInductionArrivalLocations InductionArrivalLocations
        {
            get
            {
                if (inductionArrivalLocations == null)
                {
                    inductionArrivalLocations = new KxeInductionArrivalLocations(null);
                }
                return inductionArrivalLocations;
            }
            set
            {
                inductionArrivalLocations = value;
            }
        }

        public KxeInductionArrivalSlotCache ArrivalSlotCache
        {
            get
            {
                if (arrivalSlotCache == null)
                {
                    arrivalSlotCache = new KxeInductionArrivalSlotCache();
                }
                return arrivalSlotCache;
            }
            set
            {
                arrivalSlotCache = value;
            }
        }

        public KxStudentObjectProvider()
        {
            areas = null;
            sites = null;
            siteBlocks = null;
            bedroomTypes = null;
            allocationModels = null;
            allocationModelDates = null;
            studentChargeTypes = null;
            udfMappings = null;
            inductionQuestions = null;
            inductionArrivalLocations = null;
            arrivalSlotCache = null;
            adjustedDueDates = null;
            pricingModels = null;
            webAllocationPricingList = null;
        }

        public KxPricingModels PricingModels
        {
            get
            {
                if (pricingModels == null)
                {
                    pricingModels = new KxPricingModels(null);
                }
                return pricingModels;
            }
            set
            {
                pricingModels = value;
            }
        }

        public KxWebOpalAllocationPricingList WebOpalAllocationPricingList
        {
            get
            {
                if (webOpalAllocationPricingList == null)
                {
                    webOpalAllocationPricingList = new KxWebOpalAllocationPricingList(null);
                }
                return webOpalAllocationPricingList;
            }
            set
            {
                webOpalAllocationPricingList = value;
            }
        }

        public KxBookingEligibilityRuleList BookingEligibilityRuleList
        {
            get
            {
                if (bookingEligibilityRuleList == null)
                {
                    bookingEligibilityRuleList = new KxBookingEligibilityRuleList(null);
                }
                return bookingEligibilityRuleList;
            }
            set
            {
                bookingEligibilityRuleList = value;
            }
        }
    }
}
