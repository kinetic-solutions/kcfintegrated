using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;

namespace Kx.Objects.Student.Web
{
    [Serializable]
    public class KxKCFWebStat : KxBaseClass
    {
        #region Attributes & Properties

        protected string _activity;
        protected string _trackingId;
        protected DateTime? _trackingTimestamp;
        protected string _statData;

        public int StatId
        {
            get { return base.dBIdentifier; }
        }

        public string Activity
        {
            get { return _activity; }
            set
            { 
                AddToAudit("Activity", _activity, value);
                _activity = value;
            }
        }

        public string TrackingId
        {
            get { return _trackingId; }
            set
            { 
                AddToAudit("TrackingId", _trackingId, value);
                _trackingId = value;
            }
        }

        public DateTime? TrackingTimestamp
        {
            get { return _trackingTimestamp; }
            set
            { 
                AddToAudit("TrackingTimestamp", _trackingTimestamp, value);
                _trackingTimestamp = value;
            }
        }

        public string StatData
        {
            get { return _statData; }
            set
            { 
                AddToAudit("StatData", _statData, value);
                _statData = value;
            }
        }


        #endregion


        public KxKCFWebStat(KxBaseClass parent) : base(parent)
        {
            this._writeAudit = false;
        }


        public KxKCFWebStat(KxBaseClass parent, XmlNode node):this(parent)
        {
             LoadFromXml("", node);
        }

        public KxKCFWebStat(KxBaseClass parent,int  statid):this(parent)
        {
             base.dBIdentifier = statid;
             Load();
        }


        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("kcfwebstats", "KCFWebStats", TargetType.Table);

            result.AutoKeyField = new KxDataValue("StatId",StatId,KxValueType.Int);


            if ((actiontype == ActionType.Update) && (StatId == 0))
            {
                result.Action = ActionType.Insert;
            }
            else
            {
                result.Action = actiontype;
            }

            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {
                result.AddParameter("activity", _activity, KxValueType.String, 255, false);
                result.AddParameter("trackingId", _trackingId, KxValueType.String, 255, true);
                result.AddParameter("trackingTimestamp", _trackingTimestamp, KxValueType.DateTime, false);
                result.AddParameter("statData", _statData, KxValueType.String, 2147483647, true);
            }
            return result;
        }


        public override  void LoadFromXml(string xml,XmlNode node)
        {
            dBIdentifier  = KxXmlUtils.GetNodeValue(node,"statid",0);
            _activity  = KxXmlUtils.GetNodeValue(node,"activity",string.Empty);
            _trackingId  = KxXmlUtils.GetNodeValue(node,"trackingid",string.Empty);
            _trackingTimestamp  = KxXmlUtils.GetNodeValueDT(node,"trackingtimestamp",null);
            _statData  = KxXmlUtils.GetNodeValue(node,"statdata",string.Empty);
        }

        public static IEnumerable<KxKCFWebStat> GetKCFWebStats ()
        {
            KxData data = new KxData("kcfwebstats", "KCFWebStats", TargetType.Table);
            if (data.Execute ())
            {
                foreach(XmlNode node in data.GetNodesFromData())
                {
                     yield return new KxKCFWebStat(null, node);
                }
            }
        }
    }
}

