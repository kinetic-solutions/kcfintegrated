using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Exceptions;
using Kx.Core.Utility;

namespace Kx.Objects.Student
{
    public class KxWebStudentRoomLibertyRates : KxBaseClass
    {
        private List<KxWebStudentRoomLibertyRate> items = new List<KxWebStudentRoomLibertyRate>();
        private int siteId;
        private int areaId;
        private int blockId;
        private int academicYearId;

        public List<KxWebStudentRoomLibertyRate> Items
        {
            get
            { return items; }
        }

        public KxWebStudentRoomLibertyRate this[int index]
        {
            get
            { return items[index]; }
            set
            { items[index] = value; }
        }

        public int SiteId
        {
            get
            {
                return siteId;
            }
            set
            {
                siteId = value;
            }
        }
        public int AreaId
        {
            get
            {
                return areaId;
            }
            set
            {
                areaId = value;
            }
        }
        public int BlockId
        {
            get
            {
                return blockId;
            }
            set
            {
                blockId = value;
            }
        }
        public int AcademicYearId
        {
            get
            {
                return academicYearId;
            }
            set
            {
                academicYearId = value;
            }
        }

        public string[] RoomList
        {
            get
            {
                return GetRoomList();
            }
        }

           public KxWebStudentRoomLibertyRates(KxBaseClass parent, int academicyearid,int siteid,int areaid, int blockid)
            : base(parent)
        {
            AcademicYearId = academicyearid;
            this.areaId = areaid;
            SiteId = siteid;
            BlockId = blockid;
            Load();
        }


        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = null;

            if (actiontype == ActionType.Select)
            {
                result = new KxData("webstudentroomratesliberty", "WebStudentRoomRatesLiberty", TargetType.StoredProc, ActionType.Select);

                result.Parameters.Add(new KxDataValue("academicyearid", AcademicYearId, KxValueType.Int));
                result.Parameters.Add(new KxDataValue("siteid", SiteId, KxValueType.Int));
                result.Parameters.Add(new KxDataValue("blockid", BlockId, KxValueType.Int));
            }
            return result;
        }

        public override bool LoadFromXml(string xml, string xpath)
        {
            items.Clear();
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xml);

                XmlNodeList nodes = doc.SelectNodes(@"//results/" + xpath + "/datarow");

                foreach (XmlNode node in nodes)
                {
                    if (node != null)
                    {
                        KxWebStudentRoomLibertyRate item = new KxWebStudentRoomLibertyRate(this, node);
                        items.Add(item);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "LoadFromXml", ex.Message, xpath);
                LastError = ex.Message;
                return false;
            }
        }

        public KxWebStudentRoomLibertyRate GetRoomByUniqueId(string id)
        {
            KxWebStudentRoomLibertyRate result = null;

            foreach (KxWebStudentRoomLibertyRate lrr in Items)
            {
                if (lrr.UniqueId == id)
                {
                    result = lrr;
                    break;
                }
            }

            return result;
        }

        protected string[] GetRoomList()
        {
            List<string> result = new List<string>();

            foreach (KxWebStudentRoomLibertyRate lrr in Items)
            {
                result.Add(lrr.UniqueId);
            }

            return result.ToArray();
        }
    }
}

