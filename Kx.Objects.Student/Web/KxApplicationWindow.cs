using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;

namespace Kx.Objects.Student
{
    [Serializable]
    public class KxApplicationWindow : KxBaseClass
    {
        #region Attributes & Properties

        protected int _academicYearId;
        protected int _applicationTypeId;
        protected DateTime? _applicationsOpen;
        protected DateTime? _applicationsClose;
        protected string _closedMessage;

        public int ApplicationWindowId
        {
            get { return base.dBIdentifier; }
        }

        public int AcademicYearId
        {
            get { return _academicYearId; }
            set
            { 
                AddToAudit("AcademicYearId", _academicYearId, value);
                _academicYearId = value;
            }
        }

        public int ApplicationTypeId
        {
            get { return _applicationTypeId; }
            set
            { 
                AddToAudit("ApplicationTypeId", _applicationTypeId, value);
                _applicationTypeId = value;
            }
        }

        public DateTime? ApplicationsOpen
        {
            get { return _applicationsOpen; }
            set
            { 
                AddToAudit("ApplicationsOpen", _applicationsOpen, value);
                _applicationsOpen = value;
            }
        }

        public DateTime? ApplicationsClose
        {
            get { return _applicationsClose; }
            set
            { 
                AddToAudit("ApplicationsClose", _applicationsClose, value);
                _applicationsClose = value;
            }
        }

        public string ClosedMessage
        {
            get { return _closedMessage; }
            set
            { 
                AddToAudit("ClosedMessage", _closedMessage, value);
                _closedMessage = value;
            }
        }


        #endregion 


        public KxApplicationWindow(KxBaseClass parent):base(parent)
        { }


        public KxApplicationWindow(KxBaseClass parent, XmlNode node):base(parent)
        {
             LoadFromXml("", node);
        }

        public KxApplicationWindow(KxBaseClass parent,int  applicationwindowid):base(parent)
        {
             base.dBIdentifier = applicationwindowid;
             Load();
        }

        public KxApplicationWindow(KxBaseClass parent, int applicationTypeId, int academicYearId) : this(parent)
        {
            base.dBIdentifier = -1;
            this.ApplicationTypeId = applicationTypeId;
            this.AcademicYearId = academicYearId;

            Load();
        }


        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("applicationwindow", "kcf.ApplicationWindow", TargetType.Table);

            if (ApplicationWindowId >= 0)
            {
                result.AutoKeyField = new KxDataValue("ApplicationWindowId", ApplicationWindowId, KxValueType.Int);
            }


            if ((actiontype == ActionType.Update) && (ApplicationWindowId == 0))
            {
                result.Action = ActionType.Insert;
            }
            else
            {
                result.Action = actiontype;
            }

            if (actiontype == ActionType.Select && base.dBIdentifier < 0)
            {
                result.AddKeyField("applicationtypeid", this._applicationTypeId, KxValueType.Int);
                result.AddKeyField("academicyearid", this._academicYearId, KxValueType.Int);
            }

            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {
                result.AddParameter("academicYearId", _academicYearId, KxValueType.Int, false);
                result.AddParameter("applicationTypeId", _applicationTypeId, KxValueType.Int, false);
                result.AddParameter("applicationsOpen", _applicationsOpen, KxValueType.DateTime, false);
                result.AddParameter("applicationsClose", _applicationsClose, KxValueType.DateTime, false);
                result.AddParameter("closedMessage", _closedMessage, KxValueType.String, 2147483647, true);
            }
            return result;
        }


        public override  void LoadFromXml(string xml,XmlNode node)
        {
            dBIdentifier  = KxXmlUtils.GetNodeValue(node,"applicationwindowid",0);
            _academicYearId  = KxXmlUtils.GetNodeValue(node,"academicyearid",0);
            _applicationTypeId  = KxXmlUtils.GetNodeValue(node,"applicationtypeid",0);
            _applicationsOpen  = KxXmlUtils.GetNodeValueDT(node,"applicationsopen",null);
            _applicationsClose  = KxXmlUtils.GetNodeValueDT(node,"applicationsclose",null);
            _closedMessage  = KxXmlUtils.GetNodeValue(node,"closedmessage",string.Empty);
        }

        public static IEnumerable<KxApplicationWindow> GetApplicationWindow ()
        {
            KxData data = new KxData("applicationwindow", "kcf.ApplicationWindow", TargetType.Table);
            if (data.Execute ())
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(data.XmlResults);
                XmlNodeList nodes = doc.SelectNodes(@"//applicationwindow/datarow");
                foreach(XmlNode node in nodes)
                {
                     yield return new KxApplicationWindow(null, node);
                }
            }
        }
    }
}

