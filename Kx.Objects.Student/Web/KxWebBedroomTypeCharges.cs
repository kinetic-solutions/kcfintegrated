using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Exceptions;
using Kx.Core.Utility;

namespace Kx.Objects.Student.Web
{
    public enum KxRoomSelectionType
    {
        ProbableAvailability,
        StrictAvailability,
        All
    } ;

    public class KxWebBedroomTypeCharges : KxWebBedroomTypeCharge
    {
        private List<KxWebBedroomTypeCharge> items = new List<KxWebBedroomTypeCharge>();

  
        public List<KxWebBedroomTypeCharge> Items
        {
            get
            { return items; }
        }

        public KxWebBedroomTypeCharge this[int index]
        {
            get
            { return items[index]; }
            set
            { items[index] = value; }
        }
    
        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = null;

            if (actiontype == ActionType.Select)
            {
                result = new KxData("studentcourses", "viewstudentroomtypecharges", TargetType.Table);

                result.AddOrderField("AreaId", KxDataOrderType.Ascending);
                result.AddOrderField("BlockId", KxDataOrderType.Ascending);

                result.AddKeyField("academicYearId", academicYearId, KxValueType.Int);

                if (BedroomTypeId != 0)
                {
                    result.AddKeyField("BedroomTypeId", BedroomTypeId, KxValueType.Int);
                }
                if (SubBlockId != 0)
                {
                    result.AddKeyField("SubBlockId", SubBlockId, KxValueType.Int);
                }
                if (BlockId != 0)
                {
                    result.AddKeyField("BlockId", BlockId, KxValueType.Int);
                }
                if (AreaId != 0)
                {
                    result.AddKeyField("AreaId", AreaId, KxValueType.Int);
                }
                if (SiteId != 0)
                {
                    result.AddKeyField("SiteId", SiteId, KxValueType.Int);
                }
                if (Smoking != 2)
                {
                    result.AddKeyField("Smoking", Smoking, KxValueType.Int);
                }
                if (Disabled != 2)
                {
                    result.AddKeyField("Disabled", Disabled, KxValueType.Int);
                }
                if (Gender != 0)
                {
                    result.AddKeyField("Gender", Gender, KxValueType.Int);
                }
                if (SingleSex != 2)
                {
                    result.AddKeyField("SingleSex", SingleSex, KxValueType.Int);
                }
                if (FloorNumber != 0)
                {
                    result.AddKeyField("FloorNumber", FloorNumber, KxValueType.Int);
                }

                if (CustomField1 != 2)
                {
                    result.AddKeyField("CustomField1", CustomField1, KxValueType.Int);
                }
                
                if (CustomField2 != 2)
                {
                    result.AddKeyField("CustomField2", CustomField2, KxValueType.Int);
                }

                if (!String.IsNullOrEmpty(CustomValue1))
                {
                    result.AddKeyField("CustomValue1", CustomValue1, KxValueType.String);
                }

                if (!String.IsNullOrEmpty(CustomValue2))
                {
                    result.AddKeyField("CustomValue2", CustomValue2, KxValueType.String);
                }

                if (!String.IsNullOrEmpty(CustomValue3))
                {
                    result.AddKeyField("CustomValue3", CustomValue3, KxValueType.String);
                }

                if (!String.IsNullOrEmpty(CustomValue4))
                {
                    result.AddKeyField("CustomValue4", CustomValue4, KxValueType.String);
                }
                if (!String.IsNullOrEmpty(CustomValue5))
                {
                    result.AddKeyField("CustomValue5", CustomValue5, KxValueType.String);
                }
                if (!String.IsNullOrEmpty(CustomValue6))
                {
                    result.AddKeyField("CustomValue6", CustomValue6, KxValueType.String);
                }
                if (!String.IsNullOrEmpty(CustomValue7))
                {
                    result.AddKeyField("CustomValue7", CustomValue7, KxValueType.String);
                }
                if (!String.IsNullOrEmpty(CustomValue8))
                {
                    result.AddKeyField("CustomValue8", CustomValue8, KxValueType.String);
                }
                if (ContractLength != 0)
                {
                    result.AddKeyField("ContractLength", ContractLength, KxValueType.Int);
                }
            }
            return result;
        }

        public KxWebBedroomTypeCharges(KxBaseClass parent,int academicYear) : base(parent)
        {
            this.academicYearId = academicYear;
           
        }

        public override bool LoadFromXml(string xml, string xpath)
        {
            items.Clear();
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xml);

                XmlNodeList nodes = doc.SelectNodes(@"//results/" + xpath + "/datarow");

                foreach (XmlNode node in nodes)
                {
                    if (node != null)
                    {
                        KxWebBedroomTypeCharge bc = new KxWebBedroomTypeCharge(this, node);
                        Items.Add(bc);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "LoadFromXml", ex.Message, xpath);
                LastError = ex.Message;
                return false;
            }
        }

        public KxWebBedroomTypeCharge GetBedroomTypeChargeByIndex(int idx)
        {
            KxWebBedroomTypeCharge result = null;

            try
            {
                result = Items[idx];
            }
            catch{}
            
            return result;
        }

        public string[] GetMatchingBedrooms(int academicyear, List<int> pending)
        {
            AcademicYearId = academicyear;
            Load();

            List<KxWebBedroomTypeCharge> result = new List<KxWebBedroomTypeCharge>();
            string strresult = "";

            foreach (KxWebBedroomTypeCharge charge in Items)
            {
                if (charge.DistinctFields != DistinctFields)
                {
                    charge.DistinctFields = DistinctFields;
                }

                if (IsUnique(result, charge.DistinctFieldValues))
                {
                    result.Add(charge);
                    strresult += Items.IndexOf(charge).ToString() + ",";
                }
            }
            return strresult.Split(new string[]{","},StringSplitOptions.RemoveEmptyEntries);
        }

        public bool IsUnique(List<KxWebBedroomTypeCharge> list,string unique)
        {
            bool result = true;

            foreach (KxWebBedroomTypeCharge charge in list)
            {
                if (unique != null)
                {
                    if (charge.DistinctFieldValues.ToUpper() == unique.ToUpper())
                    {
                        result = false;
                        break;
                    }
                }
            }
            return result;
        }
    }
}
