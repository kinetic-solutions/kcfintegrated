﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Kx.Objects.Student.Web
{
    public interface ICustomEnsuiteCountLimitValidator
    {
        bool CustomEnsuiteCountLimit(KxWebStudentPreferences preferences);
    }
}
