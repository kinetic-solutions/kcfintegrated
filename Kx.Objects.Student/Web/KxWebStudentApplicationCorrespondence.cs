using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;

namespace Kx.Objects.Student.Web
{
    [Serializable]
    public class KxWebStudentApplicationCorrespondence : KxBaseClass
    {
        #region Attributes & Properties

        protected int      documentIndex;
        protected string   correspondence;

        public Guid     ApplicationGuId
        {
            get
            { return base.guidIdentifier;}
            set
            {
                guidIdentifier = value;
            }
        }

        public int DocumentIndex
        {
            get
            { return documentIndex; }
            set
            {
                AddToAudit("documentIndex", documentIndex, value);
                documentIndex = value;
            }
        }

        public string   Correspondence
        {
            get
            { return correspondence;}
            set
            { 
                AddToAudit("Correspondence", correspondence, value);
                correspondence = value;
            }
        }


        #endregion 


        public KxWebStudentApplicationCorrespondence(KxBaseClass parent):base(parent)
        {}


        public KxWebStudentApplicationCorrespondence(KxBaseClass parent, XmlNode node):base(parent)
        {
             LoadFromXml("", node);
        }

        public KxWebStudentApplicationCorrespondence(KxBaseClass parent,Guid applicationguid):base(parent)
        {
             base.guidIdentifier = applicationguid;
             Load();
        }


        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("webstudentapplicationcorrespondence", "WebStudentApplicationCorrespondence", TargetType.Table);


            if ((actiontype == ActionType.Update) && (guidIdentifier == Guid.Empty))
            {
                if (changed)
                {
                    guidIdentifier = ((KxWebStudentApplicationCorrespondenceList)parent).GuidIdentifier;
                    if (guidIdentifier == Guid.Empty)
                    {
                        guidIdentifier = Parent.Parent.GuidIdentifier;
                    }
                }
                result.Action = ActionType.Insert;
            }
            else
            {
                result.Action = actiontype;
            }

            result.AddKeyField("applicationGuId",guidIdentifier,KxValueType.Guid);
            result.AddKeyField("documentindex", documentIndex, KxValueType.Int);


            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {
                
                result.AddParameter("correspondence", correspondence, KxValueType.String, 4000, true);
            }
            return result;
        }


        public override  void LoadFromXml(string xml,XmlNode node)
        {
            guidIdentifier  = KxXmlUtils.GetNodeValue(node,"applicationguid",Guid.Empty);
            documentIndex = KxXmlUtils.GetNodeValue(node, "documentindex", 0);
            correspondence  = KxXmlUtils.GetNodeValue(node,"correspondence","");
        }
    }
}

