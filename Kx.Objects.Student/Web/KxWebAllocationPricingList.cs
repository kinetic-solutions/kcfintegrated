using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Exceptions;
using Kx.Core.Utility;

namespace Kx.Objects.Student.Web
{
    public class KxWebAllocationPricingList : KxBaseClass
    {
        private int academicYearId = 0;
        private int areaId = 0;

        private List<KxWebAllocationPricing> items = new List<KxWebAllocationPricing>();

        public int AcademicYearId
        {
            get
            {
                return academicYearId;
            }
            set
            {
                if (value != null && value != academicYearId)
                {
                    academicYearId = value;
                    Load();
                }
            }
        }

        public int AreaId
        {
            get
            {
                return areaId;
            }
            set
            {
                if (value != null && value != areaId)
                {
                    areaId = value;
                    Load();
                }
            }
        }

        public List<KxWebAllocationPricing> Items
        {
            get
            { return items; }
        }

        public string[] AllocationPricingIds
        {
            get
            {
                return GetAllocationPricingIds();
            }
        }

        public KxWebAllocationPricing this[int index]
        {
            get
            { return items[index]; }
            set
            { items[index] = value; }
        }


        public KxWebAllocationPricingList(KxBaseClass parent, int academicYearId)
            : base(parent)
        {
             this.academicYearId = academicYearId;
        }

        public KxWebAllocationPricingList(KxBaseClass parent)
            : base(parent)
        {
        }



        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = null;

            if (actiontype == ActionType.Select)
            {
                result = new KxData("weballocationpricing", "WebAllocationPricing", TargetType.Table, ActionType.Select);

                result.KeyFields.Add(new KxDataValue("academicyearid", academicYearId, KxValueType.Int));

                if (areaId > 0)
                {
                    result.KeyFields.Add(new KxDataValue("areaid", areaId, KxValueType.Int));
                }
            }
            return result;
        }

        public override bool LoadFromXml(string xml, string xpath)
        {
            items.Clear();
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xml);

                XmlNodeList nodes = doc.SelectNodes(@"//results/" + xpath + "/datarow");

                foreach (XmlNode node in nodes)
                {
                    if (node != null)
                    {
                        KxWebAllocationPricing item = new KxWebAllocationPricing(this, node);
                        items.Add(item);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "LoadFromXml", ex.Message, xpath);
                LastError = ex.Message;
                return false;
            }
        }

        public string[] GetAllocationPricingIds()
        {
            List<string> list = new List<string>();

            foreach (KxWebAllocationPricing i in Items)
            {
                list.Add(i.UniqueId);
            }
            return list.ToArray();
        }


        public KxWebAllocationPricing GetWebAllocationPricing(string id)
        {
            KxWebAllocationPricing result = null;

            foreach (KxWebAllocationPricing i in Items)
            {
                if (i.UniqueId== id)
                {
                    result = i;
                    break;
                }
            }
            return result;
        }

        public KxWebAllocationPricing AddAllocationPricing()
        {
            KxWebAllocationPricing result = new KxWebAllocationPricing(this);

            items.Add(result);

            return result;
        }

    }
}

