﻿using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;
using Kx.Objects.Common;

namespace Kx.Objects.Student.Web
{
    [Serializable]
    public class KxBookingEligibilityRule : KxBaseClass
    {
        #region Properties & Attributes

        protected bool loaded = false;

        protected int academicYearId;
        protected int facultyId;
        protected int studentYearId;
        protected string isEligibleToBook;
        protected DateTime ?lastUpdatedDate;
        protected string lastUpdatedByUser;

        protected KxAcademicYear academicYear = null;
        protected KxFaculty faculty = null;
        protected KxStudentYear studentYear = null;
        protected KxUser loggedInUser = null;
        


        public int AcademicYearId
        {
            get
            { return academicYearId; }
            set
            {
                AddToAudit("AcademicYearId", academicYearId, value);
                academicYearId = value;
                academicYear = null;
            }
        }

        public int FacultyId
        {
            get
            { return facultyId; }
            set
            {
                AddToAudit("FacultyId", facultyId, value);
                facultyId = value;
                faculty = null;
            }
        }

        public int StudentYearId
        {
            get
            { return studentYearId; }
            set
            {
                AddToAudit("StudentYearId", studentYearId, value);
                studentYearId = value;
                studentYear = null;
            }
        }

        public string IsEligibleToBook
        {
            get
            {
                if (isEligibleToBook == null)
                {
                    isEligibleToBook = "false";
                }
                return isEligibleToBook.ToLower();
            }
            set
            {
                AddToAudit("IsEligibleToBook", isEligibleToBook, value);
                isEligibleToBook = value;
            }
        }

        public DateTime ?LastUpdatedDate
        {
            get
            { return lastUpdatedDate; }
            set
            {
                AddToAudit("LastUpdatedDate", lastUpdatedDate, value);
                lastUpdatedDate = value;
            }
        }

        public string LastUpdatedDateString
        {
            get
            {
                try
                {
                    return LastUpdatedDate.Value.ToString("dd/MM/yy H:mm:ss");
                }
                catch
                {
                    return "18991230";
                }
            }
        }

        public string LastUpdatedByUser
        {
            get
            { return lastUpdatedByUser; }
            set
            {
                AddToAudit("LastUpdatedByUser", lastUpdatedByUser, value);
                lastUpdatedByUser = value;
            }
        }

        public KxAcademicYear AcademicYear
        {
            get
            {
                if (academicYear == null && academicYearId > 0)
                {
                    academicYear = new KxAcademicYear(this, academicYearId);
                }
                return academicYear;
            }
        }

        public string AcademicYearName
        {
            get
            {
                if (AcademicYear != null)
                {
                    return AcademicYear.Title;
                }
                else
                {
                    return "";
                }
            }
        }

        public KxFaculty Faculty
        {
            get
            {
                if (faculty == null && facultyId > 0)
                {
                    faculty = new KxFaculty(this, facultyId);
                }
                return faculty;
            }
        }

        public string FacultyName
        {
            get
            {
                if (Faculty != null)
                {
                    return Faculty.Description;
                }
                else
                {
                    return "";
                }
            }
        }

        public KxStudentYear StudentYear
        {
            get
            {
                if (studentYear == null && studentYearId > 0)
                {
                    studentYear = new KxStudentYear(this, studentYearId);
                }
                return studentYear;
            }
        }

        public string StudentYearName
        {
            get
            {
                if (StudentYear != null)
                {
                    return StudentYear.Description;
                }
                else
                {
                    return "";
                }
            }
        }

        public string UniqueId
        {
            get
            {
                return String.Format("{0}|{1}|{2}", AcademicYearId, FacultyId, StudentYearId);
            }
        }

        #endregion

        #region Contructors
        public KxBookingEligibilityRule(KxBaseClass parent) : base(parent)
        {
        }

        public KxBookingEligibilityRule(KxBaseClass parent, XmlNode node)
            : base(parent)
        {
             LoadFromXml("", node);
        }
        #endregion

        public override KxData ConfigureData(ActionType actiontype)
        {
            lastUpdatedDate = DateTime.Now;
            
            // Convert string used for UI back to int.
            int intIsEligibleToBook = 0;

            if (isEligibleToBook.Equals("true"))
            {
                intIsEligibleToBook = 1;
            }

            KxData result = new KxData("bookingeligibilityrules", "BookingEligibilityRules", TargetType.Table);

            result.AddKeyField("academicyearid", academicYearId, KxValueType.Int);
            result.AddKeyField("facultyId", facultyId, KxValueType.Int);
            result.AddKeyField("studentYearId", studentYearId, KxValueType.Int);

            if ((actiontype == ActionType.Update) && (!loaded))
            {
                result.Action = ActionType.Insert;

                result.AddParameter("isEligibleToBook", intIsEligibleToBook, KxValueType.Int);
                result.AddParameter("lastUpdatedDate", lastUpdatedDate , KxValueType.DateTime);
                result.AddParameter("lastUpdatedByUser", lastUpdatedByUser, KxValueType.String);
            }
            else
            {
                result.Action = actiontype;
            }


            if ((actiontype == ActionType.Update) && (loaded))
            {
                result = null;

                result = new KxData("", "KxBookingEligiblityRuleUpdate", TargetType.StoredProc, ActionType.ExecuteProcedure);

                result.AddKeyField("academicyearid", academicYearId, KxValueType.Int);
                result.AddKeyField("facultyId", facultyId, KxValueType.Int);
                result.AddKeyField("studentYearId", studentYearId, KxValueType.String);
                result.AddParameter("isEligibleToBook", intIsEligibleToBook, KxValueType.Int);
                result.AddParameter("lastUpdatedDate", lastUpdatedDate, KxValueType.DateTime);
                result.AddParameter("lastUpdatedByUser", lastUpdatedByUser, KxValueType.String);

                return result;
            }

            return result;
        }

        public override void LoadFromXml(string xml, XmlNode node)
        {
            int temp = 0;

            academicYearId = KxXmlUtils.GetNodeValue(node, "academicyearid", 0);
            facultyId = KxXmlUtils.GetNodeValue(node, "facultyId", 0);
            studentYearId = KxXmlUtils.GetNodeValue(node, "studentYearId", 0);
            lastUpdatedDate = KxXmlUtils.GetNodeValueDT(node, "lastUpdatedDate",null);
            lastUpdatedByUser = KxXmlUtils.GetNodeValue(node, "lastUpdatedByUser", "");
            temp = KxXmlUtils.GetNodeValueBoolInt(node, "isEligibleToBook", 0);

            if (temp == 1)
            {
                isEligibleToBook = "true";
            }
            else
            {
                isEligibleToBook = "false";
            }

            loaded = true;
        }

       
    }
}
