﻿using System;
using System.Collections.Generic;
using System.Text;
using Kx.Core.Utility;
using Kx.Objects.Student;
using System.Collections;

namespace Kx.Objects.Student.Web
{
    public class KxWebAllocationCharge
    {
        protected DateTime chargeDate;
        protected DateTime endDate;
        protected Decimal amount = 0.0M;

        public DateTime ChargeDate
        {
            get
            {
                return chargeDate;
            }
            set
            {
                chargeDate = value;
            }
        }
        public DateTime EndDate
        {
            get
            {
                return endDate;
            }
            set
            {
                endDate = value;
            }
        }
        public Decimal Amount
        {
            get
            {
                return amount;
            }
            set
            {
                amount = value;
            }
        }
    }

    public enum AllocationChargeType { All, Accommodation, Ancillary };

    public class KxWebAllocationCharges
    {
        AllocationChargeType chargeType = AllocationChargeType.All;
        //bool AccommodationOnly = false;
        protected KxStudentAllocation allocation;
        protected SortedList<String, KxWebAllocationCharge> items = null;

        public KxStudentAllocation Allocation
        {
            get
            {
                return allocation;
            }
            set
            {
                SetAllocation(value);
            }
        }


        public SortedList<String, KxWebAllocationCharge> Items
        {
            get
            {
                if (items == null)
                {
                    items = new SortedList<string, KxWebAllocationCharge>();
                }
                return items;
            }
            set
            {
                items = value;
            }
        }

        public string ChargeString
        {
            get
            {
                return GetChargeString();
            }
        }

        public string ChargeStringHtml
        {
            get
            {
                return GetChargeStringHtml();
            }
        }

        public string[] ChargeDates
        {
            get
            {
                return GetChargeDates();
            }
        }

        public AllocationChargeType ChargeType
        {
            get
            {
                return chargeType;
            }
            set
            {
                chargeType = value;
            }
        }

        public KxWebAllocationCharges(KxStudentAllocation _allocation)
        {
            Allocation = _allocation;
        }

        public KxWebAllocationCharges(KxStudentAllocation _allocation, AllocationChargeType chargetype)
        {
            ChargeType = chargetype;
            Allocation = _allocation;
        }

        private string GetChargeString()
        {
            StringBuilder sb = new StringBuilder();

            foreach (KxWebAllocationCharge chg in Items.Values)
            {
                sb.AppendLine(KxDateTime.ToString(chg.ChargeDate,"D") + "\t\t" + chg.Amount.ToString("C"));
            }

            return sb.ToString();
        }

        private string GetChargeStringHtml()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("<table>");
            foreach (KxWebAllocationCharge chg in Items.Values)
            {
                sb.AppendLine(String.Format("<tr><td>{0}</td><td align=\"right\">{1}</td></tr>",KxDateTime.ToString(chg.ChargeDate, "D") ,chg.Amount.ToString("C")));
            }
            sb.AppendLine("</table>");

            return sb.ToString();
        }

        private void SetAllocation(KxStudentAllocation value)
        {
            allocation = value;
            Items.Clear();

            foreach (KxStudentAllocationDate allocdate in allocation.AllocationDates.Items)
            {
                foreach (KxStudentCharge charge in allocdate.Charges.Items)
                {
                    if (ChargeType == AllocationChargeType.All ||
                       (ChargeType == AllocationChargeType.Accommodation && charge.ChargeType.IsAccommodation) ||
                       (ChargeType == AllocationChargeType.Ancillary && !charge.ChargeType.IsAccommodation))
                    {
                        string dtestring = charge.DateFrom.Value.ToString("yyyyMMdd");

                        if (Items.ContainsKey(dtestring))
                        {
                            KxWebAllocationCharge webcharge = Items[dtestring];
                            webcharge.Amount += charge.Amount;
                        }
                        else
                        {
                            KxWebAllocationCharge webcharge = new KxWebAllocationCharge();
                            webcharge.Amount = charge.Amount;
                            webcharge.ChargeDate = charge.DateFrom.Value;
                            webcharge.EndDate = charge.DateTo.Value;
                            Items.Add(dtestring, webcharge);
                        }
                    }
                }
            }
        }

        public string[] GetChargeDates()
        {
            List<string> result = new List<string>();

            foreach (string s in Items.Keys)
            {
                result.Add(s);
            }

            return result.ToArray();
        }

        public KxWebAllocationCharge GetChargeById(string id)
        {
            if (Items.ContainsKey(id))
            {
                return Items[id];
            }
            else
            {
                return null;
            }
        }
    }
}
