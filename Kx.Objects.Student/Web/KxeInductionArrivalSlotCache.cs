﻿using System;
using System.Collections.Generic;
using System.Text;
using Kx.Core.Data;

namespace Kx.Objects.Student.Web
{
    public class KxeArrivalSlotEntry
    {
        protected int arrivalSlotId;
        protected int currentCount;
        protected int maxArrivals;
        protected DateTime lastRefresh = DateTime.Now.AddMinutes(-5);

        public int ArrivalSlotId
        {
            get
            {
                return arrivalSlotId;
            }
            set
            {
                arrivalSlotId = value;
            }
        }
  
        public int CurrentCount
        {
            get
            {
                if (currentCount >= (maxArrivals-5) || lastRefresh.AddMinutes(3) < DateTime.Now)
                {
                    Refresh();
                }
                return currentCount;
            }
            set
            {
                currentCount = value;
            }
        }

        public DateTime LastRefresh
        {
            get
            {
                return lastRefresh;
            }
            set
            {
                lastRefresh = value;
            }
        }

        public int MaxArrivals
        {
            get
            {
                return maxArrivals;
            }
            set
            {
                maxArrivals = value;
            }
        }

        public bool HasAvailability
        {
            get
            {
                return CurrentCount < maxArrivals;
            }
        }

        public void Refresh()
        {
            if (ArrivalSlotId > 0)
            {
                try
                {
                    KxData data = new KxData("count", "st2einductionarrivals", TargetType.Table);

                    data.AddKeyField("arrivalslotid", ArrivalSlotId, KxValueType.Int);

                    currentCount = data.ExecuteScalarCount();

                    LastRefresh = DateTime.Now;
                }
                catch { }
            }
        }
    }

    public class KxeInductionArrivalSlotCache
    {
        SortedList<int, KxeArrivalSlotEntry> items = null;

        public SortedList<int, KxeArrivalSlotEntry> Items
        {
            get
            {
                if (items == null)
                {
                    items = new SortedList<int, KxeArrivalSlotEntry>();
                }
                return items;
            }
            set
            {
                items = value;
            }
        }

        public KxeInductionArrivalSlotCache()
        {

        }

        protected KxeArrivalSlotEntry GetSlotById(int id)
        {
            KxeArrivalSlotEntry result = null;

            if (Items.Keys.Contains(id))
            {
                result = Items[id];
            }

            return result;
        }

        public bool GetSlotAVailability(int ArrivalSlotId, int MaxSlots)
        {
            bool result = false;

            KxeArrivalSlotEntry entry = GetSlotById(ArrivalSlotId); 

            if (entry == null)
            {
                entry = new KxeArrivalSlotEntry();
                entry.ArrivalSlotId = ArrivalSlotId;
                entry.MaxArrivals = MaxSlots;
                Items.Add(ArrivalSlotId, entry);
            }

            result = entry.HasAvailability;
            
            return result;
        }

    


    }
}
