﻿using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Configuration;
using Kx.Core.Utility;
using Kx.Core.Exceptions;
using Kx.Core.Web.Security;

namespace Kx.Objects.Student.Web
{
    public enum SelectType { None = 0, UserGuid = 1 };
    [Serializable]
    public class KxWebUserShortUrlPasswordReset : KxBaseClass
    {
        #region Attributes & Properties
        protected bool loaded = false;
        protected string shortCode;
        protected Guid userGuid;
        protected string shortUrl;
        protected string longUrl;
        protected DateTime? expiryDate;
        protected bool codeUsed = false;
        protected SelectType selectType;

        public string ShortCode
        {
            get
            { return shortCode; }
            set
            {
                AddToAudit("ShortCode", shortCode, value);
                shortCode = value;
            }
        }

        public Guid UserGuid
        {
            get
            { return userGuid; }
            set
            {
                AddToAudit("UserGuId", userGuid, value);
                userGuid = value;
            }
        }

        public string ShortUrl
        {
            get
            { return shortUrl; }
            set
            {
                AddToAudit("ShortUrl", shortUrl, value);
                shortUrl = value;
            }
        }

        public string LongUrl
        {
            get
            { return longUrl; }
            set
            {
                AddToAudit("LongUrl", longUrl, value);
                longUrl = value;
            }
        }

        public DateTime? ExpiryDate
        {
            get
            { return expiryDate; }
            set
            {
                AddToAudit("ExpiryDate", expiryDate, value);
                expiryDate = value;
            }
        }

        public bool IsCodeExpired
        {
            get
            {
                return ExpiryDate != null && ExpiryDate > DateTime.Now;
            }
        }

        public bool CodeUsed
        {
            get { return codeUsed; }
            set { codeUsed = value; }
        }


        #endregion 


        public KxWebUserShortUrlPasswordReset(KxBaseClass parent, Guid userGuid, SelectType selectType = SelectType.None) : base(parent)
        {
            if (selectType == SelectType.UserGuid)
            {
                this.UserGuid = userGuid;
                this.selectType = selectType;
            }
            else
            {
                InitialisePasswordResetShortCode(userGuid);
            }
            Load();
        }

        public KxWebUserShortUrlPasswordReset(KxBaseClass parent, string shortCode) : base(parent)
        {
            this.ShortCode = shortCode;
            Load();
        }


        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("webusershorturlpasswordreset", "WebUserShortUrlPasswordReset", TargetType.Table);


            if (selectType == SelectType.UserGuid)
            {
                result.AddKeyField("UserGuid", UserGuid, KxValueType.String);
            }
            else
            {
                result.AddKeyField("ShortCode", shortCode, KxValueType.String);
            }


            if ((actiontype == ActionType.Update) && (!loaded))
            {
                result.Action = ActionType.Insert;
                loaded = true;
            }
            else
            {
                result.Action = actiontype;
            }

            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {

                KxWebUserShortUrlPasswordReset shortUrlPasswordReset = new KxWebUserShortUrlPasswordReset(null, userGuid, SelectType.UserGuid);

                if (!String.IsNullOrEmpty(shortUrlPasswordReset.LongUrl))
                {
                    result.Action = ActionType.Update;
                    result.AddKeyField("UserGuid", UserGuid, KxValueType.String);
                    result.KeyFields.RemoveAt(0);
                    result.AddParameter("ShortCode", shortCode, KxValueType.String, 255, true);
                }
                else
                {
                    result.AddParameter("userGuid", userGuid, KxValueType.Guid, true);
                }
                result.AddParameter("shortUrl", shortUrl, KxValueType.String, 255, true);
                result.AddParameter("longUrl", longUrl, KxValueType.String, 255, true);
                result.AddParameter("expiryDate", expiryDate, KxValueType.DateTime, true);
                result.AddParameter("codeUsed", codeUsed, KxValueType.Boolean, true);


            }
            return result;
        }


        public override void LoadFromXml(string xml, XmlNode node)
        {
            shortCode = KxXmlUtils.GetNodeValue(node, "shortcode", "");
            userGuid = KxXmlUtils.GetNodeValue(node, "userguid", Guid.Empty);
            shortUrl = KxXmlUtils.GetNodeValue(node, "shorturl", "");
            longUrl = KxXmlUtils.GetNodeValue(node, "longurl", "");
            expiryDate = KxXmlUtils.GetNodeValueDT(node, "expirydate", null);

            int isCodeUsed = KxXmlUtils.GetNodeValueBoolInt(node, "codeUsed", 0);
            codeUsed = isCodeUsed == 0 ? false : true;

            loaded = true;
        }

        protected string GenerateShortCode()
        {

            var userGuidWithTimestamp = UserGuid.ToString() + DateTime.Now.Ticks.ToString();

            var hash = KxWebSecurity.Hash(KxWebSecurity.GenerateSalt(8) + userGuidWithTimestamp);

            return hash;
        }


        public void InitialisePasswordResetShortCode(Guid userguid)
        {
            UserGuid = userguid;
            shortCode = GenerateShortCode();
            LongUrl = KxDbConfiguration.GetConfiguration("HomePageUrl") + "?wf=" + KxDbConfiguration.GetConfiguration("PasswordResetWorkflow", "PasswordReset") + "&c=" + shortCode;
            ShortUrl = KxDbConfiguration.GetConfiguration("HomePageUrl").ToLower().Replace("default.aspx", "c.aspx?prc=" + shortCode);
            ExpiryDate = DateTime.Now.AddDays(KxDbConfiguration.GetConfigurationInt("PasswordResetCodeExpiryMinutes", 60));
            Changed = true;
            Save();

        }
    }
}

