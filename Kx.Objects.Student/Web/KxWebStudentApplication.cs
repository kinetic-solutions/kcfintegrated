using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.Caching;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;
using Kx.Objects.Common.Web;
using Kx.Objects.Common;
using System.Runtime.Serialization;
using Kx.Core.Localisation;
using Kx.Core.Configuration;
using System.Runtime.Caching;

namespace Kx.Objects.Student.Web
{
    [Serializable]
    public class KxWebStudentApplication : KxBaseClass
    {
        #region Attributes & Properties

        protected int controlPreferenceIndex;
        protected int userPreferenceIndex;
        protected int controlPreferenceNumber;
        protected int userPreferenceNumber;
        protected Guid processGuid = Guid.Empty;
        protected int      kxStudentId;
        protected int      applicationId;
        protected int      applicationTypeId;
        protected int      courseTypeId;
        protected int academicYearId;
        protected int      studentYearId;
        protected int      licenceTypeId;
        protected Guid     userGuid = Guid.Empty;
        protected string   studentIdentifier;
        protected string   title;
        protected string   foreName;
        protected string   midName;
        protected string   surName;
        protected string   emailAddress;
        protected DateTime ?dob;
        protected int      nationalityId;
        protected int      ethnicityId;
        protected DateTime ?creationDate;
        protected DateTime ?confirmedDate;
        protected string   gender;
        protected int controlUDF = 0;
        protected int userUDF = 0;
        protected int paymentScheduleTypeId = 0;
        protected string paymentmethod = "";

        protected int _sessionId = 0;
        protected DateTime? _sessionStartDate;
        protected DateTime? _sessionEndDate;

        protected bool updated = false;// Used to keep app & user / student in synch...

        protected KxApplicationType applicationType = null;
        protected KxWebStudentAddresses addresses = null;
        protected KxWebStudentApplicationCorrespondenceList correspondence = null;
        protected KxWebStudentPreferences preferences = null;
        protected KxWebFriendshipGroup friendshipGroup = null;
        protected KxWebFriendshipGroupMember groupMember = null;
        protected KxWebStudentPayments payments = null;
        protected KxStudentApplication standardApplication = null;
        protected KxUserDefinedValues userDefinedValues = null;
        protected KxWebStudentAddress currentStudentAddress = null;
        protected KxWebStudentAddress emergencyStudentAddress = null;
        protected KxWebStudentAddress homeStudentAddress = null;
        protected KxWebStudentAddress preferredEmailAddress = null;
        protected KxWebStudentAddress sponsorAddress = null;
        protected KxPaymentScheduleType paymentScheduleType = null;
        protected KxNationality nationality = null;
        protected KxEthnicity ethnicity = null;
        protected KxDomicile domicileProxy = null;
        protected KxStudentYear studentYear = null;
        protected KxeInductionQuestionAttempts questionAttempts = null;
        protected KxCourse courseMidName = null;

        protected KxeInductionArrival inductionArrival = null;

        protected KxStudentCharges applicationCharges = null;

        protected KxWebStudentRoomLibertyRates libertyRoomRates = null;
        protected KxWebStudentRoomCityRates cityRoomRates = null;
        
        protected bool proceedWithApplication = true;
        protected int generatedKXId = -99;
        protected bool expired = false;
        protected KxWebStudentCorrespondance document = null;
        protected KxStudentPhoto photo = null;
        protected KxWebStudentApplicationInduction induction = null;

        private MemoryCache _cache = MemoryCache.Default;

        public int Age
        {
            get
            { return KxDateTime.CalculateAge(dob); }
        }

        public bool IsAdult
        {
            get
            {
                return Age >= 18;
            }
        }

        public bool Updated
        {
            get
            { return updated; }
            set
            { 
                updated = value;
                changed = false; // pha stop ghost record
            }
        }

        public int SelectedUDF
        {
            get
            {
                if (controlUDF != 0)
                {
                    return controlUDF;
                }
                else
                {
                    return userUDF;
                }
            }
        }
    
        public int UserUDF
        {
            get
            { return userUDF; }
            set
            {
                userUDF = value;
            }
        }
        public int ControlUDF
        {
            get
            { return controlUDF; }
            set
            { controlUDF = value; }
        }
        public Guid     ApplicationGuid
        {
            get
            { 
                return guidIdentifier; 
            }
        }
        public Guid     ProcessGuid
        {
            get
            { return processGuid;}
            set
            { 
                AddToAudit("ProcessGuid", processGuid, value);
                processGuid = value;
            }
        }
        public int      KxStudentId
        {
            get
            { return kxStudentId;}
            set
            { 
                AddToAudit("KxStudentId", kxStudentId, value);
                kxStudentId = value;
            }
        }
        public int      ApplicationId
        {
            get
            { return applicationId;}
            set
            { 
                AddToAudit("ApplicationId", applicationId, value);
                applicationId = value;
            }
        }

        public KxApplicationType ApplicationType
        {
            get
            {
                if (applicationType == null || applicationType.ApplicationTypeId != applicationTypeId)
                {
                    applicationType = new KxApplicationType(this, applicationTypeId);
                }
                return applicationType;
            }
        }

        public int      ApplicationTypeId
        {
            get
            { return applicationTypeId;}
            set
            { 
                AddToAudit("ApplicationTypeId", applicationTypeId, value);
                applicationTypeId = value;
            }
        }
        public int      CourseTypeId
        {
            get
            { return courseTypeId;}
            set
            { 
                AddToAudit("CourseTypeId", courseTypeId, value);
                courseTypeId = value;
            }
        }
        public int AcademicYearId
        {
            get
            { return academicYearId; }
            set
            {
                AddToAudit("AcademicYearId", academicYearId, value);
                academicYearId = value;
            }
        }

        public int      StudentYearId
        {
            get
            { return studentYearId;}
            set
            { 
                AddToAudit("StudentYearId", studentYearId, value);
                studentYearId = value;
            }
        }
        public int      LicenceTypeId
        {
            get
            { return licenceTypeId;}
            set
            { 
                AddToAudit("LicenceTypeId", licenceTypeId, value);
                licenceTypeId = value;
            }
        }

        public int DomicileIdProxy
        {
            get
            { return licenceTypeId; }
            set
            {
                AddToAudit("LicenceTypeId", licenceTypeId, value);
                licenceTypeId = value;
            }
        }

        public Guid     UserGuid
        {
            get
            {
                return userGuid;
            }
            set
            { 
                AddToAudit("UserGuid", userGuid, value);
                userGuid = value;
            }
        }
        public string   StudentIdentifier
        {
            get
            { return studentIdentifier;}
            set
            { 
                AddToAudit("StudentIdentifier", studentIdentifier, value);
                if (value != null)
                {
                    value = value.TrimEnd();
                }
                studentIdentifier = value;
            }
        }
        public string PaymentMethod
        {
            get
            { return paymentmethod; }
            set
            {
                paymentmethod = value;
            }
        }
        public string Title
        {
            get
            { return title;}
            set
            { 
                AddToAudit("Title", title, value);
                title = value;
            }
        }
        public string   ForeName
        {
            get
            { return foreName;}
            set
            { 
                AddToAudit("ForeName", foreName, value);
                foreName = value;
            }
        }
        public string   MidName
        {
            get
            { return midName;}
            set
            { 
                AddToAudit("MidName", midName, value);
                midName = value;
            }
        }
        public string   SurName
        {
            get
            { return surName;}
            set
            { 
                AddToAudit("SurName", surName, value);
                surName = value;
            }
        }
        public string   EmailAddress
        {
            get
            { return emailAddress;}
            set
            { 
                AddToAudit("EmailAddress", emailAddress, value);
                emailAddress = value;
            }
        }
        public DateTime ?Dob
        {
            get
            { return dob;}
            set
            { 
                AddToAudit("Dob", dob, value);
                dob = value;
            }
        }

        public int      NationalityId
        {
            get
            { 
                return nationalityId;
            }
            set
            { 
                AddToAudit("NationalityId", nationalityId, value);
                if (value != nationalityId)
                {
                    Children.Remove(nationality);
                    nationality = null;
                }
                nationalityId = value;
            }
        }
        public int      EthnicityId
        {
            get
            { return ethnicityId;}
            set
            { 
                AddToAudit("EthnicityId", ethnicityId, value);
                ethnicityId = value;
            }
        }
        public DateTime ?CreationDate
        {
            get
            { return creationDate;}
            set
            { 
                AddToAudit("CreationDate", creationDate, value);
                creationDate = value;
            }
        }

        public bool Expired
        {
            get
            {
                return expired;
            }
            set
            {
                if (expired == value)
                    return;
                expired = value;
            }
        }

        public int PaymentScheduleTypeId
        {
            get
            { return paymentScheduleTypeId; }
            set
            {
                AddToAudit("paymentScheduleTypeId", paymentScheduleTypeId, value);
                paymentScheduleTypeId = value;
                Children.Remove(paymentScheduleType);
                paymentScheduleType = null;
            }
        }

        public int SessionID
        {
            get { 
                return _sessionId; 
            }
            set
            {
                AddToAudit("SessionID", _sessionId, value);
                _sessionId = value;
            }
        }

        public DateTime? SessionStartDate
        {
            get { return _sessionStartDate; }
            set
            {
                AddToAudit("SessionStartDate", _sessionStartDate, value);
                _sessionStartDate = value;
            }
        }

        public DateTime? SessionEndDate
        {
            get { return _sessionEndDate; }
            set
            {
                AddToAudit("SessionEndDate", _sessionEndDate, value);
                _sessionEndDate = value;
            }
        }

        //Obsolete use KxWebUserStudent instead...
        public DateTime CurrentTime
        {
            get
            {
                return DateTime.Now;
            }
        }

        public DateTime ?ConfirmedDate
        {
            get
            { return confirmedDate;}
            set
            { 
                AddToAudit("ConfirmedDate", confirmedDate, value);
                confirmedDate = value;
            }
        }
        public string   Gender
        {
            get
            { return gender;}
            set
            { 
                AddToAudit("Gender", gender, value);
                gender = value;
            }
        }

        public string VerboseGender
        {
            get
            { return KxLocalisationManager.GetSubstitution(GetVerboseGender()); }
        }

        public bool IsNew
        {
            get
            {
                return !IsInProgress();
            }
        }

        public bool IsEditable
        {
            get
            {
                return GetIsEditable();
            }
        }
        public bool IsComplete
        {
            get
            {
                return GetIsComplete();
            }
        }

        public bool IsInactive
        {
            get
            {
                if (StandardApplication != null)
                {
                    return StandardApplication.Expired;
                }
                else
                {
                    return false;
                }
            }
        }

        public bool IsActive
        {
            get
            {
                return !IsInactive;
            }
        }

        public bool IsProcessed
        {
            get
            {
                return applicationId > 0;
            }
        }

        public bool IsConfirmed
        {
            get
            { return ConfirmedDate != null; }
        }
        public bool IsUnconfirmed
        {
            get
            { return ConfirmedDate == null; }
        }
        public string ApplicationStatus
        {
            get
            {
                return KxLocalisationManager.GetSubstitution(GetApplicationStatus());
            }
        }

        public KxWebStudentAddresses Addresses
        {
            get
            {
                if (addresses == null)
                {
                    addresses = new KxWebStudentAddresses(this, guidIdentifier);
                }
                return addresses;
            }
            set
            { addresses = value; }
        }

        public KxWebStudentApplicationCorrespondenceList Correspondence
        {
            get
            {
                if (correspondence == null)
                {
                    correspondence = new KxWebStudentApplicationCorrespondenceList(this, guidIdentifier);
                }
                return correspondence;
            }
            set
            { correspondence = value; }
        }

        public KxWebStudentPreferences Preferences
        {
            get
            {
                if (preferences == null)
                {
                    preferences = new KxWebStudentPreferences(this, guidIdentifier);
                }
                return preferences;
            }
            set
            { preferences = value; }
        }
        public KxWebFriendshipGroup FriendshipGroup
        {
            get
            {
                if (friendshipGroup == null)
                {
                    friendshipGroup = new KxWebFriendshipGroup(this, guidIdentifier);
                }
                return friendshipGroup;
            }
            set
            { friendshipGroup = value; }
        }

        public KxWebStudentApplicationInduction Induction
        {
            get
            {
                if (induction == null)
                {
                    induction = KxWebStudentApplicationInduction.GetWebStudentApplicationInduction(this.ApplicationGuid);
                }
                return induction;
            }
        }

        public bool IsInductionStarted
        {
            get { return Induction.InductionStarted.HasValue; }
        }

        public bool IsInductionCompleted
        {
            get { return Induction.InductionCompleted.HasValue; }
        }

        public bool IsInFriendshipGroup
        {
            get { return IsApplicationInAGroup(); }
        }

        private bool IsApplicationInAGroup()
        {
            KxData data = new KxData("member", "kcf.IsWebApplicationInAFriendshipGroup", TargetType.StoredProc, ActionType.ExecuteProcedure);
            data.AddParameter("applicationguid", this.ApplicationGuid, KxValueType.Guid);
            if (data.Execute())
            {
                foreach (XmlNode node in data.GetNodesFromData())
                {
                    return KxXmlUtils.GetNodeValue(node, "result", 0) > 0;
                }
            }

            return false;
        }

        public KxWebStudentPayments Payments
        {
            get
            {
                if (payments == null)
                {
                    payments = new KxWebStudentPayments(this, guidIdentifier);
                }
                return payments;
            }
            set
            { payments = value; }
        }

        public KxPaymentScheduleType PaymentScheduleType
        {
            get
            {
                if (paymentScheduleType == null && paymentScheduleTypeId != 0)
                {
                    paymentScheduleType = new KxPaymentScheduleType(this, paymentScheduleTypeId);
                }
                return paymentScheduleType;
            }
            set
            {
                if (paymentScheduleType == value)
                    return;
                paymentScheduleType = value;
            }
        }

        public KxStudentApplication StandardApplication
        {
            get
            {
                if (standardApplication == null && applicationId != 0)
                {
                    standardApplication = new KxStudentApplication(this, applicationId);
                }
                return standardApplication;
            }
            set
            {
                if (standardApplication == value)
                    return;
                standardApplication = value;
            }
        }

        public KxUserDefinedValues UserDefinedValues
        {
            get
            {
                if (userDefinedValues == null)
                {
                    userDefinedValues = new KxUserDefinedValues(this, ApplicationGuid);
                }

                return userDefinedValues;
            }
            set
            {
                if (userDefinedValues == value)
                    return;
                userDefinedValues = value;
            }
        }
        public KxWebUserStudent WebUser
        {
            get
            {
                try
                {
                    if (Parent.Parent != null)
                    {
                        return ((KxWebUserStudent)Parent.Parent);
                    }
                    else
                    {
                        return null;
                    }
                }
                catch
                {
                    return null;
                }
                
            }
        }
        public bool HasGroupInvitations
        {
             get
             {
                return GetHasGroupInvitations();
             }
        }
        public bool IsGroupLeader
        {
            get
            {
                return GetIsGroupLeader();
            }
        }
        public bool IsGroupMember
        {
            get
            {
                return GetIsGroupMember();
            }
        }        
        public string GroupStatusString
        {
            get
            {
                return GetGroupStatusString();
            }
        }
        public bool IsNotInAGroup
        {
            get
            {
               return GetIsNotInAGroup();
            }
        }
        public KxWebStudentPreference CurrentIndexPreference
        {
            get
            {
                return GetCurrentPreferenceIndex();
            }
        }
        public KxWebStudentPreference CurrentNumberPreference
        {
            get
            {
                return GetCurrentPreferenceNumber();
            }
        }
        public KxWebFriendshipGroupMember GroupMember
        {
            get
            {
                if (groupMember == null)
                {
                    try
                    {
                        groupMember = friendshipGroup.GetGroupMember(WebUser);
                    }
                    catch
                    { }
                }
                return groupMember;
            }
        }

        public bool HasCurrentAddress
        { 
            get
            {
                try
                {
                    return CurrentStudentAddress.IsPopulated;
                }
                catch 
                {
                    return false;
                }
            }
        }
        public bool HasEmergencyAddress
        {
            get
            {
                try
                {
                    return EmergencyStudentAddress.IsPopulated;
                }
                catch
                {
                    return false;
                }
            }
        }
        public bool HasHomeAddress
        {
            get
            {
                try
                {
                    return HomeStudentAddress.IsPopulated;
                }
                catch
                {
                    return false;
                }
            }
        }
        public bool HasEmailAddress
        {
            get
            {
                try
                {
                    return PreferredEmailAddress.IsPopulated;
                }
                catch
                {
                    return false;
                }
            }
        }
        public bool HasCurrentAndEmergencyAddress
        {
            get
            {
                return HasCurrentAddress && HasEmergencyAddress;
            }
        }

        public bool HasHomeAndEmergencyAddress
        {
            get
            {
                return HasHomeAddress && HasEmergencyAddress;
            }
        }

        public bool HasNotCurrentAndEmergencyAddress
        {
            get
            {
                return !HasCurrentAddress || !HasEmergencyAddress;
            }
        }

        public bool HasNotHomeAndEmergencyAddress
        {
            get
            {
                return !HasHomeAddress || !HasEmergencyAddress;
            }
        }

        public KxWebStudentAddress CurrentStudentAddress
        {
            get
            {
                if (currentStudentAddress == null)
                {
                    int addId = 2;

                    try
                    {
                        addId = Convert.ToInt32(KxDbConfiguration.GetConfiguration("CurrentAddressTypeId"));
                    }
                    catch { }

                    currentStudentAddress = Addresses.GetApplicationAddress(addId, KXAddressTypes.Current);
                }
                return currentStudentAddress;
            }
            set
            {
                if (currentStudentAddress == value)
                    return;
                currentStudentAddress = value;
            }
        }
        public KxWebStudentAddress EmergencyStudentAddress
        {
            get
            {
                if (emergencyStudentAddress == null)
                {
                    int addId = 4;

                    try
                    {
                        addId = Convert.ToInt32(KxDbConfiguration.GetConfiguration("EmergencyAddressTypeId"));
                    }
                    catch { }

                    emergencyStudentAddress = Addresses.GetApplicationAddress(addId, KXAddressTypes.Emergency);
                }
                return emergencyStudentAddress;
            }
            set
            {
                if (emergencyStudentAddress == value)
                    return;
                emergencyStudentAddress = value;
            }
        }
        public KxWebStudentAddress HomeStudentAddress
        {
            get
            {
                if (homeStudentAddress == null)
                {
                    int addId = 1;

                    try
                    {
                        addId = Convert.ToInt32(KxDbConfiguration.GetConfiguration("HomeAddressTypeId"));
                    }
                    catch { }

                    homeStudentAddress = Addresses.GetApplicationAddress(addId, KXAddressTypes.Home);
                }
                return homeStudentAddress;
            }
            set
            {
                if (homeStudentAddress == value)
                    return;
                homeStudentAddress = value;
            }
        }
        public KxWebStudentAddress PreferredEmailAddress
        {
            get
            {
                if (preferredEmailAddress == null)
                {
                    preferredEmailAddress = Addresses.GetApplicationAddress(3, KXAddressTypes.Email);
                } 
                return preferredEmailAddress;
            }
            set
            {
                if (preferredEmailAddress == value)
                    return;
                preferredEmailAddress = value;
            }
        }

        public KxWebStudentRoomCityRates CityRoomRates
        {
            get
            {
                CheckCityRoomRates();
                return cityRoomRates;
            }
        }

        public KxWebStudentRoomLibertyRates LibertyRoomRates
        {
            get
            {
                CheckLibertyRoomRates();
                return libertyRoomRates;
            }
        }



        public KxWebStudentAddress SponsorAddress
        {
            get
            {
                if (sponsorAddress == null)
                {
                    sponsorAddress = Addresses.GetApplicationAddress(9, KXAddressTypes.Sponsor);
                }
                return sponsorAddress;
            }
            set
            {
                if (sponsorAddress == value)
                    return;
                sponsorAddress = value;
            }
        }

        public Decimal SafeDepositTotal
        {
            get
            {
                return GetSafeDepositTotal();
            }
        }

        public Decimal SafePaymentTotal
        {
            get
            {
                return GetSafePaymentTotal();
            }
        }

        public bool ProceedWithApplication
        {
            get
            { return proceedWithApplication; }
            set
            { proceedWithApplication = value; }
        }

        public bool DoNotProceedWithApplication
        {
            get
            { return !proceedWithApplication; }
        }
        public int CurrentPreferenceIndex
        {
            get
            {
                if (controlPreferenceIndex != 0)
                {
                    return controlPreferenceIndex;
                }
                else
                {
                    return userPreferenceIndex;
                }
            }
        }
        public int UserPreferenceIndex
        {
            get
            { return userPreferenceIndex; }
            set
            { userPreferenceIndex = value; }
        }
        public int ControlPreferenceIndex
        {
            get
            { return controlPreferenceIndex; }
            set
            { controlPreferenceIndex = value; }
        }
        public int CurrentPreferenceNumber
        {
            get
            {
                if (controlPreferenceNumber != 0)
                {
                    return controlPreferenceNumber;
                }
                else
                {
                    return userPreferenceNumber;
                }
            }
        }
        public int UserPreferenceNumber
        {
            get
            { return userPreferenceNumber; }
            set
            { userPreferenceNumber = value; }
        }
        public int ControlPreferenceNumber
        {
            get
            { return controlPreferenceNumber; }
            set
            { controlPreferenceNumber = value; }
        }
    
        public string PreferencesAsString
        {
            get
            { return GetPreferencesAsString(); }
        }
        public KxWebStudentPreference[] SavedPreferences
        {
            get
            { return GetSavedPreferences(); }
        }
   
        public Decimal ApplicationPaymentTotal
        {
            get
            { return Payments.TotalPayments;}
        }

        public Decimal ApplicationDepositTotal
        {
            get
            { return Payments.TotalDeposits; }
        }

        public Decimal ApplicationAncillaryTotal
        {
            get
            { return Payments.TotalAncillaries; }
        }
    
        public bool HasNoPayments
        {
            get
            {
                string s = System.Configuration.ConfigurationManager.AppSettings["DebugAppHasNoPayments"];  // pha
                if (s == "True")
                    return true;
                else if (s == "False")
                    return false;
                return ApplicationPaymentTotal < 0.01M; 
            }
        }

        public bool HasNoDeposits
        {
            get
            {
                string s = System.Configuration.ConfigurationManager.AppSettings["DebugAppHasNoDeposits"];  // pha
                if (s == "True")
                    return true;
                else if (s == "False")
                    return false;
                return ApplicationDepositTotal < 0.01M; 
            }
        }

        public bool HasNoAncillaries
        {
            get
            { return ApplicationAncillaryTotal < 0.01M; }
        }

        public bool HasNoInstalments
        {
            get
            { return Payments.GetHasNoInstalments(); }
        }


        public KxWebStudentPreference FirstPreference
        {
            get
            {
                controlPreferenceNumber = 1;
                return GetCurrentPreferenceNumber();
            }
        }

        public KxWebStudentPreference SecondPreference
        {
            get
            {
                controlPreferenceNumber = 2;
                return GetCurrentPreferenceNumber();
            }
        }

        public KxWebStudentPreference ThirdPreference
        {
            get
            {
                controlPreferenceNumber = 3;
                return GetCurrentPreferenceNumber();
            }
        }

		public KxWebStudentPreference FourthPreference
		{
			get
			{
				controlPreferenceNumber = 4;
				return GetCurrentPreferenceNumber();
			}
		}

		public KxWebStudentPreference FifthPreference
		{
			get
			{
				controlPreferenceNumber = 5;
				return GetCurrentPreferenceNumber();
			}
		}

		public KxWebStudentPreference SixthPreference
		{
			get
			{
				controlPreferenceNumber = 6;
				return GetCurrentPreferenceNumber();
			}
		}

		public KxWebStudentPreference SeventhPreference
		{
			get
			{
				controlPreferenceNumber = 7;
				return GetCurrentPreferenceNumber();
			}
		}

		public KxWebStudentPreference EighthPreference
		{
			get
			{
				controlPreferenceNumber = 8;
				return GetCurrentPreferenceNumber();
			}
		}

		public KxWebStudentPreference NinthPreference
		{
			get
			{
				controlPreferenceNumber = 9;
				return GetCurrentPreferenceNumber();
			}
		}

		public KxWebStudentPreference TenthPreference
		{
			get
			{
				controlPreferenceNumber = 10;
				return GetCurrentPreferenceNumber();
			}
		}

		public string FirstPreferenceCustomValue4
        {
            get
            {
                controlPreferenceNumber = 1;
                return GetCurrentPreferenceNumber().CustomValue4;
            }
        }

        public string SecondPreferenceCustomValue4
        {
            get
            {
                controlPreferenceNumber = 2;
                return GetCurrentPreferenceNumber().CustomValue4;
            }
        }
        public string FirstPreferenceCustomValue5
        {
            get
            {
                controlPreferenceNumber = 1;
                return GetCurrentPreferenceNumber().CustomValue5;
            }
        }

        public string SecondPreferenceCustomValue5
        {
            get
            {
                controlPreferenceNumber = 2;
                return GetCurrentPreferenceNumber().CustomValue5;
            }
        }
        public string FirstPreferenceCustomValue6
        {
            get
            {
                controlPreferenceNumber = 1;
                return GetCurrentPreferenceNumber().CustomValue6;
            }
        }

        public string SecondPreferenceCustomValue6
        {
            get
            {
                controlPreferenceNumber = 2;
                return GetCurrentPreferenceNumber().CustomValue6;
            }
        }
        public string FirstPreferenceCustomValue7
        {
            get
            {
                controlPreferenceNumber = 1;
                return GetCurrentPreferenceNumber().CustomValue7;
            }
        }

        public string SecondPreferenceCustomValue7
        {
            get
            {
                controlPreferenceNumber = 2;
                return GetCurrentPreferenceNumber().CustomValue7;
            }
        }
        public string FirstPreferenceCustomValue8
        {
            get
            {
                controlPreferenceNumber = 1;
                return GetCurrentPreferenceNumber().CustomValue8;
            }
        }

        public string SecondPreferenceCustomValue8
        {
            get
            {
                controlPreferenceNumber = 2;
                return GetCurrentPreferenceNumber().CustomValue8;
            }
        }

        public KxNationality Nationality
        {
            get
            {
                if (nationality == null && nationalityId > 0)
                {
                    nationality = new KxNationality(this,nationalityId);
                }
                return nationality;
            }
        }

        public KxEthnicity Ethnicity
        {
            get
            {
                if (ethnicity == null && ethnicityId > 0)
                {
                    ethnicity = new KxEthnicity(this, ethnicityId);
                }
                return ethnicity;
            }
        }

        public KxDomicile DomicileProxy
        {
            get
            {
                //104550b - PMc - Tried to change this based on Adam's advice but this seems to have made it worse, so I've changed it back
                //if ((domicileProxy == null && licenceTypeId > 0) || (domicileProxy != null && licenceTypeId != domicileProxy.DomicileId))
                if (domicileProxy == null && licenceTypeId > 0) 
                {
                    domicileProxy = new KxDomicile(this, licenceTypeId);
                }
                return domicileProxy;
            }
        }

        public KxStudentYear StudentYear
        {
            get
            {
                if (studentYear == null && studentYearId > 0)
                {
                    studentYear = new KxStudentYear(this, studentYearId);
                }
                return studentYear;
            }
        }

        public string StudentYearName
        {
            get
            {
                if (StudentYear != null)
                {
                    return StudentYear.Description;
                }
                else
                {
                    return "";
                }
            }
        }


        public string EthnicityName
        {
            get
            {
                if (Ethnicity != null)
                {
                    return Ethnicity.Description;
                }
                else
                {
                    return "";
                }
            }
        }

        public string DomicileProxyName
        {
            get
            {
                if (DomicileProxy != null)
                {
                    return DomicileProxy.Description;
                }
                else
                {
                    return "";
                }
            }
        }

      

        public string NationalityName
        {
            get
            {
                if (Nationality != null)
                {
                    return Nationality.Description;
                }
                else
                {
                    return "";
                }
            }
        }

        public KxStudentCharges ApplicationCharges
        {
            get
            {
                if (applicationCharges == null && applicationId > 0)
                {
                    applicationCharges = new KxStudentCharges(this, applicationId, KxStudentChargingType.Application);
                }
                return applicationCharges;
            }
        }

        public bool HasCoupleRoomPreference
        {
            get
            {
                return Preferences.GetHasCoupleRooms();
            }
        }

        public KxeInductionQuestionAttempts QuestionAttempts
        {
            get
            {
                if (questionAttempts == null && applicationId > 0)
                {
                    questionAttempts = new KxeInductionQuestionAttempts(this, applicationId);
                }
                return questionAttempts;
            }
            set
            {
                questionAttempts = value;
            }
        }

        public KxeInductionArrival InductionArrival
        {
            get
            {
                if (inductionArrival == null && applicationId > 0)
                {
                    inductionArrival = new KxeInductionArrival(this, applicationId);
                }
                return inductionArrival;
            }
            set
            {
                inductionArrival = value;
            }
        }

        public bool CustomInductionStatus
        {
            get
            {
                return GetCustomInductionStatus();
            }
        }

        public bool CustomEligibilityStatus
        {
            get
            {
                return GetCustomEligibilityStatus();
            }
        }

        public string CustomInductionStage
        {
            get
            {
                return GetCustomInductionStage();
            }
        }

        public bool IsInDefaultAcademicYear
        {
            get
            {
                return GetIsDefaultAcademicYear();
            }
        }

        public KxCourse CourseMidName
        {
            get
            {
                if (courseMidName == null)
                {
                    courseMidName = GetCourseMidName();
                }
                return courseMidName;
            }
        }

        public string CourseMidNameString
        {
            get
            {
                try
                {
                    return CourseMidName.Name;
                }
                catch
                {
                    return "";
                }
            }
        }

        public List<KxStudentRequest> StudentRequests
        {
            get
            {
                if (this.applicationId > 0)
                {
                    string cacheId = string.Format("Requests{0}", this.applicationId);
                    CacheItem cacheItem = _cache.GetCacheItem(cacheId);
                    List<KxStudentRequest> requests = null;

                    if (cacheItem != null)
                    {
                         requests = (List<KxStudentRequest>)cacheItem.Value;
                    }
                    if (requests == null)
                    {
                        requests = KxStudentRequest.GetST2StudentRequests(this.kxStudentId, this.applicationId, 0);
                        _cache.Add(cacheId, requests, DateTime.Now.AddSeconds(20));
                    }

                    return requests;
                }

                return new List<KxStudentRequest>();
            }
        }

        public void ClearRequestCache()
        {
            string cacheId = string.Format("Requests{0}", this.applicationId);
            CacheItem cacheItem = _cache.GetCacheItem(cacheId);

            if (cacheItem != null)
            {
                _cache.Remove(cacheId);
            }
        }

        public int CountOfActiveStudentRequests
        {
            get
            {
                return StudentRequests.FindAll(s => s.RequestStatus.IsClosed == 0).Count;
            }
        }

        public int CountOfStudentRequests
        {
            get
            {
                return StudentRequests.Count;
            }
        }

        public bool HasActiveStudentRequests
        {
            get
            {
                return CountOfActiveStudentRequests > 0;
            }
        }

        public bool HasStudentRequests
        {
            get
            {
                return CountOfStudentRequests > 0;
            }
        }

        public bool IsApplicationWindowOpen
        {
            get { return GetApplicationWindowStatus(); }
        }

        public string ApplicationWindowMessage { get; set; }

        private bool GetApplicationWindowStatus()
        {
            ApplicationWindowMessage = string.Empty;
            var window = new KxApplicationWindow(null, this.ApplicationTypeId, this.AcademicYearId);
            if (window.ApplicationWindowId > 0)
            {
                if ((window.ApplicationsOpen.HasValue && window.ApplicationsOpen.Value.Date <= DateTime.Now.Date &&
                     window.ApplicationsClose.HasValue && window.ApplicationsClose.Value.Date >= DateTime.Now.Date))
                {
                    return true;
                }
                else
                {
                    ApplicationWindowMessage = window.ClosedMessage;
                    return false;
                }
            }

            return true;
        }

        private bool DirectLoad { get; set; }
        #endregion 

        public KxWebStudentApplication(KxBaseClass parent) : base(parent)
        {
            creationDate = DateTime.Now;
        }

        public KxWebStudentApplication(KxBaseClass parent, Guid applicationGuid)
            : this(parent)
        {
            this.guidIdentifier = applicationGuid;
            Load();
        }
        public KxWebStudentApplication(KxBaseClass parent, Guid applicationGuid, bool directLoad)
            : this(parent)
        {
            this.guidIdentifier = applicationGuid;
            DirectLoad = directLoad;
            Load();
        }

        public KxWebStudentApplication(KxBaseClass parent, XmlNode node)
            : this(parent)
        {
            LoadFromXml("", node);
        }

        public bool HasActiveStudentRequestsOfType(string type)
        {
            return StudentRequests.Find(s => StringComparer.InvariantCultureIgnoreCase.Compare(s.RequestType.WebKey, type) == 0 && s.RequestStatus.IsClosed == 0) != null;
        }

        public bool HasStudentRequestsOfType(string type)
        {
            return StudentRequests.Find(s => StringComparer.InvariantCultureIgnoreCase.Compare(s.RequestType.WebKey, type) == 0) != null;
        }

        public KxStudentPhoto StudentPhoto
        {
            get
            {
                return new KxStudentPhoto();
            }
            set
            {
                photo = value;
            }
        }

        public override KxData ConfigureData(ActionType actiontype)
        {
            if (parent != null)
            {
                userGuid = parent.GuidIdentifier;
            }

            KxData result = null;

            if (UserGuid != Guid.Empty || DirectLoad)
            {
                result = new KxData("webstudentapplication", "WebStudentApplication", TargetType.Table);

                if ((actiontype == ActionType.Update) && (guidIdentifier == Guid.Empty))
                {
                    if (changed)
                    {
                        guidIdentifier = Guid.NewGuid();
                        creationDate = DateTime.Now;
                        if (System.Configuration.ConfigurationManager.AppSettings["DebugNewWebStudentApp"] == "True")
                        { 
                            using (System.IO.StreamWriter sw = new System.IO.StreamWriter(@"c:\temp\workflow.xml", true))
                            {
                                sw.WriteLine("<WebStudentApplication>");
                                sw.WriteLine("\tNEW: " + guidIdentifier);
                                sw.WriteLine("</WebStudentApplication>");
                            }
                        }
                    }
                    result.Action = ActionType.Insert;
                }
                else
                {
                    result.Action = actiontype;
                }

                result.KeyFields.Add(new KxDataValue("applicationGuid", guidIdentifier, KxValueType.Guid));

                if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
                {
                    result.AddParameter("processGuid", processGuid, KxValueType.Guid, true);
                    result.AddParameter("kxStudentId", kxStudentId, KxValueType.Int, true);
                    result.AddParameter("academicYearId", academicYearId, KxValueType.Int, true);
                    result.AddParameter("applicationId", applicationId, KxValueType.Int, true);
                    result.AddParameter("applicationTypeId", applicationTypeId, KxValueType.Int, true);
                    result.AddParameter("courseTypeId", courseTypeId, KxValueType.Int, true);
                    result.AddParameter("studentYearId", studentYearId, KxValueType.Int, true);
                    result.AddParameter("licenceTypeId", licenceTypeId, KxValueType.Int, true);
                    result.AddParameter("userGuid", userGuid, KxValueType.Guid, true);
                    result.AddParameter("studentIdentifier", studentIdentifier, KxValueType.String, 50, true);
                    result.AddParameter("title", title, KxValueType.String, 50, true);
                    result.AddParameter("foreName", foreName, KxValueType.String, 50, true);
                    result.AddParameter("midName", midName, KxValueType.String, 50, true);
                    result.AddParameter("surName", surName, KxValueType.String, 50, true);
                    result.AddParameter("emailAddress", emailAddress, KxValueType.String, 255, true);
                    result.AddParameter("dob", dob, KxValueType.DateTime, true);
                    result.AddParameter("nationalityId", nationalityId, KxValueType.Int, true);
                    result.AddParameter("ethnicityId", ethnicityId, KxValueType.Int, true);
                    result.AddParameter("creationDate", creationDate, KxValueType.DateTime, false);
                    result.AddParameter("confirmedDate", confirmedDate, KxValueType.DateTime, true);
                    result.AddParameter("gender", gender, KxValueType.String, 10, true);
                    result.AddParameter("paymentScheduleTypeId", paymentScheduleTypeId, KxValueType.Int, true);
                    result.AddParameter("expired", expired, KxValueType.Boolean, true);
                    if (SessionsExist())
                    {
                        result.AddParameter("sessionid", _sessionId, KxValueType.Int, false);
                        result.AddParameter("sessionstartdate", _sessionStartDate, KxValueType.DateTime, true);
                        result.AddParameter("sessionenddate", _sessionEndDate, KxValueType.DateTime, true);
                    }
                }
            }
            return result;
        }


        private static string _sessionsExist;
        private bool SessionsExist()
        {
            const string STR_No = "No";
            const string STR_Yes = "Yes";
            try
            {
                if (string.IsNullOrEmpty(_sessionsExist))
                {
                    KxData data = new KxData("dd", "SELECT TOP 1 SessionId FROM WebStudentApplication",
                        TargetType.Static);
                    if (data.Execute())
                    {
                        _sessionsExist = STR_Yes;
                    }
                    else
                    {
                        _sessionsExist = STR_No;
                    }
                }
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "Finding Sessions  column", ex.Message, "");
                _sessionsExist = STR_No;
            }

            return _sessionsExist == STR_Yes;
        }

        public override void LoadFromXml(string xml,XmlNode node)
        {
            Children.Remove(addresses);
            Children.Remove(preferences);
            Children.Remove(friendshipGroup);
            Children.Remove(groupMember);
            Children.Remove(payments);
            Children.Remove(standardApplication);
            Children.Remove(userDefinedValues);
            Children.Remove(currentStudentAddress);
            Children.Remove(emergencyStudentAddress);
            Children.Remove(homeStudentAddress);
            Children.Remove(preferredEmailAddress);

            addresses = null;
            preferences = null;
            friendshipGroup = null;
            groupMember = null;
            payments = null;
            standardApplication = null;
            userDefinedValues = null;
            currentStudentAddress = null;
            emergencyStudentAddress = null;
            homeStudentAddress = null;
            preferredEmailAddress = null;

            guidIdentifier = KxXmlUtils.GetNodeValue(node, "applicationguid", Guid.Empty);
            processGuid = KxXmlUtils.GetNodeValue(node, "processguid", Guid.Empty);
            kxStudentId = KxXmlUtils.GetNodeValue(node, "kxstudentid", 0);
            applicationId = KxXmlUtils.GetNodeValue(node, "applicationid", 0);
            applicationTypeId = KxXmlUtils.GetNodeValue(node, "applicationtypeid", 0);
            courseTypeId = KxXmlUtils.GetNodeValue(node, "coursetypeid", 0);
            academicYearId = KxXmlUtils.GetNodeValue(node, "academicyearid", 0);
            studentYearId = KxXmlUtils.GetNodeValue(node, "studentyearid", 0);
            licenceTypeId = KxXmlUtils.GetNodeValue(node, "licencetypeid", 0);
            userGuid = KxXmlUtils.GetNodeValue(node, "userguid", Guid.Empty);
            studentIdentifier = KxXmlUtils.GetNodeValue(node, "studentidentifier", "");
            title = KxXmlUtils.GetNodeValue(node, "title", "");
            foreName = KxXmlUtils.GetNodeValue(node, "forename", "");
            midName = KxXmlUtils.GetNodeValue(node, "midname", "");
            surName = KxXmlUtils.GetNodeValue(node, "surname", "");
            emailAddress = KxXmlUtils.GetNodeValue(node, "emailaddress", "");
            dob = KxXmlUtils.GetNodeValueDT(node, "dob", null);
            nationalityId = KxXmlUtils.GetNodeValue(node, "nationalityid", 0);
            ethnicityId = KxXmlUtils.GetNodeValue(node, "ethnicityid", 0);
            creationDate = KxXmlUtils.GetNodeValueDT(node, "creationdate", null);
            confirmedDate = KxXmlUtils.GetNodeValueDT(node, "confirmeddate", null);
            gender = KxXmlUtils.GetNodeValue(node, "gender", "");
            paymentScheduleTypeId = KxXmlUtils.GetNodeValue(node, "paymentscheduletypeid", 0);
            expired = KxXmlUtils.GetNodeValue(node, "expired", false);
            _sessionId = KxXmlUtils.GetNodeValue(node, "sessionid", 0);
            _sessionStartDate = KxXmlUtils.GetNodeValueDT(node, "sessionstartdate", null);
            _sessionEndDate = KxXmlUtils.GetNodeValueDT(node, "sessionenddate", null);
        }

        private bool GetIsComplete()
        {
            return (Preferences.Count >= 3 && applicationTypeId != 0 && confirmedDate != null);
        }

        private bool GetIsEditable()
        {
            return (confirmedDate == null && !IsNew);
        }

        public void ConfirmApplication()
        {
            if (ConfirmedDate == null)
            {
                ConfirmedDate = DateTime.Now;
            }
        }

        public string GetApplicationStatus()
        {
            if (!IsInProgress())
            {
                return "You have no application for this year";
            }

            else
            {
                try
                {
                    if (StandardApplication != null)
                    {
                        if (StandardApplication.Expired)
                        {
                            return "Your application has been cancelled ";
                        }
                        else if (Parent != null && Parent.Parent != null && Parent.Parent is KxWebUserStudent)
                        {
                            if (((KxWebUserStudent)Parent.Parent).HasPendingAllocation)
                            {
                                return "You have a pending offer of accommodation";
                            }
                            else if (((KxWebUserStudent)Parent.Parent).HasAcceptedAllocation)
                            {
                                return "You have accepted an offer of accommodation";
                            }
                            else if (((KxWebUserStudent)Parent.Parent).HasRejectedAllocation)
                            {
                                return "You have rejected an offer of accommodation";
                            }
                            else
                            {
                                return KxLocalisationManager.GetSubstitution("Your application was processed on") + " " + KxDateTime.ToString(StandardApplication.ApplicationDate,"d");
                            }
                        }
                        else
                        {
                            return KxLocalisationManager.GetSubstitution("Your application was processed on") + " " + KxDateTime.ToString(StandardApplication.ApplicationDate,"d");
                        }
                    }
                    else if (ApplicationId != 0)
                    {
                        return "Your application has been processed ";
                    }
                    else if (ConfirmedDate != null)
                    {
                        return KxLocalisationManager.GetSubstitution("Your application was confirmed on") + " " + KxDateTime.ToString(ConfirmedDate,"d");

                    }
                    else
                    {
                        return "You are currently creating an application ";
                    }
                }
                catch (Exception ex)
                {
                    KxLogger.LogActivity(this, "GetApplicationStatus", ex.Message,"");
                    return "An error was encountered determining your status. Please try again later";
                }
            }
        }

        public bool GetHasGroupInvitations()
        {
            bool result = false;

            if (FriendshipGroup != null)
            {
                result = friendshipGroup.HasInvitations(WebUser);
            }
            return result;
        }

        public bool GetIsGroupLeader()
        {
            bool result = false;

            if (FriendshipGroup != null)
            {
                result = friendshipGroup.IsGroupLeader(WebUser);
            }

            return result;
        }

        public bool GetIsGroupMember()
        {
            bool result = false;

            if (FriendshipGroup != null)
            {
                result = friendshipGroup.IsGroupMember(WebUser) && !IsGroupLeader;
            }

            return result;
        }


        public bool GetIsNotInAGroup()
        {
            bool result = false;

            if (GroupMember != null)
            {
                result = friendshipGroup.GetGroupMember(WebUser) == null;
            }
            else
            {
                return true;
            }

            return result;
        }        

        public string GetGroupStatusString()
        {
            string result = "You have no group activity";

            if (GetHasGroupInvitations())
            {
                result = "You have been invited to join " + friendshipGroup.GroupName;
            }
            else
            {
                if (GetIsGroupLeader())
                {
                    result = "You are the leader of " + friendshipGroup.GroupName + " friendship group";
                }
                else if (GetIsGroupMember())
                {
                    result = "You are a member of " + friendshipGroup.GroupName + " friendship group";
                }
            }
            return result;
        }

        public KxWebStudentPreference GetCurrentPreferenceIndex()
        {
            KxWebStudentPreference result = null;

            result = Preferences.PreferenceByIndex(CurrentPreferenceIndex);

            if (result == null)
            {
                Changed = true;
                result = Preferences.AddPreference();
                result.Temporary = CurrentPreferenceIndex == 0;
                result.PreferenceNumber = CurrentPreferenceIndex;
                return result;
            }
            else
            {
                return result;
            }
        }

        public KxWebStudentPreference GetCurrentPreferenceNumber()
        {
            KxWebStudentPreference result = null;

            result = Preferences.PreferenceByNumber(CurrentPreferenceNumber);

            if (result == null && CurrentPreferenceNumber != -99)
            {
                Changed = true;
                result = Preferences.AddPreference();
                result.Temporary = CurrentPreferenceNumber == 0;
                result.PreferenceNumber = CurrentPreferenceNumber;
                return result;
            }
            else
            {
                return result;
            }
        }

        public string GetPreferencesAsString()
        {
            StringBuilder sb = new StringBuilder();

            foreach (KxWebStudentPreference pref in Preferences.Items)
            {
                sb.AppendLine(pref.GetPreferenceDescription());
            }
            return sb.ToString();
        }

        public KxWebStudentPreference[] GetSavedPreferences()
        {
            ArrayList list = new ArrayList();

            foreach (KxWebStudentPreference pref in Preferences.Items)
            {
                if (!pref.Temporary)
                {
                    list.Add(pref);
                }
            }

            KxWebStudentPreference[] result = new KxWebStudentPreference[list.Count];

            list.CopyTo(result, 0);

            return result;
        }

        public int GetCreatedKineticId()
        {
            int newid = 0;

            if (generatedKXId == -99)
            {
                KxData data = new KxData("", "ST2WebApplication", TargetType.Table);

                data.AddKeyField("applicationguid", guidIdentifier, KxValueType.Guid);
                data.ScalarField = "kxstudentid";

                object result = data.ExecuteScalar();

                if (result != null && !(result is DBNull))
                {
                    try
                    {
                        newid = Convert.ToInt32(result);
                    }
                    catch
                    { }
                }
                generatedKXId = newid;
            }
            return newid;
        }

        public bool IsInProgress()
        {
            bool result = false;

            try
            {
                result = applicationTypeId != 0 || courseTypeId != 0 ||
                         studentYearId != 0 || licenceTypeId != 0 || 
                         Preferences.HasMadePreferenceSelection();
            }
            catch
            {
                
            }
            return result;
        }

        private string GetVerboseGender()
        {
            switch (gender)
            {
                case "M": return "Male";
                case "F": return "Female";
                default: return "Unknown";
            }
        }

        private string GetStudentYear()
        {
            switch (studentYearId)
            {
                case 0: return "Unspecified";
                case 1: return "1st Year";
                default: return "returner";
            }
        }

        private Decimal GetSafeDepositTotal()
        {
            Decimal result = 0.0M;

            if (Payments.TotalDeposits > 0.0M)
            {
                result = Payments.TotalDeposits;
            }
            else
            {
                try
                {
                    result = StandardApplication.Payments.TotalDeposit;
                }
                catch
                { }
            }

            try
            {
              //  KxLogger.LogActivity(this, "GetSafeDepositTotal", result.ToString(), KxStudentId.ToString());
            }
            catch { }

            return result;
        }

        private Decimal GetSafePaymentTotal()
        {
            Decimal result = 0.0M;

            if (Payments.TotalPayments > 0.0M)
            {
                result = Payments.TotalPayments;
            }
            else
            {
                try
                {
                    result = StandardApplication.Payments.TotalPayment;
                }
                catch
                { }
            }

            try
            {
                //  KxLogger.LogActivity(this, "GetSafeDepositTotal", result.ToString(), KxStudentId.ToString());
            }
            catch { }

            return result;
        }


        protected void CheckLibertyRoomRates()
        {
            if (libertyRoomRates == null || libertyRoomRates.SiteId != FirstPreference.SiteId || libertyRoomRates.AreaId != FirstPreference.AreaId || libertyRoomRates.BlockId != FirstPreference.BlockId || libertyRoomRates.AcademicYearId != AcademicYearId)
            {
                libertyRoomRates = new KxWebStudentRoomLibertyRates(this, AcademicYearId, FirstPreference.SiteId,FirstPreference.AreaId, FirstPreference.BlockId);
            }
        }

        protected void CheckCityRoomRates()
        {
            if (cityRoomRates == null || cityRoomRates.SiteId != FirstPreference.SiteId || cityRoomRates.AreaId != FirstPreference.AreaId || cityRoomRates.BlockId != FirstPreference.BlockId || cityRoomRates.AcademicYearId != AcademicYearId)
            {
                cityRoomRates = new KxWebStudentRoomCityRates(this, AcademicYearId, FirstPreference.SiteId, FirstPreference.AreaId, FirstPreference.BlockId);
            }
        }

        protected bool GetIsDefaultAcademicYear()
        {
            bool result = false;

            try
            {
                result = AcademicYearId == WebUser.DefaultAcademicYear;
            }
            catch
            {

            }

            return result;
        }

        protected KxCourse GetCourseMidName()
        {
            KxCourse result = null;

            try
            {
                int courseId = Convert.ToInt32(MidName);

                result = new KxCourse(null, courseId);
            }
            catch
            {

            }
            return result;
        }

        protected bool GetCustomInductionStatus()
        {
            KxData data = new KxData("", "WebGetInductionStatus", TargetType.StoredProc);
            data.AddParameter("Parm1", AcademicYearId, KxValueType.Unknown);
            data.AddParameter("Parm2", KxStudentId, KxValueType.Unknown);
            data.AddParameter("Parm3", ApplicationId, KxValueType.Unknown);
            data.AddParameter("Parm4", "", KxValueType.Unknown);
            
            return Convert.ToBoolean(data.ExecuteScalar());
        }

        protected bool GetCustomEligibilityStatus()
        {
            KxData data = new KxData("", "WebGetEligibilityStatus", TargetType.StoredProc);
            data.AddParameter("Parm1", AcademicYearId, KxValueType.Unknown);
            data.AddParameter("Parm2", KxStudentId, KxValueType.Unknown);
            data.AddParameter("Parm3", ApplicationId, KxValueType.Unknown);
            data.AddParameter("Parm4", "", KxValueType.Unknown);

            return Convert.ToBoolean(data.ExecuteScalar());
        }

        protected string GetCustomInductionStage()
        {
            KxData data = new KxData("", "WebGetInductionStage", TargetType.StoredProc);
            data.AddParameter("Parm1", AcademicYearId, KxValueType.Unknown);
            data.AddParameter("Parm2", KxStudentId, KxValueType.Unknown);
            data.AddParameter("Parm3", ApplicationId, KxValueType.Unknown);
            data.AddParameter("Parm4", "", KxValueType.Unknown);

            return data.ExecuteScalar().ToString();
        }

        public string DocumentExists
        {
            get
            {
                if (document != null)
                {
                    return document.RecordFound ? "Yes" : "";
                }
                else
                {
                    return "";
                }
            }
            set
            {
                document = document; // Dummy so can use mandtory option;
            }
        }

        public string DocumentFileName
        {
            get
            {
                if (document != null)
                {
                    return document.RecordFound ? document.FileName  : "";
                }
                else
                {
                    return "";
                }
            }
            set
            {
                document = document; // Dummy so can use mandtory option;
            }
        }

        public void GetStudentDocument(int kxstudentid, string _document)
        {
            document = new KxWebStudentCorrespondance(null, _document, kxstudentid);
        }

        public void GetStudentDocumentByAppGuid(Guid appGuid, string _document)
        {
            document = new KxWebStudentCorrespondance(null, _document, ApplicationGuid);
        }

        public int ProcessToKx()
        {
            if (this.Changed)
            {
                this.Save();
            }

            KxData data = new KxData("data", KxDbConfiguration.GetConfiguration("ApplicationProcessToKxProcedure", "kcf.Application_ProcessToKx"), TargetType.StoredProc, ActionType.ExecuteProcedure);
            data.AddParameter("applicationguid", ApplicationGuid.ToString(), KxValueType.String, 50);
            if (data.Execute())
            {
                this.Load();

                return 1;
            }
            else
            {
                data.LogException("KxWebStudentApplication", "Application_ProcessToKx", data.LastError, "");
            }

            return 0;
        }

        public int ProcessPaymentsToKx()
        {
            if (this.Changed)
            {
                this.Save();
            }

            KxData data = new KxData("data", KxDbConfiguration.GetConfiguration("PaymentProcessToKxProcedure", "kcf.ProcessPaymentToKxForApplication"), TargetType.StoredProc, ActionType.ExecuteProcedure);
            data.AddParameter("applicationguid", ApplicationGuid.ToString(), KxValueType.String, 50);
            data.AddParameter("depositpaymentreasonid", KxDbConfiguration.GetConfigurationInt("DepositPaymentReasonID", 0), KxValueType.Int);
            data.AddParameter("statementpaymentreasonid", KxDbConfiguration.GetConfigurationInt("StatementPaymentReasonID", 0), KxValueType.Int);
            data.AddParameter("paynowpaymentreasonid", KxDbConfiguration.GetConfigurationInt("PayNowPaymentReasonID", 0), KxValueType.Int);
            data.AddParameter("surchargepaymentreasonid", KxDbConfiguration.GetConfigurationInt("SurchargePaymentReasonID", 0), KxValueType.Int);
            data.AddParameter("surchargechargetypeid", KxDbConfiguration.GetConfigurationInt("SurchargeChargeTypeID", 0), KxValueType.Int);
            data.AddParameter("paymentmethodid", KxDbConfiguration.GetConfigurationInt("PaymentMethodID", 0), KxValueType.Int);
            data.AddParameter("webpaymentid", 0, KxValueType.Int);
            if (data.Execute())
            {
                this.Load();
                return 1;
            }
            else
            {
                data.LogException("KxWebStudentApplication", "PaymentProcessToKx", data.LastError, "");
            }

            return 0;
        }

        public void ProcessDocumentsToKx()
        {
            if (this.Changed)
            {
                this.Save();
            }

            KxData data = new KxData("data", KxDbConfiguration.GetConfiguration("ProcessDocumentsToKxProcedure", "kcf.ProcessDocumentsToKxForApplication"), TargetType.StoredProc, ActionType.ExecuteProcedure);
            data.AddParameter("applicationguid", ApplicationGuid.ToString(), KxValueType.String, 50);
            if (data.Execute())
            {
                this.Load();
            }
            else
            {
                data.LogException("KxWebStudentApplication", "ProcessDocumentsToKxProcedure", data.LastError, "");
            }
        }

        public string ValidateOneTimeUseCode(string code)
        {
            return KxOneTimeUseCode.ValidateOneTimeUseCode(code, this.ApplicationGuid, this.userGuid, new HttpContextWrapper(HttpContext.Current)).ToString();
        }
        
        public string ValidateMultipleUseCode(string code)
        {
            return KxOneTimeUseCode.ValidateMultipleUseCode(code).ToString();
        }

        public void InductionStart()
        {
            if (!IsInductionStarted)
            {
                Induction.InductionStarted = DateTime.Now;

                if (this.WebUser.HasAcceptedAllocation)
                {
                    Induction.AllocationId = this.WebUser.AcceptedAllocation.AllocationId;
                }
                else if (this.WebUser.HasPendingAllocation)
                {
                    Induction.AllocationId = this.WebUser.PendingAllocation.AllocationId;
                }

                Induction.AddAudit("Started");
                Induction.Save();
            }
        }

        public void InductionComplete()
        {
            if (!IsInductionCompleted)
            {
                Induction.InductionCompleted = DateTime.Now;
                Induction.AddAudit("Completed");
                Induction.Save();
            }
        }

    }
}

