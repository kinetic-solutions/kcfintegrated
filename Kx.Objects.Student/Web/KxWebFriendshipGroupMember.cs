#region copyright

// =======================================================
// Product:   KCF.Student 2015.4
// Project:   Kx.Objects.Student
// File:      KxWebFriendshipGroupMember.cs
// Created:   08/07/2016
// By:        Steve Ashley
// Modified:  08/12/2016
// 
// Copyright � Kinetic Solutions Ltd 2016
// All rights reserved.
// 
// No portion of this file may be re-created, copied or otherwise
// used without the express permission of Kinetic Solutions Ltd.
// =======================================================

#endregion

#region usings

using System;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;

#endregion

namespace Kx.Objects.Student.Web
{
    [Serializable]
    public class KxWebFriendshipGroupMember : KxBaseClass
    {
        #region Constructors

        public KxWebFriendshipGroupMember(KxBaseClass parent) : base(parent)
        {
            isKxClass = false;
        }

        public KxWebFriendshipGroupMember(KxBaseClass parent, int groupmemberid) : this(parent)
        {
            dBIdentifier = groupmemberid;
            Load();
        }

        public KxWebFriendshipGroupMember(KxBaseClass parent, int groupmemberid, XmlNode node)
            : this(parent, node)
        {
            dBIdentifier = groupmemberid;
        }

        public KxWebFriendshipGroupMember(KxBaseClass parent, XmlNode node)
            : this(parent)
        {
            LoadFromXml("", node);
        }

        #endregion

        #region Public Methods

        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("webfriendshipgroupmember", "WebFriendshipGroupMember", TargetType.Table);

            result.AutoKeyField = new KxDataValue("GroupMemberId", GroupMemberId, KxValueType.Int);


            if ((actiontype == ActionType.Update) && (GroupMemberId == 0))
            {
                result.Action = ActionType.Insert;
            }
            else
            {
                result.Action = actiontype;
            }

            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {
                if (_friendshipGroupId == 0)
                {
                    _friendshipGroupId = Group.FriendshipGroupId;
                    _creationDate = DateTime.Now;
                }
                result.AddParameter("friendshipGroupId", _friendshipGroupId, KxValueType.Int, false);
                result.AddParameter("creationDate", _creationDate, KxValueType.DateTime, false);
                result.AddParameter("acceptedDate", _acceptedDate, KxValueType.DateTime, true);
                result.AddParameter("rejectedDate", _rejectedDate, KxValueType.DateTime, true);
                result.AddParameter("rejectedReason", _rejectedReason, KxValueType.String, 255, true);
                result.AddParameter("memberEmailAddress", _memberEmailAddress, KxValueType.String, 255, true);
                result.AddParameter("memberforename", _memberForeName, KxValueType.String, 50, true);
                result.AddParameter("memberSurName", _memberSurName, KxValueType.String, 50, true);
                result.AddParameter("memberStudentId", _memberStudentId, KxValueType.String, 50, true);
                result.AddParameter("memberDOB", _memberDOB, KxValueType.DateTime, true);
                result.AddParameter("applicationguid", _applicationGuid, KxValueType.Guid, true);
            }
            return result;
        }

        public override void LoadFromXml(string xml, XmlNode node)
        {
            Children.Remove(_friendshipGroup);
            Children.Remove(associatedstudent);
            _friendshipGroup = null;
            associatedstudent = null;

            dBIdentifier = KxXmlUtils.GetNodeValue(node, "groupmemberid", 0);
            _friendshipGroupId = KxXmlUtils.GetNodeValue(node, "friendshipgroupid", 0);
            _creationDate = KxXmlUtils.GetNodeValueDT(node, "creationdate", null);
            _acceptedDate = KxXmlUtils.GetNodeValueDT(node, "accepteddate", null);
            _rejectedDate = KxXmlUtils.GetNodeValueDT(node, "rejecteddate", null);
            _rejectedReason = KxXmlUtils.GetNodeValue(node, "rejectedreason", "");
            _memberEmailAddress = KxXmlUtils.GetNodeValue(node, "memberemailaddress", "");
            _memberForeName = KxXmlUtils.GetNodeValue(node, "memberforename", "");
            _memberSurName = KxXmlUtils.GetNodeValue(node, "membersurname", "");
            _memberStudentId = KxXmlUtils.GetNodeValue(node, "memberstudentid", "");
            _memberDOB = KxXmlUtils.GetNodeValueDT(node, "memberdob", null);
            _applicationGuid = KxXmlUtils.GetNodeValue(node, "applicationguid", Guid.Empty);
        }

        public bool IsAGroupMember()
        {
            if (ValidStudent)
            {
                KxData test = new KxData("", "WebGroupMembership", TargetType.StoredProc);

                test.AddParameter("studentid", AssociatedStudent.StudentId, KxValueType.String);
                test.AddParameter("academicyearid", Group.AcademicYearId, KxValueType.Int);

                object result = test.ExecuteScalar();

                return result != null && Group.GroupName != (string)result;
            }
            else
                return false;
        }

        public bool IsAValidStudent()
        {
            bool result = true;

            if (!String.IsNullOrEmpty(_memberStudentId) || !String.IsNullOrEmpty(_memberSurName))
            {
                result = AssociatedStudent.KxStudentId > 0;
            }
            return result;
        }

        public string GetStudentStatus()
        {
            if (IsAValidStudent())
            {
                if (IsAGroupMember())
                {
                    return "Already a amember of another group";
                }
                else
                {
                    return "";
                }
            }
            else
            {
                return "Unidentified Student";
            }
        }

        public string GetStatus()
        {
            string result = "";

            if (AssociatedStudent != null & Group.Leader != null && Group.Leader.KxStudentId == AssociatedStudent.KxStudentId)
            {
                result = "Group Leader";
            }
            else if (!RejectedDate.HasValue && !AcceptedDate.HasValue)
            {
                result = "Invitation Pending since: " + KxDateTime.ToString(CreationDate, "d");
            }
            else if (RejectedDate.HasValue)
            {
                result = "Invitation rejected on: " + KxDateTime.ToString(RejectedDate, "d") + ", reason: " + RejectedReason;
            }
            else if (AcceptedDate.HasValue)
            {
                result = "Invitation accepted on: " + KxDateTime.ToString(AcceptedDate, "d");
            }
            return result;
        }

        public void RejectApplication()
        {
            if (_rejectedReason == "")
            {
                _rejectedReason = "Not Specified";
            }
            _rejectedDate = DateTime.Now;
            Save();
        }

        #endregion

        #region Private Methods

        private void LoadStudent()
        {
            if (!string.IsNullOrWhiteSpace(_memberStudentId))
            {
                associatedstudent = new KxStudent(this, MemberStudentId, MemberSurName);

                if (associatedstudent != null)
                {
                    _memberEmailAddress = associatedstudent.BestAddress.EmailAddress;
                    _memberSurName = associatedstudent.SurName;
                    _memberDOB = associatedstudent.Dob;
                }
            }
        }

        private void ResetStudent()
        {
            associatedstudent = null;
        }

        #endregion

        #region Attributes & Properties

        protected int _friendshipGroupId;
        protected DateTime? _creationDate;
        protected DateTime? _acceptedDate;
        protected DateTime? _rejectedDate;
        protected string _rejectedReason;
        protected string _memberEmailAddress;
        protected string _memberForeName;
        protected string _memberSurName;
        protected string _memberStudentId;
        protected DateTime? _memberDOB;
        protected Guid _applicationGuid;
        protected KxWebFriendshipGroup _friendshipGroup = null;

        protected string uIErrorMessage = "";
        protected bool emailRecipient = false;
        protected KxStudent associatedstudent = null;

        public int GroupMemberId
        {
            get { return dBIdentifier; }
        }

        public bool Existing
        {
            get { return !Temporary; }
        }

        public int FriendshipGroupId
        {
            get { return _friendshipGroupId; }
            set
            {
                AddToAudit("FriendshipGroupId", _friendshipGroupId, value);
                _friendshipGroupId = value;
            }
        }

        public KxWebFriendshipGroup Group
        {
            get { return ((KxWebFriendshipGroup)Parent.Parent); }
        }

        public KxWebFriendshipGroup FriendshipGroup
        {
            get
            {
                if (_friendshipGroup == null && _friendshipGroupId > 0)
                {
                    _friendshipGroup = new KxWebFriendshipGroup(this, _friendshipGroupId);
                }
                return _friendshipGroup;
            }
            set { _friendshipGroup = value; }
        }

        public DateTime? CreationDate
        {
            get { return _creationDate; }
            set
            {
                AddToAudit("CreationDate", _creationDate, value);
                _creationDate = value;
            }
        }

        public DateTime? AcceptedDate
        {
            get { return _acceptedDate; }
            set
            {
                AddToAudit("AcceptedDate", _acceptedDate, value);
                _acceptedDate = value;
            }
        }

        public DateTime? RejectedDate
        {
            get { return _rejectedDate; }
            set
            {
                AddToAudit("RejectedDate", _rejectedDate, value);
                _rejectedDate = value;
            }
        }

        public string RejectedReason
        {
            get { return _rejectedReason; }
            set
            {
                AddToAudit("RejectedReason", _rejectedReason, value);
                _rejectedReason = value;
            }
        }

        public string MemberEmailAddress
        {
            get { return _memberEmailAddress; }
            set
            {
                AddToAudit("MemberEmailAddress", _memberEmailAddress, value);
                _memberEmailAddress = value;
            }
        }

        public string MemberSurName
        {
            get { return _memberSurName; }
            set
            {
                if (AddToAudit("MemberSurName", _memberSurName, value))
                {
                    ResetStudent();
                }
                _memberSurName = value;
            }
        }

        public string MemberForeName
        {
            get { return _memberForeName; }
            set
            {
                if (AddToAudit("MemberForeName", _memberForeName, value))
                {
                    ResetStudent();
                }
                _memberForeName = value;
            }
        }

        public string MemberFullName
        {
            get { return string.Format("{0} {1}", MemberForeName, MemberSurName).Trim(); }
        }

        public string MemberStudentId
        {
            get { return _memberStudentId; }
            set
            {
                if (AddToAudit("MemberStudentId", _memberStudentId, value))
                {
                    ResetStudent();
                }
                _memberStudentId = value;
            }
        }

        public DateTime? MemberDOB
        {
            get { return _memberDOB; }
            set
            {
                AddToAudit("MemberDOB", _memberDOB, value);
                _memberDOB = value;
            }
        }

        public int TemporaryId
        {
            set { dBIdentifier = value; }
        }

        public string UIErrorMessage
        {
            get { return uIErrorMessage; }
            set { uIErrorMessage = value; }
        }

        public bool EmailRecipient
        {
            get { return emailRecipient; }
            set
            {
                if (emailRecipient == value)
                    return;
                emailRecipient = value;
            }
        }

        public Guid ApplicationGuid
        {
            get { return _applicationGuid; }
            set
            {
                AddToAudit("ApplicationGUID", _applicationGuid, value);
                _applicationGuid = value;
            }
        }

        public KxStudent AssociatedStudent
        {
            get
            {
                if (associatedstudent == null)
                {
                    LoadStudent();
                }
                return associatedstudent;
            }
        }

        public bool ValidStudent
        {
            get { return IsAValidStudent(); }
        }

        public bool ValidInvitee
        {
            get { return ValidStudent && !IsAGroupMember(); }
        }


        public string StudentStatus
        {
            get { return GetStudentStatus(); }
        }

        public string Status
        {
            get { return GetStatus(); }
        }

        public bool HasInvitation
        {
            get { return !AcceptedDate.HasValue && GroupMemberId > 0; }
        }

        public bool NeedsSaving
        {
            get { return (Temporary && !string.IsNullOrWhiteSpace(_memberEmailAddress)); }
        }

        #endregion
    }
}