using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Exceptions;
using Kx.Core.Utility;
using System.Threading;

namespace Kx.Objects.Student
{
    public class KxWebStudentRoomCityRates : KxBaseClass
    {
        private List<KxWebStudentRoomCityRate> items = new List<KxWebStudentRoomCityRate>();
        private int siteId;
        private int areaId;
        private int blockId;
        private int academicYearId;

        public string UserRoomFilter;
        public string ControlRoomFilter;

        public string CurrentRoomFilter
        {
            get
            {
                if (!String.IsNullOrEmpty(ControlRoomFilter))
                {
                    return ControlRoomFilter;
                }
                else
                {
                    return UserRoomFilter;
                }
            }
        }

        public List<KxWebStudentRoomCityRate> Items
        {
            get
            { return items; }
        }

        public KxWebStudentRoomCityRate this[int index]
        {
            get
            { return items[index]; }
            set
            { items[index] = value; }
        }

        public int SiteId
        {
            get
            {
                return siteId;
            }
            set
            {
                siteId = value;
            }
        }
        public int AreaId
        {
            get
            {
                return areaId;
            }
            set
            {
                areaId = value;
            }
        }
        public int BlockId
        {
            get
            {
                return blockId;
            }
            set
            {
                blockId = value;
            }
        }
        public int AcademicYearId
        {
            get
            {
                return academicYearId;
            }
            set
            {
                academicYearId = value;
            }
        }

        public string[] RoomList
        {
            get
            {
                return GetRoomList();
            }
        }

        public string[] FilteredRoomList
        {
            get
            {
                return GetFilteredRoomList();
            }
        }


        public bool IsFiltered
        {
            get
            {
                return !String.IsNullOrEmpty(CurrentRoomFilter);   
            }
        }

           public KxWebStudentRoomCityRates(KxBaseClass parent, int academicyearid,int siteid,int areaid, int blockid)
            : base(parent)
        {
            AcademicYearId = academicyearid;
            this.areaId = areaid;
            SiteId = siteid;
            BlockId = blockid;
            Load();
        }


        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = null;

            if (actiontype == ActionType.Select)
            {
                result = new KxData("webstudentroomratescity", "WebStudentRoomRatesCityLiving", TargetType.StoredProc, ActionType.Select);

                result.Parameters.Add(new KxDataValue("academicyearid", AcademicYearId, KxValueType.Int));
                result.Parameters.Add(new KxDataValue("siteid", SiteId, KxValueType.Int));
                result.Parameters.Add(new KxDataValue("Language", Thread.CurrentThread.CurrentCulture.Name, KxValueType.String));
            }
            return result;
        }

        public override bool LoadFromXml(string xml, string xpath)
        {
            items.Clear();
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xml);

                XmlNodeList nodes = doc.SelectNodes(@"//results/" + xpath + "/datarow");

                foreach (XmlNode node in nodes)
                {
                    if (node != null)
                    {
                        KxWebStudentRoomCityRate item = new KxWebStudentRoomCityRate(this, node);
                        items.Add(item);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "LoadFromXml", ex.Message, xpath);
                LastError = ex.Message;
                return false;
            }
        }

        public KxWebStudentRoomCityRate GetRoomByUniqueId(string id)
        {
            KxWebStudentRoomCityRate result = null;

            foreach (KxWebStudentRoomCityRate lrr in Items)
            {
                if (lrr.UniqueId == id)
                {
                    result = lrr;
                    break;
                }
            }

            return result;
        }

        protected string[] GetRoomList()
        {
            List<string> result = new List<string>();

            foreach (KxWebStudentRoomCityRate lrr in Items)
            {
                result.Add(lrr.UniqueId);
            }

            return result.ToArray();
        }

        protected string[] GetFilteredRoomList()
        {
            List<string> result = new List<string>();

            if (IsFiltered)
            {
                string[] filter = CurrentRoomFilter.Split(new char[]{'A'}, StringSplitOptions.RemoveEmptyEntries);

                int roomid = 0;
                int minweek = 0;
                int maxweek = 0;

                if (filter.Length == 2)
                {
                    roomid = Convert.ToInt32(filter[0]);

                    switch (filter[1])
                    {
                        case "3": minweek = 0; maxweek = 20; break;
                        case "5": minweek = 21; maxweek = 22; break;
                        case "6": minweek = 23; maxweek = 40; break;
                        case "10": minweek = 41; maxweek = 48; break;
                        case "12": minweek = 49; maxweek = 100; break;
                    }
                }

                foreach (KxWebStudentRoomCityRate lrr in Items)
                {
                    if (lrr.BedroomTypeId == roomid && lrr.NoWeeks >= minweek && lrr.NoWeeks <= maxweek)
                    {
                        result.Add(lrr.UniqueId);
                    }
                }
            }

            return result.ToArray();
        }
    }
}

