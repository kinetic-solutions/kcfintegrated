using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;

namespace Kx.Objects.Student.Web
{
    [Serializable]
    public class KxLocationContent : KxBaseClass
    {
        #region Attributes & Properties

        protected int _areaId;
        protected int _bedroomTypeId;
        protected string _features;
        protected string _images;
        protected string _facilities;
        protected string _moreInfo;
        protected int _blockId;
        protected int _siteId;
        protected string _latitude;
        protected string _longitude;

        public int LocationContentId
        {
            get { return base.dBIdentifier; }
        }

        public int AreaId
        {
            get { return _areaId; }
            set
            { 
                AddToAudit("AreaId", _areaId, value);
                _areaId = value;
            }
        }

        public int BedroomTypeId
        {
            get { return _bedroomTypeId; }
            set
            { 
                AddToAudit("BedroomTypeId", _bedroomTypeId, value);
                _bedroomTypeId = value;
            }
        }

        public string Features
        {
            get { return _features; }
            set
            { 
                AddToAudit("Features", _features, value);
                _features = value;
            }
        }

        public string Images
        {
            get { return _images; }
            set
            { 
                AddToAudit("Images", _images, value);
                _images = value;
            }
        }

        public string Facilities
        {
            get { return _facilities; }
            set
            { 
                AddToAudit("Facilities", _facilities, value);
                _facilities = value;
            }
        }

        public string MoreInfo
        {
            get { return _moreInfo; }
            set
            { 
                AddToAudit("MoreInfo", _moreInfo, value);
                _moreInfo = value;
            }
        }

        public int BlockId
        {
            get { return _blockId; }
            set
            { 
                AddToAudit("BlockId", _blockId, value);
                _blockId = value;
            }
        }

        public int SiteId
        {
            get { return _siteId; }
            set
            { 
                AddToAudit("SiteId", _siteId, value);
                _siteId = value;
            }
        }

        public string Latitude
        {
            get { return _latitude; }
            set
            {
                AddToAudit("Latitude", _latitude, value);
                _latitude = value;
            }
        }
        public string Longitude
        {
            get { return _longitude; }
            set
            {
                AddToAudit("Longitude", _longitude, value);
                _longitude = value;
            }
        }

        #endregion 


        public KxLocationContent(KxBaseClass parent):base(parent)
        { }


        public KxLocationContent(KxBaseClass parent, XmlNode node):base(parent)
        {
             LoadFromXml("", node);
        }

        public KxLocationContent(KxBaseClass parent,int  locationcontentid):base(parent)
        {
             base.dBIdentifier = locationcontentid;
             Load();
        }


        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("locationcontent", "dbo.LocationContent", TargetType.Table);

            result.AutoKeyField = new KxDataValue("LocationContentId",LocationContentId,KxValueType.Int);


            if ((actiontype == ActionType.Update) && (LocationContentId == 0))
            {
                result.Action = ActionType.Insert;
            }
            else
            {
                result.Action = actiontype;
            }

            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {
                result.AddParameter("areaId", _areaId, KxValueType.Int, true);
                result.AddParameter("bedroomTypeId", _bedroomTypeId, KxValueType.Int, true);
                result.AddParameter("features", _features, KxValueType.String, 500, true);
                result.AddParameter("images", _images, KxValueType.String, 2147483647, true);
                result.AddParameter("facilities", _facilities, KxValueType.String, 2147483647, true);
                result.AddParameter("moreInfo", _moreInfo, KxValueType.String, 2147483647, true);
                result.AddParameter("blockId", _blockId, KxValueType.Int, true);
                result.AddParameter("siteId", _siteId, KxValueType.Int, true);
                result.AddParameter("latitude", _latitude, KxValueType.String, 50, true);
                result.AddParameter("longitude", _longitude, KxValueType.String, 50, true);
            }
            return result;
        }


        public override  void LoadFromXml(string xml,XmlNode node)
        {
            dBIdentifier  = KxXmlUtils.GetNodeValue(node,"locationcontentid",0);
            _areaId  = KxXmlUtils.GetNodeValue(node,"areaid",0);
            _bedroomTypeId  = KxXmlUtils.GetNodeValue(node,"bedroomtypeid",0);
            _features  = KxXmlUtils.GetNodeValue(node,"features",string.Empty);
            _images  = KxXmlUtils.GetNodeValue(node,"images",string.Empty);
            _facilities  = KxXmlUtils.GetNodeValue(node,"facilities",string.Empty);
            _moreInfo  = KxXmlUtils.GetNodeValue(node,"moreinfo",string.Empty);
            _blockId  = KxXmlUtils.GetNodeValue(node,"blockid",0);
            _siteId  = KxXmlUtils.GetNodeValue(node,"siteid",0);
            _latitude  = KxXmlUtils.GetNodeValue(node,"latitude",string.Empty);
            _longitude  = KxXmlUtils.GetNodeValue(node,"longitude",string.Empty);
        }

        public static IEnumerable<KxLocationContent> GetLocationContent (int siteId, int areaId, int blockId, int bedroomTypeId)
        {
            string sql = @"SELECT * FROM dbo.LocationContent WHERE COALESCE(SiteID, @SiteID) = @SiteID
                            AND COALESCE(AreaID, @AreaID) = @AreaID
                            AND COALESCE(BlockID, @BlockID) = @BlockID
                            AND COALESCE(BedroomTypeID, @BedroomTypeID) = @BedroomTypeID
                            ORDER BY
                                CASE WHEN SiteID = @SiteID THEN 1 ELSE 0 END
                            +   CASE WHEN AreaID = @AreaID THEN 1 ELSE 0 END
                            +   CASE WHEN BlockID = @BlockID THEN 1 ELSE 0 END
                            +   CASE WHEN BedroomTypeID = @BedroomTypeID THEN 1 ELSE 0 END
                                DESC
                            ";

            KxData data = new KxData("locationcontent", sql, TargetType.Static);
            data.AddParameter("siteid", siteId, KxValueType.Int);
            data.AddParameter("areaid", areaId, KxValueType.Int);
            data.AddParameter("blockid", blockId, KxValueType.Int);
            data.AddParameter("bedroomtypeid", bedroomTypeId, KxValueType.Int);
            if (data.Execute ())
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(data.XmlResults);
                XmlNodeList nodes = doc.SelectNodes(@"//locationcontent/datarow");
                foreach(XmlNode node in nodes)
                {
                     yield return new KxLocationContent(null, node);
                }
            }
        }
    }
}

