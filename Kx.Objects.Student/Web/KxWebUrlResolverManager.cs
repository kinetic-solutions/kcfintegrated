using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Exceptions;
using Kx.Core.Utility;
using Kx.Objects.Student;
using Kx.Core.Web.Configuration;

namespace Kx.Objects.Student.Web
{
    public class KxWebUrlResolverManager : KxBaseClass
    {
        private List<KxWebUrlResolver> items = new List<KxWebUrlResolver>();

        public List<KxWebUrlResolver> Items
        {
            get
            { return items; }
        }

        public KxWebUrlResolver this[int index]
        {
            get
            { return items[index]; }
            set
            { items[index] = value; }
        }

        public KxWebUrlResolverManager()
        {
            Load();
        }

        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = null;

            if (actiontype == ActionType.Select)
            {
                result = new KxData("weburlresolver", "WebUrlResolver", TargetType.Table, ActionType.Select);
            }
            return result;
        }

        public override bool LoadFromXml(string xml, string xpath)
        {
            items.Clear();
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xml);

                XmlNodeList nodes = doc.SelectNodes(@"//results/" + xpath + "/datarow");

                foreach (XmlNode node in nodes)
                {
                    if (node != null)
                    {
                        KxWebUrlResolver item = new KxWebUrlResolver(this, node);
                        items.Add(item);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "LoadFromXml", ex.Message, xpath);
                LastError = ex.Message;
                return false;
            }
        }

        public KxWebUrlResolver GetWebUrlResolver(int id)
        {
            KxWebUrlResolver result = null;

            foreach (KxWebUrlResolver i in Items)
            {
                if (i.UrlResolverId == id)
                {
                    result = i;
                    break;
                }
            }
            return result;
        }

        public KxWebUrlResolver GetWebUrlResolver(KxStudentAllocation alloc)
        {
            Boolean bBlockMatch;
            KxWebUrlResolver result = null;
			String BlockID = String.Format(",{0},", alloc.ResidentialRoom.SubBlockId);

            if (KxConfigurationManager.Profiling)
            {
                String licenceTemplateID = (alloc.LicenceTemplateId == 0) ? alloc.StandardRoomPricing.LicenceWordTemplateId.ToString() : alloc.LicenceTemplateId.ToString();

				KxLogger.LogActivity(this, "GetWebUrlResolver",
					"AcademicyearID=" + alloc.Application.AcademicYearId.ToString() + " : " +
                    "LicenceTemplateID=" + licenceTemplateID + " : " +
					"AreaID=" + alloc.ResidentialRoom.Block.AreaId.ToString() + " : " +
					"BlockID=" + alloc.ResidentialRoom.BlockId.ToString() + " : " +
					"SubBlockID=" + alloc.ResidentialRoom.SubBlockId.ToString() + " (" + BlockID + ")", "");
			}

			foreach (KxWebUrlResolver item in Items)
            {
                Int32 licenceTemplateID = (alloc.LicenceTemplateId == 0) ? alloc.StandardRoomPricing.LicenceWordTemplateId : alloc.LicenceTemplateId;
                bBlockMatch = (item.BlockIds == String.Empty || item.BlockIds.Contains(BlockID));

                if (item.AcademicYearId == alloc.Application.AcademicYearId
                    && (item.WordTemplateId == 0 || item.WordTemplateId == licenceTemplateID)
                    && (item.AreaId == 0 || item.AreaId == alloc.ResidentialRoom.Block.AreaId)
					&& (bBlockMatch))
                {
                    result = item;

                    if (KxConfigurationManager.Profiling)
                    {
                        KxLogger.LogActivity(this, "GetWebUrlResolver", "Matched", "");
                    }

                    break;
                }
            }

			return result;
        }
    }
}

