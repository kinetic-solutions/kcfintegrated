using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;

namespace Kx.Objects.Student.Web
{
    [Serializable]
    public class KxPreferenceAvailability : KxBaseClass
    {
        #region Attributes & Properties

        protected int _applicationTypeId;
        protected int _academicYearId;
        protected string _filter1;
        protected string _filter2;
        protected string _filter3;
        protected string _filter4;
        protected int _siteId;
        protected int _areaId;
        protected int _blockId;
        protected int _bedroomTypeId;
        protected int _singleSexArea;
        protected int _smoking;
        protected int _customField1;
        protected int _customField2;
        protected string _customValue1;
        protected string _customValue2;
        protected string _customValue3;
        protected string _customValue4;
        protected string _customValue5;
        protected string _customValue6;
        protected string _customValue7;
        protected string _customValue8;
        protected string _siteName;
        protected string _areaName;
        protected string _blockName;
        protected string _bedroomTypeName;
        protected decimal _price;
        protected string _thumbnail;
        protected string _extraText1;
        protected string _extraText2;
        protected decimal _extraMoney1;
        protected int _extraInt1;
        protected int _allocationModelId;
        protected int _pricingModelId;
        protected string _contentResourceId;
        protected bool _inactive;

        public int PreferenceAvailabilityId
        {
            get { return base.dBIdentifier; }
        }

        public int ApplicationTypeId
        {
            get { return _applicationTypeId; }
            set
            { 
                AddToAudit("ApplicationTypeId", _applicationTypeId, value);
                _applicationTypeId = value;
            }
        }

        public int AcademicYearId
        {
            get { return _academicYearId; }
            set
            { 
                AddToAudit("AcademicYearId", _academicYearId, value);
                _academicYearId = value;
            }
        }

        public string Filter1
        {
            get { return _filter1; }
            set
            { 
                AddToAudit("Filter1", _filter1, value);
                _filter1 = value;
            }
        }

        public string Filter2
        {
            get { return _filter2; }
            set
            { 
                AddToAudit("Filter2", _filter2, value);
                _filter2 = value;
            }
        }

        public string Filter3
        {
            get { return _filter3; }
            set
            { 
                AddToAudit("Filter3", _filter3, value);
                _filter3 = value;
            }
        }

        public string Filter4
        {
            get { return _filter4; }
            set
            { 
                AddToAudit("Filter4", _filter4, value);
                _filter4 = value;
            }
        }

        public int SiteId
        {
            get { return _siteId; }
            set
            { 
                AddToAudit("SiteId", _siteId, value);
                _siteId = value;
            }
        }

        public int AreaId
        {
            get { return _areaId; }
            set
            { 
                AddToAudit("AreaId", _areaId, value);
                _areaId = value;
            }
        }

        public int BlockId
        {
            get { return _blockId; }
            set
            { 
                AddToAudit("BlockId", _blockId, value);
                _blockId = value;
            }
        }

        public int BedroomTypeId
        {
            get { return _bedroomTypeId; }
            set
            { 
                AddToAudit("BedroomTypeId", _bedroomTypeId, value);
                _bedroomTypeId = value;
            }
        }

        public int SingleSexArea
        {
            get { return _singleSexArea; }
            set
            { 
                AddToAudit("SingleSexArea", _singleSexArea, value);
                _singleSexArea = value;
            }
        }

        public int Smoking
        {
            get { return _smoking; }
            set
            { 
                AddToAudit("Smoking", _smoking, value);
                _smoking = value;
            }
        }

        public int CustomField1
        {
            get { return _customField1; }
            set
            { 
                AddToAudit("CustomField1", _customField1, value);
                _customField1 = value;
            }
        }

        public int CustomField2
        {
            get { return _customField2; }
            set
            { 
                AddToAudit("CustomField2", _customField2, value);
                _customField2 = value;
            }
        }

        public string CustomValue1
        {
            get { return _customValue1; }
            set
            { 
                AddToAudit("CustomValue1", _customValue1, value);
                _customValue1 = value;
            }
        }

        public string CustomValue2
        {
            get { return _customValue2; }
            set
            { 
                AddToAudit("CustomValue2", _customValue2, value);
                _customValue2 = value;
            }
        }

        public string CustomValue3
        {
            get { return _customValue3; }
            set
            { 
                AddToAudit("CustomValue3", _customValue3, value);
                _customValue3 = value;
            }
        }

        public string CustomValue4
        {
            get { return _customValue4; }
            set
            { 
                AddToAudit("CustomValue4", _customValue4, value);
                _customValue4 = value;
            }
        }

        public string CustomValue5
        {
            get { return _customValue5; }
            set
            {
                AddToAudit("CustomValue5", _customValue5, value);
                _customValue5 = value;
            }
        }

        public string CustomValue6
        {
            get { return _customValue6; }
            set
            {
                AddToAudit("CustomValue6", _customValue6, value);
                _customValue6 = value;
            }
        }

        public string CustomValue7
        {
            get { return _customValue7; }
            set
            {
                AddToAudit("CustomValue7", _customValue7, value);
                _customValue7 = value;
            }
        }

        public string CustomValue8
        {
            get { return _customValue8; }
            set
            {
                AddToAudit("CustomValue8", _customValue8, value);
                _customValue8 = value;
            }
        }

        public string SiteName
        {
            get { return _siteName; }
            set
            { 
                AddToAudit("SiteName", _siteName, value);
                _siteName = value;
            }
        }

        public string AreaName
        {
            get { return _areaName; }
            set
            { 
                AddToAudit("AreaName", _areaName, value);
                _areaName = value;
            }
        }

        public string BlockName
        {
            get { return _blockName; }
            set
            { 
                AddToAudit("BlockName", _blockName, value);
                _blockName = value;
            }
        }

        public string BedroomTypeName
        {
            get { return _bedroomTypeName; }
            set
            { 
                AddToAudit("BedroomTypeName", _bedroomTypeName, value);
                _bedroomTypeName = value;
            }
        }

        public decimal Price
        {
            get { return _price; }
            set
            { 
                AddToAudit("Price", _price, value);
                _price = value;
            }
        }

        public string Thumbnail
        {
            get { return _thumbnail; }
            set
            { 
                AddToAudit("Thumbnail", _thumbnail, value);
                _thumbnail = value;
            }
        }

        public string ExtraText1
        {
            get { return _extraText1; }
            set
            { 
                AddToAudit("ExtraText1", _extraText1, value);
                _extraText1 = value;
            }
        }

        public string ExtraText2
        {
            get { return _extraText2; }
            set
            { 
                AddToAudit("ExtraText2", _extraText2, value);
                _extraText2 = value;
            }
        }

        public decimal ExtraMoney1
        {
            get { return _extraMoney1; }
            set
            { 
                AddToAudit("ExtraMoney1", _extraMoney1, value);
                _extraMoney1 = value;
            }
        }

        public int ExtraInt1
        {
            get { return _extraInt1; }
            set
            { 
                AddToAudit("ExtraInt1", _extraInt1, value);
                _extraInt1 = value;
            }
        }

        public int AllocationModelId
        {
            get { return _allocationModelId; }
            set
            { 
                AddToAudit("AllocationModelId", _allocationModelId, value);
                _allocationModelId = value;
            }
        }

        public int PricingModelId
        {
            get { return _pricingModelId; }
            set
            { 
                AddToAudit("PricingModelId", _pricingModelId, value);
                _pricingModelId = value;
            }
        }

        public string ContentResourceId
        {
            get { return _contentResourceId; }
            set
            { 
                AddToAudit("ContentResourceId", _contentResourceId, value);
                _contentResourceId = value;
            }
        }

        public bool Inactive
        {
            get { return _inactive; }
            set
            { 
                AddToAudit("Inactive", _inactive, value);
                _inactive = value;
            }
        }

        public DateTime? LatestDepartureDate { get; protected set; }
        public DateTime? EarliestArrivalDate { get; protected set; }


        #endregion


        public KxPreferenceAvailability(KxBaseClass parent):base(parent)
        { }


        public KxPreferenceAvailability(KxBaseClass parent, XmlNode node):base(parent)
        {
             LoadFromXml("", node);
        }

        public KxPreferenceAvailability(KxBaseClass parent,int  preferenceavailabilityid):base(parent)
        {
             base.dBIdentifier = preferenceavailabilityid;
             Load();
        }


        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("preferenceavailability", "PreferenceAvailability", TargetType.Table);

            result.AutoKeyField = new KxDataValue("PreferenceAvailabilityId",PreferenceAvailabilityId,KxValueType.Int);


            if ((actiontype == ActionType.Update) && (PreferenceAvailabilityId == 0))
            {
                result.Action = ActionType.Insert;
            }
            else
            {
                result.Action = actiontype;
            }

            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {
                result.AddParameter("applicationTypeId", _applicationTypeId, KxValueType.Int, false);
                result.AddParameter("academicYearId", _academicYearId, KxValueType.Int, false);
                result.AddParameter("filter1", _filter1, KxValueType.String, 50, true);
                result.AddParameter("filter2", _filter2, KxValueType.String, 50, true);
                result.AddParameter("filter3", _filter3, KxValueType.String, 50, true);
                result.AddParameter("filter4", _filter4, KxValueType.String, 50, true);
                result.AddParameter("siteId", _siteId, KxValueType.Int, true);
                result.AddParameter("areaId", _areaId, KxValueType.Int, true);
                result.AddParameter("blockId", _blockId, KxValueType.Int, true);
                result.AddParameter("bedroomTypeId", _bedroomTypeId, KxValueType.Int, true);
                result.AddParameter("singleSexArea", _singleSexArea, KxValueType.Int, true);
                result.AddParameter("smoking", _smoking, KxValueType.Int, true);
                result.AddParameter("customField1", _customField1, KxValueType.Int, true);
                result.AddParameter("customField2", _customField2, KxValueType.Int, true);
                result.AddParameter("customValue1", _customValue1, KxValueType.String, 50, true);
                result.AddParameter("customValue2", _customValue2, KxValueType.String, 50, true);
                result.AddParameter("customValue3", _customValue3, KxValueType.String, 50, true);
                result.AddParameter("customValue4", _customValue4, KxValueType.String, 50, true);
                result.AddParameter("customValue5", _customValue5, KxValueType.String, 50, true);
                result.AddParameter("customValue6", _customValue6, KxValueType.String, 50, true);
                result.AddParameter("customValue7", _customValue7, KxValueType.String, 50, true);
                result.AddParameter("customValue8", _customValue8, KxValueType.String, 50, true);
                result.AddParameter("siteName", _siteName, KxValueType.String, 50, true);
                result.AddParameter("areaName", _areaName, KxValueType.String, 50, true);
                result.AddParameter("blockName", _blockName, KxValueType.String, 50, true);
                result.AddParameter("bedroomTypeName", _bedroomTypeName, KxValueType.String, 50, true);
                result.AddParameter("price", _price, KxValueType.Decimal, true);
                result.AddParameter("thumbnail", _thumbnail, KxValueType.String, 2147483647, true);
                result.AddParameter("extraText1", _extraText1, KxValueType.String, 255, true);
                result.AddParameter("extraText2", _extraText2, KxValueType.String, 255, true);
                result.AddParameter("extraMoney1", _extraMoney1, KxValueType.Decimal, true);
                result.AddParameter("extraInt1", _extraInt1, KxValueType.Int, true);
                result.AddParameter("allocationModelId", _allocationModelId, KxValueType.Int, true);
                result.AddParameter("pricingModelId", _pricingModelId, KxValueType.Int, true);
                result.AddParameter("contentResourceId", _contentResourceId, KxValueType.String, 50, true);
                result.AddParameter("inactive", _inactive, KxValueType.Boolean, false);
            }
            return result;
        }


        public override  void LoadFromXml(string xml,XmlNode node)
        {
            dBIdentifier  = KxXmlUtils.GetNodeValue(node,"preferenceavailabilityid",0);
            _applicationTypeId  = KxXmlUtils.GetNodeValue(node,"applicationtypeid",0);
            _academicYearId  = KxXmlUtils.GetNodeValue(node,"academicyearid",0);
            _filter1  = KxXmlUtils.GetNodeValue(node,"filter1",string.Empty);
            _filter2  = KxXmlUtils.GetNodeValue(node,"filter2",string.Empty);
            _filter3  = KxXmlUtils.GetNodeValue(node,"filter3",string.Empty);
            _filter4  = KxXmlUtils.GetNodeValue(node,"filter4",string.Empty);
            _siteId  = KxXmlUtils.GetNodeValue(node,"siteid",0);
            _areaId  = KxXmlUtils.GetNodeValue(node,"areaid",0);
            _blockId  = KxXmlUtils.GetNodeValue(node,"blockid",0);
            _bedroomTypeId  = KxXmlUtils.GetNodeValue(node,"bedroomtypeid",0);
            _singleSexArea  = KxXmlUtils.GetNodeValue(node,"singlesexarea",0);
            _smoking  = KxXmlUtils.GetNodeValue(node,"smoking",0);
            _customField1  = KxXmlUtils.GetNodeValue(node,"customfield1",0);
            _customField2  = KxXmlUtils.GetNodeValue(node,"customfield2",0);
            _customValue1  = KxXmlUtils.GetNodeValue(node,"customvalue1",string.Empty);
            _customValue2  = KxXmlUtils.GetNodeValue(node,"customvalue2",string.Empty);
            _customValue3  = KxXmlUtils.GetNodeValue(node,"customvalue3",string.Empty);
            _customValue4  = KxXmlUtils.GetNodeValue(node,"customvalue4",string.Empty);
            _customValue5 = KxXmlUtils.GetNodeValue(node, "customvalue5", string.Empty);
            _customValue6 = KxXmlUtils.GetNodeValue(node, "customvalue6", string.Empty);
            _customValue7 = KxXmlUtils.GetNodeValue(node, "customvalue7", string.Empty);
            _customValue8 = KxXmlUtils.GetNodeValue(node, "customvalue8", string.Empty);
            _siteName  = KxXmlUtils.GetNodeValue(node,"sitename",string.Empty);
            _areaName  = KxXmlUtils.GetNodeValue(node,"areaname",string.Empty);
            _blockName  = KxXmlUtils.GetNodeValue(node,"blockname",string.Empty);
            _bedroomTypeName  = KxXmlUtils.GetNodeValue(node,"bedroomtypename",string.Empty);
            _price  = KxXmlUtils.GetNodeValue(node,"price",(decimal)0.00M);
            _thumbnail  = KxXmlUtils.GetNodeValue(node,"thumbnail",string.Empty);
            _extraText1  = KxXmlUtils.GetNodeValue(node,"extratext1",string.Empty);
            _extraText2  = KxXmlUtils.GetNodeValue(node,"extratext2",string.Empty);
            _extraMoney1  = KxXmlUtils.GetNodeValue(node,"extramoney1",(decimal)0.00M);
            _extraInt1  = KxXmlUtils.GetNodeValue(node,"extraint1",0);
            _allocationModelId  = KxXmlUtils.GetNodeValue(node,"allocationmodelid",0);
            _pricingModelId  = KxXmlUtils.GetNodeValue(node,"pricingmodelid",0);
            _contentResourceId  = KxXmlUtils.GetNodeValue(node,"contentresourceid",string.Empty);
            _inactive  = KxXmlUtils.GetNodeValue(node,"inactive",false);

            EarliestArrivalDate = KxXmlUtils.GetNodeValueDT(node, "earliestarrivaldate", null);
            LatestDepartureDate = KxXmlUtils.GetNodeValueDT(node, "latestdeparturedate", null);

        }

        public static IEnumerable<KxPreferenceAvailability> GetPreferenceAvailability ()
        {
            KxData data = new KxData("preferenceavailability", "PreferenceAvailability", TargetType.Table);
            if (data.Execute ())
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(data.XmlResults);
                XmlNodeList nodes = doc.SelectNodes(@"//preferenceavailability/datarow");
                foreach(XmlNode node in nodes)
                {
                     yield return new KxPreferenceAvailability(null, node);
                }
            }
        }
    }
}

