using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Exceptions;
using Kx.Core.Utility;
using Kx.Objects.Student;

namespace Kx.Objects.Student.Web
{

    public enum KXAddressTypes { Current, Emergency, Home, Email,Sponsor };

    public class KxWebStudentAddresses : KxBaseClass
    {
        private Guid applicationGuid  = Guid.Empty;
        private List<KxWebStudentAddress> items = new List<KxWebStudentAddress>();

        public List<KxWebStudentAddress> Items
        {
            get
            { return items; }
        }

        public KxWebStudentAddress this[int index]
        {
            get
            { return items[index]; }
            set
            { items[index] = value; }
        }

        public override KxData ConfigureData(ActionType actiontype)
        {
            if (parent != null)
            {
                guidIdentifier = parent.GuidIdentifier;
            }
            KxData result = null;

            if (actiontype == ActionType.Select)
            {
                result = new KxData("webstudentaddresses", "WebStudentAddresses", TargetType.StoredProc,ActionType.Select);

                result.KeyFields.Add(new KxDataValue("ApplicationGuid", GuidIdentifier, KxValueType.Guid));
            }
            return result;
        }

        public KxWebStudentAddress AddAddress()
        {
            KxWebStudentAddress addr = new KxWebStudentAddress(this);
            Items.Add(addr);
            return addr;
        }

        public KxWebStudentAddresses(KxBaseClass parent, Guid applicationGuid)
            : base(parent)
        {
            guidIdentifier = applicationGuid;
            Load();
        }

        public KxWebStudentAddresses(KxBaseClass parent)
            : base(parent)
        {
        }

        public override bool LoadFromXml(string xml, string xpath)
        {
            items.Clear();
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xml);

                XmlNodeList nodes = doc.SelectNodes(@"//results/" + xpath + "/datarow");

                foreach (XmlNode node in nodes)
                {
                    if (node != null)
                    {
                        int applicationId = KxXmlUtils.GetNodeValue(node, "addressid", 0);
                        if (applicationId != 0)
                        {
                            KxWebStudentAddress address = new KxWebStudentAddress(this, applicationId, node);
                            Items.Add(address);
                        }
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "LoadFromXml", ex.Message, xpath);
                LastError = ex.Message;
                return false;
            }
        }
    

        public KxWebStudentAddress GetApplicationAddress(int idx,KXAddressTypes addrType)
        {
            KxWebStudentAddress result = GetAddressByType(idx);
            try
            {
                if (result == null)
                {
                    result = new KxWebStudentAddress(this);
                    result.AddressTypeId = idx;
                    items.Add(result);

                    if (Parent != null)
                    {
                        if (Parent.Parent != null)
                        {
                            if (Parent.Parent.Parent != null)
                            {
                                if (Parent.Parent.Parent is KxWebUserStudent)
                                {
                                    KxWebUserStudent user = ((KxWebUserStudent)Parent.Parent.Parent);

                                    if (user.Student != null)
                                    {
                                        KxStudentAddress addr = null;

                                        if (addrType == KXAddressTypes.Email)
                                        {
                                            addr = user.Student.PreferredEmailAddress;
                                        }
                                        else if (addrType == KXAddressTypes.Current)
                                        {
                                            addr = user.Student.CurrentAddress;
                                        }
                                        else if (addrType == KXAddressTypes.Home)
                                        {
                                            addr = user.Student.HomeAddress;
                                        }
                                        else if (addrType == KXAddressTypes.Emergency)
                                        {
                                            addr = user.Student.EmergencyAddress;
                                        }

                                        
                                        if (addr != null)
                                        {
                                            bool blankAddress = false;

                                            try
                                            {
                                                blankAddress = Boolean.Parse(System.Configuration.ConfigurationManager.AppSettings["BlankAddress"]);
                                            }
                                            catch
                                            {}

                                            if (!blankAddress)
                                            {
                                                result.CopyStudentAddress(addr);
                                            }
                                        }

                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch { }
            return result;
        }

        public override void NotifyDelete(KxBaseClass child)
        {
            KxWebStudentAddress member = (KxWebStudentAddress)child;
            if (Items.Contains(member))
            {
                Items.Remove(member);
            }
        }

        public KxWebStudentAddress GetAddressByType(int idx)
        {
            KxWebStudentAddress result = null;

            foreach (KxWebStudentAddress address in items)
            {
                if (address.AddressTypeId == idx)
                {
                    result = address;
                    break;
                }
            }
            return result;
        }
    }
}
