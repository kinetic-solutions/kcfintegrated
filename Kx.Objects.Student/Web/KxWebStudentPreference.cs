using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;
using Kx.Objects.Common;

namespace Kx.Objects.Student.Web
{
    public enum SelectedType { Unselected, Selected, Hidden }
    [Serializable]
    public partial class KxWebStudentPreference : KxBaseClass, IComparable<KxWebStudentPreference>
    {
        #region Attributes & Properties

        protected Guid     applicationGuid;
        protected int      preferenceNumber;
        protected string siteName = "";
        protected int      siteId;
        protected int      areaId;
        protected string areaName = "";
        protected int      blockId;
        protected string blockName = "";
        protected string   blockUrl = "";
        protected int      bedroomTypeId;
        protected string   bedroomTypeName="";
        protected int      floor;
        protected int      singleSexArea;
        protected int      smoking;
        protected int      customField1;
        protected int      customField2;
        protected string   customValue1="";
        protected string   customValue2="";
        protected string   customValue3="";
        protected string   customValue4="";
        protected string   customValue5 = "";
        protected string   customValue6 = "";
        protected string   customValue7 = "";
        protected string   customValue8 = "";
        protected DateTime ?creationDate;
        protected int      originalDescription;
        protected int     availableCount = 0;
        protected string bedroomWebDescription = "";
        protected bool ensuite = false;

        protected object uIObject = null;
        protected SelectedType selectionType = SelectedType.Unselected; // used for Preference searches - not held in db
        protected KxArea area = null;
        protected KxSiteBlock block = null;
        protected KxSite site = null;
        protected string  searchDescription; // used for Preference searches - not held in db

        protected KxBedroomType bedroomType = null;
       
        public int      PreferenceId
        {
            get
            { return base.dBIdentifier; }
            set  //Kludge...
            { dBIdentifier = value; }
        }
        public Guid     ApplicationGuid
        {
            get
            { return applicationGuid;}
            set
            { 
                AddToAudit("ApplicationGuid", applicationGuid, value);
                applicationGuid = value;
            }
        }
        public int      PreferenceNumber
        {
            get
            { return preferenceNumber;}
            set
            { 
                AddToAudit("PreferenceNumber", preferenceNumber, value);
                preferenceNumber = value;
            }
        }
        public int      SiteId
        {
            get
            { return siteId;}
            set
            { 
                AddToAudit("SiteId", siteId, value);
                if (siteId != value)
                {
                    Children.Remove(site);
                    site = null;
                }
                siteId = value;
            }
        }
        public int      AreaId
        {
            get
            { return areaId;}
            set
            { 
                AddToAudit("AreaId", areaId, value);
                if (areaId != value)
                {
                    areaName = "";
                }
                areaId = value;
            }
        }

        public KxArea Area
        {
            get
            {

                if ((areaId > 0 && area == null))
                {
                    area = new KxArea(this, areaId);
                }
                return area;
            }
        }

        public string AreaUrl
        {
            get
            {
                if (Area != null)
                {
                    return area.AreaUrl;
                }
                else
                {
                    return "";
                }
            }
        }

        public int      BlockId
        {
            get
            { return blockId;}
            set
            { 
                AddToAudit("BlockId", blockId, value);
                if (blockId != value)
                {
                    blockName = "";
                }
                blockId = value;
            }
        }

        public KxSiteBlock Block
        {
            get
            {
               
                if ((blockId > 0 && block == null) )
                {
                    block = new KxSiteBlock(this, blockId);
                }
                return block;
            }
        }

        public string BlockUrl
        {
            get
            {
                if (Block != null)
                {
                    blockUrl = Block.BlockUrl;
                }
                return blockUrl;
            }
            set
            { blockUrl = value; }

        }

        public string BlockUrlString1
        {
            get
            {
                if (Block != null)
                {
                    blockUrl = Block.BlockUrlString1;
                }
                return blockUrl;
            }
            set
            { blockUrl = value; }

        }

        private string blockUrl2;
        public string BlockUrl2String1
        {
            get
            {
                if (blockUrl2 == null && Block != null)
                {
                    blockUrl2 = Block.BlockUrl2String1;
                }
                return blockUrl2;
            }

        }

        public int      BedroomTypeId
        {
            get
            { return bedroomTypeId;}
            set
            { 
                AddToAudit("BedroomTypeId", bedroomTypeId, value);
                if (bedroomTypeId != value)
                {
                    bedroomTypeName = "";
                    bedroomWebDescription = "";
                }
                bedroomTypeId = value;
            }
        }
        public int      Floor
        {
            get
            { return floor;}
            set
            { 
                AddToAudit("Floor", floor, value);
                floor = value;
            }
        }
        public int      SingleSexArea
        {
            get
            { return singleSexArea;}
            set
            { 
                AddToAudit("SingleSexArea", singleSexArea, value);
                singleSexArea = value;
            }
        }
        public string SingleSexAreaString
        {
            get
            {
                return PreferenceValueToString(singleSexArea);
            }
        }

        public string SingleSexAreaStringPlus
        {
            get
            {
                return PreferenceValueToStringPlus(singleSexArea);
            }
        }

        public string SingleSexAreaStringReverse
        {
            get
            {
                return PreferenceValueToStringReverse(singleSexArea);
            }
        }

        public string SingleSexAreaStringMixed
        {
            get
            {
                return PreferenceValueToStringMixed(singleSexArea);
            }
        }

        public string SingleSexAreaVerbose
        {
            get
            {
                return GetSingleSexAreaVerbose();
            }
        }

        public int      Smoking
        {
            get
            { return smoking;}
            set
            { 
                AddToAudit("Smoking", smoking, value);
                smoking = value;
            }
        }
        public string SmokingString
        {
            get
            {
                return PreferenceValueToString(smoking);
            }
        }

        public string SmokingStringReversed
        {
            get
            {
                return PreferenceValueToStringReverse(smoking);
            }
        }

       


        public int      CustomField1
        {
            get
            { return customField1;}
            set
            { 
                AddToAudit("CustomField1", customField1, value);
                customField1 = value;
            }
        }
        public string CustomField1String
        {
            get
            {
                return PreferenceValueToString(customField1);
            }
        }

        public string CustomField1StringPlus
        {
            get
            {
                return PreferenceValueToStringPlus(customField1);
            }
        }

        public string CustomField1FullString
        {
            get
            {
                return CustomField1Name+" : "+PreferenceValueToString(customField1);
            }
        }

        public int      CustomField2
        {
            get
            { return customField2;}
            set
            { 
                AddToAudit("CustomField2", customField2, value);
                customField2 = value;
            }
        }
        public string CustomField2String
        {
            get
            {
                return PreferenceValueToString(customField2);
            }
        }

        public string CustomField2FullString
        {
            get
            {
                return CustomField2Name + " : " + PreferenceValueToString(customField1);
            }
        }

        public string   CustomValue1
        {
            get
            { return customValue1;}
            set
            { 
                AddToAudit("CustomValue1", customValue1, value);
                customValue1 = value;
            }
        }
        public string   CustomValue2
        {
            get
            { return customValue2;}
            set
            { 
                AddToAudit("CustomValue2", customValue2, value);
                customValue2 = value;
            }
        }
        public string   CustomValue3
        {
            get
            { return customValue3;}
            set
            { 
                AddToAudit("CustomValue3", customValue3, value);
                customValue3 = value;
            }
        }
        public string   CustomValue4
        {
            get
            { return customValue4;}
            set
            { 
                AddToAudit("CustomValue4", customValue4, value);
                customValue4 = value;
            }
        }

        public string CustomValue5
        {
            get
            { return customValue5; }
            set
            {
                AddToAudit("CustomValue5", customValue5, value);
                customValue5 = value;
            }
        }
        public string CustomValue6
        {
            get
            { return customValue6; }
            set
            {
                AddToAudit("CustomValue6", customValue6, value);
                customValue6 = value;
            }
        }
        public string CustomValue7
        {
            get
            { return customValue7; }
            set
            {
                AddToAudit("CustomValue7", customValue7, value);
                customValue7 = value;
            }
        }
        public string CustomValue8
        {
            get
            { return customValue8; }
            set
            {
                AddToAudit("CustomValue8", customValue8, value);
                customValue8 = value;
            }
        }
        public DateTime ?CreationDate
        {
            get
            { return creationDate;}
            set
            { 
                AddToAudit("CreationDate", creationDate, value);
                creationDate = value;
            }
        }
        public int OriginalDescription
        {
            get
            { return originalDescription;}
            set
            { 
                AddToAudit("OriginalDescription", originalDescription, value);
                originalDescription = value;
            }
        }
        public string SearchDescription
        {
            get
            { return searchDescription;}
            set
            { searchDescription = value; }
        }
        public SelectedType SelectionType
        {
            get
            { return selectionType; }
            set
            { selectionType = value; }
        }
        public bool Selected
        {
            get
            { return selectionType == SelectedType.Selected; }
            set
            {
                if (value)
                {
                    selectionType = SelectedType.Selected;
                }
                else
                {
                    selectionType = SelectedType.Unselected;
                }
            }
        } 
   
        public string SiteName
        {
            get
            {
                if (siteName == "" && siteId > 0)
                {
                    siteName = Site.Name;
                }
                if (PreferenceAvailability != null && !string.IsNullOrWhiteSpace(PreferenceAvailability.SiteName))
                {
                    return PreferenceAvailability.SiteName;
                }
                return siteName;
            }
        }

        public KxSite Site
        {
            get
            {
                if (site == null && siteId > 0)
                {
                    site = new KxSite(this, siteId);
                }
                return site;
            }
        }

        public string AreaName
        {
            get
            {
                if (areaName == "" && areaId > 0)
                {
                    Load();
                }
                if (PreferenceAvailability != null && !string.IsNullOrWhiteSpace(PreferenceAvailability.AreaName))
                {
                    return PreferenceAvailability.AreaName;
                }
                return areaName;
            }
        }

        public string BlockName
        {
            get
            {
                if (blockName == "" && blockId > 0)
                {
                    Load();
                }
                if (PreferenceAvailability != null && !string.IsNullOrWhiteSpace(PreferenceAvailability.BlockName))
                {
                    return PreferenceAvailability.BlockName;
                }
                return blockName;
            }
        }
        public string BedroomTypeName
        {
            get
            {
                if (bedroomTypeName == "" && bedroomTypeId > 0)
                {
                    Load();
                }
                if (PreferenceAvailability != null && !string.IsNullOrWhiteSpace(PreferenceAvailability.BedroomTypeName))
                {
                    return PreferenceAvailability.BedroomTypeName;
                }
                return bedroomTypeName;
            }
        }

        public KxBedroomType BedroomType
        {
            get
            {
                if (bedroomType == null && bedroomTypeId > 0)
                {
                    bedroomType = new KxBedroomType(this, bedroomTypeId);
                }
                return bedroomType;
            }
        }

        public string BedroomWebDescription
        {
            get
            {
                if (bedroomWebDescription == "" && bedroomTypeId > 0)
                {
                    if (BedroomType != null)
                    {
                        bedroomWebDescription = BedroomType.WebDescription;
                    }
                }

                return bedroomWebDescription;
            }
        }

        
        public string CustomField1Name
         {
            get
             { return ((KxWebStudentPreferences)Parent).CustomField1Name; }
        }
        public string CustomField2Name
         {
            get
             { return ((KxWebStudentPreferences)Parent).CustomField2Name; }
        }
        public string CustomValue1Name
         {
            get
             { return ((KxWebStudentPreferences)Parent).CustomValue1Name; }
        }
        public string CustomValue2Name
         {
            get
             { return ((KxWebStudentPreferences)Parent).CustomValue2Name; }
        }
        public string CustomValue3Name
         {
            get
             { return ((KxWebStudentPreferences)Parent).CustomValue3Name; }
        }
        public string CustomValue4Name
        {
            get
            { return ((KxWebStudentPreferences)Parent).CustomValue4Name; }
        }
        public string CustomValue5Name
        {
            get
            { return ((KxWebStudentPreferences)Parent).CustomValue5Name; }
        }
        public string CustomValue6Name
        {
            get
            { return ((KxWebStudentPreferences)Parent).CustomValue6Name; }
        }
        public string CustomValue7Name
        {
            get
            { return ((KxWebStudentPreferences)Parent).CustomValue7Name; }
        }
        public string CustomValue8Name
        {
            get
            { return ((KxWebStudentPreferences)Parent).CustomValue8Name; }
        }
        public string CustomValue1Options
        {
            get
            { return ((KxWebStudentPreferences)Parent).CustomValue1Options; }
        }
        public string CustomValue2Options
        {
            get
            { return ((KxWebStudentPreferences)Parent).CustomValue2Options; }
        }
        public string CustomValue3Options
        {
            get
            { return ((KxWebStudentPreferences)Parent).CustomValue3Options; }
        }
        public string CustomValue4Options
        {
            get
            { return ((KxWebStudentPreferences)Parent).CustomValue4Options; }
        }
        public string CustomValue5Options
        {
            get
            { return ((KxWebStudentPreferences)Parent).CustomValue5Options; }
        }
        public string CustomValue6Options
        {
            get
            { return ((KxWebStudentPreferences)Parent).CustomValue6Options; }
        }
        public string CustomValue7Options
        {
            get
            { return ((KxWebStudentPreferences)Parent).CustomValue7Options; }
        }
        public string CustomValue8Options
        {
            get
            { return ((KxWebStudentPreferences)Parent).CustomValue8Options; }
        }
        public string PreferenceDescription
        {
            get
            { return GetPreferenceDescription(); }
        }
        public object UIObject
        {
            get
            {
                return uIObject;
            }
            set
            {
                if (uIObject == value)
                    return;
                uIObject = value;
            }
        }


        public string AreaAndSite
        {
            get
            {
                return AreaId.ToString() + "-" + SiteId;
            }
            set
            {
                SetAreaAndSite(value);
            }
        }

        public string BedroomTypeAndSite
        {
            get
            {
                return BedroomTypeId.ToString() + "-" + SiteId;
            }
            set
            {
                SetBedroomTypeAndSite(value);
            }
        }


        public string BedroomTypeAndArea
        {
            get
            {
                return AreaId.ToString() + "-" + bedroomTypeId.ToString();
            }
            set
            {
                SetBedroomTypeAndArea(value);
            }
        }

        public string BedroomTypeAndCF2
        {
            get
            {
                return CustomField2.ToString() + "-" + bedroomTypeId.ToString();
            }
            set
            {
                SetBedroomTypeAndCF2(value);
            }
        }

        public string SiteAndBedroomTypeAndBlockAndCF2
        {
            get
            {
                return CustomField2.ToString() + "-" + BlockId.ToString() + "-" + bedroomTypeId.ToString()+ siteId.ToString();
            }
            set
            {
                SetSiteAndBedroomTypeAndBlockAndCF2(value);
            }
        }

        public string SiteAndCustomValue1
        {
            get
            {
                string result = siteId.ToString() + ":" + CustomValue1;

                if (result == "0:")
                {
                    result = "";
                }

                return result;
            }
            set
            {
                SetSiteAndCustomValue1(value);
            }
        }


        public string BedroomTypeAndBlockAndCF2
        {
            get
            {
                string result = CustomField2.ToString() + "-" + BlockId.ToString() + "-" + bedroomTypeId.ToString();

                if (result == "0-0-0")
                {
                    result = "";
                }

                return result;
            }
            set
            {
                SetBedroomTypeAndBlockAndCF2(value);
            }
        }

        public string BedroomTypeAndAreaAndCF2
        {
            get
            {
                return CustomField2.ToString() + "-" + AreaId.ToString() + "-" + bedroomTypeId.ToString();
            }
            set
            {
                SetBedroomTypeAndAreaAndCF2(value);
            }
        }


        public string BedroomTypeAndAreaAndCustomValue1AndCustomValue2
        {
            get
            {
                return CustomValue2 + "-" + CustomValue1 + "-" + AreaId.ToString() + "-" + bedroomTypeId.ToString();
            }
            set
            {
                SetBedroomTypeAndAreaAndCustomValue1AndCustomValue2(value);
            }
        }

        public string BedroomTypeAndAreaString
        {
            get
            {
                return AreaName + "-" + BedroomTypeName;
            }            
        }
        public string BedroomTypeChargeValues
        {
            get
            {
                return SiteId.ToString() + "-" + BedroomTypeId.ToString();
            }
            set
            {
                SetBedroomTypeChargeValues(value);
            }
        }
        public string CustomValue4AndArea
        {
            get
            {
                return AreaId.ToString() + "-" + CustomValue4;
            }
            set
            {
                SetCustomValue4AndArea(value);
            }
        }

        public string CustomValue4AndBedroomType
        {
            get
            {
                return BedroomTypeId.ToString() + "-" + CustomValue4;
            }
            set
            {
                SetCustomValue4AndBedroomType(value);
            }
        }

        public string CustomValue4AndAreaAndBlock
        {
            get
            {
                return AreaId.ToString() + "-" + BlockId.ToString() + "-" + CustomValue4;
            }
            set
            {
                SetCustomValue4AndAreaAndBlock(value);
            }
        }

        public string CustomValue4AndAreaString
        {
            get
            {
                return AreaName + "-" + CustomValue4;
            }
        }

        public string CustomValue5AndArea
        {
            get
            {
                return AreaId.ToString() + "-" + CustomValue5;
            }
            set
            {
                SetCustomValue5AndArea(value);
            }
        }

        public string CustomValue5AndBedroomType
        {
            get
            {
                return BedroomTypeId.ToString() + "-" + CustomValue5;
            }
            set
            {
                SetCustomValue5AndBedroomType(value);
            }
        }

        public string CustomValue5AndAreaAndBlock
        {
            get
            {
                return AreaId.ToString() + "-" + BlockId.ToString() + "-" + CustomValue5;
            }
            set
            {
                SetCustomValue5AndAreaAndBlock(value);
            }
        }

        public string CustomValue5AndAreaString
        {
            get
            {
                return AreaName + "-" + CustomValue5;
            }
        }

        public string CustomValue6AndArea
        {
            get
            {
                return AreaId.ToString() + "-" + CustomValue6;
            }
            set
            {
                SetCustomValue6AndArea(value);
            }
        }

        public string CustomValue6AndBedroomType
        {
            get
            {
                return BedroomTypeId.ToString() + "-" + CustomValue6;
            }
            set
            {
                SetCustomValue6AndBedroomType(value);
            }
        }

        public string CustomValue6AndAreaAndBlock
        {
            get
            {
                return AreaId.ToString() + "-" + BlockId.ToString() + "-" + CustomValue6;
            }
            set
            {
                SetCustomValue6AndAreaAndBlock(value);
            }
        }

        public string CustomValue7AndArea
        {
            get
            {
                return AreaId.ToString() + "-" + CustomValue7;
            }
            set
            {
                SetCustomValue7AndArea(value);
            }
        }

        public string CustomValue7AndBedroomType
        {
            get
            {
                return BedroomTypeId.ToString() + "-" + CustomValue7;
            }
            set
            {
                SetCustomValue7AndBedroomType(value);
            }
        }

        public string CustomValue7AndAreaAndBlock
        {
            get
            {
                return AreaId.ToString() + "-" + BlockId.ToString() + "-" + CustomValue7;
            }
            set
            {
                SetCustomValue7AndAreaAndBlock(value);
            }
        }

        public string CustomValue7AndAreaString
        {
            get
            {
                return AreaName + "-" + CustomValue7;
            }
        }

        public string CustomValue8AndArea
        {
            get
            {
                return AreaId.ToString() + "-" + CustomValue8;
            }
            set
            {
                SetCustomValue8AndArea(value);
            }
        }

        public string CustomValue8AndBedroomType
        {
            get
            {
                return BedroomTypeId.ToString() + "-" + CustomValue8;
            }
            set
            {
                SetCustomValue8AndBedroomType(value);
            }
        }

        public string CustomValue8AndAreaAndBlock
        {
            get
            {
                return AreaId.ToString() + "-" + BlockId.ToString() + "-" + CustomValue8;
            }
            set
            {
                SetCustomValue8AndAreaAndBlock(value);
            }
        }

        public string CustomValue8AndAreaString
        {
            get
            {
                return AreaName + "-" + CustomValue8;
            }
        }

        public string CustomValue6AndAreaString
        {
            get
            {
                return AreaName + "-" + CustomValue6;
            }
        }

        public int ContractLength
        {
            get
            {
                 return originalDescription;
            }
            set
            {
                originalDescription = value;
            }
        }

        public int AvailableCount
        {
            get
            {
                return availableCount;
            }
            set
            {
                availableCount = value;
            }
        }

        public bool Ensuite
        {
            get
            {
                return ensuite;
            }
            set
            {
                ensuite = value;
            }
        }

        protected string _tempString1;
        public string TempString1
        {
            get
            {
                return _tempString1;
            }
        }
        protected string _tempString2;
        public string TempString2
        {
            get
            {
                return _tempString2;
            }
        }
        protected string _tempString3;
        public string TempString3
        {
            get
            {
                return _tempString3;
            }
        }
        protected string _tempString4;
        public string TempString4
        {
            get
            {
                return _tempString4;
            }
        }
        protected string _tempString5;
        public string TempString5
        {
            get
            {
                return _tempString5;
            }
        }

        private string _tempString6;
        public string TempString6
        {
            get { return _tempString6; }
        }

        private string _tempString7;
        public string TempString7
        {
            get { return _tempString7; }
        }

        private string _tempString8;
        public string TempString8
        {
            get { return _tempString8; }
        }

        private string _tempString9;
        public string TempString9
        {
            get { return _tempString9; }
        }

        private string _tempString10;
        public string TempString10
        {
            get { return _tempString10; }
        }
        

        private string _tempString11;
        public string TempString11
        {
            get { return _tempString11; }
        }
        private string _tempString12;
        public string TempString12
        {
            get { return _tempString12; }
        }
        private string _tempString13;
        public string TempString13
        {
            get { return _tempString13; }
        }
        private string _tempString14;
        public string TempString14
        {
            get { return _tempString14; }
        }
        private string _tempString15;
        public string TempString15
        {
            get { return _tempString15; }
        }
        private string _tempString16;
        public string TempString16
        {
            get { return _tempString16; }
        }
        private string _tempString17;
        public string TempString17
        {
            get { return _tempString17; }
        }
        private string _tempString18;
        public string TempString18
        {
            get { return _tempString18; }
        }
        private string _tempString19;
        public string TempString19
        {
            get { return _tempString19; }
        }
        private string _tempString20;
        public string TempString20
        {
            get { return _tempString20; }
        }
        

        private int _tempInt1;
        public int TempInt1
        {
            get { return _tempInt1; }
        }

        private int _tempInt2;
        public int TempInt2
        {
            get { return _tempInt2; }
        }

        private int _tempInt3;
        public int TempInt3
        {
            get { return _tempInt3; }
        }

        private int _tempInt4;
        public int TempInt4
        {
            get { return _tempInt4; }
        }

        private int _tempInt5;
        public int TempInt5
        {
            get { return _tempInt5; }
        }

        private decimal _tempDecimal1;
        public decimal TempDecimal1
        {
            get { return _tempDecimal1; }
        }

        private decimal _tempDecimal2;
        public decimal TempDecimal2
        {
            get { return _tempDecimal2; }
        }


        private int _preferenceAvailabilityId;
        public int PreferenceAvailabilityId
        {
            get { return _preferenceAvailabilityId; }
            set
            {
                AddToAudit("PreferenceAvailabilityId", _preferenceAvailabilityId, value);
                _preferenceAvailabilityId = value;
            }
        }

        private KxPreferenceAvailability _preferenceAvailability;
        public KxPreferenceAvailability PreferenceAvailability
        {
            get
            {
                if (_preferenceAvailability == null && _preferenceAvailabilityId > 0)
                {
                    _preferenceAvailability = new KxPreferenceAvailability(null, _preferenceAvailabilityId);
                }

                return _preferenceAvailability;
            }
        }

        public DateTime? EarliestArrivalDate 
        {
            get
            {
                if (PreferenceAvailability == null)
                    return null;

                return PreferenceAvailability.EarliestArrivalDate;
            } 
        }
        public DateTime? LatestDepartureDate
        {
            get
            {
                if (PreferenceAvailability == null)
                    return null;

                return PreferenceAvailability.LatestDepartureDate;
            }
        }

        public bool HasLatestDepartureDate
        {
            get
            {
                return LatestDepartureDate.HasValue;
            }
        }

        public bool HasEarliestArrivalDate
        {
            get
            {
                return EarliestArrivalDate.HasValue;
            }
        }

        public bool ExtendedStayAvailable
        {
            get
            {
                return HasEarliestArrivalDate || HasLatestDepartureDate;
            }
        }


        private static string _hasPreferenceAvailabilityColumn;
        #endregion

        public KxWebStudentPreference() : base(null)
        {
            IsKxClass = false;
        }

        public KxWebStudentPreference(KxBaseClass parent) : base(parent)
        {
            IsKxClass = false;
        }

        public KxWebStudentPreference(KxBaseClass parent, int preferenceid) : this(parent)
        {
             base.dBIdentifier = preferenceid;
             Load();
        }

        public KxWebStudentPreference(KxBaseClass parent,XmlNode node) : this(parent)
        {
            LoadFromXml("",node);
        }
        
        public override KxData ConfigureData(ActionType actiontype)
        {
            IsKxClass = false;
            KxData result = new KxData("webstudentapplicationpreference", "WebStudentApplicationPreference", TargetType.Table);

            result.AutoKeyField = new KxDataValue("PreferenceId",PreferenceId,KxValueType.Int);
            
            if ((actiontype == ActionType.Update) && (PreferenceId == 0))
            {
                result.Action = ActionType.Insert;
            }
            else
            {
                result.Action = actiontype;
            }
                       
            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {

                if (ApplicationGuid == Guid.Empty)
                {
                    if (Parent != null)
                    {
                        if (Parent.Parent != null)
                        {
                            if (Parent.Parent.GuidIdentifier != Guid.Empty)
                            {
                                applicationGuid = Parent.Parent.GuidIdentifier;
                            }
                        }
                        else if (Parent.GuidIdentifier != Guid.Empty)
                        {
                            applicationGuid = Parent.GuidIdentifier;
                        }
                    }
                }
                result.AddParameter("applicationGuid", applicationGuid, KxValueType.Guid, false);
                result.AddParameter("preferenceNumber", preferenceNumber, KxValueType.Int, false);
                result.AddParameter("siteId", siteId, KxValueType.Int, true);
                result.AddParameter("areaId", areaId, KxValueType.Int, true);
                result.AddParameter("blockId", blockId, KxValueType.Int, true);
                result.AddParameter("bedroomTypeId", bedroomTypeId, KxValueType.Int, true);
                result.AddParameter("floor", floor, KxValueType.Int, true);
                result.AddParameter("singleSexArea", singleSexArea, KxValueType.Int, true);
                result.AddParameter("smoking", smoking, KxValueType.Int, true);
                result.AddParameter("customField1", customField1, KxValueType.Int, true);
                result.AddParameter("customField2", customField2, KxValueType.Int, true);
                result.AddParameter("customValue1", customValue1, KxValueType.String, 50, true);
                result.AddParameter("customValue2", customValue2, KxValueType.String, 50, true);
                result.AddParameter("customValue3", customValue3, KxValueType.String, 50, true);
                result.AddParameter("customValue4", customValue4, KxValueType.String, 50, true);
                result.AddParameter("customValue5", customValue5, KxValueType.String, 50, true);
                result.AddParameter("customValue6", customValue6, KxValueType.String, 50, true);
                result.AddParameter("customValue7", customValue7, KxValueType.String, 50, true);
                result.AddParameter("customValue8", customValue8, KxValueType.String, 50, true);
                result.AddParameter("creationDate", creationDate, KxValueType.DateTime, true);
                result.AddParameter("originalDescription", originalDescription, KxValueType.Int, true);
                const string STR_No = "No";
                const string STR_Yes = "Yes";
                try
                {
                    if (string.IsNullOrEmpty(_hasPreferenceAvailabilityColumn))
                    {
                        KxData data = new KxData("dd", "SELECT TOP 1 PreferenceAvailabilityId FROM WebStudentApplicationPreference", TargetType.Static);
                        if (data.Execute())
                        {
                            _hasPreferenceAvailabilityColumn = STR_Yes;
                        }
                        else
                        {
                            _hasPreferenceAvailabilityColumn = STR_No;
                        }
                    }
                }
                catch (Exception ex)
                {
                    KxLogger.LogActivity(this, "Finding PreferenceAvailabilityID column", ex.Message, "");
                    _hasPreferenceAvailabilityColumn = STR_No;
                }

                if (_hasPreferenceAvailabilityColumn == STR_Yes)
                {
                    result.AddParameter("preferenceavailabilityid", _preferenceAvailabilityId, KxValueType.Int);
                }
            }
            else if (actiontype == ActionType.Select)
            {
                result.Target = TargetType.StoredProc;
            }
            return result;
        }

        public override void LoadFromXml(string xml,XmlNode node)
        {
            selectionType = SelectedType.Unselected;

            dBIdentifier = KxXmlUtils.GetNodeValue(node, "preferenceid", 0);
            preferenceNumber = KxXmlUtils.GetNodeValue(node, "preferencenumber", 0);
            applicationGuid = KxXmlUtils.GetNodeValue(node, "applicationguid", Guid.Empty);
            siteId = KxXmlUtils.GetNodeValue(node, "siteid", 0);
            siteName = KxXmlUtils.GetNodeValue(node, "sitename", "");
            areaId = KxXmlUtils.GetNodeValue(node, "areaid", 0);
            areaName = KxXmlUtils.GetNodeValue(node, "areaname", "");
            blockId = KxXmlUtils.GetNodeValue(node, "blockid", 0);
            blockName = KxXmlUtils.GetNodeValue(node, "blockname", "");
            bedroomTypeId = KxXmlUtils.GetNodeValue(node, "bedroomtypeid", 0);
            bedroomTypeName = KxXmlUtils.GetNodeValue(node, "bedroomtypename", "");
            floor = KxXmlUtils.GetNodeValue(node, "floor", 0);
            singleSexArea = KxXmlUtils.GetNodeValue(node, "singlesexarea", 0);
            smoking = KxXmlUtils.GetNodeValueBoolInt(node, "smoking", 0);
            customField1 = KxXmlUtils.GetNodeValueBoolInt(node, "customfield1", 0);
            customField2 = KxXmlUtils.GetNodeValueBoolInt(node, "customfield2", 0);
            customValue1 = KxXmlUtils.GetNodeValue(node, "customvalue1", "");
            customValue2 = KxXmlUtils.GetNodeValue(node, "customvalue2", "");
            customValue3 = KxXmlUtils.GetNodeValue(node, "customvalue3", "");
            customValue4 = KxXmlUtils.GetNodeValue(node, "customvalue4", "");
            customValue5 = KxXmlUtils.GetNodeValue(node, "customvalue5", "");
            customValue6 = KxXmlUtils.GetNodeValue(node, "customvalue6", "");
            customValue7 = KxXmlUtils.GetNodeValue(node, "customvalue7", "");
            customValue8 = KxXmlUtils.GetNodeValue(node, "customvalue8", "");
            creationDate = KxXmlUtils.GetNodeValueDT(node, "creationdate", null);
            originalDescription = KxXmlUtils.GetNodeValue(node, "originaldescription", 0);
            searchDescription = KxXmlUtils.GetNodeValue(node, "searchDescription", "");
            availableCount = KxXmlUtils.GetNodeValue(node, "availablecount", 0);
            _tempString1 = KxXmlUtils.GetNodeValue(node, "tempstring1", string.Empty);
            _tempString2 = KxXmlUtils.GetNodeValue(node, "tempstring2", string.Empty);
            _tempString3 = KxXmlUtils.GetNodeValue(node, "tempstring3", string.Empty);
            _tempString4 = KxXmlUtils.GetNodeValue(node, "tempstring4", string.Empty);
            _tempString5 = KxXmlUtils.GetNodeValue(node, "tempstring5", string.Empty);
            _tempString6 = KxXmlUtils.GetNodeValue(node, "tempstring6", string.Empty);
            _tempString7 = KxXmlUtils.GetNodeValue(node, "tempstring7", string.Empty);
            _tempString8 = KxXmlUtils.GetNodeValue(node, "tempstring8", string.Empty);
            _tempString9 = KxXmlUtils.GetNodeValue(node, "tempstring9", string.Empty);
            _tempString10 = KxXmlUtils.GetNodeValue(node, "tempstring10", string.Empty);
            _tempString11 = KxXmlUtils.GetNodeValue(node, "tempstring11", string.Empty);
            _tempString12 = KxXmlUtils.GetNodeValue(node, "tempstring12", string.Empty);
            _tempString13 = KxXmlUtils.GetNodeValue(node, "tempstring13", string.Empty);
            _tempString14 = KxXmlUtils.GetNodeValue(node, "tempstring14", string.Empty);
            _tempString15 = KxXmlUtils.GetNodeValue(node, "tempstring15", string.Empty);
            _tempString16 = KxXmlUtils.GetNodeValue(node, "tempstring16", string.Empty);
            _tempString17 = KxXmlUtils.GetNodeValue(node, "tempstring17", string.Empty);
            _tempString18 = KxXmlUtils.GetNodeValue(node, "tempstring18", string.Empty);
            _tempString19 = KxXmlUtils.GetNodeValue(node, "tempstring19", string.Empty);
            _tempString20 = KxXmlUtils.GetNodeValue(node, "tempstring20", string.Empty);
            _tempInt1 = KxXmlUtils.GetNodeValue(node, "tempint1", 0);
            _tempInt2 = KxXmlUtils.GetNodeValue(node, "tempint2", 0);
            _tempInt3 = KxXmlUtils.GetNodeValue(node, "tempint3", 0);
            _tempInt4 = KxXmlUtils.GetNodeValue(node, "tempint4", 0);
            _tempInt5 = KxXmlUtils.GetNodeValue(node, "tempint5", 0);
            _tempDecimal1 = KxXmlUtils.GetNodeValue(node, "tempdecimal1", 0m);
            _tempDecimal2 = KxXmlUtils.GetNodeValue(node, "tempdecimal2", 0m);
            _preferenceAvailabilityId = KxXmlUtils.GetNodeValue(node, "preferenceavailabilityid", 0);

            if (searchDescription == "")
            {
                searchDescription = areaName;
            }
        }

        public string GetPreferenceDescription()
        {
            return GetPreferenceDescription("0PN0AR0SXF0SKF0CF1F0CF2F0CV1F0CV2F0CV3F0CV4F0");
        }
        public string GetPreferenceDescription(string Template)
        {

           if (((areaId > 0 && String.IsNullOrEmpty(areaName)) ||
               (blockId > 0 && String.IsNullOrEmpty(blockName)) ||
               (bedroomTypeId > 0 && String.IsNullOrEmpty(bedroomTypeName))) &&
               (!Changed && !Temporary))
           {
               Load();
           }

           string[] IncludeItems = Template.Split(new char[] { '0' }, StringSplitOptions.RemoveEmptyEntries);

           StringBuilder sb = new StringBuilder();
            
           foreach (string s in IncludeItems)
           {
               switch (s.ToUpper())
               {
                   case "PN": sb.Append(AddPreferenceValue(preferenceNumber.ToString(), ", ")); break;
                   case "SI": sb.Append(AddPreferenceValue(SiteName, ", ")); break;
                   case "AR": sb.Append(AddPreferenceValue(areaName, ", ")); break;
                   case "BA": sb.Append(AddPreferenceValue(BedroomTypeAndAreaString, ", ")); break;
                   case "BL": sb.Append(AddPreferenceValue(blockName, ", ")); break;
                   case "BT": sb.Append(AddPreferenceValue(bedroomTypeName, ", ")); break;
                   case "FR": sb.Append(AddPreferenceValue(Floor.ToString(), ", ")); break;
                   case "SX": sb.Append(AddPreferenceValue(PreferenceValueToString(singleSexArea), ", ")); break;
                   case "SK": sb.Append(AddPreferenceValue(PreferenceValueToString(smoking), ", ")); break;

                   case "SXF": sb.Append(AddPreferenceValue(PreferenceToString("Gender", singleSexArea), ", ")); break;
                   case "SKF": sb.Append(AddPreferenceValue(PreferenceToString("Smoking", smoking), ", ")); break;

                   case "CF1": sb.Append(AddPreferenceValue(PreferenceValueToString(customField1), ", ")); break;
                   case "CF2": sb.Append(AddPreferenceValue(PreferenceValueToString(customField2), ", ")); break;
                   case "CF1F": sb.Append(AddPreferenceValue(CustomField1Name, PreferenceValueToString(customField1), ", ")); break;
                   case "CF2F": sb.Append(AddPreferenceValue(CustomField2Name, PreferenceValueToString(customField2), ", ")); break;
                   case "CV1": sb.Append(AddPreferenceValue(customValue1, ", ")); break;
                   case "CV2": sb.Append(AddPreferenceValue(customValue2, ", ")); break;
                   case "CV3": sb.Append(AddPreferenceValue(customValue3, ", ")); break;
                   case "CV4": sb.Append(AddPreferenceValue(customValue4, ", ")); break;
                   case "CV5": sb.Append(AddPreferenceValue(customValue5, ", ")); break;
                   case "CV6": sb.Append(AddPreferenceValue(customValue6, ", ")); break;
                   case "CV7": sb.Append(AddPreferenceValue(customValue7, ", ")); break;
                   case "CV8": sb.Append(AddPreferenceValue(customValue8, ", ")); break;
                   case "CV1F": sb.Append(AddPreferenceValue(CustomValue1Name, customValue1, ", ")); break;
                   case "CV2F": sb.Append(AddPreferenceValue(CustomValue2Name, customValue2, ", ")); break;
                   case "CV3F": sb.Append(AddPreferenceValue(CustomValue3Name, customValue3, ", ")); break;
                   case "CV4F": sb.Append(AddPreferenceValue(CustomValue4Name, customValue4, ", ")); break;
                   case "CV5F": sb.Append(AddPreferenceValue(CustomValue5Name, customValue5, ", ")); break;
                   case "CV6F": sb.Append(AddPreferenceValue(CustomValue6Name, customValue6, ", ")); break;
                   case "CV7F": sb.Append(AddPreferenceValue(CustomValue7Name, customValue7, ", ")); break;
                   case "CV8F": sb.Append(AddPreferenceValue(CustomValue8Name, customValue8, ", ")); break;
                }
           }
           return sb.ToString().TrimEnd(new char[] { ' ', ',' })+"\r\n";
        }

        private string AddPreferenceValue(string source, string seperator)
        {
            if (source == "" || source == null)
            {
                return "";
            }
            else
            {
                return source + seperator;
            }
        }

        private string AddPreferenceValue(string identifier,string source, string seperator)
        {
            if (source == "" || source == null || identifier == "" || identifier == null)
            {
                return "";
            }
            else
            {
                return identifier + ":" + source + seperator;
            }
        }

        public string GetPreferenceDescriptionTabSeperated(string Template)
        {
            string result = GetPreferenceDescription(Template);

            return result.Replace(", ", "\t");
        }

        protected string PreferenceToString(string name,int preference)
        {
            if (name != "")
            {
                return name + ":" + PreferenceValueToString(preference);
            } return "";
        }

        protected string PreferenceValueToString(int preference)
        {
            switch (preference)
            {
                case 0: return "No";
                case 1: return "Yes";
                default: return "No Preference";
            }
        }

        protected string PreferenceValueToStringPlus(int preference)
        {
            switch (preference)
            {
                case 1: return "No";
                case 2: return "Yes";
                default: return "No Preference";
            }
        }

        protected string PreferenceValueToStringReverse(int preference)
        {
            switch (preference)
            {
                case 0: return "Yes";
                case 1: return "No";
                default: return "No";
            }
        }

        protected string PreferenceValueToStringMixed(int preference)
        {
            switch (preference)
            {
                case 0: return "Mixed";
                case 1: return "Single";
                default: return "Either";
            }
        }

        protected string GetSortType()
        {
            string result = "";

            if (Parent != null && Parent is KxWebStudentPreferences)
            {
                result = ((KxWebStudentPreferences)Parent).SortType;
            }
            return result;
        }

        public int CompareTo(KxWebStudentPreference other)
        {
            string sortType = GetSortType().ToUpper();
            int result = 0;

            switch (sortType)
            {
                case "SELECTEDUP":result = other.Selected.CompareTo(Selected);break;
                case "SELECTEDDN":result = Selected.CompareTo(other.Selected);break;
                case "DESCRIPTIONDN":result = other.SearchDescription.CompareTo(SearchDescription);break;
                case "DESCRIPTIONUP":result = SearchDescription.CompareTo(other.SearchDescription);break;
                case "CUSTOMF1DN": result = other.CustomField1String.CompareTo(CustomField1String); break;
                case "CUSTOMF1UP": result = CustomField1String.CompareTo(other.CustomField1String); break;
                case "CUSTOMF2DN": result = other.CustomField2String.CompareTo(CustomField2String); break;
                case "CUSTOMF2UP": result = CustomField2String.CompareTo(other.CustomField2String); break;
                case "CUSTOMV1DN":result = other.CustomValue1.CompareTo(CustomValue1);break;
                case "CUSTOMV1UP":result = CustomValue1.CompareTo(other.CustomValue1);break;
                case "CUSTOMV2DN":result = other.CustomValue2.CompareTo(CustomValue2);break;
                case "CUSTOMV2UP":result = CustomValue2.CompareTo(other.CustomValue2);break;
                case "CUSTOMV3DN":result = other.CustomValue3.CompareTo(CustomValue3);break;
                case "CUSTOMV3UP":result = CustomValue3.CompareTo(other.CustomValue3);break;
                case "CUSTOMV4DN":result = other.CustomValue4.CompareTo(CustomValue4);break;
                case "CUSTOMV4UP":result = CustomValue4.CompareTo(other.CustomValue4);break;
                case "CUSTOMV5DN": result = other.CustomValue5.CompareTo(CustomValue5); break;
                case "CUSTOMV5UP": result = CustomValue5.CompareTo(other.CustomValue5); break;
                case "CUSTOMV6DN": result = other.CustomValue6.CompareTo(CustomValue6); break;
                case "CUSTOMV6UP": result = CustomValue6.CompareTo(other.CustomValue6); break;
                case "CUSTOMV7DN": result = other.CustomValue7.CompareTo(CustomValue7); break;
                case "CUSTOMV7UP": result = CustomValue7.CompareTo(other.CustomValue7); break;
                case "CUSTOMV8DN": result = other.CustomValue8.CompareTo(CustomValue8); break;
                case "CUSTOMV8UP": result = CustomValue8.CompareTo(other.CustomValue8); break;
            }

            if (result == 0)
            {
                result = other.PreferenceNumber.CompareTo(PreferenceNumber);
            }
            return result;
        }

        public bool DuplicateExists(KxWebStudentPreference pref)
        {
            bool result = false;


            if ((SiteId > 0 || AreaId > 0 || BlockId > 0 || BedroomTypeId > 0 || !String.IsNullOrEmpty(CustomValue1) || !String.IsNullOrEmpty(CustomValue2) || !String.IsNullOrEmpty(CustomValue3) || !String.IsNullOrEmpty(CustomValue4)) &&
            (pref.SiteId > 0 || pref.AreaId > 0 || pref.BlockId > 0 || pref.BedroomTypeId > 0 || !String.IsNullOrEmpty(pref.CustomValue1) || !String.IsNullOrEmpty(pref.CustomValue2) || !String.IsNullOrEmpty(pref.CustomValue3) || !String.IsNullOrEmpty(pref.CustomValue4)
            || !String.IsNullOrEmpty(pref.CustomValue5) || !String.IsNullOrEmpty(pref.CustomValue6) || !String.IsNullOrEmpty(pref.CustomValue7) || !String.IsNullOrEmpty(pref.CustomValue8)))
            {
                if (
                    pref.PreferenceNumber != PreferenceNumber &&
                    pref.SiteId == SiteId &&
                    ((pref.AreaId == AreaId && AreaId > 0) || (AreaId == 0)) &&
                    pref.BlockId == BlockId &&
                    pref.BedroomTypeId == BedroomTypeId &&
                    pref.Floor == Floor &&
                    pref.SingleSexArea == SingleSexArea &&
                    pref.Smoking == Smoking &&
                    pref.CustomField1 == CustomField1 &&
                    pref.CustomField2 == CustomField2 &&
                    pref.CustomValue1 == CustomValue1 &&
                    pref.CustomValue2 == CustomValue2 &&
                    pref.CustomValue3 == CustomValue3 &&
                    pref.CustomValue4 == CustomValue4 &&
                    pref.CustomValue5 == CustomValue5 &&
                    pref.CustomValue6 == CustomValue6 &&
                    pref.CustomValue7 == CustomValue7 &&
                    pref.CustomValue8 == CustomValue8)
                {
                    result = true;
                }
            }
          
            return result;
        }
        public override string ToString()
        {
            return PreferenceNumber.ToString();
        }

        private void SetAreaAndSite(string value)
        {
            string[] parts = value.Split('-');

            if (parts.Length == 2)
            {
                try
                {
                    AreaId = Convert.ToInt32(parts[0]);
                }
                catch
                {
                }

                try
                {
                    SiteId = Convert.ToInt32(parts[1]);
                }
                catch
                {
                }
            }
        }


        private void SetBedroomTypeAndSite(string value)
        {
            string[] parts = value.Split('-');

            if (parts.Length == 2)
            {
                try
                {
                    BedroomTypeId = Convert.ToInt32(parts[0]);
                }
                catch
                {
                }

                try
                {
                    SiteId = Convert.ToInt32(parts[1]);
                }
                catch
                {
                }
            }
        }

        private void SetBedroomTypeAndArea(string value)
        {
            string[] parts = value.Split('-');

            if (parts.Length == 2)
            {
                try
                {
                    AreaId = Convert.ToInt32(parts[0]);
                }
                catch
                { 
                }

                try
                {
                    BedroomTypeId = Convert.ToInt32(parts[1]);
                }
                catch
                {
                }
            }
        }

        private void SetBedroomTypeAndCF2(string value)
        {
            if (value == "")
            {
                value = "0-0";
            }
            string[] parts = value.Split('-');

            if (parts.Length == 2)
            {
                try
                {
                    CustomField2 = Convert.ToInt32(parts[0]);
                }
                catch
                {
                }

                try
                {
                    BedroomTypeId = Convert.ToInt32(parts[1]);
                }
                catch
                {
                }
            }
        }

        private void SetSiteAndCustomValue1(string value)
        {
            if (value == "")
            {
                value = "0:";
            }

            string[] parts = value.Split(':');

            if (parts.Length == 2)
            {
                try
                {
                    SiteId = Convert.ToInt32(parts[0]);
                }
                catch
                {
                }

                try
                {
                    CustomValue1 = parts[1];
                }
                catch
                {
                }

            
            }
        }

        private void SetSiteAndBedroomTypeAndBlockAndCF2(string value)
        {
            if (value == "")
            {
                value = "0-0-0-0";
            }

            string[] parts = value.Split('-');

            if (parts.Length == 4)
            {
                try
                {
                    CustomField2 = Convert.ToInt32(parts[0]);
                }
                catch
                {
                }

                try
                {
                    BlockId = Convert.ToInt32(parts[1]);
                }
                catch
                {
                }

                try
                {
                    BedroomTypeId = Convert.ToInt32(parts[2]);
                }
                catch
                {
                }

                try
                {
                    siteId = Convert.ToInt32(parts[3]);
                }
                catch
                {
                }
            }
        }


        private void SetBedroomTypeAndBlockAndCF2(string value)
        {
            if (value == "")
            {
                value = "0-0-0";
            }

            string[] parts = value.Split('-');

            if (parts.Length == 3)
            {
                try
                {
                    CustomField2 = Convert.ToInt32(parts[0]);
                }
                catch
                {
                }

                try
                {
                    BlockId = Convert.ToInt32(parts[1]);
                }
                catch
                {
                }

                try
                {
                    BedroomTypeId = Convert.ToInt32(parts[2]);
                }
                catch
                {
                }
            }
        }



        private void SetBedroomTypeAndAreaAndCustomValue1AndCustomValue2(string value)
        {
            string[] parts = value.Split('-');

            if (parts.Length == 4)
            {
                try
                {
                    CustomValue2 = parts[0];
                }
                catch
                {
                }

                try
                {
                    CustomValue1 = parts[1];
                }
                catch
                {
                }

                try
                {
                    AreaId = Convert.ToInt32(parts[2]);
                }
                catch
                {
                }

                try
                {
                    BedroomTypeId = Convert.ToInt32(parts[3]);
                }
                catch
                {
                }
            }
            else
            {
                CustomValue2 = "";
                CustomValue1 = "";
                AreaId = 0;
                BedroomTypeId = 0;
            }
        }

        private void SetBedroomTypeAndAreaAndCF2(string value)
        {
            string[] parts = value.Split('-');

            if (parts.Length == 3)
            {
                try
                {
                    CustomField2 = Convert.ToInt32(parts[0]);
                }
                catch
                {
                }

                try
                {
                    AreaId = Convert.ToInt32(parts[1]);
                }
                catch
                {
                }

                try
                {
                    BedroomTypeId = Convert.ToInt32(parts[2]);
                }
                catch
                {
                }
            }
            else
            {
                CustomField2 = 0;
                AreaId = 0;
                BedroomTypeId = 0;
            }
        }


        private void SetCustomValue4AndBedroomType(string value)
        {
            string[] parts = value.Split('-');

            if (parts.Length == 2)
            {
                try
                {
                    BedroomTypeId = Convert.ToInt32(parts[0]);
                }
                catch
                {
                }

                try
                {
                    CustomValue4 = parts[1];
                }
                catch
                {
                }
            }
        }

        private void SetCustomValue4AndArea(string value)
        {
            string[] parts = value.Split('-');

            if (parts.Length == 2)
            {
                try
                {
                    AreaId = Convert.ToInt32(parts[0]);
                }
                catch
                {
                }

                try
                {
                    CustomValue4 = parts[1];
                }
                catch
                {
                }
            }
        }

        private void SetCustomValue4AndAreaAndBlock(string value)
        {
            string[] parts = value.Split('-');
            int id;

            if (parts.Length == 3)
            {
                if(Int32.TryParse(parts[0], out id))
                {
                    AreaId = id;
                }

                if (Int32.TryParse(parts[1], out id))
                {
                    BlockId = id;
                }

                CustomValue4 = parts[2];
            }
        }

        private void SetCustomValue5AndArea(string value)
        {
            string[] parts = value.Split('-');

            if (parts.Length == 2)
            {
                try
                {
                    AreaId = Convert.ToInt32(parts[0]);
                }
                catch
                {
                }

                try
                {
                    CustomValue5 = parts[1];
                }
                catch
                {
                }
            }
        }
        private void SetCustomValue6AndArea(string value)
        {
            string[] parts = value.Split('-');

            if (parts.Length == 2)
            {
                try
                {
                    AreaId = Convert.ToInt32(parts[0]);
                }
                catch
                {
                }

                try
                {
                    CustomValue6 = parts[1];
                }
                catch
                {
                }
            }
        }
        private void SetCustomValue7AndArea(string value)
        {
            string[] parts = value.Split('-');

            if (parts.Length == 2)
            {
                try
                {
                    AreaId = Convert.ToInt32(parts[0]);
                }
                catch
                {
                }

                try
                {
                    CustomValue7 = parts[1];
                }
                catch
                {
                }
            }
        }
        private void SetCustomValue8AndArea(string value)
        {
            string[] parts = value.Split('-');

            if (parts.Length == 2)
            {
                try
                {
                    AreaId = Convert.ToInt32(parts[0]);
                }
                catch
                {
                }

                try
                {
                    CustomValue8 = parts[1];
                }
                catch
                {
                }
            }
        }

        private void SetCustomValue5AndBedroomType(string value)
        {
            string[] parts = value.Split('-');

            if (parts.Length == 2)
            {
                try
                {
                    BedroomTypeId = Convert.ToInt32(parts[0]);
                }
                catch
                {
                }

                try
                {
                    CustomValue5 = parts[1];
                }
                catch
                {
                }
            }
        }

        private void SetCustomValue6AndBedroomType(string value)
        {
            string[] parts = value.Split('-');

            if (parts.Length == 2)
            {
                try
                {
                    BedroomTypeId = Convert.ToInt32(parts[0]);
                }
                catch
                {
                }

                try
                {
                    CustomValue6 = parts[1];
                }
                catch
                {
                }
            }
        }

        private void SetCustomValue7AndBedroomType(string value)
        {
            string[] parts = value.Split('-');

            if (parts.Length == 2)
            {
                try
                {
                    BedroomTypeId = Convert.ToInt32(parts[0]);
                }
                catch
                {
                }

                try
                {
                    CustomValue7 = parts[1];
                }
                catch
                {
                }
            }
        }

        private void SetCustomValue8AndBedroomType(string value)
        {
            string[] parts = value.Split('-');

            if (parts.Length == 2)
            {
                try
                {
                    BedroomTypeId = Convert.ToInt32(parts[0]);
                }
                catch
                {
                }

                try
                {
                    CustomValue8 = parts[1];
                }
                catch
                {
                }
            }
        }

        private void SetCustomValue5AndAreaAndBlock(string value)
        {
            string[] parts = value.Split('-');
            int id;

            if (parts.Length == 3)
            {
                if (Int32.TryParse(parts[0], out id))
                {
                    AreaId = id;
                }

                if (Int32.TryParse(parts[1], out id))
                {
                    BlockId = id;
                }

                CustomValue5 = parts[2];
            }
        }

        private void SetCustomValue6AndAreaAndBlock(string value)
        {
            string[] parts = value.Split('-');
            int id;

            if (parts.Length == 3)
            {
                if (Int32.TryParse(parts[0], out id))
                {
                    AreaId = id;
                }

                if (Int32.TryParse(parts[1], out id))
                {
                    BlockId = id;
                }

                CustomValue6 = parts[2];
            }
        }

        private void SetCustomValue7AndAreaAndBlock(string value)
        {
            string[] parts = value.Split('-');
            int id;

            if (parts.Length == 3)
            {
                if (Int32.TryParse(parts[0], out id))
                {
                    AreaId = id;
                }

                if (Int32.TryParse(parts[1], out id))
                {
                    BlockId = id;
                }

                CustomValue7 = parts[2];
            }
        }

        private void SetCustomValue8AndAreaAndBlock(string value)
        {
            string[] parts = value.Split('-');
            int id;

            if (parts.Length == 3)
            {
                if (Int32.TryParse(parts[0], out id))
                {
                    AreaId = id;
                }

                if (Int32.TryParse(parts[1], out id))
                {
                    BlockId = id;
                }

                CustomValue8 = parts[2];
            }
        }
        /// <summary>
        /// Sets the site and bedroomtype
        /// </summary>
        /// <param name="value"></param>
        private void SetBedroomTypeChargeValues(string value)
        {
            string[] parts = value.Split('-');

            if (parts.Length == 2)
            {
                try
                {
                    BedroomTypeId = Convert.ToInt32(parts[1]);
                }
                catch
                {
                }

                try
                {
                    SiteId = Convert.ToInt32(parts[0]);
                }
                catch
                {
                }
            }
        }


        public override bool Delete()
        {
            bool result = true;

            KxData data = ConfigureData(ActionType.Delete);
            try
            {
                if (data != null)
                {
                    data.Initialise();
                }
                try
                {
                    result = Children.Delete(data);

                    if (result && data != null && !IsKxClass)
                    {
                        result = data.Execute();

                        if (!result)
                        {
                            LastError = data.LastError;
                        }
                    }
                }
                finally
                {
                    if (result && data != null)
                    {
                        data.Finalise();
                    }
                    else
                    {
                        data.Cancel();
                    }
                }
                if (result && parent != null && !IsKxClass)
                {
                    parent.Children.Remove(this);
                    parent = null;
                }

                if (Parent != null)
                {
                    Parent.NotifyDelete(this);
                }
            }
            finally
            {
                data.CloseConnection();
            }

            return result;
        }

        protected string GetSingleSexAreaVerbose()
        {
            switch (singleSexArea)
            {
                case 2:  return "No Preference";
                case 1:  return "Same Sex";
                default: return "Mixed";
            }
        }
    }
}

