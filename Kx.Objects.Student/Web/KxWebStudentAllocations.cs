using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Exceptions;
using Kx.Core.Utility;

namespace Kx.Objects.Student.Web
{

    public enum KxAllocationStatus { Pending, Accepted, Rejected, Expired, Withdrawn,None };

    public class  KxWebStudentAllocations : KxBaseClass
    {
        private KxWebUserStudent webUser = null;

        public KxWebUserStudent WebUser
        {
            get
            {
                return webUser;
            }
            set
            {
                webUser = value;
            }
        }

        private List<KxWebStudentAllocation> items = new List<KxWebStudentAllocation>();

        public Guid UserGuid
        {
            get
            {
                return guidIdentifier;
            }
        }

        public List<KxWebStudentAllocation> Items
        {
            get
            { return items; }
        }

        public KxWebStudentAllocation this[int index]
        {
            get
            { return items[index]; }
            set
            { items[index] = value; }
        }

        public int[] HistoricAllocationsArray
        {
            get
            {
                return GetHistoricAllocationsArray();
            }
        }

        public int[] FutureAllocationsArray
        {
            get
            {
                return GetFutureAllocationsArray();
            }
        }

        public override KxData ConfigureData(ActionType actiontype)
        {
            if (parent != null)
            {
                guidIdentifier = parent.GuidIdentifier;
            }

            KxData result = null;

            if (actiontype == ActionType.Select)
            {
                result = new KxData("webstudentallocation", "webstudentallocation", TargetType.Table, ActionType.Select);

                result.KeyFields.Add(new KxDataValue("userGuid", UserGuid, KxValueType.Guid));
                result.AddOrderField("allocationid", KxDataOrderType.Descending);
            }
            return result;
        }

        public KxWebStudentAllocations(KxBaseClass parent, Guid userGuid)
            : this(parent)
        {
            if (parent is KxWebUserStudent)
            {
                webUser = (KxWebUserStudent)parent;
            }
            guidIdentifier = userGuid;
            Load();
        }

        public KxWebStudentAllocations(KxBaseClass parent) : base(parent)
        {

        }

        public override bool LoadFromXml(string xml, string xpath)
        {
            items.Clear();
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xml);

                XmlNodeList nodes = doc.SelectNodes(@"//results/" + xpath + "/datarow");

                foreach (XmlNode node in nodes)
                {
                    if (node != null)
                    {
                        KxWebStudentAllocation allocation = new KxWebStudentAllocation(this, node);
                        Items.Add(allocation);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "LoadFromXml", ex.Message, xpath);
                LastError = ex.Message;
                return false;
            }
        }
        
        public KxWebStudentAllocation GetLastAllocation(KxWebStudentApplication app)
        {
            KxWebStudentAllocation result = null;
            try
            {
                if (app.StandardApplication != null)
                {
                    foreach (KxWebStudentAllocation alloc in Items)
                    {
                        if (alloc.StudentAllocation.ApplicationId == app.StandardApplication.ApplicationId)
                        {
                            result = alloc;
                            break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "GetPendingAllocation", ex.Message, "");
            }
            return result;
        }

        public KxAllocationStatus GetAllocationStatus(KxWebStudentApplication app)
        {
            //override foe specified allocations....
            if (WebUser.SpecifiedPendingAllocation != null)
            {
                return KxAllocationStatus.Pending;
            }
            else if (WebUser.SpecifiedAcceptedAllocation != null)
            {
                return KxAllocationStatus.Accepted;
            }
            else if (WebUser.SpecifiedRejectedAllocation != null)
            {
                return KxAllocationStatus.Rejected;
            }
            else  if (app.ConfirmedDate == null)
            {
                return KxAllocationStatus.None;
            }
            else if (GetPendingAllocation(app) != null)
            {
                return KxAllocationStatus.Pending;
            }
            else if (GetAcceptedAllocation(app) != null)
            {
                return KxAllocationStatus.Accepted;
            }
            else if (GetExpiredAllocation(app) != null)
            {
                return KxAllocationStatus.Expired;
            }
            else if (GetRejectedAllocation(app) != null)
            {
                return KxAllocationStatus.Rejected;
            }
            else if (GetWithdrawnAllocation(app) != null)
            {
                return KxAllocationStatus.Withdrawn;
            }
            else
            {
                return KxAllocationStatus.None;
            }
        }

        public KxWebStudentAllocation GetPendingAllocation(KxWebStudentApplication app)
        {
            KxWebStudentAllocation result = null;
            try
            {
                if (WebUser.SpecifiedPendingAllocation != null)
                {
                    result = WebUser.SpecifiedPendingAllocation;
                }
                else if (app.StandardApplication != null)
                {
                    foreach (KxWebStudentAllocation alloc in Items)
                    {
                        if (
                                (alloc.StudentAllocation.ApplicationId == app.StandardApplication.ApplicationId) &&
                                (alloc.StudentAllocation.OfferDate > KxDateTime.KxApplicableDate) &&
                                (alloc.StudentAllocation.IsCurrent) &&
                                ((alloc.StudentAllocation.AcceptedDate == KxDateTime.KxNullDate &&
                                alloc.StudentAllocation.AcceptedUserId == 0 &&
                                alloc.StudentAllocation.RejectedDate == KxDateTime.KxNullDate &&
                                alloc.StudentAllocation.RejectedUserId == 0 &&
                                !alloc.StudentAllocation.OfferHasExpired) &&
                                (alloc.AcceptedDate == null && alloc.RejectedDate == null))
                            )
                        {
                            result = alloc;
                            break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "GetPendingAllocation", ex.Message, "");
            }
            return result;
        }

        public KxWebStudentAllocation GetExpiredAllocation(KxWebStudentApplication app)
        {
            KxWebStudentAllocation result = null;
            try
            {
                if (app.StandardApplication != null)
                {
                    foreach (KxWebStudentAllocation alloc in Items)
                    {
                        if (
                                (alloc.StudentAllocation.ApplicationId == app.StandardApplication.ApplicationId) &&
                                (alloc.StudentAllocation.OfferDate > KxDateTime.KxApplicableDate) &&
                                (alloc.StudentAllocation.IsCurrent) &&
                                ((alloc.StudentAllocation.AcceptedDate == KxDateTime.KxNullDate &&
                                alloc.StudentAllocation.AcceptedUserId == 0 &&
                                alloc.StudentAllocation.RejectedDate == KxDateTime.KxNullDate &&
                                alloc.StudentAllocation.RejectedUserId == 0 &&
                                alloc.StudentAllocation.OfferHasExpired) &&
                                (alloc.AcceptedDate == null && alloc.RejectedDate == null))
                            )
                        {
                            result = alloc;
                            break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "GetExpiredAllocation", ex.Message, "");
            }
            return result;
        }

        public KxWebStudentAllocation GetWithdrawnAllocation(KxWebStudentApplication app)
        {
            KxWebStudentAllocation result = null;
            try
            {
                if (app.StandardApplication != null)
                {
                    foreach (KxWebStudentAllocation alloc in Items)
                    {
                        if (
                                (alloc.StudentAllocation.ApplicationId == app.StandardApplication.ApplicationId) &&
                                (alloc.StudentAllocation.OfferDate > KxDateTime.KxApplicableDate) &&
                                (!alloc.StudentAllocation.IsCurrent) &&
                                ((alloc.StudentAllocation.AcceptedDate == KxDateTime.KxNullDate &&
                                alloc.StudentAllocation.AcceptedUserId == 0 &&
                                alloc.StudentAllocation.RejectedDate == KxDateTime.KxNullDate &&
                                alloc.StudentAllocation.RejectedUserId == 0) &&
                                (alloc.AcceptedDate == null && alloc.RejectedDate == null))
                            )
                        {
                            result = alloc;
                            break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "GetWithdrawnAllocation", ex.Message, "");
            }
            return result;
        }

    
        public KxWebStudentAllocation GetInHouseAllocation(KxWebStudentApplication app)
        {
            KxWebStudentAllocation result = null;
            DateTime zeroDate = new DateTime(1900, 1, 1);
            try
            {
                if (app.StandardApplication != null)
                {
                    foreach (KxWebStudentAllocation alloc in Items)
                    {
                        if (
                                (alloc.StudentAllocation.ApplicationId == app.StandardApplication.ApplicationId) &&
                                (alloc.StudentAllocation != null) && 
                                (alloc.StudentAllocation.AllocationDates != null) && 
                                (alloc.StudentAllocation.AllocationDates.Items.Count > 0)
                                
                            )
                        {
                            foreach (KxStudentAllocationDate allocDate in alloc.StudentAllocation.AllocationDates.Items)
                            {
                                if (allocDate.CheckedInDate.HasValue && allocDate.CheckedInDate.Value > zeroDate
                                    && (!allocDate.CheckedOutDate.HasValue || allocDate.CheckedOutDate.Value <= zeroDate))
                                {
                                    result = alloc;
                                    break;
                                }
                            }
                            if (result != null) break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "GetRejectedAllocation", ex.Message, "");
            }
            return result;
        }

        public KxWebStudentAllocation GetRejectedAllocation(KxWebStudentApplication app)
        {
            KxWebStudentAllocation result = null;
            try
            {
                if (WebUser.SpecifiedRejectedAllocation != null)
                {
                    result = WebUser.SpecifiedRejectedAllocation;
                }
                else if (app.StandardApplication != null)
                {
                    foreach (KxWebStudentAllocation alloc in Items)
                    {
                        if (
                                (alloc.StudentAllocation.ApplicationId == app.StandardApplication.ApplicationId) &&
                                (alloc.StudentAllocation.OfferDate > KxDateTime.KxApplicableDate) &&
                                ((alloc.StudentAllocation.RejectedDate > KxDateTime.KxNullDate && alloc.StudentAllocation.RejectedUserId > 0) || (alloc.RejectedDate != null))
                            )
                        {
                            result = alloc;
                            break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "GetRejectedAllocation", ex.Message, "");
            }
            return result;
        }

        public KxWebStudentAllocation GetAcceptedAllocation(KxWebStudentApplication app)
        {
            KxWebStudentAllocation result = null;
            try
            {
                if (WebUser.SpecifiedAcceptedAllocation != null)
                {
                    result = WebUser.SpecifiedAcceptedAllocation;
                }
                else if (app.StandardApplication != null)
                {
                    foreach (KxWebStudentAllocation alloc in Items)
                    {
                        if (
                                (alloc.StudentAllocation.ApplicationId == app.StandardApplication.ApplicationId) &&
                                (alloc.StudentAllocation.OfferDate > KxDateTime.KxApplicableDate) &&
                                (alloc.StudentAllocation.IsCurrent) &&
                                ((alloc.StudentAllocation.AcceptedDate > KxDateTime.KxNullDate && alloc.StudentAllocation.AcceptedUserId > 0) ||
                                (alloc.AcceptedDate != null) && (alloc.StudentAllocation.IsCurrent))
                            )
                        {
                            result = alloc;
                            break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "GetAcceptedAllocation", ex.Message, "");
            }

            return result;
        }

        public KxWebStudentAllocation GetAllocationById(int allocationId)
        {
            KxWebStudentAllocation result = null;

            foreach (KxWebStudentAllocation alloc in Items)
            {
                if (alloc.AllocationId == allocationId)
                {
                    result = alloc;
                    break;
                }
            }
            return result;
        }

        public int[] GetFutureAllocationsArray()
        {
            SortedList<DateTime, int> result = new SortedList<DateTime, int>();

            foreach (KxWebStudentAllocation wsa in Items)
            {
                if (wsa.StudentAllocation.ImplicitlyAccepted && wsa.StudentAllocation.IsCurrent && wsa.StudentAllocation.AllocationEndDate > DateTime.Now.Date)
                {
                    try
                    {
                        result.Add(wsa.StudentAllocation.AllocationStartDate, wsa.AllocationId);
                    }
                    catch
                    {
                        result.Add(wsa.StudentAllocation.OfferDate.Value, wsa.AllocationId);
                    }
                }
            }
            List<int> li = new List<int>();
                
            foreach (int i in result.Values)
            {
                li.Insert(0,i);
            }

            return li.ToArray();
        }

        public int[] GetHistoricAllocationsArray()
        {
            SortedList<DateTime, int> result = new SortedList<DateTime, int>();

            foreach (KxWebStudentAllocation wsa in Items)
            {
                if (wsa.StudentAllocation.ImplicitlyAccepted && wsa.StudentAllocation.IsCurrent && wsa.StudentAllocation.AllocationEndDate <= DateTime.Now.Date)
                {
                    try
                    {
                        result.Add(wsa.StudentAllocation.AllocationStartDate, wsa.AllocationId);
                    }
                    catch
                    {
                        result.Add(wsa.StudentAllocation.OfferDate.Value, wsa.AllocationId);
                    }
                }
            }
            List<int> li = new List<int>();
                
            foreach (int i in result.Values)
            {
                li.Insert(0,i);
            }
            return li.ToArray();
        }
    }
}
