using System;
using System.Collections.Generic;
using System.Text;
using Kx.Core.Configuration;

namespace Kx.Objects.Student.Web
{
    public class KxWebStudentAllocationSchedule
    {
        private bool PaymentDeducted = false;

        protected int cfgPaymentDate = 28;
        protected int cfgShortLeasePeriod = 92;
        protected object applicationId = null;

        #region Attributes & Properties

        private int arrivalDay = 0;
        private int departureDay = 0;
        private int arrivalMonth = 0;
        private int departureMonth = 0;
        private int arrivalYear = 0;
        private int departureYear = 0;

        private int actualArrivalDay = 0;
        private int actualDepartureDay = 0;
        private int actualArrivalMonth = 0;
        private int actualDepartureMonth = 0;
        private int actualArrivalYear = 0;
        private int actualDepartureYear = 0;

        private int AcademicYear = 0;

        private int monthCount = 0;

        private int numberOfDays = 0;
        private int totalNumberOfDays = 0;

        private DateTime arrivalDate;
        private DateTime departureDate;

        private DateTime actualArrivalDate;
        private DateTime actualDepartureDate;

        private Decimal totalFees = 0;

        private Decimal weeklycharge = 0;

        private int numberofweeks = 0;

        private List<KxWebPaymentSchedule> monthlyPayments = new List<KxWebPaymentSchedule>();
        private List<KxWebPaymentSchedule> semesterPayments = new List<KxWebPaymentSchedule>();

        private KxWebStudentAllocation allocation = null;


        public Decimal WeeklyCharge
        {
            get
            { return weeklycharge; }
        }

        public int NumberOfWeeks
        {
            get
            { return numberofweeks; }
        }

        public int ActualArrivalDay
        {
            get
            { return actualArrivalDay; }
            set
            { actualArrivalDay = value; }
        }

        public int ActualDepartureDay
        {
            get
            { return actualDepartureDay; }
            set
            { actualDepartureDay = value; }
        }

        public int ActualArrivalMonth
        {
            get
            { return actualArrivalMonth; }
            set
            { actualArrivalMonth = value; }
        }

        public int ActualDepartureMonth
        {
            get
            { return actualDepartureMonth; }
            set
            { actualDepartureMonth = value; }
        }

        public int ActualArrivalYear
        {
            get
            { return actualArrivalYear; }
            set
            { actualArrivalYear = value; }
        }

        public int ActualDepartureYear
        {
            get
            { return actualDepartureYear; }
            set
            { actualDepartureYear = value; }
        }
        
        public int ArrivalDay
        {
            get
            { return arrivalDay; }
            set
            { arrivalDay = value; }
        }

        public int DepartureDay
        {
            get
            { return departureDay; }
            set
            { departureDay = value; }
        }

        public int ArrivalMonth
        {
            get
            { return arrivalMonth; }
            set
             { arrivalMonth = value; }
        }

        public int DepartureMonth
         {
            get
            { return departureMonth; }
            set
            { departureMonth = value; }
        }

        public int ArrivalYear
        {
            get
            { return arrivalYear; }
            set
            { arrivalYear = value; }
        }

        public int DepartureYear
        {
            get
            { return departureYear; }
            set
            { departureYear = value; }
        }
        
        public int MonthCount
        {
            get
            { return monthCount; }
            set
            { monthCount = value; }
        }
        
        public int NumberOfDays
        {
            get
            { return numberOfDays; }
            set
            { numberOfDays = value; }
        }

        public int TotalNumberOfDays
        {
            get
            { return totalNumberOfDays; }
            set
            { totalNumberOfDays = value; }
        }
        
        public DateTime ArrivalDate
        {
            get
            { return arrivalDate; }
            set
            { arrivalDate = value; }
        }

        public DateTime DepartureDate
        {
            get
            { return departureDate; }
            set
            { departureDate = value; }
        }

        public DateTime ActualArrivalDate
        {
            get
            { return actualArrivalDate; }
            set
            { actualArrivalDate = value; }
        }

        public DateTime ActualDepartureDate
        {
            get
            { return actualDepartureDate; }
            set
            { actualDepartureDate = value; }
        }
        
        public Decimal TotalFees
        {
            get
            { return totalFees; }
            set
            { totalFees = value; }
        }

        public KxWebStudentAllocation Allocation
        {
            get
            { return allocation; }
            set
            { allocation = value; }
        }
        public List<KxWebPaymentSchedule> MonthlyPayments
        {
            get
            { return monthlyPayments; }
            set
            { monthlyPayments = value; }
        }

        public List<KxWebPaymentSchedule> SemesterPayments
        {
            get
            { return semesterPayments; }
            set
            { semesterPayments = value; }
        }

        public bool IsShortLease
        {
            get
            { return TotalNumberOfDays <= 92; }
        }

        public bool IsLeaseUnder120Days
        {
            get
            { return TotalNumberOfDays < 120; }
        }

        public bool IsLeaseOver120Days
        {
            get
            { return TotalNumberOfDays >= 120; }
        }

		public bool IsBefore1Feb
		{
			get
			{
				DateTime dtCheck;
				DateTime dtToday = DateTime.Today;

				dtCheck = new DateTime(dtToday.Year, 2, 1);
				if (dtCheck < dtToday)
				{
					dtCheck = dtCheck.AddYears(1);
				}
				return ActualDepartureDate < dtCheck;
			}
		}

        public bool IsLongLease
        {
            get
            { return !IsShortLease; }
        }

        public Decimal MonthlyTotal
        {
            get
            { return GetTotal(monthlyPayments); }
        }

        public Decimal SemesterTotal
        {
            get
            { return GetTotal(semesterPayments); }
        }

        public int NumberOfSemesterPayments
        {
            get
            { return SemesterPayments.Count; }
        }

        public int NumberOfMonthlyPayments
        {
            get
            { return MonthlyPayments.Count; }
        }

        public int[] MonthlyPaymentsArray
        {
            get
            { return GetPaymentArray(monthlyPayments); }
        }

        public int[] SemesterPaymentsArray
        {
            get
            { return GetPaymentArray(semesterPayments); }
        }

        #endregion

        public KxWebStudentAllocationSchedule(KxWebStudentAllocation ParentAllocation)
        {
            LoadParameters();
            Allocation = ParentAllocation;
            ConfigureParameters();
            CalculatePayments();
        }

        private void ConfigureParameters()
        {
            applicationId = KxDbConfiguration.GetConfiguration("ApplicationId");
            
            if (applicationId != null)
            {
                if (applicationId.ToString().ToUpper() == "NEWCASTLE")
                {
                    ArrivalDate = Allocation.StudentAllocation.AllocationStartDate.AddMonths(1);
                    DepartureDate = Allocation.StudentAllocation.AllocationEndDate;

                    cfgPaymentDate = 24;
                    cfgShortLeasePeriod = 92;

                    if (Allocation.StudentAllocation.AllocationEndDate <= new DateTime(AcademicYear + 1, 2, 1) || Allocation.StudentAllocation.AllocationStartDate >= new DateTime(AcademicYear + 1, 4, 24))
                    {
                        cfgShortLeasePeriod = 0;
                    }
                }
                else
                    if (applicationId.ToString().ToUpper() == "UWE")
                    {
                        ArrivalDate = Allocation.StudentAllocation.AllocationStartDate;
                        DepartureDate = Allocation.StudentAllocation.AllocationEndDate;

                        cfgPaymentDate = 22;
                        cfgShortLeasePeriod = 92;
                    }
                    else
                    {
                    cfgPaymentDate = 28;
                    cfgShortLeasePeriod = 92;
                    ArrivalDate = Allocation.StudentAllocation.AllocationStartDate.AddDays(14);
                    DepartureDate = Allocation.StudentAllocation.AllocationEndDate.AddDays(-45);
                }
            }

            TotalFees = Allocation.StudentAllocation.AllocationFees;

            try
            {
                ArrivalDay = ArrivalDate.Day;
                ArrivalMonth = ArrivalDate.Month;
                ArrivalYear = ArrivalDate.Year;

                ActualArrivalDate = Allocation.StudentAllocation.AllocationStartDate;
                ActualArrivalDay = ActualArrivalDate.Day;
                ActualArrivalMonth = ActualArrivalDate.Month;
                ActualArrivalYear = ActualArrivalDate.Year;
            }
            catch{}

            try
            {                
                DepartureDay = DepartureDate.Day;
                DepartureMonth = DepartureDate.Month;
                DepartureYear = DepartureDate.Year;

                ActualDepartureDate = Allocation.StudentAllocation.AllocationEndDate;
                ActualDepartureDay = ActualDepartureDate.Day;
                ActualDepartureMonth = ActualDepartureDate.Month;
                ActualDepartureYear = ActualDepartureDate.Year;
            }
            catch{}


            try
            {
                AcademicYear = ((DateTime)Allocation.StudentAllocation.Application.AcademicYear.StartDate).Year;
            }
            catch { }

            try
            {
                TimeSpan ts = DepartureDate - ArrivalDate;

                NumberOfDays = ts.Days;

                int weeks = (int)numberOfDays / 7;

                if (numberOfDays % 7 != 0)
                {
                    weeks++;
                }

                numberofweeks = weeks;

                if (NumberOfDays > 0)
                {
                    weeklycharge = (TotalFees / NumberOfDays) * 7;
                }

            }
            catch { }

            try
            {
                TimeSpan ts = ActualDepartureDate - ActualArrivalDate;
                TotalNumberOfDays = ts.Days;
            }
            catch { }
            
            

            try
            {
                if (ArrivalDay >= cfgPaymentDate)
                {
                    ArrivalMonth += 1;
                }

                if (ArrivalMonth > 12)
                {
                    ArrivalMonth = 1;
                    ArrivalYear++;
                }

                MonthCount = 0;

                int MonthNum = ArrivalMonth;
                int YearNum = ArrivalYear;

                DateTime MonthDate = new DateTime(ArrivalYear, ArrivalMonth, cfgPaymentDate);

                while (MonthDate <= DepartureDate)
                {
                    //MonthNum = ArrivalMonth + (MonthCount-1) ;
                    MonthNum++;
                    MonthCount++;
                    if (MonthNum > 12)
                    {
                        MonthNum = 1;
                        YearNum++;
                    }
                    
                    MonthDate = new DateTime(YearNum, MonthNum, cfgPaymentDate);
                }

                if (MonthCount == 8)
                {
                    MonthCount = 7;
                }
                else if (MonthCount > 9)
                {
                    MonthCount = 9;
                }
            }
            catch { }
        }

        public void CalculatePayments()
        {
            if (IsLongLease)
            {
                if (applicationId.ToString().ToUpper() == "NEWCASTLE")
                {
                    CalculateNewcastleMonthlyPayments();
                    CalculateNewcastleSemesterPayments();
                }
                else
                {
                    CalculateMonthlyPayments();
                    CalculateSemesterPayments();
                }
            }
        }

        public void CalculateSemesterPayments()
        {
            int MonthCount = 0;
		    int MonthNum = 0;

            if ((ActualArrivalYear == AcademicYear) && (ActualArrivalMonth < 7 || ActualArrivalMonth == 7 && ActualArrivalDay < 11) && (ActualArrivalMonth > 7 || actualDepartureYear > AcademicYear))
            {
                MonthNum = 7;
                MonthCount++;
            }

            if ((ActualArrivalYear == AcademicYear) && (ActualArrivalMonth < 9 || ActualArrivalMonth == 9 && ActualArrivalDay < 11) && (ActualDepartureMonth > 9 || ActualDepartureYear > AcademicYear))
            {
			    if (MonthNum == 0)
                {
			    	MonthNum = 9;
                }
			    MonthCount++;
            }

		    if ((ActualArrivalYear == AcademicYear || ActualArrivalMonth == 1 && ActualArrivalDay < 11) && (ActualDepartureMonth > 1) && (ActualDepartureYear == AcademicYear + 1))
		    {
			    if (MonthNum == 0)
                {
				    MonthNum = 1;
                }
			    MonthCount++;
            }
		
		    if ((ActualArrivalYear == AcademicYear || ActualArrivalMonth < 4 || ActualArrivalMonth == 4 && ActualArrivalDay < 11) && (ActualDepartureMonth > 4)&& (NumberOfDays > 342 || MonthCount < 3))
		    {
			    if (MonthNum == 0)
                {
			        MonthNum = 4;
                }
			    MonthCount++;
            }
		
		    if ((ActualArrivalYear == AcademicYear || ActualDepartureMonth > 7 || ActualArrivalMonth == 4 && ActualArrivalDay < 11) && 
			    (ActualDepartureMonth > 4) && (NumberOfDays > 342 || MonthCount < 3) && (MonthCount < 4))
		    {
			    if (MonthNum == 0)
                {
			        MonthNum = 7;
                }
			    MonthCount++;
            }
            
            Decimal PeriodAmount  = 0;

            try
            {
                PeriodAmount = Decimal.Floor(((TotalFees * 100) / MonthCount)) / 100;
            }
            catch { }

            for (int loop = 1; loop <= MonthCount; loop++)
            {
                int Year;

                if (MonthNum < 7 || (MonthNum == 7 && loop > 1))
                {
                    Year = AcademicYear+1;
                }
                else
                {
                    Year = AcademicYear;
                }

                DateTime PaymentDate = new DateTime(Year, MonthNum, cfgPaymentDate);

			    if (PaymentDate <= DepartureDate)
                {
                   SemesterPayments.Add(new KxWebPaymentSchedule(loop, PaymentDate, PeriodAmount));
                }

			    if (MonthNum == 7)
                {
			    	MonthNum = 9;
                }
			    else if (MonthNum == 9)
                {
			    	MonthNum = 1;
                }
			    else if (MonthNum == 1)
                {
			    	MonthNum = 4;
                }
			    else if (MonthNum == 4)
                {
			    	MonthNum = 7;
                }
            }
            try
            {
                SemesterPayments[0].PaymentAmount += Decimal.Round((TotalFees - (PeriodAmount * SemesterPayments.Count)), 2);
            }
            catch { }
        }

        public void CalculateMonthlyPayments()
        {
            int MonthNum = ArrivalMonth;
            int YearNum = ArrivalYear;

            if (DepartureDay > cfgPaymentDate)
            {
                MonthCount += 1;
            }

            Decimal MonthlyAmount = 0;

            try
            {
                MonthlyAmount = Decimal.Floor(((TotalFees * 100) / MonthCount)) / 100;
            }
            catch { }

            for (int loop = 1;loop <= MonthCount;loop++)
            {
                DateTime PaymentDate = new DateTime(YearNum, MonthNum, cfgPaymentDate);

                if (PaymentDate <= DepartureDate)
                {
                    MonthlyPayments.Add(new KxWebPaymentSchedule(loop,PaymentDate,MonthlyAmount));
                }

                MonthNum++;

                if (MonthNum > 12)
                {
                    MonthNum = 1;
                    YearNum++;
                }
            }

            MonthlyPayments[0].PaymentAmount += Decimal.Round((TotalFees - (MonthlyAmount * MonthCount)),2);
        }

        public void CalculateNewcastleMonthlyPayments()
        {
            int MonthNum = ArrivalMonth;
            int YearNum = ArrivalYear;

            if (DepartureDay > cfgPaymentDate)
            {
                MonthCount += 1;
            }

            Decimal MonthlyAmount = 0;

            try
            {
                MonthlyAmount = Decimal.Floor(((TotalFees * 100) / MonthCount)) / 100;
            }
            catch { }

            for (int loop = 1;loop <= MonthCount && loop <=9;loop++)
            {
                if (MonthNum > 12)
                {
                    MonthNum = 1;
                    YearNum++;
                } 
                
                DateTime PaymentDate = new DateTime(YearNum, MonthNum, cfgPaymentDate);
                               

                if (PaymentDate <= DepartureDate)
                {
                    MonthlyPayments.Add(new KxWebPaymentSchedule(loop,PaymentDate,MonthlyAmount));
                }

                MonthNum++;
            }

            MonthlyPayments[0].PaymentAmount += Decimal.Round((TotalFees - (MonthlyAmount * MonthCount)),2);
        }

        public void CalculateNewcastleSemesterPayments()
        {
            int MonthCount = 0;
            int MonthNum = 0;

            if ((ActualArrivalYear == AcademicYear) && (ActualArrivalMonth < 8 ))
            {
                if (MonthNum == 0)
                {
                    MonthNum = 8;
                }
                MonthCount++;
            }

            if ((ActualArrivalYear == AcademicYear) && (ActualArrivalMonth < 10))
            {
                if (MonthNum == 0)
                {
                    MonthNum = 10;
                }
                MonthCount++;
            }

            if (ActualArrivalYear == AcademicYear)
            {
                if (MonthNum == 0)
                {
                    MonthNum = 1;
                }
                MonthCount++;
            }

            if ((ActualArrivalYear == AcademicYear) || ((ActualArrivalMonth < 4) && (ActualArrivalYear == AcademicYear+1)))
            {
                if (MonthNum == 0)
                {
                    MonthNum = 4;
                }
                MonthCount++;
            }
  

            if ((ActualArrivalYear == AcademicYear) || ((ActualArrivalMonth < 6) && (ActualArrivalYear == AcademicYear+1)) && (MonthCount < 4))
            {
                if (MonthNum == 0)
                {
                    MonthNum = 6;
                }
                MonthCount++;
            }

            for (int loop = 1; loop <= MonthCount; loop++)
            {
                int Year;

                if (MonthNum < 7 && ActualArrivalYear != AcademicYear+1)
                {
                    Year = AcademicYear + 1;
                }
                else
                {
                    Year = AcademicYear;
                }

                DateTime PaymentDate = new DateTime(Year, MonthNum, cfgPaymentDate);

                if (PaymentDate <= DepartureDate)
                {
                    SemesterPayments.Add(new KxWebPaymentSchedule(loop, PaymentDate, GetSemesterCharge(PaymentDate)));
                }

                if (MonthNum == 8)
                {
                    MonthNum = 10;
                }
                else if (MonthNum == 10)
                {
                    MonthNum = 1;
                }
                else if (MonthNum == 1)
                {
                    MonthNum = 4;
                }
                else if (MonthNum == 4)
                {
                    MonthNum = 6;
                }
            }
        }

        public Decimal GetSemesterCharge(DateTime paymentDate)
        {
            Decimal result = 0.00M;

            if (Allocation != null)
            {
                try
                {
                    result = Allocation.StudentAllocation.AllocationDates.GetAllocationDate(paymentDate).AllocationFees;
                }
                catch { }
            }

            return result;
        }
        
        public Decimal GetTotal(List<KxWebPaymentSchedule> list)
        {
            Decimal result = 0;

            foreach(KxWebPaymentSchedule sched in list)
            {
                result += sched.PaymentAmount;
            }

            return result;
        }

        public KxWebPaymentSchedule GetMonthlyPayment(int number)
        {
            return GetPayment(monthlyPayments, number);
        }

        public KxWebPaymentSchedule GetSemesterPayment(int number)
        {
            return GetPayment(semesterPayments, number);
        }

        private KxWebPaymentSchedule GetPayment(List<KxWebPaymentSchedule> list,int number)
        {
            KxWebPaymentSchedule result = null;

            if (number <= list.Count)
            {
                try
                {
                    result = list[number - 1];
                }
                catch
                { }
            }
            else if (list.Count > 0)
            {
                result = list[list.Count-1];
            }
            return result;
        }

        protected int[] GetPaymentArray(List<KxWebPaymentSchedule> list)
        {
            int loop = 0;

            int[] result = new int[list.Count];

            foreach (KxWebPaymentSchedule sched in list)
            {
                result[loop] = sched.PaymentNumber;
                loop++;
            }
            return result;
        }


        private void LoadParameters()
        {

        }

        public void DeductPrepayment(Decimal amount)
        {
            if (!PaymentDeducted)
            {
                try
                {
                    KxWebPaymentSchedule sched = GetMonthlyPayment(99);
                    if (sched != null)
                    {
                        sched.PaymentAmount -= amount;
                    }
                    sched = GetSemesterPayment(99);

                    if (sched != null)
                    {
                        sched.PaymentAmount -= amount;
                    }

                    PaymentDeducted = true;
                }
                catch { }
            }
        }

    }
}
