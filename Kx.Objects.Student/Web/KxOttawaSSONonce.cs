﻿using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;

namespace Kx.Objects.Student.Web
{
    [Serializable]
    public class KxOttawaSSONonce : KxBaseClass
    {
        #region Attributes & Properties

        protected bool loaded = false;
        protected string id = "";
        protected DateTime? creationDate = null;
        protected string studentId = "";
        protected string nonce = "";
        protected string hashReceived = "";

        public bool Loaded
        {
            get
            {
                return loaded;
            }
            set
            {
                loaded = value;
            }
        }

        public string Id
        {
            get
            { return id; }

        }

        public DateTime? CreationDate
        {
            get
            { return creationDate; }
            set
            {
                creationDate = value;
            }
        }

        public string StudentId
        {
            get
            { return studentId; }
            set
            {
                studentId = value;
            }
        }

        public string Nonce
        {
            get
            { return nonce; }
            set
            {
                nonce = value;
            }
        }

        public string HashReceived
        {
            get
            { return hashReceived; }
            set
            {
                hashReceived = value;
            }
        }

        #endregion

        #region Contructors

        public KxOttawaSSONonce(KxBaseClass parent)
            : base(parent)
        {
        }

        #endregion

        public bool InsertToTable()
        {
            KxData result = new KxData("ottawassononces", "OttawaSSONonces", TargetType.Table, ActionType.Insert);

            result.AddParameter("creationDate", creationDate, KxValueType.DateTime, true);
            result.AddParameter("studentId", studentId, KxValueType.String, true);
            result.AddParameter("nonce", nonce, KxValueType.String, true);
            result.AddParameter("hashReceived", hashReceived, KxValueType.String, true);

            if (result.Execute())
            {
                return true;
            }

            return false;
        }


        public bool FindExistingRecordWithNonce(string nonceReceived)
        {
            
            KxData data = null;
            
            data = new KxData("", "WebOttawaSSOSearchForNonce", TargetType.StoredProc, ActionType.ExecuteProcedure);
            
            data.AddParameter("nonce", nonceReceived, KxValueType.String);
            data.Action = ActionType.ExecuteProcedure;

            int count = 0;
            count = Convert.ToInt32(data.ExecuteScalar());

            if (count > 0)
            {
                return true;
            }
            
            return false;
        }

    
    }
}
