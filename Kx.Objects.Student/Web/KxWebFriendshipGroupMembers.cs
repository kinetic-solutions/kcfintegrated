#region copyright

// =======================================================
// Product:   KCF.Student 2015.4
// Project:   Kx.Objects.Student
// File:      KxWebFriendshipGroupMembers.cs
// Created:   08/07/2016
// By:        Steve Ashley
// Modified:  08/12/2016
// 
// Copyright � Kinetic Solutions Ltd 2016
// All rights reserved.
// 
// No portion of this file may be re-created, copied or otherwise
// used without the express permission of Kinetic Solutions Ltd.
// =======================================================

#endregion

#region usings

using System;
using System.Collections.Generic;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Exceptions;
using Kx.Core.Utility;

#endregion

namespace Kx.Objects.Student.Web
{
    public class KxWebFriendshipGroupMembers : KxBaseClass
    {
        #region Fields and Properties

        private int friendshipGroupId;
        private List<KxWebFriendshipGroupMember> items = new List<KxWebFriendshipGroupMember>();
        private int memberLimit;

        public int FriendshipGroupId
        {
            get { return friendshipGroupId; }
            set { friendshipGroupId = value; }
        }

        public bool AllValid
        {
            get { return GetAllValid(); }
        }

        public List<KxWebFriendshipGroupMember> Items
        {
            get { return items; }
        }

        public int MemberLimit
        {
            get { return memberLimit; }
            set
            {
                if (memberLimit != value)
                {
                    CreatePlaceholderEntries(value);
                }
                memberLimit = value;
            }
        }

        public int ActiveCount
        {
            get { return GetActiveCount(); }
        }

        public KxWebFriendshipGroupMember this[int index]
        {
            get { return items[index]; }
            set { items[index] = value; }
        }

        public int Count
        {
            get { return items.Count; }
        }

        #endregion

        #region Constructors

        public KxWebFriendshipGroupMembers(KxBaseClass parent, int friendshipGroupid) : base(parent)
        {
            friendshipGroupId = friendshipGroupid;
            Load();
        }

        #endregion

        #region Public Methods

        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("webfriendshipgroupmember", "webfriendshipgroupmember", TargetType.Table);
            result.KeyFields.Add(new KxDataValue("friendshipGroupId", friendshipGroupId, KxValueType.Int));
            result.Action = ActionType.Select;
            return result;
        }

        public override bool LoadFromXml(string xml, string xpath)
        {
            items.Clear();

            try
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xml);

                XmlNodeList nodes = doc.SelectNodes(@"//results/webfriendshipgroupmember/datarow");

                foreach (XmlNode node in nodes)
                {
                    if (node != null)
                    {
                        int member = KxXmlUtils.GetNodeValue(node, "groupmemberid", 0);
                        if (member != 0)
                        {
                            KxWebFriendshipGroupMember groupmember = new KxWebFriendshipGroupMember(this, member, node);
                            Items.Add(groupmember);
                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "LoadXml", ex.Message, "");
                LastError = ex.Message;
                return false;
            }
        }

        public KxWebFriendshipGroupMember GetGroupMember(KxWebUserStudent user)
        {
            KxWebFriendshipGroupMember result = null;

            if (user.Student != null)
            {
                foreach (KxWebFriendshipGroupMember member in items)
                {
                    if (member.MemberSurName == user.Student.SurName && member.MemberStudentId == user.Student.StudentId)
                    {
                        result = member;
                        break;
                    }
                }
            }
            return result;
        }

        public KxWebFriendshipGroupMember GetGroupMemberById(int groupMemberId)
        {
            KxWebFriendshipGroupMember result = null;

            foreach (KxWebFriendshipGroupMember member in items)
            {
                if (member.GroupMemberId == groupMemberId)
                {
                    result = member;
                    break;
                }
            }
            return result;
        }

        public void CreatePlaceholderEntries(int Value)
        {
            if (ActiveCount < Value)
            {
                for (int i = items.Count; i < Value; i++)
                {
                    KxWebFriendshipGroupMember member = new KxWebFriendshipGroupMember(this);
                    member.Temporary = true;
                    member.TemporaryId = 0;
                    //member.TemporaryId = GetMaxMemberId(true) + 1;
                    Items.Add(member);
                }
            }
            else
            {
                while (ActiveCount > memberLimit && memberLimit > 0)
                {
                    KxWebFriendshipGroupMember member = GetGroupMemberById(GetMaxMemberId(true));
                    items.Remove(member);
                }
            }
        }

        public int GetMaxMemberId(bool temp)
        {
            int maximum = 0;

            if (temp) maximum = 900000;

            foreach (KxWebFriendshipGroupMember member in Items)
            {
                if (member.Temporary == temp && member.GroupMemberId > maximum)
                {
                    maximum = member.GroupMemberId;
                }
            }
            return maximum;
        }

        public override void NotifyDelete(KxBaseClass child)
        {
            KxWebFriendshipGroupMember member = (KxWebFriendshipGroupMember)child;
            if (Items.Contains(member))
            {
                Items.Remove(member);
            }
        }

        public bool GetAllValid()
        {
            bool result = true;

            foreach (KxWebFriendshipGroupMember member in Items)
            {
                if (!member.ValidInvitee)
                {
                    result = false;
                    break;
                }
            }
            return result;
        }

        public int GetActiveCount()
        {
            int result = 0;

            foreach (KxWebFriendshipGroupMember member in items)
            {
                if (member.RejectedDate == null)
                {
                    result++;
                }
            }
            return result;
        }

        #endregion
    }
}