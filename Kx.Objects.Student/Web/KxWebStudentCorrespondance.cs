﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;

namespace Kx.Objects.Student.Web
{
    public enum StudentCorrespondanceLoadType { KxStudentId, WebStudentCorrespondanceId, DocumentName };

    [Serializable]
    public class KxWebStudentCorrespondance: KxBaseClass
    {
        #region Attributes & Properties

        private StudentCorrespondanceLoadType loadType = StudentCorrespondanceLoadType.KxStudentId;

        protected byte[]    binaryDocument;
        protected string    fileName;
        protected string documentName;
        protected int kxStudentId;
        protected Guid      applicationGuid;
        protected bool recordFound = false;

        public int     WebStudentCorrespondanceId
        {
            get
            { return base.dBIdentifier;}
        }

        public int KxStudentId
        {
            get
            { return kxStudentId; }
            set
            {
                AddToAudit("KxStudentId", kxStudentId, value);
                kxStudentId = value;
            }
        }
        public byte[] BinaryDocument
        {
            get
            { return binaryDocument; }
            set
            {
                binaryDocument = value;
            }
        }

        public string   DocumentName
        {
            get
            { return documentName;}
            set
            { 
                AddToAudit("DocumentName", documentName, value);
                documentName = value;
            }
        }

        public string FileName
        {
            get
            { return fileName; }
            set
            {
                AddToAudit("fileName", fileName, value);
                fileName = value;
            }
        }

        public Guid ApplicationGuid
        {
            get
            { return applicationGuid;}
            set
            { 
                AddToAudit("ApplicationGuid", applicationGuid, value);
                applicationGuid = value;
            }
        }

        public bool RecordFound
        {
            get
            { return recordFound; }
        }
        
        #endregion 


        public KxWebStudentCorrespondance(KxBaseClass parent):base(parent)
        {}


        public KxWebStudentCorrespondance(KxBaseClass parent, XmlNode node):base(parent)
        {
            LoadFromXml("", node);
        }

        public KxWebStudentCorrespondance(KxBaseClass parent, string _documentName, int kxStudentId )
            : base(parent)
        {
            KxStudentId = kxStudentId;
            documentName = _documentName;
            loadType = StudentCorrespondanceLoadType.DocumentName;
            Load();
        }

        public KxWebStudentCorrespondance(KxBaseClass parent, string _documentName, Guid appGuid)
            : base(parent)
        {
            applicationGuid = appGuid;
            documentName = _documentName;
            loadType = StudentCorrespondanceLoadType.DocumentName;
            Load();
        }

        public KxWebStudentCorrespondance(KxBaseClass parent, int kxStudentId)
            : base(parent)
        {
            KxStudentId = kxStudentId;
            loadType = StudentCorrespondanceLoadType.KxStudentId;
            Load();
        }


        public KxWebStudentCorrespondance(KxBaseClass parent, int kxStudentId, int webStudentCorrespondanceId)
            : base(parent)
        {
            dBIdentifier = webStudentCorrespondanceId;
            loadType = StudentCorrespondanceLoadType.WebStudentCorrespondanceId;
            Load();
        }

        public override KxData ConfigureData(ActionType actiontype)
        {
            recordFound = false;
            KxData result = new KxData("webstudentcorrespondance", "WebStudentCorrespondance", TargetType.Table);

            if (actiontype == ActionType.Select && loadType == StudentCorrespondanceLoadType.DocumentName && KxStudentId != 0)
            {
                result.KeyFields.Add(new KxDataValue("KxStudentId", KxStudentId, KxValueType.Int));
                result.KeyFields.Add(new KxDataValue("DocumentName", documentName, KxValueType.String));
            }
            else if (actiontype == ActionType.Select && loadType == StudentCorrespondanceLoadType.KxStudentId)
            {
                result.KeyFields.Add(new KxDataValue("KxStudentId", KxStudentId, KxValueType.Int));
            }
            else if (actiontype == ActionType.Select && loadType == StudentCorrespondanceLoadType.WebStudentCorrespondanceId)
            {
                result.KeyFields.Add(new KxDataValue("WebStudentCorrespondanceId", WebStudentCorrespondanceId, KxValueType.Int));
            }
            else if (actiontype == ActionType.Select && loadType == StudentCorrespondanceLoadType.DocumentName)
            {
                result.KeyFields.Add(new KxDataValue("ApplicationGuid", ApplicationGuid, KxValueType.Guid));
                result.KeyFields.Add(new KxDataValue("DocumentName", documentName, KxValueType.String));
            }
            else
            {
                result.AutoKeyField = new KxDataValue("WebStudentCorrespondanceId", WebStudentCorrespondanceId, KxValueType.Int);
            }

            if ((actiontype == ActionType.Update) && (WebStudentCorrespondanceId == 0))
            {
                result.Action = ActionType.Insert;
            }
            else
            {
                result.Action = actiontype;
            }

            return result;
        }


        public override void LoadFromXml(string xml, XmlNode node)
        {
            dBIdentifier = KxXmlUtils.GetNodeValue(node, "webstudentcorrespondanceid", 0);
            kxStudentId = KxXmlUtils.GetNodeValue(node, "kxstudentid", 0);
            //binaryDocument = KxXmlUtils.GetNodeValue(node, "binarydocument", "");
            applicationGuid = KxXmlUtils.GetNodeValue(node, "applicationguid", Guid.Empty);
            documentName = KxXmlUtils.GetNodeValue(node, "documentname", "");
            fileName = KxXmlUtils.GetNodeValue(node, "filename", "");
            recordFound = true;
        }

    }
}
