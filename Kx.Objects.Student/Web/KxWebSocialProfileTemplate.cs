using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;

namespace Kx.Objects.Student.Web
{
    [Serializable]
    public class KxWebSocialProfileTemplate : KxBaseClass
    {
        #region Attributes & Properties

        protected DateTime? _creationDate;
        protected string _createdBy;
        protected string _field1Name;
        protected string _field1Question;
        protected int _field1Weighting;
        protected string _field2Name;
        protected string _field2Question;
        protected int _field2Weighting;
        protected string _field3Name;
        protected string _field3Question;
        protected int _field3Weighting;
        protected string _field4Name;
        protected string _field4Question;
        protected int _field4Weighting;
        protected string _field5Name;
        protected string _field5Question;
        protected int _field5Weighting;

        public int ProfileId
        {
            get { return base.dBIdentifier; }
        }

        public DateTime? CreationDate
        {
            get { return _creationDate; }
            set
            { 
                AddToAudit("CreationDate", _creationDate, value);
                _creationDate = value;
            }
        }

        public string CreatedBy
        {
            get { return _createdBy; }
            set
            { 
                AddToAudit("CreatedBy", _createdBy, value);
                _createdBy = value;
            }
        }

        public string Field1Name
        {
            get { return _field1Name; }
            set
            { 
                AddToAudit("Field1Name", _field1Name, value);
                _field1Name = value;
            }
        }

        public string Field1Question
        {
            get { return _field1Question; }
            set
            { 
                AddToAudit("Field1Question", _field1Question, value);
                _field1Question = value;
            }
        }

        public int Field1Weighting
        {
            get { return _field1Weighting; }
            set
            { 
                AddToAudit("Field1Weighting", _field1Weighting, value);
                _field1Weighting = value;
            }
        }

        public string Field2Name
        {
            get { return _field2Name; }
            set
            { 
                AddToAudit("Field2Name", _field2Name, value);
                _field2Name = value;
            }
        }

        public string Field2Question
        {
            get { return _field2Question; }
            set
            { 
                AddToAudit("Field2Question", _field2Question, value);
                _field2Question = value;
            }
        }

        public int Field2Weighting
        {
            get { return _field2Weighting; }
            set
            { 
                AddToAudit("Field2Weighting", _field2Weighting, value);
                _field2Weighting = value;
            }
        }

        public string Field3Name
        {
            get { return _field3Name; }
            set
            { 
                AddToAudit("Field3Name", _field3Name, value);
                _field3Name = value;
            }
        }

        public string Field3Question
        {
            get { return _field3Question; }
            set
            { 
                AddToAudit("Field3Question", _field3Question, value);
                _field3Question = value;
            }
        }

        public int Field3Weighting
        {
            get { return _field3Weighting; }
            set
            { 
                AddToAudit("Field3Weighting", _field3Weighting, value);
                _field3Weighting = value;
            }
        }

        public string Field4Name
        {
            get { return _field4Name; }
            set
            { 
                AddToAudit("Field4Name", _field4Name, value);
                _field4Name = value;
            }
        }

        public string Field4Question
        {
            get { return _field4Question; }
            set
            { 
                AddToAudit("Field4Question", _field4Question, value);
                _field4Question = value;
            }
        }

        public int Field4Weighting
        {
            get { return _field4Weighting; }
            set
            { 
                AddToAudit("Field4Weighting", _field4Weighting, value);
                _field4Weighting = value;
            }
        }

        public string Field5Name
        {
            get { return _field5Name; }
            set
            { 
                AddToAudit("Field5Name", _field5Name, value);
                _field5Name = value;
            }
        }

        public string Field5Question
        {
            get { return _field5Question; }
            set
            { 
                AddToAudit("Field5Question", _field5Question, value);
                _field5Question = value;
            }
        }

        public int Field5Weighting
        {
            get { return _field5Weighting; }
            set
            { 
                AddToAudit("Field5Weighting", _field5Weighting, value);
                _field5Weighting = value;
            }
        }


        #endregion 


        public KxWebSocialProfileTemplate(KxBaseClass parent):base(parent)
        { }


        public KxWebSocialProfileTemplate(KxBaseClass parent, XmlNode node):base(parent)
        {
             LoadFromXml("", node);
        }

        public KxWebSocialProfileTemplate(KxBaseClass parent,int  profileid):base(parent)
        {
             base.dBIdentifier = profileid;
             Load();
        }


        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("websocialprofiletemplate", "WebSocialProfileTemplate", TargetType.Table);

            result.AutoKeyField = new KxDataValue("ProfileId",ProfileId,KxValueType.Int);


            if ((actiontype == ActionType.Update) && (ProfileId == 0))
            {
                result.Action = ActionType.Insert;
            }
            else
            {
                result.Action = actiontype;
            }

            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {
                result.AddParameter("creationDate", _creationDate, KxValueType.DateTime, false);
                result.AddParameter("createdBy", _createdBy, KxValueType.String, 50, false);
                result.AddParameter("field1Name", _field1Name, KxValueType.String, 50, true);
                result.AddParameter("field1Question", _field1Question, KxValueType.String, 255, true);
                result.AddParameter("field1Weighting", _field1Weighting, KxValueType.Int, true);
                result.AddParameter("field2Name", _field2Name, KxValueType.String, 50, true);
                result.AddParameter("field2Question", _field2Question, KxValueType.String, 255, true);
                result.AddParameter("field2Weighting", _field2Weighting, KxValueType.Int, true);
                result.AddParameter("field3Name", _field3Name, KxValueType.String, 50, true);
                result.AddParameter("field3Question", _field3Question, KxValueType.String, 255, true);
                result.AddParameter("field3Weighting", _field3Weighting, KxValueType.Int, true);
                result.AddParameter("field4Name", _field4Name, KxValueType.String, 50, true);
                result.AddParameter("field4Question", _field4Question, KxValueType.String, 255, true);
                result.AddParameter("field4Weighting", _field4Weighting, KxValueType.Int, true);
                result.AddParameter("field5Name", _field5Name, KxValueType.String, 50, true);
                result.AddParameter("field5Question", _field5Question, KxValueType.String, 255, true);
                result.AddParameter("field5Weighting", _field5Weighting, KxValueType.Int, true);
            }
            return result;
        }


        public override  void LoadFromXml(string xml,XmlNode node)
        {
            dBIdentifier  = KxXmlUtils.GetNodeValue(node,"profileid",0);
            _creationDate  = KxXmlUtils.GetNodeValueDT(node,"creationdate",null);
            _createdBy  = KxXmlUtils.GetNodeValue(node,"createdby",string.Empty);
            _field1Name  = KxXmlUtils.GetNodeValue(node,"field1name",string.Empty);
            _field1Question  = KxXmlUtils.GetNodeValue(node,"field1question",string.Empty);
            _field1Weighting  = KxXmlUtils.GetNodeValue(node,"field1weighting",0);
            _field2Name  = KxXmlUtils.GetNodeValue(node,"field2name",string.Empty);
            _field2Question  = KxXmlUtils.GetNodeValue(node,"field2question",string.Empty);
            _field2Weighting  = KxXmlUtils.GetNodeValue(node,"field2weighting",0);
            _field3Name  = KxXmlUtils.GetNodeValue(node,"field3name",string.Empty);
            _field3Question  = KxXmlUtils.GetNodeValue(node,"field3question",string.Empty);
            _field3Weighting  = KxXmlUtils.GetNodeValue(node,"field3weighting",0);
            _field4Name  = KxXmlUtils.GetNodeValue(node,"field4name",string.Empty);
            _field4Question  = KxXmlUtils.GetNodeValue(node,"field4question",string.Empty);
            _field4Weighting  = KxXmlUtils.GetNodeValue(node,"field4weighting",0);
            _field5Name  = KxXmlUtils.GetNodeValue(node,"field5name",string.Empty);
            _field5Question  = KxXmlUtils.GetNodeValue(node,"field5question",string.Empty);
            _field5Weighting  = KxXmlUtils.GetNodeValue(node,"field5weighting",0);
        }

        public static KxWebSocialProfileTemplate GetWebSocialProfileTemplate ()
        {
            KxData data = new KxData("websocialprofiletemplate", "WebSocialProfileTemplate", TargetType.Table);
            if (data.Execute ())
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(data.XmlResults);
                XmlNodeList nodes = doc.SelectNodes(@"//websocialprofiletemplate/datarow");
                foreach(XmlNode node in nodes)
                {
                     return new KxWebSocialProfileTemplate(null, node);
                }
            }
            else
            {
                KxLogger.LogActivity("KxWebSocialProfileTemplate", "GetWebSocialProfileTemplate", data.LastError, "");
            }

            return new KxWebSocialProfileTemplate(null);
        }
    }
}

