using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;

namespace Kx.Objects.Student.Web
{
    [Serializable]
    public class KxWebStudentPaymentBatch : KxBaseClass
    {
        #region Attributes & Properties
        protected bool loaded = false;
        protected int      studentChargeId;
        protected string   statementText;
        protected decimal  outstandingBalance;
        protected int      webPaymentId;

        public Guid     UniqueId
        {
            get
            { return base.guidIdentifier;}
         
        }

        public int      StudentChargeId
        {
            get
            { return studentChargeId;}
            set
            { 
                AddToAudit("StudentChargeId", studentChargeId, value);
                studentChargeId = value;
            }
        }

        public string   StatementText
        {
            get
            { return statementText;}
            set
            { 
                AddToAudit("StatementText", statementText, value);
                statementText = value;
            }
        }

        public decimal  OutstandingBalance
        {
            get
            { return outstandingBalance;}
            set
            { 
                AddToAudit("OutstandingBalance", outstandingBalance, value);
                outstandingBalance = value;
            }
        }

        public int WebPaymentId
        {
            get
            { return webPaymentId; }
            set
            {
                AddToAudit("WebPaymentId", webPaymentId, value);
                webPaymentId = value;
            }
        }


        #endregion 


        public KxWebStudentPaymentBatch(KxBaseClass parent):base(parent)
        {}


        public KxWebStudentPaymentBatch(KxBaseClass parent, XmlNode node):base(parent)
        {
             LoadFromXml("", node);
        }

        public KxWebStudentPaymentBatch(KxBaseClass parent,Guid     batchid):base(parent)
        {
             base.guidIdentifier = batchid;
             Load();
        }


        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("webstudentpaymentbatch", "WebStudentPaymentBatch", TargetType.Table);


            if (actiontype == ActionType.Update && guidIdentifier == Guid.Empty && changed)
            {
                guidIdentifier = Guid.NewGuid();
                result.Action = ActionType.Insert;
            }
            else
            {
                result.Action = actiontype;
            }

            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {
                result.AddKeyField("UniqueId", guidIdentifier, KxValueType.Guid);
                result.AddParameter("studentChargeId", studentChargeId, KxValueType.Int);
                result.AddParameter("statementText", statementText, KxValueType.String, 200, true);
                result.AddParameter("outstandingBalance", outstandingBalance, KxValueType.Decimal, true);
                result.AddParameter("webpaymentId", webPaymentId, KxValueType.Int, true);
            }
            return result;
        }


        public override  void LoadFromXml(string xml,XmlNode node)
        {
            guidIdentifier = KxXmlUtils.GetNodeValue(node, "uniqueid", Guid.Empty);
            studentChargeId  = KxXmlUtils.GetNodeValue(node,"studentchargeid",0);
            statementText  = KxXmlUtils.GetNodeValue(node,"statementtext","");
            outstandingBalance  = KxXmlUtils.GetNodeValue(node,"outstandingbalance",(decimal)0.00M);
            webPaymentId = KxXmlUtils.GetNodeValue(node, "webpaymentid", 0);
            loaded = true;
        }
    }
}

