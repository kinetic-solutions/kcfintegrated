using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;

namespace Kx.Objects.Student.Web
{
    [Serializable]
    public class KxWebFriendshipGroup : KxBaseClass
    {
        #region Attributes & Properties

        protected DateTime ?creationDate;
        protected string   groupName;
        protected string   studentId;
        protected string   surName;
        protected int      academicYearId;
        protected Guid     applicationGuid;
        protected Guid     groupLeaderUserGuid;
        protected bool     loadByGuid;
        protected KxWebUserStudent leader = null;

        //Temporary UI storage
        protected string emailSubject = "";
        protected string emailBody = "";

        protected KxWebFriendshipGroupMembers members = null;

        public int      FriendshipGroupId
        {
            get
            { return base.dBIdentifier;}
        }

        public DateTime ?CreationDate
        {
            get
            { return creationDate;}
            set
            { 
                AddToAudit("CreationDate", creationDate, value);
                creationDate = value;
            }
        }

        public string   GroupName
        {
            get
            { return groupName;}
            set
            { 
                AddToAudit("GroupName", groupName, value);
                groupName = value;
            }
        }

        public string GroupCode
        {
            get { return string.Format("{0}{1}", GroupName, FriendshipGroupId); }
        }

        public int      AcademicYearId
        {
            get
            { return academicYearId;}
            set
            { 
                AddToAudit("AcademicYearId", academicYearId, value);
                academicYearId = value;
            }
        }

        public Guid     ApplicationGuid
        {
            get
            { return applicationGuid; }
            set
            {
                AddToAudit("applicationGuid", applicationGuid, value);
                applicationGuid = value;
            }
        }

        public Guid GroupLeaderUserGuid
        {
            get
            { return groupLeaderUserGuid; }
            set
            {
                AddToAudit("GroupLeaderUserGuid", groupLeaderUserGuid, value);
                groupLeaderUserGuid = value;
            }
        }

        public KxWebFriendshipGroupMembers Members
        {
            get
            {
                if (members == null)
                {
                    members = new KxWebFriendshipGroupMembers(this,FriendshipGroupId);
                }
                return members;
            }
        }

        public KxWebUserStudent Leader
        {
            get
            {
                if (leader == null)
                {
                    leader = new KxWebUserStudent(this, groupLeaderUserGuid);
                }
                return leader;
            }
            set
            {
                if (leader == value)
                    return;
                leader = value;
            }
        }

        //Temporary UI storage
        public string EmailSubject
        {
            get
            { return emailSubject; }
            set
            { emailSubject = value; }
        }
        public string EmailBody
        {
            get
            { return emailBody; }
            set
            { emailBody = value; }
        }
        
        #endregion 

        public KxWebFriendshipGroup(KxBaseClass parent) : base(parent)
        {
            this.isKxClass = false;
        }

        public KxWebFriendshipGroup(KxBaseClass parent, int friendshipgroupid)
            : this(parent)
        {
             base.dBIdentifier = friendshipgroupid;
             loadByGuid = false;
             Load();
        }

        public KxWebFriendshipGroup(KxBaseClass parent, Guid applicationGuid)
            : this(parent)
        {
            this.applicationGuid = applicationGuid;
            loadByGuid = true;
            Load();
        }
        
        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("webfriendshipgroup", "WebFriendshipGroup", TargetType.Table);

            if (loadByGuid && actiontype == ActionType.Select)
            {
                result.Target = TargetType.StoredProc;
                result.KeyFields.Add(new KxDataValue("ApplicationGuid", applicationGuid));
            }
            else
            {
                result.AutoKeyField = new KxDataValue("FriendshipGroupId", FriendshipGroupId, KxValueType.Int);
            }

            if ((actiontype == ActionType.Update) && (FriendshipGroupId == 0))
            {
                result.Action = ActionType.Insert;
            }
            else
            {
                result.Action = actiontype;
            }

            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {
                result.AddParameter("creationDate", creationDate, KxValueType.DateTime, true);
                result.AddParameter("groupName", groupName, KxValueType.String, 50, true);
                result.AddParameter("academicYearId", academicYearId, KxValueType.Int, false);
                result.AddParameter("applicationGuid", applicationGuid, KxValueType.Guid, true);
                result.AddParameter("groupLeaderUserGuid", groupLeaderUserGuid, KxValueType.Guid, true);
            }
            return result;
        }

        public override void LoadFromXml(string xml,XmlNode node)
        {
            Children.Remove(leader);
            Children.Remove(members);
            leader = null;
            members = null;

            dBIdentifier = KxXmlUtils.GetNodeValue(node, "friendshipgroupid", 0);
            creationDate = KxXmlUtils.GetNodeValueDT(node, "creationdate", null);
            groupName = KxXmlUtils.GetNodeValue(node, "groupname", "");
            academicYearId = KxXmlUtils.GetNodeValue(node, "academicyearid", 0);
            ApplicationGuid = KxXmlUtils.GetNodeValue(node, "applicationguid", Guid.Empty);
            groupLeaderUserGuid = KxXmlUtils.GetNodeValue(node, "groupleaderuserguid", Guid.Empty);
        }

        public KxWebFriendshipGroupMember AddMember(string studentid, string surname)
        {
            KxWebFriendshipGroupMember member = new KxWebFriendshipGroupMember(Members);
            member.CreationDate = DateTime.Now;
            member.MemberStudentId = studentid;
            member.MemberSurName = surname;
            Members.Items.Add(member);

            return member;
        }

        public KxWebFriendshipGroupMember GetGroupMember(KxWebUserStudent user)
        {
            return Members.GetGroupMember(user);
        }

        public KxWebFriendshipGroupMember GetGroupMemberById(int groupMemberId)
        {
            return Members.GetGroupMemberById(groupMemberId);
        }

        public bool HasInvitations(KxWebUserStudent webUser)
        {
            bool result = false;
            KxWebFriendshipGroupMember member = GetGroupMember(webUser);

            if (member != null)
            {
                result = member.HasInvitation;
            }
            return result;
        }

        public bool IsGroupLeader(KxWebUserStudent webUser)
        {
            return webUser.UserGuid == groupLeaderUserGuid;
        }

        public bool IsGroupMember(KxWebUserStudent webUser)
        {
            bool result = false;
            KxWebFriendshipGroupMember member = GetGroupMember(webUser);

            if (member != null)
            {
                result = member.AcceptedDate.HasValue;
            }
            return result;
        }
    }
}

