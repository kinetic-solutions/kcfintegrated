using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Linq;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;

namespace Kx.Objects.Student.Web
{
    [Serializable]
    public class KxPropertyDetail : KxBaseClass
    {
        #region Attributes & Properties

        protected int _siteId;
        protected int _areaId;
        protected int _blockId;
        protected string _region;
        protected string _cityName;
        protected string _propertyName;
        protected string _propertyAddress;
        protected string _propertyTelephone;
        protected string _propertyEmail;
        protected string _propertyLandlord;
        protected string _propertyContractResource;
        protected string _propertyTCResource;
        protected string _additional1;
        protected string _additional2;
        protected string _additional3;
        protected string _additional4;
        protected string _additional5;
        protected string _additional6;
        protected string _additional7;
        protected string _additional8;
        protected string _additional9;
        protected string _additional10;
        protected string _additional11;
        protected string _additional12;
        protected string _additional13;
        protected string _additional14;
        protected string _additional15;
        protected string _additional16;
        protected string _additional17;
        protected string _additional18;
        protected string _additional19;
        protected string _additional20;

        public int PropertyDetailId
        {
            get { return base.dBIdentifier; }
        }

        public int SiteId
        {
            get { return _siteId; }
            set
            { 
                AddToAudit("SiteId", _siteId, value);
                _siteId = value;
            }
        }

        public int AreaId
        {
            get { return _areaId; }
            set
            { 
                AddToAudit("AreaId", _areaId, value);
                _areaId = value;
            }
        }

        public int BlockId
        {
            get { return _blockId; }
            set
            { 
                AddToAudit("BlockId", _blockId, value);
                _blockId = value;
            }
        }

        public string Region
        {
            get { return _region; }
            set
            { 
                AddToAudit("Region", _region, value);
                _region = value;
            }
        }

        public string CityName
        {
            get { return _cityName; }
            set
            { 
                AddToAudit("CityName", _cityName, value);
                _cityName = value;
            }
        }

        public string PropertyName
        {
            get { return _propertyName; }
            set
            { 
                AddToAudit("PropertyName", _propertyName, value);
                _propertyName = value;
            }
        }

        public string PropertyAddress
        {
            get { return _propertyAddress; }
            set
            { 
                AddToAudit("PropertyAddress", _propertyAddress, value);
                _propertyAddress = value;
            }
        }

        public string PropertyTelephone
        {
            get { return _propertyTelephone; }
            set
            { 
                AddToAudit("PropertyTelephone", _propertyTelephone, value);
                _propertyTelephone = value;
            }
        }

        public string PropertyEmail
        {
            get { return _propertyEmail; }
            set
            { 
                AddToAudit("PropertyEmail", _propertyEmail, value);
                _propertyEmail = value;
            }
        }

        public string PropertyLandlord
        {
            get { return _propertyLandlord; }
            set
            { 
                AddToAudit("PropertyLandlord", _propertyLandlord, value);
                _propertyLandlord = value;
            }
        }

        public string PropertyContractResource
        {
            get { return _propertyContractResource; }
            set
            { 
                AddToAudit("PropertyContractResource", _propertyContractResource, value);
                _propertyContractResource = value;
            }
        }

        public string PropertyTCResource
        {
            get { return _propertyTCResource; }
            set
            { 
                AddToAudit("PropertyTCResource", _propertyTCResource, value);
                _propertyTCResource = value;
            }
        }

        public string Additional1
        {
            get { return _additional1; }
            set
            { 
                AddToAudit("Additional1", _additional1, value);
                _additional1 = value;
            }
        }

        public string Additional2
        {
            get { return _additional2; }
            set
            { 
                AddToAudit("Additional2", _additional2, value);
                _additional2 = value;
            }
        }

        public string Additional3
        {
            get { return _additional3; }
            set
            { 
                AddToAudit("Additional3", _additional3, value);
                _additional3 = value;
            }
        }

        public string Additional4
        {
            get { return _additional4; }
            set
            { 
                AddToAudit("Additional4", _additional4, value);
                _additional4 = value;
            }
        }

        public string Additional5
        {
            get { return _additional5; }
            set
            { 
                AddToAudit("Additional5", _additional5, value);
                _additional5 = value;
            }
        }

        public string Additional6
        {
            get { return _additional6; }
            set
            { 
                AddToAudit("Additional6", _additional6, value);
                _additional6 = value;
            }
        }

        public string Additional7
        {
            get { return _additional7; }
            set
            { 
                AddToAudit("Additional7", _additional7, value);
                _additional7 = value;
            }
        }

        public string Additional8
        {
            get { return _additional8; }
            set
            { 
                AddToAudit("Additional8", _additional8, value);
                _additional8 = value;
            }
        }

        public string Additional9
        {
            get { return _additional9; }
            set
            { 
                AddToAudit("Additional9", _additional9, value);
                _additional9 = value;
            }
        }

        public string Additional10
        {
            get { return _additional10; }
            set
            { 
                AddToAudit("Additional10", _additional10, value);
                _additional10 = value;
            }
        }

        public string Additional11
        {
            get { return _additional11; }
            set
            { 
                AddToAudit("Additional11", _additional11, value);
                _additional11 = value;
            }
        }

        public string Additional12
        {
            get { return _additional12; }
            set
            { 
                AddToAudit("Additional12", _additional12, value);
                _additional12 = value;
            }
        }

        public string Additional13
        {
            get { return _additional13; }
            set
            { 
                AddToAudit("Additional13", _additional13, value);
                _additional13 = value;
            }
        }

        public string Additional14
        {
            get { return _additional14; }
            set
            { 
                AddToAudit("Additional14", _additional14, value);
                _additional14 = value;
            }
        }

        public string Additional15
        {
            get { return _additional15; }
            set
            { 
                AddToAudit("Additional15", _additional15, value);
                _additional15 = value;
            }
        }

        public string Additional16
        {
            get { return _additional16; }
            set
            {
                AddToAudit("Additional16", _additional16, value);
                _additional16 = value;
            }
        }


        public string Additional17
        {
            get { return _additional17; }
            set
            {
                AddToAudit("Additional17", _additional17, value);
                _additional17 = value;
            }
        }

        public string Additional18
        {
            get { return _additional18; }
            set
            {
                AddToAudit("Additional18", _additional18, value);
                _additional18 = value;
            }
        }

        public string Additional19
        {
            get { return _additional19; }
            set
            {
                AddToAudit("Additional19", _additional19, value);
                _additional19 = value;
            }
        }

        public string Additional20
        {
            get { return _additional20; }
            set
            {
                AddToAudit("Additional20", _additional20, value);
                _additional20 = value;
            }
        }

        #endregion 


        public KxPropertyDetail(KxBaseClass parent):base(parent)
        { }


        public KxPropertyDetail(KxBaseClass parent, XmlNode node):base(parent)
        {
             LoadFromXml("", node);
        }

        public KxPropertyDetail(KxBaseClass parent,int  propertydetailid):base(parent)
        {
             base.dBIdentifier = propertydetailid;
             Load();
        }


        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("propertydetails", "kcf.PropertyDetails", TargetType.Table);

            result.AutoKeyField = new KxDataValue("PropertyDetailId",PropertyDetailId,KxValueType.Int);


            if ((actiontype == ActionType.Update) && (PropertyDetailId == 0))
            {
                result.Action = ActionType.Insert;
            }
            else
            {
                result.Action = actiontype;
            }

            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {
                result.AddParameter("siteId", _siteId, KxValueType.Int, false);
                result.AddParameter("areaId", _areaId, KxValueType.Int, false);
                result.AddParameter("blockId", _blockId, KxValueType.Int, false);
                result.AddParameter("region", _region, KxValueType.String, 50, true);
                result.AddParameter("cityName", _cityName, KxValueType.String, 50, true);
                result.AddParameter("propertyName", _propertyName, KxValueType.String, 50, true);
                result.AddParameter("propertyAddress", _propertyAddress, KxValueType.String, 1000, true);
                result.AddParameter("propertyTelephone", _propertyTelephone, KxValueType.String, 50, true);
                result.AddParameter("propertyEmail", _propertyEmail, KxValueType.String, 255, true);
                result.AddParameter("propertyLandlord", _propertyLandlord, KxValueType.String, 1000, true);
                result.AddParameter("propertyContractResource", _propertyContractResource, KxValueType.String, 255, true);
                result.AddParameter("propertyTCResource", _propertyTCResource, KxValueType.String, 255, true);
                result.AddParameter("additional1", _additional1, KxValueType.String, 255, true);
                result.AddParameter("additional2", _additional2, KxValueType.String, 255, true);
                result.AddParameter("additional3", _additional3, KxValueType.String, 255, true);
                result.AddParameter("additional4", _additional4, KxValueType.String, 255, true);
                result.AddParameter("additional5", _additional5, KxValueType.String, 255, true);
                result.AddParameter("additional6", _additional6, KxValueType.String, 255, true);
                result.AddParameter("additional7", _additional7, KxValueType.String, 255, true);
                result.AddParameter("additional8", _additional8, KxValueType.String, 255, true);
                result.AddParameter("additional9", _additional9, KxValueType.String, 255, true);
                result.AddParameter("additional10", _additional10, KxValueType.String, 255, true);
                result.AddParameter("additional11", _additional11, KxValueType.String, 255, true);
                result.AddParameter("additional12", _additional12, KxValueType.String, 255, true);
                result.AddParameter("additional13", _additional13, KxValueType.String, 255, true);
                result.AddParameter("additional14", _additional14, KxValueType.String, 255, true);
                result.AddParameter("additional15", _additional15, KxValueType.String, 255, true);
                result.AddParameter("additional16", _additional16, KxValueType.String, 255, true);
                result.AddParameter("additional17", _additional17, KxValueType.String, 255, true);
                result.AddParameter("additional18", _additional18, KxValueType.String, 255, true);
                result.AddParameter("additional19", _additional19, KxValueType.String, 255, true);
                result.AddParameter("additional20", _additional20, KxValueType.String, 255, true);
            }
            return result;
        }


        public override  void LoadFromXml(string xml,XmlNode node)
        {
            dBIdentifier  = KxXmlUtils.GetNodeValue(node,"propertydetailid",0);
            _siteId  = KxXmlUtils.GetNodeValue(node,"siteid",0);
            _areaId  = KxXmlUtils.GetNodeValue(node,"areaid",0);
            _blockId  = KxXmlUtils.GetNodeValue(node,"blockid",0);
            _region  = KxXmlUtils.GetNodeValue(node,"region",string.Empty);
            _cityName  = KxXmlUtils.GetNodeValue(node,"cityname",string.Empty);
            _propertyName  = KxXmlUtils.GetNodeValue(node,"propertyname",string.Empty);
            _propertyAddress  = KxXmlUtils.GetNodeValue(node,"propertyaddress",string.Empty);
            _propertyTelephone  = KxXmlUtils.GetNodeValue(node,"propertytelephone",string.Empty);
            _propertyEmail  = KxXmlUtils.GetNodeValue(node,"propertyemail",string.Empty);
            _propertyLandlord  = KxXmlUtils.GetNodeValue(node,"propertylandlord",string.Empty);
            _propertyContractResource  = KxXmlUtils.GetNodeValue(node,"propertycontractresource",string.Empty);
            _propertyTCResource  = KxXmlUtils.GetNodeValue(node,"propertytcresource",string.Empty);
            _additional1  = KxXmlUtils.GetNodeValue(node,"additional1",string.Empty);
            _additional2  = KxXmlUtils.GetNodeValue(node,"additional2",string.Empty);
            _additional3  = KxXmlUtils.GetNodeValue(node,"additional3",string.Empty);
            _additional4  = KxXmlUtils.GetNodeValue(node,"additional4",string.Empty);
            _additional5  = KxXmlUtils.GetNodeValue(node,"additional5",string.Empty);
            _additional6  = KxXmlUtils.GetNodeValue(node,"additional6",string.Empty);
            _additional7  = KxXmlUtils.GetNodeValue(node,"additional7",string.Empty);
            _additional8  = KxXmlUtils.GetNodeValue(node,"additional8",string.Empty);
            _additional9  = KxXmlUtils.GetNodeValue(node,"additional9",string.Empty);
            _additional10  = KxXmlUtils.GetNodeValue(node,"additional10",string.Empty);
            _additional11  = KxXmlUtils.GetNodeValue(node,"additional11",string.Empty);
            _additional12  = KxXmlUtils.GetNodeValue(node,"additional12",string.Empty);
            _additional13  = KxXmlUtils.GetNodeValue(node,"additional13",string.Empty);
            _additional14  = KxXmlUtils.GetNodeValue(node,"additional14",string.Empty);
            _additional15  = KxXmlUtils.GetNodeValue(node,"additional15",string.Empty);
            _additional16  = KxXmlUtils.GetNodeValue(node,"additional16",string.Empty);
            _additional17  = KxXmlUtils.GetNodeValue(node,"additional17",string.Empty);
            _additional18  = KxXmlUtils.GetNodeValue(node,"additional18",string.Empty);
            _additional19  = KxXmlUtils.GetNodeValue(node,"additional19",string.Empty);
            _additional20  = KxXmlUtils.GetNodeValue(node,"additional20",string.Empty);
        }

        public static IEnumerable<KxPropertyDetail> GetAllPropertyDetails ()
        {
            KxData data = new KxData("propertydetails", "kcf.PropertyDetails", TargetType.Table);
            if (data.Execute ())
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(data.XmlResults);
                XmlNodeList nodes = doc.SelectNodes(@"//propertydetails/datarow");
                foreach(XmlNode node in nodes)
                {
                     yield return new KxPropertyDetail(null, node);
                }
            }
        }
        public static KxPropertyDetail GetPropertyDetail(int siteId, int areaId, int blockId)
        {
            var result = from p in KxPropertyDetail.GetAllPropertyDetails()
                         where
                             (p.SiteId == 0 || p.SiteId == siteId)
                             && (p.AreaId == 0 || p.AreaId == areaId)
                             && (p.BlockId == 0 || p.BlockId == blockId)
                         select p;
            return result.FirstOrDefault();
        }

    }
}

