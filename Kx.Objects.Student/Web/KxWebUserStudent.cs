using System;
using System.Drawing;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Configuration;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Objects.Common.Web;
using Kx.Core.Exceptions;
using Kx.Objects.Common;
using System.Threading;
using System.Globalization;
using System.Web;

namespace Kx.Objects.Student.Web
{
    public enum LoadType {Email,Guid,User,Student};

    [Serializable]
    public class KxWebUserStudent : KxWebUser
    {
        #region Attributes & Properties
        protected int userAllocationDateId = 0;
        protected int controlAllocationDateId = 0;
        protected int defaultAcademicYear = 0;
        protected int specifiedAllocationId = 0;
        protected int specifiedAcceptedAllocationId = 0;
        protected int specifiedRejectedAllocationId = 0;
        protected int specifiedPendingAllocationId = 0;
        protected decimal fourWeekAmountWithCharge = 0;
        protected decimal uplift = 0;

        protected string userSiteLookupId;
        protected string controlSiteLookupId;

        public string UserLibertyRoomId;
        public string ControlLibertyRoomId;

        public string UserCityRoomId;
        public string ControlCityRoomId;

        public string UserOpalRoomId;
        public string ControlOpalRoomId;

        public string UserAllocationPricing;
        public string ControlAllocationPricing;

        protected KxWebAllocationPricing editAllocationPricing = null;
        protected KxWebOpalAllocationPricing editOpalAllocationPricing = null;

        protected KxBookingEligibilityRule editBookingEligibilityRule = null;

        protected KxSiteAvailabilityLookup editSiteAvailabilityLookup = null;
        protected KxWebAdjustedDueDate editAdjustedDueDate = null;


        protected string userWebResource = "";
        protected string controlWebResource = "";

        public string UserAdjustedDueDate;
        public string ControlAdjustedDueDate;

        protected KxWebStudentRoomCityRate currentCityRoom = null;
        protected KxWebStudentRoomLibertyRate currentLibertyRoom = null;
        protected KxSiteAvailabilityLookup currentSiteLookup = null;

        protected KxStudent student = null;
        protected KxWebStudentAllocations allocations = null;
        protected KxWebStudentApplications applications = null;
        protected KxWebStudentAllocation specifiedAllocation = null;
        protected KxWebStudentAllocation specifiedAcceptedAllocation = null;
        protected KxWebStudentAllocation specifiedPendingAllocation = null;
        protected KxWebStudentAllocation specifiedRejectedAllocation = null;
        protected KxWebUserSynchronisation userSynchronisation = null;

        protected Dictionary<string, RawPaymentDataItem> _lastPaymentRawData = null;

        protected Guid? _ApplicationGuid = null; // pha

        public string UserSiteLookupId
        {
            get
            {
                return userSiteLookupId;
            }
            set
            {
                userSiteLookupId = value;
            }
        }
        public string ControlSiteLookupId
        {
            get
            {
                return controlSiteLookupId;
            }
            set
            {
                controlSiteLookupId = value;
            }
        }

        public string UserWebResource
        {
            get
            {
                return userWebResource;
            }
            set
            {
                userWebResource = value;
            }
        }
        public string ControlWebResource
        {
            get
            {
                return controlWebResource;
            }
            set
            {
                controlWebResource = value;
            }
        }

        public string CurrentAllocationPricing
        {
            get
            {
                if (!String.IsNullOrEmpty(ControlAllocationPricing))
                {
                    return ControlAllocationPricing;
                }
                else
                {
                    return UserAllocationPricing;
                }
            }
        }

        public string CurrentAdjustedDueDateId
        {
            get
            {
                if (!String.IsNullOrEmpty(ControlAdjustedDueDate))
                {
                    return ControlAdjustedDueDate;
                }
                else
                {
                    return UserAdjustedDueDate;
                }
            }
        }

        public KxWebAdjustedDueDate CurrentAdjustedDueDate
        {
            get
            {
                if (!String.IsNullOrEmpty(CurrentAdjustedDueDateId))
                {
                    return KxStudentObjectManager.AdjustedDueDates.GetAdjustedDueDate(CurrentAdjustedDueDateId);
                }
                else
                {
                    return null;
                }
            }
        }

        public KxWebAllocationPricing CurrentAllocationPricingObject
        {
            get
            {
                return KxStudentObjectManager.WebAllocationPricingList.GetWebAllocationPricing(CurrentAllocationPricing);
            }
        }

        public KxWebAdjustedDueDate EditAdjustedDueDate
        {
            get
            {
                return editAdjustedDueDate;
            }
            set
            {
                editAdjustedDueDate = value;
            }
        }

        public KxWebAllocationPricing EditAllocationPricing
        {
            get
            {
                return editAllocationPricing;
            }
            set
            {
                editAllocationPricing = value;
            }
        }

        public KxSiteAvailabilityLookup EditSiteAvailabilityLookup
        {
            get
            {
                return editSiteAvailabilityLookup;
            }
            set
            {
                editSiteAvailabilityLookup = value;
            }
        }

        public string CurrentWebResource
        {
            get
            {
                if (!String.IsNullOrEmpty(ControlWebResource))
                {
                    return ControlWebResource;
                }
                else
                {
                    return UserWebResource;
                }
            }
        }


        public string CurrentLibertyRoomId
        {
            get
            {
                if (!String.IsNullOrEmpty(ControlLibertyRoomId))
                {
                    return ControlLibertyRoomId;
                }
                else 
                {
                    return UserLibertyRoomId;
                }
            }
        }

        public KxWebStudentRoomLibertyRate CurrentLibertyRoom
        {
            get
            {
                if (!String.IsNullOrEmpty(CurrentLibertyRoomId) && (currentLibertyRoom == null || currentLibertyRoom.UniqueId != CurrentLibertyRoomId))
                {
                    currentLibertyRoom = CurrentApplication.LibertyRoomRates.GetRoomByUniqueId(CurrentLibertyRoomId);
                }
                return currentLibertyRoom;
            }
        }

        public string CurrentCityRoomId
        {
            get
            {
                if (!String.IsNullOrEmpty(ControlCityRoomId))
                {
                    return ControlCityRoomId;
                }
                else
                {
                    return UserCityRoomId;
                }
            }
        }

        public KxWebStudentRoomCityRate CurrentCityRoom
        {
            get
            {
                if (!String.IsNullOrEmpty(CurrentCityRoomId) && (currentCityRoom == null || currentCityRoom.UniqueId != CurrentCityRoomId))
                {
                    currentCityRoom = CurrentApplication.CityRoomRates.GetRoomByUniqueId(CurrentCityRoomId);
                }
                return currentCityRoom;
            }
        }

        public decimal CityPaymentMethodUplift
        {
            get
            {
                decimal amount = 0;
                if ((CurrentCityRoom != null) && (CurrentApplication != null))
                {
                    string payMethodAndBrand = CurrentApplication.UserDefinedValues.GetUDF(30); // Hard Code must be 30 on website.

                    //string checkPayMethodAndBrand = payMethodAndBrand + ";";

                    //string[] sapayMethods = checkPayMethodAndBrand.Split(';');

                    //string payMethod = sapayMethods[0];

                    //string payBrand = "";

                    //if (sapayMethods.Length > 1)
                    //{
                    //    payBrand = sapayMethods[1];
                    //}

                    KxCustomSetting result = new KxCustomSetting(null, "OGONE.Payment.Method", payMethodAndBrand);

                    uplift = Convert.ToDecimal(result.String4);

                    if (uplift > 0)
                    {
                        amount = CurrentCityRoom.FourWeeklyAmount * uplift / 100;
                    }
                }
                return amount;
            }

        }

        public string CityFourWeeklyAmountWithCharge
        {
            get
            {
                if (CurrentCityRoom != null)
                {
                    try
                    {
                        fourWeekAmountWithCharge = CurrentCityRoom.FourWeeklyAmount + CityPaymentMethodUplift;
                    }
                    catch
                    {
                        fourWeekAmountWithCharge = currentCityRoom.FourWeeklyAmount;
                        KxLogger.LogActivity(this, "KxWebStudentRoomCityRate.CityFourWeeklyAmountWithCharge", "Error Getting CityPaymentMethodUplift" , "");
                    }
                }
                return ValueToString(fourWeekAmountWithCharge);
            }
        }

        public string CurrentSiteLookupId
        {
            get
            {
                if (!String.IsNullOrEmpty(ControlSiteLookupId))
                {
                    return ControlSiteLookupId;
                }
                else
                {
                    return UserSiteLookupId;
                }
            }
            set
            {
                CurrentSiteLookup = null;
                UserSiteLookupId = value;
            }
        }

        public KxWebOpalAllocationPricing CurrentOpalAllocationPricingObject
        {
            get
            {
                return KxStudentObjectManager.WebOpalAllocationPricingList.GetWebAllocationPricing(CurrentAllocationPricing);
            }
        }


        public KxWebOpalAllocationPricing EditOpalAllocationPricing
        {
            get
            {
                return editOpalAllocationPricing;
            }
            set
            {
                editOpalAllocationPricing = value;
            }
        }

        public KxBookingEligibilityRule EditBookingEligibilityRule
        {
            get
            {
                return editBookingEligibilityRule;
            }
            set
            {
                editBookingEligibilityRule = value;
            }
        }

        private string ValueToString(decimal dValue)
        {
            string sResult;
            NumberFormatInfo numberInfo = Thread.CurrentThread.CurrentCulture.NumberFormat;

            sResult = dValue.ToString("G", numberInfo);
            //KxLogger.LogActivity(this, "KxWebStudentRoomCityRate.FourWeeklyAmountString", sResult, "");
            return sResult;
        }

        public KxSiteAvailabilityLookup ControlSiteLookup
        {
            get
            {
               return KxStudentObjectManager.SiteAvailabilityLookups.GetSiteAvailabilityLookup(ControlSiteLookupId);
            }
        }

        public KxSiteAvailabilityLookup UserSiteLookup
        {
            get
            {
                return KxStudentObjectManager.SiteAvailabilityLookups.GetSiteAvailabilityLookup(UserSiteLookupId);
            }
        }

        public KxSiteAvailabilityLookup CurrentSiteLookup
        {
            get
            {
                if (!String.IsNullOrEmpty(CurrentSiteLookupId) && (currentSiteLookup == null || currentSiteLookup.UniqueId != CurrentSiteLookupId))
                {
                    currentSiteLookup = KxStudentObjectManager.SiteAvailabilityLookups.GetSiteAvailabilityLookup(CurrentSiteLookupId);
                }
                return currentSiteLookup;
            }
            set
            {
                currentSiteLookup = value;
                userSiteLookupId = "";
                controlSiteLookupId = "";
            }
        }

        public bool ControlSiteEqualsUserSite
        {
            get
            {
                return GetControlSiteEqualsUserSite();
            }
        }

        public bool ControlSiteEqualsUserSiteOrSingleSite
        {
            get
            {
                return ControlSiteEqualsUserSite || CurrentSiteLookup.IsSingleSite;
            }
        }

        public bool CurrentSiteOrPrimary
        {
            get
            {
                try
                {
                    return ControlSiteEqualsUserSite || !CurrentSiteLookup.IsSecondary;
                }
                catch
                {
                    return ControlSiteEqualsUserSite;
                }
            }
        }

        public bool NotCurrentSiteAndPrimary
        {
            get
            {
                try
                {
                    return !ControlSiteEqualsUserSite && CurrentSiteLookup.IsPrimaryAndIsMultiSite;
                }
                catch
                {
                    return ControlSiteEqualsUserSite;
                }
            }
        }

        public int DefaultAcademicYear
        {
            get
            {
                if (defaultAcademicYear == 0)
                {
                    try
                    {
                        defaultAcademicYear = KxDbConfiguration.GetConfigurationInt("DefaultAcademicYear", 0);
                    }
                    catch { }
                }
                return defaultAcademicYear;
            }
        }

        public int CurrentAcademicYear
        {
            get
            {
                if (ControlAcademicYear == 99999)
                {
                    return DefaultAcademicYear;
                }
                else if (UserAcademicYear == 99999)
                {
                    return DefaultAcademicYear;
                }
                else if (ControlAcademicYear != 0)
                {
                    return ControlAcademicYear;
                }
                else if (UserAcademicYear != 0)
                {
                    return UserAcademicYear;
                }
                else
                {
                    return DefaultAcademicYear;
                }
            }
        }

        public int CurrentAcademicYearSession
        {
            get
            {
                if (ControlAcademicYearSession > -1)
                {
                    return ControlAcademicYearSession;
                }
                else if (UserAcademicYearSession > -1)
                {
                    return UserAcademicYearSession;
                }

                return 0;
            }
        }

        public int UserAcademicYear { get; set; }

        public int UserAcademicYearSession { get; set; }

        public Guid? UserApplicationGuid { get; set; }

        public int ControlAcademicYear { get; set; }

        public int ControlAcademicYearSession { get; set; }

        public Guid? ControlApplicationGuid { get; set; }

        public KxStudentAllocationDate CurrentAllocationDate
        {
            get
            { return GetCurrentAllocationDate(); }
        }

        public int CurrentAllocationDateId
        {
            get
            {
                if (controlAllocationDateId != 0)
                {
                    return controlAllocationDateId;
                }
                else
                {
                    return userAllocationDateId;
                }
            }
        }
        public int UserAllocationDateId
        {
            get
            { return userAllocationDateId; }
            set
            { userAllocationDateId = value; }
        }
        public int ControlAllocationDateId
        {
            get
            { return controlAllocationDateId; }
            set
            { controlAllocationDateId = value; }
        }
        public int AllocationsCount
        {
            get
            { return Allocations.Items.Count; }
        }

        public string BestEmailAddress
        {
            get
            {
                if (emailAddress == "" && Student != null)
                {
                    try
                    {
                        emailAddress = Student.BestAddress.EmailAddress;
                    }
                    catch
                    { }
                }
                return emailAddress;
            }
            set
            {
                AddToAudit("EmailAddress", emailAddress, value);
                emailAddress = value;
            }
        }
     
        public KxStudent Student
        {
            get
            {
                if (student == null && kxStudentId == 0)
                {
                    student = new KxStudent(this);
                }
                else if ((student == null || student.KxStudentId == 0) && kxStudentId > 0)
                {
                    student = new KxStudent(null, kxStudentId);
                } 
                return student;
            }
            set
            {
                
                student = value;
            }
        }
        public KxWebStudentApplications Applications
        {
            get
            {
                if (applications == null)
                {
                    applications = new KxWebStudentApplications(this, guidIdentifier);
                    UserSynchronisation.SynchroniseUser();
                }
                return applications;
            }
            set
            {
                applications = value;
            }
        }

        public List<KxStudentRequest> StudentRequests
        {
            get
            {
                if (this.kxStudentId > 0)
                {
                    return KxStudentRequest.GetST2StudentRequests(this.kxStudentId, 0, 0);
                }

                return new List<KxStudentRequest>();
            }
        }

        public List<KxStudentRequest> ActiveStudentRequests
        {
            get
            {
            	return StudentRequests.FindAll(s => s.RequestStatus.IsClosed == 0);
            }
        }

        /// <summary>
        /// Retrieves Applied Discount Codes on a Pending or accepted Allocation
        /// </summary>
        public List<KxDiscountCode> AppliedDiscountCodes
        {
            get
            {
                KxWebStudentAllocation currentAllocation = PendingAllocation ?? AcceptedAllocation; // If pendingAllocation is null then accepted allocation
                if (this.kxStudentId <= 0 || currentAllocation == null)
                    return new List<KxDiscountCode>();

                return KxDiscountCode.GetAppliedDiscountCodes(this.UserGuid, CurrentApplication.ApplicationGuid);
            }
        }


        public int CountOfActiveStudentRequests
        {
            get
            {
                return ActiveStudentRequests.Count;
            }
        }

        public int CountOfStudentRequests
        {
            get
            {
                return StudentRequests.Count;
            }
        }

        public bool HasActiveStudentRequests
        {
            get
            {
                return CountOfActiveStudentRequests > 0;
            }
        }

        public bool HasStudentRequests
        {
            get
            {
                return CountOfStudentRequests > 0;
            }
        }

        public bool CurrentApplicationHasStudentRequests
        {
            get
            {
                return CurrentApplication.HasStudentRequests;
            }
        }

        public int CurrentApplicationCountOfStudentRequests
        {
            get
            {
                return CurrentApplication.CountOfStudentRequests;
            }
        }
    
        public KxWebStudentApplication CurrentApplication
        {
            get
            {
                KxWebStudentApplication result = null;
                var allApplications = Applications.GetWebApplicationsForYearAndSession(CurrentAcademicYear,
                    CurrentAcademicYearSession, KxDbConfiguration.GetConfiguration("AllowMultipleAppsForYear") != "" ? CurrentAcademicYearSession > 0 : true);
                if (ControlApplicationGuid != null && ControlApplicationGuid != Guid.Empty)
                {
                    result = allApplications.FirstOrDefault(s => s.ApplicationGuid == ControlApplicationGuid.Value);
                }
                else if (UserApplicationGuid != null && UserApplicationGuid != Guid.Empty)
                {
                    result = allApplications.FirstOrDefault(s => s.ApplicationGuid == UserApplicationGuid.Value);
                }

                // pha...
                result =  result ?? GetWebApplicationForYearAndSession(CurrentAcademicYear, CurrentAcademicYearSession);    // pha
                if (System.Configuration.ConfigurationManager.AppSettings["DebugApplicationGuid"] == "True" && _ApplicationGuid != result.ApplicationGuid)
                {
                    _ApplicationGuid = result.ApplicationGuid;
                    using (System.IO.StreamWriter sw = new System.IO.StreamWriter(@"c:\temp\workflow.xml", true))
                    {
                        sw.WriteLine("<ApplicationGuid>");
                        sw.WriteLine("\t" + _ApplicationGuid);
                        sw.WriteLine("</ApplicationGuid>");
                    }
                }
                return result;  
            }
        }

        public bool GroupInvitesPending
        {
            get
            { return true; }
        }

        public bool IsApplicationStatus
        {
            get
            {
                return ((Allocations.GetAllocationStatus(CurrentApplication) == KxAllocationStatus.None));
            }
        }

        public bool IsInHouse
        {
            get
            {
                return Allocations.GetInHouseAllocation(CurrentApplication) != null;
            }
        }

        public bool HasNoAllocation
        {
            get
            { 
                return ((Allocations.GetAllocationStatus(CurrentApplication) == KxAllocationStatus.None )); 
            }
        }

        public bool HasNoActiveAllocation
        {
            get
            {
                return (!HasAcceptedOrPendingOrRejectedAllocation && !HasExpiredAllocation);
            }
        }

        public bool HasNoneOrExpiredAllocation
        {
            get
            {
                return (HasNoAllocation || Allocations.GetAllocationStatus(CurrentApplication) == KxAllocationStatus.Expired); 
            }
        }

        public bool HasNoneOrWithdrawnExpiredAllocation
        {
            get
            {
                return (HasNoAllocation || Allocations.GetAllocationStatus(CurrentApplication) == KxAllocationStatus.Expired || Allocations.GetAllocationStatus(CurrentApplication) == KxAllocationStatus.Withdrawn);
            }
        }

        public bool HasWithdrawnOrExpiredAllocation
        {
            get
            {
                return (Allocations.GetAllocationStatus(CurrentApplication) == KxAllocationStatus.Expired || Allocations.GetAllocationStatus(CurrentApplication) == KxAllocationStatus.Withdrawn);
            }
        }

        public KxWebStudentAllocation LastAllocation
        {
            get
            { return Allocations.GetLastAllocation(CurrentApplication); }
        }

        public string LastAllocationStatus
        {
            get
            {
                return GetLastAllocationStatus();
            }
        }

        public bool HasAcceptedAllocation
        {
            get
            {
                return Allocations.GetAllocationStatus(CurrentApplication) == KxAllocationStatus.Accepted;
            }
        }
        public bool HasPendingAllocation
        {
            get
            { 
                return Allocations.GetAllocationStatus(CurrentApplication) == KxAllocationStatus.Pending; 
            }
        }
        public bool HasRejectedAllocation
        {
            get
            {
                return Allocations.GetAllocationStatus(CurrentApplication) == KxAllocationStatus.Rejected;
            }
        }
        public bool HasExpiredAllocation
        {
            get
            {
                return Allocations.GetAllocationStatus(CurrentApplication) == KxAllocationStatus.Expired;
            }
        }
        public bool HasWithdrawnAllocation
        {
            get
            {
                return Allocations.GetAllocationStatus(CurrentApplication) == KxAllocationStatus.Withdrawn;
            }
        }

        public bool HasAcceptedOrRejectedAllocation
        {
            get
            {
                return (Allocations.GetAllocationStatus(CurrentApplication) == KxAllocationStatus.Accepted || Allocations.GetAllocationStatus(CurrentApplication) == KxAllocationStatus.Rejected);
            }
        }

        public bool HasAcceptedOrPendingOrRejectedAllocation
        {
            get
            {
                return (Allocations.GetAllocationStatus(CurrentApplication) == KxAllocationStatus.Accepted || Allocations.GetAllocationStatus(CurrentApplication) == KxAllocationStatus.Pending || Allocations.GetAllocationStatus(CurrentApplication) == KxAllocationStatus.Rejected);
            }
        }	

        public int SpecifiedAllocationId
        {
            get
            {
                return specifiedAllocationId;
            }
            set
            {
                if (value != null && value != specifiedAllocationId)
                {
                    specifiedAllocationId = value;
                    specifiedAllocation = null;
                }
            }
        }

        public Guid SpecificApplicatinGuid { get; set; }
       
        public KxWebStudentAllocation SpecifiedAllocation
        {
            get
            {
                if (specifiedAllocation == null)
                {
                    specifiedAllocation = Allocations.GetAllocationById(specifiedAllocationId);
                }
                return specifiedAllocation;
            }
        }

        public int SpecifiedAcceptedAllocationId
        {
            get
            {
                return specifiedAcceptedAllocationId;
            }
            set
            {
                if (value != null && value != specifiedAcceptedAllocationId)
                {
                    specifiedAcceptedAllocationId = value;
                    specifiedAcceptedAllocation = null;
                }
            }
        }

        public KxWebStudentAllocation SpecifiedAcceptedAllocation
        {
            get
            {
                if (specifiedAcceptedAllocation == null && specifiedAcceptedAllocationId > 0)
                {
                    specifiedAcceptedAllocation = Allocations.GetAllocationById(specifiedAcceptedAllocationId);
                }
                return specifiedAcceptedAllocation;
            }
        }

        public int SpecifiedPendingAllocationId
        {
            get
            {
                return specifiedPendingAllocationId;
            }
            set
            {
                if (value != null && value != specifiedPendingAllocationId)
                {
                    specifiedPendingAllocationId = value;
                    specifiedPendingAllocation = null;
                }
            }
        }

        public KxWebStudentAllocation SpecifiedPendingAllocation
        {
            get
            {
                if (specifiedPendingAllocation == null && specifiedPendingAllocationId > 0)
                {
                    specifiedPendingAllocation = Allocations.GetAllocationById(specifiedPendingAllocationId);
                }
                return specifiedPendingAllocation;
            }
        }

        public int SpecifiedRejectedAllocationId
        {
            get
            {
                return specifiedRejectedAllocationId;
            }
            set
            {
                if (value != null && value != specifiedRejectedAllocationId)
                {
                    specifiedRejectedAllocationId = value;
                    specifiedRejectedAllocation = null;
                }
            }
        }

        public KxWebStudentAllocation SpecifiedRejectedAllocation
        {
            get
            {
                if (specifiedRejectedAllocation == null && specifiedRejectedAllocationId > 0)
                {
                    specifiedRejectedAllocation = Allocations.GetAllocationById(specifiedRejectedAllocationId);
                }
                return specifiedRejectedAllocation;
            }
        }


        public KxWebStudentAllocation AcceptedAllocation
        {
            get
            {
                return Allocations.GetAcceptedAllocation(CurrentApplication);
            }
        }

        public KxWebStudentAllocation InHouseAllocation
        {
            get
            {
                return Allocations.GetInHouseAllocation(CurrentApplication);
            }
        }
    
        public KxWebStudentAllocation PendingAllocation
        {
            get
            {
                return Allocations.GetPendingAllocation(CurrentApplication);

            }
        }
        public KxWebStudentAllocation RejectedAllocation
        {
            get
            {
                return Allocations.GetRejectedAllocation(CurrentApplication);
            }
        }
        public KxWebStudentAllocation WithdrawnAllocation
        {
            get
            {
                return Allocations.GetWithdrawnAllocation(CurrentApplication);
            }
        }
        public KxWebStudentAllocation ExpiredAllocation
        {
            get
            {
                return Allocations.GetExpiredAllocation(CurrentApplication);
            }
        }

        public KxWebStudentAllocations Allocations
        {
            get
            {
                if (allocations == null)
                {
                    allocations = new KxWebStudentAllocations(this, UserGuid);
                    UserSynchronisation.SynchroniseUser();
                }
                return allocations;
            }
        }

        public int CustomStatus1
        {
            get
            {   return GetCustomStatus(1);}
        }

        public int CustomStatus2
        {
            get
            { return GetCustomStatus(2); }
        }

        public int CustomStatus3
        {
            get
            { return GetCustomStatus(3); }
        }

        public int CustomStatus4
        {
            get
            { return GetCustomStatus(4); }
        }

        public int CustomStatus5
        {
            get
            { return GetCustomStatus(5); }
        }

        public KxWebUserSynchronisation UserSynchronisation
        {
            get
            {
                if (userSynchronisation == null)
                {
                    userSynchronisation = new KxWebUserSynchronisation(this);
                }
                return userSynchronisation;
            }
        }

        public bool IsUserRegistered
        {
            get
            {
                return GetIsUserRegistered(userName,UserGuid);
            }
        }

        public bool CanDebtorApply
        {
            get
            {
                return GetCanDebtorApply();
            }
        }

        public Decimal DepositBalance
        {
            get
            {
                try
                {
                    return Student.DepositBalances.DepositBalance;
                }
                catch
                {
                    return 0.00M;
                }
            }
        }

        public Decimal DepositBalanceShortfall200
        {
            get
            {
                try
                {
                    return Student.DepositBalances.DepositBalanceShortfall200;
                }
                catch
                {
                    return 200.00M;
                }
            }
        }

        public bool HadPriorAllocation
        {
            get
            {
                return GetHasPriorAllocation(true);
            }
        }

        public bool HadAllocationLastYear
        {
            get
            {
                return GetHasPriorAllocation(false);
            }
        }

		public string SiteBanner
		{
			get
			{
				KxData data = new KxData("banner", "Select_SiteBanner", TargetType.StoredProc);

				data.AddParameter("SiteId", CurrentApplication.FirstPreference.SiteId, KxValueType.Int);
				data.AddParameter("BlockId", CurrentApplication.FirstPreference.BlockId, KxValueType.Int);
                data.Action = ActionType.ExecuteProcedure;

				data.Execute();

				XmlDocument doc = new XmlDocument();
				doc.LoadXml(data.XmlResults);

				XmlNode node = doc.SelectSingleNode("//banner/datarow");

				return KxXmlUtils.GetNodeValue(node, "bannercontent", String.Empty);
			}
		}

        private KxWebStudentSocialProfile _socialProfile;
        private Guid _userApplicationGuid;

        public KxWebStudentSocialProfile SocialProfile
        {
            get
            {
                if (_socialProfile == null && this.KxStudentId > 0)
                {
                    _socialProfile = KxWebStudentSocialProfile.GetWebStudentSocialProfile(this.KxStudentId, this);
                }

                return _socialProfile;
            }
        }

        public Dictionary<string, RawPaymentDataItem> LastPaymentRawData
        {
            get { return _lastPaymentRawData; }
            set { _lastPaymentRawData = value; }
        }

        #endregion 

        public KxWebUserStudent() : base(null)
        {}

        public KxWebUserStudent(KxBaseClass parent) : base(parent)
        {}

        public KxWebUserStudent(KxBaseClass parent, Guid userGuid)
            : base(parent, userGuid)
        {
           
        }

        public bool AcceptAllocation(string UserDetails, string userIPAddress)
        {
            bool result = false;

			KxLogger.LogActivity(this, "KxWebUserStudent.AcceptAllocation", UserDetails, ""); //TE20100212
			if (PendingAllocation != null)
            {
				KxLogger.LogActivity(this, "KxWebUserStudent.AcceptAllocation", "Pending Allocation", ""); //TE20100212
				result = PendingAllocation.AcceptAllocation(UserDetails, userIPAddress);
				if (result)
				{
					KxLogger.LogActivity(this, "KxWebUserStudent.AcceptAllocation", "Pending Allocation - True", ""); //TE20100212
				}
				else
				{
					KxLogger.LogActivity(this, "KxWebUserStudent.AcceptAllocation", "Pending Allocation - False", ""); //TE20100212
				}
            }
            return result;
        }

        public void ClearSelectedApplication()
        {
            ControlApplicationGuid = null;
            UserApplicationGuid = null;
            UserAcademicYear = 0;
            UserAcademicYearSession = 0;
        }

        public bool RejectAllocation(string UserDetails, string userIPAddress)
        {
            bool result = false;

            if (PendingAllocation != null)
            {
                result = PendingAllocation.RejectAllocation(UserDetails, userIPAddress);
            }
            return result;
        }

        public bool AcceptLastAllocationOffer(string UserDetails, string userIPAddress)
        {
            bool result = false;

			KxLogger.LogActivity(this, "KxWebUserStudent.AcceptLastAllocationOffer", UserDetails, ""); //TE20100212
			if (LastAllocation != null)
            {
				KxLogger.LogActivity(this, "KxWebUserStudent.AcceptLastAllocationOffer", "LastAllocation", ""); //TE20100212
				result = LastAllocation.AcceptAllocation(UserDetails, userIPAddress);
            }
            return result;
        }

        public bool RejectLastAllocationOffer(string UserDetails, string userIPAddress)
        {
            bool result = false;

            if (LastAllocation != null)
            {
                result = LastAllocation.RejectAllocation(UserDetails, userIPAddress);
            }
            return result;
        }

        public bool TakePayment(DateTime transactionDate, string providerReference, decimal amount, string gatewayname,KxPaymentType payType)
        {
            try
            {
                #region Audit

                string auditDetails = "guidIdentifier: " + guidIdentifier.ToString();
                auditDetails += ", transactionDate: " + transactionDate.ToString();
                auditDetails += ", payType: " + payType.ToString();
                auditDetails += ", amount: " + amount.ToString();
                if (providerReference != null) auditDetails += ", providerReference: " + providerReference;
                if (gatewayname != null) auditDetails += ", gatewayname: " + gatewayname;
                if (CurrentApplication != null) auditDetails += ", ApplicationGuid: " + CurrentApplication.ApplicationGuid.ToString();

                KxData auditData = new KxData();
                auditData.ExecuteAuditLog("Kx.Objects.Student.Web.KxWebUserStudent", "TakePayment()", auditDetails);

                #endregion

                if (guidIdentifier == Guid.Empty)
                {
                    Changed = true;
                    Save();
                }

                bool duplicatexists = providerReference != "" && CurrentApplication.Payments.ReferenceExists(providerReference);

                if (providerReference == "" || (!duplicatexists))
                {
                    try
                    {
                        if (PendingAllocation != null && payType == KxPaymentType.Deposit)
                        {
                            try
                            {
                                PendingAllocation.DepositPaid = true;
                                PendingAllocation.DepositAmount = amount;
                                PendingAllocation.Save();
                            }
                            catch { }
                        }
                        else if (AcceptedAllocation != null && payType == KxPaymentType.Deposit)
                        {
                            try
                            {
                                AcceptedAllocation.DepositPaid = true;
                                AcceptedAllocation.DepositAmount = amount;
                                AcceptedAllocation.Save();
                            }
                            catch { }
                        }

                        KxWebStudentPayment payment = new KxWebStudentPayment(CurrentApplication.Payments);
                        payment.ApplicationGuid = CurrentApplication.ApplicationGuid;
                        payment.PaymentDate = transactionDate;
                        payment.TransactionReference = providerReference;
                        payment.TransactionAmount = amount;
                        payment.GatewayName = gatewayname;
                        payment.PaymentType = payType;
                        payment.Changed = true;
                        payment.Save();

                        CurrentApplication.Payments.Items.Add(payment);

                        try
                        {
                            WriteStudentHistoryRecord("WebPayment" + payment.PaymentType, "Transaction Reference: " + payment.TransactionReference + ", Amount: " + payment.TransactionAmount.ToString("c"));
                        }
                        catch { }
                    }
                    catch (Exception ex)
                    {
                        KxLogger.LogActivity(this, "TakePayment", ex.Message, providerReference);
                        return false;
                    }
                }
                else
                {
                    KxLogger.LogActivity(this, "TakePayment", "Duplicate Payment-Ignored", providerReference);
                }

                return true;
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "TakePayment", ex.Message, providerReference);
                return false;
            }
        }


        public void TransferStudentDetailsToApplication(KxWebStudentApplication app)
        {
            if (!app.Updated && Student != null)
            {
                if (String.IsNullOrEmpty(app.Title))
                {
                    app.Title = Student.Title;
                }

                if (String.IsNullOrEmpty(app.ForeName))
                {
                    app.ForeName = Student.ForeName;
                }

                if (String.IsNullOrEmpty(app.SurName))
                {
                    app.SurName = Student.SurName;
                }

                if (app.Dob == null)
                {
                    app.Dob = Student.Dob;
                }

                if (app.NationalityId == 0)
                {
                    app.NationalityId = Student.NationalityId;
                }

                if (String.IsNullOrEmpty(app.Gender))
                {
                    app.Gender = Student.Gender;
                }

                if (app.KxStudentId == 0)
                {
                    app.KxStudentId = Student.KxStudentId;
                }
                app.Updated = true;
            }
        }


        public KxWebStudentApplication GetWebApplicationForYearAndSession(int AcademicYear, int sessionId)
        {
            KxWebStudentApplication result = Applications.GetWebApplicationForYearAndSession(AcademicYear, sessionId);

            if (result != null && result.Updated == false)
            {
                if (String.IsNullOrEmpty(result.ForeName))
                {
                    result.ForeName = ForeName;
                }

                if (String.IsNullOrEmpty(result.EmailAddress))
                {
                    result.EmailAddress = EmailAddress;
                }

                if (String.IsNullOrEmpty(result.SurName))
                {
                    result.SurName = SurName;
                }

                if (Student != null)
                {
                    TransferStudentDetailsToApplication(result);
                }
            }

            if (result.ApplicationGuid != Guid.Empty && KxStudentId == 0)
            {
                KxStudentId = result.GetCreatedKineticId();
            }
            return result;
        }

        public static bool ValidateUniqueCode(string Code, bool ignoreActiveFlag)
        {
            return ValidateUniqueCodeVerbose(Code, ignoreActiveFlag) == "";
        }
        
        public static string ValidateUniqueCodeVerbose(string Code,bool ignoreActiveFlag)
        {
            string result = "";
            try
            {
                KxWebUserShortUrl shortUrl = new KxWebUserShortUrl(null, Code);

                if (shortUrl.UserGuid == Guid.Empty)
                {
                    result = "Invalid Confirmation Code";
                }
                else if (!shortUrl.IsCodeValid)
                {
                    result = "Sorry, this code has expired, please reregister.";
                }
                else
                {
                    KxWebUserStudent user = new KxWebUserStudent(null, shortUrl.UserGuid);

                    if (user.KxStudentId > 0)
                    {
                        KxWebUserStudent user2 = new KxWebUserStudent();

                        user2.LoadStudent(user.KxStudentId);

                        if (!ignoreActiveFlag && user2.UserGuid != Guid.Empty)
                        {
                            result = "Account for this student already exists, login using your credentials";
                        }
                    }

                    if (result == "")
                    {

                        if (GetIsUserRegistered(user.UserName,user.UserGuid))
                        {
                            result = "Username is already in use, please re-register with a different user name";
                        }
                    }

                    if (result == "")
                    {
                        if ((ignoreActiveFlag && user.EmailConfirmed == null) || user.active == 0)
                        {
                            user.Active = 1;
                            user.EmailConfirmed = DateTime.Now;
                            user.Changed = true;
                            user.Save();
                        }
                    }
                }
            }
            catch
            {
                result = "Invalid Confirmation Code";
            }
            return result;
        }

        public static string ValidatePasswordResetCodeVerbose(string Code)
        {
            string result = "";
            try
            {
                KxWebUserShortUrl shortUrl = new KxWebUserShortUrl(null, Code);

                if (shortUrl.UserGuid == Guid.Empty)
                {
                    result = "Invalid URL";
                }
                else if (!shortUrl.IsCodeValid)
                {
                    result = "Sorry, the URL is no longer valid.";
                }
                else if (shortUrl.CodeUsed)
                {
                    result = "Sorry, the URL has already been used.";
                }
            }
            catch
            {
                result = "Invalid URL";
            }
            return result;
        }

        public static bool GetIsUserRegistered(string username, Guid userGuid)
        {
            KxWebUserStudent user2 = new KxWebUserStudent();

            user2.LoadUserName(username);

            return ((user2.UserGuid != Guid.Empty) && (user2.UserGuid != userGuid));
        }


        private KxStudentAllocationDate GetCurrentAllocationDate()
        {
            KxStudentAllocationDate result = null;

            if (PendingAllocation != null)
            {
                result = PendingAllocation.StudentAllocation.GetAllocationDateById(CurrentAllocationDateId);
            }

            if (AcceptedAllocation != null && result == null)
            {
                result = AcceptedAllocation.StudentAllocation.GetAllocationDateById(CurrentAllocationDateId);
            }

            if (RejectedAllocation != null && result == null)
            {
                result = RejectedAllocation.StudentAllocation.GetAllocationDateById(CurrentAllocationDateId);
            }

            return result;
        }

        private string GetLastAllocationStatus()
        {
            string result = "NONE";

            KxWebStudentAllocation alloc = LastAllocation;

            if (alloc != null && alloc.StudentAllocation.OfferDate > KxDateTime.KxApplicableDate)
            {
                if (alloc.StudentAllocation.IsCurrent)
                {
                    if ((alloc.StudentAllocation.AcceptedDate == KxDateTime.KxNullDate && alloc.StudentAllocation.AcceptedUserId == 0 &&
                               alloc.StudentAllocation.RejectedDate == KxDateTime.KxNullDate && alloc.StudentAllocation.RejectedUserId == 0 &&
                               !alloc.StudentAllocation.OfferHasExpired) && (alloc.AcceptedDate == null && alloc.RejectedDate == null))
                    {
                        result = "PENDING";
                    }
                    else if (alloc.StudentAllocation.AcceptedDate != KxDateTime.KxNullDate || alloc.AcceptedDate != null)
                    {
                        result = "ACCEPTED";
                    }
                    else if (alloc.StudentAllocation.RejectedDate != KxDateTime.KxNullDate || alloc.RejectedDate != null)
                    {
                        result = "REJECTED";
                    }
                    else if (alloc.StudentAllocation.OfferHasExpired)
                    {
                        result = "EXPIRED";
                    }
                }
                else
                {
                    if (alloc.StudentAllocation.RejectedDate != KxDateTime.KxNullDate || alloc.RejectedDate != null)
                    {
                        result = "REJECTED";
                    }
                    else if (CurrentApplication.StandardApplication.InactiveDate != KxDateTime.KxNullDate)
                    {
                        result = "WITHDRAWN";
                    }
                }
            }

            return result;
        }

        public override void BeforeLoad()
        {
            Children.Remove(student);
            Children.Remove(allocations);
            Children.Remove(applications);

            student = null;
            allocations = null;
            applications = null;
        }

        public void ClearSync()
        {
            Children.Remove(userSynchronisation);
            userSynchronisation = null;
        }

        public override void KxStudentIdChanged()
        {
            Applications.KxStudentId = kxStudentId;
            if (kxStudentId > 0)
            {
                Student.Load(kxStudentId);

                foreach (KxWebStudentApplication app in Applications.Items)
                {
                    TransferStudentDetailsToApplication(app);
                }
            }
        }

        public void ForceSave()
        {
            Changed = true;
            Save();
        }

        public int GetCustomStatus(int Apptype)
        {
            int result = 0;

            KxData data = new KxData("", "WebStudentCustomStatus", TargetType.StoredProc);

            data.AddParameter("KxStudentId", KxStudentId, KxValueType.Int);
            data.AddParameter("ApplicationTypeId", Apptype, KxValueType.Int);
            data.AddParameter("AcademicYearId", CurrentAcademicYear, KxValueType.Int);

            object value = data.ExecuteScalar();

            if (value != null)
            {
                try
                {
                    result = Convert.ToInt32(value);
                }
                catch
                { }
            }

            return result;
        }

        public override void UserLoggedIn()
        {
            try
            {
                WriteStudentHistoryRecord("WebLogin", "User" + userName + " successfully logged in at " + DateTime.Now);
            }
            catch { }
        }

        public override void AccountActivated()
        {
            try
            {
                WriteStudentHistoryRecord("RegisterWebAccount", "User " + userName + " confirmed their registration details.");
            }
            catch { }
        }

        public void WriteStudentHistoryRecord(string HistoryType, string HistoryText)
        {
            int allocationid = 0;
            try
            {
                if (HasPendingAllocation)
                {
                    allocationid = PendingAllocation.AllocationId;
                }
                else if (HasAcceptedAllocation)
                {
                    allocationid = AcceptedAllocation.AllocationId;
                }
                else if (HasRejectedAllocation)
                {
                    allocationid = RejectedAllocation.AllocationId;
                }
            }
            catch { }

            if (allocationid != 0 || kxStudentId != 0 || (CurrentApplication != null && CurrentApplication.ApplicationId != 0))
            {
                KxStudentHistory history = new KxStudentHistory(null);
                try
                {
                    history.AllocationId = allocationid;

                    if (CurrentApplication != null)
                    {
                        history.ApplicationId = CurrentApplication.ApplicationId;
                    }
                    if (Student != null)
                    {
                        history.KxStudentId = Student.KxStudentId;
                    }
                    else
                    {
                        history.KxStudentId = kxStudentId;
                    }
                    history.HistoryNotes = HistoryText;
                    history.HistoryType = HistoryType;
                    history.CreationDate = DateTime.Now;
                    history.CreationUserId = 1;
                    history.Save();
                }
                catch { }
            }
        }

        protected bool GetCanDebtorApply()
        {
            bool result = true;

            if (Student != null)
            {
                result = Student.CanDebtorApply;
            }

            return result;
        }

        public void SetPreferenceFromSiteLookup()
        {
            if (CurrentSiteLookup != null && CurrentApplication != null)
            {
                CurrentApplication.FirstPreference.SiteId = CurrentSiteLookup.SiteId;
                CurrentApplication.FirstPreference.AreaId = CurrentSiteLookup.AreaId;
                CurrentApplication.FirstPreference.BlockId = CurrentSiteLookup.BlockId;
            }
        }

        public void SetSiteLookupFromPreference()
        {
            KxSiteAvailabilityLookup site = KxStudentObjectManager.SiteAvailabilityLookups.GetSiteAvailabilityLookup(CurrentApplication.FirstPreference.SiteId, CurrentApplication.FirstPreference.AreaId, CurrentApplication.FirstPreference.BlockId);
            
            if (site != null)
            {
                CurrentSiteLookupId = site.UniqueId;
            }
        }

        public void SetPreferenceFromRoomType()
        {
            if (!String.IsNullOrEmpty(CurrentLibertyRoomId) && CurrentApplication != null)
            {
                KxWebStudentRoomLibertyRate lrr = CurrentApplication.LibertyRoomRates.GetRoomByUniqueId(CurrentLibertyRoomId);

                if (lrr != null)
                {
                    CurrentApplication.FirstPreference.BedroomTypeId = lrr.BedroomTypeId;
                }
            }

            if (!String.IsNullOrEmpty(CurrentCityRoomId) && CurrentApplication != null)
            {
                KxWebStudentRoomCityRate lrr = CurrentApplication.CityRoomRates.GetRoomByUniqueId(CurrentCityRoomId);

                if (lrr != null)
                {
                    CurrentApplication.FirstPreference.BedroomTypeId = lrr.BedroomTypeId;
                }
            }
        }

        protected bool GetControlSiteEqualsUserSite()
        {
            bool result = false;

            if (UserSiteLookup != null && ControlSiteLookup != null)
            {
                result = UserSiteLookup.CityName == ControlSiteLookup.CityName;
            }

            return result;
        }

        protected bool GetHasPriorAllocation(bool anyYear)
        {
            Boolean result = false;

            foreach (KxWebStudentAllocation alloc in Allocations.Items)
            {
                if (anyYear)
                {
                    result = true;
                    break;
                }
                else
                {
                    if (alloc.StudentAllocation.Application.AcademicYearId == CurrentAcademicYear - 1)
                    {
                        result = true;
                        break;
                    }
                }
            }


            return result;
        }

        public bool ConfirmEmail()
        {
            Active = 1;
            EmailConfirmed = DateTime.Now;
            Changed = true;
            return Save();
        }

        public void SetCityRoomFromId()
        {
            KxWebStudentAllocation allocationid = null;
            try
            {
                if (HasPendingAllocation)
                {
                    allocationid = PendingAllocation;
                }
                else if (HasAcceptedAllocation)
                {
                    allocationid = AcceptedAllocation;
                }
                else if (HasRejectedAllocation)
                {
                    allocationid = RejectedAllocation;
                }
            }
            catch { }

            if ((allocationid != null) && (allocationid.StudentAllocation != null))
            {
                KxStudentAllocation sa = new KxStudentAllocation(null, allocationid.AllocationId);
                KxStudentApplication sap = new KxStudentApplication(null, sa.ApplicationId);
                CurrentApplication.PaymentScheduleTypeId = sap.PaymentScheduleTypeId;

                KxResidentialRoom room = new KxResidentialRoom(null, allocationid.StudentAllocation.ResidentialRoomId);
                KxSiteBlock block = new KxSiteBlock(null, room.BlockId);

                KxLogger.LogActivity(this, "KxWebUserStudent.SetCityRoomFromId", sap.AcademicYearId.ToString() + ';' + block.SiteId.ToString() + ';' + sa.AllocationId.ToString() + ';' + sa.ApplicationId.ToString(), "");
                KxWebStudentRoomCityRates cityr = new KxWebStudentRoomCityRates(null, sap.AcademicYearId, block.SiteId, block.AreaId, block.BlockId);
                string unique = String.Format("{0}A{1}A{2}", sa.AllocationModelId, sa.PricingModelId, room.BedroomTypeId);
                currentCityRoom = cityr.GetRoomByUniqueId(unique);
                if (currentCityRoom != null)
                {
                    UserCityRoomId = unique;
                    ControlCityRoomId = unique;
                }
                else
                {
                    KxLogger.LogActivity(this, "KxWebUserStudent.SetCityRoomFromId", "Cannot find room", "");
                }


            }
            else
            {
                KxLogger.LogActivity(this, "KxWebUserStudent.SetCityRoomFromId", "No Allocation Found", "");
            }
               
        }

        public object ExecuteStoredProcedureScalar(string storedProcedureName, object Parm1, object Parm2, object Parm3, object Parm4)
        {
            if (string.IsNullOrEmpty(storedProcedureName))
            {
                return null;
            }

            KxData data = new KxData("", storedProcedureName, TargetType.StoredProc);
            data.AddParameter("Parm1", Parm1, KxValueType.Unknown);
            data.AddParameter("Parm2", Parm2, KxValueType.Unknown);
            data.AddParameter("Parm3", Parm3, KxValueType.Unknown);
            data.AddParameter("Parm4", Parm4, KxValueType.Unknown);

            return data.ExecuteScalar();
        }

        public void SetSessionValue(string key, object obj)
        {
            HttpContext.Current.Session[key] = obj;
        }

        public RawPaymentDataItem GetLastPaymentRaw(string key)
        {
            RawPaymentDataItem result = new RawPaymentDataItem();
            if (_lastPaymentRawData != null && _lastPaymentRawData.ContainsKey(key))
            {
                result = _lastPaymentRawData[key];
            }

            return result;
        }

        public string GetLastPaymentRawValue(string key)
        {
            string result = string.Empty;
            if (_lastPaymentRawData != null && _lastPaymentRawData.ContainsKey(key))
            {
                result = _lastPaymentRawData[key].Value;
            }

            return result;
        }

        public void ClearLastPaymentRawData()
        {
            _lastPaymentRawData = null;
        }

        public override void ClearLoggedInUser()
        {
            base.ClearLoggedInUser();

            ClearLastPaymentRawData();
            this.student = null;
            this.applications = null;
            this.userSynchronisation = null;
            this.specifiedAcceptedAllocation = null;
            this.specifiedAcceptedAllocationId = 0;
            this.specifiedAllocation = null;
            this.specifiedAllocationId = 0;
            this.specifiedPendingAllocation = null;
            this.specifiedPendingAllocationId = 0;
            this.specifiedRejectedAllocation = null;
            this.specifiedRejectedAllocationId = 0;
            this.SpecificApplicatinGuid = Guid.Empty;
            this._socialProfile = null;
            changed = false;    // pha stop ghost record
        }

        public void ResetFlatmates()
        {
            if (AcceptedAllocation != null)
            {
                AcceptedAllocation.StudentAllocation.ResetFlatmates();
            }
            if (PendingAllocation != null)
            {
                PendingAllocation.StudentAllocation.ResetFlatmates();
            }
            if (InHouseAllocation != null)
            {
                InHouseAllocation.StudentAllocation.ResetFlatmates();
            }
        }

        public bool IsValid()
        {
            return UserGuid != Guid.Empty;
        }
    }
}

