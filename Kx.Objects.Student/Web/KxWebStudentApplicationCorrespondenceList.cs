using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Exceptions;
using Kx.Core.Utility;

namespace Kx.Objects.Student.Web
{
    public class KxWebStudentApplicationCorrespondenceList : KxBaseClass
    {
        private List<KxWebStudentApplicationCorrespondence> items = new List<KxWebStudentApplicationCorrespondence>();

        public List<KxWebStudentApplicationCorrespondence> Items
        {
            get
            { return items; }
        }

        public KxWebStudentApplicationCorrespondence this[int index]
        {
            get
            { return items[index]; }
            set
            { items[index] = value; }
        }

        
        public KxWebStudentApplicationCorrespondenceList(KxBaseClass parent, Guid applicationGuid) : base(parent)
        {
            guidIdentifier = applicationGuid;
            Load();
        }

        public KxWebStudentApplicationCorrespondenceList(KxBaseClass parent)
            : base(parent)
        {
        }

        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = null;

            if (actiontype == ActionType.Select)
            {
                result = new KxData("webstudentapplicationcorrespondence", "WebStudentApplicationCorrespondence", TargetType.Table, ActionType.Select);

                result.KeyFields.Add(new KxDataValue("applicationguid", guidIdentifier, KxValueType.Guid));

                result.AddOrderField("documentindex", KxDataOrderType.Ascending);
            }
            return result;
        }

        public override bool LoadFromXml(string xml, string xpath)
        {
            items.Clear();
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xml);

                XmlNodeList nodes = doc.SelectNodes(@"//results/" + xpath + "/datarow");

                foreach (XmlNode node in nodes)
                {
                    if (node != null)
                    {
                        KxWebStudentApplicationCorrespondence item = new KxWebStudentApplicationCorrespondence(this, node);
                        items.Add(item);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "LoadFromXml", ex.Message, xpath);
                LastError = ex.Message;
                return false;
            }
        }

        public KxWebStudentApplicationCorrespondence GetCorrespondence(int id)
        {
            KxWebStudentApplicationCorrespondence result = null;

            foreach (KxWebStudentApplicationCorrespondence i in Items)
            {
                if (i.DocumentIndex == id)
                {
                    result = i;
                    break;
                }
            }

            if (result == null && id > 0)
            {
                result = new KxWebStudentApplicationCorrespondence(this);
                result.ApplicationGuId = Guid.Empty;
                result.DocumentIndex = id;
                Items.Add(result);
            }
            return result;
        }

    }
}

