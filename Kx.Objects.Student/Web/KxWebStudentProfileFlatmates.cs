﻿#region copyright

// =======================================================
// Product:   KCF.Student
// Project:   Kx.Objects.Student
// File:      KxWebStudentProfileFlatmates.cs
// Created:   21/12/2016
// By:        Steve Ashley
// Modified:  21/12/2016
// 
// Copyright © Kinetic Solutions Ltd 2016
// All rights reserved.
// 
// No portion of this file may be re-created, copied or otherwise
// used without the express permission of Kinetic Solutions Ltd.
// =======================================================

#endregion

#region usings

using System.Collections.Generic;
using System.Linq;
using System.Xml;
using Kx.Core.Data;
using Kx.Core.Exceptions;

#endregion

namespace Kx.Objects.Student.Web
{
    public class KxWebStudentProfileFlatmates
    {
        #region Fields and Properties

        public List<KxWebStudentSocialProfileFlatmate> Flatmates = new List<KxWebStudentSocialProfileFlatmate>();

        public string[] FlatmateIndexes
        {
            get
            {
                List<string> result = new List<string>();

                for (int i = 0; i < Flatmates.Count; i++)
                {
                    result.Add(i.ToString());
                }

                return result.ToArray();
            }
        }

        #endregion

        #region Constructors

        public KxWebStudentProfileFlatmates(int allocationId, int loadingKxStudentId)
        {
            LoadFlatmates(allocationId, loadingKxStudentId);
        }

        #endregion

        #region Private Methods

        private void LoadFlatmates(int allocationId, int loadingKxStudentId)
        {
            KxData data = new KxData("flatmate", string.Format(@"DECLARE @AllocationID int = {0}

                DECLARE @AcademicYearID	int

                SELECT @AcademicYearID = AP.AcademicYearID FROM ST2StudentAllocations SA JOIN ST2StudentApplications AP ON AP.ApplicationID = SA.ApplicationID WHERE AllocationID = @AllocationID

                SELECT
	                ORR.Name AS RoomName,
	                S.StudentID,
	                OAP.KxStudentID,
	                WSP.ProfileIsPublic,
	                WSP.CreationDate,
	                WSP.ScreenName,
	                WSP.BriefDescription,
	                WSP.Field1Value,
	                WSP.Field2Value,
	                WSP.Field3Value,
	                WSP.Field4Value,
	                WSP.Field5Value,
	                WSP.Profile1,
	                WSP.Profile2,
	                WSP.Profile3,
	                WSP.Profile4,
	                WSP.Profile5,
	                WSP.Profile6,
	                WSP.Profile7,
	                WSP.Profile8,
	                WSP.Profile9,
	                WSP.ProfilePicture,
	                WSP.ProfilePictureID,
	                WSP.ProfilePictureType
                FROM
	                ST2StudentAllocations SA
	                JOIN ResidentialRooms RR ON RR.ResidentialRoomID = SA.ResidentialRoomID
	                JOIN ST2StudentApplications AP ON AP.ApplicationID = SA.ApplicationID
	                JOIN ST2RoomConfig RC ON RC.ResidentialRoomID = RR.ResidentialRoomID AND RC.AcademicYearID = AP.AcademicYearID
	                JOIN ResidentialRooms ORR ON ORR.SubBlockID = RR.SubBlockID
	                JOIN ST2RoomConfig ORCF ON ORCF.ResidentialRoomID = ORR.ResidentialRoomID AND ORCF.AcademicYearID = AP.AcademicYearID AND ORCF.WebPublished = 1
	                LEFT JOIN ST2StudentAllocations OSA 
		                JOIN ST2StudentApplications OAP ON OAP.ApplicationID = OSA.ApplicationID AND OAP.AcademicYearID = @AcademicYearID
	                ON OSA.ResidentialRoomID = ORCF.ResidentialRoomID AND EXISTS (SELECT NULL FROM ST2StudentAllocationDates MSAD JOIN ST2StudentAllocationDates OSAD ON OSAD.AllocationID = OSA.AllocationID AND OSAD.ArrivalDate <= MSAD.DepartureDate AND OSAD.DepartureDate >= MSAD.DepartureDate WHERE MSAD.AllocationID = SA.AllocationID)
	                LEFT JOIN WebStudentSocialProfile WSP ON WSP.KxStudentID = OAP.KxStudentID
	                LEFT JOIN ST2Students S ON S.KxStudentID = OAP.KxStudentID
                WHERE
	                SA.AllocationID = @AllocationID
                ORDER BY ORR.Name
                ", allocationId), TargetType.Static);

            if (data.Execute())
            {
                foreach (XmlNode node in data.GetNodesFromData())
                {
                    Flatmates.Add(new KxWebStudentSocialProfileFlatmate(null, node, loadingKxStudentId));
                }
            }
            else
            {
                KxLogger.LogActivity(this, "LoadFlatmates", data.LastError, string.Format("AllocationID: {0}", allocationId));
            }
        }

        #endregion
    }
}