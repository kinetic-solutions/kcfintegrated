﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Kx.Objects.Student.Web
{
    public class RawPaymentDataItem
    {
        public string Field { get; set; }
        public string Value { get; set; }
    }
}
