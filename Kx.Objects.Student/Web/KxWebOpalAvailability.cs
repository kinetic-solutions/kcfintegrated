using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;

namespace Kx.Objects.Student.Web
{
    [Serializable]
    public class KxWebOpalAvailability : KxBaseClass
    {
        #region Attributes & Properties

        protected int      academicYearId;
        protected string   cityName;
        protected string   letLength;
        protected int      annualAllocationModelId;
        protected int      annualPricingModelId;
        protected int      instalmentAllocationModelId;
        protected int      instalmentPricingModelId;
        protected int      bedroomTypeId;
        protected int      areaId;
        protected string   areaName;
        protected int      siteId;
        protected string   bedroomType;
        protected decimal  annualDailyCharge;
        protected decimal  annualWeeklyCharge;
        protected decimal  instalmentDailyCharge;
        protected decimal  instalmentWeeklyCharge;
        protected decimal  totalRent;
        protected decimal totalAmount;
        protected string _propertyAddress;
        protected string _additionalInformation1;

        protected Decimal[] paymentAmounts = new decimal[10];
        protected DateTime[] paymentDates = new DateTime[10];
        
        protected KxAllocationModel annualAllocationModel = null;
        protected KxAllocationModel instalmentAllocationModel = null;

        protected KxPricingModel annualPricingModel = null;
        protected KxPricingModel instalmentPricingModel = null;
        protected KxSiteAvailabilityLookup associatedSiteLookup = null;


        public KxWebOpalAvailabilityList AvailabilityList
        {
            get
            {
                return (KxWebOpalAvailabilityList)Parent;
            }
        }

        public int      AcademicYearId
        {
            get
            { return academicYearId;}
            set
            { 
                AddToAudit("AcademicYearId", academicYearId, value);
                academicYearId = value;
            }
        }

        public string   CityName
        {
            get
            { return cityName;}
            set
            { 
                AddToAudit("CityName", cityName, value);
                cityName = value;
            }
        }

        public string   LetLength
        {
            get
            { return letLength;}
            set
            { 
                AddToAudit("LetLength", letLength, value);
                letLength = value;
            }
        }

        public int      AnnualAllocationModelId
        {
            get
            { return annualAllocationModelId;}
            set
            { 
                AddToAudit("AnnualAllocationModelId", annualAllocationModelId, value);
                annualAllocationModelId = value;
            }
        }

        public int      AnnualPricingModelId
        {
            get
            { return annualPricingModelId;}
            set
            { 
                AddToAudit("AnnualPricingModelId", annualPricingModelId, value);
                annualPricingModelId = value;
            }
        }

        public int      InstalmentAllocationModelId
        {
            get
            { return instalmentAllocationModelId;}
            set
            { 
                AddToAudit("InstalmentAllocationModelId", instalmentAllocationModelId, value);
                instalmentAllocationModelId = value;
            }
        }

        public int      InstalmentPricingModelId
        {
            get
            { return instalmentPricingModelId;}
            set
            { 
                AddToAudit("InstalmentPricingModelId", instalmentPricingModelId, value);
                instalmentPricingModelId = value;
            }
        }

        public int      BedroomTypeId
        {
            get
            { return bedroomTypeId;}
            set
            { 
                AddToAudit("BedroomTypeId", bedroomTypeId, value);
                bedroomTypeId = value;
            }
        }

        public int      AreaId
        {
            get
            { return areaId;}
            set
            { 
                AddToAudit("AreaId", areaId, value);
                areaId = value;
            }
        }

        public string   AreaName
        {
            get
            { return areaName;}
            set
            { 
                AddToAudit("AreaName", areaName, value);
                areaName = value;
            }
        }

        public int      SiteId
        {
            get
            { return siteId;}
            set
            { 
                AddToAudit("SiteId", siteId, value);
                siteId = value;
            }
        }

        public string   BedroomType
        {
            get
            { return bedroomType;}
            set
            { 
                AddToAudit("BedroomType", bedroomType, value);
                bedroomType = value;
            }
        }

        public decimal  AnnualDailyCharge
        {
            get
            { return annualDailyCharge;}
            set
            { 
                AddToAudit("AnnualDailyCharge", annualDailyCharge, value);
                annualDailyCharge = value;
            }
        }

        public decimal  AnnualWeeklyCharge
        {
            get
            { return annualWeeklyCharge;}
            set
            { 
                AddToAudit("AnnualWeeklyCharge", annualWeeklyCharge, value);
                annualWeeklyCharge = value;
            }
        }

        public decimal  InstalmentDailyCharge
        {
            get
            { return instalmentDailyCharge;}
            set
            { 
                AddToAudit("InstalmentDailyCharge", instalmentDailyCharge, value);
                instalmentDailyCharge = value;
            }
        }

        public decimal  InstalmentWeeklyCharge
        {
            get
            { return instalmentWeeklyCharge;}
            set
            { 
                AddToAudit("InstalmentWeeklyCharge", instalmentWeeklyCharge, value);
                instalmentWeeklyCharge = value;
            }
        }

        public decimal  TotalRent
        {
            get
            { return totalRent;}
            set
            { 
                AddToAudit("TotalRent", totalRent, value);
                totalRent = value;
            }
        }

        public string PropertyAddress
        {
            get
            {
                return _propertyAddress;
            }
            set
            {
                AddToAudit("PropertyAddress", _propertyAddress, value);
            	_propertyAddress = value;
            }
        }

        public string AdditionalInformation1
        {
            get
            {
                return _additionalInformation1;
            }
        }

        public DateTime CurrentStartDate
        {
            get
            {
                return GetAllocationDate(true);
            }
        }

        public DateTime CurrentEndDate
        {
            get
            {
                return GetAllocationDate(false);
            }
        }

        public string UniqueId
        {
            get
            {
                return String.Format("{0}X{1}X{2}X{3}X{4}X{5}X{6}X{7}X{8}X", academicYearId, CityName, LetLength, annualAllocationModelId, annualPricingModelId, instalmentAllocationModelId, instalmentPricingModelId, bedroomTypeId, areaId);
            }
        }

        public KxAllocationModel AnnualAllocationModel
        {
            get
            {
                if (annualAllocationModel == null && annualAllocationModelId > 0)
                {
                    annualAllocationModel = KxStudentObjectManager.AllocationModels.GetModelById(annualAllocationModelId,AcademicYearId);
                }
                return annualAllocationModel;
            }
        }
        public KxAllocationModel InstalmentAllocationModel
        {
            get
            {
                if (instalmentAllocationModel == null && instalmentAllocationModelId > 0)
                {
                    instalmentAllocationModel = KxStudentObjectManager.AllocationModels.GetModelById(instalmentAllocationModelId, AcademicYearId);
                }
                return instalmentAllocationModel;
            }
        }

        public KxPricingModel AnnualPricingModel
        {
            get
            {
                if (annualPricingModel == null && annualPricingModelId > 0)
                {
                    annualPricingModel = KxStudentObjectManager.PricingModels.GetPricingModel(annualPricingModelId);
                }
                return annualPricingModel;
            }
        }
        public KxPricingModel InstalmentPricingModel
        {
            get
            {
                if (instalmentPricingModel == null && instalmentPricingModelId > 0)
                {
                    instalmentPricingModel = KxStudentObjectManager.PricingModels.GetPricingModel(instalmentPricingModelId);
                }
                return instalmentPricingModel;
            }
        }

        public DateTime AnnualStartDate
        {
            get
            {
                return AnnualAllocationModel.Dates.StartDate;
            }
        }

        public DateTime AnnualEndDate
        {
            get
            {
                return AnnualAllocationModel.Dates.EndDate;
            }
        }

        public DateTime InstalmentStartDate
        {
            get
            {
                return InstalmentAllocationModel.Dates.StartDate;
            }
        }

        public DateTime InstalmentEndDate
        {
            get
            {
                return InstalmentAllocationModel.Dates.EndDate;
            }
        }


        public KxSiteAvailabilityLookup AssociatedSiteLookup
        {
            get
            {
                if (associatedSiteLookup == null)
                {
                    associatedSiteLookup = KxStudentObjectManager.SiteAvailabilityLookups.GetSiteAvailabilityLookup(SiteId,AreaId,0);
                }
                return associatedSiteLookup;
            }
        }

        public bool HasPayment1
        {
            get
            {
                return paymentAmounts[1] > 0.00M;
            }
        }
        public bool HasPayment2
        {
            get
            {
                return paymentAmounts[2] > 0.00M;
            }
        }
        public bool HasPayment3
        {
            get
            {
                return paymentAmounts[3] > 0.00M;
            }
        }
        public bool HasPayment4
        {
            get
            {
                return paymentAmounts[4] > 0.00M;
            }
        }
        public bool HasPayment5
        {
            get
            {
                return paymentAmounts[5] > 0.00M;
            }
        }
        public bool HasPayment6
        {
            get
            {
                return paymentAmounts[6] > 0.00M;
            }
        }



        public Decimal PaymentAmount1
        {
            get
            {
                if (paymentAmounts[1] == 0.00M)
                {
                    CalculatePayments();
                }
                return paymentAmounts[1];
            }
        }
        public Decimal PaymentAmount2
        {
            get
            {
                if (paymentAmounts[1] == 0.00M)
                {
                    CalculatePayments();
                }
                return paymentAmounts[2];
            }
        }
        public Decimal PaymentAmount3
        {
            get
            {
                if (paymentAmounts[1] == 0.00M)
                {
                    CalculatePayments();
                }
                return paymentAmounts[3];
            }
        }
        public Decimal PaymentAmount4
        {
            get
            {
                if (paymentAmounts[1] == 0.00M)
                {
                    CalculatePayments();
                }
                return paymentAmounts[4];
            }
        }
        public Decimal PaymentAmount5
        {
            get
            {
                if (paymentAmounts[1] == 0.00M)
                {
                    CalculatePayments();
                }
                return paymentAmounts[5];
            }
        }
        public Decimal PaymentAmount6
        {
            get
            {
                if (paymentAmounts[1] == 0.00M)
                {
                    CalculatePayments();
                }
                return paymentAmounts[6];
            }
        }

        public DateTime PaymentDate1
        {
            get
            {
                if (paymentAmounts[1] == 0.00M)
                {
                    CalculatePayments();
                }
                return paymentDates[1];
            }
        }
        public DateTime PaymentDate2
        {
            get
            {
                if (paymentAmounts[1] == 0.00M)
                {
                    CalculatePayments();
                }
                return paymentDates[2];
            }
        }
        public DateTime PaymentDate3
        {
            get
            {
                if (paymentAmounts[1] == 0.00M)
                {
                    CalculatePayments();
                }
                return paymentDates[3];
            }
        }
        public DateTime PaymentDate4
        {
            get
            {
                if (paymentAmounts[1] == 0.00M)
                {
                    CalculatePayments();
                }
                return paymentDates[4];
            }
        }
        public DateTime PaymentDate5
        {
            get
            {
                if (paymentAmounts[1] == 0.00M)
                {
                    CalculatePayments();
                }
                return paymentDates[5];
            }
        }
        public DateTime PaymentDate6
        {
            get
            {
                if (paymentAmounts[1] == 0.00M)
                {
                    CalculatePayments();
                }
                return paymentDates[6];
            }
        }

        public Decimal TotalAmount
        {
            get
            {
                return totalAmount;
            }
        }

        public string PaymentType
        {
            get
            {
                if (AvailabilityList.PaymentScheduleType == 1)
                {
                    return "Annual Payment";
                }
                else
                {
                    return "Instalment Payments";
                }
            }
        }

        #endregion 
        
        public KxWebOpalAvailability(KxBaseClass parent) : base(parent)
        {
            
        }

        public KxWebOpalAvailability(KxBaseClass parent, XmlNode node): base(parent)
        {
            LoadFromXml("",node);
            
        }

        public override KxData ConfigureData(ActionType actiontype)
        {

            return null;
        }


        public override  void LoadFromXml(string xml,XmlNode node)
        {
            academicYearId  = KxXmlUtils.GetNodeValue(node,"academicyearid",0);
            cityName  = KxXmlUtils.GetNodeValue(node,"cityname","");
            letLength  = KxXmlUtils.GetNodeValue(node,"letlength","");
            annualAllocationModelId  = KxXmlUtils.GetNodeValue(node,"annualallocationmodelid",0);
            annualPricingModelId  = KxXmlUtils.GetNodeValue(node,"annualpricingmodelid",0);
            instalmentAllocationModelId  = KxXmlUtils.GetNodeValue(node,"instalmentallocationmodelid",0);
            instalmentPricingModelId  = KxXmlUtils.GetNodeValue(node,"instalmentpricingmodelid",0);
            bedroomTypeId  = KxXmlUtils.GetNodeValue(node,"bedroomtypeid",0);
            areaId  = KxXmlUtils.GetNodeValue(node,"areaid",0);
            areaName  = KxXmlUtils.GetNodeValue(node,"areaname","");
            siteId  = KxXmlUtils.GetNodeValue(node,"siteid",0);
            bedroomType  = KxXmlUtils.GetNodeValue(node,"bedroomtype","");
            annualDailyCharge  = KxXmlUtils.GetNodeValue(node,"annualdailycharge",(decimal)0.00M);
            annualWeeklyCharge  = KxXmlUtils.GetNodeValue(node,"annualweeklycharge",(decimal)0.00M);
            instalmentDailyCharge  = KxXmlUtils.GetNodeValue(node,"instalmentdailycharge",(decimal)0.00M);
            instalmentWeeklyCharge  = KxXmlUtils.GetNodeValue(node,"instalmentweeklycharge",(decimal)0.00M);
            totalRent  = KxXmlUtils.GetNodeValue(node,"totalrent",(decimal)0.00M);
            _propertyAddress = KxXmlUtils.GetNodeValue(node, "propertyaddress", string.Empty);
            _additionalInformation1 = KxXmlUtils.GetNodeValue(node, "additionalinformation1", string.Empty);
        }

        protected void CalculatePayments()
        {
            paymentDates = new DateTime[10];
            paymentAmounts = new Decimal[10];
            totalAmount = 0.00M;
            
            KxAllocationModel alloc;
            KxPricingModel pricing;
            Decimal weeklyAmount = 0.00M;
            Decimal dailyAmount = 0.00M;


            if (AvailabilityList.PaymentScheduleType == 1)
            {
                alloc = new KxAllocationModel(null, annualAllocationModelId, academicYearId);// AnnualAllocationModel;
                alloc.PricingModel = AnnualPricingModel;
            }
            else
            {
                alloc = new KxAllocationModel(null, instalmentAllocationModelId,academicYearId);
                alloc.PricingModel = InstalmentPricingModel;
            }

            int count = 1;

            foreach (KxAllocationModelDate date in alloc.GetAllocatedDates())
            {
                paymentAmounts[count] = date.Charges;

                totalAmount += date.Charges;

                paymentDates[count] = GetDueDate(date);

                count++;
            }
        }

        private DateTime GetDueDate(KxAllocationModelDate date)
        {
            DateTime result = date.StartDate.Value;

            /// Only 2010R2 onwards will have a due date value populated, and so
            /// we use that if it is there, otherwise we work in the original format
            /// by using the due date adjuster running.
            /// 
            if (date.DueDateColumnExists)
            {
                result = date.DueDate.Value;
            }
            else
            {
                KxWebAdjustedDueDate dueDate = KxStudentObjectManager.AdjustedDueDates.GetAdjustedDueDate(areaId, AvailabilityList.PaymentScheduleType, date.StartDate.Value);

                if (dueDate != null)
                {
                    result = dueDate.DueDate.Value;
                }
                else
                {
                    //debug
                    result = date.StartDate.Value;
                }
            }

            return result;
        }

        private DateTime GetAllocationDate(bool isStartDate)
        {
            DateTime result;
            try
            {
                if (AvailabilityList.PaymentScheduleType == 2 && InstalmentAllocationModel != null)
                {
                    if (isStartDate)
                    {
                        result = InstalmentAllocationModel.Dates.StartDate;
                    }
                    else
                    {
                        result = InstalmentAllocationModel.Dates.EndDate;
                    }
                }
                else
                {
                    if (isStartDate)
                    {
                        result = AnnualAllocationModel.Dates.StartDate;
                    }
                    else
                    {
                        result = AnnualAllocationModel.Dates.EndDate;
                    }
                }
            }
            catch
            {
                result = new DateTime(1899, 12, 30);
            }

            return result;
        }

    }
}

