﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Configuration;
using Kx.Core.Data;
using Kx.Core.Exceptions;
using Kx.Core.Utility;
using Kx.Objects.Common;
using Kx.Objects.Common.Web;
using Kx.Objects.Student;

namespace Kx.Objects.Student.Web
{
    public class KxWebUserSynchronisation : KxBaseClass
    {
        protected bool hasSynchronised = false;

        public KxWebUserStudent WebUser
        {
            get
            {
                return (KxWebUserStudent)parent;
            }
        }

        public KxWebUserSynchronisation(KxBaseClass parent):base(parent)
        {
            
        }

        public void SynchroniseUser()
        {
            if (!hasSynchronised && WebUser.KxStudentId > 0)
            {
                hasSynchronised = true;

                if (!String.IsNullOrEmpty(KxDbConfiguration.GetConfiguration("CodeSynchApplications")))
                {
                    SynchroniseApplications();
                }

                if (!String.IsNullOrEmpty(KxDbConfiguration.GetConfiguration("CodeSynchAllocations")))
                {
                    SynchroniseAllocations();
                }

                if (!String.IsNullOrEmpty(KxDbConfiguration.GetConfiguration("CodeSynchUdfsKx")))
                {
                    SynchroniseUdfsToKx();
                }

                if (!String.IsNullOrEmpty(KxDbConfiguration.GetConfiguration("CodeSynchUdfsWeb")))
                {
                    SynchroniseUdfsToWeb();
                }
                WebUser.Save();//   pha - why does it do this? screws up royally
            }
        }

        public enum KxSynchAppStatus {Existing, ForYear, Expired };

        public void SynchroniseApplications()
        {
           
            KxSynchAppStatus appStatus = KxSynchAppStatus.Existing;

            foreach (KxStudentApplication sa in WebUser.Student.Applications.Items)
            {
                
                KxWebStudentApplication wsa = WebUser.Applications.GetApplicationByid(sa.ApplicationId);

                if (wsa == null)
                {
                    wsa = WebUser.Applications.GetWebApplicationForYearAndSession(sa.AcademicYearId, sa.SessionId);

                    if (wsa == null)
                    {
                        // No Application For Year - Check for Expired Application
                        wsa = WebUser.Applications.GetWebApplicationsForYearAndSession(sa.AcademicYearId, sa.SessionId, true).FirstOrDefault();

                        if (wsa != null)
                        {
                            appStatus = KxSynchAppStatus.Expired;
                        }
                    }
                    else if (wsa.ApplicationId == 0)
                    {
                        appStatus = KxSynchAppStatus.ForYear;
                    }
                    else
                    {
                        wsa = null;
                    }
                }

                if (wsa != null)
                {
                    if (appStatus == KxSynchAppStatus.Existing)
                    {
                        if (sa.Expired && KxDbConfiguration.GetConfiguration("AllowMultipleAppsForYear") != "")
                        {
                            wsa.Expired = true;
                            wsa.Changed = true;

                        }
                    }
                    else if (appStatus == KxSynchAppStatus.ForYear)
                    {
                        wsa.ApplicationId = sa.ApplicationId;
                        wsa.ConfirmedDate = sa.ApplicationDate;
                        wsa.KxStudentId = sa.KxStudentId;
                        wsa.ApplicationTypeId = sa.ApplicationTypeId;
                        wsa.CourseTypeId = sa.CourseTypeId;
                        wsa.StudentYearId = sa.StudentYearId;
                        wsa.LicenceTypeId = sa.LicenceTypeId;
                        try
                        {
                            wsa.Title = WebUser.Student.Title;
                            wsa.Dob = WebUser.Student.Dob;
                        }
                        catch { }
                        
                    }
                }
                else if (wsa == null)
                {
                    //Add Application.....
                    wsa = new KxWebStudentApplication(WebUser.Applications);
                    wsa.ConfirmedDate = sa.ApplicationDate;
                    wsa.CreationDate = sa.CreationDate;
                    wsa.AcademicYearId = sa.AcademicYearId;
                    wsa.SessionID = sa.SessionId;
                    wsa.SessionStartDate = sa.SessionStartDate;
                    wsa.SessionEndDate = sa.SessionEndDate;
                    wsa.KxStudentId = sa.KxStudentId;
                    wsa.ApplicationId = sa.ApplicationId;
                    wsa.ApplicationTypeId = sa.ApplicationTypeId;
                    wsa.CourseTypeId = sa.CourseTypeId;
                    wsa.StudentYearId = sa.StudentYearId;
                    wsa.LicenceTypeId = sa.LicenceTypeId;
                    try
                    {
                        wsa.Title = WebUser.Student.Title;
                        wsa.Dob = WebUser.Student.Dob;
                    }
                    catch { }
                    wsa.Changed = true;
                }

                if (sa.Expired && !wsa.Expired && KxDbConfiguration.GetConfiguration("AllowMultipleAppsForYear") != "")
                {
                    wsa.Expired = true;
                    wsa.Changed = true;
                }
            }
        }

        public void SynchroniseAllocations()
        {
            foreach (KxStudentAllocation sa in WebUser.Student.Allocations.Items)
            {
                if (sa.OfferDate > KxDateTime.KxApplicableDate)
                {
                    KxWebStudentAllocation alloc = WebUser.Allocations.GetAllocationById(sa.AllocationId);

                    if (alloc == null)
                    {
                        alloc = new KxWebStudentAllocation(WebUser.Allocations);
                        alloc.AllocationId = sa.AllocationId;
                        alloc.UserGuid = WebUser.UserGuid;
                        alloc.Changed = true;
                        WebUser.Allocations.Items.Add(alloc);

                    }

                    if (sa.AcceptedDate > KxDateTime.KxApplicableDate && alloc.AcceptedDate == null)
                    {
                        alloc.AcceptedDate = sa.AcceptedDate;
                        alloc.Processed = true;
                        alloc.Changed = true;
                    }
                    else if (sa.RejectedDate > KxDateTime.KxApplicableDate && alloc.RejectedDate == null)
                    {
                        alloc.RejectedDate = sa.RejectedDate;
                        alloc.Processed = true;
                        alloc.RejectedReason = sa.RejectedReasonId;
                        alloc.Changed = true;
                    }
                }
            }
        }

        public void SynchroniseUdfsToKx()
        {
            /* NOT IMPLEMENTED AS WOULD REQUIRE UPDATE OF GERM DB
            foreach (KxWebStudentApplication wsa in WebUser.Applications.Items)
            {
                if (wsa.ApplicationId > 0)
                {
                    KxStudentApplication sa = WebUser.Applications.GetApplicationByid(wsa.ApplicationId);

                    if (sa != null)
                    {
                        foreach (KxUserDefinedValue uv in wsa.UserDefinedValues.Items)
                        {
                            int germUdf = KxStudentObjectManager.UdfMappings.GetGermUdf(uv.CustomFieldId);

                            KxCustomFields2Value cv = sa.CustomValues.GetUDFById(germUdf);

                            if (cv != null)
                            {

                            }
                            else
                            {

                            }
                        }
                    }
                }
            }
             * */
        }

        public void SynchroniseUdfsToWeb()
        {
            foreach (KxStudentApplication sa in WebUser.Student.Applications.Items)
            {

                KxWebStudentApplication wsa = WebUser.Applications.GetApplicationByid(sa.ApplicationId);

                if (wsa != null)
                {
                    foreach (KxCustomFields2Value cv in sa.CustomValues.Items)
                    {
                        int webUdf = KxStudentObjectManager.UdfMappings.GetWebUdf(cv.CustomFieldId);

                        KxUserDefinedValue uv = wsa.UserDefinedValues.GetValueById(webUdf);

                        if (uv != null)
                        {
                            //update UDF
                            if (uv.FieldValue != cv.FieldValue)
                            {
                                uv.FieldValue = cv.FieldValue;
                                uv.Changed = true;
                            }
                        }
                        else
                        {
                            //Add UDF
                            uv = new KxUserDefinedValue(wsa.UserDefinedValues);
                            uv.CustomFieldId = webUdf;
                            uv.FieldValue = cv.FieldValue;
                            uv.ApplicationGuid = wsa.ApplicationGuid;
                            uv.Changed = true;

                            wsa.UserDefinedValues.Items.Add(uv);
                        }
                    }
                }
               
            }
        }
    }
}
