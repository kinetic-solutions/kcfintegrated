﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Exceptions;
using Kx.Core.Utility;

namespace Kx.Objects.Student.Web
{
    public class KxWebStudentCorrespondanceList : KxBaseClass
    {
        private List<KxWebStudentCorrespondance> items = new List<KxWebStudentCorrespondance>();
        private string _documentName = "";

        public List<KxWebStudentCorrespondance> Items
        {
            get
            { return items; }
        }

        public KxWebStudentCorrespondance this[int index]
        {
            get
            { return items[index]; }
            set
            { items[index] = value; }
        }


        public KxWebStudentCorrespondanceList(KxBaseClass parent, int kxstudentid, string documentName)
            : base(parent)
        {
            dBIdentifier = kxstudentid;
            _documentName = documentName;
            Load();
        }

        public KxWebStudentCorrespondanceList(KxBaseClass parent, int kxstudentid)
            : base(parent)
        {
            dBIdentifier = kxstudentid;
            _documentName = "";
            Load();
        }

        public KxWebStudentCorrespondanceList(KxBaseClass parent)
            : base(parent)
        {
        }

        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = null;

            if (actiontype == ActionType.Select)
            {
                result = new KxData("webstudentcorrespondance", "WebStudentCorrespondance", TargetType.Table, ActionType.Select);

                result.KeyFields.Add(new KxDataValue("kxstudentid", dBIdentifier, KxValueType.Int));
                if (_documentName != "")
                {
                    result.AddParameter("DocumentName", _documentName, KxValueType.String);
                }
            }
            return result;
        }

        public override bool LoadFromXml(string xml, string xpath)
        {
            items.Clear();
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xml);

                XmlNodeList nodes = doc.SelectNodes(@"//results/" + xpath + "/datarow");

                foreach (XmlNode node in nodes)
                {
                    if (node != null)
                    {
                        KxWebStudentCorrespondance item = new KxWebStudentCorrespondance(this, node);
                        items.Add(item);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "LoadFromXml", ex.Message, xpath);
                LastError = ex.Message;
                return false;
            }
        }

    }
}
