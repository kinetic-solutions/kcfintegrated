using System;
using System.Collections.Generic;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;


namespace Kx.Objects.Student.Web
{
    [Serializable]
    public class KxVitaPortalTransfer : KxBaseClass
    {
        #region Attributes & Properties
        protected bool loaded = false;

        protected string vitaWebId;
        protected string   emailAddress;
        protected string   title;
        protected string firstName;
        protected string   lastName;
        protected string   gender;
        protected string   mobileNumber;
        protected string   university;
        protected string enquirySource;
        

        public bool Loaded
        {
            get
            {
                return loaded;
            }
            set
            {
                loaded = value;
            }
        }

        public string VitaWebId
        {
            get
            { return vitaWebId; }
            set
            {
                AddToAudit("VitaWebId", vitaWebId, value);
                vitaWebId = value;
            }
        }

        public string   EmailAddress
        {
            get
            { return emailAddress;}
            set
            { 
                AddToAudit("EmailAddress", emailAddress, value);
                emailAddress = value;
            }
        }

        public string   Title
        {
            get
            { return title;}
            set
            { 
                AddToAudit("Title", title, value);
                title = value;
            }
        }

        public string FirstName
        {
            get
            { return firstName; }
            set
            {
                AddToAudit("FirstName", firstName, value);
                firstName = value;
            }
        }

        public string   LastName
        {
            get
            { return lastName; }
            set
            {
                AddToAudit("LastName", lastName, value);
                lastName = value;
            }
        }

  

        public string   Gender
        {
            get
            { return gender;}
            set
            { 
                AddToAudit("Gender", gender, value);
                gender = value;
            }
        }

        public string   MobileNumber
        {
            get
            { return mobileNumber;}
            set
            { 
                AddToAudit("MobileNumber", mobileNumber, value);
                mobileNumber = value;
            }
        }

       
        public string   University
        {
            get
            { return university;}
            set
            { 
                AddToAudit("University", university, value);
                university = value;
            }
        }

        public string EnquirySource
        {
            get
            { return enquirySource; }
            set
            {
                AddToAudit("EnquirySource", enquirySource, value);
                enquirySource = value;
            }
        }

        #endregion 

        public KxVitaPortalTransfer(KxBaseClass parent): base(parent)
        {
        }

        public KxVitaPortalTransfer(KxBaseClass parent, string reference)
            : base(parent)
        {
            vitaWebId = reference;
            Load();
        }

        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("vitaportaltransferlookup", "VitaPortalTransferLookup", TargetType.Table);

            result.KeyFields.Add(new KxDataValue("vitaWebId",vitaWebId,KxValueType.String));


            if ((actiontype == ActionType.Update) && (!loaded))
            {
                result.Action = ActionType.Insert;
            }
            else
            {
                result.Action = actiontype;
            }

            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {
                   result.AddParameter("emailAddress", emailAddress, KxValueType.String, 50, true);
                   result.AddParameter("title", title, KxValueType.String, 10, true);
                   result.AddParameter("firstName", firstName, KxValueType.String, 50, true);
                   result.AddParameter("lastName", lastName, KxValueType.String, 50, true);
                   result.AddParameter("gender", gender, KxValueType.String, 1, true);
                   result.AddParameter("mobileNumber", mobileNumber, KxValueType.String, 25, true);
                   result.AddParameter("university", university, KxValueType.String, 50, true);
                   result.AddParameter("enquirySource", enquirySource, KxValueType.String, 255, true); 
                  
            }
            return result;
        }


        public override  void LoadFromXml(string xml,XmlNode node)
        {
            vitaWebId = KxXmlUtils.GetNodeValue(node, "vitawebid", "");
            emailAddress  = KxXmlUtils.GetNodeValue(node,"emailaddress","");
            title  = KxXmlUtils.GetNodeValue(node,"title","");
            firstName = KxXmlUtils.GetNodeValue(node, "firstname", "");
            lastName  = KxXmlUtils.GetNodeValue(node,"lastname","");
            gender  = KxXmlUtils.GetNodeValue(node,"gender","");
            mobileNumber  = KxXmlUtils.GetNodeValue(node,"mobilenumber","");
            university = KxXmlUtils.GetNodeValue(node, "university", "");
            enquirySource = KxXmlUtils.GetNodeValue(node, "enquirySource", "");

            loaded = !String.IsNullOrEmpty(firstName);
        }
    }
}

