using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Configuration;
using Kx.Core.Utility;
using Kx.Core.Exceptions;

namespace Kx.Objects.Student.Web
{
    public enum CodeType { Url, Group, PasswordReset };
    [Serializable]
    public class KxWebUserShortUrl : KxBaseClass
    {
        #region Attributes & Properties
        protected bool loaded = false;
        protected string shortCode;
        protected Guid userGuid;
        protected string shortUrl;
        protected string longUrl;
        protected DateTime? expiryDate;
        protected bool codeUsed = false;

        public string ShortCode
        {
            get
            { return shortCode; }
            set
            {
                AddToAudit("ShortCode", shortCode, value);
                shortCode = value;
            }
        }

        public Guid UserGuid
        {
            get
            { return userGuid; }
            set
            {
                AddToAudit("UserGuId", userGuid, value);
                userGuid = value;
            }
        }

        public string ShortUrl
        {
            get
            { return shortUrl; }
            set
            {
                AddToAudit("ShortUrl", shortUrl, value);
                shortUrl = value;
            }
        }

        public string LongUrl
        {
            get
            { return longUrl; }
            set
            {
                AddToAudit("LongUrl", longUrl, value);
                longUrl = value;
            }
        }

        public DateTime? ExpiryDate
        {
            get
            { return expiryDate; }
            set
            {
                AddToAudit("ExpiryDate", expiryDate, value);
                expiryDate = value;
            }
        }

        public bool IsCodeValid
        {
            get
            {
                return ExpiryDate != null && ExpiryDate > DateTime.Now;
            }
        }

        public bool IsGroupCode
        {
            get
            {
                return ShortUrl == "GROUPCODE" && ExpiryDate > DateTime.Now;
            }
        }

        public bool CodeUsed
        {
            get { return codeUsed; }
            set { codeUsed = value; }
        }


        #endregion 

        public KxWebUserShortUrl(KxBaseClass parent, Guid userGuid) : base(parent)
        {
            InitialiseShortCode(userGuid);
            Load();
        }

        public KxWebUserShortUrl(KxBaseClass parent, Guid userGuid, CodeType codeType) : base(parent)
        {
            if (codeType == CodeType.Url)
            {
                InitialiseShortCode(userGuid);
                Load();
            }
            else if (codeType == CodeType.PasswordReset)
            {
                InitialisePasswordResetShortCode(userGuid);
                Load();
            }
            else
            {
                InitialiseGroupShortCode(userGuid);
            }
        }

        public KxWebUserShortUrl(KxBaseClass parent, string shortCode) : base(parent)
        {
            this.ShortCode = shortCode;

            Load();
        }

        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("webusershorturl", "WebUserShortUrl", TargetType.Table);

            result.AddKeyField("ShortCode", shortCode, KxValueType.String);

            if ((actiontype == ActionType.Update) && (!loaded))
            {
                result.Action = ActionType.Insert;
                loaded = true;
            }
            else
            {
                result.Action = actiontype;
            }

            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {
                result.AddParameter("userGuid", userGuid, KxValueType.Guid, true);
                result.AddParameter("shortUrl", shortUrl, KxValueType.String, 255, true);
                result.AddParameter("longUrl", longUrl, KxValueType.String, 255, true);
                result.AddParameter("expiryDate", expiryDate, KxValueType.DateTime, true);
                result.AddParameter("codeUsed", codeUsed, KxValueType.Boolean, true);
            }
            return result;
        }


        public override void LoadFromXml(string xml, XmlNode node)
        {
            shortCode = KxXmlUtils.GetNodeValue(node, "shortcode", "");
            userGuid = KxXmlUtils.GetNodeValue(node, "userguid", Guid.Empty);
            shortUrl = KxXmlUtils.GetNodeValue(node, "shorturl", "");
            longUrl = KxXmlUtils.GetNodeValue(node, "longurl", "");
            expiryDate = KxXmlUtils.GetNodeValueDT(node, "expirydate", null);

            int isCodeUsed = KxXmlUtils.GetNodeValueBoolInt(node, "codeUsed", 0);
            codeUsed = isCodeUsed == 0 ? false : true;
            
            loaded = true;
        }

        protected string GenerateShortCode()
        {
            const string chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";

            Random rand = new Random(DateTime.Now.Second * (DateTime.Now.Minute * DateTime.Now.Millisecond));

            KxWebUserShortUrl map = null;

            string result;

            do
            {
                result = "";

                for (int i = 0; i < 6; i++)
                {
                    result += chars[rand.Next(35)];

                }

                map = new KxWebUserShortUrl(null, result);


            } while (map == null || !String.IsNullOrEmpty(map.LongUrl));


            return result;
        }

        public void InitialiseShortCode(Guid userguid)
        {
            shortCode = GenerateShortCode();
            UserGuid = userguid;
            LongUrl = KxDbConfiguration.GetConfiguration("HomePageUrl") + "?wf=" + KxDbConfiguration.GetConfiguration("EmailConfirmationWorkflow") + "&c=" + shortCode;
            ShortUrl = KxDbConfiguration.GetConfiguration("HomePageUrl").ToLower().Replace("default.aspx", "c.aspx?x=" + shortCode);
            ExpiryDate = DateTime.Now.AddDays(KxDbConfiguration.GetConfigurationInt("ConfirmUrlPeriod", 7));
            Changed = true;
            Save();

        }

        public void InitialiseGroupShortCode(Guid userguid)
        {
            shortCode = GenerateShortCode();
            UserGuid = userguid;
            LongUrl = "";
            ShortUrl = "GROUPCODE";
            ExpiryDate = DateTime.Today;
            Changed = true;
            Save();


        }

        public void InitialisePasswordResetShortCode(Guid userguid)
        {
            shortCode = GenerateShortCode();
            UserGuid = userguid;
            LongUrl = KxDbConfiguration.GetConfiguration("HomePageUrl") + "?wf=" + KxDbConfiguration.GetConfiguration("PasswordResetWorkflow") + "&c=" + shortCode;
            ShortUrl = KxDbConfiguration.GetConfiguration("HomePageUrl").ToLower().Replace("default.aspx", "c.aspx?x=" + shortCode);
            ExpiryDate = DateTime.Now.AddDays(KxDbConfiguration.GetConfigurationInt("ConfirmUrlPeriod", 7));
            Changed = true;
            Save();

        }
    }
}

