using System;
using System.Collections.Generic;
using System.Text;

namespace Kx.Objects.Student.Web
{
    public class KxWebPaymentSchedule
    {
        private int paymentNumber = 0;
        private DateTime paymentDate;
        private Decimal paymentAmount = 0M;

        public int PaymentNumber
        {
            get
            { return paymentNumber; }
            set
            { paymentNumber = value; }
        }

        public DateTime PaymentDate
        {
            get
            { return paymentDate; }
            set
            { paymentDate = value; }
        }

        public Decimal PaymentAmount
        {
            get
            { return paymentAmount; }
            set
            { paymentAmount = value; }
        }

        public KxWebPaymentSchedule(int PayNo, DateTime payDate, Decimal paymentValue)
        {
            PaymentNumber = PayNo;
            paymentDate = payDate;
            paymentAmount = paymentValue;
        }
    }
}
