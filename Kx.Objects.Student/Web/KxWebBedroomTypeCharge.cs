using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Configuration;
using Kx.Core.Data;
using Kx.Core.Reflection;
using Kx.Core.Utility;
using Kx.Core.Exceptions;
using Kx.Objects.Common;

namespace Kx.Objects.Student.Web
{
    [Serializable]
    public class KxWebBedroomTypeCharge : KxBaseClass
    {
        #region Attributes & Properties

        protected int academicYearId = 0;
        protected int allocationModelId = 0;
        protected int defaultPricingModelId = 0;
        protected int bedroomTypeId = 0;
        protected int subBlockId = 0;
        protected int blockId = 0;
        protected int areaId = 0;
        protected int siteId = 0;
        protected string description = "";
        protected int floorNumber = 0;
        protected int smoking = 2;
        protected int disabled = 2;
        protected int gender = 0;
        protected int singleSex = 2;
        protected int courseTypeId = 0;
        protected int studentYearId = 0;
        protected int customField1 = 2;
        protected int customField2 = 2;
        protected string customValue1 = "";
        protected string customValue2 = "";
        protected string customValue3 = "";
        protected string customValue4 = "";
        protected string customValue5 = "";
        protected string customValue6 = "";
        protected string customValue7 = "";
        protected string customValue8 = "";
        protected decimal dailyCharge = 0.0M;
        protected decimal weeklyCharge = 0.0M;
        protected decimal totalRent = 0.00M;
        protected int contractLength = 0;
        protected string webDescription = "";
        protected string areaName = "";

        protected DateTime ?contractStartDate = null;
        protected DateTime ?contractEndDate = null;
       
        protected int residentialRoomId  = 0;
        protected string distinctFieldValues;

        protected DateTime? lastAvailabilityCheck = null;
        protected bool lastAvailabilityValue = false;
        protected KxBedroomType bedroomType = null;
        protected KxSiteBlock siteBlock = null;
        protected KxSiteBlock siteSubBlock = null;
        protected KxAllocationModel allocationModel = null;

        protected int paymentScheduleTypeId = 0;
        protected KxPaymentScheduleType paymentScheduleType = null;
        
        protected string distinctFields = "";

        public string DistinctFields
        {
            get
            {
                return distinctFields;
            }
            set
            {
                if (distinctFields == value)
                    return;
                distinctFields = value;
                distinctFieldValues = "";
            }
        }

        public string DistinctFieldValues
        {
            get
            {
                if (String.IsNullOrEmpty(distinctFieldValues) && !String.IsNullOrEmpty(distinctFields))
                {
                    distinctFieldValues = GetDistinctFieldValues();
                }
                return distinctFieldValues;
            }
        }

        public string ApplicationId = "";


        private List<KxWebPaymentSchedule> semesterPayments = new List<KxWebPaymentSchedule>();
        
        public bool IsAvailable
        {
            get
            {
                return true;
            }
        }

        public int AcademicYearId
        {
            get
            { return academicYearId; }
            set
            {
                AddToAudit("AcademicYearId", academicYearId, value);
                academicYearId = value;
            }
        }

        public int AllocationModelId
        {
            get
            { return allocationModelId; }
            set
            {
                AddToAudit("AllocationModelId", allocationModelId, value);
                allocationModelId = value;
            }
        }

        public int DefaultPricingModelId
        {
            get
            { return defaultPricingModelId; }
            set
            {
                AddToAudit("DefaultPricingModelId", defaultPricingModelId, value);
                defaultPricingModelId = value;
            }
        }

        public int BedroomTypeId
        {
            get
            { return bedroomTypeId; }
            set
            {
                AddToAudit("BedroomTypeId", bedroomTypeId, value);
                bedroomTypeId = value;
            }
        }

        public string BedroomTypeName
        {
            get
            {
                if (BedroomType != null)
                {
                    return BedroomType.Description;
                }
                else
                {
                    return "";
                }
            }
        }

        public KxBedroomType BedroomType
        {
            get
            {
                if (bedroomType == null && bedroomTypeId > 0)
                {
                    bedroomType = KxStudentObjectManager.BedroomTypes.GetBedroomTypeById(bedroomTypeId);
                }
                return bedroomType;
            }
            set
            {
                if (bedroomType == value)
                    return;
                bedroomType = value;
            }
        }

        public KxSiteBlock Block
        {
            get
            {
                if (blockId > 0 && siteBlock == null)
                {
                    siteBlock = KxStudentObjectManager.SiteBlocks.GetSiteBlockById(blockId);
                }
                return siteBlock;
            }
        }

        public string BlockName
        {
            get
            {
                if (Block != null)
                {
                    return Block.Name;
                }
                else
                {
                    return "";
                }
            }
        }

        public int SubBlockId
        {
            get
            { return subBlockId; }
            set
            {
                AddToAudit("SubBlockId", subBlockId, value);
                subBlockId = value;
            }
        }

        public int BlockId
        {
            get
            { return blockId; }
            set
            {
                AddToAudit("BlockId", blockId, value);
                blockId = value;
            }
        }

        public int AreaId
        {
            get
            { return areaId; }
            set
            {
                AddToAudit("AreaId", areaId, value);
                areaId = value;
            }
        }

        public string AreaName
        {
            get
            {
                if (String.IsNullOrEmpty(areaName) && areaId > 0)
                {
                    KxArea area = KxStudentObjectManager.Areas.GetAreaById(areaId);
                    areaName = area.Description;
                }
                return areaName;
            }
        }

        public int SiteId
        {
            get
            { return siteId; }
            set
            {
                AddToAudit("SiteId", siteId, value);
                siteId = value;
            }
        }

        public string Description
        {
            get
            { return description; }
            set
            {
                AddToAudit("Description", description, value);
                description = value;
            }
        }

        public string WebDescription
        {
            get
            { return webDescription ; }
            set
            {
                webDescription = value;
            }
        }

        public int FloorNumber
        {
            get
            { return floorNumber; }
            set
            {
                AddToAudit("FloorNumber", floorNumber, value);
                floorNumber = value;
            }
        }

        public int Smoking
        {
            get
            { return smoking; }
            set
            {
                AddToAudit("Smoking", smoking, value);
                smoking = value;
            }
        }

        public int Disabled
        {
            get
            { return disabled; }
            set
            {
                AddToAudit("Disabled", disabled, value);
                disabled = value;
            }
        }

        public int Gender
        {
            get
            { return gender; }
            set
            {
                AddToAudit("Gender", gender, value);
                gender = value;
            }
        }

        public string TranslatedGender
        {
            get
            { 
                switch(Gender)
                {
                    case 1  : return "Male Only";
                    case 2  : return "Female Only";
                    default : return "Mixed";
                }
            }
        }

        public int SingleSex
        {
            get
            { return singleSex; }
            set
            {
                AddToAudit("SingleSex", singleSex, value);
                singleSex = value;
            }
        }

        public int CourseTypeId
        {
            get
            { return courseTypeId; }
            set
            {
                AddToAudit("CourseTypeId", courseTypeId, value);
                courseTypeId = value;
            }
        }

        public int StudentYearId
        {
            get
            { return studentYearId; }
            set
            {
                AddToAudit("StudentYearId", studentYearId, value);
                studentYearId = value;
            }
        }

        public int CustomField1
        {
            get
            { return customField1; }
            set
            {
                AddToAudit("CustomField1", customField1, value);
                customField1 = value;
            }
        }

        public int CustomField2
        {
            get
            { return customField2; }
            set
            {
                AddToAudit("CustomField2", customField2, value);
                customField2 = value;
            }
        }

        public string CustomValue1
        {
            get
            { return customValue1; }
            set
            {
                AddToAudit("CustomValue1", customValue1, value);
                customValue1 = value;
            }
        }

        public string CustomValue2
        {
            get
            { return customValue2; }
            set
            {
                AddToAudit("CustomValue2", customValue2, value);
                customValue2 = value;
            }
        }

        public string CustomValue3
        {
            get
            { return customValue3; }
            set
            {
                AddToAudit("CustomValue3", customValue3, value);
                customValue3 = value;
            }
        }

        public string CustomValue4
        {
            get
            { return customValue4; }
            set
            {
                AddToAudit("CustomValue4", customValue4, value);
                customValue4 = value;
            }
        }
        public string CustomValue5
        {
            get
            { return customValue5; }
            set
            {
                AddToAudit("CustomValue5", customValue5, value);
                customValue5 = value;
            }
        }
        public string CustomValue6
        {
            get
            { return customValue6; }
            set
            {
                AddToAudit("CustomValue6", customValue6, value);
                customValue6 = value;
            }
        }
        public string CustomValue7
        {
            get
            { return customValue7; }
            set
            {
                AddToAudit("CustomValue7", customValue7, value);
                customValue7 = value;
            }
        }
        public string CustomValue8
        {
            get
            { return customValue8; }
            set
            {
                AddToAudit("CustomValue8", customValue8, value);
                customValue8 = value;
            }
        }

        public decimal DailyCharge
        {
            get
            { return dailyCharge; }
            set
            {
                AddToAudit("DailyCharge", dailyCharge, value);
                dailyCharge = value;
            }
        }

        public decimal WeeklyCharge
        {
            get
            { return weeklyCharge; }
            set
            {
                AddToAudit("WeeklyCharge", weeklyCharge, value);
                weeklyCharge = value;
            }
        }

        public decimal TotalRentView
        {
             get
            { return totalRent; }
            set
            {
                AddToAudit("totalRent", totalRent, value);
                totalRent = value;
            }
        }

        protected KxAllocationModel AllocationModel
        {
            get
            {
                if (allocationModel == null && allocationModelId != 0)
                {
                    allocationModel = KxStudentObjectManager.AllocationModels.GetModelById(allocationModelId, academicYearId); 
                }
                return allocationModel;
            }
        }

        public int PaymentScheduleTypeId
        {
            get
            { return paymentScheduleTypeId; }
            set
            {
                AddToAudit("paymentScheduleTypeId", paymentScheduleTypeId, value);
                paymentScheduleTypeId = value;
            }
        }

        protected KxPaymentScheduleType PaymentScheduleType
        {
            get
            {
                if (paymentScheduleType == null && paymentScheduleTypeId != 0)
                {
                    paymentScheduleType = new KxPaymentScheduleType(this, paymentScheduleTypeId);
                }
                return paymentScheduleType;
            }
            set
            {
                if (paymentScheduleType == value)
                    return;
                paymentScheduleType = value;
            }

        }



        public Decimal TotalWeeks
        {
            get
            {
                if (KxDbConfiguration.GetConfiguration("ApplicationId").ToUpper() == "LIBERTY")
                {
                    return TotalWeeksUnrounded;
                }
                else
                {
                    return AllocationModel.AllocationWeeks;
                }
            }
        }

        public Decimal TotalWeeksUnrounded
        {
            get
            {
                return AllocationModel.AllocationWeeksUnrounded;
            }
        }

        public Decimal TotalWeeksRoundedUp
        {
            get
            {
                return Math.Ceiling(TotalWeeksUnrounded);
            }
        }

        public Decimal AllocationWeeks
        {
            get
            {
                return AllocationModel.AllocationWeeks;
            }
        }

        public Decimal AllocationRent
        {
            get
            {
                return AllocationWeeks * WeeklyCharge;
            }
        }

        public Decimal TotalRent
        {
            get
            {
                return (TotalWeeks * WeeklyCharge) + AllocationModel.TotalAdditionalCharges;
            }
        }

        public Decimal TotalRentUnrounded
        {
            get
            {
                return (TotalWeeksUnrounded * WeeklyCharge) + AllocationModel.TotalAdditionalCharges;
            }
        }

        public Decimal LibertyDiscTotalRent
        {
            get
            {
                return (TotalRent*98/100);
            }
        }

        public int ResidentialRoomId
        {
            get
            {
                return residentialRoomId;
            }
            set
            {
                residentialRoomId = value;
            }
        }

        public int ContractLength
        {
            get
            {
                return contractLength;
            }
            set
            {
                contractLength = value;
            }
        }


        public DateTime ?ContractStartDate
        {
            get
            {
                if (contractStartDate == null)
                {
                    CalculateSemesterPayments();
                }
                return contractStartDate;
            }
        }


        public DateTime ?ContractEndDate
        {
            get
            {
                if (contractEndDate == null)
                {
                    CalculateSemesterPayments();
                }
                return contractEndDate;
            }
        }

        public int ContractMonths
        {
            get
            {
                return GetContractMonths();
            }
        }

        public List<KxWebPaymentSchedule> SemesterPayments
        {
            get
            { return semesterPayments; }
            set
            { semesterPayments = value; }
        }

        public KxWebPaymentSchedule GetSemesterPayment(int number)
        {
            if (semesterPayments.Count == 0)
            {
                contractStartDate = null;
                contractEndDate = null;

                CalculateSemesterPayments();
            }
            return GetPayment(semesterPayments, number);
        }

        

        private KxWebPaymentSchedule GetPayment(List<KxWebPaymentSchedule> list, int number)
        {
            KxWebPaymentSchedule result = null;

            if (number <= list.Count)
            {
                try
                {
                    result = list[number - 1];
                }
                catch
                { }
            }
            else if (list.Count > 0)
            {
                result = list[list.Count - 1];
            }
            return result;
        }

        #endregion

        public KxWebBedroomTypeCharge(KxBaseClass parent) : base(parent)
        {
            ApplicationId = KxDbConfiguration.GetConfiguration("ApplicationId");
        }

        public KxWebBedroomTypeCharge(KxBaseClass parent, XmlNode node)  : base(parent)
        {
            LoadFromXml("",node);
            ApplicationId = KxDbConfiguration.GetConfiguration("ApplicationId");
        }

        public void CalculateSemesterPayments()
        {
            KxAllocationModelDate date = null;
            decimal subtotal = 0;
            decimal totalcharges = 0;

            try
            {

                KxData data = new KxData("", "SitePaymentScheduleType", TargetType.StoredProc);

                data.AddParameter("SiteID", SiteId, KxValueType.String);

                object result = data.ExecuteScalar();

                if (result != null)
                {
                    try
                    {
                        PaymentScheduleTypeId = Convert.ToInt32(result);
                    }
                    catch
                    { }
                }
            }
            catch 
            { }
         
            for (int loop = 0; loop < allocationModel.Dates.Items.Count; loop++)
            {
                date = allocationModel.Dates.Items[loop];
                
                if ((date.AcademicYearId == academicYearId) && (date.AllocateDuringPeriod == 1) && (date.AllocationModelId == allocationModelId))
                {
                    TimeSpan span = (TimeSpan)(date.EndDate - date.StartDate);
                    decimal periodamount = (date.Days /7) * weeklyCharge;

                    if (contractStartDate == null)
                    {
                        contractStartDate = date.StartDate;
                        contractEndDate = date.EndDate;
                    }
                    else
                    {
                        if (date.StartDate < contractStartDate)
                        {
                            contractStartDate = date.StartDate;
                        }
                        if (date.EndDate > contractEndDate)
                        {
                            contractEndDate = date.EndDate;
                        }
                    }
            
                    if (semesterPayments.Count == 0)
                    {
                        periodamount = periodamount + allocationModel.AdditionalCharges;
                    }

                    subtotal = subtotal + (date.Days / 7);
                    totalcharges = totalcharges + periodamount;
                    if (PaymentScheduleType == null)
                    {
                        semesterPayments.Add(new KxWebPaymentSchedule(loop, (DateTime)date.StartDate, periodamount));

                    }
                    else
                    {
                        semesterPayments.Add(new KxWebPaymentSchedule(loop, (DateTime)date.StartDate, 0));
                    }
                }
            }

            
            if (subtotal < TotalWeeks)
            {
                totalcharges = totalcharges + (weeklyCharge * (TotalWeeks - subtotal));
                if (PaymentScheduleType == null)
                {
                    semesterPayments[semesterPayments.Count - 1].PaymentAmount = semesterPayments[semesterPayments.Count - 1].PaymentAmount + (weeklyCharge * (TotalWeeks - subtotal));
                }
            }


            if (PaymentScheduleType != null)
            {
                decimal AmountToCharge = 0;
                decimal appliedCharges = 0;
                int pcount = 0;

                foreach (KxPaymentScheduleTypeModel model in PaymentScheduleType.Models.Items)
                {
                    if (model.PaymentIntervalType.Trim() == "Percentage On Date" && (model.FixedDate < new DateTime(1980, 1, 1) || model.FixedDate >= contractStartDate))
                    {
                        AmountToCharge = Math.Round((Math.Round((totalcharges / 100.00m) * ((decimal)model.Percentage), 3) * 100), MidpointRounding.AwayFromZero) / 100;

                        appliedCharges += AmountToCharge;

                        if (pcount < semesterPayments.Count)
                        {
                            semesterPayments[pcount].PaymentAmount = AmountToCharge;
                        }

                        pcount++;
                    }
                }
                if (totalcharges > appliedCharges)
                {
                    semesterPayments[0].PaymentAmount = semesterPayments[0].PaymentAmount + Math.Round((totalcharges - appliedCharges), 2);
                }
                else
                if (totalcharges < appliedCharges)
                {
                    semesterPayments[0].PaymentAmount = semesterPayments[0].PaymentAmount + Math.Round((totalcharges - appliedCharges), 2);
                }
            }

        }

        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("webbedroomtypecharge", "WebBedroomTypeCharge", TargetType.Table);

            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {
                result.AddParameter("allocationModelId", allocationModelId, KxValueType.Int, true);
                result.AddParameter("defaultPricingModelId", defaultPricingModelId, KxValueType.Int, true);
                result.AddParameter("bedroomTypeId", bedroomTypeId, KxValueType.Int, false);
                result.AddParameter("subBlockId", subBlockId, KxValueType.Int, false);
                result.AddParameter("blockId", blockId, KxValueType.Int, false);
                result.AddParameter("areaId", areaId, KxValueType.Int, true);
                result.AddParameter("siteId", siteId, KxValueType.Int, false);
                result.AddParameter("description", description, KxValueType.String, 40, false);
                result.AddParameter("floorNumber", floorNumber, KxValueType.Int, true);
                result.AddParameter("smoking", smoking, KxValueType.Int, false);
                result.AddParameter("disabled", disabled, KxValueType.Int, false);
                result.AddParameter("gender", gender, KxValueType.Int, false);
                result.AddParameter("singleSex", singleSex, KxValueType.Int, false);
                result.AddParameter("courseTypeId", courseTypeId, KxValueType.Int, false);
                result.AddParameter("studentYearId", studentYearId, KxValueType.Int, false);
                result.AddParameter("customField1", customField1, KxValueType.Int, false);
                result.AddParameter("customField2", customField2, KxValueType.Int, false);
                result.AddParameter("customValue1", customValue1, KxValueType.String, 50, false);
                result.AddParameter("customValue2", customValue2, KxValueType.String, 50, false);
                result.AddParameter("customValue3", customValue3, KxValueType.String, 50, false);
                result.AddParameter("customValue4", customValue4, KxValueType.String, 50, false);
                result.AddParameter("customValue5", customValue5, KxValueType.String, 50, false);
                result.AddParameter("customValue6", customValue6, KxValueType.String, 50, false);
                result.AddParameter("customValue7", customValue7, KxValueType.String, 50, false);
                result.AddParameter("customValue8", customValue8, KxValueType.String, 50, false);
                result.AddParameter("dailyCharge", dailyCharge, KxValueType.Decimal, false);
                result.AddParameter("weeklyCharge", weeklyCharge, KxValueType.Decimal, false);
                result.AddParameter("totalRent", totalRent, KxValueType.Decimal, false);
                result.AddParameter("contractlength", contractLength, KxValueType.Int, false);
            }
            return result;
        }


        public override void LoadFromXml(string xml, XmlNode node)
        {
            academicYearId = KxXmlUtils.GetNodeValue(node, "academicyearid", 0);
            allocationModelId = KxXmlUtils.GetNodeValue(node, "allocationmodelid", 0);
            defaultPricingModelId = KxXmlUtils.GetNodeValue(node, "defaultpricingmodelid", 0);
            bedroomTypeId = KxXmlUtils.GetNodeValue(node, "bedroomtypeid", 0);
            subBlockId = KxXmlUtils.GetNodeValue(node, "subblockid", 0);
            blockId = KxXmlUtils.GetNodeValue(node, "blockid", 0);
            areaId = KxXmlUtils.GetNodeValue(node, "areaid", 0);
            siteId = KxXmlUtils.GetNodeValue(node, "siteid", 0);
            description = KxXmlUtils.GetNodeValue(node, "description", "");
            floorNumber = KxXmlUtils.GetNodeValue(node, "floornumber", 0);
            smoking = KxXmlUtils.GetNodeValue(node, "smoking", 0);
            disabled = KxXmlUtils.GetNodeValue(node, "disabled", 0);
            gender = KxXmlUtils.GetNodeValue(node, "gender", 0);
            singleSex = KxXmlUtils.GetNodeValue(node, "singlesex", 0);
            courseTypeId = KxXmlUtils.GetNodeValue(node, "coursetypeid", 0);
            studentYearId = KxXmlUtils.GetNodeValue(node, "studentyearid", 0);
            customField1 = KxXmlUtils.GetNodeValue(node, "customfield1", 0);
            customField2 = KxXmlUtils.GetNodeValue(node, "customfield2", 0);
            customValue1 = KxXmlUtils.GetNodeValue(node, "customvalue1", "");
            customValue2 = KxXmlUtils.GetNodeValue(node, "customvalue2", "");
            customValue3 = KxXmlUtils.GetNodeValue(node, "customvalue3", "");
            customValue4 = KxXmlUtils.GetNodeValue(node, "customvalue4", "");
            customValue5 = KxXmlUtils.GetNodeValue(node, "customvalue5", "");
            customValue6 = KxXmlUtils.GetNodeValue(node, "customvalue6", "");
            customValue7 = KxXmlUtils.GetNodeValue(node, "customvalue7", "");
            customValue8 = KxXmlUtils.GetNodeValue(node, "customvalue8", "");
            dailyCharge = KxXmlUtils.GetNodeValue(node, "dailycharge", (decimal)0.00M);
            weeklyCharge = KxXmlUtils.GetNodeValue(node, "weeklycharge", (decimal)0.00M);
            totalRent = KxXmlUtils.GetNodeValue(node, "totalrent", (decimal)0.00M);
            contractLength = KxXmlUtils.GetNodeValue(node, "contractlength", 0);
            webDescription = KxXmlUtils.GetNodeValue(node, "webdescription", "");
        }

        public bool GetAvailability()
        {
            bool result = false;

            KxData data = AddGenericParameters("availability", "GetStudentRoomAvailabilityCount");
            data.Action = ActionType.Scalar;
            data.ScalarField = "RoomsAvailable";

            object obj = data.ExecuteScalar();

            if (obj != null)
            {
                try
                {
                    result = Convert.ToInt32(obj) > 0;
                }
                catch
                { }
            }

            lastAvailabilityCheck = DateTime.Now;

            return result;

        }

        public KxData AddGenericParameters(string xpath, string sproc)
        {
            KxData result = new KxData(xpath, sproc, TargetType.StoredProc);

            string appId = KxDbConfiguration.GetConfiguration("ApplicationId").ToUpper();

            if (appId == "LIBERTY" || appId == "CITYLIVING2" || appId == "OPAL")
            {
                result.AddParameter("allocationModelId", allocationModelId, KxValueType.Int);
            }
            result.AddParameter("AcademicYearId", AcademicYearId, KxValueType.Int);
            result.AddParameter("BedroomTypeId", BedroomTypeId, KxValueType.Int);
            result.AddParameter("SubBlockId", SubBlockId, KxValueType.Int);
            result.AddParameter("BlockId", BlockId, KxValueType.Int);
            result.AddParameter("AreaId", AreaId, KxValueType.Int);
            result.AddParameter("SiteId", SiteId, KxValueType.Int);
            result.AddParameter("Smoking", Smoking, KxValueType.Int); 
            result.AddParameter("Disabled", Disabled, KxValueType.Int); 
            result.AddParameter("Gender", Gender, KxValueType.Int);
            result.AddParameter("SingleSex", SingleSex, KxValueType.Int); 
            result.AddParameter("FloorNumber", FloorNumber, KxValueType.Int);
            result.AddParameter("CustomField1", CustomField1, KxValueType.Int); 
            result.AddParameter("CustomField2", CustomField2, KxValueType.Int); 
            result.AddParameter("CustomValue1", CustomValue1, KxValueType.String);
            result.AddParameter("CustomValue2", CustomValue2, KxValueType.String);
            result.AddParameter("CustomValue3", CustomValue3, KxValueType.String);
            result.AddParameter("CustomValue4", CustomValue4, KxValueType.String);
            result.AddParameter("CustomValue5", CustomValue5, KxValueType.String);
            result.AddParameter("CustomValue6", CustomValue6, KxValueType.String);
            result.AddParameter("CustomValue7", CustomValue7, KxValueType.String);
            result.AddParameter("CustomValue8", CustomValue8, KxValueType.String);

            return result;
        }

        private string GetDistinctFieldValues()
        {
            string[] fields = DistinctFields.Split(new char[]{' '}, StringSplitOptions.RemoveEmptyEntries);
            
            string result = "";
            
            foreach (string str in fields)
            {
                result += KxReflection.GetPropertyString(this, str);
            }

            return result;
        }

        private int GetContractMonths()
        {
            int result = 0;

            if (ContractEndDate != null && ContractStartDate != null)
            {
                TimeSpan ts = ContractEndDate.Value.Subtract(ContractStartDate.Value);

                result = Convert.ToInt32(Math.Floor(ts.TotalDays / 30));
            }

            return result;
        }
    }
}

