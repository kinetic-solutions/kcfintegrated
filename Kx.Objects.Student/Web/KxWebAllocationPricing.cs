using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;
using Kx.Objects.Common;

namespace Kx.Objects.Student.Web
{
    [Serializable]
    public class KxWebAllocationPricing : KxBaseClass
    {
        #region Attributes & Properties
        protected bool loaded = false;

        protected int      academicYearId;
        protected int      areaId;
        protected string   letLength;
        protected int      bedroomTypeId;
        protected int      annualPricingModelId;
        protected int      annualAllocationModelId;
        protected int      instalmentPricingModelId;
        protected int      instalmentAllocationModelId;

        protected KxAcademicYear academicYear = null;
        protected KxArea area = null;
        protected KxBedroomType bedroomType = null;
        protected KxPricingModel annualPricing = null;
        protected KxAllocationModel annualAllocation = null;
        protected KxPricingModel instalmentPricing = null;
        protected KxAllocationModel instalmentAllocation = null;
        
        public int      AcademicYearId
        {
            get
            { return academicYearId;}
            set
            { 
                AddToAudit("AcademicYearId", academicYearId, value);
                academicYearId = value;
                academicYear = null;
            }
        }

        public int      AreaId
        {
            get
            { return areaId;}
            set
            { 
                AddToAudit("AreaId", areaId, value);
                areaId = value;
                area = null;
            }
        }

        public string   LetLength
        {
            get
            { return letLength;}
            set
            { 
                AddToAudit("LetLength", letLength, value);
                letLength = value;
            }
        }

        public int      BedroomTypeId
        {
            get
            { return bedroomTypeId;}
            set
            { 
                AddToAudit("BedroomTypeId", bedroomTypeId, value);
                bedroomTypeId = value;
                bedroomType = null;
            }
        }

        public int      AnnualPricingModelId
        {
            get
            { return annualPricingModelId;}
            set
            { 
                AddToAudit("AnnualPricingModelId", annualPricingModelId, value);
                annualPricingModelId = value;
                annualPricing = null;
            }
        }

        public int      AnnualAllocationModelId
        {
            get
            { return annualAllocationModelId;}
            set
            { 
                AddToAudit("AnnualAllocationModelId", annualAllocationModelId, value);
                annualAllocationModelId = value;
                annualAllocation = null;
            }
        }

        public int      InstalmentPricingModelId
        {
            get
            { return instalmentPricingModelId;}
            set
            { 
                AddToAudit("InstalmentPricingModelId", instalmentPricingModelId, value);
                instalmentPricingModelId = value;
                instalmentPricing = null;
            }
        }

        public int      InstalmentAllocationModelId
        {
            get
            { return instalmentAllocationModelId;}
            set
            { 
                AddToAudit("InstalmentAllocationModelId", instalmentAllocationModelId, value);
                instalmentAllocationModelId = value;
                instalmentAllocation = null;
            }
        }

   
        public string UniqueId 
        {
            get
            {
                return String.Format("{0}|{1}|{2}|{3}", AcademicYearId, AreaId, LetLength, BedroomTypeId);
            }
        }


        public KxAcademicYear AcademicYear
        {
            get
            {
                if (academicYear == null && academicYearId > 0)
                {
                    academicYear = new KxAcademicYear(this, academicYearId);
                }
                return academicYear;
            }
        }

        public string AcademicYearName
        {
            get
            {
                if (AcademicYear != null)
                {
                    return AcademicYear.Title;
                }
                else
                {
                    return "";
                }
            }
        }

        public KxArea Area
        {
            get
            {
                if (area == null && areaId > 0)
                {
                    area = new KxArea(this, areaId);
                }
                return area;
            }
        }

        public string AreaName
        {
            get
            {
                if (Area != null)
                {
                    return Area.Description;
                }
                else
                {
                    return "";
                }
            }
        }

        public KxBedroomType BedroomType
        {
            get
            {
                if (bedroomType == null && bedroomTypeId > 0)
                {
                    bedroomType = new KxBedroomType(this, bedroomTypeId);
                }
                return bedroomType;
            }
        }

        public string BedroomTypeName
        {
            get
            {
                if (BedroomType != null)
                {
                    return BedroomType.Description;
                }
                else
                {
                    return "";
                }
            }
        }

        public KxPricingModel AnnualPricing
        {
            get
            {
                if (annualPricing == null && annualPricingModelId > 0)
                {
                    annualPricing = new KxPricingModel(this, annualPricingModelId);
                }
                return annualPricing;
            }
        }

        public string AnnualPricingModelName
        {
            get
            {
                if (AnnualPricing != null)
                {
                    return AnnualPricing.Description;
                }
                else
                {
                    return "";
                }
            }
        }

        public string InstalmentPricingModelName
        {
            get
            {
                if (InstalmentPricing != null)
                {
                    return InstalmentPricing.Description;
                }
                else
                {
                    return "";
                }
            }
        }

        public string AnnualAllocationModelName
        {
            get
            {
                if (AnnualAllocation != null)
                {
                    return AnnualAllocation.Description;
                }
                else
                {
                    return "";
                }
            }
        }

        public string InstalmentAllocationModelName
        {
            get
            {
                if (InstalmentAllocation != null)
                {
                    return InstalmentAllocation.Description;
                }
                else
                {
                    return "";
                }
            }
        }


        public string AnnualPricingName
        {
            get
            {
                if (AnnualPricing != null)
                {
                    return AnnualPricing.Description;
                }
                else
                {
                    return "";
                }
            }
        }

        public KxAllocationModel AnnualAllocation
        {
            get
            {
                if (annualAllocation == null && annualAllocationModelId > 0)
                {
                    annualAllocation = new KxAllocationModel(this, annualAllocationModelId);
                }
                return annualAllocation;
            }
        }

        public string AnnualAllocationName
        {
            get
            {
                if (AnnualAllocation != null)
                {
                    return AnnualAllocation.Description;
                }
                else
                {
                    return "";
                }
            }
        }

        public KxPricingModel InstalmentPricing
        {
            get
            {
                if (instalmentPricing == null && instalmentPricingModelId > 0)
                {
                    instalmentPricing = new KxPricingModel(this, instalmentPricingModelId);
                }
                return instalmentPricing;
            }
        }

        public string InstalmentPricingName
        {
            get
            {
                if (InstalmentPricing != null)
                {
                    return InstalmentPricing.Description;
                }
                else
                {
                    return "";
                }
            }
        }

        public KxAllocationModel InstalmentAllocation
        {
            get
            {
                if (instalmentAllocation == null && instalmentAllocationModelId > 0)
                {
                    instalmentAllocation = new KxAllocationModel(this, instalmentAllocationModelId);
                }
                return instalmentAllocation;
            }
        }

        public string InstalmentAllocationName
        {
            get
            {
                if (InstalmentAllocation != null)
                {
                    return InstalmentAllocation.Description;
                }
                else
                {
                    return "";
                }
            }
        }


        #endregion 
        public KxWebAllocationPricing(KxBaseClass parent) : base(parent)
        {
        }

        public KxWebAllocationPricing(KxBaseClass parent, XmlNode node)
            : base(parent)
        {
             LoadFromXml("", node);
        }
        
        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("weballocationpricing", "WebAllocationPricing", TargetType.Table);

            result.AddKeyField("academicyearid",academicYearId,KxValueType.Int);
            result.AddKeyField("areaId", areaId, KxValueType.Int);
            result.AddKeyField("letLength", letLength, KxValueType.String);
            result.AddKeyField("bedroomTypeId", bedroomTypeId, KxValueType.Int);
            
            if ((actiontype == ActionType.Update) && (!loaded))
            {
                result.Action = ActionType.Insert;
            }
            else
            {
                result.Action = actiontype;
            }

            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {
                   result.AddParameter("annualPricingModelId", annualPricingModelId, KxValueType.Int, true);
                   result.AddParameter("annualAllocationModelId", annualAllocationModelId, KxValueType.Int, true);
                   result.AddParameter("instalmentPricingModelId", instalmentPricingModelId, KxValueType.Int, true);
                   result.AddParameter("instalmentAllocationModelId", instalmentAllocationModelId, KxValueType.Int, true);
            }
            return result;
        }


        public override  void LoadFromXml(string xml,XmlNode node)
        {
            academicYearId  = KxXmlUtils.GetNodeValue(node,"academicyearid",0);
            areaId  = KxXmlUtils.GetNodeValue(node,"areaid",0);
            letLength  = KxXmlUtils.GetNodeValue(node,"letlength","");
            bedroomTypeId  = KxXmlUtils.GetNodeValue(node,"bedroomtypeid",0);
            annualPricingModelId  = KxXmlUtils.GetNodeValue(node,"annualpricingmodelid",0);
            annualAllocationModelId  = KxXmlUtils.GetNodeValue(node,"annualallocationmodelid",0);
            instalmentPricingModelId  = KxXmlUtils.GetNodeValue(node,"instalmentpricingmodelid",0);
            instalmentAllocationModelId  = KxXmlUtils.GetNodeValue(node,"instalmentallocationmodelid",0);

            loaded = true;
        }
    }
}

