using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Web.UI.WebControls;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;
using Kx.Objects.Common;
using Kx.Objects.Student.Web;

namespace Kx.Objects.Student
{
    [Serializable]
    public class KxSiteAvailabilityLookup : KxBaseClass
    {
        #region Attributes & Properties
        protected bool loaded = false;
        protected KxArea area = null;

        protected int siteId = 0;
        protected int areaId = 0;
        protected int blockId = 0;
        protected string cityName = "";
        protected string propertyName = "";
        protected string propertyAddress;
        protected string propertyTelephone;
        protected string propertyEmail;
        protected string propertyLandlord;
        protected string additionalInformation1;
        protected string additionalInformation2;
        protected string additionalInformation3;
        protected bool roomsAvailable;
        protected string comment;
        protected string postalApplicationPDF;
        protected DateTime? offer321;
        protected int paymentScheduleTypeId;
        protected decimal offer321Value;

        protected int index;

        public bool Loaded
        {
            get
            {
                return loaded;
            }
            set
            {
                loaded = value;
            }
        }

        public int SiteId
        {
            get
            { return siteId; }
            set
            {
                AddToAudit("SiteId", siteId, value);
                siteId = value;
            }
        }

        public int AreaId
        {
            get
            { return areaId; }
            set
            {
                AddToAudit("AreaId", areaId, value);
                areaId = value;
            }
        }

        public KxArea Area
        {
            get
            {
                if (area == null)
                {
                    area = KxStudentObjectManager.Areas.GetAreaById(areaId);
                }
                return area;
            }
        }

        public string AreaName
        {
            get
            {
                try
                {
                    return Area.Description;
                }
                catch
                {
                    return "";
                }
            }
        }

        public int BlockId
        {
            get
            { return blockId; }
            set
            {
                AddToAudit("BlockId", blockId, value);
                blockId = value;
            }
        }

        public string CityName
        {
            get
            { return cityName; }
            set
            {
                AddToAudit("CityName", cityName, value);
                cityName = value;
            }
        }

        public string PropertyName
        {
            get
            { return propertyName; }
            set
            {
                AddToAudit("PropertyName", propertyName, value);
                propertyName = value;
            }
        }

        public string PropertyAddress
        {
            get
            { return propertyAddress; }
            set
            {
                AddToAudit("PropertyAddress", propertyAddress, value);
                propertyAddress = value;
            }
        }

        public string PropertyAddressFull
        {
            get
            { return propertyAddress + " " + PropertyTelephonePrefixed; }
           
        }


        public string PropertyAddressUpperCase
        {
            get
            { return propertyAddress.ToUpper(); }
        }

        public string PropertyTelephone
        {
            get
            { return propertyTelephone; }
            set
            {
                AddToAudit("PropertyTelephone", propertyTelephone, value);
                propertyTelephone = value;
            }
        }

        public string PropertyEmail
        {
            get
            { return propertyEmail; }
            set
            {
                AddToAudit("PropertyEmail", propertyEmail, value);
                propertyEmail = value;
            }
        }

        public string PropertyLandlord
        {
            get
            { return propertyLandlord; }
            set
            {
                AddToAudit("PropertyLandlord", propertyLandlord, value);
                propertyLandlord = value;
            }
        }

        public string AdditionalInformation1
        {
            get
            { return additionalInformation1; }
            set
            {
                AddToAudit("AdditionalInformation1", additionalInformation1, value);
                additionalInformation1 = value;
            }
        }

        public string AdditionalInformation2
        {
            get
            { return additionalInformation2; }
            set
            {
                AddToAudit("AdditionalInformation2", additionalInformation2, value);
                additionalInformation2 = value;
            }
        }

        public string AdditionalInformation3
        {
            get
            { return additionalInformation3; }
            set
            {
                AddToAudit("AdditionalInformation3", additionalInformation3, value);
                additionalInformation3 = value;
            }
        }

        public string PropertyTelephonePrefixed
        {
            get
            { return "Tel: "+PropertyTelephone; }
        }

        public bool RoomsAvailable
        {
            get
            { return roomsAvailable; }
            set
            {
                AddToAudit("RoomsAvailable", roomsAvailable, value);
                roomsAvailable = value;
            }
        }

        public string Comment
        {
            get
            { return comment; }
            set
            {
                AddToAudit("Comment", comment, value);
                comment = value;
            }
        }

        public string PostalApplicationPDF
        {
            get
            { return postalApplicationPDF; }
            set
            {
                AddToAudit("PostalApplicationPDF", postalApplicationPDF, value);
                postalApplicationPDF = value;
            }
        }

        public DateTime? Offer321
        {
            get
            { return offer321; }
            set
            {
                AddToAudit("Offer321", offer321, value);
                offer321 = value;
            }
        }

        public int PaymentScheduleTypeId
        {
            get
            { return paymentScheduleTypeId; }
            set
            {
                AddToAudit("PaymentScheduleTypeId", paymentScheduleTypeId, value);
                paymentScheduleTypeId = value;
            }
        }

        public decimal Offer321Value
        {
            get
            { return offer321Value; }
            set
            {
                AddToAudit("Offer321Value", offer321Value, value);
                offer321Value = value;
            }
        }

        private string _offer321ValueString;
        public string Offer321ValueString
        {
            get { return _offer321ValueString; }
            set
            {
                AddToAudit("Offer321ValueString", _offer321ValueString, value);
                _offer321ValueString = value;
            }
        }
        
        
        public KxSiteAvailabilityLookups Lookup
        {
            get
            {
                return (KxSiteAvailabilityLookups)Parent;
            }
        }

        public string UniqueId
        {
            get
            {
                return String.Format("{0}A{1}B{2}", SiteId, AreaId, BlockId);
            }
        }
            
        public bool HasComment
        {
            get
            {
                return !String.IsNullOrEmpty(comment);
            }
        }

        public bool SupportsPostalApplication
        {
            get
            {
                return !String.IsNullOrEmpty(postalApplicationPDF);
            }
        }

        public int Index
        {
            get
            {
                return index;
            }
            set
            {
                index = value;
            }
        }

        public bool IsSecondary
        {
            get
            {
                return GetIsSecondary();
            }
        }

        public bool IsPrimaryAndIsMultiSite
        {
            get
            {
                return !IsSecondary && GetIsMultiSite();
            }
        }

        public bool IsSingleSite
        {
            get
            {
                return !GetIsMultiSite();
            }
        }

        public bool IsOffer321Valid
        {
            get
            {
                return GetIsOffer321Valid();
            }
        }

        #endregion 
        public KxSiteAvailabilityLookup(KxBaseClass parent)
            : base(parent)
        {
          
        }


        public KxSiteAvailabilityLookup(KxBaseClass parent, XmlNode node)
            : base(parent)
        {
             LoadFromXml("", node);
        }

        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("siteavailabilitylookup", "SiteAvailabilityLookup", TargetType.Table);

            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {
                if (!loaded)
                {
                    result.Action = ActionType.Insert;
                }
                else
                {
                    result.Action = ActionType.Update;
                }

                result.AddKeyField("siteid", siteId, KxValueType.Int);
                result.AddKeyField("areaId", areaId, KxValueType.Int);
                result.AddKeyField("blockId", blockId, KxValueType.Int);

                result.AddParameter("cityName", cityName, KxValueType.String, 50, true);
                result.AddParameter("propertyName", propertyName, KxValueType.String, 50, true);
                result.AddParameter("propertyAddress", propertyAddress, KxValueType.String, 255, true);
                result.AddParameter("propertyTelephone", propertyTelephone, KxValueType.String, 50, true);
                result.AddParameter("propertyEmail", propertyEmail, KxValueType.String, 255, true);

                result.AddParameter("propertyLandlord", propertyLandlord, KxValueType.String, 255, true);
                result.AddParameter("additionalInformation1", additionalInformation1, KxValueType.String, 255, true);
                result.AddParameter("additionalInformation2", additionalInformation2, KxValueType.String, 255, true);
                result.AddParameter("additionalInformation3", additionalInformation3, KxValueType.String, 255, true);

                result.AddParameter("roomsAvailable", roomsAvailable, KxValueType.Boolean, true);
               // result.AddParameter("comment", comment, KxValueType.String, 255, true);
               // result.AddParameter("postalApplicationPDF", postalApplicationPDF, KxValueType.String, 255, true);
                result.AddParameter("offer321", offer321, KxValueType.DateTime, true);
                result.AddParameter("paymentScheduleTypeId", paymentScheduleTypeId, KxValueType.Int, true);
                result.AddParameter("offer321Value", offer321Value, KxValueType.Decimal, true);
            }
            return result;
        }


        public override  void LoadFromXml(string xml,XmlNode node)
        {
            siteId = KxXmlUtils.GetNodeValue(node, "siteid", 0);
            areaId = KxXmlUtils.GetNodeValue(node, "areaid", 0);
            blockId = KxXmlUtils.GetNodeValue(node, "blockid", 0);
            cityName = KxXmlUtils.GetNodeValue(node, "cityname", "");
            propertyName = KxXmlUtils.GetNodeValue(node, "propertyname", "");
            propertyAddress = KxXmlUtils.GetNodeValue(node, "propertyaddress", "");
            propertyTelephone = KxXmlUtils.GetNodeValue(node, "propertytelephone", "");
            propertyEmail = KxXmlUtils.GetNodeValue(node, "propertyemail", "");
            propertyLandlord = KxXmlUtils.GetNodeValue(node, "propertylandlord", "");
            additionalInformation1 = KxXmlUtils.GetNodeValue(node, "additionalinformation1", "");
            additionalInformation2 = KxXmlUtils.GetNodeValue(node, "additionalinformation2", "");
            additionalInformation3 = KxXmlUtils.GetNodeValue(node, "additionalinformation3", "");
            roomsAvailable = KxXmlUtils.GetNodeValue(node, "roomsavailable", false);
           // comment = KxXmlUtils.GetNodeValue(node, "comment", "");
           // postalApplicationPDF = KxXmlUtils.GetNodeValue(node, "postalapplicationpdf", "");
            offer321 = KxXmlUtils.GetNodeValueDT(node, "offer321", null);
            paymentScheduleTypeId = KxXmlUtils.GetNodeValue(node, "paymentscheduletypeid", 0);
            offer321Value = KxXmlUtils.GetNodeValue(node, "offer321value", 0.0M);
            Offer321ValueString = KxXmlUtils.GetNodeValue(node, "offer321valuestring", offer321Value.ToString("#,##0.00"));

            loaded = true;
        }

        protected bool GetIsSecondary()
        {
            bool result = false;

            try
            {
                KxSiteAvailabilityLookup prev = Lookup.GetSiteAvailabilityLookupByIndex(Index - 1);

                if (prev != null)
                {
                    if (prev.CityName == CityName)
                    {
                        result = true;
                    }
                }
            }
            catch { }

            return result;
        }
        protected bool GetIsMultiSite()
        {
            bool result = false;

            foreach (KxSiteAvailabilityLookup sal in Lookup.Items)
            {

                if (sal.CityName == CityName && sal != this)
                {
                    result = true;
                    break;
                }
            }
            return result;
        }

        protected bool GetIsOffer321Valid()
        {
            bool result = false;

            if (Offer321 != null)
            {
                return DateTime.Now < Offer321.Value;
            }
            return result;
        }
    }
}

