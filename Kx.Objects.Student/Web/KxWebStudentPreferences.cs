using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Exceptions;
using Kx.Core.Reflection;
using Kx.Core.Utility;
using System.Configuration;
using Kx.Core.ServiceLocator;
using System.Linq;

namespace Kx.Objects.Student.Web
{
    public class KxWebStudentPreferences : KxBaseClass
    {
        #region attributes and properties

        private int resultsBase = 0;
        private Guid applicationGuid;
        private string sortType = "";

        static protected string customField1Name;

        static protected string customField2Name;

        static protected string customValue1Name;
        static protected string customValue1Options;

        static protected string customValue2Name;
        static protected string customValue2Options;

        static protected string customValue3Name;
        static protected string customValue3Options;

        static protected string customValue4Name;
        static protected string customValue4Options;

        static protected string customValue5Name;
        static protected string customValue5Options;

        static protected string customValue6Name;
        static protected string customValue6Options;

        static protected string customValue7Name;
        static protected string customValue7Options;

        static protected string customValue8Name;
        static protected string customValue8Options;

        private List<KxWebStudentPreference> items = new List<KxWebStudentPreference>();

        public int[] PreferenceNumbers
        {
            get
            { return GetPreferenceNumbers(); }
        }

        public Guid ApplicationGuid
        {
            get
            { return applicationGuid; }
            set
            { applicationGuid = value; }
        }

        public List<KxWebStudentPreference> Items
        {
            get
            { return items; }
        }

        public string SortType
        {
            get
            { return sortType; }
            set
            {
                if (sortType != value)
                {
                    sortType = value;
                    Sort();
                }
            }
        }

        public KxWebStudentPreference this[int index]
        {
            get
            { return items[index]; }
            set
            { items[index] = value; }
        }

        public int Count
        {
            get
            { return GetCount(); }
        }

        public int SiteId
        {
            get
            { return GetValueInt("SiteId"); }
            set
            { SetValue("SiteId", value); }
        }
        public int AreaId
        {
            get
            { return GetValueInt("AreaId"); }
            set
            { SetValue("AreaId", value); }
        }
        public int BlockId
        {
            get
            { return GetValueInt("BlockId"); }
            set
            { SetValue("BlockId", value); }
        }

        public int BedroomTypeId
        {
            get
            { return GetValueInt("BedroomTypeId"); }
            set
            { SetValue("BedroomTypeId", value); }
        }
        public int Floor
        {
            get
            { return GetValueInt("Floor"); }
            set
            { SetValue("Floor", value); }
        }
        public int SingleSexArea
        {
            get
            { return GetValueInt("SingleSexArea"); }
            set
            { SetValue("SingleSexArea", value); }
        }

        public int Smoking
        {
            get
            { return GetValueInt("Smoking"); }
            set
            { SetValue("Smoking", value); }
        }

        public int CustomField1
        {
            get
            { return GetValueInt("CustomField1"); }
            set
            { SetValue("CustomField1", value); }
        }

        public int CustomField2
        {
            get
            { return GetValueInt("CustomField2"); }
            set
            { SetValue("CustomField2", value); }
        }

        public string CustomValue1
        {
            get
            { return GetValue("CustomValue1"); }
            set
            { SetValue("CustomValue1", value); }
        }
        public string CustomValue2
        {
            get
            { return GetValue("CustomValue2"); }
            set
            { SetValue("CustomValue2", value); }
        }
        public string CustomValue3
        {
            get
            { return GetValue("CustomValue3"); }
            set
            { SetValue("CustomValue3", value); }
        }
        public string CustomValue4
        {
            get
            { return GetValue("CustomValue4"); }
            set
            { SetValue("CustomValue4", value); }
        }
        public string CustomValue5
        {
            get
            { return GetValue("CustomValue5"); }
            set
            { SetValue("CustomValue5", value); }
        }
        public string CustomValue6
        {
            get
            { return GetValue("CustomValue6"); }
            set
            { SetValue("CustomValue6", value); }
        }
        public string CustomValue7
        {
            get
            { return GetValue("CustomValue7"); }
            set
            { SetValue("CustomValue7", value); }
        }
        public string CustomValue8
        {
            get
            { return GetValue("CustomValue8"); }
            set
            { SetValue("CustomValue8", value); }
        }

        public string CustomField1Name
        {
            get
            { return customField1Name; }
        }

        public string CustomField2Name
        {
            get
            { return customField2Name; }
        }

        public string CustomValue1Name
        {
            get
            { return customValue1Name; }
        }

        public string CustomValue1Options
        {
            get
            { return customValue1Options; }
        }

        public string CustomValue2Name
        {
            get
            { return customValue2Name; }
        }

        public string CustomValue2Options
        {
            get
            { return customValue2Options; }
        }

        public string CustomValue3Name
        {
            get
            { return customValue3Name; }
        }

        public string CustomValue3Options
        {
            get
            { return customValue3Options; }
        }

        public string CustomValue4Name
        {
            get
            { return customValue4Name; }
        }

        public string CustomValue4Options
        {
            get
            { return customValue4Options; }
        }

        public string CustomValue5Name
        {
            get
            { return customValue5Name; }
        }

        public string CustomValue5Options
        {
            get
            { return customValue5Options; }
        }

        public string CustomValue6Name
        {
            get
            { return customValue6Name; }
        }

        public string CustomValue6Options
        {
            get
            { return customValue6Options; }
        }

        public string CustomValue7Name
        {
            get
            { return customValue7Name; }
        }

        public string CustomValue7Options
        {
            get
            { return customValue7Options; }
        }

        public string CustomValue8Name
        {
            get
            { return customValue8Name; }
        }

        public string CustomValue8Options
        {
            get
            { return customValue8Options; }
        }



        #endregion

        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = null;

            if (actiontype == ActionType.Select)
            {
                result = new KxData("webstudentpreferences", "WebStudentApplicationPreferences", TargetType.StoredProc, ActionType.Select);

                result.KeyFields.Add(new KxDataValue("ApplicationGuid", applicationGuid, KxValueType.Guid));

                LoadCustomSettings();
            }
            return result;
        }

        public KxWebStudentPreferences(KxBaseClass parent, Guid applicationGuid)
            : base(parent)
        {
            this.applicationGuid = applicationGuid;
            Load();
        }

        public KxWebStudentPreferences(KxBaseClass parent)
            : base(parent)
        {
        }

        public override bool LoadFromXml(string xml, string xpath)
        {
            try
            {
                Items.Clear();
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xml);

                XmlNodeList nodes = doc.SelectNodes(@"//results/" + xpath + "/datarow");

                foreach (XmlNode node in nodes)
                {
                    if (node != null)
                    {
                        int preferenceid = KxXmlUtils.GetNodeValue(node, "preferenceid", 0);
                        if (preferenceid != 0)
                        {
                            KxWebStudentPreference prefer = new KxWebStudentPreference(this, preferenceid);
                            Items.Add(prefer);
                        }
                    }
                }

                return true;
            }
            catch (Exception ex)
            {

                KxLogger.LogActivity(this, "LoadFromXml", ex.Message, xpath);
                LastError = ex.Message;
                return false;
            }
        }

        public void ClearUI()
        {
            foreach (KxWebStudentPreference pref in Items)
            {
                pref.UIObject = null;
            }
        }

        public void ResetAll()
        {
            foreach (KxWebStudentPreference pref in Items)
            {
                pref.SiteId = 0;
                pref.BedroomTypeId = 0;
                pref.Save();
            }
        }

        public bool DuplicateExists(KxWebStudentPreference pref)
        {
            bool result = false;

            foreach (KxWebStudentPreference exist in Items)
            {
                if (exist.DuplicateExists(pref))
                {
                    result = true;
                    break;
                }
            }
            return result;
        }

        public bool LoadFromXmlPreferenceManager(string xml, string xpath)
        {
            items.Clear();
            string templist = "";

            try
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xml);

                XmlNodeList nodes = doc.SelectNodes(@"//" + xpath + "/datarow");

                foreach (XmlNode node in nodes)
                {
                    if (node != null)
                    {
                        KxWebStudentPreference prefer = new KxWebStudentPreference(this, node);

                        int temp = prefer.PreferenceId;
                        prefer.Temporary = true;
                        prefer.PreferenceId = temp;
                        
                        Items.Add(prefer);
                        templist += prefer.PreferenceId.ToString() + ",";
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "LoadFromXmlPriority", ex.Message, xpath);
                LastError = ex.Message;
                return false;
            }
        }

        public bool LoadFromXmlSearch(KxWebStudentPreferences comparison, string xml, string xpath, int resultLimit)
        {
            items.Clear();

            resultsBase = 0;

            string templist = "";

            try
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xml);

                XmlNodeList nodes = doc.SelectNodes(@"//" + xpath + "/datarow");

                foreach (XmlNode node in nodes)
                {
                    if (node != null)
                    {
                        KxWebStudentPreference prefer = new KxWebStudentPreference(this, node);

                        int temp = prefer.PreferenceId;
                        prefer.Temporary = true;
                        prefer.PreferenceId = temp;

                        if (comparison != null && !comparison.DuplicateExists(prefer))
                        {
                            if (PreferenceByIndex(prefer.PreferenceId) == null)
                            {
                                resultsBase++;
                                prefer.PreferenceNumber = resultsBase;

                                Items.Add(prefer);
                                templist += prefer.PreferenceId.ToString() + ",";
                            }
                        }
                    }

                    if (items.Count >= resultLimit)
                    {
                        break;
                    }
                }
                //Sort();

                //KxLogger.LogActivity(this,"loading items",templist,"");

                return true;
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "LoadFromXmlSearch", ex.Message, xpath);
                LastError = ex.Message;
                return false;
            }
        }

        public KxWebStudentPreference AddPreference()
        {
            KxWebStudentPreference pref = new KxWebStudentPreference(this);
            pref.ApplicationGuid = ApplicationGuid;
            pref.PreferenceNumber = Items.Count + 1;
            Items.Add(pref);
            return pref;
        }

        public void DeleteAll()
        {
            foreach (KxWebStudentPreference pref in Items)
            {

                DeletePreference(pref);
                DeleteAll();
                break;
            }
            Items.Clear();
            Children.Clear();
        }

        public void DeletePreference(KxWebStudentPreference pref)
        {
            pref.Delete();
            Items.Remove(pref);
            Children.Remove(pref);
            pref = null;
        }

        public KxWebStudentPreference PreferenceByIndex(int index)
        {
            KxWebStudentPreference result = null;

            foreach (KxWebStudentPreference pref in Items)
            {
                if (pref.PreferenceId == index)
                {
                    result = pref;
                    break;
                }
            }
            return result;
        }

        public KxWebStudentPreference PreferenceByNumber(int number)
        {
            KxWebStudentPreference result = null;

            foreach (KxWebStudentPreference pref in Items)
            {
                if (pref.PreferenceNumber == number)
                {
                    result = pref;
                    break;
                }
            }
            return result;
        }

        public void LoadCustomSettings()
        {
            KxData data = new KxData("preferencecustom", "WebStudentApplicationCustomPreferences", TargetType.StoredProc, ActionType.Select);

            data.Action = ActionType.Select;

            if (data.Execute())
            {
                LoadCustomDataFromXml(data.XmlResults);
            }
        }

        static protected void LoadCustomDataFromXml(String Xml)
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(Xml);

                XmlNode node = doc.SelectSingleNode(@"//preferencecustom/datarow");

                if (node != null)
                {
                    customField1Name = KxXmlUtils.GetNodeValue(node, "customfield1", "");
                    customField2Name = KxXmlUtils.GetNodeValue(node, "customfield2", "");

                    customValue1Name = KxXmlUtils.GetNodeValue(node, "customvalue1", "");
                    customValue2Name = KxXmlUtils.GetNodeValue(node, "customvalue2", "");
                    customValue3Name = KxXmlUtils.GetNodeValue(node, "customvalue3", "");
                    customValue4Name = KxXmlUtils.GetNodeValue(node, "customvalue4", "");
                    customValue5Name = KxXmlUtils.GetNodeValue(node, "customvalue5", "");
                    customValue6Name = KxXmlUtils.GetNodeValue(node, "customvalue6", "");
                    customValue7Name = KxXmlUtils.GetNodeValue(node, "customvalue7", "");
                    customValue8Name = KxXmlUtils.GetNodeValue(node, "customvalue8", "");

                    customValue1Options = KxXmlUtils.GetNodeValue(node, "customvalue1options", "");
                    customValue2Options = KxXmlUtils.GetNodeValue(node, "customvalue2options", "");
                    customValue3Options = KxXmlUtils.GetNodeValue(node, "customvalue3options", "");
                    customValue4Options = KxXmlUtils.GetNodeValue(node, "customvalue4options", "");
                    customValue5Options = KxXmlUtils.GetNodeValue(node, "customvalue5options", "");
                    customValue6Options = KxXmlUtils.GetNodeValue(node, "customvalue6options", "");
                    customValue7Options = KxXmlUtils.GetNodeValue(node, "customvalue7options", "");
                    customValue8Options = KxXmlUtils.GetNodeValue(node, "customvalue8options", "");
                }
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity("LoadCustomDataFromXml", "LoadCustomDataFromXml", ex.Message, "");
            }
        }

        private int GetCount()
        {
            int result = 0;

            foreach (KxWebStudentPreference pref in Items)
            {
                if (pref.SelectionType != SelectedType.Hidden)
                {
                    result++;
                }
            }
            return result;
        }

        public override void NotifyDelete(KxBaseClass child)
        {
            KxWebStudentPreference member = (KxWebStudentPreference)child;
            if (Items.Contains(member))
            {
                Items.Remove(member);
            }
        }

        public void ResequencePreferences()
        {
            int idx = 1;

            foreach (KxWebStudentPreference pref in Items)
            {
                pref.PreferenceNumber = idx;
                idx++;
            }
        }

        public bool RemoveSelected()
        {
            bool result = false;
            foreach (KxWebStudentPreference pref in Items)
            {
                if (pref.SelectionType == SelectedType.Selected)
                {
                    items.Remove(pref);
                    RemoveSelected();
                    // set changed flag
                    result = true;
                    break;
                }
            }
            ResequencePreferences();
            return result;
        }

        public void Sort()
        {
            //items.Sort();
            // ResequencePreferences();
        }

        private string GetValue(string fieldname)
        {
            string result = "";

            if (items.Count > 0)
            {
                result = KxReflection.GetPropertyString(Items[0], fieldname);
            }

            return result;
        }

        private int GetValueInt(string fieldname)
        {
            try
            {
                return Convert.ToInt32(GetValue(fieldname));
            }
            catch
            {
                return 0;
            }
        }

        public void SetValue(string fieldname, object value)
        {
            foreach (KxWebStudentPreference pref in items)
            {
                KxReflection.SetProperty(pref, fieldname, value);
            }
        }

        private int[] GetPreferenceNumbers()
        {
            int[] result = new int[items.Count];

            for (int i = 0; i < Items.Count; i++)
            {
                result[i] = items[i].PreferenceNumber;
            }

            return result;
        }

        public bool IsUnique(KxWebStudentPreference source)
        {
            bool result = true;

            foreach (KxWebStudentPreference pref in Items)
            {
                if (source.DuplicateExists(pref))
                {
                    result = false;
                    break;
                }
            }
            return result;

        }

        public bool CustomField2CountLimit(int limit)
        {
            bool result = false;
            int count = 0;

            foreach (KxWebStudentPreference pref in Items)
            {
                if (pref.CustomField2 == 1)
                {
                    count++;
                }
            }

            result = count <= limit;

            return result;
        }

        public bool EnsuiteCountLimit(int limit)
        {
            bool result = false;
            int count = 0;
            string value1 = "";

            Items.Sort(delegate(KxWebStudentPreference Pref1, KxWebStudentPreference Pref2)
            {
                return Comparer<String>.Default.Compare(Pref1.CustomValue1, Pref2.CustomValue1);
            });


            if (limit == 99 &&
                !string.IsNullOrEmpty(ConfigurationManager.AppSettings["ICustomEnsuiteCountLimitValidator"]))
            {
                try
                {
                    ICustomEnsuiteCountLimitValidator validator =
                        ServiceLocator.ResolveService<ICustomEnsuiteCountLimitValidator>();
                    if (validator != null)
                    {
                        return validator.CustomEnsuiteCountLimit(this);
                    }
                }
                catch (Exception ex)
                {
                    KxLogger.LogActivity(this, "ICustomEnsuiteCountLimitValidator", ex.Message, "Invoked");
                }
            }

            foreach (KxWebStudentPreference pref in Items)
            {

                if (pref.Ensuite)
                {

                    if (pref.CustomValue1.Contains("weeks"))
                    {
                        if (value1 == "")
                        {
                            value1 = pref.CustomValue1;
                        }

                        if (value1 != pref.CustomValue1)
                        {
                            value1 = pref.CustomValue1;
                        }
                        else
                        {

                            count++;
                        }
                    }
                    else
                    {
                        count++;
                    }

                }

            }

            result = count <= limit;

            return result;
        }

        public bool GetHasCoupleRooms()
        {
            bool result = false;

            foreach (KxWebStudentPreference pref in Items)
            {
                if (pref.BedroomType != null)
                {
                    if (pref.BedroomType.CustomData2.Trim().ToUpper().Contains("COUPLE"))
                    {
                        result = true;
                        break;
                    }
                }
            }

            return result;
        }

        public int GetAreaCount(int areaId)
        {
            int result = 0;

            foreach (KxWebStudentPreference pref in Items)
            {
                if (pref.AreaId == areaId)
                {
                    result++;
                }
            }
            return result;
        }

        public int GetBlockCount(int blockId)
        {
            int result = 0;

            foreach (KxWebStudentPreference pref in Items)
            {
                if (pref.BlockId == blockId)
                {
                    result++;
                }
            }
            return result;
        }

        public int GetAreaCustomValue3Count(int areaId, string customValue3)
        {
            int result = 0;

            foreach (KxWebStudentPreference pref in Items)
            {
                if (pref.AreaId == areaId && pref.CustomValue3 == customValue3)
                {
                    result++;
                }
            }
            return result;
        }

        public int GetBlockCustomValue3Count(int blockId, string customValue3)
        {
            int result = 0;

            foreach (KxWebStudentPreference pref in Items)
            {
                if (pref.BlockId == blockId && pref.CustomValue3 == customValue3)
                {
                    result++;
                }
            }
            return result;
        }

        public bool HasMadePreferenceSelection()
        {
            return (from p in this.items
                where
                    p.SiteId != 0 || p.AreaId != 0 || p.BlockId != 0 || p.BedroomTypeId != 0 ||
                    !string.IsNullOrWhiteSpace(p.CustomValue1) ||
                    !string.IsNullOrWhiteSpace(p.CustomValue2) ||
                    !string.IsNullOrWhiteSpace(p.CustomValue3) ||
                    !string.IsNullOrWhiteSpace(p.CustomValue4) ||
                    !string.IsNullOrWhiteSpace(p.CustomValue5) ||
                    !string.IsNullOrWhiteSpace(p.CustomValue6) ||
                    !string.IsNullOrWhiteSpace(p.CustomValue7) ||
                    !string.IsNullOrWhiteSpace(p.CustomValue8) 
                select p).Count() > 0;
        }


    }
}
