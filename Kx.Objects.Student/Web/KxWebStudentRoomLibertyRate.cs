using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;

namespace Kx.Objects.Student
{
    [Serializable]
    public class KxWebStudentRoomLibertyRate : KxBaseClass
    {
        #region Attributes & Properties

        protected int      allocationModelId;
        protected int      defaultPricingModelId;
        protected int      bedroomTypeId;
        protected string   roomType;
        protected string   rentPerWeek;
        protected int      noWeeks;
        protected string   singlePayment;
        protected string   installments;
        protected int      unavailable;
        protected decimal  weeklyAmount;
        protected string comment;

        public int      AllocationModelId
        {
            get
            { return allocationModelId;}
            set
            { 
                AddToAudit("AllocationModelId", allocationModelId, value);
                allocationModelId = value;
            }
        }

        public int      DefaultPricingModelId
        {
            get
            { return defaultPricingModelId;}
            set
            { 
                AddToAudit("DefaultPricingModelId", defaultPricingModelId, value);
                defaultPricingModelId = value;
            }
        }

        public int      BedroomTypeId
        {
            get
            { return bedroomTypeId;}
            set
            { 
                AddToAudit("BedroomTypeId", bedroomTypeId, value);
                bedroomTypeId = value;
            }
        }

        public string   RoomType
        {
            get
            { return roomType;}
            set
            { 
                AddToAudit("RoomType", roomType, value);
                roomType = value;
            }
        }

        public string   RentPerWeek
        {
            get
            { return rentPerWeek;}
            set
            { 
                AddToAudit("RentPerWeek", rentPerWeek, value);
                rentPerWeek = value;
            }
        }

        public int      NoWeeks
        {
            get
            { return noWeeks;}
            set
            { 
                AddToAudit("NoWeeks", noWeeks, value);
                noWeeks = value;
            }
        }

        public string   SinglePayment
        {
            get
            { return singlePayment;}
            set
            { 
                AddToAudit("SinglePayment", singlePayment, value);
                singlePayment = value;
            }
        }

        public string Comment
        {
            get
            { return comment; }
            set
            {
                AddToAudit("Comment", comment, value);
                comment = value;
            }
        }

        public string   Installments
        {
            get
            { return installments;}
            set
            { 
                AddToAudit("Installments", installments, value);
                installments = value;
            }
        }

        public string InstallmentsNoBreaks
        {
            get
            {
                return Installments.Replace(" ", "&nbsp;");
            }
        }

        public int      Unavailable
        {
            get
            { return unavailable;}
            set
            { 
                AddToAudit("Unavailable", unavailable, value);
                unavailable = value;
            }
        }

        public decimal  WeeklyAmount
        {
            get
            { return weeklyAmount;}
            set
            { 
                AddToAudit("WeeklyAmount", weeklyAmount, value);
                weeklyAmount = value;
            }
        }

        public string UniqueId
        {
            get
            {
                return AllocationModelId.ToString() + "A" + DefaultPricingModelId.ToString() + "A" + BedroomTypeId.ToString();
            }
        }

        public bool HasComment
        {
            get
            {
                return !String.IsNullOrEmpty(comment);
            }
        }

        #endregion 

        public KxWebStudentRoomLibertyRate(KxBaseClass parent, XmlNode node) : base(parent)
        {
             LoadFromXml("", node);
        }
        
        public override  void LoadFromXml(string xml,XmlNode node)
        {
            allocationModelId  = KxXmlUtils.GetNodeValue(node,"allocationmodelid",0);
            defaultPricingModelId  = KxXmlUtils.GetNodeValue(node,"defaultpricingmodelid",0);
            bedroomTypeId  = KxXmlUtils.GetNodeValue(node,"bedroomtypeid",0);
            roomType  = KxXmlUtils.GetNodeValue(node,"roomtype","");
            rentPerWeek  = KxXmlUtils.GetNodeValue(node,"rentperweek","");
            noWeeks  = KxXmlUtils.GetNodeValue(node,"noweeks",0);
            singlePayment  = KxXmlUtils.GetNodeValue(node,"singlepayment","");
            installments  = KxXmlUtils.GetNodeValue(node,"installments","");
            unavailable  = KxXmlUtils.GetNodeValue(node,"unavailable",0);
            weeklyAmount  = KxXmlUtils.GetNodeValue(node,"weeklyamount",(decimal)0.00M);
            comment = KxXmlUtils.GetNodeValue(node, "comment", "");
        }
    }
}

