using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;
using Kx.Objects.Student.Web;

namespace Kx.Objects.Student
{
    [Serializable]
    public class KxPricingModelCharge : KxBaseClass
    {
        #region Attributes & Properties

        protected int      pricingModelId;
        protected int      studentChargeTypeId;
        protected decimal     dailyAmount;
        protected decimal     weeklyAmount;
        protected int      academicYearId;

        protected KxStudentChargeType studentChargeType;

        public int      PricingModelChargeId
        {
            get
            { return base.dBIdentifier;}
        }

        public int      PricingModelId
        {
            get
            { return pricingModelId;}
            set
            { 
                AddToAudit("PricingModelId", pricingModelId, value);
                pricingModelId = value;
            }
        }

        public int      StudentChargeTypeId
        {
            get
            { return studentChargeTypeId;}
            set
            { 
                AddToAudit("StudentChargeTypeId", studentChargeTypeId, value);
                studentChargeTypeId = value;
            }
        }

        public decimal     DailyAmount
        {
            get
            { return dailyAmount;}
            set
            { 
                AddToAudit("DailyAmount", dailyAmount, value);
                dailyAmount = value;
            }
        }

        public decimal     WeeklyAmount
        {
            get
            { return weeklyAmount;}
            set
            { 
                AddToAudit("WeeklyAmount", weeklyAmount, value);
                weeklyAmount = value;
            }
        }

        public int      AcademicYearId
        {
            get
            { return academicYearId;}
            set
            { 
                AddToAudit("AcademicYearId", academicYearId, value);
                academicYearId = value;
            }
        }

        public KxStudentChargeType StudentChargeType
        {
            get
            {
                if (studentChargeType == null && studentChargeTypeId > 0)
                {
                    studentChargeType = KxStudentObjectManager.StudentChargeTypes.GetStudentChargeTypeById(studentChargeTypeId);
                }
                return studentChargeType;
            }
        }
        
        #endregion 

        public KxPricingModelCharge(int pricingmodelchargeid)
        {
             base.dBIdentifier = pricingmodelchargeid;
             Load();
        }

        public KxPricingModelCharge(KxBaseClass parent, XmlNode node) : base(parent)
        {
             LoadFromXml("", node);
        }

        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("st2pricingmodelcharges", "ST2PricingModelCharges", TargetType.StoredProc);

            result.AutoKeyField = new KxDataValue("PricingModelChargeId",PricingModelChargeId,KxValueType.Int);


            if ((actiontype == ActionType.Update) && (PricingModelChargeId == 0))
            {
                result.Action = ActionType.Insert;
            }
            else
            {
                result.Action = actiontype;
            }

            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {
                   result.AddParameter("pricingModelId", pricingModelId, KxValueType.Int);
                   result.AddParameter("studentChargeTypeId", studentChargeTypeId, KxValueType.Int);
                   result.AddParameter("dailyAmount", dailyAmount, KxValueType.Decimal);
                   result.AddParameter("weeklyAmount", weeklyAmount, KxValueType.Decimal);
                   result.AddParameter("academicYearId", academicYearId, KxValueType.Int);
            }
            return result;
        }


        public override void LoadFromXml(string xml, XmlNode node)
        {
            dBIdentifier  = KxXmlUtils.GetNodeValue(node,"pricingmodelchargeid",0);
            pricingModelId  = KxXmlUtils.GetNodeValue(node,"pricingmodelid",0);
            studentChargeTypeId  = KxXmlUtils.GetNodeValue(node,"studentchargetypeid",0);
            dailyAmount  = KxXmlUtils.GetNodeValue(node,"dailyamount",(Decimal)0.00);
            weeklyAmount = KxXmlUtils.GetNodeValue(node, "weeklyamount", (Decimal)0.00);
            academicYearId  = KxXmlUtils.GetNodeValue(node,"academicyearid",0);
        }
    }
}

