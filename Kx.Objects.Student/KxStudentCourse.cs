using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;

namespace Kx.Objects.Student
{
    [Serializable]
    public class KxStudentCourse : KxBaseClass
    {
        #region Attributes & Properties

        protected int      kxStudentId;
        protected int      courseId;
        protected bool     currentCourse;
        protected DateTime ?courseStartDate;
        protected DateTime ?courseEndDate;
        protected string   attendanceMode;
        protected string   yearOfEntry;
        protected int      studentStatusId;

        protected KxCourse course = null;

        public int      StudentCourseId
        {
            get
            { return base.dBIdentifier;}
        }

        public int      KxStudentId
        {
            get
            { return kxStudentId;}
            set
            { 
                AddToAudit("KxStudentId", kxStudentId, value);
                kxStudentId = value;
            }
        }

        public int      CourseId
        {
            get
            { return courseId;}
            set
            { 
                AddToAudit("CourseId", courseId, value);
                courseId = value;
            }
        }

        public bool     CurrentCourse
        {
            get
            { return currentCourse;}
            set
            { 
                AddToAudit("CurrentCourse", currentCourse, value);
                currentCourse = value;
            }
        }

        public DateTime ?CourseStartDate
        {
            get
            { return courseStartDate;}
            set
            { 
                AddToAudit("CourseStartDate", courseStartDate, value);
                courseStartDate = value;
            }
        }

        public DateTime ?CourseEndDate
        {
            get
            { return courseEndDate;}
            set
            { 
                AddToAudit("CourseEndDate", courseEndDate, value);
                courseEndDate = value;
            }
        }

        public string   AttendanceMode
        {
            get
            { return attendanceMode;}
            set
            { 
                AddToAudit("AttendanceMode", attendanceMode, value);
                attendanceMode = value;
            }
        }

        public string   YearOfEntry
        {
            get
            { return yearOfEntry;}
            set
            { 
                AddToAudit("YearOfEntry", yearOfEntry, value);
                yearOfEntry = value;
            }
        }

        public int StudentStatusId
        {
            get
            { return studentStatusId;}
            set
            { 
                AddToAudit("StudentStatusId", studentStatusId, value);
                studentStatusId = value;
            }
        }

        public KxCourse Course
        {
            get
            {
                if (course == null)
                {
                    course = new KxCourse(this, courseId);
                }
                return course;
            }
            set
            { course = value; }
        }

        public bool IsUnderGraduateCourse
        {
            get
            {
                return GetIsUnderGraduateCourse();
            }
        }

        public string CourseType
        {
            get
            {
                return GetCourseType();
            }
        }

        public string FacultyName
        {

            get
            {
                if (Course != null && Course.Faculty != null)
                {
                    return Course.Faculty.Description;
                }
                else
                {
                    return "";
                }
            }
        }

        public string CourseName
        {

            get
            {
                if (Course != null )
                {
                    return Course.Description;
                }
                else
                {
                    return "";
                }
            }
        }

        public bool IsCourseStartHistoric
        {
            get
            {
                try
                {
                    return courseStartDate.Value < DateTime.Now;
                }
                catch
                {
                    return false;
                }
            }
        }
        #endregion 

        public KxStudentCourse(KxBaseClass parent,int      studentcourseid) : base(parent)
        {
             base.dBIdentifier = studentcourseid;
             Load();
        }

           public KxStudentCourse(KxBaseClass parent,XmlNode node) : base(parent)
        {
             LoadFromXml("",node);
        }
        
        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("st2studentcourses", "ST2StudentCourses", TargetType.Table);

            result.AutoKeyField = new KxDataValue("StudentCourseId",StudentCourseId,KxValueType.Int);


            if ((actiontype == ActionType.Update) && (StudentCourseId == 0))
            {
                result.Action = ActionType.Insert;
            }
            else
            {
                result.Action = actiontype;
            }

            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {
                   result.AddParameter("kxStudentId", kxStudentId, KxValueType.Int);
                   result.AddParameter("courseId", courseId, KxValueType.Int);
                   result.AddParameter("currentCourse", currentCourse, KxValueType.Boolean);
                   result.AddParameter("courseStartDate", courseStartDate, KxValueType.DateTime);
                   result.AddParameter("courseEndDate", courseEndDate, KxValueType.DateTime);
                   result.AddParameter("attendanceMode", attendanceMode, KxValueType.String);
                   result.AddParameter("yearOfEntry", yearOfEntry, KxValueType.String);
                   result.AddParameter("studentStatusId", studentStatusId, KxValueType.Int);
            }
            return result;
        }


        public override void LoadFromXml(string xml,XmlNode node)
        {
            Children.Remove(course);
            course = null;

            dBIdentifier = KxXmlUtils.GetNodeValue(node, "studentcourseid", 0);
            kxStudentId  = KxXmlUtils.GetNodeValue(node,"kxstudentid",0);
            courseId  = KxXmlUtils.GetNodeValue(node,"courseid",0);
            currentCourse  = KxXmlUtils.GetNodeValue(node,"currentcourse",false);
            courseStartDate  = KxXmlUtils.GetNodeValueDT(node,"coursestartdate",null);
            courseEndDate  = KxXmlUtils.GetNodeValueDT(node,"courseenddate",null);
            attendanceMode  = KxXmlUtils.GetNodeValue(node,"attendancemode","");
            yearOfEntry  = KxXmlUtils.GetNodeValue(node,"yearofentry","");
            studentStatusId  = KxXmlUtils.GetNodeValue(node,"studentstatusid",0);
        }

        private bool GetIsUnderGraduateCourse()
        {
            bool result = false;

            if (Course != null && Course.CourseType != null)
            {
                result = Course.CourseType.Name == "UG" || Course.CourseType.Description == "UG";
            }
            return result;
        }

        private string GetCourseType()
        {
            if (IsUnderGraduateCourse)
            {
                return "Undergraduate";
            }
            else
            {
                return "Postgraduate";
            }
        }
    }
}

