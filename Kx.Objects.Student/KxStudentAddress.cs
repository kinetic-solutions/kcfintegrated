using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;

namespace Kx.Objects.Student
{
    [Serializable]
    public class KxStudentAddress : KxBaseClass
    {
        #region Attributes & Properties

        protected int kxStudentId;
        protected int addressTypeId;
        protected string address;
        protected string town;
        protected string county;
        protected string country;
        protected string postcode;
        protected DateTime? creationDate;
        protected int creationUserId;
        protected string emailAddress;
        protected string telephone;
        protected string fax;
        protected string mobile;
        protected string foreName;
        protected string surName;
        protected int emailId;
        protected bool imported;
        protected DateTime? validFrom;
        protected DateTime? validTo;
        protected string relationship;
        protected string _title;

        public int StudentAddressId
        {
            get { return base.dBIdentifier; }
        }

        public int KxStudentId
        {
            get { return kxStudentId; }
            set
            {
                AddToAudit("KxStudentId", kxStudentId, value);
                kxStudentId = value;
            }
        }

        public int AddressTypeId
        {
            get { return addressTypeId; }
            set
            {
                AddToAudit("AddressTypeId", addressTypeId, value);
                addressTypeId = value;
            }
        }

        public string Address
        {
            get { return address; }
            set
            {
                AddToAudit("Address", address, value);
                address = value;
            }
        }

        public string Town
        {
            get { return town; }
            set
            {
                AddToAudit("Town", town, value);
                town = value;
            }
        }

        public string County
        {
            get { return county; }
            set
            {
                AddToAudit("County", county, value);
                county = value;
            }
        }

        public string Country
        {
            get { return country; }
            set
            {
                AddToAudit("Country", country, value);
                country = value;
            }
        }

        public string Postcode
        {
            get { return postcode; }
            set
            {
                AddToAudit("Postcode", postcode, value);
                postcode = value;
            }
        }

        public DateTime? CreationDate
        {
            get { return creationDate; }
            set
            {
                AddToAudit("CreationDate", creationDate, value);
                creationDate = value;
            }
        }

        public int CreationUserId
        {
            get { return creationUserId; }
            set
            {
                AddToAudit("CreationUserId", creationUserId, value);
                creationUserId = value;
            }
        }

        public string EmailAddress
        {
            get { return emailAddress; }
            set
            {
                AddToAudit("EmailAddress", emailAddress, value);
                emailAddress = value;
            }
        }

        public string Telephone
        {
            get { return telephone; }
            set
            {
                AddToAudit("Telephone", telephone, value);
                telephone = value;
            }
        }

        public string Fax
        {
            get { return fax; }
            set
            {
                AddToAudit("Fax", fax, value);
                fax = value;
            }
        }

        public string Mobile
        {
            get { return mobile; }
            set
            {
                AddToAudit("Mobile", mobile, value);
                mobile = value;
            }
        }

        public string Title
        {
            get { return _title; }
            set
            {
                AddToAudit("Title", _title, value);
                _title = value;
            }
        }

        public string ForeName
        {
            get { return foreName; }
            set
            {
                AddToAudit("ForeName", foreName, value);
                foreName = value;
            }
        }

        public string SurName
        {
            get { return surName; }
            set
            {
                AddToAudit("SurName", surName, value);
                surName = value;
            }
        }

        public int EmailId
        {
            get { return emailId; }
            set
            {
                AddToAudit("EmailId", emailId, value);
                emailId = value;
            }
        }

        public bool Imported
        {
            get { return imported; }
            set
            {
                AddToAudit("Imported", imported, value);
                imported = value;
            }
        }

        public DateTime? ValidFrom
        {
            get { return validFrom; }
            set
            {
                AddToAudit("ValidFrom", validFrom, value);
                validFrom = value;
            }
        }

        public DateTime? ValidTo
        {
            get { return validTo; }
            set
            {
                AddToAudit("ValidTo", validTo, value);
                validTo = value;
            }
        }

        public string FormattedAddressSL
        {
            get { return GetFormattedAddressSL(); }
        }

        public string Relationship
        {
            get { return relationship; }
            set
            {
                AddToAudit("Relationship", relationship, value);
                relationship = value;
            }
        }

        #endregion

        public KxStudentAddress()
            : base(null)
        {
        }

        public KxStudentAddress(KxBaseClass parent) : base(parent)
        {
        }

        public KxStudentAddress(KxBaseClass parent, int studentaddressid)
            : this(parent)
        {
            base.dBIdentifier = studentaddressid;
            Load();
        }

        public KxStudentAddress(KxBaseClass parent, int kxStudentId, int addressTypeId)
            //rearranged args to re-created method signature
            : this(parent)
        {
            this.AddressTypeId = addressTypeId;
            this.kxStudentId = kxStudentId;
            Load();
        }


        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("st2studentaddress", "ST2StudentAddresses", TargetType.Table);

            result.AutoKeyField = new KxDataValue("StudentAddressId", StudentAddressId, KxValueType.Int);

            if (actiontype == ActionType.Select)
            {
                if (addressTypeId > 0) // Stu - Added to load a specific address by address type
                {
                    result.AddKeyField("AddressTypeId", addressTypeId, KxValueType.Int);
                    result.AddKeyField("KxStudentId", kxStudentId, KxValueType.Int);
                    result.AutoKeyField = null; //set this back to null so we don't search by addressId
                }
            }


            if ((actiontype == ActionType.Update) && (StudentAddressId == 0))
            {
                result.Action = ActionType.Insert;
            }
            else
            {
                result.Action = actiontype;
            }

            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {
                result.AddParameter("kxStudentId", kxStudentId, KxValueType.Int);
                result.AddParameter("addressTypeId", addressTypeId, KxValueType.Int);
                result.AddParameter("address", address, KxValueType.String);
                result.AddParameter("town", town, KxValueType.String);
                result.AddParameter("county", county, KxValueType.String);
                result.AddParameter("country", country, KxValueType.String);
                result.AddParameter("postcode", postcode, KxValueType.String);
                result.AddParameter("creationDate", creationDate, KxValueType.DateTime);
                result.AddParameter("creationUserId", creationUserId, KxValueType.Int);
                result.AddParameter("emailAddress", emailAddress, KxValueType.String);
                result.AddParameter("telephone", telephone, KxValueType.String);
                result.AddParameter("fax", fax, KxValueType.String);
                result.AddParameter("mobile", mobile, KxValueType.String);
                result.AddParameter("foreName", foreName, KxValueType.String);
                result.AddParameter("surName", surName, KxValueType.String);
                result.AddParameter("emailId", emailId, KxValueType.Int);
                result.AddParameter("imported", imported, KxValueType.Boolean);
                result.AddParameter("validFrom", validFrom, KxValueType.DateTime);
                result.AddParameter("validTo", validTo, KxValueType.DateTime);
                result.AddParameter("relationship", relationship, KxValueType.String);
                result.AddParameter("title", _title, KxValueType.String);
            }
            return result;
        }

        public override void LoadFromXml(string xml, XmlNode node)
        {
            if (addressTypeId > 0) // if we've loaded by this, populate the Id
            {
                base.dBIdentifier = KxXmlUtils.GetNodeValue(node, "studentaddressid", 0);
            }

            kxStudentId = KxXmlUtils.GetNodeValue(node, "kxstudentid", 0);
            addressTypeId = KxXmlUtils.GetNodeValue(node, "addresstypeid", 0);
            address = KxXmlUtils.GetNodeValue(node, "address", string.Empty);
            town = KxXmlUtils.GetNodeValue(node, "town", string.Empty);
            county = KxXmlUtils.GetNodeValue(node, "county", string.Empty);
            country = KxXmlUtils.GetNodeValue(node, "country", string.Empty);
            postcode = KxXmlUtils.GetNodeValue(node, "postcode", string.Empty);
            creationDate = KxXmlUtils.GetNodeValueDT(node, "creationdate", null);
            creationUserId = KxXmlUtils.GetNodeValue(node, "creationuserid", 0);
            emailAddress = KxXmlUtils.GetNodeValue(node, "emailaddress", string.Empty);
            telephone = KxXmlUtils.GetNodeValue(node, "telephone", string.Empty);
            fax = KxXmlUtils.GetNodeValue(node, "fax", string.Empty);
            mobile = KxXmlUtils.GetNodeValue(node, "mobile", string.Empty);
            _title = KxXmlUtils.GetNodeValue(node, "title", string.Empty);
            foreName = KxXmlUtils.GetNodeValue(node, "forename", string.Empty);
            surName = KxXmlUtils.GetNodeValue(node, "surname", string.Empty);
            emailId = KxXmlUtils.GetNodeValue(node, "emailid", 0);
            imported = KxXmlUtils.GetNodeValue(node, "imported", false);
            validFrom = KxXmlUtils.GetNodeValueDT(node, "validfrom", null);
            validTo = KxXmlUtils.GetNodeValueDT(node, "validto", null);
            relationship = KxXmlUtils.GetNodeValue(node, "relationship", string.Empty);
        }

        public string GetFormattedAddressSL()
        {
            string result = address + ", " + town + ", " + county + ", " + country + ", " + postcode;

            result = result.Replace(", , , , ", ", ");
            result = result.Replace(", , , ", ", ");
            result = result.Replace(", , ", ", ");
            result = result.TrimEnd(' ', ',');

            return result;
        }

        public bool PostCodePrefixMatch(string postcodes)
        {
            return Kx.Objects.Student.Helpers.PostcodeHelper.PostCodeOutcodeMatch(Postcode, postcodes);
        }
    }
}