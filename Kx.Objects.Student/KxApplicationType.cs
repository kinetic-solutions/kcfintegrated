using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;

namespace Kx.Objects.Student
{
    [Serializable]
    public class KxApplicationType : KxBaseClass
    {
        #region Attributes & Properties

        protected string   description;
        protected int      licenceTypeId;
        protected int      studentYearId;
        protected int      courseTypeId;
        protected bool     inactive;
        protected bool     webPublished;
        protected KxCourseType courseType = null;

        public int      ApplicationTypeId
        {
            get
            { return base.dBIdentifier;}
        }

        public string   Description
        {
            get
            { return description;}
            set
            { 
                AddToAudit("Description", description, value);
                description = value;
            }
        }

        public int      LicenceTypeId
        {
            get
            { return licenceTypeId;}
            set
            { 
                AddToAudit("LicenceTypeId", licenceTypeId, value);
                licenceTypeId = value;
            }
        }

        public int      StudentYearId
        {
            get
            { return studentYearId;}
            set
            { 
                AddToAudit("StudentYearId", studentYearId, value);
                studentYearId = value;
            }
        }

        public int      CourseTypeId
        {
            get
            { return courseTypeId;}
            set
            { 
                AddToAudit("CourseTypeId", courseTypeId, value);
                courseTypeId = value;
            }
        }

        public KxCourseType CourseType
        {
            get
            {
                if (courseType == null && courseTypeId > 0)
                {
                    courseType = new KxCourseType(this, courseTypeId);
                }
                return courseType;
            }
            set
            {
                if (courseType == value)
                    return;
                courseType = value;
            }
        }

        public bool     Inactive
        {
            get
            { return inactive;}
            set
            { 
                AddToAudit("Inactive", inactive, value);
                inactive = value;
            }
        }

        public bool     WebPublished
        {
            get
            { return webPublished;}
            set
            { 
                AddToAudit("WebPublished", webPublished, value);
                webPublished = value;
            }
        }


        #endregion 

        public KxApplicationType(KxBaseClass parent): base(parent)
        {
          
        }

        public KxApplicationType(KxBaseClass parent,int      applicationtypeid) :base(parent)
        {
             base.dBIdentifier = applicationtypeid;
             Load();
        }

        public KxApplicationType(KxBaseClass parent, XmlNode node)
        {
            LoadFromXml("", node);
        }
        
        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("st2applicationtypes", "ST2ApplicationTypes", TargetType.Table);

            result.AutoKeyField = new KxDataValue("ApplicationTypeId",ApplicationTypeId,KxValueType.Int);
            
            if ((actiontype == ActionType.Update) && (ApplicationTypeId == 0))
            {
                result.Action = ActionType.Insert;
            }
            else
            {
                result.Action = actiontype;
            }

            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {
                   result.AddParameter("description", description, KxValueType.String);
                   result.AddParameter("licenceTypeId", licenceTypeId, KxValueType.Int);
                   result.AddParameter("studentYearId", studentYearId, KxValueType.Int);
                   result.AddParameter("courseTypeId", courseTypeId, KxValueType.Int);
                   result.AddParameter("inactive", inactive, KxValueType.Boolean);
                   result.AddParameter("webPublished", webPublished, KxValueType.Boolean);
            }
            return result;
        }


        public override void LoadFromXml(string xml,XmlNode node)
        {
            dBIdentifier = KxXmlUtils.GetNodeValue(node, "applicationtypeid", 0);
            description  = KxXmlUtils.GetNodeValue(node,"description","");
            licenceTypeId  = KxXmlUtils.GetNodeValue(node,"licencetypeid",0);
            studentYearId  = KxXmlUtils.GetNodeValue(node,"studentyearid",0);
            courseTypeId  = KxXmlUtils.GetNodeValue(node,"coursetypeid",0);
            inactive  = KxXmlUtils.GetNodeValue(node,"inactive",false);
            webPublished  = KxXmlUtils.GetNodeValue(node,"webpublished",false);
        }

        public static IEnumerable<KxApplicationType> GetApplicationTypes()
        {
            KxData data = new KxData("applicationtype", "ST2ApplicationTypes", TargetType.Table);
            if (data.Execute())
            {
                XmlNodeList nodes = data.GetNodesFromData();
                foreach (XmlNode node in nodes)
                {
                    yield return new KxApplicationType(null, node);
                }
            }
            else
            {
                KxLogger.LogActivity("KxApplicationType", "GetApplicationTypes", data.LastError, "");
            }
        }
    }
}

