using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;

namespace Kx.Objects.Student
{
    [Serializable]
    public class KxRoomPricingModel : KxBaseClass
    {
        #region Attributes & Properties

        protected int      roomConfigId;
        protected int      pricingModelId;

        public int      RoomConfigId
        {
            get
            { return roomConfigId;}
            set
            { 
                AddToAudit("RoomConfigId", roomConfigId, value);
                roomConfigId = value;
            }
        }

        public int      PricingModelId
        {
            get
            { return pricingModelId;}
            set
            { 
                AddToAudit("PricingModelId", pricingModelId, value);
                pricingModelId = value;
            }
        }


        #endregion 


        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("st2roompricingmodels", "ST2RoomPricingModels", TargetType.StoredProc);

//             result.AutoKeyField = new KxDataValue("",,XXXX);


//             if ((actiontype == ActionType.Update) && ( == 0))
//             {
//                 result.Action = ActionType.Insert;
//             }
//             else
//             {
//                 result.Action = actiontype;
//             }

            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {
                   result.AddParameter("roomConfigId", roomConfigId, KxValueType.Int);
                   result.AddParameter("pricingModelId", pricingModelId, KxValueType.Int);
            }
            return result;
        }


        public override void LoadFromXml(string xml, XmlNode node)
        {
            roomConfigId  = KxXmlUtils.GetNodeValue(node,"roomconfigid",0);
            pricingModelId  = KxXmlUtils.GetNodeValue(node,"pricingmodelid",0);
        }
    }
}

