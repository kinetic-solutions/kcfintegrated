using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Exceptions;
using Kx.Core.Utility;

namespace Kx.Objects.Student
{
    public class KxStudentAccount : KxBaseClass
    {
        #region Attributes & Properties

        private int kxStudentId;
       
        protected KxStudentCharges charges = null;
        protected KxStudentPayments payments = null;
       
        public int KxStudentId
        {
            get
            { return kxStudentId; }
            set
            { kxStudentId = value; }
        }

        public KxStudentCharges Charges
        {
            get
            {
                if (charges == null)
                {
                    charges = new KxStudentCharges(this, kxStudentId, KxStudentChargingType.Student);
                }
                return charges;
            }
            set
            { charges = value; }
        }


        public KxStudentPayments Payments
        {
            get
            {
                if (payments == null)
                {
                    payments = new KxStudentPayments(this,kxStudentId,KxPaymentSource.Student);
                }
                return payments;
            }
            set
            { payments = value; }
        }

        #endregion

        public KxStudentAccount(KxBaseClass parent, int kxStudentId) : base(parent)
        {
            this.kxStudentId = kxStudentId;
        }

  
    }
}
