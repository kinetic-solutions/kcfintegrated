using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;

namespace Kx.Objects.Student
{
    [Serializable]
    public class KxCourses : KxBaseClass
    {
        #region Attributes & Properties

        protected string   description;
        protected string   code;
        protected string   name;
        protected int      facultyId;
        protected int      courseTypeId;
        protected int      importId;

        public int      CourseId
        {
            get
            { return base.dBIdentifier;}
        }

        public string   Description
        {
            get
            { return description;}
            set
            { 
                AddToAudit("Description", description, value);
                description = value;
            }
        }

        public string   Code
        {
            get
            { return code;}
            set
            { 
                AddToAudit("Code", code, value);
                code = value;
            }
        }

        public string   Name
        {
            get
            { return name;}
            set
            { 
                AddToAudit("Name", name, value);
                name = value;
            }
        }

        public int      FacultyId
        {
            get
            { return facultyId;}
            set
            { 
                AddToAudit("FacultyId", facultyId, value);
                facultyId = value;
            }
        }

        public int      CourseTypeId
        {
            get
            { return courseTypeId;}
            set
            { 
                AddToAudit("CourseTypeId", courseTypeId, value);
                courseTypeId = value;
            }
        }

        public int      ImportId
        {
            get
            { return importId;}
            set
            { 
                AddToAudit("ImportId", importId, value);
                importId = value;
            }
        }


        #endregion 

        public KxCourses(KxBaseClass parent,int      courseid) : base(parent)
        {
             base.dBIdentifier = courseid;
             Load();
        }


        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("courses", "Courses", TargetType.StoredProc);

            result.AutoKeyField = new KxDataValue("CourseId",CourseId,KxValueType.Int);


            if ((actiontype == ActionType.Update) && (CourseId == 0))
            {
                result.Action = ActionType.Insert;
            }
            else
            {
                result.Action = actiontype;
            }

            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {
                   result.AddParameter("description", description, KxValueType.String);
                   result.AddParameter("code", code, KxValueType.String);
                   result.AddParameter("Name", Name, KxValueType.String);
                   result.AddParameter("facultyId", facultyId, KxValueType.Int);
                   result.AddParameter("courseTypeId", courseTypeId, KxValueType.Int);
                   result.AddParameter("importId", importId, KxValueType.Int);
            }
            return result;
        }


        public override void LoadFromXml(string xml,XmlNode node)
        {
            description  = KxXmlUtils.GetNodeValue(node,"description","");
            code  = KxXmlUtils.GetNodeValue(node,"code","");
            Name  = KxXmlUtils.GetNodeValue(node,"name","");
            facultyId  = KxXmlUtils.GetNodeValue(node,"facultyid",0);
            courseTypeId  = KxXmlUtils.GetNodeValue(node,"coursetypeid",0);
            importId  = KxXmlUtils.GetNodeValue(node,"importid",0);
        }
    }
}

