using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;

namespace Kx.Objects.Student
{
    [Serializable]
    public class KxCourseType : KxBaseClass
    {
        #region Attributes & Properties

        protected string   name;
        protected string   description;
        protected int      importId;

        public int      CourseTypeId
        {
            get
            { return base.dBIdentifier;}
        }

        public string   Name
        {
            get
            { return name;}
            set
            { 
                AddToAudit("Name", name, value);
                name = value;
            }
        }

        public string   Description
        {
            get
            { return description;}
            set
            { 
                AddToAudit("Description", description, value);
                description = value;
            }
        }

        public int      ImportId
        {
            get
            { return importId;}
            set
            { 
                AddToAudit("ImportId", importId, value);
                importId = value;
            }
        }


        #endregion 

        public KxCourseType(KxBaseClass parent, int coursetypeid) : base(parent)
        {
             base.dBIdentifier = coursetypeid;
             Load();
        }

        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("coursetypes", "CourseTypes", TargetType.Table);

            result.AutoKeyField = new KxDataValue("CourseTypeId",CourseTypeId,KxValueType.Int);

            if ((actiontype == ActionType.Update) && (CourseTypeId == 0))
            {
                result.Action = ActionType.Insert;
            }
            else
            {
                result.Action = actiontype;
            }

            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {
                   result.AddParameter("Name", Name, KxValueType.String);
                   result.AddParameter("description", description, KxValueType.String);
                   result.AddParameter("importId", importId, KxValueType.Int);
            }
            return result;
        }
        
        public override void LoadFromXml(string xml,XmlNode node)
        {
            dBIdentifier = KxXmlUtils.GetNodeValue(node, "coursetypeid", 0);
            Name  = KxXmlUtils.GetNodeValue(node,"name","");
            description  = KxXmlUtils.GetNodeValue(node,"description","");
            importId  = KxXmlUtils.GetNodeValue(node,"importid",0);
        }
    }
}

