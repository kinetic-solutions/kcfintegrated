using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Merge;
using Kx.Core.Utility;
using Kx.Core.Exceptions;
using Kx.Objects.Common;
using Kx.Objects.Common.Web;
using Kx.Objects.Student.DataProviders;


namespace Kx.Objects.Student
{
    public enum StudentLoadType {Name,KxStudentId,StudentId };

    [Serializable]
    public class KxStudent : KxBaseClass, IMergeObject
    {
        #region Attributes & Properties

        private StudentLoadType loadType = StudentLoadType.KxStudentId;

        protected string studentId = "";
        protected string ucasNumber = "";
        protected string title = "";
        protected string foreName = "";
        protected string surName = "";
        protected string midName = "";
        protected string preferredMethodOfContact = "";
        protected int disabilityId = 0;
        protected string disability = "";
        protected string internationalStatus = "";
        protected bool smoker;
        protected int ethnicityId = 0;
        protected int nationalityId = 0;
        protected string nationality = "";
        protected DateTime ?dob;
        protected int studentStatusId = 0;
        protected DateTime ?creationDate;
        protected int creationUserId = 0;
        protected int personId = 0;
        protected int currentStudentAddressId = 0;
        protected string gender = "";
        protected int homeStudentAddressId = 0;
        protected int emergencyStudentAddressId = 0;
        protected string webPassword = "";
        protected string webUserId = "";
        protected bool webAccessAllowed;
        protected string salt = "";
        protected DateTime ?lockedAccountTime;
        protected int preferredContactMethod = 0;
        protected string akaName = "";
        protected int preferredEmailAddressId = 0;
        protected int debtorStatusId = 0;
        protected int retryCount = 0;
        protected int identifierTypeId = 0;
        protected int domicileId = 0;
        protected KxStudentAddress currentStudentAddress = null;
        protected KxStudentAddress emergencyStudentAddress  = null;
        protected KxStudentAddress homeStudentAddress  = null;
        protected KxStudentAddress preferredEmailAddress  = null;
        protected KxPerson person = null;
        protected KxStudentApplications applications = null;
        protected KxStudentAllocations allocations = null;
        protected KxStudentAccount account = null;
        protected KxStudentCourses courses = null;
        protected KxStudentCourse currentCourse = null;
        protected KxCustomFields2Values customFields = null;
        protected KxStudentStatus status = null;
        protected KxDepositBalances depositBalances = null;
        protected KxStudentCustomDatas customData = null;
        protected KxDebtorStatus debtorStatus = null;
        

        protected int _oldacademicYearID = -1;
       
        /// <summary>
        /// 
        /// </summary>
        public int      KxStudentId
        {
            get
            { return base.dBIdentifier;}
        }
        /// <summary>
        /// 
        /// </summary>
        public string   StudentId
        {
            get
            { return studentId;}
            set
            { 
                AddToAudit("StudentId", studentId, value);
                studentId = value;
            }
        }

        public string StudentIdPrefixedWithS
        {
            get
            { return String.Format("S{0}",studentId);}
           
        }
        /// <summary>
        /// 
        /// </summary>
        public string   UcasNumber
        {
            get
            { return ucasNumber;}
            set
            { 
                AddToAudit("UcasNumber", ucasNumber, value);
                ucasNumber = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public string   Title
        {
            get
            { return title;}
            set
            { 
                AddToAudit("Title", title, value);
                title = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public string   ForeName
        {
            get
            { return foreName;}
            set
            { 
                AddToAudit("ForeName", foreName, value);
                foreName = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public string   SurName
        {
            get
            { return surName;}
            set
            { 
                AddToAudit("SurName", surName, value);
                surName = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public string   MidName
        {
            get
            { return midName;}
            set
            { 
                AddToAudit("MidName", midName, value);
                midName = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public string   PreferredMethodOfContact
        {
            get
            { return preferredMethodOfContact;}
            set
            { 
                AddToAudit("PreferredMethodOfContact", preferredMethodOfContact, value);
                preferredMethodOfContact = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Disability
        {
            get
            {
                if (disability == "" && disabilityId != 0)
                {
                    disability = GetDisability(disabilityId);
                }
                return disability;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public int      DisabilityId
        {
            get
            { return disabilityId;}
            set
            { 
                AddToAudit("DisabilityId", disabilityId, value);
                disabilityId = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public string   InternationalStatus
        {
            get
            { return internationalStatus;}
            set
            { 
                AddToAudit("InternationalStatus", internationalStatus, value);
                internationalStatus = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public bool     Smoker
        {
            get
            { return smoker;}
            set
            { 
                AddToAudit("Smoker", smoker, value);
                smoker = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public int      EthnicityId
        {
            get
            { return ethnicityId;}
            set
            { 
                AddToAudit("EthnicityId", ethnicityId, value);
                ethnicityId = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Nationality
        {
            get
            {
                if (nationality == "" && nationalityId != 0)
                {
                    nationality = GetNationality(nationalityId);
                }
                return nationality; 
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public int      NationalityId
        {
            get
            { return nationalityId;}
            set
            { 
                AddToAudit("NationalityId", nationalityId, value);
                nationalityId = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime ?Dob
        {
            get
            { return dob;}
            set
            { 
                AddToAudit("Dob", dob, value);
                dob = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public string DobString
        {
            get
            {
                if (dob != null)
                {
                    return KxDateTime.ToString(dob,"d");
                }
                else
                {
                    return "";
                }
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public int      StudentStatusId
        {
            get
            { return studentStatusId;}
            set
            { 
                AddToAudit("StudentStatusId", studentStatusId, value);
                studentStatusId = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime ?CreationDate
        {
            get
            { return creationDate;}
            set
            { 
                AddToAudit("CreationDate", creationDate, value);
                creationDate = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public int      CreationUserId
        {
            get
            { return creationUserId;}
            set
            { 
                AddToAudit("CreationUserId", creationUserId, value);
                creationUserId = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public int      PersonId
        {
            get
            { return personId;}
            set
            { 
                AddToAudit("PersonId", personId, value);
                personId = value;

                if (personId != 0)
                {
                    person = new KxPerson(this,personId);
                }
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public int      CurrentStudentAddressId
        {
            get
            { return currentStudentAddressId;}
            set
            { 
                AddToAudit("CurrentStudentAddressId", currentStudentAddressId, value);
                currentStudentAddressId = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public string FullName
        {
            get
            { return foreName + " " + midName + " " + surName; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string   Gender
        {
            get
            { return gender;}
            set
            { 
                AddToAudit("Gender", gender, value);
                gender = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public string VerboseGender
        {
            get
            { return GetVerboseGender(); }
        }
        /// <summary>
        /// 
        /// </summary>
        public int GenderNumeric
        {
            get
            {
                if (Gender.ToUpper() == "M")
                {
                    return 1;
                }
                else if (Gender.ToUpper() == "F")
                {
                    return 2;
                }
                else
                {
                    return 0;
                }
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public int      HomeStudentAddressId
        {
            get
            { return homeStudentAddressId;}
            set
            { 
                AddToAudit("HomeStudentAddressId", homeStudentAddressId, value);
                homeStudentAddressId = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public int      EmergencyStudentAddressId
        {
            get
            { return emergencyStudentAddressId;}
            set
            { 
                AddToAudit("EmergencyStudentAddressId", emergencyStudentAddressId, value);
                emergencyStudentAddressId = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public string   WebPassword
        {
            get
            { return webPassword;}
            set
            { 
                AddToAudit("WebPassword", webPassword, value);
                webPassword = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public string   WebUserId
        {
            get
            { return webUserId;}
            set
            { 
                AddToAudit("WebUserId", webUserId, value);
                webUserId = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public bool     WebAccessAllowed
        {
            get
            { return webAccessAllowed;}
            set
            { 
                AddToAudit("WebAccessAllowed", webAccessAllowed, value);
                webAccessAllowed = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public string   Salt
        {
            get
            { return salt;}
            set
            { 
                AddToAudit("Salt", salt, value);
                salt = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime ?LockedAccountTime
        {
            get
            { return lockedAccountTime;}
            set
            { 
                AddToAudit("LockedAccountTime", lockedAccountTime, value);
                lockedAccountTime = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public int      PreferredContactMethod
        {
            get
            { return preferredContactMethod;}
            set
            { 
                AddToAudit("PreferredContactMethod", preferredContactMethod, value);
                preferredContactMethod = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public string   AkaName
        {
            get
            { return akaName;}
            set
            { 
                AddToAudit("AkaName", akaName, value);
                akaName = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public int      PreferredEmailAddressId
        {
            get
            { return preferredEmailAddressId;}
            set
            { 
                AddToAudit("PreferredEmailAddressId", preferredEmailAddressId, value);
                preferredEmailAddressId = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public int      DebtorStatusId
        {
            get
            { return debtorStatusId;}
            set
            { 
                AddToAudit("DebtorStatusId", debtorStatusId, value);
                debtorStatusId = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public int      RetryCount
        {
            get
            { return retryCount;}
            set
            { 
                AddToAudit("RetryCount", retryCount, value);
                retryCount = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public int      IdentifierTypeId
        {
            get
            { return identifierTypeId;}
            set
            { 
                AddToAudit("IdentifierTypeId", identifierTypeId, value);
                identifierTypeId = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public int      DomicileId
        {
            get
            { return domicileId;}
            set
            { 
                AddToAudit("DomicileId", domicileId, value);
                domicileId = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public int Age
        {
            get
            { return KxDateTime.CalculateAge(dob); }
        }
        /// <summary>
        /// 
        /// </summary>
        public bool IsAdult
        {
            get
            { return Age >= 18; }
        }
        /// <summary>
        /// 
        /// </summary>
        public bool IsJuvenile
        {
            get
            { return !IsAdult; }
        }
        /// <summary>
        /// 
        /// </summary>
        public KxStudentAddress CurrentAddress
        {
            get
            {
                if (currentStudentAddress == null)
                {
                    currentStudentAddress = new KxStudentAddress(this,currentStudentAddressId);

                    if (currentStudentAddress.StudentAddressId == 0 && KxStudentId != 0)
                    {
                        Children.Remove(currentStudentAddress);
                        currentStudentAddress = null;
                    }
                }
                return currentStudentAddress;
            }
            set
            { currentStudentAddress = value;}
        }
        /// <summary>
        /// 
        /// </summary>
        public KxStudentAddress EmergencyAddress
        {
            get
            {
                if (emergencyStudentAddress == null)
                {
                    emergencyStudentAddress = new KxStudentAddress(this,emergencyStudentAddressId);

                    if (emergencyStudentAddress.StudentAddressId == 0 && KxStudentId != 0)
                    {
                        Children.Remove(emergencyStudentAddress);
                        emergencyStudentAddress = null;
                    }
                }
                return emergencyStudentAddress;
            }
            set
            { emergencyStudentAddress = value;}
        }
        /// <summary>
        /// 
        /// </summary>
        public KxStudentAddress HomeAddress
        {
            get
            {
                if (homeStudentAddress == null)
                {
                    homeStudentAddress = new KxStudentAddress(this,homeStudentAddressId);

                    if (homeStudentAddress.StudentAddressId == 0 && KxStudentId != 0)
                    {
                        Children.Remove(homeStudentAddress);
                        homeStudentAddress = null;
                    }
                }
                return homeStudentAddress;
            }
            set
            { homeStudentAddress = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public KxStudentAddress PreferredEmailAddress
        {
            get
            {
                if (preferredEmailAddress == null)
                {
                    preferredEmailAddress = new KxStudentAddress(this,preferredEmailAddressId);

                    if (preferredEmailAddress.StudentAddressId == 0 && KxStudentId != 0)
                    {
                        Children.Remove(preferredEmailAddress);
                        preferredEmailAddress = null;
                    }
                }
                return preferredEmailAddress;
            }
            set
            { preferredEmailAddress = value;}
        }

        /// <summary>
        /// Returns the first non blank email address, checked in the correct order.
        /// If no email address is found an empty string is returned.  Read Only.
        /// </summary>
        public string EmailAddress
        {
            get
            {
                if (PreferredEmailAddress != null && PreferredEmailAddress.EmailAddress != string.Empty)
                {
                    return preferredEmailAddress.EmailAddress;
                }
                else if (CurrentAddress != null && CurrentAddress.EmailAddress != string.Empty)
                {
                    return CurrentAddress.EmailAddress;
                }

                return string.Empty;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public KxPerson Person
        {
            get
            {
                if (person == null)
                {
                    person = new KxPerson(this,personId);
                }
                return person;
            }
            set
            { person = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public KxStudentApplications Applications
        {
            get
            {
                if (applications == null)
                {
                    applications = new KxStudentApplications(this,KxStudentId);
                }
                return applications;
            }
            set
            { applications = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public KxStudentAllocations Allocations
        {
            get
            {
                if (allocations == null)
                {
                    allocations = new KxStudentAllocations(this, KxStudentId);
                }
                return allocations;
            }
            set
            { allocations = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public KxStudentAccount Account
        {
            get
            {
                if (account == null)
                {
                    account = new KxStudentAccount(this,KxStudentId);
                }
                return account;
            }
            set
            { account = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public KxStudentCourses Courses
        {
            get
            {
                if (courses == null)
                {
                    courses = new KxStudentCourses(this, KxStudentId);
                }
                return courses;
            }
            set
            { courses = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public KxStudentCustomDatas CustomData
        {
            get
            {
                if (customData == null && KxStudentId > 0)
                {
                    customData = new KxStudentCustomDatas(this, KxStudentId);
                }
                return customData;
            }
            set
            { customData = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public KxStudentAddress BestAddress
        {
            get
            {
                return GetBestAddress();
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public KxCustomFields2Values CustomFields
        {
            get
            {
                if (customFields == null && KxStudentId > 0)
                {
                    customFields = new KxCustomFields2Values(this,"ST2Student", KxStudentId);
                }
                return customFields;
            }
            set
            {
                if (customFields == value)
                    return;
                customFields = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public KxStudentStatus Status
        {
            get
            {
                if (status == null && studentStatusId > 0)
                {
                    status = new KxStudentStatus(this, studentStatusId);
                }
                return status;
            }
            set
            {
                status = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public KxDebtorStatus DebtorStatus
        {
            get
            {
                if (debtorStatus == null && debtorStatusId > 0)
                {
                    debtorStatus = new KxDebtorStatus(this, debtorStatusId);
                }
                return debtorStatus;
            }
            set
            {
                debtorStatus = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public bool CanDebtorApply
        {
            get
            {
                return GetCanDebtorApply();
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public bool IsCourseBasedFirstYear
        {
            get
            {
                return Courses.EarliestDate != null && Courses.EarliestDate.Value > DateTime.Now;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public bool IsCourseBasedReturner
        {
            get
            {
                return Courses.EarliestDate != null && Courses.EarliestDate.Value < DateTime.Now;
            }
        }

        #region IMergeObject Members

        private KxMergeTagProviderFactory _mergeTagProviderFactory;
        private KxMergeTagProviderFactory MergeTagProviderFactory
        {
            get
            {
                if (_mergeTagProviderFactory == null)
                {
                    _mergeTagProviderFactory = new KxMergeTagProviderFactory();
                }

                return _mergeTagProviderFactory;
            }
        }
        public KxMergeTagProvider MergeTags
        {
            get
            {
                return MergeTagProviderFactory.GetMergeTagProvider(KxMergeTagSetName.GetST2Students,
                        this.KxStudentId, 0);
            }
        }

        public void SnapshotMergeTags(bool reCreateExisting)
        {
            MergeTagProviderFactory.SnapshotMergeTags(KxMergeTagSetName.GetST2Students, this.KxStudentId, 0, reCreateExisting);
        }
        
        #endregion
        /// <summary>
        /// 
        /// </summary>
        public KxDepositBalances DepositBalances
        {
            get
            {
                if (KxStudentId > 0 && depositBalances == null)
                {
                    depositBalances = new KxDepositBalances(this, KxStudentId);
                }
                return depositBalances;
            }
          
        }


        #endregion 
        /// <summary>
        /// 
        /// </summary>
        public KxStudent(): base(null)
        {
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="parent"></param>
        public KxStudent(KxBaseClass parent) : base(parent)
        {
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="kxstudentid"></param>
        public KxStudent(KxBaseClass parent, int kxstudentid)
            : this(parent)
        {
             base.dBIdentifier = kxstudentid;
             loadType = StudentLoadType.KxStudentId;
             Load();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="_studentId"></param>
        /// <param name="_surName"></param>
        public KxStudent(KxBaseClass parent, string _studentId,string _surName) : this(parent)
        {
            studentId = _studentId;
            surName = _surName;
            loadType = StudentLoadType.Name;
            Load();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="studentId"></param>
        public KxStudent(KxBaseClass parent, string studentId) : this(parent)
        {
            this.studentId = studentId;
            loadType = StudentLoadType.StudentId;
            Load();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="kxstudentId"></param>
        public void Load(int kxstudentId)
        {
            dBIdentifier = kxstudentId;
            loadType = StudentLoadType.KxStudentId;
            Load();
        }

        public void LoadFromUDFValue(string value, string udfFieldName)
        {
            int kxStudentId = GetKxStudentIDFromUDFValue(value, udfFieldName);
            if (kxStudentId > 0)
            {
                Load(kxStudentId);
            }
        }

        private int GetKxStudentIDFromUDFValue(string value, string fieldName)
        {
            int result = 0;
            KxData data = new KxData("students", "dbo.WebGetKxStudentIDByUDFValue", TargetType.StoredProc, ActionType.ExecuteProcedure);
            data.AddParameter("UDFFieldName", fieldName, KxValueType.String, 50);
            data.AddParameter("UDFValue", value, KxValueType.String, 50);

            if (data.Execute())
            {
                XmlNodeList nodes = data.GetNodesFromData();
                if (nodes.Count != 1)
                {
                    KxLogger.LogActivity(this, "GetKxStudentIDFromUDFValue", string.Format("{0} students returned when looking for value {1} in UDF {2}", nodes.Count, value, fieldName), "");
                }
                else
                {
                    foreach (XmlNode node in nodes)
                    {
                        result = KxXmlUtils.GetNodeValue(node, "kxstudentid", 0);
                        break;
                    }
                }
            }
            else
            {
                data.LogException("KxWebUser", "GetKxStudentIDFromUDFValue", data.LastError, string.Format("Field Name: {0}, Field Value: {1}", fieldName, value));
            }

            return result;
        }

        // Stop the saving of student data 
        /*public override bool Save()
        {
            return true;
        }*/
        /// <summary>
        /// 
        /// </summary>
        /// <param name="actiontype"></param>
        /// <returns></returns>
        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("st2student", "ST2Students", TargetType.Table);

            if (loadType == StudentLoadType.Name && actiontype == ActionType.Select)
            {
                // used for group membership...
                result.KeyFields.Add(new KxDataValue("StudentId", studentId, KxValueType.String));
                result.KeyFields.Add(new KxDataValue("surName", surName, KxValueType.String));
            }
            else if (loadType == StudentLoadType.StudentId && actiontype == ActionType.Select)
            {
                result.KeyFields.Add(new KxDataValue("StudentId", studentId, KxValueType.String));
            }
            else
            {
                result.AutoKeyField = new KxDataValue("KxStudentId", KxStudentId, KxValueType.Int);
            }
            
            if ((actiontype == ActionType.Update) && (KxStudentId == 0))
            {
                result.Action = ActionType.Insert;
            }
            else
            {
                result.Action = actiontype;
            }

            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {
                result.AddParameter("studentId", studentId, KxValueType.String);
                result.AddParameter("ucasNumber", ucasNumber, KxValueType.String);
                result.AddParameter("title", title, KxValueType.String);
                result.AddParameter("foreName", foreName, KxValueType.String);
                result.AddParameter("surName", surName, KxValueType.String);
                result.AddParameter("midName", midName, KxValueType.String);
                result.AddParameter("preferredMethodOfContact", preferredMethodOfContact, KxValueType.String);
                result.AddParameter("disabilityId", disabilityId, KxValueType.Int);
                result.AddParameter("internationalStatus", internationalStatus, KxValueType.String);
                result.AddParameter("smoker", smoker, KxValueType.Boolean);
                result.AddParameter("ethnicityId", ethnicityId, KxValueType.Int);
                result.AddParameter("nationalityId", nationalityId, KxValueType.Int);
                result.AddParameter("dob", dob, KxValueType.DateTime);
                result.AddParameter("studentStatusId", studentStatusId, KxValueType.Int);
                result.AddParameter("creationDate", creationDate, KxValueType.DateTime);
                result.AddParameter("creationUserId", creationUserId, KxValueType.Int);
                result.AddParameter("personId", personId, KxValueType.Int);
                result.AddParameter("currentStudentAddressId", currentStudentAddressId, KxValueType.Int);
                result.AddParameter("gender", gender, KxValueType.String);
                result.AddParameter("homeStudentAddressId", homeStudentAddressId, KxValueType.Int);
                result.AddParameter("emergencyStudentAddressId", emergencyStudentAddressId, KxValueType.Int);
                result.AddParameter("webPassword", webPassword, KxValueType.String);
                result.AddParameter("webUserId", webUserId, KxValueType.String);
                result.AddParameter("webAccessAllowed", webAccessAllowed, KxValueType.Boolean);
                result.AddParameter("salt", salt, KxValueType.String);
                result.AddParameter("lockedAccountTime", lockedAccountTime, KxValueType.DateTime);
                result.AddParameter("preferredContactMethod", preferredContactMethod, KxValueType.Int);
                result.AddParameter("akaName", akaName, KxValueType.String);
                result.AddParameter("preferredEmailAddressId", preferredEmailAddressId, KxValueType.Int);
                result.AddParameter("debtorStatusId", debtorStatusId, KxValueType.Int);
                result.AddParameter("retryCount", retryCount, KxValueType.Int);
                result.AddParameter("identifierTypeId", identifierTypeId, KxValueType.Int);
                result.AddParameter("domicileId", domicileId, KxValueType.Int);
            }
            return result;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="xml"></param>
        /// <param name="node"></param>
        public override void LoadFromXml(string xml, XmlNode node)
        {
            Children.Remove(currentStudentAddress);
            Children.Remove(emergencyStudentAddress);
            Children.Remove(homeStudentAddress);
            Children.Remove(preferredEmailAddress);
            Children.Remove(person);
            Children.Remove(applications);
            Children.Remove(account);
            Children.Remove(courses);

            currentStudentAddress = null;
            emergencyStudentAddress  = null;
            homeStudentAddress  = null;
            preferredEmailAddress  = null;
            person = null;
            applications = null;
            account = null;
            courses = null;

            dBIdentifier = KxXmlUtils.GetNodeValue(node, "kxstudentid", 0);
            studentId = KxXmlUtils.GetNodeValue(node, "studentid", "");
            ucasNumber = KxXmlUtils.GetNodeValue(node, "ucasnumber", "");
            title = KxXmlUtils.GetNodeValue(node, "title", "");
            foreName = KxXmlUtils.GetNodeValue(node, "forename", "");
            surName = KxXmlUtils.GetNodeValue(node, "surname", "");
            midName = KxXmlUtils.GetNodeValue(node, "midname", "");
            preferredMethodOfContact = KxXmlUtils.GetNodeValue(node, "preferredmethodofcontact", "");
            disabilityId = KxXmlUtils.GetNodeValue(node, "disabilityid", 0);
            internationalStatus = KxXmlUtils.GetNodeValue(node, "internationalstatus", "");
            smoker = KxXmlUtils.GetNodeValue(node, "smoker", false);
            ethnicityId = KxXmlUtils.GetNodeValue(node, "ethnicityid", 0);
            nationalityId = KxXmlUtils.GetNodeValue(node, "nationalityid", 0);
            dob = KxXmlUtils.GetNodeValueDT(node, "dob", null);
            studentStatusId = KxXmlUtils.GetNodeValue(node, "studentstatusid", 0);
            creationDate = KxXmlUtils.GetNodeValueDT(node, "creationdate", null);
            creationUserId = KxXmlUtils.GetNodeValue(node, "creationuserid", 0);
            PersonId = KxXmlUtils.GetNodeValue(node, "personid", 0);
            currentStudentAddressId = KxXmlUtils.GetNodeValue(node, "currentstudentaddressid", 0);
            gender = KxXmlUtils.GetNodeValue(node, "gender", "");
            homeStudentAddressId = KxXmlUtils.GetNodeValue(node, "homestudentaddressid", 0);
            emergencyStudentAddressId = KxXmlUtils.GetNodeValue(node, "emergencystudentaddressid", 0);
            webPassword = KxXmlUtils.GetNodeValue(node, "webpassword", "");
            webUserId = KxXmlUtils.GetNodeValue(node, "webuserid", "");
            webAccessAllowed = KxXmlUtils.GetNodeValue(node, "webaccessallowed", false);
            salt = KxXmlUtils.GetNodeValue(node, "salt", "");
            lockedAccountTime = KxXmlUtils.GetNodeValueDT(node, "lockedaccounttime", null);
            preferredContactMethod = KxXmlUtils.GetNodeValue(node, "preferredcontactmethod", 0);
            akaName = KxXmlUtils.GetNodeValue(node, "akaname", "");
            preferredEmailAddressId = KxXmlUtils.GetNodeValue(node, "preferredemailaddressid", 0);
            debtorStatusId = KxXmlUtils.GetNodeValue(node, "debtorstatusid", 0);
            retryCount = KxXmlUtils.GetNodeValue(node, "retrycount", 0);
            identifierTypeId = KxXmlUtils.GetNodeValue(node, "identifiertypeid", 0);
            domicileId = KxXmlUtils.GetNodeValue(node, "domicileid", 0);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        protected KxStudentAddress GetBestAddress()
        {
            if (PreferredEmailAddress != null)
            {
                return PreferredEmailAddress;
            }
            else if (CurrentAddress != null)
            {
                return CurrentAddress;
            }
            else if (HomeAddress != null)
            {
                return HomeAddress;
            }
            else
            {
                return EmergencyAddress;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private string GetVerboseGender()
        {
            switch (gender)
            {
                case "M": return "Male";
                case "F" : return "Female";
                default : return "Unknown";
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="nationalityId"></param>
        /// <returns></returns>
        public static string GetNationality(int nationalityId)
        {
            string result = "";

            KxData data = new KxData("resource", "nationalities", TargetType.Table, ActionType.Select);

            data.KeyFields.Add(new KxDataValue("nationalityid", nationalityId, KxValueType.Int));

            if (data.Execute())
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(data.XmlResults);

                XmlNode node = doc.SelectSingleNode("//resource/datarow");

                result = KxXmlUtils.GetNodeValue(node, "description", "");
            }

            return result;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="disabilityId"></param>
        /// <returns></returns>
        public static string GetDisability(int disabilityId)
        {
            string result = "";

            KxData data = new KxData("resource", "disabilities", TargetType.Table, ActionType.Select);

            data.KeyFields.Add(new KxDataValue("disabilityid", disabilityId, KxValueType.Int));

            if (data.Execute())
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(data.XmlResults);

                XmlNode node = doc.SelectSingleNode("//resource/datarow");

                result = KxXmlUtils.GetNodeValue(node, "description", "");
            }

            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        public override string ToString()
        {
            return this.FullName + " (" + this.UcasNumber + ")";
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        protected bool GetCanDebtorApply()
        {
            bool result = true;

            if (DebtorStatus != null)
            {
                result = DebtorStatus.AllowApplication == 1;
            }
            return result;
        }
    }
}

