using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;

namespace Kx.Objects.Student
{
    [Serializable]
    public class KxControlStudent : KxBaseClass
    {
        #region Attributes & Properties

        protected string   description;
        protected string   controlValue;

        public int      ControlStudentId
        {
            get
            { return base.dBIdentifier;}
        }

        public string   Description
        {
            get
            { return description;}
            set
            { 
                AddToAudit("Description", description, value);
                description = value;
            }
        }

        public string   ControlValue
        {
            get
            { return controlValue;}
            set
            { 
                AddToAudit("ControlValue", controlValue, value);
                controlValue = value;
            }
        }

        public DateTime? ControlValueDateTime
        {
            get
            {
                try
                {
                    return DateTime.Parse(controlValue);
                }
                catch
                {
                    KxLogger.LogActivity(this, "ControlValueIsHistoric", this.description, "Not valid datetime value");
                    return null;
                }
            }
        }

        #endregion 


        public KxControlStudent(KxBaseClass parent):base(parent)
        {}


        public KxControlStudent(KxBaseClass parent, XmlNode node):base(parent)
        {
             LoadFromXml("", node);
        }

        public KxControlStudent(KxBaseClass parent, string description) : base(parent)
        {
            this.description = description;
            Load();
        }
        
        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("controlstudent", "ControlStudent", TargetType.Table);

            result.AddKeyField("description",description,KxValueType.String);
            
            result.Action = actiontype;
          
            return result;
        }
        
        public override  void LoadFromXml(string xml,XmlNode node)
        {
            dBIdentifier  = KxXmlUtils.GetNodeValue(node,"controlstudentid",0);
            description  = KxXmlUtils.GetNodeValue(node,"description","");
            controlValue  = KxXmlUtils.GetNodeValue(node,"controlvalue","");
        }
    }
}

