using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;

namespace Kx.Objects.Student
{
    [Serializable]
    public class KxDepositBalance : KxBaseClass
    {
        #region Attributes & Properties

        protected int      depositTypeId;
        protected string   description;
        protected decimal  depositBalance;

        public int      DepositTypeId
        {
            get
            { return depositTypeId;}
            set
            { 
                AddToAudit("DepositTypeId", depositTypeId, value);
                depositTypeId = value;
            }
        }

        public string   Description
        {
            get
            { return description;}
            set
            { 
                AddToAudit("Description", description, value);
                description = value;
            }
        }

        public decimal  DepositBalance
        {
            get
            { return depositBalance;}
            set
            { 
                AddToAudit("DepositBalance", depositBalance, value);
                depositBalance = value;
            }
        }


        #endregion 

        public KxDepositBalance(KxBaseClass parent, XmlNode node)
            : base(parent)
        {
            LoadFromXml("",node);
        }



        public override KxData ConfigureData(ActionType actiontype)
        {
 
            return null;
        }


        public override  void LoadFromXml(string xml,XmlNode node)
        {
            depositTypeId  = KxXmlUtils.GetNodeValue(node,"deposittypeid",0);
            description  = KxXmlUtils.GetNodeValue(node,"description","");
            depositBalance  = KxXmlUtils.GetNodeValue(node,"depositbalance",(decimal)0.00M);
        }
    }
}

