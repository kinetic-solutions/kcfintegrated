using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;

namespace Kx.Objects.Student
{
    public class KxStudentTermDate : KxBaseClass
    {
        #region attributes and properties
        protected int index = 0;
        protected DateTime ?licenceStartDate = null;
        protected DateTime ?licenceEndDate = null;
        protected DateTime ?termStartDate = null;
        protected DateTime ?termEndDate  = null;
        protected Decimal periodCharge = 0M;
        protected DateTime ?termPaymentDueDate = null;

        public int Index
        {
            get
            { return index; }
            set
            { index = value; }
        }

        public DateTime ?LicenceStartDate
        {
            get
            { return licenceStartDate;}
            set
            { licenceStartDate = value;}
        }

        public DateTime ?LicenceEndDate
        {
            get
            { return licenceEndDate;}
            set
            { licenceEndDate = value;}
        }

        public DateTime ?TermStartDate
        {
            get
            { return termStartDate;}
            set
            { termStartDate = value;}
        }

        public DateTime ?TermEndDate
        {
            get
            { return termEndDate;}
            set
            { termEndDate = value;}
        }

        public Decimal PeriodCharge
        {
            get
            { return periodCharge;}
            set
            { periodCharge = value;}
        }

        public DateTime ?TermPaymentDueDate
        {
            get
            { return termPaymentDueDate;}
            set
            { termPaymentDueDate = value;}
        }

        #endregion 

        public KxStudentTermDate(KxBaseClass parent,int idx,XmlNode node):base(parent)
        {
             index = idx;
             LoadFromXml("",node);
        }


        public override KxData ConfigureData(ActionType actiontype)
        {
            return null;
        }


        public override  void LoadFromXml(string xml,XmlNode node)
        {
            licenceStartDate  = KxXmlUtils.GetNodeValueDT(node,"licencestartdate",null);
            licenceEndDate  = KxXmlUtils.GetNodeValueDT(node,"licenceenddate",null);
            termStartDate  = KxXmlUtils.GetNodeValueDT(node,"termstartdate",null);
            termEndDate   = KxXmlUtils.GetNodeValueDT(node,"termenddate",null);
            periodCharge  = KxXmlUtils.GetNodeValue(node,"periodcharge",0.00M);
            termPaymentDueDate  = KxXmlUtils.GetNodeValueDT(node,"termpaymentduedate",null);
        }
    }
}
