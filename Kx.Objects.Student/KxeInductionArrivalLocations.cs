using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Exceptions;
using Kx.Core.Utility;
using Kx.Objects.Common;

namespace Kx.Objects.Student
{
    public class KxeInductionArrivalLocations : KxBaseClass
    {
        private List<KxeInductionArrivalLocation> items = new List<KxeInductionArrivalLocation>();

        public List<KxeInductionArrivalLocation> Items
        {
            get
            { return items; }
        }

        public KxeInductionArrivalLocation this[int index]
        {
            get
            { return items[index]; }
            set
            { items[index] = value; }
        }

        public KxeInductionArrivalLocations(KxBaseClass parent)
            : base(parent)
        {
            Load();
        }

        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = null;

            if (actiontype == ActionType.Select)
            {
                result = new KxData("st2einductionarrivallocations", "ST2eInductionArrivalLocations", TargetType.Table, ActionType.Select);

                //result.Parameters.Add(new KxDataValue("keyfield", keyfield, KxValueType.Int));
            }
            return result;
        }

        public override bool LoadFromXml(string xml, string xpath)
        {
            items.Clear();
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xml);

                XmlNodeList nodes = doc.SelectNodes(@"//results/" + xpath + "/datarow");

                foreach (XmlNode node in nodes)
                {
                    if (node != null)
                    {
                        KxeInductionArrivalLocation item = new KxeInductionArrivalLocation(this, node);
                        items.Add(item);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "LoadFromXml", ex.Message, xpath);
                LastError = ex.Message;
                return false;
            }
        }

        public KxeInductionArrivalLocation GeteInductionArrivalLocation(int id)
        {
            KxeInductionArrivalLocation result = null;

            foreach (KxeInductionArrivalLocation i in Items)
            {
                if (i.LocationId == id)
                {
                    result = i;
                    break;
                }
            }
            return result;
        }

        protected KxeInductionArrivalLocation GetLocationByBlock(int id, int AcademicYearId)
        {
            KxeInductionArrivalLocation result = null;

            foreach (KxeInductionArrivalLocation location in Items)
            {
                if (location.BlockId == id && location.AcademicYearId == AcademicYearId)
                {
                    result = location;
                    break;
                }
            }

            return result;
        }

        protected KxeInductionArrivalLocation GetLocationByArea(int id, int AcademicYearId)
        {
            KxeInductionArrivalLocation result = null;

            foreach (KxeInductionArrivalLocation location in Items)
            {
                if (location.AreaId == id && location.AcademicYearId == AcademicYearId)
                {
                    result = location;
                    break;
                }
            }
            return result;
        }

        protected KxeInductionArrivalLocation GetLocationBySite(int id, int AcademicYearId)
        {
            KxeInductionArrivalLocation result = null;

            foreach (KxeInductionArrivalLocation location in Items)
            {
                if (location.SiteId == id && location.AcademicYearId == AcademicYearId)
                {
                    result = location;
                    break;
                }
            }

            return result;
        }

        public int GetAllocationLocationId(KxStudentAllocation allocation)
        {
            int result = 0;

            KxeInductionArrivalLocation temp = GetLocationByBlock(allocation.ResidentialRoom.BlockId, allocation.Application.AcademicYearId);

            if (temp == null)
            {
                temp = GetLocationByArea(allocation.ResidentialRoom.Block.AreaId, allocation.Application.AcademicYearId);
            }

            if (temp == null)
            {
                temp = GetLocationBySite(allocation.ResidentialRoom.Block.SiteId, allocation.Application.AcademicYearId);
            }

            if (temp != null)
            {
                result = temp.LocationId;
            }

            return result;
        }

    }
}

