using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;

namespace Kx.Objects.Student
{
    [Serializable]
    public class KxStudentCharge : KxBaseClass
    {
        #region Attributes & Properties

        protected int      kxStudentId;
        protected int      applicationId;
        protected int      allocationDateId;
        protected int      studentChargeTypeId;
        protected int      siteId;
        protected int      areaId;
        protected DateTime ?creationDate;
        protected int      creationUserId;
        protected DateTime ?dateFrom;
        protected DateTime ?dateTo;
        protected string   statementText;
        protected decimal     amount;
        protected int vatId;
        protected decimal     totalAmount;
        protected bool     printOnStatement;
        protected int      invoiceNumber;
        protected int      exportBatchId;
        protected int      creditReasonId;
        protected int      roomDamageId;
        protected int      residentialRoomId;
        protected int      suppressFromInterface;
        protected int      linkedInvoiceNumber;
        protected int      period;
        protected string   lineType;
        protected int      taxFromAccountChargeId;
        protected int installmentNo;
        protected DateTime? _dueDate;

        protected KxStudentChargeType chargeType = null;
        protected KxVAT vat;


        public int      StudentChargeId
        {
            get
            { return base.dBIdentifier;}
        }

        public int      KxStudentId
        {
            get
            { return kxStudentId;}
            set
            { 
                AddToAudit("KxStudentId", kxStudentId, value);
                kxStudentId = value;
            }
        }

        public int      ApplicationId
        {
            get
            { return applicationId;}
            set
            { 
                AddToAudit("ApplicationId", applicationId, value);
                applicationId = value;
            }
        }

        public int      AllocationDateId
        {
            get
            { return allocationDateId;}
            set
            { 
                AddToAudit("AllocationDateId", allocationDateId, value);
                allocationDateId = value;
            }
        }

        public int      StudentChargeTypeId
        {
            get
            { return studentChargeTypeId;}
            set
            { 
                AddToAudit("StudentChargeTypeId", studentChargeTypeId, value);
                studentChargeTypeId = value;
            }
        }

        public int      SiteId
        {
            get
            { return siteId;}
            set
            { 
                AddToAudit("SiteId", siteId, value);
                siteId = value;
            }
        }

        public int      AreaId
        {
            get
            { return areaId;}
            set
            { 
                AddToAudit("AreaId", areaId, value);
                areaId = value;
            }
        }

        public DateTime ?CreationDate
        {
            get
            { return creationDate;}
            set
            { 
                AddToAudit("CreationDate", creationDate, value);
                creationDate = value;
            }
        }

        public int      CreationUserId
        {
            get
            { return creationUserId;}
            set
            { 
                AddToAudit("CreationUserId", creationUserId, value);
                creationUserId = value;
            }
        }

        public DateTime ?DateFrom
        {
            get
            { return dateFrom;}
            set
            { 
                AddToAudit("DateFrom", dateFrom, value);
                dateFrom = value;
            }
        }

        public DateTime ?DateTo
        {
            get
            { return dateTo;}
            set
            { 
                AddToAudit("DateTo", dateTo, value);
                dateTo = value;
            }
        }

        public string   StatementText
        {
            get
            { return statementText;}
            set
            { 
                AddToAudit("StatementText", statementText, value);
                statementText = value;
            }
        }

        public decimal     Amount
        {
            get
            { return amount;}
            set
            { 
                AddToAudit("Amount", amount, value);
                amount = value;
            }
        }

        public int      VatId
        {
            get
            { return vatId;}
            set
            {
                AddToAudit("VatId", vatId, value);
                vatId = value;
            }
        }

        public decimal     TotalAmount
        {
            get
            { return totalAmount;}
            set
            { 
                AddToAudit("TotalAmount", totalAmount, value);
                totalAmount = value;
            }
        }

        public bool     PrintOnStatement
        {
            get
            { return printOnStatement;}
            set
            { 
                AddToAudit("PrintOnStatement", printOnStatement, value);
                printOnStatement = value;
            }
        }

        public int      InvoiceNumber
        {
            get
            { return invoiceNumber;}
            set
            { 
                AddToAudit("InvoiceNumber", invoiceNumber, value);
                invoiceNumber = value;
            }
        }

        public int      ExportBatchId
        {
            get
            { return exportBatchId;}
            set
            { 
                AddToAudit("ExportBatchId", exportBatchId, value);
                exportBatchId = value;
            }
        }

        public int      CreditReasonId
        {
            get
            { return creditReasonId;}
            set
            { 
                AddToAudit("CreditReasonId", creditReasonId, value);
                creditReasonId = value;
            }
        }

        public int      RoomDamageId
        {
            get
            { return roomDamageId;}
            set
            { 
                AddToAudit("RoomDamageId", roomDamageId, value);
                roomDamageId = value;
            }
        }

        public int      ResidentialRoomId
        {
            get
            { return residentialRoomId;}
            set
            { 
                AddToAudit("ResidentialRoomId", residentialRoomId, value);
                residentialRoomId = value;
            }
        }

        public int      SuppressFromInterface
        {
            get
            { return suppressFromInterface;}
            set
            { 
                AddToAudit("SuppressFromInterface", suppressFromInterface, value);
                suppressFromInterface = value;
            }
        }

        public int      LinkedInvoiceNumber
        {
            get
            { return linkedInvoiceNumber;}
            set
            { 
                AddToAudit("LinkedInvoiceNumber", linkedInvoiceNumber, value);
                linkedInvoiceNumber = value;
            }
        }

        public int      Period
        {
            get
            { return period;}
            set
            { 
                AddToAudit("Period", period, value);
                period = value;
            }
        }

        public string   LineType
        {
            get
            { return lineType;}
            set
            { 
                AddToAudit("LineType", lineType, value);
                lineType = value;
            }
        }

        public int      TaxFromAccountChargeId
        {
            get
            { return taxFromAccountChargeId;}
            set
            { 
                AddToAudit("TaxFromAccountChargeId", taxFromAccountChargeId, value);
                taxFromAccountChargeId = value;
            }
        }

        public int InstallmentNo
        {
            get
            {
                return installmentNo;
            }
            set
            {
                AddToAudit("InstallmentNo", installmentNo, value);
                installmentNo = value;
            }
        }

        /// <summary>
        /// Due Date column is only written if in the specific version of the module (2010R2)
        /// If it isn't present then it won't be written and the due date will be reported as the
        /// DateFrom of the charge.
        /// </summary>
        public DateTime? DueDate
        {
            get
            {
                return _dueDate;
            }
            set
            {
            	AddToAudit("DueDate", _dueDate, value);
                _dueDate = value;
            }
        }

        public KxStudentChargeType ChargeType
        {
            get
            {
                if (chargeType == null && studentChargeTypeId > 0)
                {
                    chargeType = new KxStudentChargeType(this, studentChargeTypeId);
                }
                return chargeType;
            }
        }

        public KxVAT Vat
        {
            get
            {
                if (vat == null && vatId > 0)
                {
                    vat = new KxVAT(this, vatId);
                }
                return vat;
            }
         
        }

        private static string _hasDueDateColumn;
#if DEBUG
        public static void ResetHasDueDateColumn ()
        {
            _hasDueDateColumn = null;
        }
#endif


        #endregion 

        public KxStudentCharge(KxBaseClass parent) : base(parent)
        {
        }

        public KxStudentCharge(KxBaseClass parent, int studentchargeid)
            : this(parent)
        {
             base.dBIdentifier = studentchargeid;
             Load();
        }


        public KxStudentCharge(KxBaseClass parent, XmlNode node)
            : this(parent)
        {
            LoadFromXml("",node);
        }


        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("st2studentcharge", "ST2StudentCharges", TargetType.Table);

            result.AutoKeyField = new KxDataValue("StudentChargeId", StudentChargeId, KxValueType.Int);       

            if ((actiontype == ActionType.Update) && (StudentChargeId == 0))
            {
                result.Action = ActionType.Insert;
            }
            else
            {
                result.Action = actiontype;
            }

            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {
                result.AddParameter("kxStudentId", kxStudentId, KxValueType.Int);
                result.AddParameter("applicationId", applicationId, KxValueType.Int);
                result.AddParameter("allocationDateId", allocationDateId, KxValueType.Int);
                result.AddParameter("studentChargeTypeId", studentChargeTypeId, KxValueType.Int);
                result.AddParameter("siteId", siteId, KxValueType.Int);
                result.AddParameter("areaId", areaId, KxValueType.Int);
                result.AddParameter("creationDate", creationDate, KxValueType.DateTime);
                result.AddParameter("creationUserId", creationUserId, KxValueType.Int);
                result.AddParameter("dateFrom", dateFrom, KxValueType.DateTime);
                result.AddParameter("dateTo", dateTo, KxValueType.DateTime);
                result.AddParameter("statementText", statementText, KxValueType.String);
                result.AddParameter("amount", amount, KxValueType.Decimal);
                result.AddParameter("vatid", vatId, KxValueType.Int);
                result.AddParameter("totalAmount", totalAmount, KxValueType.Decimal);
                result.AddParameter("printOnStatement", printOnStatement, KxValueType.Boolean);
                result.AddParameter("invoiceNumber", invoiceNumber, KxValueType.Int);
                result.AddParameter("exportBatchId", exportBatchId, KxValueType.Int);
                result.AddParameter("creditReasonId", creditReasonId, KxValueType.Int);
                result.AddParameter("roomDamageId", roomDamageId, KxValueType.Int);
                result.AddParameter("residentialRoomId", residentialRoomId, KxValueType.Int);
                result.AddParameter("suppressFromInterface", suppressFromInterface, KxValueType.Int);
                result.AddParameter("linkedInvoiceNumber", linkedInvoiceNumber, KxValueType.Int);
                result.AddParameter("period", period, KxValueType.Int);
                result.AddParameter("lineType", lineType, KxValueType.String);
                result.AddParameter("taxFromAccountChargeId", taxFromAccountChargeId, KxValueType.Int);
                result.AddParameter("installmentNo", installmentNo, KxValueType.Int);

                const string STR_No = "No";
                const string STR_Yes = "Yes";
                try
                {
                    if (string.IsNullOrEmpty(_hasDueDateColumn))
                    {
                        KxData data = new KxData("dd", "SELECT TOP 1 DueDate FROM ST2StudentCharges", TargetType.Static);
                        if (data.Execute())
                        {
                            _hasDueDateColumn = STR_Yes;
                        }
                        else
                        {
                            _hasDueDateColumn = STR_No;
                        }
                    }
                }
                catch (Exception ex)
                {
                    KxLogger.LogActivity(this, "Finding DueDate column", ex.Message, "");
                    _hasDueDateColumn = STR_No;
                }

                if (_hasDueDateColumn == STR_Yes)
                {
                    result.AddParameter("duedate", _dueDate, KxValueType.DateTime);
                }
            }
            return result;
        }

        public override void LoadFromXml(string xml, XmlNode node)
        {
            dBIdentifier = KxXmlUtils.GetNodeValue(node, "studentchargeid", 0);
            kxStudentId = KxXmlUtils.GetNodeValue(node, "kxstudentid", 0);
            applicationId = KxXmlUtils.GetNodeValue(node, "applicationid", 0);
            allocationDateId = KxXmlUtils.GetNodeValue(node, "allocationdateid", 0);
            studentChargeTypeId = KxXmlUtils.GetNodeValue(node, "studentchargetypeid", 0);
            siteId = KxXmlUtils.GetNodeValue(node, "siteid", 0);
            areaId = KxXmlUtils.GetNodeValue(node, "areaid", 0);
            creationDate = KxXmlUtils.GetNodeValueDT(node, "creationdate", null);
            creationUserId = KxXmlUtils.GetNodeValue(node, "creationuserid", 0);
            dateFrom = KxXmlUtils.GetNodeValueDT(node, "datefrom", null);
            dateTo = KxXmlUtils.GetNodeValueDT(node, "dateto", null);
            statementText = KxXmlUtils.GetNodeValue(node, "statementtext", "");
            amount = KxXmlUtils.GetNodeValue(node, "amount", (decimal)0);
            vatId = KxXmlUtils.GetNodeValue(node, "vatid", 0);
            totalAmount = KxXmlUtils.GetNodeValue(node, "totalamount", (decimal)0);
            printOnStatement = KxXmlUtils.GetNodeValue(node, "printonstatement", false);
            invoiceNumber = KxXmlUtils.GetNodeValue(node, "invoicenumber", 0);
            exportBatchId = KxXmlUtils.GetNodeValue(node, "exportbatchid", 0);
            creditReasonId = KxXmlUtils.GetNodeValue(node, "creditreasonid", 0);
            roomDamageId = KxXmlUtils.GetNodeValue(node, "roomdamageid", 0);
            residentialRoomId = KxXmlUtils.GetNodeValue(node, "residentialroomid", 0);
            suppressFromInterface = KxXmlUtils.GetNodeValue(node, "suppressfrominterface", 0);
            linkedInvoiceNumber = KxXmlUtils.GetNodeValue(node, "linkedinvoicenumber", 0);
            period = KxXmlUtils.GetNodeValue(node, "period", 0);
            lineType = KxXmlUtils.GetNodeValue(node, "linetype", "");
            taxFromAccountChargeId = KxXmlUtils.GetNodeValue(node, "taxfromaccountchargeid", 0);
            installmentNo = KxXmlUtils.GetNodeValue(node, "installmentno", 0);
            _dueDate = KxXmlUtils.GetNodeValueDT(node, "duedate", dateFrom);
        }
    }
}

