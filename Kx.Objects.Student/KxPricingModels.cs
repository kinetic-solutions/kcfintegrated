using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Exceptions;
using Kx.Core.Utility;

namespace Kx.Objects.Student
{
    public class KxPricingModels : KxBaseClass
    {
        private SortedList<int, KxPricingModel> items = new SortedList<int,KxPricingModel>();

        public SortedList<int, KxPricingModel> Items
        {
            get
            { return items; }
        }

        public KxPricingModel this[int index]
        {
            get
            { return items[index]; }
            set
            { items[index] = value; }
        }

        public KxPricingModels(KxBaseClass parent) : base(parent)
        {
            Load();
        }


        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = null;

            if (actiontype == ActionType.Select)
            {
                result = new KxData("st2pricingmodels", "ST2PricingModels", TargetType.Table, ActionType.Select);
            }
            return result;
        }

        public override bool LoadFromXml(string xml, string xpath)
        {
            items.Clear();
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xml);

                XmlNodeList nodes = doc.SelectNodes(@"//results/" + xpath + "/datarow");

                foreach (XmlNode node in nodes)
                {
                    if (node != null)
                    {
                        KxPricingModel item = new KxPricingModel(this, node);
                        items.Add(item.PricingModelId,item);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "LoadFromXml", ex.Message, xpath);
                LastError = ex.Message;
                return false;
            }
        }

        public KxPricingModel GetPricingModel(int id)
        {
            KxPricingModel result = null;

            if (items.Keys.Contains(id))
            {
                result = items[id];
            }

            return result;
        }

    }
}

