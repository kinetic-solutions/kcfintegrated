using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;

namespace Kx.Objects.Student
{
    [Serializable]
    public class KxPaymentScheduleTypeModel : KxBaseClass
    {
        #region Attributes & Properties

        protected int      paymentScheduleTypeId;
        protected int      paymentCount;
        protected string   paymentIntervalType;
        protected DateTime ?fixedDate;
        protected int      fixedDay;
        protected int      weekDay;
        protected int      weekDayOfMonth;
        protected int      intervalFromArrival;
        protected int      intervalFromDeparture;
        protected bool     inactive;
        protected double   percentage;
        protected int      nbrOfMonths;

        protected decimal amountToCharge = 0.0M;

        public int      PaymentScheduleModelId
        {
            get
            { return base.dBIdentifier;}
        }

        public int      PaymentScheduleTypeId
        {
            get
            { return paymentScheduleTypeId;}
            set
            { 
                AddToAudit("PaymentScheduleTypeId", paymentScheduleTypeId, value);
                paymentScheduleTypeId = value;
            }
        }

        public int      PaymentCount
        {
            get
            { return paymentCount;}
            set
            { 
                AddToAudit("PaymentCount", paymentCount, value);
                paymentCount = value;
            }
        }

        public string   PaymentIntervalType
        {
            get
            { return paymentIntervalType;}
            set
            { 
                AddToAudit("PaymentIntervalType", paymentIntervalType, value);
                paymentIntervalType = value;
            }
        }

        public DateTime ?FixedDate
        {
            get
            { return fixedDate;}
            set
            { 
                AddToAudit("FixedDate", fixedDate, value);
                fixedDate = value;
            }
        }

        public int      FixedDay
        {
            get
            { return fixedDay;}
            set
            { 
                AddToAudit("FixedDay", fixedDay, value);
                fixedDay = value;
            }
        }

        public int      WeekDay
        {
            get
            { return weekDay;}
            set
            { 
                AddToAudit("WeekDay", weekDay, value);
                weekDay = value;
            }
        }

        public int      WeekDayOfMonth
        {
            get
            { return weekDayOfMonth;}
            set
            { 
                AddToAudit("WeekDayOfMonth", weekDayOfMonth, value);
                weekDayOfMonth = value;
            }
        }

        public int      IntervalFromArrival
        {
            get
            { return intervalFromArrival;}
            set
            { 
                AddToAudit("IntervalFromArrival", intervalFromArrival, value);
                intervalFromArrival = value;
            }
        }

        public int      IntervalFromDeparture
        {
            get
            { return intervalFromDeparture;}
            set
            { 
                AddToAudit("IntervalFromDeparture", intervalFromDeparture, value);
                intervalFromDeparture = value;
            }
        }

        public bool     Inactive
        {
            get
            { return inactive;}
            set
            { 
                AddToAudit("Inactive", inactive, value);
                inactive = value;
            }
        }

        public double   Percentage
        {
            get
            { return percentage;}
            set
            { 
                AddToAudit("Percentage", percentage, value);
                percentage = value;
            }
        }

        public decimal   AmountToCharge
        {
            get
            { return amountToCharge; }
            set
            { 
                amountToCharge = value;
            }
        }

        public int NbrOfMonths
        {
            get
            { return nbrOfMonths; }
            set
            {
                nbrOfMonths = value;
            }
        }

        #endregion 


        public KxPaymentScheduleTypeModel(KxBaseClass parent):base(parent)
        {}


        public KxPaymentScheduleTypeModel(KxBaseClass parent, XmlNode node):base(parent)
        {
             LoadFromXml("", node);
        }

        public KxPaymentScheduleTypeModel(KxBaseClass parent,int      paymentschedulemodelid):base(parent)
        {
             base.dBIdentifier = paymentschedulemodelid;
             Load();
        }


        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("st2paymentscheduletypemodel", "ST2PaymentScheduleTypeModel", TargetType.Table);

            result.AutoKeyField = new KxDataValue("PaymentScheduleModelId",PaymentScheduleModelId,KxValueType.Int);


            if ((actiontype == ActionType.Update) && (PaymentScheduleModelId == 0))
            {
                result.Action = ActionType.Insert;
            }
            else
            {
                result.Action = actiontype;
            }

            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {
                   result.AddParameter("paymentCount", paymentCount, KxValueType.Int, false);
                   result.AddParameter("paymentIntervalType", paymentIntervalType, KxValueType.String, 20, false);
                   result.AddParameter("fixedDate", fixedDate, KxValueType.DateTime, true);
                   result.AddParameter("fixedDay", fixedDay, KxValueType.Int, true);
                   result.AddParameter("weekDay", weekDay, KxValueType.Int, true);
                   result.AddParameter("weekDayOfMonth", weekDayOfMonth, KxValueType.Int, true);
                   result.AddParameter("intervalFromArrival", intervalFromArrival, KxValueType.Int, true);
                   result.AddParameter("intervalFromDeparture", intervalFromDeparture, KxValueType.Int, true);
                   result.AddParameter("inactive", inactive, KxValueType.Boolean, true);
                   result.AddParameter("percentage", percentage, KxValueType.Float, true);
            }
            return result;
        }


        public override  void LoadFromXml(string xml,XmlNode node)
        {
            dBIdentifier  = KxXmlUtils.GetNodeValue(node,"paymentschedulemodelid",0);
            paymentScheduleTypeId  = KxXmlUtils.GetNodeValue(node,"paymentscheduletypeid",0);
            paymentCount  = KxXmlUtils.GetNodeValue(node,"paymentcount",0);
            paymentIntervalType  = KxXmlUtils.GetNodeValue(node,"paymentintervaltype","");
            fixedDate  = KxXmlUtils.GetNodeValueDT(node,"fixeddate",null);
            fixedDay  = KxXmlUtils.GetNodeValue(node,"fixedday",0);
            weekDay  = KxXmlUtils.GetNodeValue(node,"weekday",0);
            weekDayOfMonth  = KxXmlUtils.GetNodeValue(node,"weekdayofmonth",0);
            intervalFromArrival  = KxXmlUtils.GetNodeValue(node,"intervalfromarrival",0);
            intervalFromDeparture  = KxXmlUtils.GetNodeValue(node,"intervalfromdeparture",0);
            inactive  = KxXmlUtils.GetNodeValue(node,"inactive",false);
            percentage  = KxXmlUtils.GetNodeValue(node,"percentage",0.0f);
        }
    }
}

