using System;
using System.Collections.Generic;
using System.Text;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Exceptions;
using Kx.Core.Utility;
using Kx.Objects.Student.Web;

namespace Kx.Objects.Student
{
    public class KxBaseStudentChargingModel : KxBaseClass
    {
        protected int studentChargeId1;
        protected int studentChargeId2;
        protected int studentChargeId3;
        protected int studentChargeId4;

        protected KxStudentChargeType studentCharge1;
        protected KxStudentChargeType studentCharge2;
        protected KxStudentChargeType studentCharge3;
        protected KxStudentChargeType studentCharge4;

        public Decimal AdditionalCharges
        {
            get
            {
                return GetAdditionalCharges();
            }
        }

        public int StudentChargeId1
        {
            get
            { return studentChargeId1; }
            set
            {
                AddToAudit("StudentChargeId1", studentChargeId1, value);
                studentChargeId1 = value;
            }
        }

        public int StudentChargeId2
        {
            get
            { return studentChargeId2; }
            set
            {
                AddToAudit("StudentChargeId2", studentChargeId2, value);
                studentChargeId2 = value;
            }
        }

        public int StudentChargeId3
        {
            get
            { return studentChargeId3; }
            set
            {
                AddToAudit("StudentChargeId3", studentChargeId3, value);
                studentChargeId3 = value;
            }
        }

        public int StudentChargeId4
        {
            get
            { return studentChargeId4; }
            set
            {
                AddToAudit("StudentChargeId4", studentChargeId4, value);
                studentChargeId4 = value;
            }
        }


        public KxStudentChargeType StudentCharge1
        {
            get
            {
                if (studentCharge1 == null && studentChargeId1 > 0)
                {
                    studentCharge1 = KxStudentObjectManager.StudentChargeTypes.GetStudentChargeTypeById(studentChargeId1);
                }
                return studentCharge1;
            }
        }
        public KxStudentChargeType StudentCharge2
        {
            get
            {
                if (studentCharge2 == null && studentChargeId2 > 0)
                {
                    studentCharge2 = KxStudentObjectManager.StudentChargeTypes.GetStudentChargeTypeById(studentChargeId2);
                }
                return studentCharge2;
            }
        }
        public KxStudentChargeType StudentCharge3
        {
            get
            {
                if (studentCharge3 == null && studentChargeId3 > 0)
                {
                    studentCharge3 = KxStudentObjectManager.StudentChargeTypes.GetStudentChargeTypeById(studentChargeId3);
                }
                return studentCharge3;
            }
        }
        public KxStudentChargeType StudentCharge4
        {
            get
            {
                if (studentCharge4 == null && studentChargeId4 > 0)
                {
                    studentCharge4 = KxStudentObjectManager.StudentChargeTypes.GetStudentChargeTypeById(studentChargeId4);
                }
                return studentCharge4;
            }
        }


        public KxBaseStudentChargingModel(KxBaseClass parent)
            : base(parent)
        {}

        private Decimal GetAdditionalCharges()
        {
            Decimal result = 0.0M;

            if (StudentCharge1 != null)
                result += StudentCharge1.DefaultCharge;

            if (StudentCharge2 != null)
                result += StudentCharge2.DefaultCharge;

            if (StudentCharge3 != null)
                result += StudentCharge3.DefaultCharge;

            if (StudentCharge4 != null)
                result += StudentCharge4.DefaultCharge;

            return result;
        }
    }
}
