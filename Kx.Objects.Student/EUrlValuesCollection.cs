﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace Kx.Objects.Student
{
    public class EUrlValuesCollection
    {
        private Dictionary<String,String> _eurlDictionary = new Dictionary<string,string>();

        public Dictionary<String, String> EurlDicationary
        {
            get
            {
                return _eurlDictionary;
            }
        }

        /// <summary>
        /// A non-case-sensative method of access items in the EUrl
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public string this[String key]
        {
            get
            {
                if (_eurlDictionary.ContainsKey(key.ToLower()))
                {
                    return _eurlDictionary[key.ToLower()];
                }
                else
                {
                    return "";
                }
            }
        }


        /// <summary>
        /// Holds a dictionary from the eURL passed into it providing an exception safe method of access items in the eURL.
        /// </summary>
        /// <param name="eUrl">The eURL</param>
        /// <param name="password">The Password to decrypt the eURL</param>
        public EUrlValuesCollection(String eUrl, String password)
        {
            //get the eURL
            string decrpytedEurl = Kx.Core.Security.KxWebSecurity.DecryptString(eUrl, password);

            //Split it down into the items...
            string[] eurlArray = decrpytedEurl.Split(new Char[] { '&' });

            
            foreach (String item in eurlArray)
            {
                //Split them again into Key/Value pairs...
                string[] eurlItem = item.Split(new Char[] { '=' });

                if (eurlItem.Length > 1) //Make sure we have the 2 items in the array!
                {
                    _eurlDictionary.Add(eurlItem[0].ToLower(), eurlItem[1]);
                }
            }
        }

    }
}
