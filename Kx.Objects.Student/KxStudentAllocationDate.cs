using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;

namespace Kx.Objects.Student
{
    
    [Serializable]
    public class KxStudentAllocationDate : KxBaseClass
    {
        #region Attributes & Properties

        protected int      allocationId;
        protected DateTime ?arrivalDate;
        protected DateTime ?departureDate;
        protected DateTime ?checkedInDate;
        protected DateTime ?checkedOutDate;
        protected int      earlyDepartureReasonId;
        protected int      earlyDepartureUserId;
        protected int      checkedInUserId;
        protected int      checkedOutUserId;
        protected DateTime ?auditDate;
        protected bool     selected;
        protected Decimal  allocationFees = 0.00M;
        protected Decimal accommodationCharges = 0.00M;
        protected Decimal ancillaryCharges = 0.00M;
        protected Decimal adjustedAllocationFees = 0.00M;

        protected KxStudentCharges charges = null;

        public int      AllocationDateId
        {
            get
            { return base.dBIdentifier;}
        }

        public int      AllocationId
        {
            get
            { return allocationId;}
            set
            { 
                AddToAudit("AllocationId", allocationId, value);
                allocationId = value;
            }
        }

        public DateTime ?ArrivalDate
        {
            get
            { return arrivalDate;}
            set
            { 
                AddToAudit("ArrivalDate", arrivalDate, value);
                arrivalDate = value;
            }
        }

        public DateTime ?ArrivalDatePlus10
        {
            get
            {
                try
                {
                    return ((DateTime)arrivalDate).AddDays(10);
                }
                catch
                { return arrivalDate; }
            }
        }

        public DateTime ?DepartureDate
        {
            get
            { return departureDate;}
            set
            { 
                AddToAudit("DepartureDate", departureDate, value);
                departureDate = value;
            }
        }

        public DateTime ?CheckedInDate
        {
            get
            { return checkedInDate;}
            set
            { 
                AddToAudit("CheckedInDate", checkedInDate, value);
                checkedInDate = value;
            }
        }

        public DateTime ?CheckedOutDate
        {
            get
            { return checkedOutDate;}
            set
            { 
                AddToAudit("CheckedOutDate", checkedOutDate, value);
                checkedOutDate = value;
            }
        }

        public int      EarlyDepartureReasonId
        {
            get
            { return earlyDepartureReasonId;}
            set
            { 
                AddToAudit("EarlyDepartureReasonId", earlyDepartureReasonId, value);
                earlyDepartureReasonId = value;
            }
        }

        public int      EarlyDepartureUserId
        {
            get
            { return earlyDepartureUserId;}
            set
            { 
                AddToAudit("EarlyDepartureUserId", earlyDepartureUserId, value);
                earlyDepartureUserId = value;
            }
        }

        public int      CheckedInUserId
        {
            get
            { return checkedInUserId;}
            set
            { 
                AddToAudit("CheckedInUserId", checkedInUserId, value);
                checkedInUserId = value;
            }
        }

        public int      CheckedOutUserId
        {
            get
            { return checkedOutUserId;}
            set
            { 
                AddToAudit("CheckedOutUserId", checkedOutUserId, value);
                checkedOutUserId = value;
            }
        }

        public DateTime ?AuditDate
        {
            get
            { return auditDate;}
            set
            { 
                AddToAudit("AuditDate", auditDate, value);
                auditDate = value;
            }
        }

        public bool     Selected
        {
            get
            { return selected;}
            set
            { 
                AddToAudit("Selected", selected, value);
                selected = value;
            }
        }

        public KxStudentCharges Charges
        {
            get
            {
                if (charges == null)
                {
                    charges = new KxStudentCharges(this, AllocationDateId, KxStudentChargingType.Allocation);
                }
                return charges;
            }
            set
            { charges = value; }
        }

        public Decimal AllocationFees
        {
            get
            {
                if (allocationFees == 0.00M)
                {
                    allocationFees = Charges.TotalCharges;
                }
                return allocationFees;
            }
            set
            { allocationFees = value; }
        }

        public Decimal AccommodationCharges
        {
            get
            {
                if (accommodationCharges == 0.00M)
                {
                    accommodationCharges = Charges.AccommodationCharges;
                }
                return accommodationCharges;
            }
            set
            { accommodationCharges = value; }
        }

        public Decimal AncillaryCharges
        {
            get
            {
                if (ancillaryCharges == 0.00M)
                {
                    ancillaryCharges = Charges.AncillaryCharges;
                }
                return ancillaryCharges;
            }
            set
            { ancillaryCharges = value; }
        }

        public Decimal AdjustedAllocationFees
        {
            get
            {
                if (adjustedAllocationFees == 0.00M)
                {
                    return AllocationFees;
                }
                else
                {
                    return adjustedAllocationFees;
                }
            }
            set
            { adjustedAllocationFees = value; }
        }
         
        #endregion 

        public KxStudentAllocationDate(KxBaseClass parent) : base(parent)
        {
        }

        public KxStudentAllocationDate(KxBaseClass parent, int allocationdateid)
            : this(parent)
        {
             base.dBIdentifier = allocationdateid;
             Load();
        }

        public KxStudentAllocationDate(KxBaseClass parent, XmlNode node)
            : base(parent)
        {
            LoadFromXml("",node);
        }


        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("st2studentallocationdate", "ST2StudentAllocationDates", TargetType.Table);

            result.AutoKeyField = new KxDataValue("AllocationDateId",AllocationDateId,KxValueType.Int);


            if ((actiontype == ActionType.Update) && (AllocationDateId == 0))
            {
                result.Action = ActionType.Insert;
            }
            else
            {
                result.Action = actiontype;
            }

            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {
                   result.AddParameter("allocationId", allocationId, KxValueType.Int);
                   result.AddParameter("arrivalDate", arrivalDate, KxValueType.DateTime);
                   result.AddParameter("departureDate", departureDate, KxValueType.DateTime);
                   result.AddParameter("checkedInDate", checkedInDate, KxValueType.DateTime);
                   result.AddParameter("checkedOutDate", checkedOutDate, KxValueType.DateTime);
                   result.AddParameter("earlyDepartureReasonId", earlyDepartureReasonId, KxValueType.Int);
                   result.AddParameter("earlyDepartureUserId", earlyDepartureUserId, KxValueType.Int);
                   result.AddParameter("checkedInUserId", checkedInUserId, KxValueType.Int);
                   result.AddParameter("checkedOutUserId", checkedOutUserId, KxValueType.Int);
                   result.AddParameter("auditDate", auditDate, KxValueType.DateTime);
                   result.AddParameter("selected", selected, KxValueType.Boolean);
            }
            return result;
        }

        public override void LoadFromXml(string xml, XmlNode node)
        {
            dBIdentifier = KxXmlUtils.GetNodeValue(node, "allocationdateid", 0);
            allocationId = KxXmlUtils.GetNodeValue(node, "allocationid", 0);
            arrivalDate = KxXmlUtils.GetNodeValueDT(node, "arrivaldate", null);
            departureDate = KxXmlUtils.GetNodeValueDT(node, "departuredate", null);
            checkedInDate = KxXmlUtils.GetNodeValueDT(node, "checkedindate", null);
            checkedOutDate = KxXmlUtils.GetNodeValueDT(node, "checkedoutdate", null);
            earlyDepartureReasonId = KxXmlUtils.GetNodeValue(node, "earlydeparturereasonid", 0);
            earlyDepartureUserId = KxXmlUtils.GetNodeValue(node, "earlydepartureuserid", 0);
            checkedInUserId = KxXmlUtils.GetNodeValue(node, "checkedinuserid", 0);
            checkedOutUserId = KxXmlUtils.GetNodeValue(node, "checkedoutuserid", 0);
            auditDate = KxXmlUtils.GetNodeValueDT(node, "auditdate", null);
            selected = KxXmlUtils.GetNodeValue(node, "selected", false);
        }
    }
}

