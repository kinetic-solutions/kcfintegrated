﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.IO;
using System.Web;
using System.Drawing;
using Kx.Core.Utility;

namespace Kx.Objects.Student
{
    public class KxStudentPhoto
    {
        public byte[] imageBytes { get; set; }
        public bool isPhotoUploaded { get; set; }
        public string PhotoFileName { get; set; }
        public string ActualPhotoFileName { get; set; }

        public string PhotoImageBase64
        {
            get {
                if (imageBytes == null)
                    return "";
                return KxBase64.Base64Encode(imageBytes).Replace("B64:", ""); 
           }
        }
        public KxStudentPhoto()
        {
            string studentId = HttpContext.Current.Session["StudentId"] == null ? "" : HttpContext.Current.Session["StudentId"].ToString();
            GetStudentPhoto(studentId);
        }

        public KxStudentPhoto(string studentId)
        {
            GetStudentPhoto(studentId);
        }

        public void GetStudentPhoto(string studentId)
        {
            try
            {
                string imageDir = ConfigurationManager.AppSettings["ImageLocation"] == null ? "" : ConfigurationManager.AppSettings["ImageLocation"].ToString();

                if (!string.IsNullOrEmpty(imageDir) && studentId != "") // image directory set and studentid exists student image with any extension like .jpg / png /bmp / jpeg 
                {
                    var files = Directory.GetFiles(imageDir, studentId + ".*");

                    if (files.Length > 0)
                    {
                        FileInfo fi = new FileInfo(files[0].ToString());

                        FileStream fs = new FileStream(imageDir + @"\" + studentId + fi.Extension, FileMode.Open, FileAccess.Read);

                        BinaryReader breader = new BinaryReader(fs);

                        imageBytes = breader.ReadBytes((Int32)fs.Length);

                        isPhotoUploaded = true;

                        if (PhotoFileName == null)
                        {
                            PhotoFileName = studentId + fi.Extension;
                        }
                        if (HttpContext.Current.Session["ActualPhotoFileName"] != null)
                        {
                            ActualPhotoFileName = HttpContext.Current.Session["ActualPhotoFileName"].ToString();
                        }

                        breader.Close();
                        fs.Close();

                    }
                    else
                    {
                        isPhotoUploaded = false;
                    }
                }

            }
            catch
            {
            }
        }
    }
}
