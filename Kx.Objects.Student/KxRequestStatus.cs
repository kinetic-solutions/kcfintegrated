using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;

namespace Kx.Objects.Student
{
    [Serializable]
    public class KxRequestStatus : KxBaseClass
    {
        #region Attributes & Properties

        protected string _description;
        protected int _creationUserId;
        protected DateTime? _creationDate;
        protected int _isClosed;

        public int RequestStatusId
        {
            get { return base.dBIdentifier; }
        }

        public string Description
        {
            get { return _description; }
            set
            { 
                AddToAudit("Description", _description, value);
                _description = value;
            }
        }

        public int CreationUserId
        {
            get { return _creationUserId; }
            set
            { 
                AddToAudit("CreationUserId", _creationUserId, value);
                _creationUserId = value;
            }
        }

        public DateTime? CreationDate
        {
            get { return _creationDate; }
            set
            { 
                AddToAudit("CreationDate", _creationDate, value);
                _creationDate = value;
            }
        }

        public int IsClosed
        {
            get { return _isClosed; }
            set
            { 
                AddToAudit("IsClosed", _isClosed, value);
                _isClosed = value;
            }
        }


        #endregion 


        public KxRequestStatus(KxBaseClass parent):base(parent)
        { }


        public KxRequestStatus(KxBaseClass parent, XmlNode node):base(parent)
        {
             LoadFromXml("", node);
        }

        public KxRequestStatus(KxBaseClass parent,int  requeststatusid):base(parent)
        {
             base.dBIdentifier = requeststatusid;
             Load();
        }


        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("st2requeststatuses", "ST2RequestStatuses", TargetType.Table);

            result.AutoKeyField = new KxDataValue("RequestStatusId",RequestStatusId,KxValueType.Int);


            if ((actiontype == ActionType.Update) && (RequestStatusId == 0))
            {
                result.Action = ActionType.Insert;
            }
            else
            {
                result.Action = actiontype;
            }

            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {
                result.AddParameter("description", _description, KxValueType.String, 50, false);
                result.AddParameter("creationUserId", _creationUserId, KxValueType.Int, false);
                result.AddParameter("creationDate", _creationDate, KxValueType.DateTime, false);
                result.AddParameter("isClosed", _isClosed, KxValueType.Int, false);
            }
            return result;
        }


        public override  void LoadFromXml(string xml,XmlNode node)
        {
            dBIdentifier  = KxXmlUtils.GetNodeValue(node,"requeststatusid",0);
            _description  = KxXmlUtils.GetNodeValue(node,"description",string.Empty);
            _creationUserId  = KxXmlUtils.GetNodeValue(node,"creationuserid",0);
            _creationDate  = KxXmlUtils.GetNodeValueDT(node,"creationdate",null);
            _isClosed  = KxXmlUtils.GetNodeValue(node,"isclosed",0);
        }

        public static IEnumerable<KxRequestStatus> GetST2RequestStatuses ()
        {
            KxData data = new KxData("st2requeststatuses", "ST2RequestStatuses", TargetType.Table);
            if (data.Execute ())
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(data.XmlResults);
                XmlNodeList nodes = doc.SelectNodes(@"//st2requeststatuses/datarow");
                foreach(XmlNode node in nodes)
                {
                     yield return new KxRequestStatus(null, node);
                }
            }
        }
    }
}

