using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Exceptions;
using Kx.Core.Utility;

namespace Kx.Objects.Student
{
    public class KxeInductionArrivalSlots : KxBaseClass
    {
        private List<KxeInductionArrivalSlot> items = new List<KxeInductionArrivalSlot>();

        protected KxeInductionArrivalLocation arrivalLocation = null;

        protected KxStudentAllocation _allocation = null;

        protected DateTime? _fromDate = null;

        protected KxeInductionArrivalSlot[,] slotArray = new KxeInductionArrivalSlot[30,24];

        public List<KxeInductionArrivalSlot> Items
        {
            get
            { return items; }
        }

        public KxeInductionArrivalSlot this[int index]
        {
            get
            { return items[index]; }
            set
            { items[index] = value; }
        }

        public KxeInductionArrivalLocation ArrivalLocation
        {
            get
            {
                return arrivalLocation;
            }
            set
            {
                arrivalLocation = value;
            }
        }

        public int XLength
        {
            get
            {
                return GetXArray().Length;
            }
        }

        public int YLength
        {
            get
            {
                return GetYArray().Length;
            }
        }

        public int[] YArrayIndices
        {
            get
            {
                return GetYArrayIndices();
            }
        }

        public KxeInductionArrivalSlots(KxeInductionArrivalLocation parent)
            : base(parent)
        {
            ArrivalLocation = parent;
            Load();
        }

        public KxeInductionArrivalSlots(KxeInductionArrivalLocation parent, KxStudentAllocation allocation)
            : base(parent)
        {
            ArrivalLocation = parent;
            _allocation = allocation;
            Load();
        }

        public KxeInductionArrivalSlots(KxeInductionArrivalLocation parent, DateTime fromDate)
            : base(parent)
        {
            ArrivalLocation = parent;
            _fromDate = fromDate;
            Load();
        }

        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = null;

            if (actiontype == ActionType.Select)
            {
                result = new KxData("st2einductionarrivalslots", "ST2eInductionArrivalSlots", TargetType.Table, ActionType.Select);

                result.KeyFields.Add(new KxDataValue("LocationId", arrivalLocation.LocationId, KxValueType.Int));
                //result.Parameters.Add(new KxDataValue("AcademicYearId", arrivalLocation.Allocation.Application.AcademicYearId, KxValueType.Int));
                if (_allocation != null)
                {
                    try
                    {
                        result.AddKeyField("arrivaldate", _allocation.AllocationStartDate.Date, KxValueType.DateTime);
                    }
                    catch
                    {}
                }

                result.AddOrderField("arrivalDate", KxDataOrderType.Ascending);
                result.AddOrderField("arrivalHourFrom", KxDataOrderType.Ascending);
                result.AddOrderField("arrivalHourTo", KxDataOrderType.Ascending);
            }
            return result;
        }

        public override bool LoadFromXml(string xml, string xpath)
        {
            items.Clear();
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xml);

                XmlNodeList nodes = doc.SelectNodes(@"//results/" + xpath + "/datarow");

                foreach (XmlNode node in nodes)
                {
                    if (node != null)
                    {
                        KxeInductionArrivalSlot item = new KxeInductionArrivalSlot(this, node);
                        if(!_fromDate.HasValue || _fromDate.HasValue && item.ArrivalDate >= _fromDate.Value)
                        {
                            items.Add(item);
                        }
                    }
                }
                Build2dArray();

                return true;
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "LoadFromXml", ex.Message, xpath);
                LastError = ex.Message;
                return false;
            }
        }

        public KxeInductionArrivalSlot GeteInductionArrivalSlot(int id)
        {
            KxeInductionArrivalSlot result = null;

            foreach (KxeInductionArrivalSlot i in Items)
            {
                if (i.ArrivalSlotId == id)
                {
                    result = i;
                    break;
                }
            }
            return result;
        }

        public KxeInductionArrivalSlot GeteInductionArrivalSlot(DateTime dte, string times)
        {
            KxeInductionArrivalSlot result = null;

            foreach (KxeInductionArrivalSlot i in Items)
            {
                if (i.TimesString == times && i.ArrivalDate.Value == dte)
                {
                    result = i;
                    break;
                }
            }
            return result;
        }

        public KxeInductionArrivalSlot GeteInductionArrivalSlot(int x,int y)
        {
            try
            {
                if (slotArray[x, y] != null)
                {
                    return slotArray[x, y];
                }
                else
                {
                    return new KxeInductionArrivalSlot(null);
                }
            }
            catch
            {
                return new KxeInductionArrivalSlot(null);
            }
        }

        public KxeInductionArrivalSlot GeteInductionArrivalColumnSlot(int x)
        {
            try
            {
                return GetXArray()[x];
            }
            catch
            {
                return null;
            }
        }

        public KxeInductionArrivalSlot GeteInductionArrivalRowSlot(int y)
        {
            try
            {
                return GetYArray()[y];
            }
            catch
            {
                return null;
            }
        }


        public KxeInductionArrivalSlot[] GetXArray()
        {
            SortedList<DateTime, KxeInductionArrivalSlot> result = new SortedList<DateTime, KxeInductionArrivalSlot>();

            foreach (KxeInductionArrivalSlot i in Items)
            {
                if (i.ArrivalDate != null && !result.ContainsKey(i.ArrivalDate.Value))
                {
                    result.Add(i.ArrivalDate.Value, i);
                }
            }

            KxeInductionArrivalSlot[] result2 = new KxeInductionArrivalSlot[result.Values.Count];

            result.Values.CopyTo(result2, 0);

            return result2;
        }

        public KxeInductionArrivalSlot[] GetYArray()
        {
            SortedList<string, KxeInductionArrivalSlot> result = new SortedList<string, KxeInductionArrivalSlot>();

            foreach (KxeInductionArrivalSlot i in Items)
            {
                if (!result.ContainsKey(i.TimesString))
                {
                    result.Add(i.TimesString, i);
                }
            }

            KxeInductionArrivalSlot[] result2 = new KxeInductionArrivalSlot[result.Values.Count];

            result.Values.CopyTo(result2, 0);

            return result2;
        }

        public void Build2dArray()
        {
            slotArray = new KxeInductionArrivalSlot[30, 24];

            KxeInductionArrivalSlot[] dates = GetXArray();
            KxeInductionArrivalSlot[] times = GetYArray();

            int width = dates.Length;
            int Height = times.Length;

            for (int x = 0; x < dates.Length; x++)
            {
                for (int y = 0; y < times.Length; y++)
                {
                    slotArray[x, y] = GeteInductionArrivalSlot(dates[x].ArrivalDate.Value, times[y].TimesString);
                }
            }

        }

        protected int[] GetYArrayIndices()
        {
            List<int> result = new List<int>();

            int count = 0;

            for (int i = 0; i < YLength; i++)
            {
                result.Add(count);
                count++;
            }

            return result.ToArray();
        }



    }
}

