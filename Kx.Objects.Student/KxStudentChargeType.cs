using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;

namespace Kx.Objects.Student
{
    [Serializable]
    public class KxStudentChargeType : KxBaseClass
    {
        #region Attributes & Properties

        protected string description;
        protected int suspenseNominalId;
        protected int incomeNominalId;
        protected bool refundable;
        protected string refundRules;
        protected bool inactive;
        protected string statementText;
        protected bool grouped;
        protected int vatid;
        protected decimal defaultCharge;
        protected bool massCharge;
        protected int allowPaymentPlan;
        protected string invoiceGroup;
        protected int extraInfo;
        protected string chargeGroup;

        public int StudentChargeTypeId
        {
            get
            { return base.dBIdentifier; }
        }

        public string Description
        {
            get
            { return description; }
            set
            {
                AddToAudit("Description", description, value);
                description = value;
            }
        }

        public int SuspenseNominalId
        {
            get
            { return suspenseNominalId; }
            set
            {
                AddToAudit("SuspenseNominalId", suspenseNominalId, value);
                suspenseNominalId = value;
            }
        }

        public int IncomeNominalId
        {
            get
            { return incomeNominalId; }
            set
            {
                AddToAudit("IncomeNominalId", incomeNominalId, value);
                incomeNominalId = value;
            }
        }

        public bool Refundable
        {
            get
            { return refundable; }
            set
            {
                AddToAudit("Refundable", refundable, value);
                refundable = value;
            }
        }

        public string RefundRules
        {
            get
            { return refundRules; }
            set
            {
                AddToAudit("RefundRules", refundRules, value);
                refundRules = value;
            }
        }

        public bool Inactive
        {
            get
            { return inactive; }
            set
            {
                AddToAudit("Inactive", inactive, value);
                inactive = value;
            }
        }

        public string StatementText
        {
            get
            { return statementText; }
            set
            {
                AddToAudit("StatementText", statementText, value);
                statementText = value;
            }
        }

        public bool Grouped
        {
            get
            { return grouped; }
            set
            {
                AddToAudit("Grouped", grouped, value);
                grouped = value;
            }
        }

        public int VatId
        {
            get
            { return vatid; }
            set
            {
                AddToAudit("VatId", vatid, value);
                vatid = value;
            }
        }

        public decimal DefaultCharge
        {
            get
            { return defaultCharge; }
            set
            {
                AddToAudit("DefaultCharge", defaultCharge, value);
                defaultCharge = value;
            }
        }

        public bool MassCharge
        {
            get
            { return massCharge; }
            set
            {
                AddToAudit("MassCharge", massCharge, value);
                massCharge = value;
            }
        }

        public int AllowPaymentPlan
        {
            get
            { return allowPaymentPlan; }
            set
            {
                AddToAudit("AllowPaymentPlan", allowPaymentPlan, value);
                allowPaymentPlan = value;
            }
        }

        public string InvoiceGroup
        {
            get
            { return invoiceGroup; }
            set
            {
                AddToAudit("InvoiceGroup", invoiceGroup, value);
                invoiceGroup = value;
            }
        }

        public int ExtraInfo
        {
            get
            { return extraInfo; }
            set
            {
                AddToAudit("ExtraInfo", extraInfo, value);
                extraInfo = value;
            }
        }

        public string ChargeGroup
        {
            get
            { return chargeGroup; }
            set
            {
                AddToAudit("ChargeGroup", chargeGroup, value);
                chargeGroup = value;
            }
        }

        public bool IsAccommodation
        {
            get
            {
                return ChargeGroup.ToUpper().Trim() == "ACCOMMODATION";
            }
        }


        #endregion


        public KxStudentChargeType(KxBaseClass parent)
            : base(parent)
        { }


        public KxStudentChargeType(KxBaseClass parent, XmlNode node)
            : base(parent)
        {
            LoadFromXml("", node);
        }

        public KxStudentChargeType(KxBaseClass parent, int studentchargetypeid)
            : base(parent)
        {
            base.dBIdentifier = studentchargetypeid;
            Load();
        }


        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("studentchargetypes", "StudentChargeTypes", TargetType.Table);

            result.AutoKeyField = new KxDataValue("StudentChargeTypeId", StudentChargeTypeId, KxValueType.Int);


            if ((actiontype == ActionType.Update) && (StudentChargeTypeId == 0))
            {
                result.Action = ActionType.Insert;
            }
            else
            {
                result.Action = actiontype;
            }

            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {
                result.AddParameter("suspenseNominalId", suspenseNominalId, KxValueType.Int, false);
                result.AddParameter("incomeNominalId", incomeNominalId, KxValueType.Int, false);
                result.AddParameter("refundable", refundable, KxValueType.Boolean, false);
                result.AddParameter("refundRules", refundRules, KxValueType.String, 2147483647, false);
                result.AddParameter("inactive", inactive, KxValueType.Boolean, false);
                result.AddParameter("statementText", statementText, KxValueType.String, 255, false);
                result.AddParameter("grouped", grouped, KxValueType.Boolean, false);
                result.AddParameter("vatid", vatid, KxValueType.Int, false);
                result.AddParameter("defaultCharge", defaultCharge, KxValueType.Decimal, true);
                result.AddParameter("massCharge", massCharge, KxValueType.Boolean, true);
                result.AddParameter("allowPaymentPlan", allowPaymentPlan, KxValueType.Int, false);
                result.AddParameter("invoiceGroup", invoiceGroup, KxValueType.String, 20, true);
                result.AddParameter("extraInfo", extraInfo, KxValueType.Int, false);
                result.AddParameter("chargeGroup", chargeGroup, KxValueType.String, 20, true);
            }
            return result;
        }


        public override void LoadFromXml(string xml, XmlNode node)
        {
            dBIdentifier = KxXmlUtils.GetNodeValue(node, "studentchargetypeid", 0);
            description = KxXmlUtils.GetNodeValue(node, "description", "");
            suspenseNominalId = KxXmlUtils.GetNodeValue(node, "suspensenominalid", 0);
            incomeNominalId = KxXmlUtils.GetNodeValue(node, "incomenominalid", 0);
            refundable = KxXmlUtils.GetNodeValue(node, "refundable", false);
            refundRules = KxXmlUtils.GetNodeValue(node, "refundrules", "");
            inactive = KxXmlUtils.GetNodeValue(node, "inactive", false);
            statementText = KxXmlUtils.GetNodeValue(node, "statementtext", "");
            grouped = KxXmlUtils.GetNodeValue(node, "grouped", false);
            vatid = KxXmlUtils.GetNodeValue(node, "vatid", 0);
            defaultCharge = KxXmlUtils.GetNodeValue(node, "defaultcharge", (decimal)0.00M);
            massCharge = KxXmlUtils.GetNodeValue(node, "masscharge", false);
            allowPaymentPlan = KxXmlUtils.GetNodeValue(node, "allowpaymentplan", 0);
            invoiceGroup = KxXmlUtils.GetNodeValue(node, "invoicegroup", "");
            extraInfo = KxXmlUtils.GetNodeValue(node, "extrainfo", 0);
            chargeGroup = KxXmlUtils.GetNodeValue(node, "chargegroup", "");
        }
    }
}

