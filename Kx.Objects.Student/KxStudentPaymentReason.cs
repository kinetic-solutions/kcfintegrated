using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;

namespace Kx.Objects.Student
{
    [Serializable]
    public class KxStudentPaymentReason : KxBaseClass
    {
        #region Attributes & Properties

        protected string   description;
        protected bool     inactive;
        protected bool     memoPayment;
        protected bool     enableQuestion;
        protected string   question;
        protected string   questionHint;
        protected int      signed;

        protected KxStudentDepositType depositType = null;

        public int      PaymentReasonId
        {
            get
            { return base.dBIdentifier;}
        }

        public string   Description
        {
            get
            { return description;}
            set
            { 
                AddToAudit("Description", description, value);
                description = value;
            }
        }

        public bool     Inactive
        {
            get
            { return inactive;}
            set
            { 
                AddToAudit("Inactive", inactive, value);
                inactive = value;
            }
        }

        public bool     MemoPayment
        {
            get
            { return memoPayment;}
            set
            { 
                AddToAudit("MemoPayment", memoPayment, value);
                memoPayment = value;
            }
        }

        public bool     EnableQuestion
        {
            get
            { return enableQuestion;}
            set
            { 
                AddToAudit("EnableQuestion", enableQuestion, value);
                enableQuestion = value;
            }
        }

        public string   Question
        {
            get
            { return question;}
            set
            { 
                AddToAudit("Question", question, value);
                question = value;
            }
        }

        public string   QuestionHint
        {
            get
            { return questionHint;}
            set
            { 
                AddToAudit("QuestionHint", questionHint, value);
                questionHint = value;
            }
        }

        public int      Signed
        {
            get
            { return signed;}
            set
            { 
                AddToAudit("Signed", signed, value);
                signed = value;
            }
        }

        public  KxStudentDepositType DepositType
        {
            get
            {
                if (depositType == null)
                {
                    depositType = new KxStudentDepositType(this, PaymentReasonId, KxDepositLoadType.Reason);
                }
                return depositType;
            }
        }
        
        public bool HasDeposit
        {
            get
            {
                try
                {
                    return DepositType.DepositTypeId > 0;
                }
                catch
                {
                    return false;
                }
            }
        }

        #endregion 



        public KxStudentPaymentReason(KxBaseClass parent,int      paymentreasonid):base(parent)
        {
             base.dBIdentifier = paymentreasonid;
             Load();
        }


        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("studentpaymentreasons", "StudentPaymentReasons", TargetType.Table);

            result.AutoKeyField = new KxDataValue("PaymentReasonId",PaymentReasonId,KxValueType.Int);


            if ((actiontype == ActionType.Update) && (PaymentReasonId == 0))
            {
                result.Action = ActionType.Insert;
            }
            else
            {
                result.Action = actiontype;
            }

            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {
                   result.AddParameter("description", description, KxValueType.String);
                   result.AddParameter("inactive", inactive, KxValueType.Boolean);
                   result.AddParameter("memoPayment", memoPayment, KxValueType.Boolean);
                   result.AddParameter("enableQuestion", enableQuestion, KxValueType.Boolean);
                   result.AddParameter("question", question, KxValueType.String);
                   result.AddParameter("questionHint", questionHint, KxValueType.String);
                   result.AddParameter("signed", signed, KxValueType.Int);
            }
            return result;
        }


        public override  void LoadFromXml(string xml,XmlNode node)
        {
            dBIdentifier  = KxXmlUtils.GetNodeValue(node,"paymentreasonid",0);
            description  = KxXmlUtils.GetNodeValue(node,"description","");
            inactive  = KxXmlUtils.GetNodeValue(node,"inactive",false);
            memoPayment  = KxXmlUtils.GetNodeValue(node,"memopayment",false);
            enableQuestion  = KxXmlUtils.GetNodeValue(node,"enablequestion",false);
            question  = KxXmlUtils.GetNodeValue(node,"question","");
            questionHint  = KxXmlUtils.GetNodeValue(node,"questionhint","");
            signed  = KxXmlUtils.GetNodeValue(node,"signed",0);
        }
    }
}

