﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Core.Configuration;
using Kx.Core.Exceptions;
using Kx.Core.Merge;
using Kx.Objects.Student.MergeTagProvider;
using Kx.Core.Data;
using Kx.Objects.Student.Web;

namespace Kx.Objects.Student.DataProviders
{
    public class KxMergeTagProviderFactory
    {
        private List<KxMergeTagProvider> _mergeTags = new List<KxMergeTagProvider>();
        public KxMergeTagProvider GetMergeTagProvider(KxMergeTagSetName setName, int kxStudentId, int allocationId)
        {
            KxMergeTagProvider result = null;
            if (GetParamValue(setName, kxStudentId, allocationId) > 0)
            {
                result = (from p in _mergeTags
                    where
                        (p.Name == setName.ToString() || p.MergeTagName == setName.ToString()) && p.ParameterValue is int &&
                        (int)p.ParameterValue == (GetParamValue(setName, kxStudentId, allocationId))
                    select p).FirstOrDefault();

                if (result == null)
                {
                    if (TagsExist(setName, kxStudentId, allocationId))
                    {
                        result = new KxSnapshotMergeTagProvider(setName, allocationId, kxStudentId);
                    }
                    else
                    {
                        result = new KxMergeTagProvider(setName.ToString(), GetParamName(setName),
                            GetParamValue(setName, kxStudentId, allocationId));
                    }
                    _mergeTags.Add(result);
                }

            }
            return result;
        }

        private int GetParamValue(KxMergeTagSetName setName, int kxStudentId, int allocationId)
        {
            switch (setName)
            {
                case KxMergeTagSetName.GetST2Students:
                    return kxStudentId;
                case KxMergeTagSetName.GetST2StudentAllocations:
                default:
                    return allocationId;

            }
        }

        private string GetParamName(KxMergeTagSetName setName)
        {
            switch (setName)
            {
                case KxMergeTagSetName.GetST2Students:
                    return "KXSTUDENTID";
                case KxMergeTagSetName.GetST2StudentAllocations:
                default:
                    return "ALLOCATIONID";

            }
        }

        private bool TagsExist(KxMergeTagSetName setName, int kxStudentId, int allocationId)
        {
            KxData data = new KxData("tags", string.Format("SELECT COUNT(*) AS Counter FROM kcf.mergetagsnapshot WHERE mergetagset = '{0}' AND allocationid = {1} AND kxstudentid = {2}", setName.ToString(), allocationId, kxStudentId), TargetType.Static);
            return ((int)data.ExecuteScalar()) > 0;
        }

        public void SnapshotMergeTags(KxMergeTagSetName setName, int kxStudentId, int allocationId,  bool reCreateExisting)
        {
            if (reCreateExisting && TagsExist(setName, kxStudentId, allocationId))
            {
                KxLogger.LogActivity(this, "SnapshotMergeTags", string.Format("Existing snapshotted merge tags removed from {0} for KxStudentID:{1}, AllocationID:{2}", setName, kxStudentId, allocationId), string.Empty);
                ClearExistingTags(setName, kxStudentId, allocationId);
            }

            var mergeTags = GetMergeTagProvider(setName, kxStudentId, allocationId);
            if (mergeTags is KxSnapshotMergeTagProvider && !reCreateExisting)
            {
                return;
            }
            else if (mergeTags is KxSnapshotMergeTagProvider && reCreateExisting)
            {
                _mergeTags.Remove(mergeTags);
                mergeTags = GetMergeTagProvider(setName, kxStudentId, allocationId);
            }

            if (!(mergeTags is KxSnapshotMergeTagProvider))
            {
                KxLogger.LogActivity(this, "SnapshotMergeTags", string.Format("Merge tags snapshotted from {0} for KxStudentID:{1}, AllocationID:{2}", setName, kxStudentId, allocationId), string.Empty);
                if (KxDbConfiguration.GetConfigurationBool("SnapshotMergetagsOnThread", false))
                {
                    ThreadedMergeTagSnapshot snapshot = new ThreadedMergeTagSnapshot(setName, kxStudentId, allocationId);
                    snapshot.SnapshotMergeTagsOnThread();
                }
                else
                {
                    foreach (string tag in mergeTags.MergeTagNames)
                    {
                        mergeTags.CurrentMergeTag = tag;

                        KxMergeTagSnapshot snapshot = new KxMergeTagSnapshot(null);
                        snapshot.MergeTagSet = setName.ToString();
                        snapshot.KxStudentId = kxStudentId;
                        snapshot.AllocationId = allocationId;
                        snapshot.CreationDate = DateTime.Now;
                        snapshot.MergeTag = tag;
                        snapshot.MergeData = mergeTags.MergeTag;
                        if (!snapshot.Save())
                        {
                            KxLogger.LogActivity(this, "SnapshotMergeTags",
                                string.Format(
                                    "Error saving merge tag {0} from {1} for KxStudentID:{2}, AllocationID:{3}", tag,
                                    setName, kxStudentId, allocationId), snapshot.LastError);
                        }
                    }
                }
            }
        }

        private void ClearExistingTags(KxMergeTagSetName setName, int kxStudentId, int allocationId)
        {
            KxData data = new KxData("tags", string.Format("DELETE FROM kcf.mergetagsnapshot WHERE mergetagset = '{0}' AND allocationid = {1} AND kxstudentid = {2}", setName.ToString(), allocationId, kxStudentId), TargetType.Static);
            data.Execute();
        }
    }
}
