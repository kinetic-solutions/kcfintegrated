﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Kx.Objects.Student.DataProviders
{
    public static class StudentCoreDataProvider
    {
        private static KxAcademicYears _academicYears = new KxAcademicYears(null);
        public static KxAcademicYears GetAcademicYears()
        {
            return _academicYears;
        }

        public static KxAcademicYear GetAcademicYear(int academicYearId)
        {
            return _academicYears.GetAcademicYear(academicYearId);
        }

        private static List<KxAcademicYearSession> _sessions = new List<KxAcademicYearSession>();

        public static KxAcademicYearSession GetSession(int academicYearId, int sessionId)
        {
            if (sessionId < 0)
            {
                sessionId = 0;
            }

            KxAcademicYearSession result = (from sess in _sessions
                where sess.AcademicYearId == academicYearId && sess.SessionId == sessionId
                select sess).FirstOrDefault();

            if (result == null)
            {
                if (sessionId > 0)
                {
                    result = new KxAcademicYearSession(null, sessionId);
                }
                else
                {
                    result = new KxAcademicYearCoreSession(GetAcademicYear(academicYearId));
                }
                _sessions.Add(result);
            }

            return result;
        }
    }
}
