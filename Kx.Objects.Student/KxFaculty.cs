using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;

namespace Kx.Objects.Student
{
    [Serializable]
    public class KxFaculty : KxBaseClass
    {
        #region Attributes & Properties

        protected string   description;
        protected string   code;
        protected int      importId;

        public int      FacultyId
        {
            get
            { return base.dBIdentifier;}
        }

        public string   Description
        {
            get
            { return description;}
            set
            { 
                AddToAudit("Description", description, value);
                description = value;
            }
        }

        public string   Code
        {
            get
            { return code;}
            set
            { 
                AddToAudit("Code", code, value);
                code = value;
            }
        }

        public int      ImportId
        {
            get
            { return importId;}
            set
            { 
                AddToAudit("ImportId", importId, value);
                importId = value;
            }
        }


        #endregion 

        public KxFaculty(KxBaseClass parent, int facultyid) : base(parent)
        {
             base.dBIdentifier = facultyid;
             Load();
        }


        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("faculties", "Faculties", TargetType.Table);

            result.AutoKeyField = new KxDataValue("FacultyId",FacultyId,KxValueType.Int);
            
            if ((actiontype == ActionType.Update) && (FacultyId == 0))
            {
                result.Action = ActionType.Insert;
            }
            else
            {
                result.Action = actiontype;
            }

            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {
                   result.AddParameter("description", description, KxValueType.String);
                   result.AddParameter("code", code, KxValueType.String);
                   result.AddParameter("importId", importId, KxValueType.Int);
            }
            return result;
        }


        public override void LoadFromXml(string xml,XmlNode node)
        {
            dBIdentifier = KxXmlUtils.GetNodeValue(node, "facultyid", 0);
            description  = KxXmlUtils.GetNodeValue(node,"description","");
            code  = KxXmlUtils.GetNodeValue(node,"code","");
            importId  = KxXmlUtils.GetNodeValue(node,"importid",0);
        }
    }
}

