using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;

namespace Kx.Objects.Student
{
    public enum KxDepositLoadType { Deposit, Reason };

    [Serializable]
    public class KxStudentDepositType : KxBaseClass
    {
        #region Attributes & Properties

        protected string   description;
        protected bool     prepayment;
        protected bool     inactive;
        protected int      paymentReasonId;

        protected KxDepositLoadType depositLoadType = KxDepositLoadType.Deposit;

        public int      DepositTypeId
        {
            get
            { return base.dBIdentifier;}
        }

        public string   Description
        {
            get
            { return description;}
            set
            { 
                AddToAudit("Description", description, value);
                description = value;
            }
        }

        public bool     Prepayment
        {
            get
            { return prepayment;}
            set
            { 
                AddToAudit("Prepayment", prepayment, value);
                prepayment = value;
            }
        }

        public bool     Inactive
        {
            get
            { return inactive;}
            set
            { 
                AddToAudit("Inactive", inactive, value);
                inactive = value;
            }
        }

        public int      PaymentReasonId
        {
            get
            { return paymentReasonId;}
            set
            { 
                AddToAudit("PaymentReasonId", paymentReasonId, value);
                paymentReasonId = value;
            }
        }
        
        #endregion 

        public KxStudentDepositType(KxBaseClass parent,int      identifier, KxDepositLoadType loadType ):base(parent)
        {
            depositLoadType = loadType;

            if (depositLoadType == KxDepositLoadType.Deposit)
            {
                base.dBIdentifier = identifier;
            }
            else
            {
                paymentReasonId = identifier;
            }

             
             Load();
        }


        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("studentdeposittypes", "StudentDepositTypes", TargetType.Table);

            if (depositLoadType == KxDepositLoadType.Deposit)
            {
                result.AutoKeyField = new KxDataValue("DepositTypeId",DepositTypeId,KxValueType.Int);
            }
            else
            {
                result.AutoKeyField = new KxDataValue("paymentreasonid",paymentReasonId,KxValueType.Int);
            }
                     
            if ((actiontype == ActionType.Update) && (DepositTypeId == 0))
            {
                result.Action = ActionType.Insert;
            }
            else
            {
                result.Action = actiontype;
            }

            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {
                   result.AddParameter("description", description, KxValueType.String);
                   result.AddParameter("prepayment", prepayment, KxValueType.Boolean);
                   result.AddParameter("inactive", inactive, KxValueType.Boolean);
                   result.AddParameter("paymentReasonId", paymentReasonId, KxValueType.Int);
            }
            return result;
        }
        
        public override  void LoadFromXml(string xml,XmlNode node)
        {
            dBIdentifier  = KxXmlUtils.GetNodeValue(node,"deposittypeid",0);
            description  = KxXmlUtils.GetNodeValue(node,"description","");
            prepayment  = KxXmlUtils.GetNodeValue(node,"prepayment",false);
            inactive  = KxXmlUtils.GetNodeValue(node,"inactive",false);
            paymentReasonId  = KxXmlUtils.GetNodeValue(node,"paymentreasonid",0);
        }
    }
}

