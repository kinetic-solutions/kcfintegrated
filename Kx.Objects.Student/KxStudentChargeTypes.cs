using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Exceptions;
using Kx.Core.Utility;

namespace Kx.Objects.Student
{

    public class KxStudentChargeTypes : KxBaseClass
    {

        private List<KxStudentChargeType> items = new List<KxStudentChargeType>();

    
        public List<KxStudentChargeType> Items
        {
            get
            {
                if (items == null)
                {
                    items = new List<KxStudentChargeType>();
                } 
                return items; }
        }

        public KxStudentChargeType this[int index]
        {
            get
            { return items[index]; }
            set
            { items[index] = value; }
        }

        public override KxData ConfigureData(ActionType actiontype)
        {
             KxData result = null;

             if (actiontype == ActionType.Select)
             {
                 result = new KxData("studentcharges", "ST2StudentCharges", TargetType.Table);
             }
            return result;
        }

        public KxStudentChargeTypes(KxBaseClass parent) : base(parent)
        {
           
        }

        public override bool LoadFromXml(string xml, string xpath)
        {
            items.Clear();
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xml);

                XmlNodeList nodes = doc.SelectNodes(@"//results/" + xpath + "/datarow");

                foreach (XmlNode node in nodes)
                {
                    if (node != null)
                    {
                        KxStudentChargeType app = new KxStudentChargeType(this, node);
                        Items.Add(app);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "LoadFromXml", ex.Message, xpath);
                LastError = ex.Message;
                return false;
            }
        }

        public KxStudentChargeType GetStudentChargeTypeById(int id)
        {
            KxStudentChargeType result = null;

            foreach (KxStudentChargeType charge in Items)
            {
                if (charge.StudentChargeTypeId == id)
                {
                    result = charge;
                    break;
                }
            }
            
            if (result == null)
            {
                KxStudentChargeType dummy = new KxStudentChargeType(this, id);

                if (dummy.StudentChargeTypeId == id)
                {
                    Items.Add(dummy);
                    result = dummy;
                }
            }


            return result;
        }
    }
}
