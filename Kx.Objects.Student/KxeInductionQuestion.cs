using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;

namespace Kx.Objects.Student
{
    [Serializable]
    public class KxeInductionQuestion : KxBaseClass
    {
        #region Attributes & Properties

        protected int      academicYearId;
        protected DateTime ?creationDate;
        protected int      creationUserId;
        protected string   question;
        protected string   answers;
        protected int      correctAnswerIndex;
        protected int questionIndex = 0;
        protected string webkey;
        public int      QuestionId
        {
            get
            { return base.dBIdentifier;}
        }

        /// <summary>
        /// Academic Year ID for the question (used with Webkey based Induction Questions)
        /// </summary>
        public int      AcademicYearId
        {
            get
            { return academicYearId;}
            set
            { 
                academicYearId = value;
            }
        }

        public DateTime ?CreationDate
        {
            get
            { return creationDate;}
            set
            { 
                AddToAudit("CreationDate", creationDate, value);
                creationDate = value;
            }
        }

        public int      CreationUserId
        {
            get
            { return creationUserId;}
            set
            { 
                AddToAudit("CreationUserId", creationUserId, value);
                creationUserId = value;
            }
        }

        public string   Question
        {
            get
            { return question;}
            set
            { 
                AddToAudit("Question", question, value);
                question = value;
            }
        }

        public int QuestionIndex
        {
            get
            {
                return questionIndex;
            }
            set
            {
                questionIndex = value;
            }
        }

        public string   Answers
        {
            get
            { return answers;}
            set
            { 
                AddToAudit("Answers", answers, value);
                answers = value;
            }
        }

        public string[] AnswersArray
        {
            get
            {
                return answers.Split(new string[] {"\r\n"}, StringSplitOptions.RemoveEmptyEntries);
            }
        }


        public int[] AnswerIndices
        {
            get
            {
                return GetAnswerIndices();
            }
        }

        public int      CorrectAnswerIndex
        {
            get
            { return correctAnswerIndex;}
            set
            { 
                AddToAudit("CorrectAnswerIndex", correctAnswerIndex, value);
                correctAnswerIndex = value;
            }
        }

        public string AnswerA
        {
            get
            {
                return GetAnswerById(0);
            }
        }

        public string AnswerB
        {
            get
            {
                return GetAnswerById(1);
            }
        }

        public string AnswerC
        {
            get
            {
                return GetAnswerById(2);
            }
        }

        public string AnswerD
        {
            get
            {
                return GetAnswerById(3);
            }
        }

        public string AnswerE
        {
            get
            {
                return GetAnswerById(4);
            }
        }

        public string AnswerF
        {
            get
            {
                return GetAnswerById(5);
            }
        }
        public string webKey
        {
            get
            { return webkey; }
            set
            {
                AddToAudit("webKey", webkey, value);
                webkey = value;
            }
        }
        #endregion 


        public KxeInductionQuestion(KxBaseClass parent):base(parent)
        {}


        public KxeInductionQuestion(KxBaseClass parent, XmlNode node):base(parent)
        {
             LoadFromXml("", node);
        }

        /// <summary>
        /// This is the original 'standard' Induction Question constructor, takes a hard coded question ID, used in sites up to 2021.
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="questionid">Question ID as in database</param>
        public KxeInductionQuestion(KxBaseClass parent, int questionid) : base(parent)
        {
            base.dBIdentifier = questionid;
            Load();
        }

        /// <summary>
        /// The 'new' Induction Question constructor, takes the webkey and academic year, used in sites from 2021 onwards. 
        /// NEEDS BACK OFFICE Kx2018 ONWARDS - Removes need for hardcoded Ids.
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="webkey">Web key from page</param>
        /// <param name="academicyearid">Academic year associated with question</param>
        public KxeInductionQuestion(KxBaseClass parent, string webkey, int academicyearid) : this(parent)
        {
            this.webKey = webkey;
            this.AcademicYearId = academicyearid;
            Load();
        }

        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("st2einductionquestions", "ST2eInductionQuestions", TargetType.Table);


            if (actiontype == ActionType.Select && webkey != null && academicYearId != 0)
            {
                result.AddKeyField("academicyearid", academicYearId, KxValueType.Int);
                result.AddKeyField("webkey", webKey, KxValueType.String);
            }
            else result.AutoKeyField = new KxDataValue("QuestionId",QuestionId,KxValueType.Int);
        

            if ((actiontype == ActionType.Update) && (QuestionId == 0) && (webkey == null))
            {
                result.Action = ActionType.Insert;
            }
            else
            {
                result.Action = actiontype;
            }

            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {
                   result.AddParameter("creationDate", creationDate, KxValueType.DateTime, true);
                   result.AddParameter("creationUserId", creationUserId, KxValueType.Int, true);
                   result.AddParameter("question", question, KxValueType.String, 2147483647, true);
                   result.AddParameter("answers", answers, KxValueType.String, 2147483647, true);
                   result.AddParameter("webkey", webkey, KxValueType.String, 2147483647, true);
                   result.AddParameter("correctAnswerIndex", correctAnswerIndex, KxValueType.Int, true);
            }
            return result;
        }


        public override  void LoadFromXml(string xml,XmlNode node)
        {
            dBIdentifier  = KxXmlUtils.GetNodeValue(node,"questionid",0);
            academicYearId  = KxXmlUtils.GetNodeValue(node,"academicyearid",0);
            creationDate  = KxXmlUtils.GetNodeValueDT(node,"creationdate",null);
            creationUserId  = KxXmlUtils.GetNodeValue(node,"creationuserid",0);
            question  = KxXmlUtils.GetNodeValue(node,"question","");
            answers  = KxXmlUtils.GetNodeValue(node,"answers","");
            webKey = KxXmlUtils.GetNodeValue(node, "webkey", "");
            correctAnswerIndex  = KxXmlUtils.GetNodeValue(node,"correctanswerindex",0);
        }

        private int[] GetAnswerIndices()
        {
            List<int> result = new List<int>();
            
            int count = 0;

            foreach (string s in AnswersArray)
            {
                result.Add(count);
                count++;
            }
            
            return result.ToArray();
        }

        public string GetAnswerById(int id)
        {
            string result = "";
            
            try
            {
                result = AnswersArray[id];
            }
            catch 
            { }

            return result;
        }

        public bool Validate(string guess)
        {
            return AnswersArray[CorrectAnswerIndex].ToUpper().Trim() == guess.ToUpper().Trim();
        }


    }
}

