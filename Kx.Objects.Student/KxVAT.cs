using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;

namespace Kx.Objects.Student
{
    [Serializable]
    public class KxVAT : KxBaseClass
    {
        #region Attributes & Properties

        protected string   description;
        protected double   rate;
        protected int      nominalId;
        protected string   status;
        protected int      taxModelId;
        protected string   rateType;
        protected int      orderOfApplication;
        protected bool     accumulates;
        protected decimal  appliesIfGreaterThan;
        protected decimal  appliesIfLessThan;
        protected bool     appliesToInvoice;

        public int      VatId
        {
            get
            { return base.dBIdentifier;}
        }

        public string   Description
        {
            get
            { return description;}
            set
            { 
                AddToAudit("Description", description, value);
                description = value;
            }
        }

        public double   Rate
        {
            get
            { return rate;}
            set
            { 
                AddToAudit("Rate", rate, value);
                rate = value;
            }
        }

        public int      NominalId
        {
            get
            { return nominalId;}
            set
            { 
                AddToAudit("NominalId", nominalId, value);
                nominalId = value;
            }
        }

        public string   Status
        {
            get
            { return status;}
            set
            { 
                AddToAudit("Status", status, value);
                status = value;
            }
        }

        public int      TaxModelId
        {
            get
            { return taxModelId;}
            set
            { 
                AddToAudit("TaxModelId", taxModelId, value);
                taxModelId = value;
            }
        }

        public string   RateType
        {
            get
            { return rateType;}
            set
            { 
                AddToAudit("RateType", rateType, value);
                rateType = value;
            }
        }

        public int      OrderOfApplication
        {
            get
            { return orderOfApplication;}
            set
            { 
                AddToAudit("OrderOfApplication", orderOfApplication, value);
                orderOfApplication = value;
            }
        }

        public bool     Accumulates
        {
            get
            { return accumulates;}
            set
            { 
                AddToAudit("Accumulates", accumulates, value);
                accumulates = value;
            }
        }

        public decimal  AppliesIfGreaterThan
        {
            get
            { return appliesIfGreaterThan;}
            set
            { 
                AddToAudit("AppliesIfGreaterThan", appliesIfGreaterThan, value);
                appliesIfGreaterThan = value;
            }
        }

        public decimal  AppliesIfLessThan
        {
            get
            { return appliesIfLessThan;}
            set
            { 
                AddToAudit("AppliesIfLessThan", appliesIfLessThan, value);
                appliesIfLessThan = value;
            }
        }

        public bool     AppliesToInvoice
        {
            get
            { return appliesToInvoice;}
            set
            { 
                AddToAudit("AppliesToInvoice", appliesToInvoice, value);
                appliesToInvoice = value;
            }
        }


        #endregion 


        public KxVAT(KxBaseClass parent):base(parent)
        {}


        public KxVAT(KxBaseClass parent, XmlNode node):base(parent)
        {
             LoadFromXml("", node);
        }

        public KxVAT(KxBaseClass parent,int      vatid):base(parent)
        {
             base.dBIdentifier = vatid;
             Load();
        }


        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("vat", "VAT", TargetType.Table);

            result.AutoKeyField = new KxDataValue("VatId",VatId,KxValueType.Int);


            if ((actiontype == ActionType.Update) && (VatId == 0))
            {
                result.Action = ActionType.Insert;
            }
            else
            {
                result.Action = actiontype;
            }

            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {
                   result.AddParameter("rate", rate, KxValueType.Float, false);
                   result.AddParameter("nominalId", nominalId, KxValueType.Int, true);
                   result.AddParameter("status", status, KxValueType.String, 10, false);
                   result.AddParameter("taxModelId", taxModelId, KxValueType.Int, true);
                   result.AddParameter("rateType", rateType, KxValueType.String, 10, true);
                   result.AddParameter("orderOfApplication", orderOfApplication, KxValueType.Int, true);
                   result.AddParameter("accumulates", accumulates, KxValueType.Boolean, true);
                   result.AddParameter("appliesIfGreaterThan", appliesIfGreaterThan, KxValueType.Decimal, true);
                   result.AddParameter("appliesIfLessThan", appliesIfLessThan, KxValueType.Decimal, true);
                   result.AddParameter("appliesToInvoice", appliesToInvoice, KxValueType.Boolean, true);
            }
            return result;
        }


        public override  void LoadFromXml(string xml,XmlNode node)
        {
            dBIdentifier  = KxXmlUtils.GetNodeValue(node,"vatid",0);
            description  = KxXmlUtils.GetNodeValue(node,"description","");
            rate  = KxXmlUtils.GetNodeValue(node,"rate",0.0f);
            nominalId  = KxXmlUtils.GetNodeValue(node,"nominalid",0);
            status  = KxXmlUtils.GetNodeValue(node,"status","");
            taxModelId  = KxXmlUtils.GetNodeValue(node,"taxmodelid",0);
            rateType  = KxXmlUtils.GetNodeValue(node,"ratetype","");
            orderOfApplication  = KxXmlUtils.GetNodeValue(node,"orderofapplication",0);
            accumulates  = KxXmlUtils.GetNodeValue(node,"accumulates",false);
            appliesIfGreaterThan  = KxXmlUtils.GetNodeValue(node,"appliesifgreaterthan",(decimal)0.00M);
            appliesIfLessThan  = KxXmlUtils.GetNodeValue(node,"appliesiflessthan",(decimal)0.00M);
            appliesToInvoice  = KxXmlUtils.GetNodeValue(node,"appliestoinvoice",false);
        }
    }
}

