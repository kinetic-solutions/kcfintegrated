using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;

namespace Kx.Objects.Student
{
    [Serializable]
    public class KxStudentStatement : KxBaseClass
    {
        #region Attributes & Properties

        protected string uniqueId;
        protected int      kxStudentId;
        protected int      academicYearId;
        protected int      studentChargeId;
        protected int      studentPaymentId;
        protected int      applicationId;
        protected int      allocationDateId;
        protected DateTime ?dateFrom;
        protected DateTime ?dateTo;
        protected DateTime? dateDue;
        protected int      nights;
        protected string   statementText;
        protected decimal  totalAmount;
        protected string   siteName;
        protected string   areaName;
        protected string   roomName;
        protected int      studentChargeTypeId;
        protected string   chargeDescription;
        protected int documentNumber;
        protected int      paymentReasonId;
        protected string   paymentReasonDescription;
        protected int      paymentMethodId;
        protected string   paymentMethodDescription;
        protected int      depositTypeId;
        protected Decimal fromCharge;
        protected Decimal fromPayment;
        protected Decimal toCharge;
        protected Decimal toPayment;
        protected bool _pendingBankTransfers;

        protected bool selected = false;

        protected decimal allocatedAmount = 0.00M;
        protected decimal unallocatedBalance = 0.00M;

        public bool Selected
        {
            get
            {
                return selected;
            }
            set
            {
                selected = value;
            }
        }

        public KxStudentStatements Statements
        {
            get
            {
                return (KxStudentStatements)Parent;
            }
        }

        public bool IsCharge
        {
            get
            {
                return StudentChargeId > 0;
            }
        }

        public bool IsPayment
        {
            get
            {
                return StudentPaymentId > 0;
            }
        }

        public bool IsDeposit
        {
            get
            {
                return DepositTypeId > 0;
            }
        }

        public string UniqueId
        {
            get
            { return uniqueId; }
            set
            {
                AddToAudit("UniqueId", uniqueId, value);
                uniqueId = value;
            }
        }

        public int      KxStudentId
        {
            get
            { return kxStudentId;}
            set
            { 
                AddToAudit("KxStudentId", kxStudentId, value);
                kxStudentId = value;
            }
        }

        public int AcademicYearId
        {
            get
            { return academicYearId; }
            set
            {
                AddToAudit("AcademicYearId", academicYearId, value);
                academicYearId = value;
            }
        }

        public int      StudentChargeId
        {
            get
            { return studentChargeId;}
            set
            { 
                AddToAudit("StudentChargeId", studentChargeId, value);
                studentChargeId = value;
            }
        }

        public int      StudentPaymentId
        {
            get
            { return studentPaymentId;}
            set
            { 
                AddToAudit("StudentPaymentId", studentPaymentId, value);
                studentPaymentId = value;
            }
        }

        public int      ApplicationId
        {
            get
            { return applicationId;}
            set
            { 
                AddToAudit("ApplicationId", applicationId, value);
                applicationId = value;
            }
        }

        public int      AllocationDateId
        {
            get
            { return allocationDateId;}
            set
            { 
                AddToAudit("AllocationDateId", allocationDateId, value);
                allocationDateId = value;
            }
        }

        public DateTime ?DateFrom
        {
            get
            { return dateFrom;}
            set
            { 
                AddToAudit("DateFrom", dateFrom, value);
                dateFrom = value;
            }
        }

        public DateTime ?DateTo
        {
            get
            { return dateTo;}
            set
            { 
                AddToAudit("DateTo", dateTo, value);
                dateTo = value;
            }
        }

        public DateTime? DateDue
        {
            get
            { return dateDue; }
            set
            {
                AddToAudit("DateDue", dateDue, value);
                dateDue = value;
            }
        }

        public int      Nights
        {
            get
            { return nights;}
            set
            { 
                AddToAudit("Nights", nights, value);
                nights = value;
            }
        }

        public string   StatementText
        {
            get
            { return statementText;}
            set
            { 
                AddToAudit("StatementText", statementText, value);
                statementText = value;
            }
        }

        public decimal  TotalAmount
        {
            get
            { return totalAmount;}
            set
            { 
                AddToAudit("TotalAmount", totalAmount, value);
                totalAmount = value;
            }
        }

        public string   SiteName
        {
            get
            { return siteName;}
            set
            { 
                AddToAudit("SiteName", siteName, value);
                siteName = value;
            }
        }

        public string   AreaName
        {
            get
            { return areaName;}
            set
            { 
                AddToAudit("AreaName", areaName, value);
                areaName = value;
            }
        }

        public string   RoomName
        {
            get
            { return roomName;}
            set
            { 
                AddToAudit("RoomName", roomName, value);
                roomName = value;
            }
        }

        public int      StudentChargeTypeId
        {
            get
            { return studentChargeTypeId;}
            set
            { 
                AddToAudit("StudentChargeTypeId", studentChargeTypeId, value);
                studentChargeTypeId = value;
            }
        }

        public string   ChargeDescription
        {
            get
            { return chargeDescription;}
            set
            { 
                AddToAudit("ChargeDescription", chargeDescription, value);
                chargeDescription = value;
            }
        }

        public int      DocumentNumber
        {
            get
            { return documentNumber; }
            set
            {
                AddToAudit("DocumentNumber", documentNumber, value);
                documentNumber = value;
            }
        }

        public string DocumentNumberString
        {
            get
            {
                if (DocumentNumber == 0)
                {
                    return "";
                }
                else
                {
                    return DocumentNumber.ToString();
                }
            }
            
        }

        public int      PaymentReasonId
        {
            get
            { return paymentReasonId;}
            set
            { 
                AddToAudit("PaymentReasonId", paymentReasonId, value);
                paymentReasonId = value;
            }
        }

        public string   PaymentReasonDescription
        {
            get
            { return paymentReasonDescription;}
            set
            { 
                AddToAudit("PaymentReasonDescription", paymentReasonDescription, value);
                paymentReasonDescription = value;
            }
        }

        public int      PaymentMethodId
        {
            get
            { return paymentMethodId;}
            set
            { 
                AddToAudit("PaymentMethodId", paymentMethodId, value);
                paymentMethodId = value;
            }
        }

        public string   PaymentMethodDescription
        {
            get
            { return paymentMethodDescription;}
            set
            { 
                AddToAudit("PaymentMethodDescription", paymentMethodDescription, value);
                paymentMethodDescription = value;
            }
        }

        public int      DepositTypeId
        {
            get
            { return depositTypeId;}
            set
            { 
                AddToAudit("DepositTypeId", depositTypeId, value);
                depositTypeId = value;
            }
        }

        public Decimal FromCharge
        {
            get
            {
                return fromCharge;
            }
            set
            {
                fromCharge = value;
            }
        }
        public Decimal FromPayment
        {
            get
            {
                return fromPayment;
            }
            set
            {
                fromPayment = value;
            }
        }

        public Decimal ToCharge
        {
            get
            {
                return toCharge;
            }
            set
            {
                toCharge = value;
            }
        }
        public Decimal ToPayment
        {
            get
            {
                return toPayment;
            }
            set
            {
                toPayment = value;
            }
        }

        public string LineType
        {
            get
            {
                if (IsCharge)
                {
                    if (TotalAmount < 0)
                    {
                        return "Credit";
                    }
                    else
                    {
                        return "Charge";
                    }
                }
                else
                {
                    return "Payment";
                }
            }
        }

        public int KxReference
        {
            get
            {
                if (IsCharge)
                {
                    return StudentChargeId;
                }
                else
                {
                    return StudentPaymentId;
                }
            }
        }


        public decimal AllocatedAmount
        {
            get
            {
                if (allocatedAmount == 0.00M)
                {
                    DetermineBalances();
                }
                return allocatedAmount;
            }
            set
            {
                allocatedAmount = value;
            }
        }

        public Decimal OutStandingBalance
        {
            get
            {
                if (IsCharge)
                {
                    return UnallocatedBalance;
                }
                else
                {
                    return 0.00M;
                }
            }
        }


        protected decimal UnallocatedBalance
        {
            get
            {
                if (unallocatedBalance == 0.00M)
                {
                    DetermineBalances();
                }
                return unallocatedBalance;
            }
            set
            {
                unallocatedBalance = value;
            }
        }

        public bool IsUnallocated
        {
            get
            {
                return unallocatedBalance > 0;
            }
        }

        public bool IsPayable
        {
            get
            {
                return IsCharge && unallocatedBalance > 0 && DocumentNumber > 0;            
            }
        }

        public bool PendingBankTransfers
        {
            get { return _pendingBankTransfers; }
        }

        #endregion 

        public KxStudentStatement(KxBaseClass parent)
            : base(parent)
        {
        }

        public KxStudentStatement(KxBaseClass parent, XmlNode node)
            : this(parent)
        {
            LoadFromXml("",node);
        }

        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("studentstatement", "StudentStatement", TargetType.Table);


            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {
                   result.AddParameter("uniqueid", uniqueId, KxValueType.String, 50, true);
                   result.AddParameter("studentChargeId", studentChargeId, KxValueType.Int, false);
                   result.AddParameter("studentPaymentId", studentPaymentId, KxValueType.Int, false);
                   result.AddParameter("applicationId", applicationId, KxValueType.Int, true);
                   result.AddParameter("allocationDateId", allocationDateId, KxValueType.Int, true);
                   result.AddParameter("dateFrom", dateFrom, KxValueType.DateTime, false);
                   result.AddParameter("dateTo", dateTo, KxValueType.DateTime, false);
                   result.AddParameter("dueDate", dateDue, KxValueType.DateTime, false);
                   result.AddParameter("nights", nights, KxValueType.Int, true);
                   result.AddParameter("statementText", statementText, KxValueType.String, 255, true);
                   result.AddParameter("totalAmount", totalAmount, KxValueType.Decimal, true);
                   result.AddParameter("siteName", siteName, KxValueType.String, 50, false);
                   result.AddParameter("areaName", areaName, KxValueType.String, 40, false);
                   result.AddParameter("roomName", roomName, KxValueType.String, 40, false);
                   result.AddParameter("studentChargeTypeId", studentChargeTypeId, KxValueType.Int, false);
                   result.AddParameter("chargeDescription", chargeDescription, KxValueType.String, 50, false);
                   result.AddParameter("documentNumber", documentNumber, KxValueType.Int, false);
                   result.AddParameter("paymentReasonId", paymentReasonId, KxValueType.Int, false);
                   result.AddParameter("paymentReasonDescription", paymentReasonDescription, KxValueType.String, 50, false);
                   result.AddParameter("paymentMethodId", paymentMethodId, KxValueType.Int, false);
                   result.AddParameter("paymentMethodDescription", paymentMethodDescription, KxValueType.String, 40, false);
                   result.AddParameter("depositTypeId", depositTypeId, KxValueType.Int, false);
                   result.AddParameter("fromPayment", FromPayment, KxValueType.Decimal, true);
                   result.AddParameter("fromCharge", FromCharge, KxValueType.Decimal, true);
                   result.AddParameter("toPayment", toPayment, KxValueType.Decimal, true);
                   result.AddParameter("toCharge", toCharge, KxValueType.Decimal, true);
            }
            return result;
        }


        public override  void LoadFromXml(string xml,XmlNode node)
        {
            uniqueId = KxXmlUtils.GetNodeValue(node, "uniqueid", "");
            kxStudentId  = KxXmlUtils.GetNodeValue(node,"kxstudentid",0);
            academicYearId = KxXmlUtils.GetNodeValue(node, "academicyearid", 0);
            studentChargeId  = KxXmlUtils.GetNodeValue(node,"studentchargeid",0);
            studentPaymentId  = KxXmlUtils.GetNodeValue(node,"studentpaymentid",0);
            applicationId  = KxXmlUtils.GetNodeValue(node,"applicationid",0);
            allocationDateId  = KxXmlUtils.GetNodeValue(node,"allocationdateid",0);
            dateFrom  = KxXmlUtils.GetNodeValueDT(node,"datefrom",null);
            dateTo  = KxXmlUtils.GetNodeValueDT(node,"dateto",null);
            dateDue = KxXmlUtils.GetNodeValueDT(node, "duedate", null);
            nights  = KxXmlUtils.GetNodeValue(node,"nights",0);
            statementText  = KxXmlUtils.GetNodeValue(node,"statementtext","");
            totalAmount  = KxXmlUtils.GetNodeValue(node,"totalamount",(decimal)0.00M);
            siteName  = KxXmlUtils.GetNodeValue(node,"sitename","");
            areaName  = KxXmlUtils.GetNodeValue(node,"areaname","");
            roomName  = KxXmlUtils.GetNodeValue(node,"roomname","");
            studentChargeTypeId  = KxXmlUtils.GetNodeValue(node,"studentchargetypeid",0);
            chargeDescription  = KxXmlUtils.GetNodeValue(node,"chargedescription","");
            documentNumber = KxXmlUtils.GetNodeValue(node, "documentnumber", 0);
            paymentReasonId  = KxXmlUtils.GetNodeValue(node,"paymentreasonid",0);
            paymentReasonDescription  = KxXmlUtils.GetNodeValue(node,"paymentreasondescription","");
            paymentMethodId  = KxXmlUtils.GetNodeValue(node,"paymentmethodid",0);
            paymentMethodDescription  = KxXmlUtils.GetNodeValue(node,"paymentmethoddescription","");
            depositTypeId  = KxXmlUtils.GetNodeValue(node,"deposittypeid",0);
            FromPayment = KxXmlUtils.GetNodeValue(node, "frompayment", (decimal)0.00M);
            FromCharge = KxXmlUtils.GetNodeValue(node, "fromcharge", (decimal)0.00M);
            toPayment = KxXmlUtils.GetNodeValue(node, "topayment", (decimal)0.00M);
            toCharge = KxXmlUtils.GetNodeValue(node, "tocharge", (decimal)0.00M);
            _pendingBankTransfers = KxXmlUtils.GetNodeValue(node, "banktransferpaymentsoutstanding", 0) > 0;
        }

        private void DetermineBalances()
        {
            if (IsCharge)
            {
                allocatedAmount = fromCharge - toCharge;
                unallocatedBalance = totalAmount - allocatedAmount;
            }
            else
            {
                allocatedAmount = fromPayment - toPayment;
                unallocatedBalance = totalAmount - allocatedAmount;
            }
        }
    }
}

