using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Exceptions;
using Kx.Core.Utility;

namespace Kx.Objects.Student
{
    public class KxPricingModelCharges : KxBaseClass
    {
        //private bool PaymentDeducted = false;
        private int pricingModelid;
        private List<KxPricingModelCharge> items = new List<KxPricingModelCharge>();

        public int PricingModelid
        {
            get
            { return pricingModelid; }
            set
            { pricingModelid = value; }
        }

        public List<KxPricingModelCharge> Items
        {
            get
            { return items; }
        }

        public KxPricingModelCharge this[int index]
        {
            get
            { return items[index]; }
            set
            { items[index] = value; }
        }

        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = null;

            if (actiontype == ActionType.Select)
            {
                result = new KxData("prices", "st2pricingmodelcharges", TargetType.Table);

                result.KeyFields.Add(new KxDataValue("pricingmodelid", pricingModelid, KxValueType.Int));

                result.AddOrderField("pricingmodelid", KxDataOrderType.Ascending);
            }
            return result;
        }

        public KxPricingModelCharges(KxBaseClass parent, int pricingModelid)
            : base(parent)
        {
            this.pricingModelid = pricingModelid;
            Load();
        }

        public override bool LoadFromXml(string xml, string xpath)
        {
            items.Clear();
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xml);

                XmlNodeList nodes = doc.SelectNodes(@"//results/" + xpath + "/datarow");

                foreach (XmlNode node in nodes)
                {
                    if (node != null)
                    {
                        KxPricingModelCharge chg = new KxPricingModelCharge(this, node);
                        Items.Add(chg);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "LoadFromXml", ex.Message, xpath);
                LastError = ex.Message;
                return false;
            }
        }
    }
}
