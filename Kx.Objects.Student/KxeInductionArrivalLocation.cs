using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;

namespace Kx.Objects.Student
{
    [Serializable]
    public class KxeInductionArrivalLocation : KxBaseClass
    {
        #region Attributes & Properties

        protected int academicYearId = 0;
        protected int      siteId;
        protected int      areaId;
        protected int      blockId;
        protected DateTime ?creationDate;
        protected int      creationUserId;

        protected KxeInductionArrivalSlots arrivalSlots = null;
        protected KxeInductionArrivalSlots allocationArrivalSlots = null;
        protected KxeInductionArrivalSlots fromAllocationArrivalSlots = null;

        public int      LocationId
        {
            get
            { return base.dBIdentifier;}
        }

        public int      SiteId
        {
            get
            { return siteId;}
            set
            { 
                AddToAudit("SiteId", siteId, value);
                siteId = value;
            }
        }

        public int      AreaId
        {
            get
            { return areaId;}
            set
            { 
                AddToAudit("AreaId", areaId, value);
                areaId = value;
            }
        }

        public int      BlockId
        {
            get
            { return blockId;}
            set
            { 
                AddToAudit("BlockId", blockId, value);
                blockId = value;
            }
        }

        public DateTime ?CreationDate
        {
            get
            { return creationDate;}
            set
            { 
                AddToAudit("CreationDate", creationDate, value);
                creationDate = value;
            }
        }

        public int      CreationUserId
        {
            get
            { return creationUserId;}
            set
            { 
                AddToAudit("CreationUserId", creationUserId, value);
                creationUserId = value;
            }
        }

        public KxStudentAllocation Allocation
        {
            get
            {
                try
                {
                    return (KxStudentAllocation)parent;
                }
                catch
                {
                    return null;
                }
            }
        }

        public KxeInductionArrivalSlots ArrivalSlots
        {
            get
            {
                if (arrivalSlots == null && Allocation != null)
                {
                    arrivalSlots = new KxeInductionArrivalSlots(this);
                }
                return arrivalSlots;
            }
            set
            {
                arrivalSlots = value;
            }
        }

        public KxeInductionArrivalSlots AllocationArrivalSlots
        {
            get
            {
                if (allocationArrivalSlots == null && Allocation != null)
                {
                    allocationArrivalSlots = new KxeInductionArrivalSlots(this, Allocation);
                }
                return allocationArrivalSlots;
            }
        }

        public KxeInductionArrivalSlots FromAllocationArrivalSlots
        {
            get
            {
                if (fromAllocationArrivalSlots == null && Allocation != null)
                {
                    fromAllocationArrivalSlots = new KxeInductionArrivalSlots(this, Allocation.AllocationStartDate);
                }
                return fromAllocationArrivalSlots;
            }
        }

        public int AcademicYearId
        {
            get
            {
                return academicYearId;
            }
            set
            {
                academicYearId = value;
            }
        }

        #endregion 


        public KxeInductionArrivalLocation(KxBaseClass parent):base(parent)
        {}


        public KxeInductionArrivalLocation(KxBaseClass parent, XmlNode node):base(parent)
        {
             LoadFromXml("", node);
        }

        public KxeInductionArrivalLocation(KxBaseClass parent,int      locationid):base(parent)
        {
             base.dBIdentifier = locationid;
             Load();
        }


        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("st2einductionarrivallocations", "ST2eInductionArrivalLocations", TargetType.Table);

            result.AutoKeyField = new KxDataValue("LocationId",LocationId,KxValueType.Int);


            if ((actiontype == ActionType.Update) && (LocationId == 0))
            {
                result.Action = ActionType.Insert;
            }
            else
            {
                result.Action = actiontype;
            }

            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {
                result.AddParameter("academicyearid", academicYearId, KxValueType.Int, true);
                result.AddParameter("areaId", areaId, KxValueType.Int, true);
                result.AddParameter("blockId", blockId, KxValueType.Int, true);
                result.AddParameter("creationDate", creationDate, KxValueType.DateTime, true);
                result.AddParameter("creationUserId", creationUserId, KxValueType.Int, true);
            }
            return result;
        }


        public override  void LoadFromXml(string xml,XmlNode node)
        {
            dBIdentifier  = KxXmlUtils.GetNodeValue(node,"locationid",0);
            academicYearId = KxXmlUtils.GetNodeValue(node, "academicyearid", 0);
            siteId  = KxXmlUtils.GetNodeValue(node,"siteid",0);
            areaId  = KxXmlUtils.GetNodeValue(node,"areaid",0);
            blockId  = KxXmlUtils.GetNodeValue(node,"blockid",0);
            creationDate  = KxXmlUtils.GetNodeValueDT(node,"creationdate",null);
            creationUserId  = KxXmlUtils.GetNodeValue(node,"creationuserid",0);
        }
    }
}

