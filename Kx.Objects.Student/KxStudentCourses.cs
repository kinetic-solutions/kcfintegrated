using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Exceptions;
using Kx.Core.Utility;

namespace Kx.Objects.Student
{
    public class KxStudentCourses : KxBaseClass
    {
        private int kxStudentId;
        private List<KxStudentCourse> items = new List<KxStudentCourse>();

        private DateTime? earliestDate = null;
        private DateTime? latestDate = null;

        public int KxStudentId
        {
            get
            { return kxStudentId; }
            set
            { kxStudentId = value; }
        }

        public List<KxStudentCourse> Items
        {
            get
            { return items; }
        }

        public KxStudentCourse this[int index]
        {
            get
            { return items[index]; }
            set
            { items[index] = value; }
        }

        public KxStudentCourse CurrentCourse
        {
            get
            { 
                return GetCurrentCourse(); 
            }
        }

        public DateTime? EarliestDate
        {
            get
            {
                if (earliestDate == null)
                {
                    earliestDate =  GetEarliestDate();
                }
                return earliestDate;
            }
        }

     

        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = null;

            if (actiontype == ActionType.Select)
            {
                result = new KxData("studentcourses", "st2studentcourses", TargetType.Table);

                result.KeyFields.Add(new KxDataValue("kxStudentId", kxStudentId, KxValueType.Int));
            }
            return result;
        }

        public KxStudentCourses(KxBaseClass parent, int kxStudentId) : base(parent)
        {
            this.kxStudentId = kxStudentId;
            Load();
        }

        public override bool LoadFromXml(string xml, string xpath)
        {
            items.Clear();
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xml);

                XmlNodeList nodes = doc.SelectNodes(@"//results/" + xpath + "/datarow");

                foreach (XmlNode node in nodes)
                {
                    if (node != null)
                    {
                        KxStudentCourse sc = new KxStudentCourse(this, node);
                        Items.Add(sc);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "LoadFromXml", ex.Message, xpath);
                LastError = ex.Message;
                return false;
            }
        }

        public KxStudentCourse GetCurrentCourse()
        {
            KxStudentCourse result = null;

            foreach (KxStudentCourse course in items)
            {
                if (course.CurrentCourse)
                {
                    result = course;
                    break;
                }
            }
            return result;
        }

        public DateTime? GetEarliestDate()
        {
            DateTime? result = null;

            foreach (KxStudentCourse course in Items)
            {

                if (course.CourseEndDate != null && course.CourseEndDate.Value > DateTime.Now)
                {
                    if (result == null ||  course.CourseStartDate.Value < result.Value)
                    {
                        result = course.CourseStartDate;
                    }
                }
                
            }

            return result;
        }
    }
}
