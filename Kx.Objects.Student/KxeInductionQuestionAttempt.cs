using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;
using Kx.Objects.Student.Web;

namespace Kx.Objects.Student
{
    [Serializable]
    public class KxeInductionQuestionAttempt : KxBaseClass
    {
        #region Attributes & Properties

        protected int      applicationId;
        protected int      questionId;
        protected string   answerLog;
        protected int answer = -1;

        protected KxeInductionQuestion question = null;

        public int      AnswerId
        {
            get
            { return base.dBIdentifier;}
        }

        public int      ApplicationId
        {
            get
            { return applicationId;}
            set
            { 
                AddToAudit("ApplicationId", applicationId, value);
                applicationId = value;
            }
        }

        public int      QuestionId
        {
            get
            { return questionId;}
            set
            { 
                AddToAudit("QuestionId", questionId, value);
                questionId = value;
            }
        }

        public string   AnswerLog
        {
            get
            { return answerLog;}
            set
            { 
                AddToAudit("AnswerLog", answerLog, value);
                answerLog = value;
            }
        }

        public KxeInductionQuestion Question
        {
            get
            {
                if (question == null && questionId > 0)
                {
                    question = KxStudentObjectManager.InductionQuestions.GeteInductionQuestion(questionId);
                }
                return question;
            }
            set
            {
                question = value;
            }
        }

        public string AnswerA
        {
            get
            {
                return Question.AnswerA;
            }
        }

        public string AnswerB
        {
            get
            {
                return Question.AnswerB;
            }
        }

        public string AnswerC
        {
            get
            {
                return Question.AnswerC;
            }
        }

        public string AnswerD
        {
            get
            {
                return Question.AnswerD;
            }
        }

        public string AnswerE
        {
            get
            {
                return Question.AnswerE;
            }
        }

        public string AnswerF
        {
            get
            {
                return Question.AnswerF;
            }
        }

        public int Answer
        {
            get
            {
                return answer;
            }
            set
            {
                if (value != answer)
                {
                    answer = value;
                    LogAnswer(value);
                }
            }
        }

        public bool IsCorrect
        {
            get
            {
                return answer == Question.CorrectAnswerIndex;
            }
        }

        #endregion 


        public KxeInductionQuestionAttempt(KxBaseClass parent):base(parent)
        {}


        public KxeInductionQuestionAttempt(KxBaseClass parent, XmlNode node):base(parent)
        {
             LoadFromXml("", node);
        }

        public KxeInductionQuestionAttempt(KxBaseClass parent,int      answerid):base(parent)
        {
             base.dBIdentifier = answerid;
             Load();
        }


        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("st2einductionquestionattempt", "ST2eInductionQuestionAttempt", TargetType.Table);

            result.AutoKeyField = new KxDataValue("AnswerId",AnswerId,KxValueType.Int);


            if ((actiontype == ActionType.Update) && (AnswerId == 0))
            {
                result.Action = ActionType.Insert;
            }
            else
            {
                result.Action = actiontype;
            }

            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {
                result.AddParameter("questionId", questionId, KxValueType.Int, true);
                result.AddParameter("applicationid", applicationId, KxValueType.Int, true);
                result.AddParameter("answerLog", answerLog, KxValueType.String, 2147483647, true);
            }
            return result;
        }


        public override  void LoadFromXml(string xml,XmlNode node)
        {
            dBIdentifier  = KxXmlUtils.GetNodeValue(node,"answerid",0);
            applicationId  = KxXmlUtils.GetNodeValue(node,"applicationid",0);
            questionId  = KxXmlUtils.GetNodeValue(node,"questionid",0);
            answerLog  = KxXmlUtils.GetNodeValue(node,"answerlog","");

            if (answerLog.EndsWith(" CORRECT\r\n"))
            {
                answer = Question.CorrectAnswerIndex;
            }
        }

        public void LogAnswer(int idx)
        {
            Changed = true;

            string ans = Question.GetAnswerById(idx);

            answerLog += KxDateTime.ToString(DateTime.Now,"g")+"\t\tAnswer changed to " + ans+"\t\t";

            if (idx == Question.CorrectAnswerIndex)
            {
                answerLog += " CORRECT\r\n";
            }
            else
            {
                answerLog += " WRONG\r\n";
            }

        }
    }
}

