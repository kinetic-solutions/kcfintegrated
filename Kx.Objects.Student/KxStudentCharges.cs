using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Exceptions;
using Kx.Core.Utility;

namespace Kx.Objects.Student
{
    public enum KxStudentChargingType { Student, Allocation, Invoice, Application };

    public class KxStudentCharges : KxBaseClass
    {
        private int identifier = 0;

        private KxStudentChargingType chargeType = KxStudentChargingType.Student;

        private List<KxStudentCharge> items = new List<KxStudentCharge>();

        public Decimal TotalCharges
        {
            get
            { return GetTotalCharges(); }
        }

        public Decimal AccommodationCharges
        {
            get
            { return GetAccommodationCharges(); }
        }

        public Decimal AncillaryCharges
        {
            get
            { return GetAncillaryCharges(); }
        }

        public List<KxStudentCharge> Items
        {
            get
            { return items; }
        }

        public KxStudentCharge this[int index]
        {
            get
            { return items[index]; }
            set
            { items[index] = value; }
        }

        public override KxData ConfigureData(ActionType actiontype)
        {
             KxData result = null;

             if (actiontype == ActionType.Select)
             {
                 result = new KxData("studentcharges", "ST2StudentCharges", TargetType.Table);

                 switch (chargeType)
                 {
                     case KxStudentChargingType.Student: result.KeyFields.Add(new KxDataValue("kxStudentId", identifier, KxValueType.Int)); break;
                     case KxStudentChargingType.Allocation: result.KeyFields.Add(new KxDataValue("allocationdateid", identifier, KxValueType.Int)); break;
                     case KxStudentChargingType.Application: result.KeyFields.Add(new KxDataValue("applicationid", identifier, KxValueType.Int)); break;
                     case KxStudentChargingType.Invoice: result.KeyFields.Add(new KxDataValue("invoiceNumber", identifier, KxValueType.Int)); break;
                 }

                 result.AddOrderField("datefrom", KxDataOrderType.Ascending);

             }
            return result;
        }

        public KxStudentCharges(KxBaseClass parent, int id, KxStudentChargingType kxChargeType)
            : base(parent)
        {
            chargeType = kxChargeType;
            identifier = id;

            Load();
        }

        public override bool LoadFromXml(string xml, string xpath)
        {
            items.Clear();
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xml);

                XmlNodeList nodes = doc.SelectNodes(@"//results/" + xpath + "/datarow");

                foreach (XmlNode node in nodes)
                {
                    if (node != null)
                    {
                        KxStudentCharge app = new KxStudentCharge(this, node);
                        Items.Add(app);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "LoadFromXml", ex.Message, xpath);
                LastError = ex.Message;
                return false;
            }
        }

        public Decimal GetTotalCharges()
        {
            Decimal result = 0.00M;

            foreach(KxStudentCharge chg in Items)
            {
                result += chg.TotalAmount;
            }
            return result;
        }

        public Decimal GetAccommodationCharges()
        {
            Decimal result = 0.00M;

            foreach (KxStudentCharge chg in Items)
            {
                if (chg.ChargeType.IsAccommodation)
                {
                    result += chg.TotalAmount;
                }
            }
            return result;
        }

        public Decimal GetAncillaryCharges()
        {
            Decimal result = 0.00M;

            foreach (KxStudentCharge chg in Items)
            {
                if (!chg.ChargeType.IsAccommodation)
                {
                    result += chg.TotalAmount;
                }
            }
            return result;
        }

        public List<DateTime> GetPaymentDates()
        {
            List<DateTime> result = new List<DateTime>();

            foreach (KxStudentCharge charge in Items)
            {
                if (result.IndexOf(charge.DateFrom.Value) < 0)
                {
                    result.Add(charge.DateFrom.Value);
                }
            }
            
            return result;
        }

        public string GetPaymentDateString(string seperator)
        {
            string result = "";

            foreach (DateTime dte in GetPaymentDates())
            {
                if (result != "")
                {
                   result += seperator;
                }
                result += dte.ToString("dd/MM/yyyy");
            }
            return result;
        }

        public DateTime GetFirstPaymentDate()
        {
            DateTime result = new DateTime(1900,1,1);

            foreach (DateTime dte in GetPaymentDates())
            {

                result = dte;
                break;
            }
            return result;
        }

        public string GetFirstPaymentDateString()
        {
            string result = "";

            foreach (DateTime dte in GetPaymentDates())
            {
               
                result += dte.ToString("dd/MM/yyyy");
                break;
            }
            return result;
        }

        public string GetPaymentAmounts(string seperator)
        {
            string result = "";

            foreach (DateTime dte in GetPaymentDates())
            {
                if (result != "")
                {
                    result += seperator;
                }
                result += Math.Round(GetPaymentTotalForDate(dte),2).ToString();
            }
            return result;
        }

        public decimal GetPaymentTotalForDate(DateTime dte)
        {
            Decimal result = 0.0M;

            foreach (KxStudentCharge charge in Items)
            {
                if (charge.DateFrom.Value == dte)
                {
                    result += charge.Amount;
                }
            }
            return result;
        }
    }
}
