using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;
using Kx.Objects.Student.Web;

namespace Kx.Objects.Student
{
    [Serializable]
    public class KxeInductionArrival : KxBaseClass
    {
        #region Attributes & Properties

        protected int applicationId;
        protected int arrivalSlotId;
        protected DateTime? creationDate;

        protected KxeInductionArrivalSlot arrivalSlot = null;

        public int ArrivalId
        {
            get
            { return base.dBIdentifier; }
        }

        public int ApplicationId
        {
            get
            { return applicationId; }
            set
            {
                AddToAudit("ApplicationId", applicationId, value);
                applicationId = value;
            }
        }

        public int ArrivalSlotId
        {
            get
            { return arrivalSlotId; }
            set
            {
                AddToAudit("ArrivalSlotId", arrivalSlotId, value);
                arrivalSlot = null;
                arrivalSlotId = value;
            }
        }

        public DateTime? CreationDate
        {
            get
            { return creationDate; }
            set
            {
                AddToAudit("CreationDate", creationDate, value);
                creationDate = value;
            }
        }

        public KxeInductionArrivalSlot ArrivalSlot
        {
            get
            {
                if (arrivalSlot == null && arrivalSlotId > 0)
                {
                    arrivalSlot = new KxeInductionArrivalSlot(this, arrivalSlotId);
                }
                else
                    if (arrivalSlot == null)
                    {
                        arrivalSlot = new KxeInductionArrivalSlot(null);
                    }

                return arrivalSlot;
            }
            set
            {
                arrivalSlot = value;
            }
        }


        #endregion


        public KxeInductionArrival(KxBaseClass parent)
            : base(parent)
        {
        }


        public KxeInductionArrival(KxBaseClass parent, XmlNode node)
            : base(parent)
        {
            LoadFromXml("", node);
        }

        public KxeInductionArrival(KxBaseClass parent, int applicationid)
            : base(parent)
        {
            applicationId = applicationid;
            Load();
        }


        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("st2einductionarrivals", "ST2eInductionArrivals", TargetType.Table);

            if (actiontype == ActionType.Select)
            {
                result.KeyFields.Add(new KxDataValue("applicationId", applicationId, KxValueType.Int));
            }
            else
            {
                result.AutoKeyField = new KxDataValue("arrivalId", dBIdentifier, KxValueType.Int);
            }

            if (creationDate == null)
            {
                creationDate = DateTime.Now;
            }

            if ((actiontype == ActionType.Update) && (ArrivalId == 0))
            {
                result.Action = ActionType.Insert;
            }
            else
            {
                result.Action = actiontype;
            }

            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {
                result.AddParameter("applicationid", applicationId, KxValueType.Int, false);
                result.AddParameter("arrivalSlotId", arrivalSlotId, KxValueType.Int, true);
                result.AddParameter("creationDate", creationDate, KxValueType.DateTime, true);
            }
            return result;
        }


        public override void LoadFromXml(string xml, XmlNode node)
        {
            dBIdentifier = KxXmlUtils.GetNodeValue(node, "arrivalid", 0);
            applicationId = KxXmlUtils.GetNodeValue(node, "applicationid", 0);
            arrivalSlotId = KxXmlUtils.GetNodeValue(node, "arrivalslotid", 0);
            creationDate = KxXmlUtils.GetNodeValueDT(node, "creationdate", null);
        }
    }
}

