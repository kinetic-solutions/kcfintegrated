using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Exceptions;
using Kx.Core.Utility;

namespace Kx.Objects.Student
{
    public class KxControlStudents : KxBaseClass
    {
        private static readonly object locker = new object ();  
        private static KxControlStudents instance = null;
        /// <summary>
        /// Changed from List to Dictionary to enable quick lookup of control settings. 
        /// </summary>
        private static ConcurrentDictionary<String, KxControlStudent> controlsettings = new ConcurrentDictionary<String, KxControlStudent>();


        public List<KxControlStudent> Items => controlsettings.Values.ToList();

        public KxControlStudent this[int index]
        {
            get
            { return Items[index]; }
            set
            { Items[index] = value; }
        }

        public KxControlStudent AddControlStudent()
        {
            KxControlStudent result = new KxControlStudent(this);
            controlsettings.GetOrAdd(result.Description, result);
            return result;
        }

        public override KxData ConfigureData(ActionType actiontype)
        {
            return null;
        }

        public KxControlStudents(KxBaseClass parent) : base(parent)
        {

        }

        /// <summary>
        /// Setting up a lone instance
        /// </summary>
        public static KxControlStudents Instance
        {
            get
            {
                lock (locker)
                    {
                        if (instance == null)
                        {
                            instance = new KxControlStudents(null);
                        }
                        return instance;
                    }
            }
        }

        public override bool LoadFromXml(string xml, string xpath)
        {
            return true;
        }

        /// <summary>
        /// Gets a Control Setting (ControlStudent table) based on description value
        /// Fetches from a dictionary now.
        /// </summary>
        /// <param name="description">Description of setting</param>
        /// <returns></returns>
        public KxControlStudent GetControlStudent(string description)
        {
            // Try to set control setting from dictionary, If we can't find it in the dictionary, add with null value.
            KxControlStudent result = controlsettings.GetOrAdd(description, func => {
                result = new KxControlStudent(this, description);
                return result;
            });
     
            return result;
        }
    }
}
