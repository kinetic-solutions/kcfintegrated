using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Exceptions;
using Kx.Core.Utility;

namespace Kx.Objects.Student
{
    public class KxAllocationModelDateCache : KxBaseClass
    {
        protected List<KxAllocationModelDate> items = null;

        public List<KxAllocationModelDate> Items
        {
            get
            {
                if (items == null)
                {
                    items = new List<KxAllocationModelDate>();
                } 
                return items;
            }
        }

        public KxAllocationModelDate this[int index]
        {
            get
            { return items[index]; }
            set
            { items[index] = value; }
        }

    

        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = null;

            if (actiontype == ActionType.Select)
            {
                result = new KxData("allocationmodeldates", "st2allocationmodeldates", TargetType.Table);
            }
            return result;
        }

        public KxAllocationModelDateCache(KxBaseClass parent) : base(parent)
        {
            
        }

        public override bool LoadFromXml(string xml, string xpath)
        {
            items.Clear();
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xml);

                XmlNodeList nodes = doc.SelectNodes(@"//results/" + xpath + "/datarow");

                foreach (XmlNode node in nodes)
                {
                    if (node != null)
                    {
                        KxAllocationModelDate mad = new KxAllocationModelDate(this, node);
                        Items.Add(mad);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "LoadFromXml", ex.Message, xpath);
                LastError = ex.Message;
                return false;
            }
        }

        public KxAllocationModelDate GetModelDateById(int id)
        {
            KxAllocationModelDate result = null;

            foreach (KxAllocationModelDate date in Items)
            {
                if (date.AllocationModelDateId == id)
                {
                    result = date;
                    break;
                }
            }

            if (result == null)
            {
                KxAllocationModelDate dummy = new KxAllocationModelDate(this, id);

                if (dummy.AllocationModelDateId == id)
                {
                    Items.Add(dummy);
                    result = dummy;
                }
            }

            return result;
        }
   

    }
}
