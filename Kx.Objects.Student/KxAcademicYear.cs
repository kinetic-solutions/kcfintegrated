using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Linq;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;
using Kx.Objects.Student.Web;
namespace Kx.Objects.Student
{
    [Serializable]
    public class KxAcademicYear : KxBaseClass
    {
        #region Attributes & Properties

        protected DateTime ?startDate;
        protected DateTime ?endDate;
        protected string   title;
        protected int      eventModuleId;
        protected bool     webPublished;
        protected bool  _visible;
        protected string _coreSessionDescription;

        protected KxWebAllocationPricingList allocationPricing = null;

        public int      AcademicYearId
        {
            get
            { return base.dBIdentifier;}
        }

        public DateTime ?StartDate
        {
            get
            { return startDate;}
            set
            { 
                AddToAudit("StartDate", startDate, value);
                startDate = value;
            }
        }

        public string StartDateString
        {
            get
            {
                try
                {
                    return KxDateTime.ToString(startDate,"d");
                }
                catch
                {
                    return "";
                }
            }
        }

        public DateTime ?EndDate
        {
            get
            { return endDate;}
            set
            { 
                AddToAudit("EndDate", endDate, value);
                endDate = value;
            }
        }

        public string   Title
        {
            get
            { return title;}
            set
            { 
                AddToAudit("Title", title, value);
                title = value;
            }
        }

        public int      EventModuleId
        {
            get
            { return eventModuleId;}
            set
            { 
                AddToAudit("EventModuleId", eventModuleId, value);
                eventModuleId = value;
            }
        }

        public bool     WebPublished
        {
            get
            { return webPublished;}
            set
            { 
                AddToAudit("WebPublished", webPublished, value);
                webPublished = value;
            }
        }

        public bool Visible

        {
            get { return _visible; }
            set
            {
                AddToAudit("Visible", _visible, value);
                _visible = value;
            }
        }

        public string CoreSessionDescription
        {
            get { return _coreSessionDescription; }
            set
            {
                AddToAudit("CoreSessionDescription", _coreSessionDescription, value);
                _coreSessionDescription = value;
            }
        }

        public KxWebAllocationPricingList AllocationPricing
        {
            get
            {
                if (allocationPricing == null)
                {
                    allocationPricing = new KxWebAllocationPricingList(this, AcademicYearId);
                }
                return allocationPricing;
            }
            set
            {
                allocationPricing = value;
            }
        }

        public IEnumerable<KxAcademicYearSession> Sessions
        {
            get { return KxAcademicYearSession.GetST2AcademicYearSessions(this.AcademicYearId); }
        }

        public IEnumerable<KxAcademicYearSession> AllSessions
        {
            get
            {
                List<KxAcademicYearSession> coreSession = new List<KxAcademicYearSession>();
                coreSession.Add(new KxAcademicYearCoreSession(this));
                return Sessions.Concat(coreSession).OrderBy(s => s.SessionId);
            }
        }

        public string [] AllSessionIDs
        {
            get
            {
                return (from s in AllSessions
                        orderby s.SessionId == 0 ? DateTime.MinValue : s.StartDate
                        select s.SessionId.ToString()).ToArray();
            }
        }

        public string [] ActiveSessionIDs
        {
            get
            {
                return (from s in AllSessions
                    where s.EndDate >= DateTime.Now.Date || s.SessionId == 0
                    orderby s.SessionId == 0 ? DateTime.MinValue : s.StartDate
                    select s.SessionId.ToString()).ToArray();
            }
        }

        #endregion 


        /// <summary>
        /// 
        /// </summary>
        public KxAcademicYear(KxBaseClass parent)
            :base(parent)
        {            
        }

        public KxAcademicYear(KxBaseClass parent,int academicyearid):base(parent)
        {
             base.dBIdentifier = academicyearid;
             Load();
        }

        public KxAcademicYear(KxBaseClass parent, XmlNode node)  : base(parent)
        {
             LoadFromXml("", node);
        }

        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("st2academicyears", "ST2AcademicYears", TargetType.Table);

            result.AutoKeyField = new KxDataValue("AcademicYearId",AcademicYearId,KxValueType.Int);


            if ((actiontype == ActionType.Update) && (AcademicYearId == 0))
            {
                result.Action = ActionType.Insert;
            }
            else
            {
                result.Action = actiontype;
            }

            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {
                   result.AddParameter("startDate", startDate, KxValueType.DateTime);
                   result.AddParameter("endDate", endDate, KxValueType.DateTime);
                   result.AddParameter("title", title, KxValueType.String);
                   result.AddParameter("eventModuleId", eventModuleId, KxValueType.Int);
                   result.AddParameter("webPublished", webPublished, KxValueType.Boolean);
            }
            return result;
        }


        public override  void LoadFromXml(string xml,XmlNode node)
        {
            dBIdentifier  = KxXmlUtils.GetNodeValue(node,"academicyearid",0);
            startDate  = KxXmlUtils.GetNodeValueDT(node,"startdate",null);
            endDate  = KxXmlUtils.GetNodeValueDT(node,"enddate",null);
            title  = KxXmlUtils.GetNodeValue(node,"title","");
            eventModuleId = KxXmlUtils.GetNodeValue(node,"eventmoduleid",0);
            webPublished  = KxXmlUtils.GetNodeValue(node,"webpublished",false);
            _visible = KxXmlUtils.GetNodeValue(node, "visible", true);
            _coreSessionDescription = KxXmlUtils.GetNodeValue(node, "coresessiondescription", string.Empty);
        }

        public override string ToString()
        {
            return this.Title;
        }
    }
}

