using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;

namespace Kx.Objects.Student
{
    [Serializable]
    public class KxStudentHistory : KxBaseClass
    {
        #region Attributes & Properties

        protected int      kxStudentId;
        protected int      applicationId;
        protected int      allocationId;
        protected int      allocationDateId;
        protected DateTime ?creationDate;
        protected int      creationUserId;
        protected string   historyType;
        protected string   historyNotes;

        public int      HistoryId
        {
            get
            { return base.dBIdentifier;}
        }

        public int      KxStudentId
        {
            get
            { return kxStudentId;}
            set
            { 
                AddToAudit("KxStudentId", kxStudentId, value);
                kxStudentId = value;
            }
        }

        public int      ApplicationId
        {
            get
            { return applicationId;}
            set
            { 
                AddToAudit("ApplicationId", applicationId, value);
                applicationId = value;
            }
        }

        public int      AllocationId
        {
            get
            { return allocationId;}
            set
            { 
                AddToAudit("AllocationId", allocationId, value);
                allocationId = value;
            }
        }

        public int      AllocationDateId
        {
            get
            { return allocationDateId;}
            set
            { 
                AddToAudit("AllocationDateId", allocationDateId, value);
                allocationDateId = value;
            }
        }

        public DateTime ?CreationDate
        {
            get
            { return creationDate;}
            set
            { 
                AddToAudit("CreationDate", creationDate, value);
                creationDate = value;
            }
        }

        public int      CreationUserId
        {
            get
            { return creationUserId;}
            set
            { 
                AddToAudit("CreationUserId", creationUserId, value);
                creationUserId = value;
            }
        }

        public string   HistoryType
        {
            get
            { return historyType;}
            set
            { 
                AddToAudit("HistoryType", historyType, value);
                historyType = value;
            }
        }

        public string   HistoryNotes
        {
            get
            { return historyNotes;}
            set
            { 
                AddToAudit("HistoryNotes", historyNotes, value);
                historyNotes = value;
            }
        }


        #endregion 


        public KxStudentHistory(KxBaseClass parent):base(parent)
        {}


        public KxStudentHistory(KxBaseClass parent, XmlNode node):base(parent)
        {
             LoadFromXml("", node);
        }

        public KxStudentHistory(KxBaseClass parent,int      historyid):base(parent)
        {
             base.dBIdentifier = historyid;
             Load();
        }


        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("st2studenthistory", "ST2StudentHistory", TargetType.Table);

            result.AutoKeyField = new KxDataValue("HistoryId",HistoryId,KxValueType.Int);


            if ((actiontype == ActionType.Update) && (HistoryId == 0))
            {
                result.Action = ActionType.Insert;
            }
            else
            {
                result.Action = actiontype;
            }

            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {
                result.AddParameter("kxstudentid", kxStudentId, KxValueType.Int, false);
                result.AddParameter("applicationId", applicationId, KxValueType.Int, false);
                result.AddParameter("allocationId", allocationId, KxValueType.Int, false);
                result.AddParameter("allocationDateId", allocationDateId, KxValueType.Int, false);
                result.AddParameter("creationDate", creationDate, KxValueType.DateTime, false);
                result.AddParameter("creationUserId", creationUserId, KxValueType.Int, false);
                result.AddParameter("historyType", historyType, KxValueType.String, 50, false);
                result.AddParameter("historyNotes", historyNotes, KxValueType.String, 2147483647, false);
            }
            return result;
        }


        public override  void LoadFromXml(string xml,XmlNode node)
        {
            dBIdentifier  = KxXmlUtils.GetNodeValue(node,"historyid",0);
            kxStudentId  = KxXmlUtils.GetNodeValue(node,"kxstudentid",0);
            applicationId  = KxXmlUtils.GetNodeValue(node,"applicationid",0);
            allocationId  = KxXmlUtils.GetNodeValue(node,"allocationid",0);
            allocationDateId  = KxXmlUtils.GetNodeValue(node,"allocationdateid",0);
            creationDate  = KxXmlUtils.GetNodeValueDT(node,"creationdate",null);
            creationUserId  = KxXmlUtils.GetNodeValue(node,"creationuserid",0);
            historyType  = KxXmlUtils.GetNodeValue(node,"historytype","");
            historyNotes  = KxXmlUtils.GetNodeValue(node,"historynotes","");
        }
    }
}

