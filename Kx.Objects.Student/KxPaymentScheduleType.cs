using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;

namespace Kx.Objects.Student
{
    [Serializable]
    public class KxPaymentScheduleType : KxBaseClass
    {
        #region Attributes & Properties

        protected string   description;
        protected bool     inactive;
        protected int      creationUserId;
        protected DateTime ?creationDate;
        protected int      minimumDuration;
        protected decimal     surchargePC;
        protected decimal     discountPC;
        protected string   customSQL;

        protected KxPaymentScheduleTypeModels models = null;

        public int      PaymentScheduleTypeId
        {
            get
            { return base.dBIdentifier;}
        }

        public string   Description
        {
            get
            { return description;}
            set
            { 
                AddToAudit("Description", description, value);
                description = value;
            }
        }

        public bool     Inactive
        {
            get
            { return inactive;}
            set
            { 
                AddToAudit("Inactive", inactive, value);
                inactive = value;
            }
        }

        public int      CreationUserId
        {
            get
            { return creationUserId;}
            set
            { 
                AddToAudit("CreationUserId", creationUserId, value);
                creationUserId = value;
            }
        }

        public DateTime ?CreationDate
        {
            get
            { return creationDate;}
            set
            { 
                AddToAudit("CreationDate", creationDate, value);
                creationDate = value;
            }
        }

        public int      MinimumDuration
        {
            get
            { return minimumDuration;}
            set
            { 
                AddToAudit("MinimumDuration", minimumDuration, value);
                minimumDuration = value;
            }
        }

        public decimal     SurchargePC
        {
            get
            { return surchargePC;}
            set
            { 
                AddToAudit("SurchargePC", surchargePC, value);
                surchargePC = value;
            }
        }

        public decimal     DiscountPC
        {
            get
            { return discountPC;}
            set
            { 
                AddToAudit("DiscountPC", discountPC, value);
                discountPC = value;
            }
        }

        public string   CustomSQL
        {
            get
            { return customSQL;}
            set
            { 
                AddToAudit("CustomSQL", customSQL, value);
                customSQL = value;
            }
        }

        public KxPaymentScheduleTypeModels Models
        {
            get
            {
                if (models == null)
                {
                    models = new KxPaymentScheduleTypeModels(this, PaymentScheduleTypeId);
                }
                return models;
            }
        }

        #endregion 

        public KxPaymentScheduleType(KxBaseClass parent,int      paymentscheduletypeid):base(parent)
        {
             base.dBIdentifier = paymentscheduletypeid;
             Load();
        }


        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("st2paymentscheduletypes", "ST2PaymentScheduleTypes", TargetType.Table);

            result.AutoKeyField = new KxDataValue("PaymentScheduleTypeId",PaymentScheduleTypeId,KxValueType.Int);


            if ((actiontype == ActionType.Update) && (PaymentScheduleTypeId == 0))
            {
                result.Action = ActionType.Insert;
            }
            else
            {
                result.Action = actiontype;
            }

            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {
                   result.AddParameter("description", description, KxValueType.String);
                   result.AddParameter("inactive", inactive, KxValueType.Boolean);
                   result.AddParameter("creationUserId", creationUserId, KxValueType.Int);
                   result.AddParameter("creationDate", creationDate, KxValueType.DateTime);
                   result.AddParameter("minimumDuration", minimumDuration, KxValueType.Int);
                   result.AddParameter("surchargePC", surchargePC, KxValueType.Decimal);
                   result.AddParameter("discountPC", discountPC, KxValueType.Decimal);
                   result.AddParameter("customSQL", customSQL, KxValueType.String);
            }
            return result;
        }
        
        public override  void LoadFromXml(string xml,XmlNode node)
        {
            dBIdentifier  = KxXmlUtils.GetNodeValue(node,"paymentscheduletypeid",0);
            description  = KxXmlUtils.GetNodeValue(node,"description","");
            inactive  = KxXmlUtils.GetNodeValue(node,"inactive",false);
            creationUserId  = KxXmlUtils.GetNodeValue(node,"creationuserid",0);
            creationDate  = KxXmlUtils.GetNodeValueDT(node,"creationdate",null);
            minimumDuration  = KxXmlUtils.GetNodeValue(node,"minimumduration",0);
            surchargePC  = KxXmlUtils.GetNodeValue(node,"surchargepc",(decimal)0.00M);
            discountPC  = KxXmlUtils.GetNodeValue(node,"discountpc",(decimal)0.00M);
            customSQL  = KxXmlUtils.GetNodeValue(node,"customsql","");
        }
    }
}

