using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;

namespace Kx.Objects.Student
{
    [Serializable]
    public class KxDomicile : KxBaseClass
    {
        #region Attributes & Properties

        protected string   description;
        protected string   externalId;

        public int      DomicileId
        {
            get
            { return base.dBIdentifier;}
        }

        public string   Description
        {
            get
            { return description;}
            set
            { 
                AddToAudit("Description", description, value);
                description = value;
            }
        }

        public string   ExternalId
        {
            get
            { return externalId;}
            set
            { 
                AddToAudit("ExternalId", externalId, value);
                externalId = value;
            }
        }


        #endregion 


        public KxDomicile(KxBaseClass parent):base(parent)
        {}


        public KxDomicile(KxBaseClass parent, XmlNode node):base(parent)
        {
             LoadFromXml("", node);
        }

        public KxDomicile(KxBaseClass parent,int      domicileid):base(parent)
        {
             base.dBIdentifier = domicileid;
             Load();
        }


        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("st2domicile", "ST2Domicile", TargetType.Table);

            result.AutoKeyField = new KxDataValue("DomicileId",DomicileId,KxValueType.Int);


            if ((actiontype == ActionType.Update) && (DomicileId == 0))
            {
                result.Action = ActionType.Insert;
            }
            else
            {
                result.Action = actiontype;
            }

            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {
                   result.AddParameter("externalId", externalId, KxValueType.String, 50, true);
            }
            return result;
        }


        public override  void LoadFromXml(string xml,XmlNode node)
        {
            dBIdentifier  = KxXmlUtils.GetNodeValue(node,"domicileid",0);
            description  = KxXmlUtils.GetNodeValue(node,"description","");
            externalId  = KxXmlUtils.GetNodeValue(node,"externalid","");
        }
    }
}

