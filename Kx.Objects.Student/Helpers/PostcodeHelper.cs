﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Kx.Objects.Student.Helpers
{
    public static class PostcodeHelper
    {
        public static string GetOutcodeFromPostCode(string postcode)
        {
            string outcode = string.Empty;

            if (!string.IsNullOrEmpty(postcode))
            {
                // Remove whitespace
                postcode = postcode.Replace(" ", string.Empty);

                // Get the Outcode when the post is longer than 5 chars
                Int32 output = -1;

                if (postcode.Length >= 5)
                {
                    for (Int32 i = postcode.Length; i >= 0; i--)
                    {
                        if ((i > 0) && (Int32.TryParse(postcode.Substring(i - 1, 1), out output)))
                        {
                            // Character is a number
                            outcode = postcode.Substring(0, i - 1);
                            break;
                        }
                    }
                }
                else
                {
                    outcode = postcode;
                }
            }

            return outcode;
        }

        public static bool PostCodeOutcodeMatch(string postcode, string matchPostcodes)
        {
            // Get the Outcode (First 2-4 characters of postcode before the space)
            string outcode = GetOutcodeFromPostCode(postcode);

            // Compare the disallowed list
            bool result = false;
            string[] prefixes = matchPostcodes.ToUpper().Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

            foreach (string pfx in prefixes)
            {
                if (pfx == outcode)
                {
                    result = true;
                    break;
                }
            }

            return result;
        }
    }
}
