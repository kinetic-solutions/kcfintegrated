using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;

namespace Kx.Objects.Student
{
    [Serializable]
    public class KxRoomPricing : KxBaseClass
    {
        #region Attributes & Properties

        protected int      offerWordTemplateId;
        protected int      licenceWordTemplateId;
        protected int      courseTypeId;
        protected int      studentYearId;
        protected int      academicYearId;
        protected bool     stopChargeCreation;
        protected int      licenceTypeId;
        protected string   description;
        protected bool     offSite;
        protected bool     chargeExtraDay;

        public int      RoomPricingId
        {
            get
            { return base.dBIdentifier;}
        }

        public int      OfferWordTemplateId
        {
            get
            { return offerWordTemplateId;}
            set
            { 
                AddToAudit("OfferWordTemplateId", offerWordTemplateId, value);
                offerWordTemplateId = value;
            }
        }

        public int      LicenceWordTemplateId
        {
            get
            { return licenceWordTemplateId;}
            set
            { 
                AddToAudit("LicenceWordTemplateId", licenceWordTemplateId, value);
                licenceWordTemplateId = value;
            }
        }

        public int      CourseTypeId
        {
            get
            { return courseTypeId;}
            set
            { 
                AddToAudit("CourseTypeId", courseTypeId, value);
                courseTypeId = value;
            }
        }

        public int      StudentYearId
        {
            get
            { return studentYearId;}
            set
            { 
                AddToAudit("StudentYearId", studentYearId, value);
                studentYearId = value;
            }
        }

        public int      AcademicYearId
        {
            get
            { return academicYearId;}
            set
            { 
                AddToAudit("AcademicYearId", academicYearId, value);
                academicYearId = value;
            }
        }

        public bool     StopChargeCreation
        {
            get
            { return stopChargeCreation;}
            set
            { 
                AddToAudit("StopChargeCreation", stopChargeCreation, value);
                stopChargeCreation = value;
            }
        }

        public int      LicenceTypeId
        {
            get
            { return licenceTypeId;}
            set
            { 
                AddToAudit("LicenceTypeId", licenceTypeId, value);
                licenceTypeId = value;
            }
        }

        public string   Description
        {
            get
            { return description;}
            set
            { 
                AddToAudit("Description", description, value);
                description = value;
            }
        }

        public bool     OffSite
        {
            get
            { return offSite;}
            set
            { 
                AddToAudit("OffSite", offSite, value);
                offSite = value;
            }
        }

        public bool     ChargeExtraDay
        {
            get
            { return chargeExtraDay;}
            set
            { 
                AddToAudit("ChargeExtraDay", chargeExtraDay, value);
                chargeExtraDay = value;
            }
        }


        #endregion 

        public KxRoomPricing(KxBaseClass parent,int      roompricingid):base(parent)
        {
             base.dBIdentifier = roompricingid;
             Load();
        }


        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("st2roompricing", "ST2RoomPricing", TargetType.Table);

            result.AutoKeyField = new KxDataValue("RoomPricingId",RoomPricingId,KxValueType.Int);


            if ((actiontype == ActionType.Update) && (RoomPricingId == 0))
            {
                result.Action = ActionType.Insert;
            }
            else
            {
                result.Action = actiontype;
            }

            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {
                   result.AddParameter("offerWordTemplateId", offerWordTemplateId, KxValueType.Int);
                   result.AddParameter("licenceWordTemplateId", licenceWordTemplateId, KxValueType.Int);
                   result.AddParameter("courseTypeId", courseTypeId, KxValueType.Int);
                   result.AddParameter("studentYearId", studentYearId, KxValueType.Int);
                   result.AddParameter("academicYearId", academicYearId, KxValueType.Int);
                   result.AddParameter("stopChargeCreation", stopChargeCreation, KxValueType.Boolean);
                   result.AddParameter("licenceTypeId", licenceTypeId, KxValueType.Int);
                   result.AddParameter("description", description, KxValueType.String);
                   result.AddParameter("offSite", offSite, KxValueType.Boolean);
                   result.AddParameter("chargeExtraDay", chargeExtraDay, KxValueType.Boolean);
            }
            return result;
        }


        public override void LoadFromXml(string xml, XmlNode node)
        {
            dBIdentifier = KxXmlUtils.GetNodeValue(node, "roompricingid", 0);
            offerWordTemplateId  = KxXmlUtils.GetNodeValue(node,"offerwordtemplateid",0);
            licenceWordTemplateId  = KxXmlUtils.GetNodeValue(node,"licencewordtemplateid",0);
            courseTypeId  = KxXmlUtils.GetNodeValue(node,"coursetypeid",0);
            studentYearId  = KxXmlUtils.GetNodeValue(node,"studentyearid",0);
            academicYearId  = KxXmlUtils.GetNodeValue(node,"academicyearid",0);
            stopChargeCreation  = KxXmlUtils.GetNodeValue(node,"stopchargecreation",false);
            licenceTypeId  = KxXmlUtils.GetNodeValue(node,"licencetypeid",0);
            description  = KxXmlUtils.GetNodeValue(node,"description","");
            offSite  = KxXmlUtils.GetNodeValue(node,"offsite",false);
            chargeExtraDay  = KxXmlUtils.GetNodeValue(node,"chargeextraday",false);
        }
    }
}

