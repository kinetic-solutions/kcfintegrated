using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;

namespace Kx.Objects.Student
{
    [Serializable]
    public class KxDebtorStatus : KxBaseClass
    {
        #region Attributes & Properties

        protected string description;
        protected int allowApplication;
        protected int allowAllocation;
        protected int allowMove;
        protected int allowCharges;
        protected int allowRefunds;
        protected int allowWriteOff;
        protected DateTime? creationDate;
        protected int creationUserId;
        protected bool inactive;
        protected int stage;

        public int DebtorStatusId
        {
            get
            { return base.dBIdentifier; }
        }

        public string Description
        {
            get
            { return description; }
            set
            {
                AddToAudit("Description", description, value);
                description = value;
            }
        }

        public int AllowApplication
        {
            get
            { return allowApplication; }
            set
            {
                AddToAudit("AllowApplication", allowApplication, value);
                allowApplication = value;
            }
        }

        public int AllowAllocation
        {
            get
            { return allowAllocation; }
            set
            {
                AddToAudit("AllowAllocation", allowAllocation, value);
                allowAllocation = value;
            }
        }

        public int AllowMove
        {
            get
            { return allowMove; }
            set
            {
                AddToAudit("AllowMove", allowMove, value);
                allowMove = value;
            }
        }

        public int AllowCharges
        {
            get
            { return allowCharges; }
            set
            {
                AddToAudit("AllowCharges", allowCharges, value);
                allowCharges = value;
            }
        }

        public int AllowRefunds
        {
            get
            { return allowRefunds; }
            set
            {
                AddToAudit("AllowRefunds", allowRefunds, value);
                allowRefunds = value;
            }
        }

        public int AllowWriteOff
        {
            get
            { return allowWriteOff; }
            set
            {
                AddToAudit("AllowWriteOff", allowWriteOff, value);
                allowWriteOff = value;
            }
        }

        public DateTime? CreationDate
        {
            get
            { return creationDate; }
            set
            {
                AddToAudit("CreationDate", creationDate, value);
                creationDate = value;
            }
        }

        public int CreationUserId
        {
            get
            { return creationUserId; }
            set
            {
                AddToAudit("CreationUserId", creationUserId, value);
                creationUserId = value;
            }
        }

        public bool Inactive
        {
            get
            { return inactive; }
            set
            {
                AddToAudit("Inactive", inactive, value);
                inactive = value;
            }
        }

        public int Stage
        {
            get
            { return stage; }
            set
            {
                AddToAudit("Stage", stage, value);
                stage = value;
            }
        }


        #endregion


        public KxDebtorStatus(KxBaseClass parent)
            : base(parent)
        { }


        public KxDebtorStatus(KxBaseClass parent, XmlNode node)
            : base(parent)
        {
            LoadFromXml("", node);
        }

        public KxDebtorStatus(KxBaseClass parent, int debtorstatusid)
            : base(parent)
        {
            base.dBIdentifier = debtorstatusid;
            Load();
        }


        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("st2debtorstatus", "ST2DebtorStatus", TargetType.Table);

            result.AutoKeyField = new KxDataValue("DebtorStatusId", DebtorStatusId, KxValueType.Int);


            if ((actiontype == ActionType.Update) && (DebtorStatusId == 0))
            {
                result.Action = ActionType.Insert;
            }
            else
            {
                result.Action = actiontype;
            }

            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {
                result.AddParameter("allowApplication", allowApplication, KxValueType.Int, false);
                result.AddParameter("allowAllocation", allowAllocation, KxValueType.Int, false);
                result.AddParameter("allowMove", allowMove, KxValueType.Int, false);
                result.AddParameter("allowCharges", allowCharges, KxValueType.Int, false);
                result.AddParameter("allowRefunds", allowRefunds, KxValueType.Int, false);
                result.AddParameter("allowWriteOff", allowWriteOff, KxValueType.Int, false);
                result.AddParameter("creationDate", creationDate, KxValueType.DateTime, false);
                result.AddParameter("creationUserId", creationUserId, KxValueType.Int, false);
                result.AddParameter("inactive", inactive, KxValueType.Boolean, false);
                result.AddParameter("stage", stage, KxValueType.Int, true);
            }
            return result;
        }


        public override void LoadFromXml(string xml, XmlNode node)
        {
            dBIdentifier = KxXmlUtils.GetNodeValue(node, "debtorstatusid", 0);
            description = KxXmlUtils.GetNodeValue(node, "description", "");
            allowApplication = KxXmlUtils.GetNodeValue(node, "allowapplication", 0);
            allowAllocation = KxXmlUtils.GetNodeValue(node, "allowallocation", 0);
            allowMove = KxXmlUtils.GetNodeValue(node, "allowmove", 0);
            allowCharges = KxXmlUtils.GetNodeValue(node, "allowcharges", 0);
            allowRefunds = KxXmlUtils.GetNodeValue(node, "allowrefunds", 0);
            allowWriteOff = KxXmlUtils.GetNodeValue(node, "allowwriteoff", 0);
            creationDate = KxXmlUtils.GetNodeValueDT(node, "creationdate", null);
            creationUserId = KxXmlUtils.GetNodeValue(node, "creationuserid", 0);
            inactive = KxXmlUtils.GetNodeValue(node, "inactive", false);
            stage = KxXmlUtils.GetNodeValue(node, "stage", 0);
        }
    }
}

