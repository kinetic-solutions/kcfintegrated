using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;

namespace Kx.Objects.Student
{
    [Serializable]
    public class KxStudentYear : KxBaseClass
    {
        #region Attributes & Properties

        protected string   description;
        protected int      sortOrder;
        protected bool     depositDue;

        public int      StudentYearId
        {
            get
            { return base.dBIdentifier;}
        }

        public string   Description
        {
            get
            { return description;}
            set
            { 
                AddToAudit("Description", description, value);
                description = value;
            }
        }

        public int      SortOrder
        {
            get
            { return sortOrder;}
            set
            { 
                AddToAudit("SortOrder", sortOrder, value);
                sortOrder = value;
            }
        }

        public bool     DepositDue
        {
            get
            { return depositDue;}
            set
            { 
                AddToAudit("DepositDue", depositDue, value);
                depositDue = value;
            }
        }


        #endregion 


        public KxStudentYear(KxBaseClass parent):base(parent)
        {}


        public KxStudentYear(KxBaseClass parent, XmlNode node):base(parent)
        {
             LoadFromXml("", node);
        }

        public KxStudentYear(KxBaseClass parent, int studentyearid)
            : base(parent)
        {
             base.dBIdentifier = studentyearid;
             Load();
        }


        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("studentyears", "StudentYears", TargetType.Table);

            result.AutoKeyField = new KxDataValue("StudentYearId",StudentYearId,KxValueType.Int);


            if ((actiontype == ActionType.Update) && (StudentYearId == 0))
            {
                result.Action = ActionType.Insert;
            }
            else
            {
                result.Action = actiontype;
            }

            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {
                   result.AddParameter("sortOrder", sortOrder, KxValueType.Int, false);
                   result.AddParameter("depositDue", depositDue, KxValueType.Boolean, false);
            }
            return result;
        }


        public override  void LoadFromXml(string xml,XmlNode node)
        {
            dBIdentifier  = KxXmlUtils.GetNodeValue(node,"studentyearid",0);
            description  = KxXmlUtils.GetNodeValue(node,"description","");
            sortOrder  = KxXmlUtils.GetNodeValue(node,"sortorder",0);
            depositDue  = KxXmlUtils.GetNodeValue(node,"depositdue",false);
        }
    }
}

