using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;
using Kx.Objects.Student.Web;

namespace Kx.Objects.Student
{
    [Serializable]
    public class KxeInductionArrivalSlot : KxBaseClass
    {
        #region Attributes & Properties

        protected int      locationId;
        protected int      academicYearId;
        protected DateTime ?arrivalDate;
        protected int      arrivalHourFrom;
        protected int      arrivalHourTo;
        protected int      maxArrivals;
        protected DateTime ?canChangeUntil;
        protected DateTime ?creationDate;
        protected int      creationUserId;

        protected bool isCurrent = false;

        public int      ArrivalSlotId
        {
            get
            { return base.dBIdentifier;}
        }

        public int      LocationId
        {
            get
            { return locationId;}
            set
            { 
                AddToAudit("LocationId", locationId, value);
                locationId = value;
            }
        }

        public DateTime ?ArrivalDate
        {
            get
            { return arrivalDate;}
            set
            { 
                AddToAudit("ArrivalDate", arrivalDate, value);
                arrivalDate = value;
            }
        }

        public int      ArrivalHourFrom
        {
            get { return GetHour(arrivalHourFrom); }
        }

        private int GetHour(int hour)
        {
            if ((hour & 64) == 64)
            {
                return hour - 64;
            }

            return hour;
        }

        public int ArrivalMinuteFrom
        {
            get { return GetMinute(arrivalHourFrom); }
        }

        private int GetMinute(int hour)
        {
            if ((hour & 64) == 64)
            {
                return 30;
            }

            return 0;
        }

        public int      ArrivalHourTo
        {
            get { return GetHour(arrivalHourTo); }
        }

        public int ArrivalMinuteTo
        {
            get { return GetMinute(arrivalHourTo); }
        }

        public int      MaxArrivals
        {
            get
            { return maxArrivals;}
            set
            { 
                AddToAudit("MaxArrivals", maxArrivals, value);
                maxArrivals = value;
            }
        }

        public DateTime ?CanChangeUntil
        {
            get
            { return canChangeUntil;}
            set
            { 
                AddToAudit("CanChangeUntil", canChangeUntil, value);
                canChangeUntil = value;
            }
        }

        public DateTime ?CreationDate
        {
            get
            { return creationDate;}
            set
            { 
                AddToAudit("CreationDate", creationDate, value);
                creationDate = value;
            }
        }

        public int      CreationUserId
        {
            get
            { return creationUserId;}
            set
            { 
                AddToAudit("CreationUserId", creationUserId, value);
                creationUserId = value;
            }
        }
        public string TimesString
        {
            get
            {
                return ArrivalHourFrom.ToString().PadLeft(2, '0') + ArrivalMinuteFrom.ToString().PadLeft(2, '0') + ArrivalHourTo.ToString().PadLeft(2, '0') + ArrivalMinuteTo.ToString().PadLeft(2, '0');
            }
        }

        public string FormattedArrivalFromTimeString
        {
            get { return KxDateTime.HourAndMinuteToString(ArrivalHourFrom, ArrivalMinuteFrom); }
        }

        public string FormattedArrivalToTimeString
        {
            get { return KxDateTime.HourAndMinuteToString(ArrivalHourTo, ArrivalMinuteTo); }
        }
       

        public string FormattedTimesString
        {
            get
            {
                return FormattedArrivalFromTimeString + " to " + FormattedArrivalToTimeString;
            }
        }

        public string FormattedString
        {
            get
            {
                return  KxDateTime.ToString(ArrivalDate,"D") + " between "+ FormattedArrivalFromTimeString + " and " + FormattedArrivalToTimeString;
            }
        }


        public bool IsAvailable
        {
            get
            {
                return ArrivalSlotId > 0;
            }
        }

        public bool HasCapacity
        {
            get
            {
                return !GetIsFull();
            }
        }

        public bool IsFull
        {
            get
            {
                return GetIsFull();
            }
        }


        public bool IsUnavailable
        {
            get
            {
                return !isCurrent && (IsFull || (CanChangeUntil != null && CanChangeUntil.Value < DateTime.Now)); 
            }
        }

        public bool IsCurrent
        {
            get
            {
                return isCurrent;
            }
            set
            {
                isCurrent = value;
            }
        }

        #endregion 


        public KxeInductionArrivalSlot(KxBaseClass parent):base(parent)
        {}


        public KxeInductionArrivalSlot(KxBaseClass parent, XmlNode node):base(parent)
        {
             LoadFromXml("", node);
        }

        public KxeInductionArrivalSlot(KxBaseClass parent,int      arrivalslotid):base(parent)
        {
             base.dBIdentifier = arrivalslotid;
             Load();
        }


        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("st2einductionarrivalslots", "ST2eInductionArrivalSlots", TargetType.Table);

            result.AutoKeyField = new KxDataValue("ArrivalSlotId",ArrivalSlotId,KxValueType.Int);


            if ((actiontype == ActionType.Update) && (ArrivalSlotId == 0))
            {
                result.Action = ActionType.Insert;
            }
            else
            {
                result.Action = actiontype;
            }

            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {
                   result.AddParameter("arrivalDate", arrivalDate, KxValueType.DateTime, true);
                   result.AddParameter("arrivalHourFrom", arrivalHourFrom, KxValueType.Int, true);
                   result.AddParameter("arrivalHourTo", arrivalHourTo, KxValueType.Int, false);
                   result.AddParameter("maxArrivals", maxArrivals, KxValueType.Int, true);
                   result.AddParameter("canChangeUntil", canChangeUntil, KxValueType.DateTime, true);
                   result.AddParameter("creationDate", creationDate, KxValueType.DateTime, true);
                   result.AddParameter("creationUserId", creationUserId, KxValueType.Int, true);
            }
            return result;
        }


        public override  void LoadFromXml(string xml,XmlNode node)
        {
            dBIdentifier  = KxXmlUtils.GetNodeValue(node,"arrivalslotid",0);
            locationId  = KxXmlUtils.GetNodeValue(node,"locationid",0);
            arrivalDate  = KxXmlUtils.GetNodeValueDT(node,"arrivaldate",null);
            arrivalHourFrom  = KxXmlUtils.GetNodeValue(node,"arrivalhourfrom",0);
            arrivalHourTo  = KxXmlUtils.GetNodeValue(node,"arrivalhourto",0);
            maxArrivals  = KxXmlUtils.GetNodeValue(node,"maxarrivals",0);
            canChangeUntil  = KxXmlUtils.GetNodeValueDT(node,"canchangeuntil",null);
            creationDate  = KxXmlUtils.GetNodeValueDT(node,"creationdate",null);
            creationUserId  = KxXmlUtils.GetNodeValue(node,"creationuserid",0);
        }

        public bool GetIsFull()
        {
            bool result = false;

            if (ArrivalSlotId > 0)
            {
                try
                {
                    result = !KxStudentObjectManager.ArrivalSlotCache.GetSlotAVailability(ArrivalSlotId, maxArrivals);
                }
                catch { }
            }

            return result;
        }
    }
}

