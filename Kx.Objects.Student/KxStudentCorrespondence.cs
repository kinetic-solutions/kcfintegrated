using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;

namespace Kx.Objects.Student
{
    [Serializable]
    public class KxStudentCorrespondence : KxBaseClass
    {
        #region Attributes & Properties

        protected int      kxStudentId;
        protected DateTime ?creationDate;
        protected int      creationUserId;
        protected string   documentType;
        protected string   description;
        protected string   documentName;
        protected byte[]    document;
        protected string   documentText;
        protected DateTime ?responseRequestedBy;
        protected DateTime ?responseReceived;
        protected int      applicationId;
        protected int      allocationId;
        protected int      templateId;
        protected int      documentTypeId;
        protected int      documentImageType;
        protected DateTime ?documentCreated;
        protected int      contactMethodId;
        protected string   printToFileDestination;
        protected int      postSourceAddressId;
        protected DateTime ?sent;
        protected int      massMailingId;

        public int      CorrespondenceId
        {
            get
            { return base.dBIdentifier;}
        }

        public int      KxStudentId
        {
            get
            { return kxStudentId;}
            set
            { 
                AddToAudit("KxStudentId", kxStudentId, value);
                kxStudentId = value;
            }
        }

        public DateTime ?CreationDate
        {
            get
            { return creationDate;}
            set
            { 
                AddToAudit("CreationDate", creationDate, value);
                creationDate = value;
            }
        }

        public int      CreationUserId
        {
            get
            { return creationUserId;}
            set
            { 
                AddToAudit("CreationUserId", creationUserId, value);
                creationUserId = value;
            }
        }

        public string   DocumentType
        {
            get
            { return documentType;}
            set
            { 
                AddToAudit("DocumentType", documentType, value);
                documentType = value;
            }
        }

        public string   Description
        {
            get
            { return description;}
            set
            { 
                AddToAudit("Description", description, value);
                description = value;
            }
        }

        public string   DocumentName
        {
            get
            { return documentName;}
            set
            { 
                AddToAudit("DocumentName", documentName, value);
                documentName = value;
            }
        }

        public byte[]    Document
        {
            get
            { return document;}
            set
            { 
                AddToAudit("Document", document, value);
                document = value;
            }
        }

        public string   DocumentText
        {
            get
            { return documentText;}
            set
            { 
                AddToAudit("DocumentText", documentText, value);
                documentText = value;
            }
        }

        public DateTime ?ResponseRequestedBy
        {
            get
            { return responseRequestedBy;}
            set
            { 
                AddToAudit("ResponseRequestedBy", responseRequestedBy, value);
                responseRequestedBy = value;
            }
        }

        public DateTime ?ResponseReceived
        {
            get
            { return responseReceived;}
            set
            { 
                AddToAudit("ResponseReceived", responseReceived, value);
                responseReceived = value;
            }
        }

        public int      ApplicationId
        {
            get
            { return applicationId;}
            set
            { 
                AddToAudit("ApplicationId", applicationId, value);
                applicationId = value;
            }
        }

        public int      AllocationId
        {
            get
            { return allocationId;}
            set
            { 
                AddToAudit("AllocationId", allocationId, value);
                allocationId = value;
            }
        }

        public int      TemplateId
        {
            get
            { return templateId;}
            set
            { 
                AddToAudit("TemplateId", templateId, value);
                templateId = value;
            }
        }

        public int      DocumentTypeId
        {
            get
            { return documentTypeId;}
            set
            { 
                AddToAudit("DocumentTypeId", documentTypeId, value);
                documentTypeId = value;
            }
        }

        public int      DocumentImageType
        {
            get
            { return documentImageType;}
            set
            { 
                AddToAudit("DocumentImageType", documentImageType, value);
                documentImageType = value;
            }
        }

        public DateTime ?DocumentCreated
        {
            get
            { return documentCreated;}
            set
            { 
                AddToAudit("DocumentCreated", documentCreated, value);
                documentCreated = value;
            }
        }

        public int      ContactMethodId
        {
            get
            { return contactMethodId;}
            set
            { 
                AddToAudit("ContactMethodId", contactMethodId, value);
                contactMethodId = value;
            }
        }

        public string   PrintToFileDestination
        {
            get
            { return printToFileDestination;}
            set
            { 
                AddToAudit("PrintToFileDestination", printToFileDestination, value);
                printToFileDestination = value;
            }
        }

        public int      PostSourceAddressId
        {
            get
            { return postSourceAddressId;}
            set
            { 
                AddToAudit("PostSourceAddressId", postSourceAddressId, value);
                postSourceAddressId = value;
            }
        }

        public DateTime ?Sent
        {
            get
            { return sent;}
            set
            { 
                AddToAudit("Sent", sent, value);
                sent = value;
            }
        }

        public int      MassMailingId
        {
            get
            { return massMailingId;}
            set
            { 
                AddToAudit("MassMailingId", massMailingId, value);
                massMailingId = value;
            }
        }

    
        #endregion 
        public KxStudentCorrespondence(KxBaseClass parent)
            : base(parent)
        {
            
        }

        public KxStudentCorrespondence(KxBaseClass parent,int      correspondenceid):base(parent)
        {
             base.dBIdentifier = correspondenceid;
             Load();
        }
        
        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("st2studentcorrespondence", "ST2StudentCorrespondence", TargetType.Table);

            result.AutoKeyField = new KxDataValue("CorrespondenceId",CorrespondenceId,KxValueType.Int);


            if ((actiontype == ActionType.Update) && (CorrespondenceId == 0))
            {
                result.Action = ActionType.Insert;
            }
            else
            {
                result.Action = actiontype;
            }

            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {
                   result.AddParameter("kxStudentId", kxStudentId, KxValueType.Int);
                   result.AddParameter("creationDate", creationDate, KxValueType.DateTime);
                   result.AddParameter("creationUserId", creationUserId, KxValueType.Int);
                   result.AddParameter("documentType", documentType, KxValueType.String);
                   result.AddParameter("description", description, KxValueType.String);
                   result.AddParameter("documentName", documentName, KxValueType.String);
                   result.AddParameter("document", document, KxValueType.Image);
                   result.AddParameter("documentText", documentText, KxValueType.String);
                   result.AddParameter("responseRequestedBy", responseRequestedBy, KxValueType.DateTime);
                   result.AddParameter("responseReceived", responseReceived, KxValueType.DateTime);
                   result.AddParameter("applicationId", applicationId, KxValueType.Int);
                   result.AddParameter("allocationId", allocationId, KxValueType.Int);
                   result.AddParameter("templateId", templateId, KxValueType.Int);
                   result.AddParameter("documentTypeId", documentTypeId, KxValueType.Int);
                   result.AddParameter("documentImageType", documentImageType, KxValueType.Int);
                   result.AddParameter("documentCreated", documentCreated, KxValueType.DateTime);
                   result.AddParameter("contactMethodId", contactMethodId, KxValueType.Int);
                   result.AddParameter("printToFileDestination", printToFileDestination, KxValueType.String);
                   result.AddParameter("postSourceAddressId", postSourceAddressId, KxValueType.Int);
                   result.AddParameter("sent", sent, KxValueType.DateTime);
                   result.AddParameter("massMailingId", massMailingId, KxValueType.Int);
            }
            return result;
        }


        public override  void LoadFromXml(string xml,XmlNode node)
        {
            dBIdentifier  = KxXmlUtils.GetNodeValue(node,"correspondenceid",0);
            kxStudentId  = KxXmlUtils.GetNodeValue(node,"kxstudentid",0);
            creationDate  = KxXmlUtils.GetNodeValueDT(node,"creationdate",null);
            creationUserId  = KxXmlUtils.GetNodeValue(node,"creationuserid",0);
            documentType  = KxXmlUtils.GetNodeValue(node,"documenttype","");
            description  = KxXmlUtils.GetNodeValue(node,"description","");
            documentName  = KxXmlUtils.GetNodeValue(node,"documentname","");
            document  = KxXmlUtils.GetNodeValueBA(node,"document",null);
            documentText  = KxXmlUtils.GetNodeValue(node,"documenttext","");
            responseRequestedBy  = KxXmlUtils.GetNodeValueDT(node,"responserequestedby",null);
            responseReceived  = KxXmlUtils.GetNodeValueDT(node,"responsereceived",null);
            applicationId  = KxXmlUtils.GetNodeValue(node,"applicationid",0);
            allocationId  = KxXmlUtils.GetNodeValue(node,"allocationid",0);
            templateId  = KxXmlUtils.GetNodeValue(node,"templateid",0);
            documentTypeId  = KxXmlUtils.GetNodeValue(node,"documenttypeid",0);
            documentImageType  = KxXmlUtils.GetNodeValue(node,"documentimagetype",0);
            documentCreated  = KxXmlUtils.GetNodeValueDT(node,"documentcreated",null);
            contactMethodId  = KxXmlUtils.GetNodeValue(node,"contactmethodid",0);
            printToFileDestination  = KxXmlUtils.GetNodeValue(node,"printtofiledestination","");
            postSourceAddressId  = KxXmlUtils.GetNodeValue(node,"postsourceaddressid",0);
            sent  = KxXmlUtils.GetNodeValueDT(node,"sent",null);
            massMailingId  = KxXmlUtils.GetNodeValue(node,"massmailingid",0);
        }
    }
}

