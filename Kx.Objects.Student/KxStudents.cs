﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Exceptions;
using Kx.Core.Utility;

namespace Kx.Objects.Student
{
    [Serializable]
    public class KxStudents : KxBaseClass
    {
        private string _surnameFilter;
        private string _fornameFilter;
        private string _studentIdFilter;
        private string _ucasFilter;
        private DateTime? _dobFilter;
        private List<KxStudent> items = new List<KxStudent>();
        public List<KxStudent> Items
        {
            get
            { return items; }
        }

        public KxStudents(KxBaseClass parent)
            : this(parent, string.Empty, string.Empty,string.Empty,string.Empty,null)
        {
        }

        public KxStudents(KxBaseClass parent, string surname, string forename, string studentId, string ucas, DateTime? dob)
            : base(parent)
        {
            _surnameFilter = surname;
            _fornameFilter = forename;
            _studentIdFilter = studentId;
            _ucasFilter = ucas;
            _dobFilter = dob;
            Load();
        }

        

        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = null;

            result = new KxData("kxproperties_getstudents", "KxProperties_GetStudents", TargetType.StoredProc, ActionType.ExecuteProcedure);

            if (!string.IsNullOrEmpty(_surnameFilter))
            {
                result.AddKeyField("Surname", _surnameFilter, KxValueType.String);
            }

            if (!string.IsNullOrEmpty(_fornameFilter))
            {
                result.AddKeyField("Forename", _fornameFilter, KxValueType.String);
            }

            if (!string.IsNullOrEmpty(_studentIdFilter))
            {
                result.AddKeyField("StudentID", _studentIdFilter, KxValueType.String);
            }

            if (!string.IsNullOrEmpty(_ucasFilter))
            {
                result.AddKeyField("UCASNumber", _ucasFilter, KxValueType.String);
            }

            if (_dobFilter.HasValue && _dobFilter.Value != DateTime.MinValue)
            {
                result.AddKeyField("DOB", _dobFilter.Value, KxValueType.DateTime);
            }

            return result;
        }

        public override bool LoadFromXml(string xml, string xpath)
        {
            items.Clear();
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xml);

                XmlNodeList nodes = doc.SelectNodes(@"//results/" + xpath + "/datarow");

                foreach (XmlNode node in nodes)
                {
                    if (node != null)
                    {
                        KxStudent student = new KxStudent(this);
                        student.LoadFromXml(string.Empty, node);
                        items.Add(student);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "LoadFromXml", ex.Message, xpath);
                LastError = ex.Message;
                return false;
            }
        }
    }
}
