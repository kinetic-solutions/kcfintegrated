using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;

namespace Kx.Objects.Student
{
    [Serializable]
    public class KxPaymentAllocation : KxBaseClass
    {
        #region Attributes & Properties

        protected int      fromStudentChargeId;
        protected int      fromStudentPaymentId;
        protected int      toStudentChargeId;
        protected int      toStudentPaymentId;
        protected decimal  amountAllocated;
        protected int      creationUserId;
        protected DateTime ?creationDate;

        public int      PaymentAllocationId
        {
            get
            { return base.dBIdentifier;}
        }

        public int      FromStudentChargeId
        {
            get
            { return fromStudentChargeId;}
            set
            { 
                AddToAudit("FromStudentChargeId", fromStudentChargeId, value);
                fromStudentChargeId = value;
            }
        }

        public int      FromStudentPaymentId
        {
            get
            { return fromStudentPaymentId;}
            set
            { 
                AddToAudit("FromStudentPaymentId", fromStudentPaymentId, value);
                fromStudentPaymentId = value;
            }
        }

        public int      ToStudentChargeId
        {
            get
            { return toStudentChargeId;}
            set
            { 
                AddToAudit("ToStudentChargeId", toStudentChargeId, value);
                toStudentChargeId = value;
            }
        }

        public int      ToStudentPaymentId
        {
            get
            { return toStudentPaymentId;}
            set
            { 
                AddToAudit("ToStudentPaymentId", toStudentPaymentId, value);
                toStudentPaymentId = value;
            }
        }

        public decimal  AmountAllocated
        {
            get
            { return amountAllocated;}
            set
            { 
                AddToAudit("AmountAllocated", amountAllocated, value);
                amountAllocated = value;
            }
        }

        public int      CreationUserId
        {
            get
            { return creationUserId;}
            set
            { 
                AddToAudit("CreationUserId", creationUserId, value);
                creationUserId = value;
            }
        }

        public DateTime ?CreationDate
        {
            get
            { return creationDate;}
            set
            { 
                AddToAudit("CreationDate", creationDate, value);
                creationDate = value;
            }
        }


        #endregion 


        public KxPaymentAllocation(KxBaseClass parent):base(parent)
        {}


        public KxPaymentAllocation(KxBaseClass parent, XmlNode node):base(parent)
        {
             LoadFromXml("", node);
        }

        public KxPaymentAllocation(KxBaseClass parent,int      paymentallocationid):base(parent)
        {
             base.dBIdentifier = paymentallocationid;
             Load();
        }


        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("st2paymentallocations", "ST2PaymentAllocations", TargetType.Table);

            result.AutoKeyField = new KxDataValue("PaymentAllocationId",PaymentAllocationId,KxValueType.Int);


            if ((actiontype == ActionType.Update) && (PaymentAllocationId == 0))
            {
                result.Action = ActionType.Insert;
            }
            else
            {
                result.Action = actiontype;
            }

            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {
                   result.AddParameter("fromStudentPaymentId", fromStudentPaymentId, KxValueType.Int, true);
                   result.AddParameter("toStudentChargeId", toStudentChargeId, KxValueType.Int, true);
                   result.AddParameter("toStudentPaymentId", toStudentPaymentId, KxValueType.Int, true);
                   result.AddParameter("amountAllocated", amountAllocated, KxValueType.Decimal, false);
                   result.AddParameter("creationUserId", creationUserId, KxValueType.Int, false);
                   result.AddParameter("creationDate", creationDate, KxValueType.DateTime, false);
            }
            return result;
        }


        public override  void LoadFromXml(string xml,XmlNode node)
        {
            dBIdentifier  = KxXmlUtils.GetNodeValue(node,"paymentallocationid",0);
            fromStudentChargeId  = KxXmlUtils.GetNodeValue(node,"fromstudentchargeid",0);
            fromStudentPaymentId  = KxXmlUtils.GetNodeValue(node,"fromstudentpaymentid",0);
            toStudentChargeId  = KxXmlUtils.GetNodeValue(node,"tostudentchargeid",0);
            toStudentPaymentId  = KxXmlUtils.GetNodeValue(node,"tostudentpaymentid",0);
            amountAllocated  = KxXmlUtils.GetNodeValue(node,"amountallocated",(decimal)0.00M);
            creationUserId  = KxXmlUtils.GetNodeValue(node,"creationuserid",0);
            creationDate  = KxXmlUtils.GetNodeValueDT(node,"creationdate",null);
        }
    }
}

