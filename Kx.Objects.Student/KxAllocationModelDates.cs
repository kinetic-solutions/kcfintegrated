using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Exceptions;
using Kx.Core.Utility;

namespace Kx.Objects.Student
{
    public class KxAllocationModelDates : KxBaseClass
    {
      
        private int allocationModelId;
        private int academicYearId;
        private List<KxAllocationModelDate> items = new List<KxAllocationModelDate>();

        public Decimal AdditionalCharges
        {
            get
            {
                return GetAdditionalCharges();
            }
        }

        public int AllocationModelId
        {
            get
            { return allocationModelId; }
            set
            { allocationModelId = value; }
        }

        public List<KxAllocationModelDate> Items
        {
            get
            { return items; }
        }

        public KxAllocationModelDate this[int index]
        {
            get
            { return items[index]; }
            set
            { items[index] = value; }
        }

        public int AcademicYearId
        {
            get
            {
                return academicYearId;
            }
            set
            {
                academicYearId = value;
            }
        }

        public DateTime StartDate
        {
            get
            {
                return GetStartDate();
            }
        }

        public DateTime EndDate
        {
            get
            {
                return GetEndDate();
            }
        }

        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = null;

            if (actiontype == ActionType.Select)
            {
                result = new KxData("allocationmodeldates", "st2allocationmodeldates", TargetType.Table);

                result.KeyFields.Add(new KxDataValue("allocationmodelId", allocationModelId, KxValueType.Int));
                result.KeyFields.Add(new KxDataValue("academicYearId", academicYearId, KxValueType.Int));

                result.AddOrderField("allocationmodeldateid", KxDataOrderType.Ascending);
            }
            return result;
        }

        public KxAllocationModelDates(KxBaseClass parent, int allocationModelId, int academicYearId) : base(parent)
        {
            this.AcademicYearId = academicYearId;
            this.allocationModelId = allocationModelId;
            Load();
        }

        public override bool LoadFromXml(string xml, string xpath)
        {
            items.Clear();
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xml);

                XmlNodeList nodes = doc.SelectNodes(@"//results/" + xpath + "/datarow");

                foreach (XmlNode node in nodes)
                {
                    if (node != null)
                    {
                        KxAllocationModelDate mad = new KxAllocationModelDate(this, node);
                        Items.Add(mad);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "LoadFromXml", ex.Message, xpath);
                LastError = ex.Message;
                return false;
            }
        }

        public Decimal GetTotalDays(bool all)
        {
            Decimal result = 0;

            foreach (KxAllocationModelDate date in Items)
            {
                if (all)
                {
                    result += date.Days;
                }
                else if (date.AllocateDuringPeriod == 1)
                {
                    result += date.Days;
                }
            }
            return result;
        }

        public Decimal GetTotalWeeks(bool all)
        {
            Decimal result = 0.0M;

            result = System.Math.Floor(GetTotalDays(all) / 7.0M );

            return result;
        }

        public Decimal GetTotalWeeksUnrounded(bool all)
        {
            Decimal result = 0.0M;

            result = GetTotalDays(all) / 7.0M;

            return result;
        }

        private Decimal GetAdditionalCharges()
        {
            Decimal result = 0.0M;

            foreach (KxAllocationModelDate date in Items)
            {
                if (date.AllocateDuringPeriod > 0)
                {
                    result += date.AdditionalCharges;
                }
            }
            return result;
        }

        private DateTime GetStartDate()
        {
            DateTime result = DateTime.Now.AddYears(10);


            foreach (KxAllocationModelDate date in Items)
            {
                if (date.AllocateDuringPeriod > 0)
                {
                    if (date.StartDate < result)
                    {
                        result = date.StartDate.Value;
                    }
                }
            }

            return result;
        }

        private DateTime GetEndDate()
        {
            DateTime result = DateTime.Now.AddYears(-10);


            foreach (KxAllocationModelDate date in Items)
            {
                if (date.AllocateDuringPeriod > 0)
                {
                    if (date.EndDate > result)
                    {
                        result = date.EndDate.Value;
                    }
                }
            }

            return result;
        }


    }
}
