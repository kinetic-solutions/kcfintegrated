using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;

namespace Kx.Objects.Student
{
    [Serializable]
    public class KxRequestType : KxBaseClass
    {
        #region Attributes & Properties

        protected string _description;
        protected string _webKey;
        protected int _initialStatusId;
        protected int _creationUserId;
        protected DateTime? _creationDate;

        public int RequestTypeId
        {
            get { return base.dBIdentifier; }
        }

        public string Description
        {
            get { return _description; }
            set
            { 
                AddToAudit("Description", _description, value);
                _description = value;
            }
        }

        public string WebKey
        {
            get { return _webKey; }
            set
            { 
                AddToAudit("WebKey", _webKey, value);
                _webKey = value;
            }
        }

        public int InitialStatusId
        {
            get { return _initialStatusId; }
            set
            { 
                AddToAudit("InitialStatusId", _initialStatusId, value);
                _initialStatusId = value;
            }
        }

        public int CreationUserId
        {
            get { return _creationUserId; }
            set
            { 
                AddToAudit("CreationUserId", _creationUserId, value);
                _creationUserId = value;
            }
        }

        public DateTime? CreationDate
        {
            get { return _creationDate; }
            set
            { 
                AddToAudit("CreationDate", _creationDate, value);
                _creationDate = value;
            }
        }


        #endregion 


        public KxRequestType(KxBaseClass parent):base(parent)
        { }


        public KxRequestType(KxBaseClass parent, XmlNode node):base(parent)
        {
             LoadFromXml("", node);
        }

        public KxRequestType(KxBaseClass parent,int  requesttypeid):base(parent)
        {
             base.dBIdentifier = requesttypeid;
             Load();
        }


        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("st2requesttypes", "ST2RequestTypes", TargetType.Table);

            result.AutoKeyField = new KxDataValue("RequestTypeId",RequestTypeId,KxValueType.Int);


            if ((actiontype == ActionType.Update) && (RequestTypeId == 0))
            {
                result.Action = ActionType.Insert;
            }
            else
            {
                result.Action = actiontype;
            }

            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {
                result.AddParameter("description", _description, KxValueType.String, 50, false);
                result.AddParameter("webKey", _webKey, KxValueType.String, 10, false);
                result.AddParameter("initialStatusId", _initialStatusId, KxValueType.Int, false);
                result.AddParameter("creationUserId", _creationUserId, KxValueType.Int, false);
                result.AddParameter("creationDate", _creationDate, KxValueType.DateTime, false);
            }
            return result;
        }


        public override  void LoadFromXml(string xml,XmlNode node)
        {
            dBIdentifier  = KxXmlUtils.GetNodeValue(node,"requesttypeid",0);
            _description  = KxXmlUtils.GetNodeValue(node,"description",string.Empty);
            _webKey  = KxXmlUtils.GetNodeValue(node,"webkey",string.Empty);
            _initialStatusId  = KxXmlUtils.GetNodeValue(node,"initialstatusid",0);
            _creationUserId  = KxXmlUtils.GetNodeValue(node,"creationuserid",0);
            _creationDate  = KxXmlUtils.GetNodeValueDT(node,"creationdate",null);
        }

        public static KxRequestType GetST2RequestTypes (string webKey)
        {
            KxData data = new KxData("st2requesttypes", "ST2RequestTypes", TargetType.Table);
            data.AddKeyField("webkey", webKey, KxValueType.String);
            if (data.Execute ())
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(data.XmlResults);
                XmlNodeList nodes = doc.SelectNodes(@"//st2requesttypes/datarow");
                foreach(XmlNode node in nodes)
                {
                     return new KxRequestType(null, node);
                }
            }

            return null;
        }
    }
}