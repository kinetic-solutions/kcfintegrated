using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Exceptions;
using Kx.Core.Utility;

namespace Kx.Objects.Student
{
    public class KxStudentAllocations : KxBaseClass
    {
        private int kxStudentId;
        private List<KxStudentAllocation> items = new List<KxStudentAllocation>();

        public int KxStudentId
        {
            get
            { return kxStudentId; }
            set
            { kxStudentId = value; }
        }

        public List<KxStudentAllocation> Items
        {
            get
            { return items; }
        }

        public KxStudentAllocation this[int index]
        {
            get
            { return items[index]; }
            set
            { items[index] = value; }
        }

        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = null;

            if (actiontype == ActionType.Select)
            {
                result = new KxData("studentallocations", "StudentAllocationsByStudent", TargetType.StoredProc, ActionType.Select);

                result.Parameters.Add(new KxDataValue("kxStudentId", KxStudentId, KxValueType.Int));
            }
            return result;
        }

        public KxStudentAllocations(KxBaseClass parent, int kxstudentid) : base(parent)
        {
            this.KxStudentId = kxstudentid;
            Load();
        }

        public override bool LoadFromXml(string xml, string xpath)
        {
            items.Clear();
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xml);

                XmlNodeList nodes = doc.SelectNodes(@"//results/" + xpath + "/datarow");

                foreach (XmlNode node in nodes)
                {
                    if (node != null)
                    {
                        KxStudentAllocation all = new KxStudentAllocation(this, node);
                        items.Add(all);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "LoadFromXml", ex.Message, xpath);
                LastError = ex.Message;
                return false;
            }
        }

   
    }
}
