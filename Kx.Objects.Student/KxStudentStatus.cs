using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;

namespace Kx.Objects.Student
{
    [Serializable]
    public class KxStudentStatus : KxBaseClass
    {
        #region Attributes & Properties

        protected string   description;
        protected string   statusType;
        protected bool     hideAllocation;
        protected string   tasks;
        protected string   taskList;
        protected string   foregroundColour;
        protected string   backgroundColour;
        protected int      statusOrder;

        public int      StudentStatusId
        {
            get
            { return base.dBIdentifier;}
        }

        public string   Description
        {
            get
            { return description;}
            set
            { 
                AddToAudit("Description", description, value);
                description = value;
            }
        }

        public string   StatusType
        {
            get
            { return statusType;}
            set
            { 
                AddToAudit("StatusType", statusType, value);
                statusType = value;
            }
        }

        public bool     HideAllocation
        {
            get
            { return hideAllocation;}
            set
            { 
                AddToAudit("HideAllocation", hideAllocation, value);
                hideAllocation = value;
            }
        }

        public string   Tasks
        {
            get
            { return tasks;}
            set
            { 
                AddToAudit("Tasks", tasks, value);
                tasks = value;
            }
        }

        public string   TaskList
        {
            get
            { return taskList;}
            set
            { 
                AddToAudit("TaskList", taskList, value);
                taskList = value;
            }
        }

        public string   ForegroundColour
        {
            get
            { return foregroundColour;}
            set
            { 
                AddToAudit("ForegroundColour", foregroundColour, value);
                foregroundColour = value;
            }
        }

        public string   BackgroundColour
        {
            get
            { return backgroundColour;}
            set
            { 
                AddToAudit("BackgroundColour", backgroundColour, value);
                backgroundColour = value;
            }
        }

        public int      StatusOrder
        {
            get
            { return statusOrder;}
            set
            { 
                AddToAudit("StatusOrder", statusOrder, value);
                statusOrder = value;
            }
        }

        public bool IsCF
        {
            get
            { return description.ToUpper() == "CF"; }
        }

        public bool IsUF
        {
            get
            { return description.ToUpper() == "UF"; }
        }

        public bool IsCFOrUF
        {
            get
            { return IsUF || IsCF; }
        }

        public bool Withdrawn
        {
            get
            {
                return HideAllocation;
            }
        }

        public bool NotWithdrawn
        {
            get
            {
                return !HideAllocation;
            }
        }

        #endregion 

        public KxStudentStatus(KxBaseClass parent,int      studentstatusid):base(parent)
        {
             base.dBIdentifier = studentstatusid;
             Load();
        }


        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("studentstatus", "StudentStatus", TargetType.Table);

            result.AutoKeyField = new KxDataValue("StudentStatusId",StudentStatusId,KxValueType.Int);


            if ((actiontype == ActionType.Update) && (StudentStatusId == 0))
            {
                result.Action = ActionType.Insert;
            }
            else
            {
                result.Action = actiontype;
            }

            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {
                   result.AddParameter("description", description, KxValueType.String);
                   result.AddParameter("statusType", statusType, KxValueType.String);
                   result.AddParameter("hideAllocation", hideAllocation, KxValueType.Boolean);
                   result.AddParameter("tasks", tasks, KxValueType.String);
                   result.AddParameter("taskList", taskList, KxValueType.String);
                   result.AddParameter("foregroundColour", foregroundColour, KxValueType.String);
                   result.AddParameter("backgroundColour", backgroundColour, KxValueType.String);
                   result.AddParameter("statusOrder", statusOrder, KxValueType.Int);
            }
            return result;
        }


        public override  void LoadFromXml(string xml,XmlNode node)
        {
            dBIdentifier  = KxXmlUtils.GetNodeValue(node,"studentstatusid",0);
            description  = KxXmlUtils.GetNodeValue(node,"description","");
            statusType  = KxXmlUtils.GetNodeValue(node,"statustype","");
            hideAllocation  = KxXmlUtils.GetNodeValue(node,"hideallocation",false);
            tasks  = KxXmlUtils.GetNodeValue(node,"tasks","");
            taskList  = KxXmlUtils.GetNodeValue(node,"tasklist","");
            foregroundColour  = KxXmlUtils.GetNodeValue(node,"foregroundcolour","");
            backgroundColour  = KxXmlUtils.GetNodeValue(node,"backgroundcolour","");
            statusOrder  = KxXmlUtils.GetNodeValue(node,"statusorder",0);
        }
    }
}

