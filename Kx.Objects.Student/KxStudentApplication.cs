using System;
using System.Drawing;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;
using Kx.Objects.Common;

namespace Kx.Objects.Student
{
    [Serializable]
    public class KxStudentApplication : KxBaseClass
    {
        #region Attributes & Properties
        private static string _eInductionsCompleteColumnExists;

        protected int      kxStudentId;
        protected DateTime ?applicationDate;
        protected int      licenceTypeId;
        protected int      academicYearId;
        protected int      studentYearId;
        protected int      courseTypeId;
        protected DateTime ?creationDate;
        protected int      creationUserId;
        protected DateTime ?waitingListDate;
        protected int      waitingListUserId;
        protected int      waitingListReasonId;
        protected DateTime ?inactiveDate;
        protected int      inactiveUserId;
        protected int      inactiveReasonId;
        protected string   inactiveNotes;
        protected string   waitingListNotes;
        protected int      applicationTypeId;
        protected int      waitingListId;
        protected int      siteId;
        protected int      areaId;
        protected int      blockId;
        protected int      applicationSourceId;
        protected int      paymentScheduleTypeId;
        protected bool     manualAllocationOnly;
        protected int _sessionId;
        protected DateTime? _sessionStartDate;
        protected DateTime? _sessionEndDate;
        protected bool _eInductionsComplete;

        protected KxStudentAllocations allocations = null;
        protected KxAcademicYear academicYear = null;
        protected KxAcademicYearSession session = null;
        protected KxStudentPayments payments = null;
        protected KxStudentReason inactiveReason = null;
        protected KxCustomFields2Values customValues = null;
        protected KxStudentStatements studentStatements = null;

        public int      ApplicationId
        {
            get
            { return base.dBIdentifier;}
        }

        public int      KxStudentId
        {
            get
            { return kxStudentId;}
            set
            { 
                AddToAudit("KxStudentId", kxStudentId, value);
                kxStudentId = value;
            }
        }

        public DateTime ?ApplicationDate
        {
            get
            { return applicationDate;}
            set
            { 
                AddToAudit("ApplicationDate", applicationDate, value);
                applicationDate = value;
            }
        }

        public int      LicenceTypeId
        {
            get
            { return licenceTypeId;}
            set
            { 
                AddToAudit("LicenceTypeId", licenceTypeId, value);
                licenceTypeId = value;
            }
        }

        public int      AcademicYearId
        {
            get
            { return academicYearId;}
            set
            { 
                AddToAudit("AcademicYearId", academicYearId, value);
                academicYearId = value;
            }
        }

        public int SessionId
        {
            get { return _sessionId; }
            set
            {
                AddToAudit("SessionID", _sessionId, value);
                _sessionId = value;
            }
        }

        public int      StudentYearId
        {
            get
            { return studentYearId;}
            set
            { 
                AddToAudit("StudentYearId", studentYearId, value);
                studentYearId = value;
            }
        }

        public int      CourseTypeId
        {
            get
            { return courseTypeId;}
            set
            { 
                AddToAudit("CourseTypeId", courseTypeId, value);
                courseTypeId = value;
            }
        }

        public DateTime ?CreationDate
        {
            get
            { return creationDate;}
            set
            { 
                AddToAudit("CreationDate", creationDate, value);
                creationDate = value;
            }
        }

        public int      CreationUserId
        {
            get
            { return creationUserId;}
            set
            { 
                AddToAudit("CreationUserId", creationUserId, value);
                creationUserId = value;
            }
        }

        public DateTime ?WaitingListDate
        {
            get
            { return waitingListDate;}
            set
            { 
                AddToAudit("WaitingListDate", waitingListDate, value);
                waitingListDate = value;
            }
        }

        public int      WaitingListUserId
        {
            get
            { return waitingListUserId;}
            set
            { 
                AddToAudit("WaitingListUserId", waitingListUserId, value);
                waitingListUserId = value;
            }
        }

        public int      WaitingListReasonId
        {
            get
            { return waitingListReasonId;}
            set
            { 
                AddToAudit("WaitingListReasonId", waitingListReasonId, value);
                waitingListReasonId = value;
            }
        }

        public DateTime ?InactiveDate
        {
            get
            { return inactiveDate;}
            set
            { 
                AddToAudit("InactiveDate", inactiveDate, value);
                inactiveDate = value;
            }
        }

        public int      InactiveUserId
        {
            get
            { return inactiveUserId;}
            set
            { 
                AddToAudit("InactiveUserId", inactiveUserId, value);
                inactiveUserId = value;
            }
        }

        public int      InactiveReasonId
        {
            get
            { return inactiveReasonId;}
            set
            { 
                AddToAudit("InactiveReasonId", inactiveReasonId, value);
                inactiveReasonId = value;
            }
        }

        public KxStudentReason InactiveReason
        {
            get
            {
                if (inactiveReason == null && inactiveReasonId > 0)
                {
                    inactiveReason = new KxStudentReason(this, inactiveReasonId);
                }
                return inactiveReason;
            }
        }

        public string   InactiveNotes
        {
            get
            { return inactiveNotes;}
            set
            { 
                AddToAudit("InactiveNotes", inactiveNotes, value);
                inactiveNotes = value;
            }
        }

        public string   WaitingListNotes
        {
            get
            { return waitingListNotes;}
            set
            { 
                AddToAudit("WaitingListNotes", waitingListNotes, value);
                waitingListNotes = value;
            }
        }

        public int      ApplicationTypeId
        {
            get
            { return applicationTypeId;}
            set
            { 
                AddToAudit("ApplicationTypeId", applicationTypeId, value);
                applicationTypeId = value;
            }
        }

        public int      WaitingListId
        {
            get
            { return waitingListId;}
            set
            { 
                AddToAudit("WaitingListId", waitingListId, value);
                waitingListId = value;
            }
        }

        public int      SiteId
        {
            get
            { return siteId;}
            set
            { 
                AddToAudit("SiteId", siteId, value);
                siteId = value;
            }
        }

        public int      AreaId
        {
            get
            { return areaId;}
            set
            { 
                AddToAudit("AreaId", areaId, value);
                areaId = value;
            }
        }

        public int      BlockId
        {
            get
            { return blockId;}
            set
            { 
                AddToAudit("BlockId", blockId, value);
                blockId = value;
            }
        }

        public int      ApplicationSourceId
        {
            get
            { return applicationSourceId;}
            set
            { 
                AddToAudit("ApplicationSourceId", applicationSourceId, value);
                applicationSourceId = value;
            }
        }

        public int      PaymentScheduleTypeId
        {
            get
            { return paymentScheduleTypeId;}
            set
            { 
                AddToAudit("PaymentScheduleTypeId", paymentScheduleTypeId, value);
                paymentScheduleTypeId = value;
            }
        }

        public bool     ManualAllocationOnly
        {
            get
            { return manualAllocationOnly;}
            set
            { 
                AddToAudit("ManualAllocationOnly", manualAllocationOnly, value);
                manualAllocationOnly = value;
            }
        }

        public KxStudentAllocations Allocations
        {
            get
            {
                if (allocations == null)
                {
                    allocations = new KxStudentAllocations(this,ApplicationId);
                }
                return allocations;
            }
            set
            { allocations = value; }
        }


        public DateTime? SessionStartDate
        {
            get { return _sessionStartDate; }
            set
            {
                AddToAudit("SessionStartDate", _sessionStartDate, value);
                _sessionStartDate = value;
            }
        }

        public DateTime? SessionEndDate
        {
            get { return _sessionEndDate; }
            set
            {
                AddToAudit("SessionEndDate", _sessionEndDate, value);
                _sessionEndDate = value;
            }
        }

        public KxAcademicYear AcademicYear
        {
            get
            {
                if (AcademicYearId > 0 && academicYear == null)
                {
                    academicYear = new KxAcademicYear(this, AcademicYearId);
                }
                return academicYear;
            }
        }

        public KxAcademicYearSession Session
        {
            get
            {
                if (_sessionId > 0)
                {
                    session = AcademicYear.Sessions.FirstOrDefault(s => s.SessionId == _sessionId);
                }

                return session ?? new KxAcademicYearCoreSession(AcademicYear);
            }
        }

        public KxStudentPayments Payments
        {
            get
            {
                if (payments == null)
                {
                    payments = new KxStudentPayments(this, ApplicationId, KxPaymentSource.Application);
                }
                return payments;
            }
            set
            { payments = value; }
        }

        public KxCustomFields2Values CustomValues
        {
            get
            {
                if (customValues == null)
                {
                    customValues = new KxCustomFields2Values(this, "ST2StudentApplication", ApplicationId);
                }
                return customValues;
            }
            set
            {
                customValues = value;
            }
        }

        public bool Expired
        {
            get
            {
                return InactiveDate.Value > KxDateTime.KxApplicableDate;
            }
        }

        public KxStudentStatements StudentStatements
        {
            get
            {
                if (studentStatements == null)
                {
                    studentStatements = new KxStudentStatements(this, KxStudentId,academicYearId);
                }
                return studentStatements;
            }
            set
            {
                studentStatements = value;
            }
        }

        public bool eInductionsComplete
        {
            get { return _eInductionsComplete; }
            set
            {
                AddToAudit("eInductionsComplete", _eInductionsComplete, value);
                _eInductionsComplete = value;
            }
        }

        #endregion 

        public KxStudentApplication(KxBaseClass parent) : base(parent)
        {
        }

        public KxStudentApplication(KxBaseClass parent, int applicationid)
            : this(parent)
        {
             base.dBIdentifier = applicationid;
             Load();
        }

        public KxStudentApplication(KxBaseClass parent, XmlNode node): base(parent)
        {
            LoadFromXml("",node);
        }


        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("st2studentapplication", "ST2StudentApplications", TargetType.Table);

            result.AutoKeyField = new KxDataValue("ApplicationId",ApplicationId,KxValueType.Int);


            if ((actiontype == ActionType.Update) && (ApplicationId == 0))
            {
                result.Action = ActionType.Insert;
            }
            else
            {
                result.Action = actiontype;
            }

            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {
                result.AddParameter("kxStudentId", kxStudentId, KxValueType.Int);
                result.AddParameter("applicationDate", applicationDate, KxValueType.DateTime);
                result.AddParameter("licenceTypeId", licenceTypeId, KxValueType.Int);
                result.AddParameter("academicYearId", academicYearId, KxValueType.Int);
                result.AddParameter("studentYearId", studentYearId, KxValueType.Int);
                result.AddParameter("courseTypeId", courseTypeId, KxValueType.Int);
                result.AddParameter("creationDate", creationDate, KxValueType.DateTime);
                result.AddParameter("creationUserId", creationUserId, KxValueType.Int);
                result.AddParameter("waitingListDate", waitingListDate, KxValueType.DateTime);
                result.AddParameter("waitingListUserId", waitingListUserId, KxValueType.Int);
                result.AddParameter("waitingListReasonId", waitingListReasonId, KxValueType.Int);
                result.AddParameter("inactiveDate", inactiveDate, KxValueType.DateTime);
                result.AddParameter("inactiveUserId", inactiveUserId, KxValueType.Int);
                result.AddParameter("inactiveReasonId", inactiveReasonId, KxValueType.Int);
                result.AddParameter("inactiveNotes", inactiveNotes, KxValueType.String);
                result.AddParameter("waitingListNotes", waitingListNotes, KxValueType.String);
                result.AddParameter("applicationTypeId", applicationTypeId, KxValueType.Int);
                result.AddParameter("waitingListId", waitingListId, KxValueType.Int);
                result.AddParameter("siteId", siteId, KxValueType.Int);
                result.AddParameter("areaId", areaId, KxValueType.Int);
                result.AddParameter("blockId", blockId, KxValueType.Int);
                result.AddParameter("applicationSourceId", applicationSourceId, KxValueType.Int);
                result.AddParameter("paymentScheduleTypeId", paymentScheduleTypeId, KxValueType.Int);
                result.AddParameter("manualAllocationOnly", manualAllocationOnly, KxValueType.Boolean);

                const string STR_No = "No";
                const string STR_Yes = "Yes";
                try
                {
                    if (string.IsNullOrEmpty(_eInductionsCompleteColumnExists))
                    {
                        KxData data = new KxData("dd", "SELECT TOP 1 eInductionsComplete FROM ST2StudentApplications", TargetType.Static);
                        if (data.Execute())
                        {
                            _eInductionsCompleteColumnExists = STR_Yes;
                        }
                        else
                        {
                            _eInductionsCompleteColumnExists = STR_No;
                        }
                    }
                }
                catch (Exception)
                {
                    _eInductionsCompleteColumnExists = STR_No;
                }

                if (_eInductionsCompleteColumnExists == STR_Yes)
                {
                    result.AddParameter("einductionscomplete", _eInductionsComplete, KxValueType.Boolean);
                }
            }
            return result;
        }

        public override void LoadFromXml(string xml, XmlNode node)
        {
            Children.Remove(allocations);
            allocations = null;
            dBIdentifier = KxXmlUtils.GetNodeValue(node, "applicationid", 0);
            kxStudentId = KxXmlUtils.GetNodeValue(node, "kxstudentid", 0);
            applicationDate = KxXmlUtils.GetNodeValueDT(node, "applicationdate", null);
            licenceTypeId = KxXmlUtils.GetNodeValue(node, "licencetypeid", 0);
            academicYearId = KxXmlUtils.GetNodeValue(node, "academicyearid", 0);
            studentYearId = KxXmlUtils.GetNodeValue(node, "studentyearid", 0);
            courseTypeId = KxXmlUtils.GetNodeValue(node, "coursetypeid", 0);
            creationDate = KxXmlUtils.GetNodeValueDT(node, "creationdate", null);
            creationUserId = KxXmlUtils.GetNodeValue(node, "creationuserid", 0);
            waitingListDate = KxXmlUtils.GetNodeValueDT(node, "waitinglistdate", null);
            waitingListUserId = KxXmlUtils.GetNodeValue(node, "waitinglistuserid", 0);
            waitingListReasonId = KxXmlUtils.GetNodeValue(node, "waitinglistreasonid", 0);
            inactiveDate = KxXmlUtils.GetNodeValueDT(node, "inactivedate", null);
            inactiveUserId = KxXmlUtils.GetNodeValue(node, "inactiveuserid", 0);
            inactiveReasonId = KxXmlUtils.GetNodeValue(node, "inactivereasonid", 0);
            inactiveNotes = KxXmlUtils.GetNodeValue(node, "inactivenotes", "");
            waitingListNotes = KxXmlUtils.GetNodeValue(node, "waitinglistnotes", "");
            applicationTypeId = KxXmlUtils.GetNodeValue(node, "applicationtypeid", 0);
            waitingListId = KxXmlUtils.GetNodeValue(node, "waitinglistid", 0);
            siteId = KxXmlUtils.GetNodeValue(node, "siteid", 0);
            areaId = KxXmlUtils.GetNodeValue(node, "areaid", 0);
            blockId = KxXmlUtils.GetNodeValue(node, "blockid", 0);
            applicationSourceId = KxXmlUtils.GetNodeValue(node, "applicationsourceid", 0);
            paymentScheduleTypeId = KxXmlUtils.GetNodeValue(node, "paymentscheduletypeid", 0);
            manualAllocationOnly = KxXmlUtils.GetNodeValue(node, "manualallocationonly", false);
            _sessionId = KxXmlUtils.GetNodeValue(node, "sessionid", 0);
            _sessionStartDate = KxXmlUtils.GetNodeValueDT(node, "sessionstartdate", null);
            _sessionEndDate = KxXmlUtils.GetNodeValueDT(node, "sessionenddate", null);
            _eInductionsComplete = KxXmlUtils.GetNodeValue(node, "einductionscomplete", false);
        }
    }
}

