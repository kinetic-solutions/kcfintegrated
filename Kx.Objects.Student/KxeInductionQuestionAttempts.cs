using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Exceptions;
using Kx.Core.Utility;
using Kx.Objects.Student.Web;

namespace Kx.Objects.Student
{
    public class KxeInductionQuestionAttempts : KxBaseClass
    {
        private List<KxeInductionQuestionAttempt> items = new List<KxeInductionQuestionAttempt>();

        public List<KxeInductionQuestionAttempt> Items
        {
            get
            { return items; }
        }

        public KxeInductionQuestionAttempt this[int index]
        {
            get
            { return items[index]; }
            set
            { items[index] = value; }
        }

        public KxeInductionQuestionAttempts(KxBaseClass parent, int applicationid)
            : base(parent)
        {
            base.dBIdentifier = applicationid;
             Load();
        }

        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = null;

            if (actiontype == ActionType.Select)
            {
                result = new KxData("attempt", "ST2eInductionQuestionAttempt", TargetType.Table, ActionType.Select);

                result.KeyFields.Add(new KxDataValue("applicationid", dBIdentifier, KxValueType.Int));
            }
            return result;
        }

        public override bool LoadFromXml(string xml, string xpath)
        {
            items.Clear();
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xml);

                XmlNodeList nodes = doc.SelectNodes(@"//results/" + xpath + "/datarow");

                foreach (XmlNode node in nodes)
                {
                    if (node != null)
                    {
                        KxeInductionQuestionAttempt item = new KxeInductionQuestionAttempt(this, node);
                        items.Add(item);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "LoadFromXml", ex.Message, xpath);
                LastError = ex.Message;
                return false;
            }
        }

        public KxeInductionQuestionAttempt GeteInductionQuestionAttempt(int id)
        {
            KxeInductionQuestionAttempt result = null;

            foreach (KxeInductionQuestionAttempt i in Items)
            {
                if (i.QuestionId == id)
                {
                    result = i;
                    break;
                }
            }

            if ((result == null) && (KxStudentObjectManager.InductionQuestions.GeteInductionQuestion(id) != null))
            {
                result = new KxeInductionQuestionAttempt(this);
                result.ApplicationId = dBIdentifier;
                result.QuestionId = id;
                items.Add(result);
            }

            return result;
        }

    }
}

