using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Exceptions;
using Kx.Core.Utility;

namespace Kx.Objects.Student
{
    public class KxAllocationModelCache : KxBaseClass
    {
        protected List<KxAllocationModel> items = null;

        public List<KxAllocationModel> Items
        {
            get
            {
                if (items == null)
                {
                    items = new List<KxAllocationModel>();
                }
                return items; 
            }
        }

        public KxAllocationModel this[int index]
        {
            get
            { return items[index]; }
            set
            { items[index] = value; }
        }

    

        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = null;

            if (actiontype == ActionType.Select)
            {
                result = new KxData("allocationmodels", "st2allocationmodels", TargetType.Table);
            }
            return result;
        }

        public KxAllocationModelCache(KxBaseClass parent) : base(parent)
        {
            
        }

        public override bool LoadFromXml(string xml, string xpath)
        {
            items.Clear();
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xml);

                XmlNodeList nodes = doc.SelectNodes(@"//results/" + xpath + "/datarow");

                foreach (XmlNode node in nodes)
                {
                    if (node != null)
                    {
                        KxAllocationModel mad = new KxAllocationModel(this, node);
                        Items.Add(mad);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "LoadFromXml", ex.Message, xpath);
                LastError = ex.Message;
                return false;
            }
        }

        public KxAllocationModel GetModelById(int id, int academicYearId)
        {
            KxAllocationModel result = null;

            foreach (KxAllocationModel model in Items)
            {
                if (model.AllocationModelId == id && model.AcademicYearId == academicYearId)
                {
                    result = model;
                    break;
                }
            }

            if (result == null)
            {
                KxAllocationModel dummy = new KxAllocationModel(this, id, academicYearId);

                if (dummy.AllocationModelId == id)
                {
                    Items.Add(dummy);
                    result = dummy;
                }
            }

            return result;
        }
   

    }
}
