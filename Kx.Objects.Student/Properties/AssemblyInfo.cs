using System.Reflection;
using System.Resources;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Kx.Objects.Student")]
[assembly: AssemblyDescription("Kinetic Common Framework - Student Accommodation Objects")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Kinetic Solutions Ltd")]
[assembly: AssemblyProduct("Kx.Objects.Student")]
[assembly: AssemblyCopyright("Copyright ©  2020")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following Guid is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("46ee7d88-20fa-41d5-a852-760f9e7613c1")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Revision and Build Numbers 
// by using the '*' as shown below:
[assembly: AssemblyVersion("2020.1.*")]
[assembly: AssemblyFileVersion("2020.1.5.0")]
[assembly: NeutralResourcesLanguageAttribute("en-GB")]
[assembly: InternalsVisibleTo("KCF.Student.UnitTesting")]