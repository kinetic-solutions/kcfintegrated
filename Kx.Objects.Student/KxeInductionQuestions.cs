using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Exceptions;
using Kx.Core.Utility;

namespace Kx.Objects.Student
{
    public class KxeInductionQuestions : KxBaseClass
    {
        protected List<KxeInductionQuestion> items = null;

        public List<KxeInductionQuestion> Items
        {
            get
            {
                if (items == null)
                {
                    items = new List<KxeInductionQuestion>();
                }                
                return items; 
            }
        }

        public KxeInductionQuestion this[int index]
        {
            get
            { return Items[index]; }
            set
            { Items[index] = value; }
        }

        public KxeInductionQuestions(KxBaseClass parent): base(parent)
        {
            Load();
        }

        public KxeInductionQuestions(KxBaseClass parent,int      academicyearid):base(parent)
        {
            base.dBIdentifier = academicyearid;
            Load();
        }

        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = null;

            if (actiontype == ActionType.Select)
            {
                result = new KxData("st2einductionquestions", "ST2eInductionQuestions", TargetType.Table, ActionType.Select);

                if (dBIdentifier > 0)
                {
                    result.Parameters.Add(new KxDataValue("academicyearid", dBIdentifier, KxValueType.Int));
                }
            }
            return result;
        }

        public override bool LoadFromXml(string xml, string xpath)
        {
            Items.Clear();
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xml);

                XmlNodeList nodes = doc.SelectNodes(@"//results/" + xpath + "/datarow");

                foreach (XmlNode node in nodes)
                {
                    if (node != null)
                    {
                        KxeInductionQuestion item = new KxeInductionQuestion(this, node);
                        Items.Add(item);
                    }
                }
                NumberQuestions();
                return true;
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "LoadFromXml", ex.Message, xpath);
                LastError = ex.Message;
                return false;
            }
        }

        public KxeInductionQuestion GeteInductionQuestion(int id)
        {
            KxeInductionQuestion result = null;

            foreach (KxeInductionQuestion i in Items)
            {
                if (i.QuestionId == id)
                {
                    result = i;
                    break;
                }
            }
            return result;
        }

        public List<KxeInductionQuestion> GeteInductionQuestionsByYear(int academicYear)
        {
            List<KxeInductionQuestion> result = new List<KxeInductionQuestion>();

            foreach (KxeInductionQuestion iq in Items)
            {
                if (iq.AcademicYearId == academicYear)
                {
                    result.Add(iq);
                }
            }
            return result;
        }

        public int[] GeteInductionQuestionIdsByYear(int academicYear)
        {
            List<int> result = new List<int>();

            List<KxeInductionQuestion> questions = GeteInductionQuestionsByYear(academicYear);

            foreach (KxeInductionQuestion iq in questions)
            {
                result.Add(iq.QuestionId);
            }
            return result.ToArray();
        }

        protected void NumberQuestions()
        {
            int lastAY = -1;
            int count = 1;

            foreach (KxeInductionQuestion iq in Items)
            {
                if (iq.AcademicYearId != lastAY)
                {
                    lastAY = iq.AcademicYearId;
                    count = 1;
                }
                else
                {
                    count += 1;
                }
                iq.QuestionIndex = count;
            }
        }
    }
}

