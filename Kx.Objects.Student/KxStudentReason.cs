using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;

namespace Kx.Objects.Student
{
    [Serializable]
    public class KxStudentReason : KxBaseClass
    {
        #region Attributes & Properties

        protected string   reasonType;
        protected string   description;
        protected bool     inactive;
        protected bool     defaultReason;

        public int      ReasonId
        {
            get
            { return base.dBIdentifier;}
        }

        public string   ReasonType
        {
            get
            { return reasonType;}
            set
            { 
                AddToAudit("ReasonType", reasonType, value);
                reasonType = value;
            }
        }

        public string   Description
        {
            get
            { return description;}
            set
            { 
                AddToAudit("Description", description, value);
                description = value;
            }
        }

        public bool     Inactive
        {
            get
            { return inactive;}
            set
            { 
                AddToAudit("Inactive", inactive, value);
                inactive = value;
            }
        }

        public bool     DefaultReason
        {
            get
            { return defaultReason;}
            set
            { 
                AddToAudit("DefaultReason", defaultReason, value);
                defaultReason = value;
            }
        }


        #endregion 

        public KxStudentReason(KxBaseClass parent,int      reasonid):base(parent)
        {
             base.dBIdentifier = reasonid;
             Load();
        }


        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("st2studentreasons", "ST2StudentReasons", TargetType.Table);

            result.AutoKeyField = new KxDataValue("ReasonId",ReasonId,KxValueType.Int);


            if ((actiontype == ActionType.Update) && (ReasonId == 0))
            {
                result.Action = ActionType.Insert;
            }
            else
            {
                result.Action = actiontype;
            }

            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {
                   result.AddParameter("reasonType", reasonType, KxValueType.String);
                   result.AddParameter("description", description, KxValueType.String);
                   result.AddParameter("inactive", inactive, KxValueType.Boolean);
                   result.AddParameter("defaultReason", defaultReason, KxValueType.Boolean);
            }
            return result;
        }


        public override  void LoadFromXml(string xml,XmlNode node)
        {
            dBIdentifier  = KxXmlUtils.GetNodeValue(node,"reasonid",0);
            reasonType  = KxXmlUtils.GetNodeValue(node,"reasontype","");
            description  = KxXmlUtils.GetNodeValue(node,"description","");
            inactive  = KxXmlUtils.GetNodeValue(node,"inactive",false);
            defaultReason  = KxXmlUtils.GetNodeValue(node,"defaultreason",false);
        }
    }
}

