using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;

namespace Kx.Objects.Student 
{
    public class KxStudentTermDates : KxBaseClass
    {
        private int allocationId = 0;

        private List<KxStudentTermDate> items = new List<KxStudentTermDate>();

        public int AllocationId
        {
            get
            { return allocationId; }
            set
            { allocationId = value; }
        }

        public List<KxStudentTermDate> Items
        {
            get
            { return items; }
        }

        public KxStudentTermDate this[int index]
        {
            get
            { return items[index]; }
            set
            { items[index] = value; }
        }

        public override KxData ConfigureData(ActionType actiontype)
        {
             KxData result = null;

             if (actiontype == ActionType.Select)
             {
                 result = new KxData("studenttermdate", "WebGetStudentTermDatesByAllocation", TargetType.StoredProc);

                 result.KeyFields.Add(new KxDataValue("allocationid", allocationId, KxValueType.Int));
             }
            return result;
        }

        public KxStudentTermDates(KxBaseClass parent, int allocationid) : base(parent)
        {
            allocationId = allocationid;
            Load();
        }

        public override bool LoadFromXml(string xml, string xpath)
        {
            items.Clear();
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xml);

                XmlNodeList nodes = doc.SelectNodes(@"//results/" + xpath + "/datarow");

                int count = 0;

                foreach (XmlNode node in nodes)
                {
                    if (node != null)
                    {
                        count++;
                        KxStudentTermDate td = new KxStudentTermDate(this,count, node);
                        Items.Add(td);
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "LoadFromXml", ex.Message, xpath);
                LastError = ex.Message;
                return false;
            }
        }

        public KxStudentTermDate GetTermDateByIndex(int id)
        {
            KxStudentTermDate result = null;

            foreach (KxStudentTermDate td in items)
            {
                if (td.Index == id)
                {
                    result = td;
                    break;
                }
            }

            return result;
        }

   
    }
}
