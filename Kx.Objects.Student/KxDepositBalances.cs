using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Configuration;
using Kx.Core.Data;
using Kx.Core.Exceptions;
using Kx.Core.Utility;

namespace Kx.Objects.Student
{

    public class KxDepositBalances : KxBaseClass
    {
        
        private List<KxDepositBalance> items = new List<KxDepositBalance>();

        private int kxStudentId;

        public int KxStudentId
        {
            get
            {
                return GetKxStudentId();
            }
        }

        public List<KxDepositBalance> Items
        {
            get
            { return items; }
        }

        public KxDepositBalance this[int index]
        {
            get
            { return items[index]; }
            set
            { items[index] = value; }
        }

        public Decimal DepositType1Balance
        {
            get
            { return GetDepositTypeBalance(1); }
        }

        public Decimal DepositType2Balance
        {
            get
            { return GetDepositTypeBalance(2); }
        }

        public Decimal DepositType3Balance
        {
            get
            { return GetDepositTypeBalance(3); }
        }

        public Decimal DepositType4Balance
        {
            get
            { return GetDepositTypeBalance(4); }
        }

        public Decimal DepositType5Balance
        {
            get
            { return GetDepositTypeBalance(5); }
        }
        public Decimal DepositType6Balance
        {
            get
            { return GetDepositTypeBalance(6); }
        }
        public Decimal DepositType7Balance
        {
            get
            { return GetDepositTypeBalance(7); }
        }

        public Decimal DepositType8Balance
        {
            get
            { return GetDepositTypeBalance(8); }
        }

        public Decimal DepositType9Balance
        {
            get
            { return GetDepositTypeBalance(9); }
        }

        public Decimal DepositType10Balance
        {
            get
            { return GetDepositTypeBalance(10); }
        }

        public Decimal DepositBalance
        {
            get
            {
                return 50;  //pha
                
                return GetDepositBalance();
            }
        }              

        public Decimal DepositBalanceShortfall200
        {
            get
            {
                return GetDepositBalanceShortFall(200);
            }
        }


        public Decimal DepositType1BalanceShortFall200
        {
            get
            {
                return DepositTypeBalanceShortFall(1, 200);
            }
        }

        public Decimal DepositType2BalanceShortFall200
        {
            get
            {
                return DepositTypeBalanceShortFall(2, 200);
            }
        }

        public Decimal DepositType3BalanceShortFall200
        {
            get
            {
                return DepositTypeBalanceShortFall(3, 200);
            }
        }

        public Decimal DepositType4BalanceShortFall200
        {
            get
            {
                return DepositTypeBalanceShortFall(4, 200);
            }
        }

        public Decimal DepositType5BalanceShortFall200
        {
            get
            {
                return DepositTypeBalanceShortFall(5, 200);
            }
        }

        public override KxData ConfigureData(ActionType actiontype)
        {
             KxData result = null;

             if (actiontype == ActionType.Select)
             {
                 string procedureName = KxDbConfiguration.GetConfiguration("StudentDepositBalances",
                     "StudentDepositBalances");

                 result = new KxData("depositbalance", procedureName, TargetType.StoredProc, ActionType.Select);

                 result.AddParameter("kxstudentId", KxStudentId, KxValueType.Int);
             }

            return result;
        }

        public KxDepositBalances(KxBaseClass parent, int kxstudentid)
            : base(parent)
        {
            kxStudentId = kxstudentid;
            Load();
        }

        public override bool LoadFromXml(string xml, string xpath)
        {
            items.Clear();
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xml);

                XmlNodeList nodes = doc.SelectNodes(@"//results/" + xpath + "/datarow");

                foreach (XmlNode node in nodes)
                {
                    if (node != null)
                    {
                        KxDepositBalance dep = new KxDepositBalance(this, node);
                        Items.Add(dep);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "LoadFromXml", ex.Message, xpath);
                LastError = ex.Message;
                return false;
            }
        }

        private int GetKxStudentId()
        {
            if (kxStudentId == 0 && parent != null)
            {
                if (parent is KxStudent)
                {
                    kxStudentId = ((KxStudent)parent).KxStudentId;
                }
            }
            return kxStudentId;
        }

        public KxDepositBalance GetBalanceById(int id)
        {
            KxDepositBalance result = null;

            foreach (KxDepositBalance dep in Items)
            {
                if (dep.DepositTypeId == id) 
                {
                    result = dep;
                    break;
                }
            }

            return result;
        }

        public Decimal GetDepositTypeBalance(int id)
        {
            Decimal result = 0;

            KxDepositBalance dummy = GetBalanceById(id);

            if (dummy != null)
            {
                result = dummy.DepositBalance;
            }
            return result;
        }

        public Decimal DepositTypeBalanceShortFall(int type, Decimal balance)
        {
            Decimal result = balance;

            Decimal CurrentTotal = GetDepositTypeBalance(type);

            result = balance - CurrentTotal;

            if (result < 0)
            {
                result = 0.00m;
            }

            return result;
        }

        public Decimal GetDepositBalance()
        {
            Decimal result = 0.00M;

            foreach (KxDepositBalance dep in Items)
            {
                result += dep.DepositBalance;
            }

            return result;
        }

        public Decimal GetDepositBalanceShortFall(Decimal balance)
        {
            Decimal result = balance;

            Decimal CurrentTotal = GetDepositBalance();

            result = balance - CurrentTotal;

            if (result < 0)
            {
                result = 0.00m;
            }

            return result;
        }

    }
}
