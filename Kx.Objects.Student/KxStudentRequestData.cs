using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;

namespace Kx.Objects.Student
{
    [Serializable]
    public class KxStudentRequestData : KxBaseClass
    {
        #region Attributes & Properties

        protected int _studentRequestId;
        protected string _dataLabel;
        protected string _dataValue;

        public int StudentRequestDataId
        {
            get { return base.dBIdentifier; }
        }

        public int StudentRequestId
        {
            get { return _studentRequestId; }
            set
            { 
                AddToAudit("StudentRequestId", _studentRequestId, value);
                _studentRequestId = value;
            }
        }

        public string DataLabel
        {
            get { return _dataLabel; }
            set
            { 
                AddToAudit("DataLabel", _dataLabel, value);
                _dataLabel = value;
            }
        }

        public string DataValue
        {
            get { return _dataValue; }
            set
            { 
                AddToAudit("DataValue", _dataValue, value);
                _dataValue = value;
            }
        }


        #endregion 


        public KxStudentRequestData(KxBaseClass parent):base(parent)
        { }


        public KxStudentRequestData(KxBaseClass parent, XmlNode node):base(parent)
        {
             LoadFromXml("", node);
        }

        public KxStudentRequestData(KxBaseClass parent,int  studentrequestdataid):base(parent)
        {
             base.dBIdentifier = studentrequestdataid;
             Load();
        }


        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("st2studentrequestdata", "ST2StudentRequestData", TargetType.Table);

            result.AutoKeyField = new KxDataValue("StudentRequestDataId",StudentRequestDataId,KxValueType.Int);


            if ((actiontype == ActionType.Update) && (StudentRequestDataId == 0))
            {
                result.Action = ActionType.Insert;
            }
            else
            {
                result.Action = actiontype;
            }

            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {
                if (_studentRequestId == 0 && Parent != null && Parent is KxStudentRequest)
                {
                    _studentRequestId = (Parent as KxStudentRequest).StudentRequestId;
                }

                result.AddParameter("studentRequestId", _studentRequestId, KxValueType.Int, false);
                result.AddParameter("dataLabel", _dataLabel, KxValueType.String, 255, false);
                result.AddParameter("dataValue", _dataValue, KxValueType.String, 2147483647, true);
            }
            return result;
        }


        public override  void LoadFromXml(string xml,XmlNode node)
        {
            dBIdentifier  = KxXmlUtils.GetNodeValue(node,"studentrequestdataid",0);
            _studentRequestId  = KxXmlUtils.GetNodeValue(node,"studentrequestid",0);
            _dataLabel  = KxXmlUtils.GetNodeValue(node,"datalabel",string.Empty);
            _dataValue  = KxXmlUtils.GetNodeValue(node,"datavalue",string.Empty);
        }

        public static List<KxStudentRequestData> GetST2StudentRequestData (int studentRequestId)
        {
            List<KxStudentRequestData> result = new List<KxStudentRequestData>();
            KxData data = new KxData("st2studentrequestdata", "ST2StudentRequestData", TargetType.Table);
            data.AddKeyField("studentrequestid", studentRequestId, KxValueType.Int);
            if (data.Execute ())
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(data.XmlResults);
                XmlNodeList nodes = doc.SelectNodes(@"//st2studentrequestdata/datarow");
                foreach(XmlNode node in nodes)
                {
                    result.Add(new KxStudentRequestData(null, node));
                }
            }

            return result;
        }
    }
}

