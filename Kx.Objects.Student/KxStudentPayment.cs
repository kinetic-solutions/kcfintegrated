using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;

namespace Kx.Objects.Student
{
    [Serializable]
    public class KxStudentPayment : KxBaseClass
    {
        #region Attributes & Properties

        protected int      applicationId;
        protected int      kxStudentId;
        protected int      paymentReasonId;
        protected string   statementText;
        protected int      paymentMethodId;
        protected int      siteId;
        protected int      areaId;
        protected int      residentialRoomId;
        protected DateTime ?creationDate;
        protected int      creationUserId;
        protected string   referenceNumber;
        protected DateTime ?referenceDate;
        protected int      receiptNumber;
        protected int      exportBatchId;
        protected int      releaseExportBatchId;
        protected decimal     amount;
        protected int      chequeRequestBatchId;
        protected DateTime ?reconciledDate;
        protected int      reconciledUserId;
        protected int      suppressFromInterface;

        protected KxStudentPaymentReason paymentReason = null;


        public int      StudentPaymentId
        {
            get
            { return base.dBIdentifier;}
        }

        public int      ApplicationId
        {
            get
            { return applicationId;}
            set
            { 
                AddToAudit("ApplicationId", applicationId, value);
                applicationId = value;
            }
        }

        public int      KxStudentId
        {
            get
            { return kxStudentId;}
            set
            { 
                AddToAudit("KxStudentId", kxStudentId, value);
                kxStudentId = value;
            }
        }

        public int      PaymentReasonId
        {
            get
            { return paymentReasonId;}
            set
            { 
                AddToAudit("PaymentReasonId", paymentReasonId, value);
                paymentReasonId = value;
            }
        }

        public string   StatementText
        {
            get
            { return statementText;}
            set
            { 
                AddToAudit("StatementText", statementText, value);
                statementText = value;
            }
        }

        public int      PaymentMethodId
        {
            get
            { return paymentMethodId;}
            set
            { 
                AddToAudit("PaymentMethodId", paymentMethodId, value);
                paymentMethodId = value;
            }
        }

        public int      SiteId
        {
            get
            { return siteId;}
            set
            { 
                AddToAudit("SiteId", siteId, value);
                siteId = value;
            }
        }

        public int      AreaId
        {
            get
            { return areaId;}
            set
            { 
                AddToAudit("AreaId", areaId, value);
                areaId = value;
            }
        }

        public int      ResidentialRoomId
        {
            get
            { return residentialRoomId;}
            set
            { 
                AddToAudit("ResidentialRoomId", residentialRoomId, value);
                residentialRoomId = value;
            }
        }

        public DateTime ?CreationDate
        {
            get
            { return creationDate;}
            set
            { 
                AddToAudit("CreationDate", creationDate, value);
                creationDate = value;
            }
        }

        public int      CreationUserId
        {
            get
            { return creationUserId;}
            set
            { 
                AddToAudit("CreationUserId", creationUserId, value);
                creationUserId = value;
            }
        }

        public KxStudentPaymentReason PaymentReason
        {
            get
            {
                if (paymentReasonId > 0 && paymentReason == null)
                {
                    paymentReason = new KxStudentPaymentReason(this, paymentReasonId);
                }
                return paymentReason;
            }
        }

        public string   ReferenceNumber
        {
            get
            { return referenceNumber;}
            set
            { 
                AddToAudit("ReferenceNumber", referenceNumber, value);
                referenceNumber = value;
            }
        }

        public DateTime ?ReferenceDate
        {
            get
            { return referenceDate;}
            set
            { 
                AddToAudit("ReferenceDate", referenceDate, value);
                referenceDate = value;
            }
        }

        public int      ReceiptNumber
        {
            get
            { return receiptNumber;}
            set
            { 
                AddToAudit("ReceiptNumber", receiptNumber, value);
                receiptNumber = value;
            }
        }

        public int      ExportBatchId
        {
            get
            { return exportBatchId;}
            set
            { 
                AddToAudit("ExportBatchId", exportBatchId, value);
                exportBatchId = value;
            }
        }

        public int      ReleaseExportBatchId
        {
            get
            { return releaseExportBatchId;}
            set
            { 
                AddToAudit("ReleaseExportBatchId", releaseExportBatchId, value);
                releaseExportBatchId = value;
            }
        }

        public decimal     Amount
        {
            get
            { return amount;}
            set
            { 
                AddToAudit("Amount", amount, value);
                amount = value;
            }
        }

        public int      ChequeRequestBatchId
        {
            get
            { return chequeRequestBatchId;}
            set
            { 
                AddToAudit("ChequeRequestBatchId", chequeRequestBatchId, value);
                chequeRequestBatchId = value;
            }
        }

        public DateTime ?ReconciledDate
        {
            get
            { return reconciledDate;}
            set
            { 
                AddToAudit("ReconciledDate", reconciledDate, value);
                reconciledDate = value;
            }
        }

        public int      ReconciledUserId
        {
            get
            { return reconciledUserId;}
            set
            { 
                AddToAudit("ReconciledUserId", reconciledUserId, value);
                reconciledUserId = value;
            }
        }

        public int      SuppressFromInterface
        {
            get
            { return suppressFromInterface;}
            set
            { 
                AddToAudit("SuppressFromInterface", suppressFromInterface, value);
                suppressFromInterface = value;
            }
        }

        public bool IsDeposit
        {
            get
            {
                try
                {
                    return PaymentReason.HasDeposit;
                }
                catch
                {
                    return false;
                }
            }
        }

        #endregion 

        public KxStudentPayment(KxBaseClass parent)
            : base(parent)
        {
        }

        public KxStudentPayment(KxBaseClass parent, int studentpaymentid)
            : this(parent)
        {
             base.dBIdentifier = studentpaymentid;
             Load();
        }

        public KxStudentPayment(KxBaseClass parent,XmlNode node)
            : this(parent)
        {
            LoadFromXml("",node);
        }


        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("st2studentpayment", "ST2StudentPayments", TargetType.Table);

            result.AutoKeyField = new KxDataValue("StudentPaymentId",StudentPaymentId,KxValueType.Int);


            if ((actiontype == ActionType.Update) && (StudentPaymentId == 0))
            {
                result.Action = ActionType.Insert;
            }
            else
            {
                result.Action = actiontype;
            }

            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {
                   result.AddParameter("applicationId", applicationId, KxValueType.Int);
                   result.AddParameter("kxStudentId", kxStudentId, KxValueType.Int);
                   result.AddParameter("paymentReasonId", paymentReasonId, KxValueType.Int);
                   result.AddParameter("statementText", statementText, KxValueType.String);
                   result.AddParameter("paymentMethodId", paymentMethodId, KxValueType.Int);
                   result.AddParameter("siteId", siteId, KxValueType.Int);
                   result.AddParameter("areaId", areaId, KxValueType.Int);
                   result.AddParameter("residentialRoomId", residentialRoomId, KxValueType.Int);
                   result.AddParameter("creationDate", creationDate, KxValueType.DateTime);
                   result.AddParameter("creationUserId", creationUserId, KxValueType.Int);
                   result.AddParameter("referenceNumber", referenceNumber, KxValueType.String);
                   result.AddParameter("referenceDate", referenceDate, KxValueType.DateTime);
                   result.AddParameter("receiptNumber", receiptNumber, KxValueType.Int);
                   result.AddParameter("exportBatchId", exportBatchId, KxValueType.Int);
                   result.AddParameter("releaseExportBatchId", releaseExportBatchId, KxValueType.Int);
                   result.AddParameter("amount", amount, KxValueType.Decimal);
                   result.AddParameter("chequeRequestBatchId", chequeRequestBatchId, KxValueType.Int);
                   result.AddParameter("reconciledDate", reconciledDate, KxValueType.DateTime);
                   result.AddParameter("reconciledUserId", reconciledUserId, KxValueType.Int);
                   result.AddParameter("suppressFromInterface", suppressFromInterface, KxValueType.Int);
            }
            return result;
        }

        public override void LoadFromXml(string xml, XmlNode node)
        {
            dBIdentifier = KxXmlUtils.GetNodeValue(node, "studentpaymentid", 0);
            applicationId = KxXmlUtils.GetNodeValue(node, "applicationid", 0);
            kxStudentId = KxXmlUtils.GetNodeValue(node, "kxstudentid", 0);
            paymentReasonId = KxXmlUtils.GetNodeValue(node, "paymentreasonid", 0);
            statementText = KxXmlUtils.GetNodeValue(node, "statementtext", "");
            paymentMethodId = KxXmlUtils.GetNodeValue(node, "paymentmethodid", 0);
            siteId = KxXmlUtils.GetNodeValue(node, "siteid", 0);
            areaId = KxXmlUtils.GetNodeValue(node, "areaid", 0);
            residentialRoomId = KxXmlUtils.GetNodeValue(node, "residentialroomid", 0);
            creationDate = KxXmlUtils.GetNodeValueDT(node, "creationdate", null);
            creationUserId = KxXmlUtils.GetNodeValue(node, "creationuserid", 0);
            referenceNumber = KxXmlUtils.GetNodeValue(node, "referencenumber", "");
            referenceDate = KxXmlUtils.GetNodeValueDT(node, "referencedate", null);
            receiptNumber = KxXmlUtils.GetNodeValue(node, "receiptnumber", 0);
            exportBatchId = KxXmlUtils.GetNodeValue(node, "exportbatchid", 0);
            releaseExportBatchId = KxXmlUtils.GetNodeValue(node, "releaseexportbatchid", 0);
            amount = KxXmlUtils.GetNodeValue(node, "amount", (decimal)0);
            chequeRequestBatchId = KxXmlUtils.GetNodeValue(node, "chequerequestbatchid", 0);
            reconciledDate = KxXmlUtils.GetNodeValueDT(node, "reconcileddate", null);
            reconciledUserId = KxXmlUtils.GetNodeValue(node, "reconcileduserid", 0);
            suppressFromInterface = KxXmlUtils.GetNodeValue(node, "suppressfrominterface", 0);
        }
    }
}

