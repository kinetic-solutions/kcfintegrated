using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Exceptions;
using Kx.Core.Utility;

namespace Kx.Objects.Student
{
    public class KxStudentApplications : KxBaseClass
    {
        private int kxStudentId;
        private List<KxStudentApplication> items = new List<KxStudentApplication>();

        public int KxStudentId
        {
            get
            { return kxStudentId; }
            set
            { kxStudentId = value; }
        }

        public List<KxStudentApplication> Items
        {
            get
            { return items; }
        }

        public KxStudentApplication this[int index]
        {
            get
            { return items[index]; }
            set
            { items[index] = value; }
        }

        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = null;

            if (actiontype == ActionType.Select)
            {
                result = new KxData("studentapplications", "ST2StudentApplications", TargetType.Table);

                result.KeyFields.Add(new KxDataValue("kxStudentId", kxStudentId, KxValueType.Int));

                result.AddOrderField("applicationId", KxDataOrderType.Ascending);
            }
            
            return result;
        }

        public KxStudentApplications(KxBaseClass parent,int kxStudentId) : base(parent)
        {
            this.kxStudentId = kxStudentId;
            Load();
        }

        public override bool LoadFromXml(string xml, string xpath)
        {
            items.Clear();
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xml);

                XmlNodeList nodes = doc.SelectNodes(@"//results/" + xpath + "/datarow");

                foreach (XmlNode node in nodes)
                {
                    if (node != null)
                    {
                        KxStudentApplication app = new KxStudentApplication(this, node);
                        Items.Add(app);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "LoadFromXml", ex.Message, xpath);
                LastError = ex.Message;
                return false;
            }
        }
    }
}
