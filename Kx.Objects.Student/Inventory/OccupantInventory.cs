﻿#region Copyright
// =======================================================
// 	Product:	KCF.Student
// 	Project: 	Kx.Objects.Student
// 	File:		OccupantInventory.cs
// 	Created:	06/08/2013
// 	By:			Steve Ashley
// 	Modified:	06/08/2013
// 
// 	Copyright © Kinetic Solutions Ltd 2013
// 	All rights reserved.
// 
// 	No portion of this file may be re-created, copied or otherwise
// 	used without the express permission of Kinetic Solutions Ltd.
// =======================================================
#endregion

using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Kx.Objects.Student.Inventory
{
    public class OccupantInventory
    {
        public int AllocationId { get; set; }
        public System.Collections.ObjectModel.Collection<OccupantInventoryRoom> Rooms { get; set; }
    }
}