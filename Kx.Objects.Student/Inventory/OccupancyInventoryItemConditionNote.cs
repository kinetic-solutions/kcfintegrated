﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Kx.Objects.Student.Inventory
{
    public class OccupancyInventoryItemConditionNote
    {
        public int Id { get; set; }
        public int OccupantInventoryItemId { get; set; }
        public string Note { get; set; }
        public DateTime CreationDate { get; set; }
    }
}
