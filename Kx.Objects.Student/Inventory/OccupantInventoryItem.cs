﻿#region Copyright
// =======================================================
// 	Product:	KCF.Student
// 	Project: 	Kx.Objects.Student
// 	File:		OccupantInventoryItem.cs
// 	Created:	06/08/2013
// 	By:			Steve Ashley
// 	Modified:	06/08/2013
// 
// 	Copyright © Kinetic Solutions Ltd 2013
// 	All rights reserved.
// 
// 	No portion of this file may be re-created, copied or otherwise
// 	used without the express permission of Kinetic Solutions Ltd.
// =======================================================
#endregion

using System.Collections.Generic;
using System.Linq;

namespace Kx.Objects.Student.Inventory
{
    public class OccupantInventoryItem
    {
        public int OccupantInventoryItemId { get; set; }
        public string Description { get; set; }
        public int Quantity { get; set; }
        public string Condition { get; set; }
        public System.Collections.ObjectModel.Collection<OccupantInventoryComment> Comments { get; set; }
        public System.Collections.ObjectModel.Collection<OccupancyInventoryItemConditionNote> ConditionNotes { get; set; }

        public string NewComment { get; set; }

        public int CommentCount
        {
            get { return Comments.Count; }
        }

        public string [] CommentIDs
        {
            get
            {
                List<string> result = new List<string>();
                for (int i = 0; i < Comments.Count; i++)
                {
                    result.Add(i.ToString());
                }
                return result.ToArray();
            }
        }

        public string[] ConditionNoteIDs
        {
            get
            {
                var result = from r in ConditionNotes.ToList()
                    select r.Id.ToString();
                return result.ToArray();
            }
        }

        public OccupantInventoryItem()
        {
        }
    }
}