﻿#region usings

#region Copyright

// =======================================================
// 	Product:	KCF.Student
// 	Project: 	Kx.Objects.Student
// 	File:		OccupantInventoryComment.cs
// 	Created:	06/08/2013
// 	By:			Steve Ashley
// 	Modified:	08/08/2013
// 
// 	Copyright © Kinetic Solutions Ltd 2013
// 	All rights reserved.
// 
// 	No portion of this file may be re-created, copied or otherwise
// 	used without the express permission of Kinetic Solutions Ltd.
// =======================================================

#endregion

using System;

#endregion

namespace Kx.Objects.Student.Inventory
{
    public class OccupantInventoryComment
    {
        public string Text { get; set; }
        public string CommentedBy { get; set; }
        public bool StudentComment { get; set; }
        public DateTime CommentDateTime { get; set; }
        public DateTime CommentReadDateTime { get; set; }

        public bool Read
        {
            get
            {
                return StudentComment || CommentReadDateTime > DateTime.MinValue;
            }
        }
    }
}