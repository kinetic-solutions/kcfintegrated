﻿#region Copyright
// =======================================================
// 	Product:	KCF.Student
// 	Project: 	Kx.Objects.Student
// 	File:		OccupantInventoryRoom.cs
// 	Created:	06/08/2013
// 	By:			Steve Ashley
// 	Modified:	06/08/2013
// 
// 	Copyright © Kinetic Solutions Ltd 2013
// 	All rights reserved.
// 
// 	No portion of this file may be re-created, copied or otherwise
// 	used without the express permission of Kinetic Solutions Ltd.
// =======================================================
#endregion
using System;
using System.Collections.Generic;

namespace Kx.Objects.Student.Inventory
{
    public class OccupantInventoryRoom
    {
        public int OccupantInventoryRoomId { get; set; }
        public string RoomName { get; set; }
        public bool ResidentialRoom { get; set; }
        public bool Accepted { get; set; }
        public bool Disputed { get; set; }

        public bool Pending
        {
            get
            {
                return !Accepted && !Disputed;
            }
        }

        public DateTime DueDate { get; set; }
        public DateTime AcceptedDate { get; set; }
        public string AcceptedBy { get; set; }
        public bool AcceptedByStudent { get; set; }
        public DateTime DisputedDate { get; set; }
        public string DisputedBy { get; set; }
        public System.Collections.ObjectModel.Collection<OccupantInventoryItem> Items { get; set; }
        public System.Collections.ObjectModel.Collection<OccupantInventoryComment> RoomComments { get; set; }

        public bool IsDisputed
        {
            get
            {
                return !Accepted && Disputed;
            }
        }

        public string RoomInventoryState
        {
            get
            {
                if (Accepted)
                {
                    return "accepted";
                }
                else if (Disputed)
                {
                    return "disputed";
                }

                return "pending";
            }
        }

        public string NewComment { get; set; }

        public string[] ItemIDs
        {
            get
            {
                List<string> result = new List<string>();
                foreach (OccupantInventoryItem item in Items)
                {
                    result.Add(item.OccupantInventoryItemId.ToString());
                }

                return result.ToArray();
            }
        }

        public string[] CommentIDs
        {
            get
            {
                List<string> result = new List<string>();
                for (int i = 0; i < RoomComments.Count; i++)
                {
                    result.Add(i.ToString());
                }

                return result.ToArray();
            }
        }

        public OccupantInventoryRoom()
        {
        }
    }
}