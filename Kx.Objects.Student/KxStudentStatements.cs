using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Exceptions;
using Kx.Core.Utility;

namespace Kx.Objects.Student
{
    public class KxStudentStatements : KxBaseClass
    {

        private int kxStudentId;
        private int academicYearId;

        private List<KxStudentStatement> items = new List<KxStudentStatement>();

        protected decimal totalPayments = 0.00M;
        protected decimal totalCharges = 0.00M;

        public List<KxStudentStatement> Items
        {
            get
            { return items; }
        }

        public KxStudentStatement this[int index]
        {
            get
            { return items[index]; }
            set
            { items[index] = value; }
        }

        public decimal TotalPayments
        {
            get
            {
                if (totalPayments == 0.00M && totalCharges == 0.00M)
                {
                    DetermineBalances();
                }
                return totalPayments;
            }
            set
            {
                totalPayments = value;
            }
        }

        public decimal TotalCharges
        {
            get
            {
                if (totalCharges == 0.00M && totalPayments == 0.00M)
                {
                    DetermineBalances();
                }
                return totalCharges;
            }
            set
            {
                totalCharges = value;
            }
        }

        public KxStudentStatements(KxBaseClass parent)
            : base(parent)
        {
        }

        public KxStudentStatements(KxBaseClass parent, int kxstudentid, int academicyearid)  : this(parent)
        {
             this.kxStudentId = kxstudentid;
             this.academicYearId = academicyearid;
             Load();
        }

        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = null;

            if (actiontype == ActionType.Select)
            {
                result = new KxData("viewstudentstatement", "viewStudentStatement", TargetType.Table, ActionType.Select);

                result.AddKeyField("kxstudentid", kxStudentId, KxValueType.Int);
                result.AddKeyField("academicyearid", academicYearId, KxValueType.Int);

                result.AddOrderField("DateFrom", KxDataOrderType.Ascending);
                result.AddOrderField("DateTo", KxDataOrderType.Ascending);
                
            }
            return result;
        }

        public override bool LoadFromXml(string xml, string xpath)
        {
            items.Clear();
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xml);

                XmlNodeList nodes = doc.SelectNodes(@"//results/" + xpath + "/datarow");

                foreach (XmlNode node in nodes)
                {
                    if (node != null)
                    {
                        KxStudentStatement item = new KxStudentStatement(this, node);
                        items.Add(item);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "LoadFromXml", ex.Message, xpath);
                LastError = ex.Message;
                return false;
            }
        }

        public KxStudentStatement GetStudentStatement(string id)
        {
            KxStudentStatement result = null;

            foreach (KxStudentStatement i in Items)
            {
                if (i.UniqueId == id)
                {
                    result = i;
                    break;
                }
            }
            return result;
        }


        private void DetermineBalances()
        {
            totalCharges = 0.00M;
            totalPayments = 0.00M;

            foreach (KxStudentStatement ss in Items)
            {
                if (ss.IsCharge)
                {
                    totalCharges += Math.Abs(ss.TotalAmount);
                }
                else
                {
                    totalPayments += Math.Abs(ss.TotalAmount);
                }
            }
        }
    }
}

