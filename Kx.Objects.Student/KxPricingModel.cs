using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;

namespace Kx.Objects.Student
{
    [Serializable]
    public class KxPricingModel : KxBaseClass
    {
        #region Attributes & Properties

        protected string   description;
        protected DateTime ?creationDate;
        protected int      creationUserId;
        protected DateTime ?inactiveDate;
        protected int      studentYearId;
        protected int      courseTypeId;
        protected string   modelType;

        protected KxPricingModelCharges charges;

        public int      PricingModelId
        {
            get
            { return base.dBIdentifier;}
        }

        public string   Description
        {
            get
            { return description;}
            set
            { 
                AddToAudit("Description", description, value);
                description = value;
            }
        }

        public DateTime ?CreationDate
        {
            get
            { return creationDate;}
            set
            { 
                AddToAudit("CreationDate", creationDate, value);
                creationDate = value;
            }
        }

        public int      CreationUserId
        {
            get
            { return creationUserId;}
            set
            { 
                AddToAudit("CreationUserId", creationUserId, value);
                creationUserId = value;
            }
        }

        public DateTime ?InactiveDate
        {
            get
            { return inactiveDate;}
            set
            { 
                AddToAudit("InactiveDate", inactiveDate, value);
                inactiveDate = value;
            }
        }

        public int      StudentYearId
        {
            get
            { return studentYearId;}
            set
            { 
                AddToAudit("StudentYearId", studentYearId, value);
                studentYearId = value;
            }
        }

        public int      CourseTypeId
        {
            get
            { return courseTypeId;}
            set
            { 
                AddToAudit("CourseTypeId", courseTypeId, value);
                courseTypeId = value;
            }
        }

        public string   ModelType
        {
            get
            { return modelType;}
            set
            { 
                AddToAudit("ModelType", modelType, value);
                modelType = value;
            }
        }

        public KxPricingModelCharges Charges
        {
            get
            {
                if (charges == null)
                {
                    charges = new KxPricingModelCharges(this, PricingModelId);
                }
                return charges;
            }
        }
        
        #endregion 

        public KxPricingModel(KxBaseClass parent,int      pricingmodelid): base(parent)
        {
             base.dBIdentifier = pricingmodelid;
             Load();
        }

        public KxPricingModel(KxBaseClass parent, XmlNode node)
            : base(parent)
        {
            LoadFromXml("",node);
        }


        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("st2pricingmodels", "ST2PricingModels", TargetType.Table);

            result.AutoKeyField = new KxDataValue("PricingModelId",PricingModelId,KxValueType.Int);


            if ((actiontype == ActionType.Update) && (PricingModelId == 0))
            {
                result.Action = ActionType.Insert;
            }
            else
            {
                result.Action = actiontype;
            }

            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {
                   result.AddParameter("description", description, KxValueType.String);
                   result.AddParameter("creationDate", creationDate, KxValueType.DateTime);
                   result.AddParameter("creationUserId", creationUserId, KxValueType.Int);
                   result.AddParameter("inactiveDate", inactiveDate, KxValueType.DateTime);
                   result.AddParameter("studentYearId", studentYearId, KxValueType.Int);
                   result.AddParameter("courseTypeId", courseTypeId, KxValueType.Int);
                   result.AddParameter("modelType", modelType, KxValueType.String);
            }
            return result;
        }

        public override void LoadFromXml(string xml, XmlNode node)
        {
            dBIdentifier = KxXmlUtils.GetNodeValue(node, "pricingmodelid", 0);
            description  = KxXmlUtils.GetNodeValue(node,"description","");
            creationDate  = KxXmlUtils.GetNodeValueDT(node,"creationdate",null);
            creationUserId  = KxXmlUtils.GetNodeValue(node,"creationuserid",0);
            inactiveDate  = KxXmlUtils.GetNodeValueDT(node,"inactivedate",null);
            studentYearId  = KxXmlUtils.GetNodeValue(node,"studentyearid",0);
            courseTypeId  = KxXmlUtils.GetNodeValue(node,"coursetypeid",0);
            modelType  = KxXmlUtils.GetNodeValue(node,"modeltype","");
        }

        public KxPricingModelCharge GetChargeByType(int typeId,int AcademicYearId)
        {
            KxPricingModelCharge result = null;

            foreach (KxPricingModelCharge charge in Charges.Items)
            {
                if (charge.StudentChargeTypeId == typeId && charge.AcademicYearId == AcademicYearId)
                {
                    result = charge;
                    break;
                }
            }
            return result;
        }
    }
}

