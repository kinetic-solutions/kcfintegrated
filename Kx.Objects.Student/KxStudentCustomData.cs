using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;

namespace Kx.Objects.Student
{
    [Serializable]
    public class KxStudentCustomData : KxBaseClass
    {
        #region Attributes & Properties

        protected int      kxStudentId;
        protected string   dataField;
        protected string   dataValue;
        protected int      applicationId;
        protected bool     hiddenData;
        protected int      encryptionLevel;

        public int      CustomDataId
        {
            get
            { return base.dBIdentifier;}
        }

        public int      KxStudentId
        {
            get
            { return kxStudentId;}
            set
            { 
                AddToAudit("KxStudentId", kxStudentId, value);
                kxStudentId = value;
            }
        }

        public string   DataField
        {
            get
            { return dataField;}
            set
            { 
                AddToAudit("DataField", dataField, value);
                dataField = value;
            }
        }

        public string   DataValue
        {
            get
            { return dataValue;}
            set
            { 
                AddToAudit("DataValue", dataValue, value);
                dataValue = value;
            }
        }

        public int      ApplicationId
        {
            get
            { return applicationId;}
            set
            { 
                AddToAudit("ApplicationId", applicationId, value);
                applicationId = value;
            }
        }

        public bool     HiddenData
        {
            get
            { return hiddenData;}
            set
            { 
                AddToAudit("HiddenData", hiddenData, value);
                hiddenData = value;
            }
        }

        public int      EncryptionLevel
        {
            get
            { return encryptionLevel;}
            set
            { 
                AddToAudit("EncryptionLevel", encryptionLevel, value);
                encryptionLevel = value;
            }
        }


        #endregion 


        public KxStudentCustomData(KxBaseClass parent):base(parent)
        {}


        public KxStudentCustomData(KxBaseClass parent, XmlNode node):base(parent)
        {
             LoadFromXml("", node);
        }

        public KxStudentCustomData(KxBaseClass parent,int      customdataid):base(parent)
        {
             base.dBIdentifier = customdataid;
             Load();
        }


        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("st2studentcustomdata", "ST2StudentCustomData", TargetType.Table);

            result.AutoKeyField = new KxDataValue("CustomDataId",CustomDataId,KxValueType.Int);


            if ((actiontype == ActionType.Update) && (CustomDataId == 0))
            {
                result.Action = ActionType.Insert;
            }
            else
            {
                result.Action = actiontype;
            }

            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {
                   result.AddParameter("dataField", dataField, KxValueType.String, 255, true);
                   result.AddParameter("dataValue", dataValue, KxValueType.String, 1000, true);
                   result.AddParameter("applicationId", applicationId, KxValueType.Int, true);
                   result.AddParameter("hiddenData", hiddenData, KxValueType.Boolean, true);
                   result.AddParameter("encryptionLevel", encryptionLevel, KxValueType.Int, true);
            }
            return result;
        }


        public override  void LoadFromXml(string xml,XmlNode node)
        {
            dBIdentifier  = KxXmlUtils.GetNodeValue(node,"customdataid",0);
            kxStudentId  = KxXmlUtils.GetNodeValue(node,"kxstudentid",0);
            dataField  = KxXmlUtils.GetNodeValue(node,"datafield","");
            dataValue  = KxXmlUtils.GetNodeValue(node,"datavalue","");
            applicationId  = KxXmlUtils.GetNodeValue(node,"applicationid",0);
            hiddenData  = KxXmlUtils.GetNodeValue(node,"hiddendata",false);
            encryptionLevel  = KxXmlUtils.GetNodeValue(node,"encryptionlevel",0);
        }
    }
}

