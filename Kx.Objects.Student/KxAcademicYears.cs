using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Linq;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Exceptions;
using Kx.Core.Utility;

namespace Kx.Objects.Student
{
    public class KxAcademicYears : KxBaseClass
    {
        private List<KxAcademicYear> items = new List<KxAcademicYear>();
        private int ?_currentAcademicYearId = null;

        #region properties

        /// <summary>
        /// 
        /// </summary>
        public List<KxAcademicYear> Items
        {
            get { return items; }
        }

        /// <summary>
        /// 
        /// </summary>
        public KxAcademicYear this[int index]
        {
            get
            { return Items[index]; }
            set
            { Items[index] = value; }
        }
        /// <summary>
        /// Read-only: Returns the current Academic year ID
        /// </summary>
        public int CurrentAcademicYearId
        {
            get
            {
                int result = 0;

                if (!_currentAcademicYearId.HasValue)
                {
                    KxData data = new KxData("qryst2getcurrentacademicyear", "qryST2GetCurrentAcademicYear", TargetType.StoredProc, ActionType.ExecuteProcedure);

                    if (data.Execute())
                    {
                        XmlDocument doc = new XmlDocument();
                        doc.LoadXml(data.XmlResults);

                        XmlNode node = doc.SelectSingleNode(@"//qryst2getcurrentacademicyear/datarow");

                        if (node != null)
                        {
                            _currentAcademicYearId = KxXmlUtils.GetNodeValue(node, "academicyearid", 0);
                        }
                    }
                }

                if (_currentAcademicYearId.HasValue)
                    result = _currentAcademicYearId.Value;  
                
                return result;
            }
        }
        /// <summary>
        /// Returns the Current Academic Year as calculated by the stored proc qryST2GetCurrentAcademicYear
        /// </summary>
        public KxAcademicYear CurrentAcademicYear
        {
            get { return new KxAcademicYear(null, CurrentAcademicYearId); }
        }

        #endregion

        /// <summary>
        /// 
        /// </summary>
        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = null;

            if (actiontype == ActionType.Select)
            {
                result = new KxData("years", "st2academicyears", TargetType.Table);

                result.AddOrderField("academicyearid",KxDataOrderType.Ascending);
            }
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        public KxAcademicYears(KxBaseClass parent) : base(parent)
        {
            Load();
        }

        /// <summary>
        /// 
        /// </summary>
        public override bool LoadFromXml(string xml, string xpath)
        {
            items.Clear();
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xml);

                XmlNodeList nodes = doc.SelectNodes(@"//results/" + xpath + "/datarow");

                foreach (XmlNode node in nodes)
                {
                    if (node != null)
                    {
                        KxAcademicYear year = new KxAcademicYear(this, node);
                        Items.Add(year);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "LoadFromXml", ex.Message, xpath);
                LastError = ex.Message;
                return false;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public KxAcademicYear GetAcademicYear(int Year)
        {
            return (from y in Items
                where y.AcademicYearId == Year
                select y).FirstOrDefault();
        }

        /// <summary>
        /// 
        /// </summary>
        public string[] GetYearIds()
        {
            string[] result = new string[GetPublishedCount()];
            int count = 0;

            foreach (KxAcademicYear year in Items)
            {
                if (year.WebPublished)
                {
                    result[count] = year.AcademicYearId.ToString();
                    count++;
                }
            }
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        public string[] GetYearIdsDesc()
        {
            string[] result = new string[GetPublishedCount()];
            int count = GetPublishedCount() - 1;

            foreach (KxAcademicYear year in Items)
            {
                if (year.WebPublished)
                {
                    result[count] = year.AcademicYearId.ToString();
                    count--;
                }
            }
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        private int GetPublishedCount()
        {
            int result = 0;

            foreach (KxAcademicYear year in Items)
            {
                if (year.WebPublished)
                {
                    result++;
                }
            }
            return result;
        }
            
            
    }
}
