﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Core.Merge;
using Kx.Objects.Student.Web;
using Kx.Core.Exceptions;
using System.Threading;

namespace Kx.Objects.Student.MergeTagProvider
{
    public class ThreadedMergeTagSnapshot
    {
        private KxMergeTagSetName SetName { get; set; }
        private int KxStudentId { get; set; }
        private int AllocationId { get; set; }

        public ThreadedMergeTagSnapshot(KxMergeTagSetName setName, int kxStudentId, int allocationId)
        {
            this.SetName = setName;
            this.KxStudentId = kxStudentId;
            this.AllocationId = allocationId;
        }

        public void SnapshotMergeTagsOnThread()
        {
            var threadLoad = new Thread(new ThreadStart(PerformSnapshot));
            threadLoad.Start();
        }

        private int GetParamValue(KxMergeTagSetName setName, int kxStudentId, int allocationId)
        {
            switch (setName)
            {
                case KxMergeTagSetName.GetST2Students:
                    return kxStudentId;
                case KxMergeTagSetName.GetST2StudentAllocations:
                default:
                    return allocationId;

            }
        }

        private string GetParamName(KxMergeTagSetName setName)
        {
            switch (setName)
            {
                case KxMergeTagSetName.GetST2Students:
                    return "KXSTUDENTID";
                case KxMergeTagSetName.GetST2StudentAllocations:
                default:
                    return "ALLOCATIONID";

            }
        }

        private void PerformSnapshot()
        {
            var tags = new KxMergeTagProvider(SetName.ToString(), GetParamName(SetName),GetParamValue(SetName, KxStudentId, AllocationId));

            foreach (string tag in tags.MergeTagNames)
            {
                tags.CurrentMergeTag = tag;

                KxMergeTagSnapshot snapshot = new KxMergeTagSnapshot(null);
                snapshot.MergeTagSet = SetName.ToString();
                snapshot.KxStudentId = KxStudentId;
                snapshot.AllocationId = AllocationId;
                snapshot.CreationDate = DateTime.Now;
                snapshot.MergeTag = tag;
                snapshot.MergeData = tags.MergeTag;
                if (!snapshot.Save())
                {
                    KxLogger.LogActivity(this, "SnapshotMergeTags", string.Format("Error saving merge tag {0} from {1} for KxStudentID:{2}, AllocationID:{3}", tag, SetName, KxStudentId, AllocationId), snapshot.LastError);
                }
            }

        }
    }
}
