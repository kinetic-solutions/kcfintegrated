﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Core.Merge;
using Kx.Objects.Student.Web;

namespace Kx.Objects.Student.MergeTagProvider
{
    public class KxSnapshotMergeTagProvider : KxMergeTagProvider
    {
        public KxMergeTagSetName SetName { get; private set; }
        public int AllocationId { get; private set; }
        public int KxStudentId { get; private set; }

        public KxSnapshotMergeTagProvider(KxMergeTagSetName setName, int allocationId, int kxStudentId)
            : base()

        {
            SetName = setName;
            AllocationId = allocationId;
            KxStudentId = kxStudentId;

            SetBaseValues();
            LoadFromSnapshot();
        }

        private void SetBaseValues()
        {
            _name = SetName.ToString();
            if (SetName == KxMergeTagSetName.GetST2Students)
            {
                ParameterName = "KXSTUDENTID";
                ParameterValue = KxStudentId;
            }
            else if (SetName == KxMergeTagSetName.GetST2StudentAllocations)
            {
                ParameterName = "ALLOCATIONID";
                ParameterValue = AllocationId;
            }
        }

        private void LoadFromSnapshot()
        {
            this._tags.Clear();
            foreach (var tag in KxMergeTagSnapshot.GetMergeTagSnapshot(this.SetName, this.AllocationId, this.KxStudentId))
            {
                this._tags.Add(tag.MergeTag, tag.MergeData);
            }
        }
    }
}
