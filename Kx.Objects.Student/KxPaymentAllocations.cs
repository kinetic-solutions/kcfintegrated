using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Exceptions;
using Kx.Core.Utility;

namespace Kx.Objects.Student
{
    public class KxPaymentAllocations : KxBaseClass
    {
        private List<KxPaymentAllocation> items = new List<KxPaymentAllocation>();

        protected int      fromStudentChargeId = 0;
        protected int      fromStudentPaymentId = 0;
        protected int      toStudentChargeId = 0;
        protected int      toStudentPaymentId = 0;

        public List<KxPaymentAllocation> Items
        {
            get
            { return items; }
        }

        public KxPaymentAllocation this[int index]
        {
            get
            { return items[index]; }
            set
            { items[index] = value; }
        }

        public KxPaymentAllocations(KxBaseClass parent,int fromCharge,int toCharge,int toPayment,int fromPayment) : base(parent)
        {
            fromStudentChargeId = fromCharge;
            fromStudentPaymentId = fromPayment;
            toStudentChargeId = toCharge;
            toStudentPaymentId = toPayment;

            Load();
        }

        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = null;

            if (actiontype == ActionType.Select)
            {
                result = new KxData("st2paymentallocations", "ST2PaymentAllocations", TargetType.Table, ActionType.Select);

                if (fromStudentChargeId > 0)
                {
                    result.Parameters.Add(new KxDataValue("fromStudentChargeId", fromStudentChargeId, KxValueType.Int));
                }
                else if (fromStudentPaymentId > 0)
                {
                    result.Parameters.Add(new KxDataValue("fromStudentPaymentId", fromStudentPaymentId, KxValueType.Int));
                }
                else if (toStudentChargeId > 0)
                {
                    result.Parameters.Add(new KxDataValue("toStudentChargeId", toStudentChargeId, KxValueType.Int));
                }
                else 
                {
                    result.Parameters.Add(new KxDataValue("toStudentPaymentId", toStudentPaymentId, KxValueType.Int));
                }
                result.AddOrderField("creationDate", KxDataOrderType.Ascending);
            }
            return result;
        }

        public override bool LoadFromXml(string xml, string xpath)
        {
            items.Clear();
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xml);

                XmlNodeList nodes = doc.SelectNodes(@"//results/" + xpath + "/datarow");

                foreach (XmlNode node in nodes)
                {
                    if (node != null)
                    {
                        KxPaymentAllocation item = new KxPaymentAllocation(this, node);
                        items.Add(item);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "LoadFromXml", ex.Message, xpath);
                LastError = ex.Message;
                return false;
            }
        }

        public KxPaymentAllocation GetPaymentAllocation(int id)
        {
            KxPaymentAllocation result = null;

            foreach (KxPaymentAllocation i in Items)
            {
                if (i.PaymentAllocationId == id)
                {
                    result = i;
                    break;
                }
            }
            return result;
        }

    }
}

