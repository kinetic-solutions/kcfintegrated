using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Exceptions;
using Kx.Core.Utility;

namespace Kx.Objects.Student
{
    public class KxStudentAllocationDates : KxBaseClass
    {
        private bool PaymentDeducted = false;
        private int allocationId;
        private List<KxStudentAllocationDate> items = new List<KxStudentAllocationDate>();

        public int AllocationId
        {
            get
            { return allocationId; }
            set
            { allocationId = value; }
        }

        public List<KxStudentAllocationDate> Items
        {
            get
            { return items; }
        }

        public KxStudentAllocationDate this[int index]
        {
            get
            { return items[index]; }
            set
            { items[index] = value; }
        }

        public DateTime AllocationStartDate
        {
            get
            { return GetAllocationStartDate(); }
        }

        public DateTime AllocationEndDate
        {
            get
            { return GetAllocationEndDate(); }
        }

        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = null;

            if (actiontype == ActionType.Select)
            {
                result = new KxData("studentallocationdates", "ST2StudentAllocationdates", TargetType.Table);

                result.KeyFields.Add(new KxDataValue("allocationId", allocationId, KxValueType.Int));

                result.AddOrderField("arrivaldate", KxDataOrderType.Ascending);
            }
            return result;
        }

        public KxStudentAllocationDates(KxBaseClass parent, int allocationId) : base(parent)
        {
            this.allocationId = allocationId;
            Load();
        }

        public override bool LoadFromXml(string xml, string xpath)
        {
            items.Clear();
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xml);

                XmlNodeList nodes = doc.SelectNodes(@"//results/" + xpath + "/datarow");

                foreach (XmlNode node in nodes)
                {
                    if (node != null)
                    {
                        KxStudentAllocationDate sad = new KxStudentAllocationDate(this, node);
                        Items.Add(sad);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "LoadFromXml", ex.Message, xpath);
                LastError = ex.Message;
                return false;
            }
        }


        public DateTime GetAllocationStartDate()
        {
            DateTime result = KxDateTime.KxNullDate;

            foreach (KxStudentAllocationDate dte in Items)
            {
                if ((result == KxDateTime.KxNullDate))
                {
                    result = ((DateTime)dte.ArrivalDate);
                }
                else
                {
                    if (dte.ArrivalDate < result)
                    {
                        result = ((DateTime)dte.ArrivalDate);
                    }
                }
            }
            return result;
        }

        public KxStudentAllocationDate GetFirstAllocation()
        {
            KxStudentAllocationDate result = null;

            foreach (KxStudentAllocationDate dte in Items)
            {
                if ((result == null))
                {
                    result = dte;
                }
                else
                {
                    if (dte.ArrivalDate < result.ArrivalDate)
                    {
                        result = dte;
                    }
                }
            }
            return result;
        }

        public DateTime GetAllocationEndDate()
        {
            DateTime result = KxDateTime.KxNullDate;

            foreach (KxStudentAllocationDate dte in Items)
            {
               if (dte.DepartureDate > result)
               {
                   result = ((DateTime)dte.DepartureDate);
               }
            }
            return result;
        }

        public void DeductPrepayment(Decimal amount)
        {
            if (!PaymentDeducted)
            {
                try
                {
                    KxStudentAllocationDate dte = Items[Items.Count - 1];

                    dte.AdjustedAllocationFees = (dte.AllocationFees - amount);
                }
                catch { }

                PaymentDeducted = true;
            }
        }

        public bool AllocatedOnDate(DateTime date)
        {
            return GetAllocationDate(date) != null;
        }

        public KxStudentAllocationDate GetAllocationDate(DateTime datespan)
        {
            KxStudentAllocationDate result = null;

            foreach (KxStudentAllocationDate allocdate in Items)
            {
                if (allocdate.ArrivalDate <= datespan && allocdate.DepartureDate > datespan)
                {
                    result = allocdate;
                    break;
                }
            }

            return result;
        }

        public bool GetCheckedIn()
        {
            bool result = false;

            foreach (KxStudentAllocationDate date in Items)
            {
                if (date.CheckedInDate > KxDateTime.KxApplicableDate)
                {
                    result = true;
                    break;
                }
            }
            return result;
        }

    
    }
}
