using System;
using System.Data;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;

namespace Kx.Objects.Student
{
    [Serializable]
    public class KxStudentRequest : KxBaseClass
    {
        #region Attributes & Properties

        protected int _kxStudentId;
        protected int _applicationId;
        protected int _allocationId;
        protected DateTime? _requestDate;
        protected int _requestTypeId;
        protected int _requestStatusId;
        protected DateTime? _lastStatusUpdate;
        protected string _lastStatusUpdateBy;

        public int StudentRequestId
        {
            get { return base.dBIdentifier; }
        }

        public int KxStudentId
        {
            get { return _kxStudentId; }
            set
            {
                AddToAudit("KxStudentId", _kxStudentId, value);
                _kxStudentId = value;
            }
        }

        public int ApplicationId
        {
            get { return _applicationId; }
            set
            {
                AddToAudit("ApplicationId", _applicationId, value);
                _applicationId = value;
            }
        }

        public int AllocationId
        {
            get { return _allocationId; }
            set
            {
                AddToAudit("AllocationId", _allocationId, value);
                _allocationId = value;
            }
        }

        public DateTime? RequestDate
        {
            get { return _requestDate; }
            set
            {
                AddToAudit("RequestDate", _requestDate, value);
                _requestDate = value;
            }
        }

        public int RequestTypeId
        {
            get { return _requestTypeId; }
            set
            {
                AddToAudit("RequestTypeId", _requestTypeId, value);
                _requestTypeId = value;
            }
        }

        private KxRequestType _requestType;
        public KxRequestType RequestType
        {
            get
            {
                if (_requestType == null)
                {
                    _requestType = new KxRequestType(null, this._requestTypeId);
                }

                return _requestType;
            }
        }

        public string RequestTypeDescription
        {
            get
            {
                try
                {
                    return RequestType.Description;
                }
                catch
                {
                    return string.Empty;
                }
            }
        }

        public string RequestTypeWebKey
        {
            get
            {
                try
                {
                    return RequestType.WebKey;
                }
                catch
                {
                    return string.Empty;
                }
            }
        }

        public int RequestStatusId
        {
            get { return _requestStatusId; }
            set
            {
                AddToAudit("RequestStatusId", _requestStatusId, value);
                _requestStatusId = value;
            }
        }

        protected KxRequestStatus _requestStatus;
        public KxRequestStatus RequestStatus
        {
            get
            {
                if (_requestStatus == null)
                {
                    _requestStatus = new KxRequestStatus(null, this._requestStatusId);
                }
                return _requestStatus;
            }
        }

        public string RequestStatusDescription
        {
            get
            {
                try
                {
                    return this.RequestStatus.Description;
                }
                catch
                {
                    return string.Empty;
                }
            }
        }

        public DateTime? LastStatusUpdate
        {
            get { return _lastStatusUpdate; }
            set
            {
                AddToAudit("LastStatusUpdate", _lastStatusUpdate, value);
                _lastStatusUpdate = value;
            }
        }

        public string LastStatusUpdateBy
        {
            get { return _lastStatusUpdateBy; }
            set
            {
                AddToAudit("LastStatusUpdateBy", _lastStatusUpdateBy, value);
                _lastStatusUpdateBy = value;
            }
        }

        #endregion


        #region Constructors
        public KxStudentRequest(KxBaseClass parent)
            : base(parent)
        { }


        public KxStudentRequest(KxBaseClass parent, XmlNode node)
            : base(parent)
        {
            LoadFromXml("", node);
        }

        public KxStudentRequest(KxBaseClass parent, int studentrequestid)
            : base(parent)
        {
            base.dBIdentifier = studentrequestid;
            Load();
        }

        #endregion

        public KxStudentRequestData GetDataValueByLabel(string label)
        {

            if (this.StudentRequestId > 0 && children.Children.Count == 0)
            {
                LoadChildData();
            }
            KxStudentRequestData data = children.Children.Find(s => (s is KxStudentRequestData && (s as KxStudentRequestData).DataLabel == label)) as KxStudentRequestData;
            if (data == null)
            {
                data = new KxStudentRequestData(this);
                data.DataLabel = label;
                data.DataValue = string.Empty;
            }

            return data;
        }

        private void LoadChildData()
        {
            foreach (var data in KxStudentRequestData.GetST2StudentRequestData(this.StudentRequestId))
            {
                data.Parent = this;
                children.Add(data);
            }
        }

        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("st2studentrequests", "ST2StudentRequests", TargetType.Table);

            result.AutoKeyField = new KxDataValue("StudentRequestId", StudentRequestId, KxValueType.Int);


            if ((actiontype == ActionType.Update) && (StudentRequestId == 0))
            {
                result.Action = ActionType.Insert;
            }
            else
            {
                result.Action = actiontype;
            }

            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {
                result.AddParameter("kxStudentId", _kxStudentId, KxValueType.Int, false);
                result.AddParameter("applicationId", _applicationId, KxValueType.Int, false);
                result.AddParameter("allocationId", _allocationId, KxValueType.Int, false);
                result.AddParameter("requestDate", _requestDate, KxValueType.DateTime, false);
                result.AddParameter("requestTypeId", _requestTypeId, KxValueType.Int, false);
                result.AddParameter("requestStatusId", _requestStatusId, KxValueType.Int, false);
                result.AddParameter("lastStatusUpdate", _lastStatusUpdate, KxValueType.DateTime, false);
                result.AddParameter("lastStatusUpdateBy", _lastStatusUpdateBy, KxValueType.String, 50, false);
            }
            return result;
        }


        public override void LoadFromXml(string xml, XmlNode node)
        {
            dBIdentifier = KxXmlUtils.GetNodeValue(node, "studentrequestid", 0);
            _kxStudentId = KxXmlUtils.GetNodeValue(node, "kxstudentid", 0);
            _applicationId = KxXmlUtils.GetNodeValue(node, "applicationid", 0);
            _allocationId = KxXmlUtils.GetNodeValue(node, "allocationid", 0);
            _requestDate = KxXmlUtils.GetNodeValueDT(node, "requestdate", null);
            _requestTypeId = KxXmlUtils.GetNodeValue(node, "requesttypeid", 0);
            _requestStatusId = KxXmlUtils.GetNodeValue(node, "requeststatusid", 0);
            _lastStatusUpdate = KxXmlUtils.GetNodeValueDT(node, "laststatusupdate", null);
            _lastStatusUpdateBy = KxXmlUtils.GetNodeValue(node, "laststatusupdateby", string.Empty);
        }

        public static List<KxStudentRequest> GetST2StudentRequests(int kxStudentId, int applicationId, int allocationId)
        {
            List<KxStudentRequest> list = new List<KxStudentRequest>();
            KxData data = new KxData("st2studentrequests", "ST2StudentRequests", TargetType.Table);
            if (kxStudentId > 0) { data.AddKeyField("kxstudentid", kxStudentId, KxValueType.Int);}
            if (applicationId > 0) { data.AddKeyField("applicationid", applicationId, KxValueType.Int);}
            if (allocationId > 0) { data.AddKeyField("allocationid", allocationId, KxValueType.Int); }

            if (kxStudentId + applicationId + allocationId > 0)
            {
                if (data.Execute())
                {
                    XmlDocument doc = new XmlDocument();
                    doc.LoadXml(data.XmlResults);
                    XmlNodeList nodes = doc.SelectNodes(@"//st2studentrequests/datarow");
                    foreach (XmlNode node in nodes)
                    {
                        list.Add(new KxStudentRequest(null, node));
                    }
                }
            }

            return list;
        }
    }
}

