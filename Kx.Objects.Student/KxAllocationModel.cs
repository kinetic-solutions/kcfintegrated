using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;

namespace Kx.Objects.Student
{
    [Serializable]
    public class KxAllocationModel : KxBaseStudentChargingModel
    {
        #region Attributes & Properties

        protected string   description;
        protected DateTime ?creationDate;
        protected int      creationUserId;
        protected DateTime ?inactiveDate;
     //   protected int      studentChargeId1;
     //   protected int      studentChargeId2;
     //   protected int      studentChargeId3;
     //   protected int      studentChargeId4;
        protected int      depositId1;
        protected int      depositId2;
        protected int      depositId3;
        protected int      depositId4;

        protected int academicYearId;

        protected KxAllocationModelDates dates = null;
        protected KxPricingModel pricingModel = null;

 
        public int      AllocationModelId
        {
            get
            { return base.dBIdentifier;}
        }

        public string   Description
        {
            get
            { return description;}
            set
            { 
                AddToAudit("Description", description, value);
                description = value;
            }
        }

        public DateTime ?CreationDate
        {
            get
            { return creationDate;}
            set
            { 
                AddToAudit("CreationDate", creationDate, value);
                creationDate = value;
            }
        }

        public int      CreationUserId
        {
            get
            { return creationUserId;}
            set
            { 
                AddToAudit("CreationUserId", creationUserId, value);
                creationUserId = value;
            }
        }

        public DateTime ?InactiveDate
        {
            get
            { return inactiveDate;}
            set
            { 
                AddToAudit("InactiveDate", inactiveDate, value);
                inactiveDate = value;
            }
        }
        /*
        public int      StudentChargeId1
        {
            get
            { return studentChargeId1;}
            set
            { 
                AddToAudit("StudentChargeId1", studentChargeId1, value);
                studentChargeId1 = value;
            }
        }

        public int      StudentChargeId2
        {
            get
            { return studentChargeId2;}
            set
            { 
                AddToAudit("StudentChargeId2", studentChargeId2, value);
                studentChargeId2 = value;
            }
        }

        public int      StudentChargeId3
        {
            get
            { return studentChargeId3;}
            set
            { 
                AddToAudit("StudentChargeId3", studentChargeId3, value);
                studentChargeId3 = value;
            }
        }

        public int      StudentChargeId4
        {
            get
            { return studentChargeId4;}
            set
            { 
                AddToAudit("StudentChargeId4", studentChargeId4, value);
                studentChargeId4 = value;
            }
        }
        */
        public int      DepositId1
        {
            get
            { return depositId1;}
            set
            { 
                AddToAudit("DepositId1", depositId1, value);
                depositId1 = value;
            }
        }

        public int      DepositId2
        {
            get
            { return depositId2;}
            set
            { 
                AddToAudit("DepositId2", depositId2, value);
                depositId2 = value;
            }
        }

        public int      DepositId3
        {
            get
            { return depositId3;}
            set
            { 
                AddToAudit("DepositId3", depositId3, value);
                depositId3 = value;
            }
        }

        public int      DepositId4
        {
            get
            { return depositId4;}
            set
            { 
                AddToAudit("DepositId4", depositId4, value);
                depositId4 = value;
            }
        }

        public int AcademicYearId
        {
            get
            {
                return academicYearId;
            }
            set
            {
                if (academicYearId == value)
                    return;
                academicYearId = value;
            }
        }

        public KxAllocationModelDates Dates
        {
            get
            {
                if (dates == null)
                {
                    dates = new KxAllocationModelDates(this, AllocationModelId, AcademicYearId);
                }
                return dates;
            }
        }

        public Decimal TotalWeeks
        {
            get
            {
                return Dates.GetTotalWeeks(true);
            }
        }

        public Decimal AllocationWeeks
        {
            get
            {
                return Dates.GetTotalWeeks(false);
            }
        }

        public Decimal AllocationWeeksUnrounded
        {
            get
            {
                return Dates.GetTotalWeeksUnrounded(false);
            }
        }

        public Decimal TotalAdditionalCharges
        {
            get
            {
                return AdditionalCharges + Dates.AdditionalCharges;
            }
        }

        public int[] AllocatedAllocationDates
        {
            get
            {
                return GetAllocationDates(true);
            }
        }

        public int[] AllAllocationDates
        {
            get
            {
                return GetAllocationDates(false);
            }
        }


        public KxPricingModel PricingModel
        {
            get
            {
                return pricingModel;
            }
            set
            {
                pricingModel = value;
            }
        }

        #endregion 
        
        public KxAllocationModel(KxBaseClass parent):base(parent)
        {}
        
        public KxAllocationModel(KxBaseClass parent, XmlNode node):base(parent)
        {
             LoadFromXml("", node);
        }

        public KxAllocationModel(KxBaseClass parent, int allocationModelId)
            : base(parent)
        {
            base.dBIdentifier = allocationModelId;
            Load();
        }

        public KxAllocationModel(KxBaseClass parent,int      allocationmodelid, int academicYearId):base(parent)
        {
            this.AcademicYearId = academicYearId;
             base.dBIdentifier = allocationmodelid;
             Load();
        }


        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("st2allocationmodel", "ST2AllocationModel", TargetType.Table);

            result.AutoKeyField = new KxDataValue("AllocationModelId",AllocationModelId,KxValueType.Int);


            if ((actiontype == ActionType.Update) && (AllocationModelId == 0))
            {
                result.Action = ActionType.Insert;
            }
            else
            {
                result.Action = actiontype;
            }

            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {
                   result.AddParameter("creationDate", creationDate, KxValueType.DateTime, false);
                   result.AddParameter("creationUserId", creationUserId, KxValueType.Int, false);
                   result.AddParameter("inactiveDate", inactiveDate, KxValueType.DateTime, true);
                   result.AddParameter("studentChargeId1", studentChargeId1, KxValueType.Int, true);
                   result.AddParameter("studentChargeId2", studentChargeId2, KxValueType.Int, true);
                   result.AddParameter("studentChargeId3", studentChargeId3, KxValueType.Int, true);
                   result.AddParameter("studentChargeId4", studentChargeId4, KxValueType.Int, true);
                   result.AddParameter("depositId1", depositId1, KxValueType.Int, true);
                   result.AddParameter("depositId2", depositId2, KxValueType.Int, true);
                   result.AddParameter("depositId3", depositId3, KxValueType.Int, true);
                   result.AddParameter("depositId4", depositId4, KxValueType.Int, true);
            }
            return result;
        }

        public override  void LoadFromXml(string xml,XmlNode node)
        {
            dBIdentifier  = KxXmlUtils.GetNodeValue(node,"allocationmodelid",0);
            description  = KxXmlUtils.GetNodeValue(node,"description","");
            creationDate  = KxXmlUtils.GetNodeValueDT(node,"creationdate",null);
            creationUserId  = KxXmlUtils.GetNodeValue(node,"creationuserid",0);
            inactiveDate  = KxXmlUtils.GetNodeValueDT(node,"inactivedate",null);
            studentChargeId1  = KxXmlUtils.GetNodeValue(node,"studentchargeid1",0);
            studentChargeId2  = KxXmlUtils.GetNodeValue(node,"studentchargeid2",0);
            studentChargeId3  = KxXmlUtils.GetNodeValue(node,"studentchargeid3",0);
            studentChargeId4  = KxXmlUtils.GetNodeValue(node,"studentchargeid4",0);
            depositId1  = KxXmlUtils.GetNodeValue(node,"depositid1",0);
            depositId2  = KxXmlUtils.GetNodeValue(node,"depositid2",0);
            depositId3  = KxXmlUtils.GetNodeValue(node,"depositid3",0);
            depositId4  = KxXmlUtils.GetNodeValue(node,"depositid4",0);
        }

        private int[] GetAllocationDates(bool AllocatedOnly)
        {
            List<int> result = new List<int>();

            foreach (KxAllocationModelDate dte in Dates.Items)
            {
                if (!AllocatedOnly || (AllocatedOnly && dte.AllocateDuringPeriod == 1))
                {
                    result.Add(dte.AllocationModelDateId);
                }
            }
            return result.ToArray();
        }

        public List<KxAllocationModelDate> GetAllocatedDates()
        {
            List<KxAllocationModelDate> result = new List<KxAllocationModelDate>();

            foreach (KxAllocationModelDate dte in Dates.Items)
            {
                if (dte.AllocateDuringPeriod == 1)
                {
                    result.Add(dte);
                }
            }
            return result;
        }

        public KxAllocationModelDate GetAllocationModelDateById(int id)
        {
            KxAllocationModelDate result = null;

            foreach (KxAllocationModelDate dte in Dates.Items)
            {
                if (dte.AllocationModelDateId == id)
                {
                    result = dte;
                    break;
                }
            }
            return result;
        }
    }
}

