using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Exceptions;
using Kx.Core.Utility;

namespace Kx.Objects.Student
{
    public class KxPaymentScheduleTypeModels : KxBaseClass
    {
      
        private int paymentScheduleTypeId;
        private List<KxPaymentScheduleTypeModel> items = new List<KxPaymentScheduleTypeModel>();

        public int PaymentScheduleTypeId
        {
            get
            { return paymentScheduleTypeId; }
            set
            { paymentScheduleTypeId = value; }
        }

        public List<KxPaymentScheduleTypeModel> Items
        {
            get
            { return items; }
        }

        public KxPaymentScheduleTypeModel this[int index]
        {
            get
            { return items[index]; }
            set
            { items[index] = value; }
        }

        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = null;

            if (actiontype == ActionType.Select)
            {
                result = new KxData("scheduletypemodel", "st2paymentscheduletypemodel", TargetType.Table);

                result.KeyFields.Add(new KxDataValue("PaymentScheduleTypeId", PaymentScheduleTypeId, KxValueType.Int));
                result.KeyFields.Add(new KxDataValue("Inactive", 0, KxValueType.Int));

                result.AddOrderField("fixeddate", KxDataOrderType.Ascending);
            }
            return result;
        }

        public KxPaymentScheduleTypeModels(KxBaseClass parent, int paymentScheduleTypeId) : base(parent)
        {
            this.paymentScheduleTypeId = paymentScheduleTypeId;
            Load();
        }

        public override bool LoadFromXml(string xml, string xpath)
        {
            items.Clear();
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xml);

                XmlNodeList nodes = doc.SelectNodes(@"//results/" + xpath + "/datarow");

                foreach (XmlNode node in nodes)
                {
                    if (node != null)
                    {
                        KxPaymentScheduleTypeModel mod = new KxPaymentScheduleTypeModel(this, node);
                        Items.Add(mod);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "LoadFromXml", ex.Message, xpath);
                LastError = ex.Message;
                return false;
            }
        }

    }
}
