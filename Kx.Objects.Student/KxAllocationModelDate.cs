using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;

namespace Kx.Objects.Student
{
    [Serializable]
    public class KxAllocationModelDate : KxBaseStudentChargingModel
    {
        #region Attributes & Properties

        protected int      allocationModelId;
        protected DateTime ?startDate;
        protected DateTime ?endDate;
        protected int      academicYearId;
        protected int      allocateDuringPeriod;
        protected int      periodNumber;
        protected DateTime? _dueDate;

        protected decimal charges = 0.00M;

        public KxAllocationModel AllocationModel
        {
            get
            {
                if (Parent != null && Parent.Parent != null && Parent.Parent is KxAllocationModel)
                {
                    return ((KxAllocationModel)Parent.Parent);
                }
                else 
                {
                    return null;
                }
            }
        }

        
        public int      AllocationModelDateId
        {
            get
            { return base.dBIdentifier;}
        }

        public int      AllocationModelId
        {
            get
            { return allocationModelId;}
            set
            { 
                AddToAudit("AllocationModelId", allocationModelId, value);
                allocationModelId = value;
            }
        }

        public DateTime ?StartDate
        {
            get
            { return startDate;}
            set
            { 
                AddToAudit("StartDate", startDate, value);
                startDate = value;
            }
        }

        public DateTime ?EndDate
        {
            get
            { return endDate;}
            set
            { 
                AddToAudit("EndDate", endDate, value);
                endDate = value;
            }
        }

        public DateTime? DueDate
        {
            get
            {
                return _dueDate;
            }
            set
            {
                AddToAudit("DueDate", _dueDate, value);
            	_dueDate = value;
            }
        }

        public int      AcademicYearId
        {
            get
            { return academicYearId;}
            set
            { 
                AddToAudit("AcademicYearId", academicYearId, value);
                academicYearId = value;
            }
        }

        public int      AllocateDuringPeriod
        {
            get
            { return allocateDuringPeriod;}
            set
            { 
                AddToAudit("AllocateDuringPeriod", allocateDuringPeriod, value);
                allocateDuringPeriod = value;
            }
        }

    
        public int      PeriodNumber
        {
            get
            { return periodNumber;}
            set
            { 
                AddToAudit("PeriodNumber", periodNumber, value);
                periodNumber = value;
            }
        }

        public int Days
        {
            get
            {
                return GetDaySpan(); 
            }

        }

        public int FullWeeks
        {
            get
            {
                return Convert.ToInt32(System.Math.Floor((Decimal)Days / 7));
            }
        }

        public int RemainderDays
        {
            get
            {
                return Convert.ToInt32(Days - (FullWeeks * 7));
            }
        }


        public Decimal Charges
        {
            get
            {
                if (AllocationModel.PricingModel != null && charges == 0.00M)
                {
                    charges = GetCharges();
                }
                return charges;
            }
        }

        private static string _dueDateColumnExists;
        public static void ResetDueDateColumnExists ()
        {
            _dueDateColumnExists = null;
        }

        public bool DueDateColumnExists
        {
            get
            {
                const string STR_Yes = "Yes";
                if (string.IsNullOrEmpty(_dueDateColumnExists))
                {
                    KxData data = new KxData("dd", "SELECT TOP 1 DueDate FROM ST2AllocationModelDates", TargetType.Static);
                    try
                    {
                        if (data.Execute())
                        {
                            _dueDateColumnExists = STR_Yes;
                        }
                        else
                        {
                            _dueDateColumnExists = "No";
                        }
                    }
                    catch
                    {
                        _dueDateColumnExists = "No";
                    }
                }

                return _dueDateColumnExists == STR_Yes;
            }
        }
        

        #endregion 


        public KxAllocationModelDate(KxBaseClass parent)
            : base(parent)
        { }


        public KxAllocationModelDate(KxBaseClass parent, XmlNode node):base(parent)
        {
             LoadFromXml("", node);
        }

        public KxAllocationModelDate(KxBaseClass parent,int      allocationmodeldateid):base(parent)
        {
             base.dBIdentifier = allocationmodeldateid;
             Load();
        }

        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("st2allocationmodeldates", "ST2AllocationModelDates", TargetType.Table);

            result.AutoKeyField = new KxDataValue("AllocationModelDateId",AllocationModelDateId,KxValueType.Int);

            if ((actiontype == ActionType.Update) && (AllocationModelDateId == 0))
            {
                result.Action = ActionType.Insert;
            }
            else
            {
                result.Action = actiontype;
            }

            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {
                   result.AddParameter("startDate", startDate, KxValueType.DateTime, false);
                   result.AddParameter("endDate", endDate, KxValueType.DateTime, false);
                   result.AddParameter("academicYearId", academicYearId, KxValueType.Int, false);
                   result.AddParameter("allocateDuringPeriod", allocateDuringPeriod, KxValueType.Int, false);
                   result.AddParameter("studentChargeId1", studentChargeId1, KxValueType.Int, true);
                   result.AddParameter("studentChargeId2", studentChargeId2, KxValueType.Int, true);
                   result.AddParameter("studentChargeId3", studentChargeId3, KxValueType.Int, true);
                   result.AddParameter("studentChargeId4", studentChargeId4, KxValueType.Int, true);
                   result.AddParameter("periodNumber", periodNumber, KxValueType.Int, false);
                    // Not done because we should never write to this.
                // TODO: Write Due date if required at a later stage.
                   //result.AddParameter("duedate", endDate, KxValueType.DateTime, false);
            }
            return result;
        }
        
        public override  void LoadFromXml(string xml,XmlNode node)
        {
            dBIdentifier  = KxXmlUtils.GetNodeValue(node,"allocationmodeldateid",0);
            allocationModelId  = KxXmlUtils.GetNodeValue(node,"allocationmodelid",0);
            startDate  = KxXmlUtils.GetNodeValueDT(node,"startdate",null);
            endDate  = KxXmlUtils.GetNodeValueDT(node,"enddate",null);
            academicYearId  = KxXmlUtils.GetNodeValue(node,"academicyearid",0);
            allocateDuringPeriod  = KxXmlUtils.GetNodeValue(node,"allocateduringperiod",0);
            studentChargeId1  = KxXmlUtils.GetNodeValue(node,"studentchargeid1",0);
            studentChargeId2  = KxXmlUtils.GetNodeValue(node,"studentchargeid2",0);
            studentChargeId3  = KxXmlUtils.GetNodeValue(node,"studentchargeid3",0);
            studentChargeId4  = KxXmlUtils.GetNodeValue(node,"studentchargeid4",0);
            periodNumber  = KxXmlUtils.GetNodeValue(node,"periodnumber",0);
            _dueDate = KxXmlUtils.GetNodeValueDT(node, "duedate", null);
            if (DueDateColumnExists && (_dueDate == null || _dueDate.Value.Date < DateTime.Now.Date))
            {
                _dueDate = DateTime.Now.Date;
            }
        }

        private int GetDaySpan()
        {
            int result = 0;

            try
            {
                TimeSpan ts = EndDate.Value.Subtract(StartDate.Value);
                result = Convert.ToInt32(ts.TotalDays);
            }
            catch
            {}

            return result;
        }

        private Decimal GetCharges()
        {
            Decimal result = 0.00M;
            
            if (AllocationModel != null)
            {
            KxPricingModelCharge priceCharge;

            if (StudentChargeId1 > 0)
            {
                priceCharge = AllocationModel.PricingModel.GetChargeByType(StudentChargeId1, AllocationModel.AcademicYearId);
                result += (priceCharge.WeeklyAmount * FullWeeks)+(priceCharge.DailyAmount * RemainderDays);
            }

            if (StudentChargeId2 > 0)
            {
                priceCharge = AllocationModel.PricingModel.GetChargeByType(StudentChargeId2, AllocationModel.AcademicYearId);
                result += (priceCharge.WeeklyAmount * FullWeeks) + (priceCharge.DailyAmount * RemainderDays);
            }


            if (StudentChargeId3 > 0)
            {
                priceCharge = AllocationModel.PricingModel.GetChargeByType(StudentChargeId3, AllocationModel.AcademicYearId);
                result += (priceCharge.WeeklyAmount * FullWeeks) + (priceCharge.DailyAmount * RemainderDays);
            }


            if (StudentChargeId4 > 0)
            {
                priceCharge = AllocationModel.PricingModel.GetChargeByType(StudentChargeId4, AllocationModel.AcademicYearId);
                result += (priceCharge.WeeklyAmount * FullWeeks) + (priceCharge.DailyAmount * RemainderDays);
            }
            }

            return result;
        }
    }
}

