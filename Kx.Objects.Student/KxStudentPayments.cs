using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Exceptions;
using Kx.Core.Utility;

namespace Kx.Objects.Student
{
    public enum KxPaymentSource {Application, Student};

    public class KxStudentPayments : KxBaseClass
    {
        private int kxStudentId = 0;
        private int applicationId = 0;

        private List<KxStudentPayment> items = new List<KxStudentPayment>();

        protected KxPaymentSource paymentSource = KxPaymentSource.Student;

        public int KxStudentId
        {
            get
            { return kxStudentId; }
            set
            { kxStudentId = value; }
        }

        public List<KxStudentPayment> Items
        {
            get
            { return items; }
        }

        public KxStudentPayment this[int index]
        {
            get
            { return items[index]; }
            set
            { items[index] = value; }
        }

        public bool HasDeposit
        {
            get
            {
                return GetHasDeposit();
            }
        }
        public Decimal TotalDeposit
        {
            get
            {
                return GetTotalDeposit();
            }
        }

        public Decimal TotalPayment
        {
            get
            {
                return GetTotalPayment();
            }
        }


        public override KxData ConfigureData(ActionType actiontype)
        {
             KxData result = null;

             if (actiontype == ActionType.Select)
             {
                 result = new KxData("studentpayments", "ST2StudentPayments", TargetType.Table);

                 if (paymentSource == KxPaymentSource.Student)
                 {
                     result.KeyFields.Add(new KxDataValue("kxStudentId", kxStudentId, KxValueType.Int));
                 }
                 else
                 {
                     result.KeyFields.Add(new KxDataValue("applicationId", applicationId, KxValueType.Int));
                 }
             }
            return result;
        }

        public KxStudentPayments(KxBaseClass parent, int identifier,KxPaymentSource source) : base(parent)
        {
            paymentSource = source;
            if (paymentSource == KxPaymentSource.Student)
            {
                kxStudentId = identifier;
            }
            else
            {
                applicationId = identifier;
            }
            Load();
        }

        public override bool LoadFromXml(string xml, string xpath)
        {
            items.Clear();
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xml);

                XmlNodeList nodes = doc.SelectNodes(@"//results/" + xpath + "/datarow");

                foreach (XmlNode node in nodes)
                {
                    if (node != null)
                    {
                        KxStudentPayment paym = new KxStudentPayment(this, node);
                        Items.Add(paym);
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "LoadFromXml", ex.Message, xpath);
                LastError = ex.Message;
                return false;
            }
        }

        private Decimal GetTotalDeposit()
        {
            Decimal result = 0.0M;

            foreach (KxStudentPayment paym in Items)          
            {
                if (paym.IsDeposit)
                {
                    result += Math.Abs(paym.Amount);
                }
            }
            return result;
        }

        private Decimal GetTotalPayment()
        {
            Decimal result = 0.0M;

            foreach (KxStudentPayment paym in Items)
            {
                if (!paym.IsDeposit)
                {
                    result += Math.Abs(paym.Amount);
                }
            }
            return result;
        }

        private bool GetHasDeposit()
        {
            Boolean result = false;

            foreach (KxStudentPayment payment in Items)
            {
                if (payment.IsDeposit)
                {
                    result = true;
                    break;
                }
            }
            return result;
        }



    }
}
