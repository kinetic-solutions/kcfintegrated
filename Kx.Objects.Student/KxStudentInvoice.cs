using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;

namespace Kx.Objects.Student
{
    [Serializable]
    public class KxStudentInvoice : KxBaseClass
    {
        #region Attributes & Properties

        protected int      invoiceNumber = 0;
        protected DateTime ?creationDate;
        protected int      creationUserId;
        protected int      kxStudentId;
        protected int      applicationId;
        protected DateTime ?printedDate;
        protected int      printedUserId;
        protected DateTime ?taxPointDate;
        protected int      plannedPaymentMethodId;
        protected string   invoiceToAddress;
        protected string   externalAccountCode;
        protected string   invoiceType;
        protected int      sponsorId;
        protected int      originalInvoiceNumber;

        protected KxStudentCharges lines = null;

        public int      InvoiceNumber
        {
            get
            { return invoiceNumber;}
            set
            { 
                AddToAudit("InvoiceNumber", invoiceNumber, value);
                invoiceNumber = value;
            }
        }

        public DateTime ?CreationDate
        {
            get
            { return creationDate;}
            set
            { 
                AddToAudit("CreationDate", creationDate, value);
                creationDate = value;
            }
        }

        public int      CreationUserId
        {
            get
            { return creationUserId;}
            set
            { 
                AddToAudit("CreationUserId", creationUserId, value);
                creationUserId = value;
            }
        }

        public int      KxStudentId
        {
            get
            { return kxStudentId;}
            set
            { 
                AddToAudit("KxStudentId", kxStudentId, value);
                kxStudentId = value;
            }
        }

        public int      ApplicationId
        {
            get
            { return applicationId;}
            set
            { 
                AddToAudit("ApplicationId", applicationId, value);
                applicationId = value;
            }
        }

        public DateTime ?PrintedDate
        {
            get
            { return printedDate;}
            set
            { 
                AddToAudit("PrintedDate", printedDate, value);
                printedDate = value;
            }
        }

        public int      PrintedUserId
        {
            get
            { return printedUserId;}
            set
            { 
                AddToAudit("PrintedUserId", printedUserId, value);
                printedUserId = value;
            }
        }

        public DateTime ?TaxPointDate
        {
            get
            { return taxPointDate;}
            set
            { 
                AddToAudit("TaxPointDate", taxPointDate, value);
                taxPointDate = value;
            }
        }

        public int      PlannedPaymentMethodId
        {
            get
            { return plannedPaymentMethodId;}
            set
            { 
                AddToAudit("PlannedPaymentMethodId", plannedPaymentMethodId, value);
                plannedPaymentMethodId = value;
            }
        }

        public string   InvoiceToAddress
        {
            get
            { return invoiceToAddress;}
            set
            { 
                AddToAudit("InvoiceToAddress", invoiceToAddress, value);
                invoiceToAddress = value;
            }
        }

        public string   ExternalAccountCode
        {
            get
            { return externalAccountCode;}
            set
            { 
                AddToAudit("ExternalAccountCode", externalAccountCode, value);
                externalAccountCode = value;
            }
        }

        public string   InvoiceType
        {
            get
            { return invoiceType;}
            set
            { 
                AddToAudit("InvoiceType", invoiceType, value);
                invoiceType = value;
            }
        }

        public int      SponsorId
        {
            get
            { return sponsorId;}
            set
            { 
                AddToAudit("SponsorId", sponsorId, value);
                sponsorId = value;
            }
        }

        public int      OriginalInvoiceNumber
        {
            get
            { return originalInvoiceNumber;}
            set
            { 
                AddToAudit("OriginalInvoiceNumber", originalInvoiceNumber, value);
                originalInvoiceNumber = value;
            }
        }

        public KxStudentCharges Lines
        {
            get
            {
                if (lines == null && invoiceNumber !=0)
                {
                    lines = new KxStudentCharges(this, invoiceNumber, KxStudentChargingType.Invoice);
                }
                return lines;
            }
        }

        public Decimal TotalValue
        {
            get
            { return Lines.TotalCharges; }
        }
        
        #endregion 

        public KxStudentInvoice(KxBaseClass parent, int InvoiceNumber): base(parent)
        {
            this.InvoiceNumber = InvoiceNumber;
            Load();
        }

        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("st2studentinvoices", "ST2StudentInvoices", TargetType.Table);

            result.AddKeyField("InvoiceNumber", invoiceNumber, KxValueType.Int);

            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {
                   result.AddParameter("invoiceNumber", invoiceNumber, KxValueType.Int);
                   result.AddParameter("creationDate", creationDate, KxValueType.DateTime);
                   result.AddParameter("creationUserId", creationUserId, KxValueType.Int);
                   result.AddParameter("kxStudentId", kxStudentId, KxValueType.Int);
                   result.AddParameter("applicationId", applicationId, KxValueType.Int);
                   result.AddParameter("printedDate", printedDate, KxValueType.DateTime);
                   result.AddParameter("printedUserId", printedUserId, KxValueType.Int);
                   result.AddParameter("taxPointDate", taxPointDate, KxValueType.DateTime);
                   result.AddParameter("plannedPaymentMethodId", plannedPaymentMethodId, KxValueType.Int);
                   result.AddParameter("invoiceToAddress", invoiceToAddress, KxValueType.String);
                   result.AddParameter("externalAccountCode", externalAccountCode, KxValueType.String);
                   result.AddParameter("invoiceType", invoiceType, KxValueType.String);
                   result.AddParameter("sponsorId", sponsorId, KxValueType.Int);
                   result.AddParameter("originalInvoiceNumber", originalInvoiceNumber, KxValueType.Int);
            }
            return result;
        }


        public override  void LoadFromXml(string xml,XmlNode node)
        {
            invoiceNumber  = KxXmlUtils.GetNodeValue(node,"invoicenumber",0);
            creationDate  = KxXmlUtils.GetNodeValueDT(node,"creationdate",null);
            creationUserId  = KxXmlUtils.GetNodeValue(node,"creationuserid",0);
            kxStudentId  = KxXmlUtils.GetNodeValue(node,"kxstudentid",0);
            applicationId  = KxXmlUtils.GetNodeValue(node,"applicationid",0);
            printedDate  = KxXmlUtils.GetNodeValueDT(node,"printeddate",null);
            printedUserId  = KxXmlUtils.GetNodeValue(node,"printeduserid",0);
            taxPointDate  = KxXmlUtils.GetNodeValueDT(node,"taxpointdate",null);
            plannedPaymentMethodId  = KxXmlUtils.GetNodeValue(node,"plannedpaymentmethodid",0);
            invoiceToAddress  = KxXmlUtils.GetNodeValue(node,"invoicetoaddress","");
            externalAccountCode  = KxXmlUtils.GetNodeValue(node,"externalaccountcode","");
            invoiceType  = KxXmlUtils.GetNodeValue(node,"invoicetype","");
            sponsorId  = KxXmlUtils.GetNodeValue(node,"sponsorid",0);
            originalInvoiceNumber  = KxXmlUtils.GetNodeValue(node,"originalinvoicenumber",0);
        }
    }
}

