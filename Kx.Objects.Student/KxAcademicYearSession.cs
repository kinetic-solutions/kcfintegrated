using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;

namespace Kx.Objects.Student
{
    [Serializable]
    public class KxAcademicYearSession : KxBaseClass
    {
        #region Attributes & Properties

        protected int _academicYearId;
        protected string _description;
        protected bool _allowMultipleApplications;
        protected bool _applicationDates;
        protected DateTime _startDate;
        protected DateTime _endDate;
        protected bool _webPublished;
        protected bool _inactive;

        public int SessionId
        {
            get
            {
                return base.dBIdentifier;
            }

        }

        public int AcademicYearId
        {
            get { return _academicYearId; }
            set
            { 
                AddToAudit("AcademicYearId", _academicYearId, value);
                _academicYearId = value;
            }
        }

        public string Description
        {
            get { return _description; }
            set
            { 
                AddToAudit("Description", _description, value);
                _description = value;
            }
        }

        public bool AllowMultipleApplications
        {
            get { return _allowMultipleApplications; }
            set
            { 
                AddToAudit("AllowMultipleApplications", _allowMultipleApplications, value);
                _allowMultipleApplications = value;
            }
        }

        public bool ApplicationDates
        {
            get { return _applicationDates; }
            set
            { 
                AddToAudit("ApplicationDates", _applicationDates, value);
                _applicationDates = value;
            }
        }

        public DateTime StartDate
        {
            get { return _startDate; }
            set
            { 
                AddToAudit("StartDate", _startDate, value);
                _startDate = value;
            }
        }

        public DateTime EndDate
        {
            get { return _endDate; }
            set
            { 
                AddToAudit("EndDate", _endDate, value);
                _endDate = value;
            }
        }

        public bool WebPublished
        {
            get { return _webPublished; }
            set
            { 
                AddToAudit("WebPublished", _webPublished, value);
                _webPublished = value;
            }
        }

        public bool Inactive
        {
            get { return _inactive; }
            set
            { 
                AddToAudit("Inactive", _inactive, value);
                _inactive = value;
            }
        }


        #endregion 


        public KxAcademicYearSession(KxBaseClass parent):base(parent)
        { }


        public KxAcademicYearSession(KxBaseClass parent, XmlNode node):base(parent)
        {
             LoadFromXml("", node);
        }

        public KxAcademicYearSession(KxBaseClass parent,int  sessionid):base(parent)
        {
             base.dBIdentifier = sessionid;
             Load();
        }


        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("st2academicyearsessions", "dbo.ST2AcademicYearSessions", TargetType.Table);

            result.AutoKeyField = new KxDataValue("SessionId",SessionId,KxValueType.Int);


            if ((actiontype == ActionType.Update) && (SessionId == 0))
            {
                result.Action = ActionType.Insert;
            }
            else
            {
                result.Action = actiontype;
            }

            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {
                result.AddParameter("academicYearId", _academicYearId, KxValueType.Int, false);
                result.AddParameter("description", _description, KxValueType.String, 50, false);
                result.AddParameter("allowMultipleApplications", _allowMultipleApplications, KxValueType.Boolean, false);
                result.AddParameter("applicationDates", _applicationDates, KxValueType.Boolean, false);
                result.AddParameter("startDate", _startDate, KxValueType.DateTime, true);
                result.AddParameter("endDate", _endDate, KxValueType.DateTime, true);
                result.AddParameter("webPublished", _webPublished, KxValueType.Boolean, true);
                result.AddParameter("inactive", _inactive, KxValueType.Boolean, false);
            }
            return result;
        }


        public override  void LoadFromXml(string xml,XmlNode node)
        {
            dBIdentifier  = KxXmlUtils.GetNodeValue(node,"sessionid",0);
            _academicYearId  = KxXmlUtils.GetNodeValue(node,"academicyearid",0);
            _description  = KxXmlUtils.GetNodeValue(node,"description",string.Empty);
            _allowMultipleApplications  = KxXmlUtils.GetNodeValue(node,"allowmultipleapplications",false);
            _applicationDates  = KxXmlUtils.GetNodeValue(node,"applicationdates",false);
            _startDate  = KxXmlUtils.GetNodeValueDT(node,"startdate",DateTime.MinValue).Value;
            _endDate  = KxXmlUtils.GetNodeValueDT(node,"enddate",DateTime.MaxValue).Value;
            _webPublished  = KxXmlUtils.GetNodeValue(node,"webpublished",false);
            _inactive  = KxXmlUtils.GetNodeValue(node,"inactive",false);
        }

        public static IEnumerable<KxAcademicYearSession> GetST2AcademicYearSessions (int academicYearId)
        {
            KxData data = new KxData("st2academicyearsessions", "dbo.ST2AcademicYearSessions", TargetType.Table);
            data.AddKeyField("academicyearid", academicYearId, KxValueType.Int);
            data.AddKeyField("webpublished", true, KxValueType.Boolean);
            if (data.Execute ())
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(data.XmlResults);
                XmlNodeList nodes = doc.SelectNodes(@"//st2academicyearsessions/datarow");
                foreach(XmlNode node in nodes)
                {
                     yield return new KxAcademicYearSession(null, node);
                }
            }
        }
    }

    public class KxAcademicYearCoreSession : KxAcademicYearSession
    {
        public KxAcademicYearCoreSession(KxAcademicYear year)
            : base(null)
        {
            if (year != null)
            {
                _academicYearId = year.AcademicYearId;
                _startDate = year.StartDate.Value;
                _endDate = year.EndDate.Value;
                _webPublished = year.WebPublished;
                _description = year.CoreSessionDescription;
            }
            else
            {
                _academicYearId = 0;
                _startDate = DateTime.MinValue;
                _endDate = DateTime.MinValue;
                _webPublished = false;
                _description = "ERROR";
            }
            _allowMultipleApplications = false;
            dBIdentifier = 0;
        }

        public override bool Delete()
        {
            return true;
        }

        public override bool Save()
        {
            return true;
        }

        public override bool Load()
        {
            return true;
        }

        public override void LoadFromXml(string xml, XmlNode node)
        {
            // do nothing.
        }
    }
}

