using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Configuration;
using Kx.Core.Data;
using Kx.Core.Encryption;
using Kx.Core.Utility;
using Kx.Core.Exceptions;
using Kx.Objects.Common;
using Kx.Objects.Student.DataProviders;
using Kx.Objects.Student.Web;
using Kx.Core.Merge;
namespace Kx.Objects.Student
{
    [Serializable]
    public class KxStudentAllocation : KxBaseClass, IMergeObject
    {
        protected KxWebStudentProfileFlatmates _flatmates = null;

        protected int      applicationId;
        protected int      blockId;
        protected int      residentialRoomId;
        protected int      moveReasonId;
        protected int      creationUserId;
        protected DateTime ?creationDate;
        protected DateTime ?offerDate;
        protected int      offerUserId;
        protected int      offerCorrespondanceId;
        protected DateTime? acceptedDate = null;
        protected int      acceptedUserId;
        protected DateTime? rejectedDate = null;
        protected int      rejectedUserId;
        protected int      rejectedReasonId;
        protected int      licenceCorrespondanceId;
        protected int      roomPricingId;
        protected int      movedToAllocationId;
        protected DateTime ?waitingListDate;
        protected int      waitingListReasonId;
        protected string   waitingListNotes;
        protected bool     chargesCreated;
        protected int      waitingListUserId;
        protected string   offerNotes;
        protected DateTime ?offerExpireDate;
        protected int      massBatchId;
        protected int      licenceTemplateId;
        protected int      offerTemplateId;
        protected int      pricingModelId;
        protected int      allocationModelId;
        protected int  invoiceNumber = 0;

        protected KxStudentAllocationDates allocationDates = null;
        protected KxRoomPricing standardRoomPricing = null;
        protected KxAllocationModel allocationModel = null;
        protected KxPricingModel pricingModel = null;
        protected KxStudentTermDates termDates = null;

        protected KxStudentReason rejectedReason = null;
        
        protected KxResidentialRoom residentialRoom = null;
        protected KxResidentialRoom residentialRoomAY = null;
        protected KxStudentApplication application = null;
        protected KxStudentCorrespondence offerCorrespondence = null;
        protected KxStudentCorrespondence licenceCorrespondence = null;
        protected KxStudentInvoice invoice = null;
        protected KxWebAllocationCharges allocationCharges = null;
        protected KxWebAllocationCharges allocationAccommodationCharges = null;
        protected KxWebAllocationCharges allocationAncillaryCharges = null;

        protected KxSiteBlock block = null;

        protected KxeInductionArrivalLocation inductionArrivalLocation = null;

        protected Decimal allocationFees = 0;
        protected Decimal accommodationCharges = 0;
        protected Decimal ancillaryCharges = 0;

        public int      AllocationId
        {
            get
            { return base.dBIdentifier;}
        }

        public int      ApplicationId
        {
            get
            { return applicationId;}
            set
            { 
                AddToAudit("ApplicationId", applicationId, value);
                applicationId = value;
            }
        }

        public KxStudentApplication Application
        {
            get
            {
                if (ApplicationId > 0 && application == null)
                {
                    application = new KxStudentApplication(this, ApplicationId);
                }
                return application;
            }
        }

        public string Accommodation
        {
            get
            {
                return GetAccommodation();
            }
        }

        public string BlockUrl
        {
            get
            {
                try
                {
                    return ResidentialRoom.Block.BlockUrl;
                }
                catch
                {
                    return "";
                }
            }
        }
        
        public string RoomName
        {
            get
            {
                return GetRoomName();
            }
        }

        public string BlockName
        {
            get
            {
                return GetBlockName();
            }
        }


        public string AreaName
        {
            get
            {
                return ResidentialRoom.Block.AreaName;
            }
        }

        public int      BlockId
        {
            get
            { return blockId;}
            set
            { 
                AddToAudit("BlockId", blockId, value);
                blockId = value;
            }
        }

        public int      ResidentialRoomId
        {
            get
            { return residentialRoomId;}
            set
            { 
                AddToAudit("ResidentialRoomId", residentialRoomId, value);
                residentialRoomId = value;
            }
        }

        public int      MoveReasonId
        {
            get
            { return moveReasonId;}
            set
            { 
                AddToAudit("MoveReasonId", moveReasonId, value);
                moveReasonId = value;
            }
        }

        public int      CreationUserId
        {
            get
            { return creationUserId;}
            set
            { 
                AddToAudit("CreationUserId", creationUserId, value);
                creationUserId = value;
            }
        }

        public DateTime ?CreationDate
        {
            get
            { return creationDate;}
            set
            { 
                AddToAudit("CreationDate", creationDate, value);
                creationDate = value;
            }
        }

        public DateTime ?OfferDate
        {
            get
            { return offerDate;}
            set
            { 
                AddToAudit("OfferDate", offerDate, value);
                offerDate = value;
            }
        }

        public int      OfferUserId
        {
            get
            { return offerUserId;}
            set
            { 
                AddToAudit("OfferUserId", offerUserId, value);
                offerUserId = value;
            }
        }

        public int      OfferCorrespondanceId
        {
            get
            { return offerCorrespondanceId;}
            set
            { 
                AddToAudit("OfferCorrespondanceId", offerCorrespondanceId, value);
                offerCorrespondanceId = value;
            }
        }

        public DateTime ?AcceptedDate
        {
            get
            { return acceptedDate;}
            set
            { 
                AddToAudit("AcceptedDate", acceptedDate, value);
                acceptedDate = value;
            }
        }

        public int      AcceptedUserId
        {
            get
            { return acceptedUserId;}
            set
            { 
                AddToAudit("AcceptedUserId", acceptedUserId, value);
                acceptedUserId = value;
            }
        }

        public DateTime ?RejectedDate
        {
            get
            { return rejectedDate;}
            set
            { 
                AddToAudit("RejectedDate", rejectedDate, value);
                rejectedDate = value;
            }
        }

        public int      RejectedUserId
        {
            get
            { return rejectedUserId;}
            set
            { 
                AddToAudit("RejectedUserId", rejectedUserId, value);
                rejectedUserId = value;
            }
        }

        public int      RejectedReasonId
        {
            get
            { return rejectedReasonId;}
            set
            { 
                AddToAudit("RejectedReasonId", rejectedReasonId, value);
                rejectedReasonId = value;
            }
        }

        public KxStudentReason RejectedReason
        {
            get
            {
                if (rejectedReason == null && rejectedReasonId > 0)
                {
                    rejectedReason = new KxStudentReason(this, rejectedReasonId);
                }
                return rejectedReason;
            }
        }

        public int      LicenceCorrespondanceId
        {
            get
            { return licenceCorrespondanceId;}
            set
            { 
                AddToAudit("LicenceCorrespondanceId", licenceCorrespondanceId, value);
                licenceCorrespondanceId = value;
            }
        }

        public int      RoomPricingId
        {
            get
            { return roomPricingId;}
            set
            { 
                AddToAudit("RoomPricingId", roomPricingId, value);
                roomPricingId = value;
            }
        }

        public int      MovedToAllocationId
        {
            get
            { return movedToAllocationId;}
            set
            { 
                AddToAudit("MovedToAllocationId", movedToAllocationId, value);
                movedToAllocationId = value;
            }
        }

        public DateTime ?WaitingListDate
        {
            get
            { return waitingListDate;}
            set
            { 
                AddToAudit("WaitingListDate", waitingListDate, value);
                waitingListDate = value;
            }
        }

        public int      WaitingListReasonId
        {
            get
            { return waitingListReasonId;}
            set
            { 
                AddToAudit("WaitingListReasonId", waitingListReasonId, value);
                waitingListReasonId = value;
            }
        }

        public string   WaitingListNotes
        {
            get
            { return waitingListNotes;}
            set
            { 
                AddToAudit("WaitingListNotes", waitingListNotes, value);
                waitingListNotes = value;
            }
        }

        public bool     ChargesCreated
        {
            get
            { return chargesCreated;}
            set
            { 
                AddToAudit("ChargesCreated", chargesCreated, value);
                chargesCreated = value;
            }
        }

        public int      WaitingListUserId
        {
            get
            { return waitingListUserId;}
            set
            { 
                AddToAudit("WaitingListUserId", waitingListUserId, value);
                waitingListUserId = value;
            }
        }

        public string   OfferNotes
        {
            get
            { return offerNotes;}
            set
            { 
                AddToAudit("OfferNotes", offerNotes, value);
                offerNotes = value;
            }
        }

        public int DaysToExpiry
        {
            get
            {
                return GetDaysToExpiry();
            }
        }

        public DateTime ?OfferExpireDate
        {
            get
            { return offerExpireDate;}
            set
            { 
                AddToAudit("OfferExpireDate", offerExpireDate, value);
                offerExpireDate = value;
            }
        }

        public int      MassBatchId
        {
            get
            { return massBatchId;}
            set
            { 
                AddToAudit("MassBatchId", massBatchId, value);
                massBatchId = value;
            }
        }

        public int      LicenceTemplateId
        {
            get
            { return licenceTemplateId;}
            set
            { 
                AddToAudit("LicenceTemplateId", licenceTemplateId, value);
                licenceTemplateId = value;
            }
        }

        private KxWordTemplate _licenceTemplate;
        public string LicenceTemplateDescription
        {
            get
            {
                if (LicenceTemplateId > 0 && _licenceTemplate == null)
                {
                    _licenceTemplate = new KxWordTemplate(LicenceTemplateId);
                }
                if (_licenceTemplate != null)
                {
                    return _licenceTemplate.Description;
                }

                return string.Empty;
            }
        }
        public string LicenceTemplateComments
        {
            get
            {
                if (LicenceTemplateId > 0 && _licenceTemplate == null)
                {
                    _licenceTemplate = new KxWordTemplate(LicenceTemplateId);
                }
                if (_licenceTemplate != null)
                {
                    return _licenceTemplate.Comments;
                }

                return string.Empty;
            }
        }

        public int      OfferTemplateId
        {
            get
            { return offerTemplateId;}
            set
            { 
                AddToAudit("OfferTemplateId", offerTemplateId, value);
                offerTemplateId = value;
            }
        }

        private KxWordTemplate _offerTemplate;
        public string OfferTemplateDescription
        {
            get
            {
                if (offerTemplateId > 0 && _offerTemplate == null)
                {
                    _offerTemplate = new KxWordTemplate(offerTemplateId);
                }
                if (_offerTemplate != null)
                {
                    return _offerTemplate.Description;
                }

                return string.Empty;
            }
        }

        public int PricingModelId
        {
            get
            { return pricingModelId;}
            set
            { 
                AddToAudit("PricingModelId", pricingModelId, value);
                pricingModelId = value;
            }
        }

        public int      AllocationModelId
        {
            get
            { return allocationModelId;}
            set
            { 
                AddToAudit("AllocationModelId", allocationModelId, value);
                allocationModelId = value;
            }
        }

        public string AllocationPeriod
        {
            get
            { return KxDateTime.ToString(AllocationStartDate,"d") + " - " + KxDateTime.ToString(AllocationEndDate,"d"); }
        }

        public string AllocationPeriodLong
        {
            get
            { return KxDateTime.ToString(AllocationStartDate,"D") + " to " + KxDateTime.ToString(AllocationEndDate,"D"); }
        }

        public DateTime AllocationStartDate
        {
            get
            { return AllocationDates.GetAllocationStartDate(); }
        }

        public DateTime AllocationEndDate
        {
            get
            { return AllocationDates.GetAllocationEndDate(); }
        }

        public int[] AllocationDateIds
        {
            get
            {
                return GetAllocationDateIds();
            }
        }

        public int[] TermDateIndexes
        {
            get
            {
                return GetTermDates();
            }
        }

        public KxWebAllocationCharges AllocationCharges
        {
            get
            {
                if (allocationCharges == null)
                {
                    allocationCharges = new KxWebAllocationCharges(this, AllocationChargeType.All);
                }
                return allocationCharges;
            }
            set
            {
                allocationCharges = value;
            }
        }

        public KxWebAllocationCharges AllocationAccommodationCharges
        {
            get
            {
                if (allocationAccommodationCharges == null)
                {
                    allocationAccommodationCharges = new KxWebAllocationCharges(this, AllocationChargeType.Accommodation);
                }
                return allocationAccommodationCharges;
            }
            set
            {
                allocationAccommodationCharges = value;
            }
        }

        public KxWebAllocationCharges AllocationAncillaryCharges
        {
            get
            {
                if (allocationAncillaryCharges == null)
                {
                    allocationAncillaryCharges = new KxWebAllocationCharges(this, AllocationChargeType.Ancillary);
                }
                return allocationAncillaryCharges;
            }
            set
            {
                allocationAncillaryCharges = value;
            }
        }

        public KxStudentCorrespondence OfferCorrespondence
        {
            get
            {
                if (offerCorrespondanceId > 0 & offerCorrespondence == null)
                {
                    offerCorrespondence = new KxStudentCorrespondence(this,offerCorrespondanceId); 
                }
                return offerCorrespondence;
            }
        }

        public KxStudentCorrespondence LicenceCorrespondence
        {
            get
            {
                if (licenceCorrespondanceId > 0 & licenceCorrespondence == null)
                {
                    licenceCorrespondence = new KxStudentCorrespondence(this,licenceCorrespondanceId); 
                }
                return licenceCorrespondence;
            }

        }
  
        public KxStudentAllocationDates AllocationDates
        {
            get
            {
                if (allocationDates == null)
                {
                    allocationDates = new KxStudentAllocationDates(this,AllocationId);
                }
                return allocationDates;
            }
            set
            { allocationDates = value; }
        }

        public KxRoomPricing StandardRoomPricing
        {
            get
            {
                if (standardRoomPricing == null && roomPricingId != 0)
                {
                    standardRoomPricing = new KxRoomPricing(this, roomPricingId);
                }
                return standardRoomPricing;
            }
            set
            { standardRoomPricing = value; }
        }

        public KxAllocationModel AllocationModel
        {
            get
            {
                if (allocationModel == null && allocationModelId != 0)
                {
                    allocationModel = new KxAllocationModel(this, allocationModelId, Application.AcademicYearId);
                }
                return allocationModel;
            }
            set
            { allocationModel = value; }
        }

        public KxPricingModel PricingModel
        {
            get
            {
                if (pricingModel == null && pricingModelId != 0)
                {
                    pricingModel = new KxPricingModel(this, pricingModelId);
                }
                return pricingModel;
            }
            set
            { pricingModel = value; }
        }

        public KxStudentTermDates TermDates
        {
            get
            {
                if (termDates == null)
                {
                    termDates = new KxStudentTermDates(this, AllocationId);
                }
                return termDates;
            }
        }

        public KxResidentialRoom ResidentialRoom
        {
            get
            {
                if (residentialRoom == null && residentialRoomId > 0)
                {
                    residentialRoom = new KxResidentialRoom(this, residentialRoomId);
                }
                return residentialRoom;
            }
            set
            {
                if (residentialRoom == value)
                    return;
                residentialRoom = value;
            }
        }

        public KxResidentialRoom AcademicYearResidentialRoom
        {
            get
            {
                if (residentialRoomAY == null && residentialRoomId > 0)
                {
                    residentialRoomAY = new KxResidentialRoom(this, residentialRoomId, Application.AcademicYearId);
                }
                return residentialRoomAY;
            }
            set
            {
                if (residentialRoomAY == value)
                    return;
                residentialRoomAY = value;
            }
        }

        public Decimal AllocationFees
        {
            get
            {
                if (allocationFees == 0)
                {
                    allocationFees = GetAllocationFees();
                }
                return allocationFees;
            }
        }

        public Decimal AllocationFeesPlus200
        {
            get
            {
               return AllocationFees+200.00M;
            }
        }

        public Decimal AllocationFeesPlus300
        {
            get
            {
                return AllocationFees + 300.00M;
            }
        }

        public Decimal AccommodationCharges
        {
            get
            {
                if (accommodationCharges == 0)
                {
                    accommodationCharges = GetFees(true);
                }
                return accommodationCharges;
            }
        }


        public Decimal AncillaryCharges
        {
            get
            {
                if (ancillaryCharges == 0)
                {
                    ancillaryCharges = GetFees(false);
                }
                return ancillaryCharges;
            }
        }

        public Decimal TotalCharges
        {
            get
            {
                return AncillaryCharges + AccommodationCharges;
            }
        }



        public Decimal DiscountedAllocationFee2PC
        {
            get
            {
                return (AllocationFees/100) * 98;
            }
        }

        public bool OfferHasExpired
        {
            get
            { return GetOfferHasExpired(); }
        }

        public bool IsCurrent
        {
            get
            {
                return GetIsCurrent();
            }
        }

        public string SalfordStudyCode
        {
            get
            { return GetSalfordStudyCode();}
        }

        public int InvoiceNumber
        {
            get
            { return InvoiceNumber; }
            set
            {
                if (invoiceNumber != value && value != 0)
                {
                    invoiceNumber = value;
                    invoice = new KxStudentInvoice(this, invoiceNumber);
                }
            }
        }

        public KxStudentInvoice Invoice
        {
            get
            { return invoice; }
        }

        public string OfferDownloadUrl
        {
            get
            {
                return "CorrespondenceDownload.aspx?eurl=" + Rijndael.EncryptString("correspondenceId=" + OfferCorrespondanceId.ToString(), "p455w0rd");
            }
        }

        public string LicenceDownloadUrl
        {
            get
            {
                return "CorrespondenceDownload.aspx?eurl="+ Rijndael.EncryptString("correspondenceId="+LicenceCorrespondanceId.ToString(), "p455w0rd");
            }
        }

        public bool IsCheckedIn
        {
            get
            {
                return AllocationDates.GetCheckedIn();
            }
        }

        public bool HasOfferDocument
        {
            get
            {
                return OfferCorrespondanceId > 0;
            }
        }

        public bool ImplicitlyAccepted
        {
            get
            {
                return IsCheckedIn || (AcceptedDate != null && AcceptedDate > KxDateTime.KxApplicableDate);
            }
        }

        public bool HasLicenceDocument
        {
            get
            {
                return LicenceCorrespondanceId > 0;
            }
        }

        public KxeInductionArrivalLocation InductionArrivalLocation
        {
            get
            {
                if (inductionArrivalLocation == null)
                {
                    int locationId = KxStudentObjectManager.InductionArrivalLocations.GetAllocationLocationId(this);
                    if (locationId > 0)
                    {
                        inductionArrivalLocation = new KxeInductionArrivalLocation(this, locationId);
                    }
                }
                return inductionArrivalLocation;
            }
            set
            {
                inductionArrivalLocation = value;
            }
        }

        public bool HasOfferTemplateUrl
        {
            get
            {
                return !String.IsNullOrEmpty(OfferTemplateUrl);
            }
        }

        public string OfferTemplateUrl
        {
            get
            {
                return GetOfferTemplateUrl();
            }
        }

        private KxMergeTagProviderFactory _mergeTagProviderFactory;

        private KxMergeTagProviderFactory MergeTagProviderFactory
        {
            get
            {
                if (_mergeTagProviderFactory == null)
                {
                    _mergeTagProviderFactory = new KxMergeTagProviderFactory();
                }

                return _mergeTagProviderFactory;
            }
        }

        public KxMergeTagProvider MergeTags
        {
            get
            {
                return MergeTagProviderFactory.GetMergeTagProvider(KxMergeTagSetName.GetST2StudentAllocations, 0,
                    this.AllocationId);
            }
        }

        public void SnapshotMergeTags(bool reCreateExisting)
        {
            MergeTagProviderFactory.SnapshotMergeTags(KxMergeTagSetName.GetST2StudentAllocations, 0, this.AllocationId, reCreateExisting);
        }

        public int Weeks
        {
            get
            {
                return GetWeeks();
            }
        }


        public KxWebStudentProfileFlatmates Flatmates
        {
            get
            {
                if (_flatmates == null)
                {
                    _flatmates = new KxWebStudentProfileFlatmates(this.AllocationId, this.Application.KxStudentId);
                }
                return _flatmates;
            }
        }

        public string[] FlatmateIndexes
        {
            get { return Flatmates.FlatmateIndexes; }
        }


        public KxStudentAllocation(KxBaseClass parent) : base(parent)
        {

        }

        public KxStudentAllocation(KxBaseClass parent, int allocationid)
            : this(parent)
        {
             base.dBIdentifier = allocationid;
             Load();
        }

        public KxStudentAllocation(KxBaseClass parent, XmlNode node)
            : base(parent)
        {
            LoadFromXml("",node);
        }
        
        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("st2studentallocation", "ST2StudentAllocations", TargetType.Table);

            result.AutoKeyField = new KxDataValue("AllocationId",AllocationId,KxValueType.Int);


            if ((actiontype == ActionType.Update) && (AllocationId == 0))
            {
                result.Action = ActionType.Insert;
            }
            else if (actiontype == ActionType.Update)
            {
                result.Target = TargetType.StoredProc;
                result.Action = actiontype;
            }
            else
            {
                result.Action = actiontype;
            }

            if (result.Action == ActionType.Update)

            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {
                   result.AddParameter("applicationId", applicationId, KxValueType.Int);
                   result.AddParameter("blockId", blockId, KxValueType.Int);
                   result.AddParameter("residentialRoomId", residentialRoomId, KxValueType.Int);
                   result.AddParameter("moveReasonId", moveReasonId, KxValueType.Int);
                   result.AddParameter("creationUserId", creationUserId, KxValueType.Int);
                   result.AddParameter("creationDate", creationDate, KxValueType.DateTime);
                   result.AddParameter("offerDate", offerDate, KxValueType.DateTime);
                   result.AddParameter("offerUserId", offerUserId, KxValueType.Int);
                   result.AddParameter("offerCorrespondanceId", offerCorrespondanceId, KxValueType.Int);
                   result.AddParameter("acceptedDate", acceptedDate, KxValueType.DateTime);
                   result.AddParameter("acceptedUserId", acceptedUserId, KxValueType.Int);
                   result.AddParameter("rejectedDate", rejectedDate, KxValueType.DateTime);
                   result.AddParameter("rejectedUserId", rejectedUserId, KxValueType.Int);
                   result.AddParameter("rejectedReasonId", rejectedReasonId, KxValueType.Int);
                   result.AddParameter("licenceCorrespondanceId", licenceCorrespondanceId, KxValueType.Int);
                   result.AddParameter("roomPricingId", roomPricingId, KxValueType.Int);
                   result.AddParameter("movedToAllocationId", movedToAllocationId, KxValueType.Int);
                   result.AddParameter("waitingListDate", waitingListDate, KxValueType.DateTime);
                   result.AddParameter("waitingListReasonId", waitingListReasonId, KxValueType.Int);
                   result.AddParameter("waitingListNotes", waitingListNotes, KxValueType.String);
                   result.AddParameter("chargesCreated", chargesCreated, KxValueType.Boolean);
                   result.AddParameter("waitingListUserId", waitingListUserId, KxValueType.Int);
                   result.AddParameter("offerNotes", offerNotes, KxValueType.String);
                   result.AddParameter("offerExpireDate", offerExpireDate, KxValueType.DateTime);
                   result.AddParameter("massBatchId", massBatchId, KxValueType.Int);
                   result.AddParameter("licenceTemplateId", licenceTemplateId, KxValueType.Int);
                   result.AddParameter("offerTemplateId", offerTemplateId, KxValueType.Int);
                   result.AddParameter("pricingModelId", pricingModelId, KxValueType.Int);
                   result.AddParameter("allocationModelId", allocationModelId, KxValueType.Int);
            }
            return result;
        }


        public override void LoadFromXml(string xml, XmlNode node)
        {
            Children.Remove(AllocationDates);
            allocationDates = null;
            dBIdentifier = KxXmlUtils.GetNodeValue(node, "allocationid", 0);
            applicationId = KxXmlUtils.GetNodeValue(node, "applicationid", 0);
            blockId = KxXmlUtils.GetNodeValue(node, "blockid", 0);
            residentialRoomId = KxXmlUtils.GetNodeValue(node, "residentialroomid", 0);
            moveReasonId = KxXmlUtils.GetNodeValue(node, "movereasonid", 0);
            creationUserId = KxXmlUtils.GetNodeValue(node, "creationuserid", 0);
            creationDate = KxXmlUtils.GetNodeValueDT(node, "creationdate", null);
            offerDate = KxXmlUtils.GetNodeValueDT(node, "offerdate", null);
            offerUserId = KxXmlUtils.GetNodeValue(node, "offeruserid", 0);
            offerCorrespondanceId = KxXmlUtils.GetNodeValue(node, "offercorrespondanceid", 0);
            acceptedDate = KxXmlUtils.GetNodeValueDT(node, "accepteddate", null);
            acceptedUserId = KxXmlUtils.GetNodeValue(node, "accepteduserid", 0);
            rejectedDate = KxXmlUtils.GetNodeValueDT(node, "rejecteddate", null);
            rejectedUserId = KxXmlUtils.GetNodeValue(node, "rejecteduserid", 0);
            rejectedReasonId = KxXmlUtils.GetNodeValue(node, "rejectedreasonid", 0);
            licenceCorrespondanceId = KxXmlUtils.GetNodeValue(node, "licencecorrespondanceid", 0);
            roomPricingId = KxXmlUtils.GetNodeValue(node, "roompricingid", 0);
            movedToAllocationId = KxXmlUtils.GetNodeValue(node, "movedtoallocationid", 0);
            waitingListDate = KxXmlUtils.GetNodeValueDT(node, "waitinglistdate", null);
            waitingListReasonId = KxXmlUtils.GetNodeValue(node, "waitinglistreasonid", 0);
            waitingListNotes = KxXmlUtils.GetNodeValue(node, "waitinglistnotes", "");
            chargesCreated = KxXmlUtils.GetNodeValue(node, "chargescreated", false);
            waitingListUserId = KxXmlUtils.GetNodeValue(node, "waitinglistuserid", 0);
            offerNotes = KxXmlUtils.GetNodeValue(node, "offernotes", "");
            offerExpireDate = KxXmlUtils.GetNodeValueDT(node, "offerexpiredate", null);
            massBatchId = KxXmlUtils.GetNodeValue(node, "massbatchid", 0);
            licenceTemplateId = KxXmlUtils.GetNodeValue(node, "licencetemplateid", 0);
            offerTemplateId = KxXmlUtils.GetNodeValue(node, "offertemplateid", 0);
            pricingModelId = KxXmlUtils.GetNodeValue(node, "pricingmodelid", 0);
            allocationModelId = KxXmlUtils.GetNodeValue(node, "allocationmodelid", 0);
        }

        public Decimal GetAllocationFees()
        {
            Decimal result = 0;

            KxData data = new KxData("", "WebStudentAllocationCharges", TargetType.StoredProc);

            data.AddParameter("allocationId", AllocationId, KxValueType.Int);

            object value = data.ExecuteScalar();

            if (value != null)
            {
                try
                {
                    result = Convert.ToDecimal(value);
                }
                catch
                { }
            }

            return result;
        }

        private string GetAccommodation()
        {
            string result = "";

            if (ResidentialRoom != null)
            {
                result += " " + ResidentialRoom.Name;

                if (ResidentialRoom.Block != null)
                {
                    result += ", " + ResidentialRoom.Block.Name;
                }
            } 
            return result.Trim(',',' ');
        }

        private string GetRoomName()
        {
            string result = "";

            if (ResidentialRoom != null)
            {
                result = ResidentialRoom.Name;
            }
            return result;
        }

        private string GetBlockName()
        {
            string result = "";

            if (ResidentialRoom != null)
            {

                if (ResidentialRoom.Block != null)
                {
                    result = ResidentialRoom.Block.Name;
                }
            }
            return result;
           
        }

        public KxStudentAllocationDate GetAllocationDateById(int Id)
        {
            KxStudentAllocationDate result = null;

            foreach (KxStudentAllocationDate ad in AllocationDates.Items)
            {
                if (ad.AllocationDateId == Id)
                {
                    result = ad;
                    break;
                }
            }
            return result;
        }

       
        private int[] GetAllocationDateIds()
        {
            int[] result = new int[AllocationDates.Items.Count];

            for (int i = 0; i < AllocationDates.Items.Count; i++)
            {
                result[i] = AllocationDates.Items[i].AllocationDateId;
            }
            return result;
        }

        private int[] GetTermDates()
        {
            int[] result = new int[TermDates.Items.Count];

            for (int i = 0; i < TermDates.Items.Count; i++)
            {
                result[i] = TermDates.Items[i].Index;
            }
            return result;
        }

        private string GetSalfordStudyCode()
        {
            string result = "";

            string Year = AllocationEndDate.Year.ToString().Remove(0,2);

            string YearPlus = AllocationEndDate.AddYears(1).Year.ToString().Remove(0, 2);

            string Months = AllocationStartDate.Month.ToString()+"-"+ AllocationEndDate.Month.ToString();

            switch (Months)
            {
		//110802 Codes Updated PMc/DH
                case "9-6": result = "SJ"  + Year; break;
		case "1-6": result = "FJ"  + Year; break;	
                case "1-1": result = "JJ"  + Year; break;
		case "1-8": result = "JA"  + Year; break; 
                case "9-8": result = "SA"  + Year; break;
                case "9-9": result = "SS"  + Year; break;
                case "3-3": result = "MM"  + Year; break;
		//case "2-6": result = "FA"  + Year; break;                
		//case "1-6": result = "JA"  + Year; break; 
                //case "9-12": result = "SD" + YearPlus; break;
                //case "9-1": result = "JA"  + Year; break;
                //case "9-3": result = "SM"  + Year; break;
                //case "1-8": result = "JS"  + Year; break;
                //case "4-6": result = "AJ"  + Year; break;
                //case "4-9": result = "AS"  + Year; break;
                //case "1-3": result = "JM"  + Year; break;
                //case "3-9": result = "MS"  + Year; break;

                default: result = "??" + Year; break;
            }
            return result;
        }

        private bool GetIsCurrent()
        {
            return AllocationDates.Items.Count > 0;
        }

        public int CreateInvoiceStrValue(int AllocationId, string InvoiceValue,Decimal Default)
        {
            Decimal Value = 0.00M;
            try
            {
                Value = Convert.ToDecimal(InvoiceValue);
            }
            catch
            {
                Value = Default;
            }
            return CreateInvoice(AllocationId, Value);
        }

        public int CreateInvoice(int AllocationId, Decimal InvoiceValue)
        {
            int result = 0;

            KxData data = new KxData("", "WebCreateAdvancePayment", TargetType.StoredProc);

            data.AddParameter("allocationId", AllocationId, KxValueType.Int);
            data.AddParameter("Amount", InvoiceValue, KxValueType.Decimal);

            object value = data.ExecuteScalar();

            if (value != null)
            {
                try
                {
                    result = Convert.ToInt32(value);
                }
                catch
                { }
            }

            InvoiceNumber = result;

            return result;
        }

        private bool GetOfferHasExpired()
        {
            bool result = true;

            try
            {
                result = !KxDbConfiguration.GetConfigurationBool("IgnoreOfferExpiry", false);
            }
            catch{ }
            try
            {
                if (result)
                {
                    result = OfferExpireDate != KxDateTime.KxNullDate && ((DateTime)OfferExpireDate).AddDays(1) < DateTime.Now;
                }
            }
            catch { }

            return result;
        }

        private int GetDaysToExpiry()
        {
            int result = 0;

            try
            {
                TimeSpan timspn = OfferExpireDate.Value.Subtract(DateTime.Today);
                result = timspn.Days;
            }
            catch { }

            return result;
        }

        private Decimal GetFees(bool isAccommodation)
        {
            Decimal result = 0.00m;

            foreach (KxStudentAllocationDate dte in AllocationDates.Items)
            {
                if (isAccommodation)
                {
                    result += dte.AccommodationCharges; 
                }
                else
                {
                    result += dte.AncillaryCharges; 
                }
            }
            return result;
        }

        protected string GetOfferTemplateUrl()
        {
            string result = "";

            if (dBIdentifier != 0)
            {
                try
                {
                    KxCustomSetting setting = new KxCustomSetting(null,"Kx.Web.OfferTemplate.URL", Application.AcademicYearId);
                    result = setting.AllString;
                }
                catch { }
            }
            return result;
        }

        private int GetWeeks()
        {
            int result = 0;

            try
            {
                TimeSpan timspn = AllocationDates.AllocationEndDate.Subtract(AllocationDates.AllocationStartDate);
                result = Convert.ToInt32(Math.Floor(timspn.TotalDays / 7));
            }
            catch { }

            return result;
        }

        public void ResetFlatmates()
        {
            _flatmates = null;
        }
    }
}

