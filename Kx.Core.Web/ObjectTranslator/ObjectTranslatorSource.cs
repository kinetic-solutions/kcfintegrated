﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Core.Web.PageRenderer;
using Kx.Core.Web.PageRenderer.Utility;

namespace Kx.Core.Web.ObjectTranslator
{
    public abstract class ObjectTranslatorSource
    {
        public string KxAttribute { get; private set; }
        public string FriendlyName { get; private set; }
        public string Modifier { get; private set; }
        protected KxPageRenderer PageRenderer { get; private set; }
        protected string AlternateKxAttributes { get; set; }

        public string FullFriendlyName
        {
            get { return string.Format("{0} ({1})", KxAttribute, FriendlyName); }
        }

        public ObjectTranslatorSource(KxPageRenderer pageRenderer, string kxAttribute, string friendlyName, string modifier)
        {
            PageRenderer = pageRenderer;
            KxAttribute = kxAttribute;
            FriendlyName = friendlyName;
            Modifier = modifier;
        }

        public abstract object GetTargetObject(KxWebAttribute attributes);

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public abstract Type GetTargetObjectType();

        public bool IsKxAttributeValue(string kxAttribute)
        {
            List<string> KxAttributeValues = new List<string>();
            KxAttributeValues.Add(KxAttribute.ToUpper());
            if (!string.IsNullOrWhiteSpace(AlternateKxAttributes))
            {
                string[] split = AlternateKxAttributes.Split(new[] {"|"}, StringSplitOptions.RemoveEmptyEntries);
                foreach (string s in split)
                {
                    KxAttributeValues.Add(s.ToUpper());
                }
            }
            return KxAttributeValues.Contains(kxAttribute.ToUpper());
        }
    }
}
