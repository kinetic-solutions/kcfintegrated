using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using Kx.Core.Exceptions;
using Kx.Core.Localisation;
using Kx.Core.Reflection;
using Kx.Core.Utility;
using Kx.Core.Web.BaseClass;
using Kx.Core.Web.Configuration;
using Kx.Core.Web.PageRenderer;
using Kx.Core.Web.PageRenderer.Utility;
using Kx.Core.Web.WebApplication;
using Kx.Objects.Common;
using Kx.Objects.Common.Web;

namespace Kx.Core.Web.ObjectTranslator
{
    [Serializable]
    public class KxObjectTranslator
    {
        #region Atrributes & Properties

        protected KxPageRenderer pageRenderer;

        public int GenericControlIndex;
        public int GenericControlIndex2;

        public KxPageRenderer PageRenderer
        {
            get
            {
                return pageRenderer;
            }
            set
            {
                if (pageRenderer == value)
                    return;
                pageRenderer = value;
            }
        }
        #endregion

        public void ObjectMapping(KxWebAttribute attributes)
        {
            MappingOverride(attributes);

            attributes.PrimaryObject = ClassNameToObject(attributes.PrimaryClass, attributes);

            if (!String.IsNullOrEmpty(attributes.SecondaryClass))
            {
                attributes.SecondaryObject = ClassNameToObject(attributes.SecondaryClass, attributes);
            }

            if (!String.IsNullOrEmpty(attributes.TertiaryClass))
            {
                attributes.TertiaryObject = ClassNameToObject(attributes.TertiaryClass, attributes);
            }
        }

        public virtual string GetMergeData(string Name, out string objectName)
        {
            string temp = Name.Replace("{", "").Replace("}", "");

            // pha
            KxWebAttribute kwa = new KxWebAttribute(temp);
            string data = GetMergeData(kwa);
            objectName = "";
            try
            {
                objectName = kwa.PrimaryObject.ToString();
            }
            catch { }
            return data;
        }

        public virtual string GetMergeData(KxWebAttribute attributes)
        {
            string StrVal = "";

            object val = GetMergeDataObject(attributes);

            val = CheckAddValue(val, attributes);

            StrVal = KxPageUtility.FormatObject(val, attributes.FormatStr, attributes.CultureFormat);

            if (attributes.Capitalise > 0)
            {
                StrVal = KxStringUtils.Capitalise(StrVal, attributes.Capitalise);
            }

            if (attributes.ShowNoData && String.IsNullOrEmpty(StrVal))
            {
                if (!String.IsNullOrEmpty(attributes.ShowNoDataValue))
                {
                    StrVal = KxLocalisationManager.GetSubstitution(attributes.ShowNoDataValue);
                }
                else
                {
                    StrVal = KxLocalisationManager.GetSubstitution("N/A");
                }
            }

            return StrVal;
        }

        public object CheckAddValue(object source, KxWebAttribute attributes)
        {
            object result = source;
            try
            {
                if (source != null && attributes.AddValue > 0.00M)
                {
                    if (source is Decimal)
                    {
                        result = ((Decimal)source) + attributes.AddValue;
                    }
                    else if (source is Int32)
                    {
                        result = ((Int32)source) + Convert.ToInt32(attributes.AddValue);
                    }
                    else if (source is Int64)
                    {
                        result = ((Int64)source) + Convert.ToInt64(attributes.AddValue);
                    }
                }
            }
            catch { }

            return result;
        }

        public virtual object GetMergeDataObject(KxWebAttribute attributes)
        {
            ObjectMapping(attributes);

            object result = KxReflection.GetPropertyObject(attributes.PrimaryObject, attributes.PropertyName);

            if (attributes.OverrideType != null && result != null)
            {
                try
                {
                    result = Convert.ChangeType(result, attributes.OverrideType);
                }
                catch
                { }
            }

            result = CheckAddValue(result, attributes);


            if (result != null && attributes.FormatStr != "")
            {
                result = KxPageUtility.FormatObject(result, attributes.FormatStr, attributes.CultureFormat);
            }

            if ((result == null || result.ToString() == "") && attributes.ShowNoData)
            {
                result = "N/A";
            }

            return result;
        }

        public virtual object GetMergeDataObject(KxWebAttribute attributes, string alternateProperty)
        {
            ObjectMapping(attributes);

            object result = KxReflection.GetPropertyObject(attributes.PrimaryObject, alternateProperty);

            if (attributes.OverrideType != null && result != null)
            {
                try
                {
                    result = Convert.ChangeType(result, attributes.OverrideType);
                }
                catch
                { }
            }

            result = CheckAddValue(result, attributes);


            if (result != null && attributes.FormatStr != "")
            {
                result = KxPageUtility.FormatObject(result, attributes.FormatStr, attributes.CultureFormat);
            }

            if ((result == null || result.ToString() == "") && attributes.ShowNoData)
            {
                result = "N/A";
            }

            return result;
        }

        public virtual object GetMergeDataObjectSecondary(KxWebAttribute attributes, string alternateProperty)
        {
            ObjectMapping(attributes);

            object result = KxReflection.GetPropertyObject(attributes.SecondaryObject, alternateProperty);

            if (attributes.OverrideType != null && result != null)
            {
                try
                {
                    result = Convert.ChangeType(result, attributes.OverrideType);
                }
                catch
                { }
            }

            result = CheckAddValue(result, attributes);


            if (result != null && attributes.FormatStr != "")
            {
                result = KxPageUtility.FormatObject(result, attributes.FormatStr, attributes.CultureFormat);
            }

            if ((result == null || result.ToString() == "") && attributes.ShowNoData)
            {
                result = "N/A";
            }

            return result;
        }

        public virtual object GetMergeDataObjectTertiary(KxWebAttribute attributes, string alternateProperty)
        {
            ObjectMapping(attributes);

            object result = KxReflection.GetPropertyObject(attributes.TertiaryObject, alternateProperty);

            if (attributes.OverrideType != null && result != null)
            {
                try
                {
                    result = Convert.ChangeType(result, attributes.OverrideType);
                }
                catch
                { }
            }

            result = CheckAddValue(result, attributes);


            if (result != null && attributes.FormatStr != "")
            {
                result = KxPageUtility.FormatObject(result, attributes.FormatStr, attributes.CultureFormat);
            }

            if ((result == null || result.ToString() == "") && attributes.ShowNoData)
            {
                result = "N/A";
            }

            return result;
        }


        public Type GetMergeDataType(KxWebAttribute attributes)
        {
            ObjectMapping(attributes);

            try
            {
                PropertyInfo result = KxReflection.GetPropertyInfo(attributes.PrimaryObject, attributes.PropertyName);

                if (result != null)
                {
                    return result.PropertyType;
                }
                else
                {
                    return typeof(string);
                }
            }
            catch
            {
                return typeof(string);
            }
        }

        public bool GetMergeReadonlyStateFlag(KxWebAttribute attributes, out string objectname) // pha
        {
            return GetMergeStateFlag(attributes, attributes.ReadOnlyProperty, attributes.ReadOnly, out objectname);
        }

        public bool GetMergeMandatoryStateFlag(KxWebAttribute attributes, out string objectname) // pha
        {
            return GetMergeStateFlag(attributes, attributes.MandatoryProperty, attributes.Mandatory, out objectname);
        }

        public bool GetMergeVisibleStateFlag(KxWebAttribute attributes, out string objectname) // pha
        {
            objectname = "";    // pha
            if (!string.IsNullOrEmpty(attributes.VisibleMethod))
            {
                return GetMergeStateFlagByMethod(attributes, attributes.VisibleMethod, attributes.GenericStringIndex, attributes.Visible);
            }

            return GetMergeStateFlag(attributes, attributes.VisibleProperty, attributes.Visible, out objectname);
        }

        public bool GetMergeStateFlagByMethod(KxWebAttribute attributes, string StateMethod, string ParamValue, bool defaultvalue)
        {
            if (StateMethod != "")
            {
                ObjectMapping(attributes);

                object result = null;

                if (KxReflection.GetMethodInfo(attributes.TertiaryObject, StateMethod) != null)
                {
                    result = KxReflection.InvokeMethod(attributes.TertiaryObject, StateMethod, new object[] { ParamValue });
                }

                if (result == null)
                {
                    if (KxReflection.GetMethodInfo(attributes.SecondaryObject, StateMethod) != null)
                    {
                        result = KxReflection.InvokeMethod(attributes.SecondaryObject, StateMethod, new object[] { ParamValue });
                    }
                }

                if (result == null)
                {
                    if (KxReflection.GetMethodInfo(attributes.PrimaryObject, StateMethod) != null)
                    {
                        result = KxReflection.InvokeMethod(attributes.PrimaryObject, StateMethod, new object[] { ParamValue });
                    }
                }

                if (result == null)
                {
                    return false;
                }

                bool? customMergeFlag = GetCustomMergeStateFlag(attributes, StateMethod, defaultvalue, result); 
                if (customMergeFlag.HasValue)
                {
                    return customMergeFlag.Value;
                }
                else if (attributes.ComparisonWith == "XNB")
                {
                    return !KxWebAttributeComparison.DoComparison(attributes, "", result.ToString());
                }
                else if (attributes.ComparisonWith != "")
                {
                    return KxWebAttributeComparison.DoComparison(attributes, result.ToString(), attributes.ComparisonWith);
                }
                else if (result is bool)
                {
                    return (bool)result;
                }
                else
                {
                    return bool.Parse(result.ToString());
                }
            }
            else
                return defaultvalue;
        }

        public bool GetMergeStateFlag(KxWebAttribute attributes, string StateProperty, bool defaultvalue, out string objectname)    // pha
        {
            objectname = "";
            if (StateProperty != "")
            {
                ObjectMapping(attributes);

                object result = null;

                result = GetObjectPropertyFromAttributes(attributes, StateProperty, out objectname);

                if (result == null)
                {
                    return false;
                }

                bool? customMergeFlag = GetCustomMergeStateFlag(attributes, StateProperty, defaultvalue, result);
                if (customMergeFlag.HasValue)
                {
                    return customMergeFlag.Value;
                }
                else if (attributes.ComparisonWith == "XNB")
                {
                    return !KxWebAttributeComparison.DoComparison(attributes, "", result.ToString());
                }
                else if (attributes.ComparisonWith != "")
                {
                    return KxWebAttributeComparison.DoComparison(attributes, result.ToString(), attributes.ComparisonWith);
                }
                else if (result is bool)
                {
                    return (bool)result;
                }
                else
                {
                    return bool.Parse(result.ToString());
                }
            }
            else
                return defaultvalue;
        }

        private static object GetObjectPropertyFromAttributes(KxWebAttribute attributes, string property, out string objectname)
        {
            objectname = "";    // pha
            object result = null;
            if (KxReflection.GetPropertyInfo(attributes.TertiaryObject, property) != null)
            {
                objectname = attributes.TertiaryObject.ToString();
                result = KxReflection.GetPropertyObject(attributes.TertiaryObject, property);
            }

            if (result == null)
            {
                if (KxReflection.GetPropertyInfo(attributes.SecondaryObject, property) != null)
                {
                    objectname = attributes.SecondaryObject.ToString();
                    result = KxReflection.GetPropertyObject(attributes.SecondaryObject, property);
                }
            }

            if (result == null)
            {
                if (KxReflection.GetPropertyInfo(attributes.PrimaryObject, property) != null)
                {
                    objectname = attributes.PrimaryObject.ToString();
                    result = KxReflection.GetPropertyObject(attributes.PrimaryObject, property);
                }
            }
            return result;
        }

        public string MergeTags(string tag)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(KxLocalisationManager.GetSubstitution(tag));
            while (sb.ToString().IndexOf("{KX_") > -1)
            {
                int idx = sb.ToString().IndexOf("{KX_");
                int endidx = sb.ToString().IndexOf("}", idx);

                string element = sb.ToString().Substring(idx, (endidx - idx) + 1);

                string objectName = "";
                string sub = KxLocalisationManager.GetSubstitution(GetMergeData(element, out objectName));

                // pha
                if (Core.Configuration.KxDbConfiguration.GetConfiguration("DebugMergeTags").ToLower() == "true")
                {
                    string pn = "";
                    using (System.IO.StreamWriter sw = new System.IO.StreamWriter(@"c:\temp\workflow.xml", true))
                    {
                        int i = element.LastIndexOf("_PN");
                        if (i > 0)
                        {
                            pn = element.Substring(i + 3);
                            pn = pn.Substring(0, pn.Length - 1);
                        }
                        else
                        {
                            System.Diagnostics.Debugger.Break();
                        }
                        sw.WriteLine("<MergeTags>");
                        sw.WriteLine("\t<Element>" + element + "</Element>");
                        sw.WriteLine("\t<Object>" + objectName + "</Object>");
                        sw.WriteLine("\t<Property>" + pn + "</Property>");
                        sw.WriteLine("\t<Value>");
                        sw.WriteLine("\t<!--");
                        sw.WriteLine("\t" + sub);
                        sw.WriteLine("\t-->");
                        sw.WriteLine("\t</Value>");
                        sw.WriteLine("</MergeTags>");
                    }
                }

                sb.Replace(element, sub);
            }
            return sb.ToString();
        }

        public KxObjectTranslator(KxPageRenderer _pageRenderer)
        {
            pageRenderer = _pageRenderer;
        }

        public virtual void MappingOverride(KxWebAttribute attributes)
        {
            GenericControlIndex = attributes.GenericIndex;
            GenericControlIndex2 = attributes.GenericIndex2;
        }

        public virtual object ClassNameToObject(string ClassName, KxWebAttribute stdattributes)
        {
            return null;
        }

        public static Type ObjectTypes(string Id)
        {
            switch (Id)
            {

                default: return null;
            }
        }

        public static string Objects()
        {
            StringBuilder sb = new StringBuilder();


            return sb.ToString();
        }

        public virtual KxUserDefinedValue GetUserDefinedValue(int ComparisonId)
        {
            return null;//WebUser.CurrentApplication.UserDefinedValues.GetValueById(attributes.CompareUDF);
        }

        public bool? GetCustomMergeStateFlag(KxWebAttribute stdattributes, string StateProperty, bool defaultvalue, object result)
        {
            string objectname = ""; // pha
            KxWebAttribute attributes = (KxWebAttribute)stdattributes;

            string compareWith = null;

            if (attributes.CompareUDF != -99)
            {
                KxUserDefinedValue comparewith = GetUserDefinedValue(attributes.CompareUDF);

                if (comparewith != null)
                {
                    compareWith = comparewith.FieldValue.ToString();
                }
            }
            else if (!string.IsNullOrWhiteSpace(attributes.CompareWithProperty))
            {
                object compareProperty = GetObjectPropertyFromAttributes(attributes, attributes.CompareWithProperty, out objectname);
                compareWith = (compareProperty ?? string.Empty).ToString();
            }

            if (!string.IsNullOrWhiteSpace(compareWith))
            {
                if (attributes.ComparisonWith != "")
                {
                    return KxWebAttributeComparison.DoComparison(attributes, compareWith, attributes.ComparisonWith);
                }
                else
                {
                    return KxWebAttributeComparison.DoComparison(attributes, compareWith, result.ToString());
                }
            }
            else
            {
                return null;
            }
        }
    }
}
