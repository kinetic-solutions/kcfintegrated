using System;
using System.Collections.Generic;
using Kx.Core.Web.Providers;
using System.Text;
using Kx.Core.Resources;
using Kx.Core.Web.BaseClass;
using Kx.Core.Web.WebApplication;
using Kx.Core.Web.Configuration;
using Kx.Objects.Common;

namespace Kx.Core.Web.Resource
{
 
    [Serializable]
    public class KxResourceManager : WebBaseClass
    {
        protected KxControl controlObject = null;

        public KxControl ControlObject
        {
            get
            {
                if (controlObject == null)
                {
                    controlObject = new KxControl(null);
                }
                return controlObject;
            }
        }

        public KxListProvider ListProvider
        {
            get
            {
                try
                {
                    return (KxListProvider)KxConfigurationManager.GetSystemObject("ListProvider");
                }
                catch
                {
                    return null;
                }
            }
        }

        public KxResourceProvider ResourceProvider
        {
            get
            {
                try
                {
                    return (KxResourceProvider)KxConfigurationManager.GetSystemObject("ResourceProvider");
                }
                catch
                {
                    return null;
                }
            }
        }

        public KxUserDefinedFields UserDefinedFields
        {
            get
            {
                try
                {
                    return (KxUserDefinedFields)KxConfigurationManager.GetSystemObject("UserDefinedFields");
                }
                catch
                {
                    return null;
                }
            }
        }

        public KxResourceManager(KxWebApplication webApp) : base(webApp)
        {
          
        }
    }
}
