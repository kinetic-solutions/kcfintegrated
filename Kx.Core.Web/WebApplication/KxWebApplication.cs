using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using Kx.Core.Exceptions;
using Kx.Core.Web.Configuration;
using Kx.Core.Web.PageRenderer;
using Kx.Core.Web.Resource;
using Kx.Objects.Common;
using Kx.Objects.Common.Web;

namespace Kx.Core.Web.WebApplication
{
    [Serializable]
    public class KxWebApplication
    {
        protected bool inboundPayment = false;
        protected Page currentPage = null;
        protected KxWebUser webUser = null;
        protected KxPageRenderer pageRenderer = null;

        protected KxConfigurationManager configurationManager = null;       
        protected KxResourceManager resourceManager = null;

        public bool InboundPayment
        {
            get
            {
                return inboundPayment;
            }
            set
            {
                inboundPayment = value;
            }
        }

        public Page CurrentPage
        {
            get
            { return currentPage; }
        }

        public KxConfigurationManager ConfigurationManager
        {
            get
            {
                if (configurationManager == null)
                {
                    configurationManager = new KxConfigurationManager();
                }
                return configurationManager;
            }
        }     

        public KxResourceManager ResourceManager
        {
            get
            {
                if (resourceManager == null)
                {
                    resourceManager = new KxResourceManager(this);
                }
                return resourceManager;
            }
        }

  
    }
}
