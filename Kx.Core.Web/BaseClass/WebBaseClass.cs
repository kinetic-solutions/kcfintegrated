using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using Kx.Core.Web.BaseClass;
using Kx.Core.Web.WebApplication;
using Kx.Core.Web.Configuration;
using Kx.Core.Web.PageRenderer;
using Kx.Core.Web.Resource;
using Kx.Objects.Common.Web;


namespace Kx.Core.Web.BaseClass
{
    [Serializable]
    public class WebBaseClass
    {
       
        protected KxWebApplication webApp = null;

 
       public KxWebApplication WebApp
       {
           get
           { return webApp; }
       }

       public KxConfigurationManager ConfigurationManager
       {
           get
           {
               return WebApp.ConfigurationManager;
           }
       }
           

       public KxResourceManager ResourceManager
       {
           get
           {
               return WebApp.ResourceManager;
           }
       }

       public WebBaseClass(KxWebApplication webApp)
       {
           this.webApp = webApp;
       }  
       
    }
}
