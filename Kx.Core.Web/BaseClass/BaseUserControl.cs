﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Kx.Core.Web.BaseClass
{
    public class BaseUserControl : System.Web.UI.UserControl
    {

        public virtual bool IsValid()
        {
            return true;
        }

        public virtual bool Save()
        {
            return true;
        }

        public virtual void Setup()
        {

        }


    }
}
