using System;
using System.Collections.Generic;
using System.Text;

namespace Kx.Core.Web.BaseClass
{
    public interface IAjaxLink
    {

        void AjaxGenericChanged(Object sender, EventArgs e);

    }
}
