using System;
using System.Collections.Generic;
using System.Text;
using Kx.Core;
using Kx.Core.Exceptions;

namespace Kx.Core.Web.WorkflowInterface.WorkflowExtenders
{
    public class KxWpmWorkflowPaymentExtender : KxWorkflowExtender
    {
        #region Properties & Attributes

        private string paymentTypes = "";
        private string depositAmount = "0.00";
        private string paymentAmount = "0.00";
        private string instalmentMethod = "";
        private string paymentDescription = "";
        private string depositDescription = "";

        private bool testMode = false;

        private string instalmentDates = "";
        private string instalmentAmounts = "";

        private string providerUrl = "";
        private string providerCallbackUrl = "";

        private string customfield1Name = "";
        private string customfield1Value = "";

        private string customfield2Name = "";
        private string customfield2Value = "";

        private string customfield3Name = "";
        private string customfield3Value = "";

        private string customfield4Name = "";
        private string customfield4Value = "";

        private string customfield5Name = "";
        private string customfield5Value = "";

        private bool autoPost = false;

        public string PaymentTypes
        {
            get
            { return paymentTypes.ToUpper(); }
            set
            { paymentTypes = value; }
        }
        public string DepositAmount
        {
            get
            { return depositAmount; }
            set
            { depositAmount = value; }
        }
        public string PaymentAmount
        {
            get
            { return paymentAmount; }
            set
            { paymentAmount = value; }
        }
        public string InstalmentMethod
        {
            get
            { return instalmentMethod.ToUpper(); }
            set
            { instalmentMethod = value; }
        }
        public string InstalmentDates
        {
            get
            { return instalmentDates; }
            set
            { instalmentDates = value; }
        }
        public string InstalmentAmounts
        {
            get
            { return instalmentAmounts; }
            set
            { instalmentAmounts = value; }
        }
        public string PaymentDescription
        {
            get
            { return paymentDescription; }
            set
            { paymentDescription = value; }
        }
        public string DepositDescription
        {
            get
            {  return depositDescription; }
            set
            {  depositDescription = value; }
        }

        public bool TestMode
        {
            get
            { return testMode; }
            set
            { testMode = value; }
        }

        public string ProviderUrl
        {
            get
            { return providerUrl; }
            set
            { providerUrl = value; }
        }
       
        public string ProviderCallbackUrl
        {
            get
            { return providerCallbackUrl; }
            set
            { providerCallbackUrl = value; }
        }

        public string Customfield1Name
        {
            get
            { return customfield1Name; }
            set
            { customfield1Name = value; }
        }
        public string Customfield1Value
        {
            get
            { return customfield1Value; }
            set
            { customfield1Value = value; }
        }

        public string Customfield2Name
        {
            get
            { return customfield2Name; }
            set
            { customfield2Name = value; }
        }
        public string Customfield2Value
        {
            get
            {  return customfield2Value; }
            set
            { customfield2Value = value; }
        }

        public string Customfield3Name
        {
            get
            { return customfield3Name; }
            set
            { customfield3Name = value; }
        }

        public string Customfield3Value
        {
            get
            { return customfield3Value; }
            set
            { customfield3Value = value; }
        }
     
        public string Customfield4Name
        {
            get
            { return customfield4Name; }
            set
            { customfield4Name = value; }
        }
        public string Customfield4Value
        {
            get
            { return customfield4Value; }
            set
            { customfield4Value = value; }
        }

        public string Customfield5Name
        {
            get
            { return customfield5Name; }
            set
            { customfield5Name = value; }
        }

        public string Customfield5Value
        {
            get
            { return customfield5Value; }
            set
            {  customfield5Value = value; }
        }

        public bool AutoPost
        {
            get
            { return autoPost; }
            set
            { autoPost = value; }
        }

        #endregion

        public override void AfterLoad()
        {
            if (AutoPost && (WebApp != null && !WebApp.InboundPayment))
            {
                base.AfterLoad();

                Execute();
            }
            else if (AutoPost && PageRenderer.WebApp != null && !PageRenderer.WebApp.InboundPayment)
            {
                base.AfterLoad();

                Execute();
            }
        }

        public override string Execute()
        {
            try
            {
                MakePayment();
                return "";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public override WorkFlowStatus DetermineCompletionState()
        {
            WorkFlowStatus result = base.DetermineCompletionState();

            if (result == WorkFlowStatus.Finished && WorkflowResult == "makepayment")
            {

                if (Execute() != "")
                {
                    result =  WorkFlowStatus.Indeterminate;
                }
            }
            return result;
        }

        public virtual void MakePayment()
        {
           
        }
    }
}
