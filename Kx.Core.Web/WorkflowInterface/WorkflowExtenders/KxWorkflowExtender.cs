using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.UI;
using Kx.Core.Exceptions;
using Kx.Core.Reflection;
using Kx.Core.Utility;
using Kx.Core.Workflow;
using Kx.Core.Web.BaseClass;
using Kx.Core.Web.Configuration;
using Kx.Core.Web.PageRenderer;
using Kx.Core.Web.WebApplication;

namespace Kx.Core.Web.WorkflowInterface.WorkflowExtenders
{
    public enum WorkFlowStatus { Indeterminate, Back, Finished };

    [Serializable]
    public class KxWorkflowExtender : WebBaseClass
    {
        protected KxPageRenderer pageRenderer = null;
        protected string rules = "";
        protected string workflowResult = "";
        
        protected int progress = 0;
        public string PageTitle = "";

        public string Rule
        {
            set
            { rules = value; }
        }

        public string WorkflowResult
        {
            get
            { return workflowResult; }
            set
            { workflowResult = value; }
        }

        public KxPageRenderer PageRenderer
        {
            get
            { return pageRenderer; }
            set
            { pageRenderer = value; }
        }

        public Page CurrentPage
        {
            get
            { return pageRenderer.CurrentPage; }
        }

        public int Progress
        {
            get
            {
                return progress;
            }
            set
            {
                progress = value;
            }
        }

        public KxWorkflowExtender() : base(null)
        {

        }

        public virtual void LoadConfiguration(WorkflowExternalAction externalaction)
        {
            foreach (WorkflowExternalValueSource exsrc in externalaction.Values.Values)
            {
                if (externalaction.IsApplicable(exsrc))
                {
                    KxReflection.SetProperty(this, exsrc.ExtenderPropertyName, exsrc.GetValue());
                }
            }
        }
      

        public virtual string Execute()
        {
            return "";
        }

        public bool ValidateRule()
        {
            bool result = PageRenderer.IsPostbackValid();

            if (result && rules != string.Empty)
            {
                result = PageRenderer.WorkflowInterface.ExecuteRule(rules);
            }

            return result;
        }

        public virtual void BeforeAjax()
        {

        }

        public virtual void AfterAjax()
        {

        }

        public virtual void BeforeLoad()
        {
            try
            {
                if (CurrentPage != null)
                {
                    CurrentPage.Title = PageTitle;
                }
            }
            catch
            {
            }
        }

        public virtual void AfterLoad()
        {
            if (KxConfigurationManager.ShowDebug)
            {
                HttpContext.Current.Response.Write(KxLogger.LatestErrors);
                KxLogger.LatestErrors = "";
            }
        }

        public virtual void AfterRender()
        {

        }

        public virtual WorkFlowStatus DetermineCompletionState()
        {
            WorkFlowStatus wfs; //pha
            if (PageRenderer.UserAction.ReturnType == "S")
            {
                workflowResult = PageRenderer.UserAction.ReturnValue.ToLower();
                wfs =  WorkFlowStatus.Finished;
            }
            else if (PageRenderer.UserAction.ReturnType == "A" && ValidateRule())
            {
                workflowResult = PageRenderer.UserAction.ReturnValue.ToLower();
                wfs = WorkFlowStatus.Finished;
            }
            else if (PageRenderer.UserAction.ReturnType == "B")
            {
                wfs = WorkFlowStatus.Back;
            }
            else
            {
                workflowResult = PageRenderer.UserAction.ReturnValue.ToLower();
                wfs = WorkFlowStatus.Indeterminate;
            }
            // pha
            try
            {
                using (System.IO.StreamWriter sw = new System.IO.StreamWriter(@"c:\temp\workflow.xml", true))
                {
                    sw.WriteLine("<DetermineCompletionState>");
                    sw.WriteLine("\t<Extender>" + PageRenderer.WorkflowInterface.ExtenderName + "</Extender>");
                    sw.WriteLine("\t<Status>" + wfs.ToString() + "</Status>");
                    sw.WriteLine("\t<Result>" + workflowResult + "</Result>");
                    sw.WriteLine("\t<PageName>" + PageRenderer.PageName + "</PageName>");
                    sw.WriteLine("\t<WorkflowStep>" + PageRenderer.WorkflowInterface.CurrentWorkflow.CurrentStep + "</WorkflowStep>");
                    sw.WriteLine("</DetermineCompletionState>");
                }
            }
            catch { }
            return wfs;
        }

        protected void WriteControlProperty(string controlName, string propertyName, object value)
        {
            PageRenderer.WriteControlProperty(controlName, propertyName, value);
        }

    }
}
