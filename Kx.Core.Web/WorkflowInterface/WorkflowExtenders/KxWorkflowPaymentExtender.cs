using System;
using System.Collections.Generic;
using System.Text;
using Kx.Core;
using Kx.Core.Exceptions;

namespace Kx.Core.Web.WorkflowInterface.WorkflowExtenders
{
    public class KxWorkflowPaymentExtender : KxWorkflowExtender
    {
        #region Properties & Attributes

        private string paymentProvider = "";
        private string description = "";
        private string currency = "";
        private string amount = "";
        private string amount2 = "";
        private bool testMode = false;
        private bool testTransactionOutcome = true;
        private string providerUrl = "";
        private string providerAccountId = "";
        private string providerCallbackUrl = "";
        private string paymentMethod = "";
        private string paymentType = "";
        private string confirmationPageUrl = "";
        private string cancellationPageUrl = "";
        private bool sendAddressDetails = true;
        private bool semesterPaymentOption = false;
        private bool monthlyPaymentOption = false;
        private bool isExtendedWPMProvider = false;
        private string extendedPaymentType = "";
        private string providerPassword = "";

        private string eraReturnCode = "";
        private string eraDepartmentId = "";
        private string eraContactId = "";
        private string eraCustomerId = "";
        private string eraItemList = "";

        private string secPayPassword = "";
        private string sh1Key = "";
        private string cardPaymentMethod = "";

        private string nominalCode = "";
        private string siteCode = "";

        private bool autoPost = false;
        private bool secPaySendEmail = false;

        private string customvalue1 = "";
        private string customvalue2 = "";

        private string merchantDisplayName = "";
        private string email = "";
        private string clientEmail = "";

        /* These are for KxPayments */
        private string instalmentMethod = "";
        private string instalmentDates = "";
        private string instalmentAmounts = "";

        private string merchantCountry = "GBR";
        /* ----------------------- */

        public int siteId;
        private int scpId;
        private int hmackeyId;
        private string hmackey = "";
        private string subjectType = "";
        private string systemCode = "";
        private int identifier;
        private string uniqueReference = "";
        private string timestamp = "";
        private string algorithm = "";

        public string CustomValue1
        {
            get
            { return customvalue1; }
            set
            { customvalue1 = value; }
        }

        public string CustomValue2
        {
            get
            { return customvalue2; }
            set
            { customvalue2 = value; }
        }

        public bool SecPaySendEmail
        {
            get
            {
                return secPaySendEmail;
            }
            set
            {
                secPaySendEmail = value;
            }
        }
        public string PaymentProvider
        {
            get
            { return paymentProvider; }
            set
            { paymentProvider = value; }
        }
        public string Description
        {
            get
            { return description; }
            set
            { description = value; }
        }
        public string Currency
        {
            get
            { return currency; }
            set
            { currency = value; }
        }
        public String Amount
        {
            get
            { return amount; }
            set
            { amount = value; }
        }

        public string Amount2
        {
            get
            {
                return amount2;
            }
            set
            {
                amount2 = value;
            }
        }
        public String CardPaymentMethod
        {
            get
            { return cardPaymentMethod; }
            set
            { cardPaymentMethod = value; }
        }
        public bool AutoPost
        {
            get
            { return autoPost; }
            set
            { autoPost = value; }
        }
        public bool TestMode
        {
            get
            { return testMode; }
            set
            { testMode = value; }
        }
        public bool MonthlyPaymentOption
        {
            get
            { return monthlyPaymentOption; }
            set
            { monthlyPaymentOption = value; }
        }
        public bool SemesterPaymentOption
        {
            get
            { return semesterPaymentOption; }
            set
            { semesterPaymentOption = value; }
        }
        public bool TestTransactionOutcome
        {
            get
            { return testTransactionOutcome; }
            set
            { testTransactionOutcome = value; }
        }
        public string ProviderUrl
        {
            get
            { return providerUrl; }
            set
            { providerUrl = value; }
        }

        public string ProviderPassword
        {
            get
            { return providerPassword; }
            set
            { providerPassword = value; }
        }
        public string ProviderAccountId
        {
            get
            { return providerAccountId; }
            set
            { providerAccountId = value; }
        }
        public string ProviderCallbackUrl
        {
            get
            {
                return providerCallbackUrl;
            }
            set
            {
                providerCallbackUrl = value;
            }
        }
        public bool IsExtendedWPMProvider
        {
            get
            {
                return isExtendedWPMProvider;
            }
            set
            {
                isExtendedWPMProvider = value;
            }
        }
        public string ExtendedPaymentType
        {
            get
            {
                return extendedPaymentType;
            }
            set
            {
                extendedPaymentType = value;
            }
        }
        public string PaymentMethod
        {
            get
            { return paymentMethod; }
            set
            { paymentMethod = value; }
        }
        public string PaymentType
        {
            get
            { return paymentType; }
            set
            { paymentType = value; }
        }
        public string ConfirmationPageUrl
        {
            get
            { return confirmationPageUrl; }
            set
            { confirmationPageUrl = value; }
        }
        public string CancellationPageUrl
        {
            get
            { return cancellationPageUrl; }
            set
            { cancellationPageUrl = value; }
        }
        public bool SendAddressDetails
        {
            get
            {
                return sendAddressDetails;
            }
            set
            {
                sendAddressDetails = value;
            }
        }
        public string EraReturnCode
        {
            get
            { return eraReturnCode; }
            set
            { eraReturnCode = value; }
        }
        public string EraDepartmentId
        {
            get
            { return eraDepartmentId; }
            set
            { eraDepartmentId = value; }
        }
        public string EraContactId
        {
            get
            { return eraContactId; }
            set
            { eraContactId = value; }
        }
        public string EraCustomerId
        {
            get
            { return eraCustomerId; }
            set
            { eraCustomerId = value; }
        }

        public string SH1Key
        {
            get { return sh1Key; }
            set { sh1Key = value; }
        }

        public string EraItemList
        {
            get
            { return eraItemList; }
            set
            { eraItemList = value; }
        }
        public string SecPayPassword
        {
            get
            { return secPayPassword; }
            set
            { secPayPassword = value; }
        }

        public string NominalCode
        {
            get
            { return nominalCode; }
            set
            { nominalCode = value; }
        }

        public string SiteCode
        {
            get
            { return siteCode; }
            set
            { siteCode = value; }
        }

        public string MerchantDisplayName
        {
            get
            { return merchantDisplayName; }
            set
            { merchantDisplayName = value; }
        }

        public string Email
        {
            get
            { return email; }
            set
            { email = value; }
        }

        public string ClientEmail
        {
            get
            { return clientEmail; }
            set
            { clientEmail = value; }
        }

        /* Added for KxPayments only */

        public string InstalmentMethod
        {
            get
            { return instalmentMethod.ToUpper(); }
            set
            { instalmentMethod = value; }
        }
        public string InstalmentDates
        {
            get
            { return instalmentDates; }
            set
            { instalmentDates = value; }
        }
        public string InstalmentAmounts
        {
            get
            { return instalmentAmounts; }
            set
            { instalmentAmounts = value; }
        }

        public string MerchantCountry
        {
            get { return merchantCountry; }
            set { merchantCountry = value; }
        }

        /* ------------------------- */

        /* Added for Pay360 only */
        public int SiteId
        {
            get { return siteId; }
            set { siteId = value; }
        }
        public int SCPId
        {
            get { return scpId; }
            set { scpId = value; }
        }
        public int HMACKeyId
        {
            get { return hmackeyId; }
            set { hmackeyId = value; }
        }
        public string HMACKey
        {
            get { return hmackey; }
            set { hmackey = value; }
        }
        public string SubjectType
        {
            get { return subjectType; }
            set { subjectType = value; }
        }
        public string SystemCode
        {
            get { return systemCode; }
            set { systemCode = value; }
        }
        public int Identifier
        {
            get { return identifier; }
            set { identifier = value; }
        }
        public string UniqueReference
        {
            get { return uniqueReference; }
            set { uniqueReference = value; }
        }
        public string Timestamp
        {
            get { return timestamp; }
            set { timestamp = value; }
        }
        public string Algorithm
        {
            get { return algorithm; }
            set { algorithm = value; }
        }

        /* ------------------------- */
        #endregion

        public override void AfterLoad()
        {
            if (AutoPost && (WebApp != null && !WebApp.InboundPayment))
            {
                base.AfterLoad();

                Execute();
            }
            else if (AutoPost && PageRenderer.WebApp != null && !PageRenderer.WebApp.InboundPayment)
            {
                base.AfterLoad();

                Execute();
            }
        }

        public override string Execute()
        {
            try
            {
                MakePayment();
                return "";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public override WorkFlowStatus DetermineCompletionState()
        {
            WorkFlowStatus result = base.DetermineCompletionState();

            if (result == WorkFlowStatus.Finished && WorkflowResult == "makepayment")
            {

                if (Execute() != "")
                {
                    result = WorkFlowStatus.Indeterminate;
                }
            }
            return result;
        }

        public virtual void MakePayment()
        {
            //WebApp.ContractManager.PostPayment(this);
        }
    }
}
