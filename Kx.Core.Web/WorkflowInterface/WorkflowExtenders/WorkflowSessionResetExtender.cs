using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using Kx.Core.Web.Configuration;

namespace Kx.Core.Web.WorkflowInterface.WorkflowExtenders
{
    public class WorkflowSessionResetExtender : KxWorkflowExtender
    {
        public override void BeforeLoad()
        {
            HttpContext.Current.Session.Abandon();
            //base.BeforeLoad();
            
            KxConfigurationManager.ResetSession(WebApp.CurrentPage);
        }

    }
}
