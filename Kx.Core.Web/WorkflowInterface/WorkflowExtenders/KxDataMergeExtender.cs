using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using System.IO;
using Kx.Core.Configuration;
using Kx.Core.Exceptions;

namespace Kx.Core.Web.WorkflowInterface.WorkflowExtenders
{
    [Serializable]
    public class KxDataMergeExtender : KxWorkflowExtender
    {
        protected string template = "";
        protected bool useHtmlControl = false; 
        protected string htmlTemplateControl = "";

        public string Template
        {
            get
            { return template; }
            set
            { template = value; }
        }

        public bool UseHtmlControl
        {
            get
            { return useHtmlControl; }
            set
            { useHtmlControl = value; }
        }

        public string HtmlTemplateControl
        {
            get
            { return htmlTemplateControl; }
            set
            { htmlTemplateControl = value; }
        }


        public string MergeData()
        {
            return MergeData(false);
        }

        public string MergeData(bool isPlainText)
        {
            if (UseHtmlControl)
            {
                return GetEmailContent(isPlainText);
            }
            else
            {
                string defaultLang = System.Configuration.ConfigurationManager.AppSettings["DefaultLanguage"];
                string overrideLang = HttpContext.Current.Request["lang"];

                string Source = PageRenderer.ResourceManager.ResourceProvider.GetResource(PageRenderer.ObjectTranslator.MergeTags(template.Replace("-lang-", overrideLang ?? defaultLang ?? string.Empty)));
                if (string.IsNullOrWhiteSpace(Source))
                {
                    KxLogger.LogActivity(this, "MergeData", "Data being merged from resource is empty", PageRenderer.ObjectTranslator.MergeTags(template.Replace("-lang-", overrideLang ?? defaultLang ?? string.Empty)));
                    Source = string.Empty;
                }

                return PageRenderer.ObjectTranslator.MergeTags(Source);
            }
        }

        public string GetEmailContent(bool isPlainText)
        {
            try
            {
                Page page = new Page();
                page.EnableViewState = false;
                HtmlForm form = new HtmlForm();
                page.Controls.Add(form);

                UserControl userControl = null;

                try
                {
                    userControl = (UserControl)page.LoadControl(HtmlTemplateControl);
                }
                catch (Exception ex)
                {
                    KxLogger.LogActivity(this, "GetEmailContent", "Unable to load page resource", $"An exception occurred while loading page resource {HtmlTemplateControl}. Exception: {ex.Message}.");
                }

                if (userControl == null)
                {
                    try
                    {
                        var applicationId = KxDbConfiguration.GetConfiguration("ApplicationId");
                        userControl = (UserControl)page.LoadControl($"{applicationId}/{HtmlTemplateControl}");
                    }
                    catch (Exception ex)
                    {
                        KxLogger.LogActivity(this, "GetEmailContent", "Unable to load page resource", $"An exception occurred while loading page resource {HtmlTemplateControl}. Exception: {ex.Message}.");
                    }
                }

                form.Controls.Add(userControl);

                PageRenderer.PopulateControlsList(page);

                StringWriter writer = new StringWriter();
                HttpContext.Current.Server.Execute(page, writer, false);

                string written = writer.ToString();

                if (!isPlainText
                    && !(written.ToUpper().Contains("<HTML")))
                {
                    return String.Format("<HTML><HEAD></HEAD><BODY>{0}</BODY></HTML>", written);
                }
                else if (isPlainText)
                {
                    // This removes the html tags automatically put in the Page
                    // specifically for use in plain text emails
                    written = written.Remove(0, written.IndexOf("</div>") + 6);
                    written = written.Remove(written.LastIndexOf("</form>"));
                    return written;
                }
                else
                {
                    return written;
                }
            }
            catch
            {
                return string.Empty;
            }
        }
    }
}
