using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Kx.Core.Email;
using Kx.Core.Exceptions;
using Kx.Core.Web.Configuration;
using Kx.Core.Web.PageRenderer.Utility;
using Kx.Objects.Common;
using Kx.Core.Utility;
using Winnovative;
using Kx.Core.Configuration;

namespace Kx.Core.Web.WorkflowInterface.WorkflowExtenders
{
	[Serializable]
	public class KxEmailExtender : KxDataMergeExtender
	{
		protected string recipients = "";
		protected string subject = "";
		protected string senderemail = "";
		protected string bodyFormat = "";
		protected string cC = "";
		protected string bCC = "";
		protected bool autoSend = false;
		protected string attachments = "";
		protected string templatetopdf = "";
		protected string complexwordtemplatetopdf = ""; 
		protected string wordtemplatetopdf = "";
		protected string templateAttachment = "";
		protected string wordtemplateallocationtype = "";
		protected bool resetSessionAfterAutoSend = false;
		protected string uniqueIdentifier = "";
		protected EmailTransmitEvent onEmailTransmit = null;

		protected EmailTransmitEvent OnEmailTransmit
		{
			get
			{ return onEmailTransmit; }
			set
			{ onEmailTransmit = value; }
		}


		public bool ResetSessionAfterAutoSend
		{
			get
			{
				return resetSessionAfterAutoSend;
			}
			set
			{
				resetSessionAfterAutoSend = value;
			}
		}

		public string Recipients
		{
			get
			{ return recipients; }
			set
			{ recipients = value; }
		}

		public string BCC
		{
			get
			{ return bCC; }
			set
			{ bCC = value; }
		}

		public string CC
		{
			get
			{ return cC; }
			set
			{ cC = value; }
		}

		public string Subject
		{
			get
			{ return subject; }
			set
			{ subject = value; }
		}

		public string TemplateToPDF
		{
			get
			{ return templatetopdf; }
			set
			{ templatetopdf = value; }
		}

		public string WordTemplateToPDF
		{
			get
			{ return wordtemplatetopdf; }
			set
			{ wordtemplatetopdf = value; }
		}

		public string ComplexWordTemplateToPDF
		{
			get
			{ return complexwordtemplatetopdf; }
			set
			{ complexwordtemplatetopdf = value; }
		}

		public string TemplateAttachment
		{
			get
			{ return templateAttachment; }
			set
			{ templateAttachment = value; }
		}

		public string WordTemplateAllocationType
		{
			get
			{ return wordtemplateallocationtype; }
			set
			{ wordtemplateallocationtype = value; }
		}

		public string SenderEmail
		{
			get
			{ return senderemail; }
			set
			{ senderemail = value; }
		}

		public string BodyFormat
		{
			get
			{ return bodyFormat; }
			set
			{ bodyFormat = value; }
		}

		public bool AutoSend
		{
			get
			{ return autoSend; }
			set
			{ autoSend = value; }
		}

		public string UniqueIdentifier
		{
			get
			{ return uniqueIdentifier; }
			set
			{ uniqueIdentifier = value; }
		}

        public bool SendAsync { get; set; } = false;


		public override void AfterLoad()
		{
			try
			{
				base.AfterLoad();
				if (AutoSend)
				{
					//KxLogger.LogActivity(this, "AfterLoad", "AutoSend Done", "");

					if (Execute() == "go")
					{
						//KxLogger.LogActivity(this, "AfterLoad", "Reset Now", "");

						AutoSend = false;

						if (resetSessionAfterAutoSend)
						{
							try
							{
								
								KxConfigurationManager.ResetSession();
							}
							catch { }
						}
						AfterAutoSend();
					}
				}
			}
			catch (Exception ex)
			{
				KxLogger.LogActivity(this, "AfterLoad", ex.Message, "");
			}
		}

		public override string Execute()
		{
			string result = "stop";

			string recipientlist = PageRenderer.ObjectTranslator.MergeTags(recipients);

			if (recipientlist.IndexOf(";") > -1)
			{
				string[] recips = recipientlist.Split(';');

				foreach (string recip in recips)
				{
					if (recip != "")
					{
						if (SendMail(recip))
						{
							result = "go";
						}
						else
						{
							result = "stop";
						}
					}
				}
			}
			else
			{
				if (SendMail(recipientlist))
				{
					result = "go";
				}
				else
				{
					result = "stop";
				}
			}
			return result;
		}

		public string Attachments
		{
			get
			{
				return attachments;
			}
			set
			{
				attachments = value;
			}
		}

		public bool SendMail(string recipient, string body)
		{

			template = body;
			return SendMail(recipient);
		}

		public bool SendMail(string recipient, string body, string subject2)
		{
			if (KxConfigurationManager.Profiling)
			{
				KxLogger.LogActivity(this, "SendMail1", " email recipient,body and subjec2", recipient + body + subject2);
			}
			string Source = PageRenderer.ResourceManager.ResourceProvider.GetResource(PageRenderer.ObjectTranslator.MergeTags(body));

			return SendMail(PageRenderer.ObjectTranslator.MergeTags(recipient), PageRenderer.ObjectTranslator.MergeTags(subject2), PageRenderer.ObjectTranslator.MergeTags(senderemail), PageRenderer.ObjectTranslator.MergeTags(Source), bodyFormat);
		}

		public bool SendMail(string recipient)
		{
			if (KxConfigurationManager.Profiling)
			{
				KxLogger.LogActivity(this, "SendMail2", " email recipient", recipient);
			}

            bool isPlainText = (BodyFormat == "PlainText") ? true : false;
            return SendMail(PageRenderer.ObjectTranslator.MergeTags(recipient), PageRenderer.ObjectTranslator.MergeTags(subject), PageRenderer.ObjectTranslator.MergeTags(senderemail), MergeData(isPlainText), bodyFormat);
		}

		public bool SendMailNoMerge(string recipient, string subject, string body)
		{
			if (KxConfigurationManager.Profiling)
			{
				KxLogger.LogActivity(this, "SendMail3", " email recipient, subject and body", recipient + body + subject);
			}
			return SendMail(recipient, subject, PageRenderer.ObjectTranslator.MergeTags(senderemail), body, bodyFormat);
		}

		public bool SendMail(string recipient, string _subject, string from, string body, string bodyformat)
        {
            if (SendAsync)
            {
                Task.Run(() => { PerformSendMail(recipient, _subject, from, body, bodyformat); });
                return true;
            }

            return PerformSendMail(recipient, _subject, from, body, bodyformat);
        }

		private bool PerformSendMail(string recipient, string _subject, string from, string body, string bodyformat)
		{
			if (KxConfigurationManager.Profiling)
			{
				KxLogger.LogActivity(this, "SendMail4", "Prepare email start", "");
			}

			EmailMessage msg = new EmailMessage();

			if (onEmailTransmit != null)
			{
				msg.OnEmailTransmit += new EmailTransmitEvent(onEmailTransmit);
			}

			msg.ToAddress = recipient;
			msg.Subject = _subject;
			msg.FromAddress = from;

			body = GetTheSubjectFromTheBody(body, msg);

			msg.TextBody = body;

			if (!string.IsNullOrEmpty(System.Configuration.ConfigurationManager.AppSettings["OverrideFromEmailAddress"]))
			{
				msg.FromAddress = System.Configuration.ConfigurationManager.AppSettings["OverrideFromEmailAddress"];
			}

			if (!string.IsNullOrEmpty(System.Configuration.ConfigurationManager.AppSettings["Email-TLS"]))
			{
				bool enableSSL = false;
				string enableSSLString = System.Configuration.ConfigurationManager.AppSettings["Email-TLS"];
				if (bool.TryParse(enableSSLString, out enableSSL))
				{
					msg.EnableSSL = enableSSL;
				}
			}

			if (BCC != "")
			{
				msg.BccAddress = PageRenderer.ObjectTranslator.MergeTags(BCC);
				BCC = BCC.Trim();
			}

			if (CC != "")
			{
				msg.CcAddress = PageRenderer.ObjectTranslator.MergeTags(CC);
				CC = CC.Trim();
			}

			if (bodyformat.ToLower() == "html" || (UseHtmlControl && !bodyformat.Equals("plaintext", StringComparison.InvariantCultureIgnoreCase)))
			{
				msg.HtmlBody = body;
			}

			string pdfno = PageRenderer.ObjectTranslator.MergeTags(UniqueIdentifier);

			if (String.IsNullOrEmpty(pdfno) || pdfno == UniqueIdentifier || pdfno == "0")
			{
				pdfno = Guid.NewGuid().ToString("N");
			}

			if (templatetopdf != "")
			{
				try
				{
					string[] pdfArray = templatetopdf.Trim().Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);

					foreach (string pdftemp in pdfArray)
					{
						string attachment = KxDocumentProcessor.GetTemplatePdfAsFile(PageRenderer, pdftemp, pdfno, "");
						msg.AddAttachment(attachment);
					}
				}
				catch (Exception ex)
				{
					KxLogger.LogActivity(this, "SendMail - Attachment1 ", ex.Message, "");
				}
			}

			if (templateAttachment != "")
			{
				try
				{
					string[] wordArray = templateAttachment.Trim().Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);

					foreach (string temp in wordArray)
					{
						string attachment = KxDocumentProcessor.GetTemplateAsFile(PageRenderer, temp, pdfno, "");
						msg.AddAttachment(attachment);
					}
				}
				catch (Exception ex)
				{
					KxLogger.LogActivity(this, "SendMail - Attachment2 ", ex.Message, "");
				}
			}

			if (wordtemplatetopdf != "")
			{
				try
				{
					int wordtemplateid = 0;

					string[] pdfWTArray = wordtemplatetopdf.Trim().Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);

					foreach (string pdftemp in pdfWTArray)
					{
						wordtemplateid = Convert.ToInt16(pdftemp);
						string attachment = KxDocumentProcessor.GetWordTemplatePdfAsFile(PageRenderer, wordtemplateid, pdfno, wordtemplateallocationtype, "");
						msg.AddAttachment(attachment);
					}
				}
				catch (Exception ex)
				{
					KxLogger.LogActivity(this, "SendMail - Attachment3 ", ex.Message, "");
				}
			}

			if (complexwordtemplatetopdf != "")
			{
				try
				{
					int wordtemplateid = 0;

					string[] pdfWTArray = complexwordtemplatetopdf.Trim().Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);

					foreach (string pdftemp in pdfWTArray)
					{

						wordtemplateid = Convert.ToInt16(pdftemp);
						string attachment = KxDocumentProcessor.GetComplexWordTemplateAsFile(PageRenderer, wordtemplateid, pdfno, wordtemplateallocationtype, "");
						msg.AddAttachment(attachment);
					}
				}
				catch (Exception ex)
				{
					KxLogger.LogActivity(this, "SendMail - Attachment4 ", ex.Message, "");
				}
			}

			try
			{
				string basePath = KxConfigurationManager.WorkflowFileLocation;

				if (basePath != null)
				{
					string atts = PageRenderer.ObjectTranslator.MergeTags(attachments);

					string[] attachmentArray = atts.Trim().Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);

					foreach (string att in attachmentArray)
					{
						string temp = PageRenderer.ObjectTranslator.MergeTags(att);
						if (temp.Contains("~"))
						{
							try
							{
								temp = HttpContext.Current.Server.MapPath(temp);
							}
							catch (Exception ex)
							{
								KxLogger.LogActivity(this, "SendMail - Add attachment ", ex.Message, att);
							}
						}
						if (File.Exists(temp))
						{
							msg.AddAttachment(temp);
							if (KxConfigurationManager.Profiling)
							{
								KxLogger.LogActivity(this, "SendMail - Adding attachment", temp, "");
							}
						}
						else
						{
							string path = Path.Combine(basePath, temp);

							if (File.Exists(path))
							{
								msg.AddAttachment(path);
								if (KxConfigurationManager.Profiling)
								{
									KxLogger.LogActivity(this, "SendMail - Adding attachment", path, "");
								}
							}
						}
					}
				}
			}
			catch (Exception ex)
			{
				KxLogger.LogActivity(this, "SendMail - Merge Tags ", ex.Message, "");
			}

			if (KxConfigurationManager.Profiling)
			{
				KxLogger.LogActivity(this, "SendMail5", "Prepare email end", "");
			}

			try
			{
				if (KxConfigurationManager.Profiling)
				{
					KxLogger.LogActivity(this, "SendMail6", "Sending mail start", "to:" + msg.ToAddress + " from :" + msg.FromAddress);
				}
				msg.Send();

				if (KxConfigurationManager.Profiling)
				{
					KxLogger.LogActivity(this, "SendMail8", "Sending mail end", "to:" + msg.ToAddress + " from :" + msg.FromAddress);
				}
				return true;
			}
			catch (Exception ex)
			{
				KxLogger.LogActivity(this, "SendMail9", ex.Message, "to:" + msg.ToAddress + " from :" + msg.FromAddress);
				return false;
			}
		}


		protected static string GetTheSubjectFromTheBody(string body, EmailMessage msg)
	    {
	        if (body.StartsWith("<!--SUBJECT:"))
	        {
	            string subject = body.Substring(0, body.IndexOf("-->"));
	            subject = subject.Replace("<!--SUBJECT:", "");
	            subject = subject.Replace("-->", "");
	            if (!string.IsNullOrEmpty(subject))
	            {
	                msg.Subject = subject;
	            }
	            body = body.Replace(string.Format("<!--SUBJECT:{0}-->", subject), "");
	        }
	        return body;
	    }

	    public override WorkFlowStatus DetermineCompletionState()
		{
			WorkFlowStatus result = base.DetermineCompletionState();

			if (result == WorkFlowStatus.Finished && workflowResult.ToUpper() == "SENDEMAIL")
			{
				Execute();
			}
			return result;
		}

		public virtual void AfterAutoSend()
		{

		}
	}
}
