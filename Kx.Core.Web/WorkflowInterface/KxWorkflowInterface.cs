using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.UI;
using Kx.Core.Exceptions;
using Kx.Core.Utility;
using Kx.Core.Workflow;
using Kx.Core.Web.BaseClass;
using Kx.Core.Web.Configuration;
using Kx.Core.Web.PageRenderer;
using Kx.Core.Web.PageRenderer.Utility;
using Kx.Core.Web.WebApplication;
using Kx.Core.Web.WorkflowInterface.WorkflowExtenders;
using Kx.Objects.Common;
using Kx.Objects.Common.Web;
using System.IO;

namespace Kx.Core.Web.WorkflowInterface
{
    public class KxWorkflowInterface
    {
        protected KxPageRenderer pageRenderer;
        protected Kx.Core.Workflow.Workflow currentWorkflow = null;
        protected Kx.Core.Workflow.Workflow previousWorkflow = null;
        protected string lastWorkflowPage = "";
        protected WorkflowManager workflowMgr = null;
        protected Guid workflowGuid = Guid.Empty;
        protected string lastAction = "";
        protected Object internalObject = null;
        // This is a safety hack to be removed if sites remain stable longer term
        protected static string _enableOldWebUserSaveBehaviour;

        public string ExtenderName { get; set; }

        /// <summary>
        /// Check to restore the old functionality for the ResumeExternal switches
        /// This is a safety hack to be removed if sites remain stable longer term
        /// </summary>
        protected bool enableOldWebUserSaveBehaviour
        {
            get
            {
                if (_enableOldWebUserSaveBehaviour == null)
                {
                    _enableOldWebUserSaveBehaviour = KxConfigurationManager.GetConfiguration("enableOldWebUserSaveBehaviour");
                }
                return _enableOldWebUserSaveBehaviour == "True";
            }
        }

        public KxWebUser WebUser
        {
            get
            {
                KxWebUser result = null;
                var wfObj = GetWorkflowObjectById("webuser");
                if (wfObj != null)
                {
                    result = wfObj.InternalObject as KxWebUser;

                    if (result != null)
                    {
                        CurrentWorkflow.KeyObjectIdentifier = result.UserGuid;

                        if (!CurrentWorkflow.LoggededInState && result.LoggedIn)
                        {
                            CurrentWorkflow.LoggededInState = result.LoggedIn;
                            LogOutOldUser(result.UserGuid);
                        }
                    }
                }

                return result;
            }
        }

        public Page CurrentPage
        {
            get
            {
                return PageRenderer.CurrentPage;
            }
        }

        public KxPageRenderer PageRenderer
        {
            get
            {
                return pageRenderer;
            }
            set
            {
                pageRenderer = value;
            }
        }

        public string LastAction
        {
            get
            {
                return lastAction;
            }
            set
            {
                lastAction = value;
            }
        }

        public virtual KxWorkflowExtender Extender
        {
            get
            {
                KxWorkflowExtender result = null;

                if (ExtenderName != "")
                {
                    var wfObj = GetWorkflowObjectById(ExtenderName);

                    if (wfObj != null)
                    {
                        result = wfObj.InternalObject as KxWorkflowExtender;
                        if (result != null)
                        {
                            result.PageRenderer = PageRenderer;
                        }
                    }
                }

                return result;
            }
        }


        public Kx.Core.Workflow.Workflow CurrentWorkflow
        {
            get
            {
                if (currentWorkflow == null || currentWorkflow.Identifier.CompareTo(workflowGuid) != 0)
                {
                    try
                    {
                        if (currentWorkflow != null && currentWorkflow.Identifier.CompareTo(workflowGuid) != 0)
                        {
                            previousWorkflow = currentWorkflow;
                        }
                        currentWorkflow = WorkflowMgr.GetActiveWorkflowByIdentifier(WorkflowGuid);
                    }
                    catch
                    {
                        currentWorkflow = null;
                    }
                }

                return currentWorkflow;
            }
        }

        public WorkflowManager WorkflowMgr
        {
            get
            {
                if (workflowMgr == null)
                {
                    try
                    {
                        workflowMgr = (WorkflowManager)KxConfigurationManager.GetSystemObject("WorkflowManager");
                    }
                    catch
                    {
                        workflowMgr = null;
                    }
                }

                return workflowMgr;
            }
        }

        public Guid WorkflowGuid
        {
            get
            {
                if (workflowGuid == Guid.Empty)
                {
                    try
                    {
                        if (HttpContext.Current.Session["Workflow"] != null)
                        {
                            workflowGuid = (Guid)HttpContext.Current.Session["Workflow"];
                        }
                    }
                    catch
                    {
                        workflowGuid = Guid.Empty;
                    }
                }
                return workflowGuid;
            }
            set
            {
                SetWorkflowGuid(value);
            }
        }

        public void ResetWebUser()
        {
            CurrentWorkflow.GetObjectByID("webuser").ResetObject();
        }

        public void AfterAjax()
        {
            if (Extender != null)
            {
                Extender.AfterAjax();
            }
        }

        public void AfterLoad()
        {
            if (Extender != null)
            {
                Extender.AfterLoad();
            }
        }

        public void Back()
        {
            LastAction = "";
            CurrentWorkflow.Back();
        }

        public void BeforeAjax()
        {
            if (Extender != null)
            {
                Extender.BeforeAjax();
            }
        }

        public void BeforeLoad()
        {
            if (Extender != null)
            {
                Extender.BeforeLoad();
            }
        }

        public void CheckForUnauthorisedNavigation(string url)
        {
            if (url.IndexOf(lastWorkflowPage.ToUpper()) < 0)
            {
                Repeat();
            }
        }

        public bool ExecuteRule(string rules)
        {
            LastAction = "";
            object obj = WorkflowMgr.ExecuteRule(rules, CurrentWorkflow);

            try
            {
                return Convert.ToBoolean(obj);
            }
            catch
            {
                return false;
            }
        }

        public void ExternalResume()
        {
            LastAction = "";

            if (HttpContext.Current.Session["Workflow"] != null)
            {
                workflowGuid = new Guid(HttpContext.Current.Session["Workflow"].ToString());
                CurrentWorkflow.OnExternalAction = null;
                CurrentWorkflow.OnExternalAction += new ExternalAction(OnWorkflowEvent);
            }

            CheckBlocked();
        }

        public void InitialiseWorkflow()
        {
            InitialiseWorkflow(false, false);
        }

        public void InitialiseWorkflow(bool IsPaymentResponse, bool AjaxPostback)
        {
            LastAction = "";

            string newWorkflowName = KxStringUtils.ObjectToString(HttpContext.Current.Request["wf"]);
            string oldWorkflowName = KxStringUtils.ObjectToString(HttpContext.Current.Session["WorkflowName"]);
            string overridestart = KxStringUtils.ObjectToString(HttpContext.Current.Request["start"]);

            if (CurrentWorkflow == null || (newWorkflowName != "" && newWorkflowName != oldWorkflowName))
            {
                if (!String.IsNullOrEmpty(newWorkflowName))
                {
                    Kx.Core.Web.Providers.AuditProvider.ConfiguredAddToAudit("KxWorkflowInterface", "InitialiseWorkflow", "New Workflow: " + newWorkflowName);

                    if ((CurrentWorkflow != null)
                        && (CurrentWorkflow.GetObjectByID("webuser") != null))
                    {
                        internalObject = CurrentWorkflow.GetObjectByID("webuser").InternalObject;
                    }

                    workflowGuid = WorkflowMgr.StartWorkflow(newWorkflowName, null, OnWorkflowEvent);
                    HttpContext.Current.Session["WorkflowName"] = newWorkflowName;
                }
                else
                {
                    workflowGuid = WorkflowMgr.StartWorkflow(KxConfigurationManager.DefaultWorkflow, null, OnWorkflowEvent);
                    HttpContext.Current.Session["WorkflowName"] = KxConfigurationManager.DefaultWorkflow;
                }

                HttpContext.Current.Session["Workflow"] = workflowGuid;

                if (!string.IsNullOrEmpty(overridestart))
                {
                    WorkflowMgr.ExecuteWorkFlow(WorkflowGuid, overridestart);
                }
                else
                {
                    WorkflowMgr.ExecuteWorkFlow(WorkflowGuid);
                }
            }

            CheckBlocked();

            if (IsPaymentResponse)
            {
                ProcessPaymentResponse();
            }
            else if (Extender != null && CurrentPage.IsPostBack)
            {
                WorkFlowStatus status = Extender.DetermineCompletionState();

                if (status == WorkFlowStatus.Finished && !AjaxPostback)
                {
                    Resume();
                }
                else if (status == WorkFlowStatus.Back && !AjaxPostback)
                {
                    Back();
                }
            }
        }

        public void AddStrike()
        {
            if (CurrentPage != null)
            {
                try
                {
                    if (WorkflowMgr.AddStrike(HttpContext.Current.Request.UserHostAddress) && !CurrentWorkflow.Blocked)
                    {
                        CurrentWorkflow.Blocked = true;
                        try
                        {
                            CurrentWorkflow.ExecuteStep(CurrentWorkflow.BlockStep);
                        }
                        catch { }
                    }
                }
                catch { }
            }
        }

        public void CheckBlocked()
        {
            if (CurrentPage != null)
            {
                try
                {
                    if (WorkflowMgr.BruteForce.IsBlocked(HttpContext.Current.Request.UserHostAddress) && !CurrentWorkflow.Blocked)
                    {
                        CurrentWorkflow.Blocked = true;
                        try
                        {
                            CurrentWorkflow.ExecuteStep(CurrentWorkflow.BlockStep);
                        }
                        catch { }
                    }
                }
                catch { }
            }
        }

        public void LoadWorkflow(string WorkflowName)
        {
            workflowGuid = WorkflowMgr.StartWorkflow(WorkflowName, null, OnWorkflowEvent);
        }

        public string OnWorkflowEvent(Kx.Core.Workflow.Workflow callingWorkflow, WorkflowExternalAction action)
        {
            string result = "";
            string pagename = MergeActionId(action.ActionID);

            Kx.Core.Web.Providers.AuditProvider.ConfiguredAddToAudit("Kx.Core.Web.WorkflowInterface.KxWorkflowInterface", "OnWorkflowEvent()", String.Format("pagename (URL): {0}, ActionID: {1}", pagename, action.ActionID));

            if ((internalObject != null)
                && (callingWorkflow != null)
                && (callingWorkflow.GetObjectByID("webuser") != null)
                && (callingWorkflow.GetObjectByID("webuser").InternalObject != null))
            {
                callingWorkflow.GetObjectByID("webuser").InternalObject = internalObject;
            }

            lastWorkflowPage = LocaliseURL(pagename);
            ExtenderName = action.ObjectId;
            Extender.PageTitle = action.Title;
            Extender.Progress = action.Progress;

            if (callingWorkflow.LogExternalAction != null)
            {
                LogExternalAction();
            }

            if (action != null)
            {
                Extender.LoadConfiguration(action);

                if (action.Log)
                {
                    action.TimeStarted = DateTime.Now;
                    callingWorkflow.LogExternalAction = action;
                }
                else
                {
                    callingWorkflow.LogExternalAction = null;
                }

                if (action.Rule != "")
                {
                    Extender.Rule = action.Rule;
                }

                if (pagename == string.Empty)
                {
                    result = Extender.Execute();
                }
                else
                {
                    Transfer(pagename);
                    result = "";
                }
            }

            return result;
        }

        public virtual string MergeActionId(string source)
        {
            return source;
        }

        public void Repeat()
        {
            LastAction = "";
            CurrentWorkflow.Repeat();
        }

        public void Resume()
        {
            LastAction = "";

            if (PageRenderer.UserAction.ReturnType == "A")
            {
                ResumeExternal();
            }
            else
            {
                CurrentWorkflow.Resume(Extender.WorkflowResult);
            }
        }

        public void Resume(string Result)
        {
            LastAction = Result;
            ResumeExternal(Result);
        }

        /// <summary>
        /// Changed so that we don't save WebUsers unnnecessarily
        /// </summary>
        /// <param name="Result"></param>
        public void ResumeExternal(string Result)
        {
            if (enableOldWebUserSaveBehaviour)
            {
                // This is a safety hack to be removed if sites remain stable longer term
                if (WebUser.Save())
                {
                    CurrentWorkflow.Resume(Result);
                }
                else
                {
                    CurrentWorkflow.ExecuteStep(CurrentWorkflow.ErrorStep);
                }
            }
            else
            {
                if (WebUser.IsInWebUsers)
                {
                    if (WebUser.Save())
                    {
                        CurrentWorkflow.Resume(Result);
                    }
                    else
                    {
                        CurrentWorkflow.ExecuteStep(CurrentWorkflow.ErrorStep);
                    }
                }
                else
                {
                    CurrentWorkflow.Resume(Result);
                }
            }
        }

        private void Transfer(string url)
        {
            SaveState();

            try
            {
                url = LocaliseURL(url);

                Kx.Core.Web.Providers.AuditProvider.ConfiguredAddToAudit("Kx.Core.Web.WorkflowInterface.KxWorkflowInterface", "Transfer(string url)", String.Format("url: {0}", url));

                bool startsWith = url.StartsWith("/");
                string[] split = url.Split('/');

                new Data.KxData().LogDebug("TransferURL", url); // pha

                if ((startsWith && (split.Length > 2))
                    || (!startsWith && (split.Length > 1)))
                {
                    HttpContext.Current.Server.Transfer("~/" + url);
                }
                else
                {
                    HttpContext.Current.Server.Transfer(url);
                }
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                    KxLogger.LogActivity(this, "Transfer Error:", ex.InnerException.Message, url);  // pha
                else
                    KxLogger.LogActivity(this, "Transfer Error:", ex.Message, url);
            }
        }

        private static string LocaliseURL(string url)
        {
            string defaultLang = System.Configuration.ConfigurationManager.AppSettings["DefaultLanguage"];
            string overrideLang = HttpContext.Current.Request["lang"];

            string result = url.Replace("-lang-", overrideLang ?? defaultLang ?? string.Empty);

            if (!File.Exists(HttpContext.Current.Server.MapPath(string.Format("~//{0}", result))))
            {
                result = url.Replace("-lang-", defaultLang ?? string.Empty);
            }

            return result;
        }

        public KxWorkflowInterface(KxPageRenderer _pageRenderer)
        {
            ExtenderName = "";
            PageRenderer = _pageRenderer;
        }

        public void ResumeExternal()
        {
            if (enableOldWebUserSaveBehaviour)
            {
                if (WebUser.Save())
                {
                    CurrentWorkflow.Resume(Extender.WorkflowResult);
                }
                else
                {
                    KxLogger.LogActivity(this, "Save", WebUser.LastError, "");
                    CurrentWorkflow.ExecuteStep(CurrentWorkflow.ErrorStep);
                }
            }
            else
            {
                if (WebUser.IsInWebUsers)
                {
                    if (WebUser.Save())
                    {
                        CurrentWorkflow.Resume(Extender.WorkflowResult);
                    }
                    else
                    {
                        KxLogger.LogActivity(this, "Save", WebUser.LastError, "");
                        CurrentWorkflow.ExecuteStep(CurrentWorkflow.ErrorStep);
                    }
                }
                else
                {
                    CurrentWorkflow.Resume(Extender.WorkflowResult);
                }
            }
        }

        public bool WorkflowSave()
        {
            if (System.Configuration.ConfigurationManager.AppSettings["DebugWorkflowSave"] == "True")
            {
                string log = LogWorkflowSave(WebUser);
                new Data.KxData().LogDebug("WorkflowSave", log); // pha
            }
            bool result = WebUser.Save();

            if (!result)
            {
                KxLogger.LogActivity(this, "Save", WebUser.LastError, "");
                CurrentWorkflow.ExecuteStep(CurrentWorkflow.ErrorStep);
            }

            return result;
        }

        private string LogWorkflowSave(Base.KxBaseClass o, int indent = 1)
        {
            if (o.Changed == false && o.ChildChanged == false)
                return "";
            string log = "\n" + new String('\t', indent) + "<Object>";
            string tabs = new String('\t', indent + 1);
            log += "\n" + tabs + "<Name>" + o.GetType().Name.ToString() + "</Name>";
            log += "\n" + tabs + "<Changed>" + o.Changed + "</Changed>";
            log += "\n" + tabs + "<ChildChanged>" + o.ChildChanged + "</ChildChanged>";
            if (o.Children.Children.Count > 0)
            {
                log += "\n" + tabs + "<Children>";
                foreach (Base.KxBaseClass child in o.Children.Children)
                {
                    log += LogWorkflowSave(child, indent + 2);
                }
                log += "\n" + tabs + "</Children>";
            }
            log += "\n" + new String('\t', indent) + "</Object>";
            return log;
        }

        public bool LogOutOldUser(Guid userGuid)
        {
            try
            {
                return workflowMgr.LogOutOldUser(userGuid, CurrentWorkflow.Identifier);
            }
            catch
            {
                return false;
            }
        }

        public virtual void ProcessPaymentResponse()
        {

        }

        public virtual void ProcessButtonTrigger(KxWebAttribute UserAction)
        {

        }

        public virtual void SaveState()
        {

        }

        public void LogExternalAction()
        {
            try
            {
                WorkflowExternalAction wfea = currentWorkflow.LogExternalAction;

                if (WebUser != null)
                {
                    TimeSpan st = DateTime.Now.Subtract(wfea.TimeStarted.Value);

                    KxWebUserPageLog pagelog = new KxWebUserPageLog(null, WebUser.UserGuid, wfea.ActionID);
                    pagelog.ViewDuration += Convert.ToInt32(st.TotalSeconds);
                    pagelog.Save();
                }
            }
            catch { };
        }

        public void SetWorkflowGuid(Guid value)
        {
            if (WorkflowMgr.ChangeWorkflowGuid(workflowGuid, value))
            {
                currentWorkflow = null;
                workflowGuid = value;
                HttpContext.Current.Session["Workflow"] = value;
            }
        }

        public void RefreshWorkflow()
        {
            currentWorkflow = null;
        }

        private WorkflowObject GetWorkflowObjectById(string objectId)
        {
            WorkflowObject wfObj = CurrentWorkflow.GetObjectByID(objectId);

            if (wfObj == null)
            {
                wfObj = previousWorkflow.GetObjectByID(objectId);
            }
            return wfObj;
        }

    }
}
