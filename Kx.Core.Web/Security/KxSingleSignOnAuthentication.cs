﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Text;
using System.Net;
using System.Web;
using Kx.Core.Configuration;

namespace Kx.Core.Web.Security
{
    public class KxSingleSignOnAuthentication
    {

        public static string Login(string user, string password)
        {
            string result = "";
            try
            {
                NameValueCollection values = ValidateUser(user, password);

                if (Convert.ToInt32(values["returnType"]) < 51)
                {
                    result = values["id"];
                }
            }
            catch { }
            return result;
                     
        }

        public static bool Authenticate(string user, string password, string studentId)
        {
            bool result = false;
            try
            {
                NameValueCollection values = ValidateUser(user, password);

                if (Convert.ToInt32(values["returnType"]) < 51)
                {
                    if (values["id"] == studentId)
                    {
                        result = true;
                    }
                }
            }
            catch 
            { }
            return result;            
        }

     	public static NameValueCollection ValidateUser(string user,string password) 
		{

			user = HttpContext.Current.Server.UrlEncode(user);
			password = HttpContext.Current.Server.UrlEncode(password);

            string url = string.Format("{0}?requestType=2&user={1}&pass={2}", System.Configuration.ConfigurationManager.AppSettings["SSO"], user, password);
			
			NameValueCollection r = CallSentry(url);

			return r;
		}

		public static NameValueCollection ValidateToken(string token) 
		{

            string url = string.Format("{0}?requestType=1&token={1}", System.Configuration.ConfigurationManager.AppSettings["SSO"], token);
			
			NameValueCollection r = CallSentry(url);
			
			return r;
		}

        private static NameValueCollection CallSentry(string url)
        {
            NameValueCollection values = new NameValueCollection(20);

            try
            {
                HttpWebRequest webrequest = (HttpWebRequest)WebRequest.Create(url);

                HttpWebResponse webresponse = (HttpWebResponse)webrequest.GetResponse();

                string responseData;

                using (webresponse)
                {
                    StreamReader sr = new StreamReader(webresponse.GetResponseStream());

                    using (sr)
                    {
                        responseData = sr.ReadToEnd();
                    }
                }

                string[] lines = responseData.Split('\n');

                foreach (string line in lines)
                {
                    string cleanLine = line.Trim();
                    if (cleanLine.Length > 0)
                    {
                        string[] parts = cleanLine.Split('=');
                        values.Add(parts[0], parts[1]);
                    }
                }
            }
            catch 
            {
            }
            return values;
        }


    }
}
