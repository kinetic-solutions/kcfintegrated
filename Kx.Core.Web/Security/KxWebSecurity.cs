using System;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Text;
using System.Security.Cryptography;
using System.Security.Principal;
using Kx.Objects.Common;

namespace Kx.Core.Web.Security
{
    public class KxWebSecurity
    {
        public static string Hash(string password)
        {
            // Returns a SHA1 Hash of the Password
            return FormsAuthentication.HashPasswordForStoringInConfigFile(password, "SHA1");
        }

        public static string GeneratePassword(string email)
        {
            return "";
        }

        public static bool ValidateLogin(string EnteredPassword,string password, string Salt)
        {
            return true;    // pha

            bool result = false;

            string newHash = Hash(Salt+EnteredPassword);

            if (newHash == password)
            {
                result = true;
            }
            return result;
        }

        public static string GenerateSalt(int saltByteSize)
        {
            // Random Number Generator
            RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();

            // Create the Salt
            byte[] buff = new byte[saltByteSize];
            rng.GetBytes(buff);

            // Convert the Salt to a Base 64 String
            return Convert.ToBase64String(buff);
        }

        public static bool ValidateAdminLogin(string userName, string Password)
        {
            bool result = false;

            KxUser user = new KxUser(null, userName);

            if (user.UserId > 0)
            {
                if (Kx.Core.Encryption.KxSHA1.Hash(Password, user.GuId.ToString()) == user.Password && user.AdminRights)
                {
                    result = user.AdminRights;
                    HttpContext.Current.User = new AdminPrincipal(new AdminIdentity(user.UserName, user.UserId, true, true, user.UserName, user.Email, "Admin"),"Admin");
                    HttpContext.Current.Session["ADMNLOGIN"] = user.UserName;
                }
            }
            return result;
        }
    }

    [Serializable]
    public class AdminIdentity : IIdentity
    {
        private String userId = String.Empty;
        private int userPK = 0;
        private bool login = false;
        private bool isSuperUser = false;
        private string fullName  = String.Empty;
        private string userEmail  = String.Empty;
        private string roles  = String.Empty;

        public string AuthenticationType
        {
            get { return "KxAdmin"; }
        }

        public bool IsAuthenticated
        {
            get { return login; }
            set { login = value; }
        }

        public string Name
        {
            get { return userId; }
            set { userId = value; }
        }

        public int UserPK
        {
            get { return userPK; }
            set { userPK = value; }
        }

        public bool IsSuperUser
        {
            get { return isSuperUser; }
            set { isSuperUser = value; }
        }

        public string UserFullName
        {
            get { return fullName; }
            set { fullName = value; }
        }

        public string UserEmail
        {
            get { return userEmail; }
            set { userEmail = value; }
        }

        public string UserRoles
        {
            get { return roles; }
            set { roles = value; }
        }
        
        public AdminIdentity()
        {
           
        }

        public AdminIdentity(string uId, int upk, bool islogin, bool isAdmin, string userName, string email, string uRoles)
        {
            this.userPK = upk;
            this.userId = uId;
            this.login = islogin;
            this.isSuperUser = isAdmin;
            this.fullName = userName;
            this.userEmail = email;
            this.roles = uRoles;
        }
    }

    [Serializable]
    public class AdminPrincipal : IPrincipal
    {
        private IIdentity identity;
        private ArrayList roles = new ArrayList();

        public IIdentity Identity
        {
            get { return identity; }
            set { identity = value; }
        }

        public AdminPrincipal(IIdentity id, string role)
        {
            identity = id;
            roles.Add(role);
        }

        public bool IsInRole(string role)
        {
            return roles.Contains(role);
        }
        
    }
}
