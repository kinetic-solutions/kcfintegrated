﻿using System;
using System.Collections.Specialized;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using Kx.Core.Exceptions;

namespace Kx.Core.Web.Security
{
	public class KxBrockAuthentication
	{
		public enum EncryptAlgorithmType { DES, RC2, Rijndael, TripleDES }
		public enum CryptoDirection { Encrypt, Decrypt }
		private static string _StudentID;
		private static string _LoginDate;
		private const string TestSharedKey = "X3NkUv9VC1g9zMcNTGdSv5yNKGw/+zOi"; // This will need to be updated with the correct shared key.

		public static string StudentID { get { return _StudentID; } }
		public static string LoginDate { get { return _LoginDate; } }
		
		public static NameValueCollection ValidateCookie(string cookie)
		{
			NameValueCollection values = new NameValueCollection(1);
			string SharedKey = System.Configuration.ConfigurationManager.AppSettings["BrockKey"];

            Kx.Core.Web.Providers.AuditProvider.ConfiguredAddToAudit("KxBrockAuthentication", "ValidateCookie", "Started. " + cookie);

			if (string.IsNullOrEmpty(SharedKey))
			{
				SharedKey = TestSharedKey;
			}

            Kx.Core.Web.Providers.AuditProvider.ConfiguredAddToAudit("KxBrockAuthentication", "ValidateCookie", "SharedKey: " + SharedKey);

			if (cookie.Length > 14)
            {
				string sData = cookie.Substring(0, cookie.Length - 12);
				string sIV = cookie.Substring(cookie.Length - 12);

                Kx.Core.Web.Providers.AuditProvider.ConfiguredAddToAudit("KxBrockAuthentication", "ValidateCookie", "sData:" + sData + "sIV:" + sIV);

				byte[] Key = Convert.FromBase64String(SharedKey);
                byte[] IV = Convert.FromBase64String(sIV);

				try
                {
                    string sResult = DecryptString(sData, EncryptAlgorithmType.TripleDES, Key, IV);

                    Kx.Core.Web.Providers.AuditProvider.ConfiguredAddToAudit("KxBrockAuthentication", "ValidateCookie", "sResult: " + sResult);

					_StudentID = sResult.Substring(0, sResult.Length - 14);
					_LoginDate = sResult.Substring(sResult.Length - 14);
				}
				catch (Exception ex)
				{
					KxLogger.LogActivity("KxBrockAuthentication", "ValidateCookie", ex.Message, cookie);
					_StudentID = "";
					_LoginDate = "";
				}
			}
			else
			{
				_StudentID = "";
				_LoginDate = "";
			}

            Kx.Core.Web.Providers.AuditProvider.ConfiguredAddToAudit("KxBrockAuthentication", "ValidateCookie", "_StudentID: " + _StudentID + " - _LoginDate: " + _LoginDate);

			values.Add("id", _StudentID);
			values.Add("date", _LoginDate);

            Kx.Core.Web.Providers.AuditProvider.ConfiguredAddToAudit("KxBrockAuthentication", "ValidateCookie", "Finished: " +  String.Format("StudentID={0} : Date={1}", _StudentID, _LoginDate));
			
            return values;
        }

        public static SymmetricAlgorithm GetAlgorithm(EncryptAlgorithmType Type)
        {

            //Return the right encryption service provider
            switch (Type)
            {
                case EncryptAlgorithmType.DES:
                    return new DESCryptoServiceProvider();
                case EncryptAlgorithmType.RC2:
                    return new RC2CryptoServiceProvider();
                case EncryptAlgorithmType.Rijndael:
                    return new RijndaelManaged();
                case EncryptAlgorithmType.TripleDES:
                    return new TripleDESCryptoServiceProvider();
                default:
                    //throw new ArgumentException("Invalid Algorithm Type");
                    break;
            }
            return null;
        }

		private static string DecryptString(string ValueToDecrypt, EncryptAlgorithmType Type, byte[] Key, byte[] IV)
		{

			ASCIIEncoding encoder = new ASCIIEncoding();
			byte[] value = Convert.FromBase64String(ValueToDecrypt);
			byte[] decrypted = DecryptByteArray(value, Type, Key, IV);

			return encoder.GetString(decrypted);
		}

		private static byte[] DecryptByteArray(byte[] byteArrayToEncrypt, EncryptAlgorithmType Type, byte[] Key, byte[] IV)
		{

			ICryptoTransform algorithm = GetCryptoTransformer(Type, CryptoDirection.Decrypt, Key, IV);

			using (MemoryStream buffer = new MemoryStream())
			{
				CryptoStream decStream = new CryptoStream(buffer, algorithm, CryptoStreamMode.Write);
				//Write data to encryption stream which stores it in the buffer
				try
				{
					decStream.Write(byteArrayToEncrypt, 0, byteArrayToEncrypt.Length);
					decStream.FlushFinalBlock();
				}
				catch (Exception)
				{
					//throw new IOException("Could not decrypt data", ex);
				}
				finally
				{
					decStream.Close();
				}
				return buffer.ToArray();
			}
		}

		private static ICryptoTransform GetCryptoTransformer(EncryptAlgorithmType Type, CryptoDirection Direction, byte[] key, byte[] IV)
		{

			SymmetricAlgorithm algorithm = GetAlgorithm(Type);
			//Set the mode to use
			algorithm.Mode = CipherMode.CBC;

			if (key == null)
				key = algorithm.Key; //Allow algorithm to generate the key
			else
				algorithm.Key = key; //Set the key

			if (IV == null)
				IV = algorithm.IV; //Allow algorithm to generate the key
			else
				algorithm.IV = IV; //Set IV

			switch (Direction)
			{
				case CryptoDirection.Decrypt:
					return algorithm.CreateDecryptor();			
				case CryptoDirection.Encrypt:
					return algorithm.CreateEncryptor();
				default:
					//throw new ArgumentException("Invalid Crypto Direction");
					break;
			}
			return null;
		}
	}
}
