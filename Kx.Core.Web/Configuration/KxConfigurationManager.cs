using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Text;
using System.Web;
using System.Web.UI;
using Kx.Core.Configuration;
using Kx.Core.Data;
using Kx.Core.Exceptions;
using Kx.Core.Resources;
using Kx.Core.Utility;
using Kx.Core.Web.BaseClass;
using Kx.Core.Web.WebApplication;
using Kx.Core.Workflow;
using Kx.Objects.Common;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Web.Hosting;

namespace Kx.Core.Web.Configuration
{
    public class KxConfigurationManager : KxDbConfiguration
    {
        public KxConfigurationManager()
        {

        }

        public string ApplicationId
        {
            get
            {
                return  GetConfiguration("ApplicationId");
            }
        }

        public bool AutoConfirmEmail
        {
            get
            {
                return GetConfigurationBool("autoConfirmEmail", false);
            }
            set
            {
                SetConfiguration("autoConfirmEmail", value);
            }
        }

        public static string EmailBCCAddress
        {
            get
            {
                return GetConfiguration("EmailBCCAddress");
            }
            set
            {
                SetConfiguration("EmailBCCAddress", value);
            }
        }

        public static string DefaultWorkflow
        {
            get
            {
                return GetConfiguration("DefaultWorkflow");
            }
            set
            {
                SetConfiguration("DefaultWorkflow", value);
            }
        }

        public static string ReCaptchaPublicKey
        {
            get
            {
                return GetConfiguration("Recaptcha.PublicKey");
            }
            set
            {
                SetConfiguration("Recaptcha.PublicKey", value);
            }
        }

        public static string ReCaptchaPrivateKey
        {
            get
            {
                return GetConfiguration("Recaptcha.PrivateKey");
            }
            set
            {
                SetConfiguration("Recaptcha.PrivateKey", value);
            }
        }

        public static bool ReCaptchaRequired
        {
            get
            {
                return GetConfigurationBool("Recaptcha.Required", false);
            }
            set
            {
                SetConfiguration("Recaptcha.Required", value);
            }
        }

        public static string ReCaptchaWorkflows
        {
            get
            {
                return GetConfiguration("Recaptcha.Workflows");
            }
            set
            {
                SetConfiguration("Recaptcha.Workflows", value);
            }
        }

        public static string WorkflowFileLocation
        {
            get
            {
                string location = GetConfiguration("WorkflowFileLocation");

                if (location.StartsWith("~"))
                {
                    location = HttpContext.Current.Server.MapPath(location);
                }
                // pha
                if (!System.IO.Directory.Exists(location))
                {
                    System.Diagnostics.Debugger.Break();
                }

                return location;
            }
            set
            {
                SetConfiguration("WorkflowFileLocation", value);
            }
        }

        public string HomeUrl
        {
            get
            {
                return GetConfiguration("homeUrl");
            }
            set
            {
                SetConfiguration("homeUrl", value);
            }
        }

        public string RootUrl
        {
            get { return new Uri(HttpContext.Current.Request.Url, System.Web.VirtualPathUtility.ToAbsolute("~/")).AbsoluteUri; }
        }

        public static string WorkflowHomePageUrl
        {
            get
            {
                return GetConfiguration("WorkflowHomePageUrl");
            }
            set
            {
                SetConfiguration("WorkflowHomePageUrl", value);
            }
        }

        public string CustomUrl1
        {
            get
            {
                return GetConfiguration("CustomUrl1");
            }
            set
            {
                SetConfiguration("CustomUrl1", value);
            }
        }

        public string CustomUrl2
        {
            get
            {
                return GetConfiguration("CustomUrl2");
            }
            set
            {
                SetConfiguration("CustomUrl2", value);
            }
        }

        public int DefaultAcademicYear
        {
            get
            {
                return GetConfigurationInt("DefaultAcademicYear", 0);
            }
            set
            {
                SetConfiguration("DefaultAcademicYear", value);
            }
        }

        public string EmailConfirmationWorkflow
        {
            get
            {
                return GetConfiguration("emailConfirmationWorkflow");
            }
            set
            {
                SetConfiguration("emailConfirmationWorkflow", value);
            }
        }

        public string EmailConfirmationUrl
        {
            get
            {
                return HomePageUrl + "?wf=" + EmailConfirmationWorkflow;
            }
            set
            {

            }
        }

        public static bool BreadCrumbing
        {
            get
            {
                return GetConfigurationBool("BreadCrumbing", false);
            }
            set
            {
                SetConfiguration("BreadCrumbing", value);
            }
        }

        public static bool UseApplicationObjects
        {
            get
            {
                return GetConfigurationBool("useApplicationObjects", false);
            }
            set
            {
                SetConfiguration("useApplicationObjects", value);
            }
        }

        public string HomePageUrl
        {
            get
            {
                return GetConfiguration("homepageUrl");
            }
            set
            {
                SetConfiguration("homepageUrl", value);
            }
        }

        public string DefaultEmail
        {
            get
            {
                return GetConfiguration("defaultEmail");
            }
            set
            {
                SetConfiguration("defaultEmail", value);
            }
        }

        public static bool ShowDebug
        {
            get
            {
                return GetConfigurationBool("showDebug", false);
            }
            set
            {
                SetConfiguration("showDebug", value);
            }
        }

        public static string Profiler
        {
            get
            {
                return GetSettingFromConfigNoAddSetting("Profiler");
            }
        }

        public static bool Profiling
        {
            get { return false; }    // pha
        }

        public static bool DontConvertAcceptancePage
        {
            get
            {
                return GetConfigurationBool("dontConvertAcceptancePage", false);
            }
            set
            {
                SetConfiguration("dontConvertAcceptancePage", value);
            }
        }

        public static object GetSystemObject(string ObjectName)
        {
            if (UseApplicationObjects)
            {
                return HttpContext.Current.Application[ObjectName];
            }
            else
            {
                return HttpContext.Current.Session[ObjectName];
            }
        }

        public static object SetSystemObject(string ObjectName, object obj)
        {
            if (UseApplicationObjects)
            {
                HttpContext.Current.Application[ObjectName] = obj;
                return obj;
            }
            else
            {
                HttpContext.Current.Session[ObjectName] = obj;
                return obj; 
            }
        }
        public static void InitialiseObjects(HttpApplication app)
        {
            // pha
            if (File.Exists(@"C:\temp\workflow.xml"))
                File.Delete(@"C:\temp\workflow.xml");

            KxDbConfiguration.Reset();
            string temp = KxDbConfiguration.GetConfiguration("WorkflowSource") + ";";

            string[] files = temp.Split(';');

            WorkflowManager mgr = new WorkflowManager();

            foreach (string str in files)
            {
                if (str != "")
                {
                    string filename = Path.Combine(WorkflowFileLocation, str + ".xml");

                    if ((filename != "") && (filename != null))
                    {
                        if (File.Exists(filename))
                        {
                            System.Xml.XmlDocument doc = new System.Xml.XmlDocument();

                            StreamReader r = new StreamReader(filename);
                            string xml = r.ReadToEnd();
                            r.Close();

                            mgr.AddWorkflow(xml);
                        }
                    }
                }
            }
            try
            {
                if (Kx.Core.Web.Configuration.KxConfigurationManager.UseApplicationObjects)
                {
                    HttpContext.Current.Application["WorkflowManager"] = mgr;
                }
                else
                {
                    HttpContext.Current.Session["WorkflowManager"] = mgr;
                }
            }
            catch { }

            try
            {
                if (KxDbConfiguration.GetConfigurationBool("LogEnvironment", false))
                {
                    var threadLoad = new Thread(new ThreadStart(StoreEnvironmentInformation));
                    threadLoad.Start();
                }
            }
            catch
            {
            }
        }

        private static void StoreEnvironmentInformation()
        {
            try
            {
                ClearCurrentEnvironmentInformation();
                foreach (var key in ConfigurationManager.AppSettings.AllKeys)
                {
                    string value = ConfigurationManager.AppSettings[key];
                    StoreEnvironmentValue("appSetting", key, value);
                }

                try
                {
                    StoreEnvironmentValue("IIS", "URL", new Uri(HttpContext.Current.Request.Url, "/").AbsoluteUri);
                }
                catch
                {
                    // We are probably on Application_Start and don't have a valid HTTPContext.Current.Request
                }
                StoreEnvironmentValue("IIS", "Path", HostingEnvironment.ApplicationPhysicalPath);

                var assemblies = from a in Directory.EnumerateFiles(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "bin"), "*.dll")
                    select new
                    {
                        Assemblyname = Path.GetFileName(a),
                        FileVersion = FileVersionInfo.GetVersionInfo(a).FileVersion,
                        FileDate = File.GetLastWriteTime(a)
                    };

                foreach (var assembly in assemblies)
                {
                    StoreEnvironmentValue("bin", assembly.Assemblyname, string.Format("{0} ({1})", assembly.FileVersion, assembly.FileDate.ToString("G")));
                }

                var connection = ConfigurationManager.ConnectionStrings["Kx"];
                if (connection == null)
                {
                    connection = ConfigurationManager.ConnectionStrings["Web"];
                }
                string connectionString = connection.ConnectionString;
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder(connectionString);
                StoreEnvironmentValue("Database", "Server", string.Format("{0}", builder.DataSource));
                StoreEnvironmentValue("Database", "Database", string.Format("{0}", builder.InitialCatalog));
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity("KxConfigurationManager", "StoreEnvironmentInformation", "Error storing information", ex.Message);
            }
        }

        private static void ClearCurrentEnvironmentInformation()
        {
            KxData data = new KxData("info", "DELETE WebResources WHERE ApplicationID = 'naff' AND ResourceType LIKE 'Environment.%' AND SiteVersion = 1", TargetType.Static);
            data.Execute();
        }

        private static void StoreEnvironmentValue(string variableType, string key, string value)
        {
            KxWebResource cfg = new KxWebResource(null, "naff", key, 1);
            cfg.ApplicationId = "naff";
            cfg.ResourceType = string.Format("Environment.{0}", variableType);
            cfg.Identifier = key;
            if ((key.ToLowerInvariant().Contains("password") || key.ToLowerInvariant().Contains("key")) && !string.IsNullOrWhiteSpace(value))
            {
                cfg.Data = "************************";
            }
            else
            {
                cfg.Data = value;
            }
            cfg.Changed = true;
            cfg.Save();
        }

        public static void ResetSession()
        {
            try
            {
                for (int i = 0; i < HttpContext.Current.Session.Count; i++)
                {
                    HttpContext.Current.Session[i] = null;
                }
            }
            catch { }

            HttpContext.Current.Session.Abandon();
        }

        public static void ResetSession(Page page)
        {
            try
            {
                for (int i = 0; i < page.Session.Count; i++)
                {
                    page.Session[i] = null;
                }
            }
            catch { }

            page.Session.Abandon();
        }
    }
}
