using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Kx.Core.Configuration;
using Kx.Core.Data;
using Kx.Core.Exceptions;
using Kx.Core.Localisation;
using Kx.Core.Web.BaseClass;
using Kx.Core.Web.Configuration;
using Kx.Core.Web.PageRenderer;
using Kx.Core.Web.PageRenderer.Utility;
using Kx.Core.Reflection;
using Kx.Core.Web.ObjectTranslator;
using Kx.Core.Web.Providers;
using Kx.Core.Web.WebApplication;
using Kx.Core.Web.WorkflowInterface;
using Kx.Core.Workflow;
using System.Threading;

namespace Kx.Core.Web.PageRenderer
{
    public class KxPageRenderer : WebBaseClass
    {
        protected KxObjectTranslator objectTranslator;
        protected KxWorkflowInterface workflowInterface;
        protected List<KxWebControl> KxControls = new List<KxWebControl>();
        protected Hashtable currentPageRequest = null;
        protected static string debug = "";
        protected string postbackTrigger = "";
        protected string validationText = "";
        protected string pageContent = "";
        protected bool ajaxPostback = false;

        protected KxWebAttribute userAction = null;       

        public Hashtable CurrentPageRequest
        {
            get
            {
                if (currentPageRequest == null)
                {
                    currentPageRequest = new Hashtable();
                }
                return currentPageRequest;
            }
        }

        public string ValidationText
        {
            get
            { return validationText; }
        }

        /// <summary>
        /// Determines if Page Renderer Debug is enabled (ShowPopulationErrors = y). 
        /// </summary>
        public bool Debug
        {
            get
            {
                return true;    //pha
                if (debug == null)
                {
                    debug = KxDbConfiguration.GetConfiguration("ShowPopulationErrors");
                }
                return debug == "y";
            }
        }

        public bool IsAutoPostBack
        {
            get
            {
                return postbackTrigger != "";
            }
        }

        public bool AjaxPostback
        {
            get
            {
                return ajaxPostback;
            }
            set
            {
                ajaxPostback = value;
            }
        }

        public KxObjectTranslator ObjectTranslator
        {
            get
            {
                if (objectTranslator == null)
                {
                    CreateObjectTranslator();
                }
                return objectTranslator;
            }
        }

        public KxWorkflowInterface WorkflowInterface
        {
            get
            {
                if (workflowInterface == null)
                {
                    CreateWorkflowInterface();
                }
                return workflowInterface;
            }
        }

        public string PageContent
        {
            get
            { return pageContent; }
            set
            { pageContent = value;}
        }

        public string PostbackTrigger
        {
            get
            { return postbackTrigger; }
        }

        public KxWebAttribute UserAction
        {
            get
            { return userAction; }
        }

        public Page CurrentPage
        {
            get
            { return webApp.CurrentPage; }
        }

        public string PageName
        {
            get
            {
                try
                {
                    return Path.GetFileNameWithoutExtension(CurrentPage.AppRelativeVirtualPath);
                }
                catch 
                {
                    return "";
                }
            }
        }

        public KxPageRenderer(KxWebApplication webApp) : base(webApp)
        {
            
        }

        public void PopulateControlsList(Control control)
        {
            if (control != null && control.ID != null)
            {
                try
                {
                    int idx = control.ID.ToUpper().IndexOf("KX");

                    if (idx > -1)
                    {
                        AddWebControl(webApp, control, CurrentPage, WebApp.ResourceManager.ListProvider);
                    }
                }
                catch (NullReferenceException e)
                {
                    if (Debug)
                    { 
                    KxData data = new KxData();
                    data.LogException("KxPageRenderer", "PopulateControlsList: " + this.PageName, e.Message, control.ID);
                    }
                }               
            }

            if (control != null)
            {
                foreach (Control c in control.Controls)
                {
                    PopulateControlsList(c);
                }
            }
        }

        public void PopulateControlsListReverse(Control control)
        {
            if (control != null)
            {
                foreach (Control c in control.Controls)
                {
                    PopulateControlsList(c);
                }
            }

            if (control != null && control.ID != null)
            {
                int idx = control.ID.ToUpper().IndexOf("KX");

                if (idx > -1)
                {
                    AddWebControl(webApp, control, CurrentPage, WebApp.ResourceManager.ListProvider);
                }
            }

          
        }

        public virtual void AddWebControl(object webApp, Control control, Page CurrentPage, KxListProvider listProvider)
        {

        }

        public virtual void CreateWorkflowInterface()
        {
            workflowInterface = new KxWorkflowInterface(this);
        }

        public virtual void CreateObjectTranslator()
        {
            objectTranslator = new KxObjectTranslator(this);
        }

        protected void PopulateRequest(Control trigger)
        {
            CurrentPageRequest.Clear();
            userAction = new KxWebAttribute("");

            if (trigger == null)
            {
                foreach (string key in HttpContext.Current.Request.Form.AllKeys)
                {
                    ProcessRequest(key);
                }
            }
            else
            {
                ProcessRequest(trigger.ID);
            }
        }

        private void ProcessRequest(string key)
        {
            try
            {
                string Keyname = "";
                string value = "";

                if (key == "__EVENTTARGET")
                {
                    postbackTrigger = KxPageUtility.StripUniqueNaming(HttpContext.Current.Request[key]);
                    Keyname = KxPageUtility.StripUniqueNaming(HttpContext.Current.Request[key]);
                    value = Keyname;
                }
                else
                {
                    Keyname = KxPageUtility.StripUniqueNaming(key);
                    value = KxPageUtility.StripUniqueNaming(HttpContext.Current.Request[key]);
                }

                if (Keyname.StartsWith("KX_BC"))
                {
                    userAction = new KxWebAttribute(Keyname);

                    ProcessButtonTrigger(Keyname);

                    value = Keyname;
                    Keyname = "RX" + userAction.ReturnType;
                }
            }
            catch { }
        }

        public void ProcessPage(Page page, bool IsPaymentResponse)
        {
            try
            {
                try
                {
                    Breadcrumb();
                }
                catch (Exception ex)
                {
                    throw new Exception("Breadcrumb Error: " + ex.Message);
                }

                try
                {
                    KxControls.Clear();
                    PopulateRequest(null);
                }
                catch (Exception ex)
                {
                    throw new Exception("Populate Request Error: " + ex.Message);
                }

                if (IsPaymentResponse)
                {
                    ProcessPaymentResponse();
                }
                else
                {
                    WorkflowInterface.CheckForUnauthorisedNavigation(page.AppRelativeVirtualPath.ToUpper());
                }

                try
                {
                    WorkflowInterface.BeforeLoad();
                }
                catch (Exception ex)
                {
                   throw new Exception("Workflow BeforeLoad Error: " + ex.Message);
                }

                 try
                {
                    PopulateControlsList(page);
                }
                 catch (Exception ex)
                {
                    throw new Exception("Populate Controls List: " + ex.Message);
                }
                
                try
                {
                    WorkflowInterface.AfterLoad();
                }
                catch (Exception ex)
                {
                     throw new Exception("Workflow AfterLoad Error: " + ex.Message);
                }
                try
                {
                    WorkflowInterface.InitialiseWorkflow(IsPaymentResponse, false);
                }
                catch (ThreadAbortException)
                { }
                catch (Exception ex)
                {
                    throw new Exception("Initialise Workflow: " + ex.Message);
                }
            }
            catch (Exception ex)
            {
                //if (Debug) //pha
                //{
                if (!ex.Message.StartsWith("Thread was being aborted"))
                    KxLogger.LogActivity(this,"ProcessPage",ex.Message,page.Title, true);   // pha
                //}
            }
        }

        public void ProcessAjax(Control control, Control trigger)
        {
            ProcessAjax(control, trigger,true);
        }

        public void ProcessAjax(Control control, Control trigger, bool ProcessWorkflow)
        {
            try
            {
                try
                {
                    KxControls.Clear();
                    PopulateRequest(trigger);
                }
                catch (Exception ex)
                {
                    throw new Exception("Populate Request Error: " + ex.Message);
                }

                try
                {
                    WorkflowInterface.BeforeAjax();
                }
                catch (Exception ex)
                {
                    throw new Exception("Workflow BeforeAjax Error: " + ex.Message);
                }

                try
                {
                    PopulateControlsList(control);
                }
                catch (Exception ex)
                {
                    throw new Exception("Populate Controls List: " + ex.Message);
                }

                try
                {
                    WorkflowInterface.AfterAjax();
                }
                catch (Exception ex)
                {
                    throw new Exception("Workflow AfterAjax Error: " + ex.Message);
                }
            
                try
                {
                    if (ProcessWorkflow)
                    {
                        WorkflowInterface.InitialiseWorkflow(false,true);
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Initialise Workflow Error: " + ex.Message);
                }
    
            }
            catch (Exception ex)
            {
                if (Debug) 
                { 
                try
                {
                    KxLogger.LogActivity(this, "ProcessPage", ex.Message, CurrentPage.Title);
                }
                catch { }
                }
            }
        }

        private void Breadcrumb()
        {
            if (KxConfigurationManager.BreadCrumbing)
            {
                try
                {
                    string userName = "anon";

                    if (WorkflowInterface.WebUser != null)
                    {
                        userName = WorkflowInterface.WebUser.UserName;
                    }

                    KxData dummy = new KxData();
                    dummy.BreadcrumbTrail(HttpContext.Current.Request.UrlReferrer.AbsolutePath, HttpContext.Current.Request.UserHostAddress,
                    userName, HttpContext.Current.Request.Url.AbsolutePath);
                }
                catch
                { }
            }
        }

        public Control FindKxControl(string Id)
        {
            Control result = null;

            KxWebControl webcontrol = FindKxWebControl(Id);

            if (webcontrol != null)
            {
                result = webcontrol.ParentControl;
            }
            return result;

        }

        public KxWebControl FindKxWebControl(string Id)
        {
            KxWebControl result = null;

            foreach (KxWebControl ctrl in KxControls)
            {
                if (ctrl.ParentControl.ID.ToUpper() == Id.ToUpper() || ctrl.ParentControl.UniqueID.ToUpper() == Id.ToUpper())
                {
                    result = ctrl;
                    break;
                }
            }

            if (result == null)
            {
                foreach (KxWebControl ctrl in KxControls)
                {
                    if (ctrl.ParentControl.ID.ToUpper().Contains(Id.ToUpper()))
                    {
                        result = ctrl;
                        break;
                    }
                }
            }

            return result;
        }

        public bool GetIsExtendedError()
        {
            return GetIsExtendedError("KXError");
        }

        public bool GetIsExtendedError(string controlName)
        {
            bool result = false;
            try
            {
                Control control = KxPageUtility.FindControl(CurrentPage, controlName);
                if (control != null)
                {
                    if (control is Literal)
                    {
                        result = true;
                    }
                }
            }
            catch{}

            return result;
        }


        public void WriteControlProperty(string controlName,string propertyName, object Value)
        {
            if (controlName.ToUpper().StartsWith("KX_"))
            {
                Control control = FindKxControl(controlName);

                if (control != null)
                {
                    KxReflection.SetProperty(control, propertyName, Value);
                }
            }
            else
            {
                Control control = KxPageUtility.FindControl(CurrentPage, controlName);

                if (control != null)
                {
                    KxReflection.SetProperty(control, propertyName, Value);
                }
            }
        }

        public string CurrentRequestToString(string key)
        {
            string result = "";
        
            if (CurrentPageRequest[key] != null)
            {
                result = CurrentPageRequest[key].ToString();
            }
            return result;
       }

        public bool IsPostbackValid()
        {
            return IsPostbackValid(null);
        }

        public bool IsPostbackValid(Control control)
        {
            bool extendedControl = GetIsExtendedError();
            validationText = "";

            bool result = true;

            try
            {

                foreach (KxWebControl wc in KxControls)
                {
                    if ((control == null && !wc.Valid) || !wc.GetValid(control))
                    {
                        string validationString = wc.ValidationString;

                        if (validationText.IndexOf(validationString) < 0)
                        {
                            validationText += validationString;
                        }
                        result = false;
                    }
                    
                }
            }
            catch (Exception ex)
            {
                if (Debug) 
                { 
                KxLogger.LogActivity(this, "IsPostbackValid", ex.Message, "");
                }
            }

            WriteErrorStatus(validationText != "", validationText);

            return result;
        }

        public void ProcessButtonTrigger(string Keyname)
        {
            KxWebAttribute UserAction = new KxWebAttribute(Keyname);
            if (System.Configuration.ConfigurationManager.AppSettings["DebugProcessButtonTrigger"] == "True")
                DebugProcessButtonTrigger(Keyname); // pha
            WorkflowInterface.ProcessButtonTrigger(UserAction);          
        }

        public void DebugProcessButtonTrigger(string keyname)
        {
            using (StreamWriter sw = new StreamWriter(@"c:\temp\workflow.xml", true))
            {
                sw.WriteLine("<Keyname>" + keyname + "</Keyname>");
                sw.WriteLine("<ReturnValue>" + UserAction.ReturnValue + "</ReturnValue>");

                if (!String.IsNullOrEmpty(UserAction.SiteLookup))
                {
                    sw.WriteLine("<SiteLookup>" + UserAction.SiteLookup + "</SiteLookup>");
                }

                if (!String.IsNullOrEmpty(UserAction.LibertyRoom))
                {
                    sw.WriteLine("<LibertyRoom>" + UserAction.LibertyRoom + "</LibertyRoom>");
                }

                if (!String.IsNullOrEmpty(UserAction.OpalRoom))
                {
                    sw.WriteLine("<OpalRoom>" + UserAction.OpalRoom + "</OpalRoom>");
                }

                if (!String.IsNullOrEmpty(UserAction.RoomFilter))
                {
                    sw.WriteLine("<RoomFilter>" + UserAction.RoomFilter + "</SiRoomFilterteLookup>");
                }

                if (!String.IsNullOrEmpty(UserAction.WebResource))
                {
                    sw.WriteLine("<WebResource>" + UserAction.WebResource + "</WebResource>");
                }

                if (!String.IsNullOrEmpty(UserAction.DocumentName))
                {
                    sw.WriteLine("<DocumentName>" + UserAction.DocumentName + "</DocumentName>");
                }
                if (UserAction.AcademicYear > 0)
                {
                    sw.WriteLine("<AcademicYear>" + UserAction.AcademicYear + "</AcademicYear>");
                }

                if (UserAction.AcademicYearSession > -1)
                {
                    sw.WriteLine("<AcademicYearSession>" + UserAction.AcademicYearSession + "</AcademicYearSession>");
                }

                if (UserAction.SpecificApplicationGuid != null)
                {
                    sw.WriteLine("<SpecificApplicationGuid>" + UserAction.SpecificApplicationGuid + "</SpecificApplicationGuid>");
                }

                if (UserAction.RequestIdentifier > 0)
                {
                    sw.WriteLine("<RequestIdentifier>" + UserAction.RequestIdentifier + "</RequestIdentifier>");
                }

                if (UserAction.AllocationPricing != "")
                {
                    sw.WriteLine("<AllocationPricing>" + UserAction.AllocationPricing + "</AllocationPricing>");
                }

                if (UserAction.DueDate != "")
                {
                    sw.WriteLine("<DueDate>" + UserAction.DueDate + "</DueDate>");
                }


                if (UserAction.BedroomType > -1)
                {
                    sw.WriteLine("<BedroomType>" + UserAction.DueDate + "</BedroomType>");
                }

                if (UserAction.UDF > 0)
                {
                    sw.WriteLine("<UDF>" + UserAction.UDF + "</UDF>");
                }

                if (UserAction.AllocationDate > 0)
                {
                    sw.WriteLine("<AllocationDate>" + UserAction.AllocationDate + "</AllocationDate>");
                }

                if (UserAction.Preference > 0)
                {
                    sw.WriteLine("<Preference>" + UserAction.Preference + "</Preference>");
                }

                if (UserAction.PrefNumber > 0)
                {
                    sw.WriteLine("<PrefNumber>" + UserAction.PrefNumber + "</PrefNumber>");
                }

                if (UserAction.GroupMember > 0)
                {
                    sw.WriteLine("<GroupMember>" + UserAction.GroupMember + "</GroupMember>");
                }

                if (UserAction.StudentTermDate > 0)
                {
                    sw.WriteLine("<StudentTermDate>" + UserAction.StudentTermDate + "</StudentTermDate>");
                }

                if (UserAction.PrefBasketIndex > 0)
                {
                    sw.WriteLine("<PrefBasketIndex>" + UserAction.PrefBasketIndex + "</PrefBasketIndex>");
                }

                if (UserAction.PrefSearchIndex > 0)
                {
                    sw.WriteLine("<PrefSearchIndex>" + UserAction.PrefSearchIndex + "</PrefSearchIndex>");
                }

                if (UserAction.GenericIndex > 0)
                {
                    sw.WriteLine("<GenericIndex>" + UserAction.GenericIndex + "</GenericIndex>");
                }
                if (UserAction.GenericIndex2 > 0)
                {
                    sw.WriteLine("<GenericIndex2>" + UserAction.GenericIndex2 + "</GenericIndex2>");
                }
                if (UserAction.GenericIndex3 > 0)
                {
                    sw.WriteLine("<GenericIndex3>" + UserAction.GenericIndex3 + "</GenericIndex3>");
                }
            }
        }

        public void WriteErrorStatus(string controlName, bool HasError, string validationText)
        {
            if (HasError && GetIsExtendedError(controlName))
            {
                WriteControlProperty(controlName, "Text", ConvertErrorString(validationText));
            }
            else
            {
                WriteControlProperty(controlName, "Text", validationText);
            }
            WriteControlProperty("KXErrPanel", "Visible", HasError);
        }

        public void WriteErrorStatus(bool HasError,string validationText)
        {
            WriteErrorStatus("KXError", HasError, validationText);
        }

        public virtual void ProcessPaymentResponse()
        {
            WorkflowInterface.ExternalResume();
        }

        protected string ConvertErrorString(string source)
        {
            StringBuilder result = new StringBuilder();
            result.AppendLine("<div class=\"KxExtError\">");
            result.AppendLine(KxLocalisationManager.GetSubstitution("Please correct the following errors before continuing") + "<br /><br />");
            result.AppendLine("<ul>");

            String[] lines = source.Split(new string[]{"\r\n"},StringSplitOptions.RemoveEmptyEntries);

            foreach (string s in lines)
            {
                result.AppendLine("<li>"+s+"</li>");
            }
            result.AppendLine("</ul>");
            result.AppendLine("</div>");

            return result.ToString();
        }
    }
}
