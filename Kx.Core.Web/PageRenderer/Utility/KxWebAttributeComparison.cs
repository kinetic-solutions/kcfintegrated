using System;
using System.Collections.Generic;
using System.Text;
using Kx.Core.Localisation;
using Kx.Core.Utility;

namespace Kx.Core.Web.PageRenderer.Utility
{
    public class KxWebAttributeComparison
    {
        public static string GetValidationText(KxWebAttribute attributes,string controlvalue)
        {
            string result = "";

            switch (attributes.ValidationType)
            {                
               case "GT":  result = " " + KxLocalisationManager.GetSubstitution("must be greater than") + "  " + controlvalue + ". \r\n"; break;     
               case "LT":  result = " " + KxLocalisationManager.GetSubstitution("must be less than") + " " + controlvalue + ". \r\n"; break;     
               case "GTE": result = " " + KxLocalisationManager.GetSubstitution("must be greater than or equal to") + "  " + controlvalue + ". \r\n"; break;     
               case "LTE": result = " " + KxLocalisationManager.GetSubstitution("must be less than or equal to") + " " + controlvalue + ". \r\n"; break;     
               case "NE":  result = " " + KxLocalisationManager.GetSubstitution("must not equal") + " " + controlvalue + ". \r\n"; break;     
               default:    result = " " + KxLocalisationManager.GetSubstitution("must equal") + " " + controlvalue + ". \r\n"; break;     
            }
            return result;
        }

        public static Boolean DoValidationComparison(KxWebAttribute attributes, string comparison, string controlvalue)
        {
            return DoComparison(attributes, comparison, controlvalue,true);
        }

        public static Boolean DoComparison(KxWebAttribute attributes, string comparison, string controlvalue)
        {
            return DoComparison(attributes, comparison, controlvalue,false);
        }

        public static Boolean GetComparison(KxWebAttribute attributes, string comparison, string controlvalue)
        {
            return DoComparison(attributes, comparison, controlvalue, false);
        }

        public static Boolean DoComparison(KxWebAttribute attributes, string comparison, string controlvalue, bool validationComparison)
        {
            bool result = false;

            string ComparisonType = attributes.ComparisonType;

            if (validationComparison)
            {
                ComparisonType = attributes.ValidationType;
            }

            if (attributes.OverrideType == null || attributes.OverrideType == typeof(string))
            {
                result = DoComparison(attributes, ComparisonType, comparison, controlvalue);
            }
            else if (attributes.OverrideType == typeof(DateTime))
            {
                DateTime dcomp = KxDateTime.KxNullDate;
                DateTime dcv = KxDateTime.KxNullDate;

                try
                {
                    if (comparison.ToUpper() == "NOW")
                    {
                        dcomp = DateTime.Now;
                    }
                    else if (comparison.ToUpper() == "TODAY")
                    {
                        dcomp = DateTime.Now;
                    }
                    else
                    {
                        dcomp = KxDateTime.Parse(comparison);
                    }
                }
                catch { }

                try
                {
                    if (controlvalue.ToUpper() == "NOW")
                    {
                        dcv = DateTime.Now;
                    }
                    else if (controlvalue.ToUpper() == "TODAY")
                    {
                        dcv = DateTime.Now;
                    }
                    else
                    {
                        dcv = KxDateTime.Parse(controlvalue);
                    }
                }
                catch { }

                result =  DoComparison(ComparisonType, dcomp, dcv);

            }
            else if (attributes.OverrideType == typeof(int))
            {
                int icomp = 0;
                int icv = 0;

                try
                {
                    icomp = Convert.ToInt32(comparison);
                }
                catch { }

                try
                {
                    icv = Convert.ToInt32(controlvalue);
                }
                catch { }

                result = DoComparison(ComparisonType, icomp, icv);
            }
                
            return result;
        }

        private static bool DoComparison(KxWebAttribute attributes, string ComparisonType, string comparison, string controlvalue)
        {
            bool result = false;

            switch (ComparisonType)
            {   
                case "GT":  try { result = Convert.ToInt32(comparison) > Convert.ToInt32(controlvalue); }catch { };break;
                case "LT":  try { result = Convert.ToInt32(comparison) < Convert.ToInt32(controlvalue); }catch { };break;
                case "GTE": try { result = Convert.ToInt32(comparison) >= Convert.ToInt32(controlvalue); } catch { };break;
                case "LTE": try { result = Convert.ToInt32(comparison) <= Convert.ToInt32(controlvalue); } catch {} ;break;
                case "NE":  
                    result = comparison != controlvalue;
                    break;
                case "IN":  result = controlvalue.IndexOf(attributes.FilterSeperator + comparison + attributes.FilterSeperator) > -1;break;
                case "NI":  result = controlvalue.IndexOf(attributes.FilterSeperator + comparison + attributes.FilterSeperator) == -1;break;
                case "CN":  result = !String.IsNullOrEmpty(comparison) && comparison.ToUpper().IndexOf(controlvalue.ToUpper()) > -1; break;
                case "NC":  result = String.IsNullOrEmpty(comparison) || comparison.ToUpper().IndexOf(controlvalue.ToUpper()) == -1;break;
                case "SW":  result = comparison.ToUpper().StartsWith(controlvalue.ToUpper());break;
                case "NSW": result = !comparison.ToUpper().StartsWith(controlvalue.ToUpper());break;
                default:    result = comparison == controlvalue;break;
            }
            return result;
        }

        private static bool DoComparison(string ComparisonType, DateTime comparison,DateTime controlvalue)
        {
            bool result = false;

            switch (ComparisonType)
            {  
               case "GT":  result = comparison > controlvalue;break;
               case "LT":  result = comparison < controlvalue;break;
               case "GTE": result = comparison >= controlvalue;break;
               case "LTE": result = comparison <= controlvalue;break;
               case "NE":  result = comparison != controlvalue;break;
               default:    result = comparison == controlvalue;break;
            }
            return result;
        }

        private static bool DoComparison(string ComparisonType, int comparison,int controlvalue)
        {
            bool result = false;

            switch (ComparisonType)
            {
               case "GT":  result = comparison > controlvalue; break;
               case "LT":  result = comparison < controlvalue; break;
               case "GTE": result = comparison >= controlvalue; break;
               case "LTE": result = comparison <= controlvalue; break;
               case "NE":  result = comparison != controlvalue; break;
               default:    result = comparison == controlvalue; break;
            }
            return result;
        }
    }
}
