using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Kx.Core.Exceptions;
using Kx.Core.Localisation;
using Kx.Core.Reflection;
using Kx.Core.Utility;
using Kx.Core.Web.BaseClass;
using Kx.Core.Web.ObjectTranslator;
using Kx.Core.Web.PageRenderer;
using Kx.Core.Web.PageRenderer.Utility;
using Kx.Core.Web.Providers;
using Kx.Core.Web.WebApplication;
using Kx.Objects.Common;
using Kx.Objects.Common.Web;
using System.Runtime.Serialization;
using System.Globalization;
using Kx.Core.Configuration;
using System.IO;
using System.Configuration;
using System.Text.RegularExpressions;
using System.Diagnostics;

namespace Kx.Core.Web.PageRenderer.Utility
{
    public class KxWebControl
    {
        private const string STR_DotCRLF = ".\r\n";
        protected Page currentPage = null;
        protected Control parentControl = null;
        protected string validationString = "";
        protected object classValue = null;
        protected object postbackValue = null;
        protected Type classValueType = null;
        protected KxWebAttribute attributes = null;
        protected KxListProvider listProvider = null;
        protected object webApp;
        protected bool ajaxPostback = false;
        
        protected bool isHidden = false;
        protected bool isDisabled = false;

        public bool AjaxPostback
        {
            get
            {
                return ajaxPostback;
            }
            set
            {
                ajaxPostback = value;
            }
        }

        public object ClassValue
        {
            get
            {
                return classValue;
            }
            set
            {
                if (classValue == value)
                    return;
                if (value.ToString() == "System.string[]")
                    System.Diagnostics.Debugger.Break();
                classValue = value;
            }
        }

        public object PostbackValue
        {
            get
            {
                if (attributes.UseClassValue != "" || attributes.UseClassValueWhenBlank)
                {
                    return GetPostbackOverride();
                }
                else
                {
                    return postbackValue;
                }
            }
            set
            {
                if (postbackValue == value)
                    return;
                postbackValue = value;
            }
        }

        public string PostbackValueString
        {
            get
            {
                try
                {
                    if (PostbackValue != null)
                    {
                        return PostbackValue.ToString();
                    }
                    else
                    {
                        return "";
                    }
                }
                catch
                {
                    return "";
                }
            }
        }

        public bool IsEditable
        {
            get
            { return GetIsEditable(); }
        }

        public Control ParentControl
        {
            get
            {
                return parentControl;
            }
            set
            {
                if (parentControl == value)
                    return;
                attributes = new KxWebAttribute(parentControl.ID);
                parentControl = value;
            }
        }
        public Type ValueType
        {
            get
            {
                if (attributes.OverrideType != null)
                {
                    return attributes.OverrideType;
                }
                else
                {
                    return classValueType;
                }
            }
            set
            {
                if (classValueType == value)
                    return;
                classValueType = value;
            }
        }
        public string DisplayName
        {
            get
            {
                return GetDisplayName();
            }
        }
        public KxWebAttribute Attributes
        {
            get
            {
                return attributes;
            }
            set
            {
                if (attributes == value)
                    return;
                attributes = value;
            }
        }
        public object CurrentValue
        {
            get
            {
                if (IsPostBack && PostbackValue != null)
                {
                    return PostbackValue;   
                }
                else
                {
                    object result = ClassValue;
                    if (attributes.DatePart != string.Empty)
                    {
                        if (attributes.DatePart == "D")
                        {
                            if (Convert.ToDateTime(result).Year < 1900)
                            {
                                result = 0;
                            }
                            else
                            {
                                result = (object)Convert.ToDateTime(result).Day;
                            }
                        }
                        else if (attributes.DatePart == "M")
                        {
                            if (Convert.ToDateTime(result).Year < 1900)
                            {
                                result = 0;
                            }
                            else
                            {
                                result = (object)Convert.ToDateTime(result).Month;
                            }
                        }
                        else if (attributes.DatePart == "Y")
                        {
                            result = (object)Convert.ToDateTime(result).Year;
                        }
                    }
                    return result;
                }
            }
        }

        public string CurrentValueString
        {
            get
            {
                try
                {
                    if (CurrentValue == null)
                    {
                        return "";
                    }
                    else
                    {
                        return CurrentValue.ToString();
                    }
                }
                catch
                {
                    return "";
                }
            }
        }
        public bool IsPostBack
        {
            get
            { return GetIsPostBack(); }
        }

        public bool Valid
        {
            get
            { return GetValid(null); }
        }
        public string ValidationString
        {
            get
            {
                GetValid(null);
                string replaced =
                    KxLocalisationManager.GetSubstitution(validationString.Replace("\r", "").Replace("\n", ""));
                if (replaced == validationString.Replace("\r", "").Replace("\n", ""))
                    return validationString;
                else
                    return replaced;
            }
        }

        private bool IsDescendant(Control parent, Control control)
        {
            bool result = false;

            if (control.Parent != null)
            {
                if (parent == control.Parent)
                {
                    result = true;
                }
                else
                {
                    return IsDescendant(parent, control.Parent);
                }
            }
            return result;
        }

        public bool IsUserControl
        {
            get
            {
                return GetIsUserControl();
            }
        }

        protected bool IsBlank(string value)
        {
            if (classValueType.Name == "Int16" || classValueType.Name == "Int32" || classValueType.Name == "Int64")
            {
                return value == "0" || value == "";
            }
            else
            {
                return value == "" || value == "0-0" || value == "0-0-0";
            }
        }

        public bool GetIsEditable()
        {
            bool result = true;

            if (parentControl.Visible)
            {
                if (KxReflection.GetPropertyInfo(parentControl, "ReadOnly") != null)
                {
                    if (((bool)KxReflection.GetPropertyObject(parentControl, "ReadOnly")))
                    {
                        result = false;
                    }
                }

                if (KxReflection.GetPropertyInfo(parentControl, "Enabled") != null)
                {
                    if (!((bool)KxReflection.GetPropertyObject(parentControl, "Enabled")))
                    {
                        result = false;
                    }
                }
            }
            else
            {
                result = false;
            }

            return result;
        }

        public bool GetValid(Control control)
        {
            validationString = "";

            if (IsPostBack && (control == null || IsDescendant(control, parentControl)))
            {
                if (attributes.Mandatory && IsBlank(PostbackValueString) && IsEditable && (attributes.DatePart == string.Empty))
                {
                    validationString += DisplayName + " " + KxLocalisationManager.GetSubstitution("is a required value")+STR_DotCRLF;
                }
                else if (attributes.Mandatory && IsBlank(PostbackValueString) && IsEditable && (attributes.DatePart != string.Empty))
                {
                    validationString += DisplayName + " " + KxLocalisationManager.GetSubstitution("must be a valid date") + STR_DotCRLF;
                }
                else if (attributes.MandatoryProperty != "" && GetMandatoryState() && IsBlank(PostbackValueString) && IsEditable && (attributes.DatePart == string.Empty))
                {
                    validationString += DisplayName + " " + KxLocalisationManager.GetSubstitution("is a required value") + STR_DotCRLF;
                }
                else if (attributes.CheckRequired && parentControl is CheckBox && parentControl.Visible)
                {
                    if (!((CheckBox)parentControl).Checked)
                    {
                        validationString += DisplayName + " " + KxLocalisationManager.GetSubstitution("must be checked before you can continue") + STR_DotCRLF;
                    }
                }

                
                if (attributes.PropertyName.ToUpper().IndexOf("EMAIL") > -1)
                {
                    if (PostbackValueString != "" && !KxPageUtility.ValidateEmail(PostbackValueString) && ValueType == typeof(string))
                    {
                        validationString += DisplayName + " "+KxLocalisationManager.GetSubstitution("must be a valid email address")+STR_DotCRLF;
                    }
                }
                else if (attributes.PropertyName.ToUpper().IndexOf("TELEPHONE") > -1)
                {
                    if (PostbackValueString != "" && !KxPageUtility.ValidatePhone(PostbackValueString) && ValueType == typeof(string))
                    {
                        validationString += DisplayName + " " + KxLocalisationManager.GetSubstitution("must be a valid phone number") + STR_DotCRLF;
                    }
                }
                else if (attributes.PropertyName.ToUpper().IndexOf("MOBILE") > -1)
                {
                    if (PostbackValueString != "" && !KxPageUtility.ValidatePhone(PostbackValueString) && ValueType == typeof(string))
                    {
                        validationString += DisplayName + " " + KxLocalisationManager.GetSubstitution("must be a valid mobile number") + STR_DotCRLF;
                    }
                }

                if (IsUserControl)
                {
                    if (!((BaseUserControl)parentControl).IsValid())
                    {
                        validationString += "   ";
                    }
                }

                if (validationString == "" && attributes.ValidateCustom != "")
                {
                    string customstring =  ValidateCustom(PostbackValueString, attributes.ValidateCustom.ToUpper());

                    if (!String.IsNullOrEmpty(customstring))
                    {
                        validationString += String.Format("{0} {1}", DisplayName, customstring);
                    }
                }

                if (attributes.MaxLength > 0 && ParentControl is TextBox)
                {
                    if (((TextBox)ParentControl).Text.Length > attributes.MaxLength)
                    {
                        validationString += String.Format("{0} {1} {2} {3}", DisplayName, KxLocalisationManager.GetSubstitution("must be no more than"), attributes.MaxLength, KxLocalisationManager.GetSubstitution("characters long. "));
                    }
                }

                if (attributes.MinLength > 0 && ParentControl is TextBox)
                {
                    if (((TextBox)ParentControl).Text.Length < attributes.MinLength)
                    {
                        validationString += String.Format("{0} {1} {2} {3}", DisplayName, KxLocalisationManager.GetSubstitution("must be more than"), attributes.MinLength, KxLocalisationManager.GetSubstitution("characters long. "));
                    }
                }

                if (parentControl is Button)
                {
                    if ((parentControl as Button).ID.Contains("RVADDPHOTO_DNPhoto")) // photo upload button
                    {
                        Control ctrl = KxPageUtility.FindControl(currentPage, "KX_DNPhoto");

                        if (ctrl != null)
                        {
                            int size = ((HtmlInputFile)ctrl).PostedFile.ContentLength;

                            FileInfo file = new FileInfo(((HtmlInputFile)ctrl).PostedFile.FileName);

                            bool isvalidImageType = ConfigurationManager.AppSettings["ImageTypes"] == null ? false : Regex.IsMatch(ConfigurationManager.AppSettings["ImageTypes"].ToString().ToLower(), file.Extension.TrimStart('.').ToLower());
                            int imageSize = ConfigurationManager.AppSettings["ImageMaxSize"] == null ? 0 : Convert.ToInt32(ConfigurationManager.AppSettings["ImageMaxSize"]);

                            if (!isvalidImageType)
                            {
                                validationString += KxLocalisationManager.GetSubstitution("Image file type invalid") + STR_DotCRLF; 
                            }
                            if(size > imageSize ){
                                validationString += KxLocalisationManager.GetSubstitution("Image exceeds maximum size in bytes of ") + imageSize + STR_DotCRLF;
                            }
                            if (size < 1)
                            {
                                validationString += KxLocalisationManager.GetSubstitution("No image attached") + STR_DotCRLF; 
                            }

                        }
                    }

                    // Extended to include validation on documents
                    if ((parentControl as Button).ID.Contains("RVADDDOCUMENT")) // photo upload button
                    {
                        Control ctrl = KxPageUtility.FindControl(currentPage, "KX_DN" + attributes.DocumentName);

                        if ((ctrl != null))
                        {
                            if (ctrl is System.Web.UI.HtmlControls.HtmlInputFile)
                            {
                                System.Web.UI.HtmlControls.HtmlInputFile inp = (System.Web.UI.HtmlControls.HtmlInputFile)ctrl;
                                HttpPostedFile myFile = inp.PostedFile;
                                if (myFile.FileName != "")
                                {
                                    int maxDocumentSize = KxDbConfiguration.GetConfigurationInt("DocumentUploadMaxSize", myFile.ContentLength + 1000);
                                    int nFileLen = myFile.ContentLength;
                                    if (nFileLen > 0 && nFileLen <= maxDocumentSize)
                                    {
                                        string filename = System.IO.Path.GetFileName(myFile.FileName);
                                        string extension = System.IO.Path.GetExtension(myFile.FileName);
                                        string validDocumentTypes = KxDbConfiguration.GetConfiguration("DocumentUploadTypes");
                                        bool isvalidDocumentType = string.IsNullOrWhiteSpace(validDocumentTypes) ? true : Regex.IsMatch(validDocumentTypes.ToLower(), (extension ?? "").TrimStart('.').ToLower());
                                        if (!isvalidDocumentType)
                                        {
                                            validationString += KxLocalisationManager.GetSubstitution("File format incorrect, please attach a valid type") + STR_DotCRLF;
                                        }
                                    }
                                    else
                                    {
                                        validationString += KxLocalisationManager.GetSubstitution("File size must be greater than zero and less than maximum size in bytes of ") + maxDocumentSize + STR_DotCRLF;
                                    }
                                }
                                else
                                {
                                    validationString += KxLocalisationManager.GetSubstitution("No file name provided") + STR_DotCRLF;
                                }
                            }
                        }
                    }

                }

                validationString += GetExtendedValid();


                if (validationString == "" && attributes.ValidationType != "" && attributes.ComparisonWith != "" && IsEditable)
                {
                    if (!KxWebAttributeComparison.DoValidationComparison(attributes, PostbackValueString, attributes.ComparisonWith))
                    {
                        validationString += String.Format("{0} {1}", DisplayName, KxWebAttributeComparison.GetValidationText(attributes, attributes.ComparisonWith));
                    }
                }

                if (validationString == "" && attributes.CompareWithProperty != "")
                {
                    object comparisonproperty = null;

                    if (attributes.TertiaryObject != null)
                    {
                        comparisonproperty = KxReflection.GetPropertyObject(attributes.TertiaryObject, attributes.CompareWithProperty);
                    }

                    if (comparisonproperty == null && attributes.SecondaryObject != null)
                    {
                        comparisonproperty = KxReflection.GetPropertyObject(attributes.SecondaryObject, attributes.CompareWithProperty);
                    }

                    if (comparisonproperty == null && attributes.PrimaryObject != null)
                    {
                        comparisonproperty = KxReflection.GetPropertyObject(attributes.PrimaryObject, attributes.CompareWithProperty);
                    }

                    if (comparisonproperty != null)
                    {
                        if (PostbackValueString != comparisonproperty.ToString())
                        {
                            validationString += String.Format("{0} {1} {2}", DisplayName, KxLocalisationManager.GetSubstitution("does not match"), MakeDisplayName(attributes.CompareWithProperty));
                        }
                    }

                }

              
                if (PostbackValueString.Trim() != "" && IsEditable )
                {
                    if (attributes.OverrideType != null )
                    {
						String sError = "";
						CultureInfo culture = new CultureInfo(KxDbConfiguration.Culture, true);
                        Boolean invalidDate = false;

						try
                        {
                            if (attributes.DatePart != string.Empty)
                            {
                                object obj = KxReflection.GetPropertyObject(attributes.PrimaryObject, attributes.PropertyName);
                                if (obj is DateTime)
                                {
                                    if (Convert.ToDateTime(obj).Year < 1900)
                                    {
                                        invalidDate = true;
                                    }
                                    PostbackValue = Convert.ToDateTime(obj).ToShortDateString();
                                }

                            }

							//KxLogger.LogActivity(this, "Convert Date", PostbackValue.ToString(), culture.DateTimeFormat.ShortDatePattern.ToString());
                            
                            object value = Convert.ChangeType(PostbackValue, attributes.OverrideType);

                            if (attributes.OverrideType.Name == "DateTime")
                            {
								try
                                {
                                    DateTime valuedt = Convert.ToDateTime(value);
									if (valuedt.Year < 1900)
                                    {
                                        validationString += DisplayName + KxLocalisationManager.GetSubstitution("must be in format dd/mm/yyyy and have a year greater than 1900") + STR_DotCRLF;
                                    }
                                    else if (invalidDate)
                                    {
                                        validationString += DisplayName + KxLocalisationManager.GetSubstitution("must be a valid date") + STR_DotCRLF;
                                    }
                                }
								catch (Exception ex)
                                {
									sError = ex.Message;
                                    validationString += DisplayName + KxLocalisationManager.GetSubstitution("must be in format dd/mm/yyyy and have a year greater than 1901") + STR_DotCRLF;
                                }
                            }

                        }
						catch (Exception ex)
                        {
							sError = ex.Message;
                            if (attributes.OverrideType.Name == "DateTime")
                            {
                                validationString += DisplayName + KxLocalisationManager.GetSubstitution("must be in format dd/mm/yyyy and have a year greater than 1902") + STR_DotCRLF;
                            }
                            else
                            {
                                validationString += DisplayName + " " + KxLocalisationManager.GetSubstitution("must be of type") + ": " + attributes.OverrideType.Name + STR_DotCRLF;
                            }
                        }
						//KxLogger.LogActivity(this, "Convert Date Done", validationString, sError);
					}
                    else if (!ValueType.IsGenericType)
                    {
                        try
                        {
                            if (!(ParentControl is RadioButton))
                            {
                                object value = Convert.ChangeType(postbackValue, ValueType);
                            }
                        }
                        catch
                        {
                            validationString += DisplayName + " " + KxLocalisationManager.GetSubstitution("must be of type") + ": " + ValueType.Name + STR_DotCRLF;
                        }
                    }
                }

            }

            try
            {
                if(!(parentControl is Literal) && !(parentControl is HiddenField) && !(parentControl is PlaceHolder)) { 
                if (validationString == string.Empty && (((WebControl)parentControl).CssClass ?? string.Empty).Contains("KxError"))
                {
                    try
                    {
                        ((WebControl)parentControl).CssClass = ((WebControl)parentControl).CssClass.Replace("KxError", "").Trim();
                    }
                    catch { }
                }
                else if (validationString != string.Empty)
                {
                    try
                    {
                        if (!(((WebControl) parentControl).CssClass ?? string.Empty).Contains("KxError"))
                        {
                            ((WebControl)parentControl).CssClass = (String.Format("{0} KxError", ((WebControl)parentControl).CssClass)).Trim();
                        }
                    }
                    catch { }
                }
                }
            }
            catch { }

            return validationString == "";
        }

        public KxWebControl(object _webApp, Control control, Page _currentPage, KxListProvider _listProvider, bool ajaxPostBack)
        {
            this.AjaxPostback = ajaxPostBack;

            webApp = _webApp;
            currentPage = _currentPage;
            listProvider = _listProvider;
            parentControl = control;
            attributes = new KxWebAttribute(control.ID);

            InitialiseValues();

            postbackValue = GetPostBackValue();
            ProcessControls();

            SaveChanges();
        }

        public KxWebControl(object _webApp, Control control, Page _currentPage, KxListProvider _listProvider)
            : this(_webApp, control, _currentPage, _listProvider,false)
        {
          
        }

        public object GetPostBackValue()
        {
            object result = null;

            if (currentPage != null)
            {
                if (IsPostBack)
                {
                    if (parentControl is RadioButton)
                    {
                        string temp = GetGroupValue(((RadioButton)parentControl).GroupName);

                        KxWebAttribute parentattr = new KxWebAttribute(temp);

                        if (parentattr.SelectedProperty != string.Empty)
                        {
                            result = GetSecondaryValue(parentattr,parentattr.SelectedProperty);
                        }
                        else
                        {
                            result = parentattr.SelectedValue;
                        }
                    }
                    else if (parentControl is CheckBoxList)
                    {
                        result = GetCheckedList();
                    }
                    else
                    {
                        try
                        {
                            result = HttpContext.Current.Request[parentControl.UniqueID];
                        }
                        catch { }
                    }
                }
            }
           
            return result;
        }

    
        public void ProcessControls()
        {

            try
            {
                WebControl webparentcontrol = parentControl as WebControl;
                    if (webparentcontrol != null) {
                    isHidden = !((WebControl)parentControl).Visible && !attributes.IgnoreHidden;
                    isDisabled = !((WebControl)parentControl).Enabled;

                    ((WebControl)parentControl).Enabled = true;
                    ((WebControl)parentControl).Visible = true;
                }
            }
            catch { }

            switch (parentControl.GetType().Name)
            {
                case "TextBox": ProcessTextBox(); break;
                case "DropDownList": ProcessDropDownList(); break;
                case "Button": ProcessButton(); break;
                case "LinkButton": ProcessLinkButton(); break;
                case "ImageButton": ProcessButton(); break;
                case "RadioButton": ProcessRadioButton(); break;
                case "RadioButtonList": ProcessRadioButtonList(); break;
                case "BulletedList": ProcessBulletedList(); break;
                case "Label": ProcessLabel(); break;
                case "TableHeaderCell":
                case "TableCell": ProcessTableCell(); break;
                case "TableRow": ProcessTableRow(); break;
                case "ListBox": ProcessListBox(); break;
                case "HyperLink": ProcessHyperLink(); break;
                case "CheckBox": ProcessCheckBox(); break;
                case "CheckBoxList": ProcessCheckBoxList(); break;
                case "PlaceHolder": ProcessPlaceHolder(); break;
                case "Table": ProcessTable(); break;
                case "Image": ProcessImage(); break;
                case "Literal": ProcessLiteral(); break;
                case "Panel": ProcessPanel(); break;
                default: ProcessGeneric(); break;
            }
        }

        public void ProcessBulletedList()
        {
            BulletedList listbox = (BulletedList)parentControl;
            try
            {
                listbox.Items.Clear();
                if (attributes.LookUpName != "")
                {
                    listbox.Items.Add(new ListItem("", ""));
                    listProvider.PopulateList(listbox.Items, attributes.LookUpName, null, attributes.LookupCache);
                }
                else if (attributes.LookUpArray != "")
                {
                    string[] array = (string[])GetSupportingProperty(attributes.LookUpArray);
                    AddItemToList(array, listbox.Items);
                }
                else if (attributes.LookUpXml != "")
                {
                    listbox.Items.Add(new ListItem("", ""));
                    listProvider.PopulateXmlList(listbox.Items, attributes.LookUpXml);
                }
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity("WebControl", "BulletedList", ex.Message, attributes.Id);
            }

            try
            {

                string Temp = CurrentValueString + ",";

                foreach (ListItem li in listbox.Items)
                {
                    if (Temp.IndexOf(li.Value + ",") > -1)
                    {
                        li.Selected = true;
                    }
                }
            }
            catch
            {
            }
        }

        private void ProcessPanel()
        {
            try
            {
                ((Panel)parentControl).Visible = GetVisibleState();
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity("WebControl", "ProcessPlaceHolder", ex.Message, attributes.Id);
            }
        }

        public void SaveChanges()
        {
            if (IsPostBack)
            {
                if (IsUserControl)
                {
                    ((BaseUserControl)ParentControl).Save();
                }
                else if (isHidden && attributes.ResetWhenHidden)
                {
                    KxReflection.SetProperty(attributes.PrimaryObject, attributes.PropertyName, GetResetValue());
                }
                else if (!isDisabled && !isHidden && PostbackValue != classValue)
                {
                    if (parentControl.GetType().Name == "CheckBoxList")
                    {
                        if (IsEditable)
                        {
                            KxReflection.SetProperty(attributes.PrimaryObject, attributes.PropertyName, PostbackValue);
                        }
                    }
                    else if (parentControl.GetType().Name == "CheckBox")
                    {
                        if (IsEditable || attributes.ForcedUpdate)
                        {
                            KxReflection.SetProperty(attributes.PrimaryObject, attributes.PropertyName, PostbackValue);
                        }
                    }
                    else if (parentControl.GetType().Name == "DropDownList")
                    {
                        if (IsEditable && (attributes.DatePart != string.Empty))
                        {
                            DateTime? dtProp = new DateTime(1899, 1, 1);
                            try
                            {
                                object obj = KxReflection.GetPropertyObject(attributes.PrimaryObject, attributes.PropertyName);
                                if (obj is DateTime)
                                {
                                    dtProp = Convert.ToDateTime(obj);
                                    int partValue = Convert.ToInt32(PostbackValue);
                                    if (attributes.DatePart == "D")
                                    {
                                        dtProp = new DateTime(dtProp.Value.Year, dtProp.Value.Month, partValue);

                                    }
                                    else
                                        if (attributes.DatePart == "M")
                                        {
                                            dtProp = new DateTime(dtProp.Value.Year, partValue, dtProp.Value.Day);

                                        }
                                        else
                                            if (attributes.DatePart == "Y")
                                            {
                                                dtProp = new DateTime(partValue, dtProp.Value.Month, dtProp.Value.Day);
                                            }
                                }
                                KxReflection.SetProperty(attributes.PrimaryObject, attributes.PropertyName, (object)dtProp);
                            }
                            catch (Exception)
                            {
                                KxReflection.SetProperty(attributes.PrimaryObject, attributes.PropertyName, (object)new DateTime(1899, 1, 1));
                                PostbackValue = "";
                                //KxLogger.LogActivity("WebControl", "SaveChanges", ex.Message, attributes.Id);
                            }
                        }
                        else
                        {
                            KxReflection.SetProperty(attributes.PrimaryObject, attributes.PropertyName, PostbackValue);
                        }
                    }

                    else
                    {
                        KxReflection.SetProperty(attributes.PrimaryObject, attributes.PropertyName, PostbackValue);
                    }
                }
            }
        }

        private object GetResetValue()
        {
            object result = null;

            if (ValueType.Name == "String" || ValueType.Name == "Char")
            {
                result = "";
            }
            else if (ValueType.Name == "Int32" || ValueType.Name == "Int16" || ValueType.Name == "Int64")
            {
                result = 0;
            }

            return result;
        }

        public void ProcessControl()
        {
            //pha
            //try
            //{
            //    if (((Control)parentControl).ClientID.StartsWith("MainContent"))
            //    {
            //        System.Diagnostics.Debugger.Break();
            //    }

            //}
            //catch { }
            try
            {
                if (!(parentControl is Literal) && !(parentControl is HiddenField))
                {
                    
                    ((WebControl)parentControl).Enabled = GetReadOnlyState();
                }
            }
            catch
            {}

            if (parentControl is Literal)
            {
                try
                {
                    ((Literal)parentControl).Visible = GetVisibleState();
                }
                catch
                {
                }
            }
            else
            {
                try
                {
                    if(!(parentControl is HiddenField)) { 
                    ((WebControl)parentControl).Visible = GetVisibleState();
                    }
                }
                catch
                {
                    try
                    {
                        ((HtmlControl)parentControl).Visible = GetVisibleState();
                    }
                    catch
                    {

                    }
                }
            }

            try
            {
                if (attributes.StyleSuffixProperty != "")
                {
                    string suffixsuffix = GetSecondaryPropertyValue(attributes.StyleSuffixProperty);

                    ((WebControl)parentControl).CssClass = attributes.StyleSuffix + suffixsuffix;
                }
                else if (attributes.StyleProperty != "" && KxReflection.GetPropertyInfo(parentControl, "CssClass") != null)
                {
                    if (GetMergeStateFlag(attributes.StyleProperty, false))
                    {
                        if (!((WebControl)parentControl).CssClass.Contains(attributes.StyleSuffix))
                        {
                            ((WebControl)parentControl).CssClass += attributes.StyleSuffix;
                        }
                    }
                    else
                    {
                        ((WebControl)parentControl).CssClass = ((WebControl)parentControl).CssClass.Replace(attributes.StyleSuffix, "");
                    }
                }
                if (!(parentControl is Literal) && !(parentControl is HiddenField))
                {
                    ((WebControl)parentControl).Visible = GetVisibleState();
                }// WebApp.ObjectTranslator.GetMergeVisibleStateFlag(attributes);
            }
            catch 
            {
            
            }
        }

        public virtual bool GetReadOnlyState()
        {
            return false;
        }

        public virtual bool GetVisibleState()
        {
            return true;
        }

        public virtual bool GetMergeStateFlag(string StateProperty, bool defaultvalue)
        {
            return false;
        }

        public virtual bool GetMandatoryState()
        {
            return false;
        }


        public void ProcessGeneric()
        {
            try
            {
                if (IsUserControl)
                {
                    ((BaseUserControl)ParentControl).Setup();
                }
                else if (parentControl.GetType().GetProperty("Text") != null)
                {
                    KxReflection.SetProperty(parentControl, "Text", CurrentValue);
                }
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity("WebControl", "ProcessGeneric", ex.Message, attributes.Id);
            }


            ProcessControl();
        }

        public void ProcessButton()
        {
            //if (((Button)(parentControl)).Text.StartsWith("Inventory"))
            //{
            //    System.Diagnostics.Debugger.Break();
            //}
            if (attributes.DefaultButton)
            {
                try
                {
                    parentControl.Page.Form.DefaultButton = parentControl.UniqueID;
                }
                catch { }
            }

            ProcessControl();
        }

        public void ProcessLinkButton()
        {
            if (attributes.PropertyName != "")
            {
                ((LinkButton)parentControl).Text = CurrentValueString;
            }
            ProcessControl();
        }

        public void ProcessImage()
        {
            if (attributes.PropertyName != "")
            {
                ((System.Web.UI.WebControls.Image)parentControl).ImageUrl = CurrentValueString;
            }
            ProcessControl();
        }

        public void ProcessCheckBox()
        {
            try
            {
                CheckBox check = ((CheckBox)parentControl);

                if (IsPostBack)
                {
                    if (attributes.ForcedValue && postbackValue == null)
                    {
                        ProcessControl();

                        if (check.Visible)
                        {
                            if (check.Enabled)
                            {
                                if (attributes.UncheckedValue != "")
                                {
                                    postbackValue = attributes.UncheckedValue;
                                }
                                else
                                {
                                    postbackValue = false;
                                }
                            }
                            else
                            {
                                if (SafeBooleanCheck(CurrentValueString))
                                {
                                    postbackValue = attributes.CheckedValue;
                                }
                                ((CheckBox)parentControl).Checked = postbackValue.ToString().ToUpper() == attributes.CheckedValue.ToString().ToUpper();
                            }
                        }
                    }
                    else if (attributes.CheckedValue != "")
                    {
                        try
                        {
                            if (check.Checked || CurrentValueString == "on" || SafeBooleanCheck(CurrentValueString))
                            {
                                postbackValue = attributes.CheckedValue;
                            }
                            else
                            {
                                if (attributes.UncheckedValue != "")
                                {
                                    postbackValue = attributes.UncheckedValue;
                                }
                                else
                                {
                                    postbackValue = "";
                                }
                            }
                        }
                        catch
                        {
                            postbackValue = "";
                        }

                        if (attributes.ForcedValue)
                        {
                            ((CheckBox)parentControl).Checked = postbackValue.ToString().ToUpper() == attributes.CheckedValue.ToString().ToUpper();
                        }
                    }
                    else
                    {
                        if (postbackValue == null && !isHidden && !isDisabled)
                        {
                            if (attributes.UncheckedValue != "")
                            {
                                postbackValue = attributes.UncheckedValue;
                            }
                            else
                            {
                                postbackValue = false;
                            }
                        }
                        else
                        {
                            postbackValue = ((CheckBox)parentControl).Checked || CurrentValueString == "on" || SafeBooleanCheck(CurrentValueString);
                        }

                        ((CheckBox)parentControl).Checked = (bool)postbackValue;
                    }
                }
                else
                {
                    try
                    {
                        ((CheckBox)parentControl).Checked = (CurrentValueString == attributes.CheckedValue && attributes.CheckedValue != "") || SafeBooleanCheck(CurrentValueString);
                    }
                    catch
                    {
                        ((CheckBox)parentControl).Checked = false;
                    }
                }
            }
            catch 
            {
               
            }

            ProcessControl();
        }


        private bool SafeBooleanCheck(string value)
        {
            try
            {
                if (value == "" || value == "0")
                {
                    return false;
                }
                else
                {
                    return Boolean.Parse(value);
                }
            }
            catch
            {
                return false;
            }
        }

        public void ProcessCheckBoxList()
        {
            CheckBoxList chklist = (CheckBoxList)parentControl;

            try
            {
                if (chklist.Items.Count == 0 || Attributes.RefreshList)
                {
                    chklist.Items.Clear();

                    if (attributes.LookUpName != "")
                    {
                        //chklist.Items.Add(new ListItem("", ""));
                        listProvider.PopulateList(chklist.Items, attributes.LookUpName, null, attributes.LookupCache);
                    }
                    else if (attributes.LookUpArray != "")
                    {
                        string[] array = (string[])GetSupportingProperty(attributes.LookUpArray);
                        if (array != null)
                        {
                            foreach (string str in array)
                            {
                                ListItem li = new ListItem(str, str);
                                li.Selected = CurrentValueString.IndexOf(str + ",") > -1;
                                chklist.Items.Add(li);
                            }
                        }
                    }
                    else if (attributes.LookUpXml != "")
                    {
                        listProvider.PopulateXmlList(chklist.Items, attributes.LookUpXml);
                    }
                    else if (attributes.LookUpItems != "")
                    {
                        ListItemCollection coll = (ListItemCollection)GetSupportingProperty(attributes.LookUpItems);

                        foreach (ListItem li in coll)
                        {
                            li.Selected = CurrentValueString.IndexOf(li.Value + ",") > -1;
                            chklist.Items.Add(li);
                        }
                    }
                }

                string Temp = CurrentValueString + ",";

                foreach (ListItem li in chklist.Items)
                {
                    if (Temp.IndexOf(li.Value + ",") > -1)
                    {
                        li.Selected = true;
                    }
                }
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity("WebControl", "ProcessCheckBoxList", ex.Message, attributes.Id);
            }

            ProcessControl();

        }

        public string GetCheckedList()
        {
            string result = "";

            CheckBoxList check = (CheckBoxList)parentControl;

            for (int idx = 0; idx < check.Items.Count; idx++)
            {
                object val = currentPage.Request[check.UniqueID + "$" + idx.ToString()];
                if (val != null)
                {
                    result += check.Items[idx].Value + ",";
                }
            }
            return result;
        }

        public void ProcessHyperLink()
        {
            string text = CurrentValueString;

            if (text.ToLower().Contains("mailto"))
            {
                text = text.ToLower().Replace("mailto:", "");
            }

            
            if (CurrentValueString != "")
            {
                if (attributes.Hyperlink != "")
                {
                    if (((HyperLink)parentControl).Text == "")
                    {
                        ((HyperLink)parentControl).Text = text;
                    }

                    string url = KxReflection.GetPropertyString(attributes.SecondaryObject, attributes.Hyperlink);

                    if (url.IndexOf("@") > -1 )
                    {
                        ((HyperLink)parentControl).NavigateUrl = "mailto:" + url;
                    }
                    else
                    {
                        ((HyperLink)parentControl).NavigateUrl = url;
                    }
                }
                else
                {
                    if (((HyperLink)parentControl).Text == "")
                    {
                        ((HyperLink)parentControl).Text = text;
                    }

                    if (CurrentValueString.IndexOf("@") > -1 && (!CurrentValueString.ToUpper().StartsWith("MAILTO")))
                    {
                        ((HyperLink)parentControl).NavigateUrl = "mailto:" + CurrentValueString;
                    }
                    else
                    {
                        ((HyperLink)parentControl).NavigateUrl = CurrentValueString;
                    }
                }
            }
            ProcessControl();
        }

        public void ProcessLabel()
        {
            if (parentControl.ID.ToUpper() != "KXERROR")
            {
                try
                {
                    if (CurrentValue != null)
                    {
                        if (KxDbConfiguration.GetConfigurationBool("DoNotMergeLabelText", false))
                        {
                            ((Label)parentControl).Text = CurrentValueString;
                        }
                        else
                        {
                            ((Label)parentControl).Text = MergeTags(CurrentValueString);
                        }
                    }
                }
                catch (Exception ex)
                {
                    KxLogger.LogActivity("WebControl", "ProcessLabel", ex.Message, attributes.Id);
                }
                ProcessControl();
            }
            else

            {
                parentControl.Visible = !isHidden;
            }
        }

        public void ProcessLiteral()
        {
            try
            {
                if (ClassValue != null)
                {
                    if (!String.IsNullOrEmpty(ClassValue.ToString()))
                    {
                        ((Literal)parentControl).Text = CurrentValueString;
                    }
                }

                if (KxBase64.IsB64Encoded(((Literal)parentControl).Text))
                {
                    ((Literal)parentControl).Text = KxBase64.Base64Decode(((Literal)parentControl).Text);
                }

                if (!KxPageUtility.IsLocal)
                {
                    if (this.Attributes.AcademicYear > 0)
                    {
                        ((Literal)parentControl).Text = ((Literal) parentControl).Text.Replace("{KX_", "{KX_AY" + this.Attributes.AcademicYear + "_");
                    }
                    if (this.Attributes.AcademicYearSession > 0)
                    {
                        ((Literal)parentControl).Text = ((Literal) parentControl).Text.Replace("{KX_", "{KX_AS" + this.Attributes.AcademicYearSession + "_");
                    }
                    if (this.Attributes.SpecificApplicationGuid != null)
                    {
                        ((Literal)parentControl).Text = ((Literal) parentControl).Text.Replace("{KX_", "{KX_AG" + this.Attributes.SpecificApplicationGuid.Value.ToString("N") + "_");
                    }
                    if (this.Attributes.GenericStringIndex != null)
                    {
                        ((Literal)parentControl).Text = ((Literal) parentControl).Text.Replace("{KX_", "{KX_GS" + this.Attributes.GenericStringIndex + "_");
                    }
                    if (this.Attributes.GenericIndex > 0)
                    {
                        ((Literal)parentControl).Text = ((Literal) parentControl).Text.Replace("{KX_", "{KX_GI" + this.Attributes.GenericIndex.ToString() + "_");
                    }
                    ((Literal)parentControl).Text = MergeTags(((Literal)parentControl).Text);
                }

            }
            catch (Exception ex)
            {
                KxLogger.LogActivity("WebControl", "ProcessLiteral", ex.Message, attributes.Id);
            }
            ProcessControl();
        }

        public void ProcessTableCell()
        {
            try
            {
                TableCell cell = (TableCell)parentControl;
                TableRow row = ((TableRow)cell.Parent);
                Table table = ((Table)row.Parent);

                ColorConverter cconv = new ColorConverter();

                if (!String.IsNullOrEmpty(attributes.ColourOdd))
                {
                    if (table.Rows.GetRowIndex(row) % 2 != 0)
                    {
                        cell.BackColor = (Color)cconv.ConvertFromString(attributes.ColourOdd);
                    }
                }

                if (!String.IsNullOrEmpty(attributes.ColourEven))
                {
                    if (table.Rows.GetRowIndex(row) % 2 == 0)
                    {
                        cell.BackColor = (Color)cconv.ConvertFromString(attributes.ColourEven);
                    }
                }

                if (!String.IsNullOrEmpty(attributes.StyleOdd))
                {
                    if (table.Rows.GetRowIndex(row) % 2 != 0)
                    {
                        cell.CssClass = attributes.StyleOdd;
                    }
                }

                if (!String.IsNullOrEmpty(attributes.StyleEven))
                {
                    if (table.Rows.GetRowIndex(row) % 2 == 0)
                    {
                        cell.CssClass = attributes.StyleEven;
                    }
                }

                if (CurrentValue != null)
                {
                    cell.Text = CurrentValueString;
                }
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity("WebControl", "ProcessTableCell", ex.Message, attributes.Id);
            }
            ProcessControl();
        }

        public void ProcessTableRow()
        {
            try
            {
                TableRow row = (TableRow)parentControl;
                Table table = ((Table)row.Parent);

                ColorConverter cconv = new ColorConverter();

                if (!String.IsNullOrEmpty(attributes.ColourOdd))
                {
                    if (table.Rows.GetRowIndex(row) % 2 != 0)
                    {
                        row.BackColor = (Color)cconv.ConvertFromString(attributes.ColourOdd);
                    }
                }

                if (!String.IsNullOrEmpty(attributes.ColourEven))
                {
                    if (table.Rows.GetRowIndex(row) % 2 == 0)
                    {
                        row.BackColor = (Color)cconv.ConvertFromString(attributes.ColourEven);
                    }
                }

                if (!String.IsNullOrEmpty(attributes.StyleOdd))
                {
                    if (table.Rows.GetRowIndex(row) % 2 != 0)
                    {
                        row.CssClass = attributes.StyleOdd;
                    }
                }

                if (!String.IsNullOrEmpty(attributes.StyleEven))
                {
                    if (table.Rows.GetRowIndex(row) % 2 == 0)
                    {
                        row.CssClass = attributes.StyleEven;
                    }
                }
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity("WebControl", "ProcessTableRow", ex.Message, attributes.Id);
            }

            ProcessControl();
        }

        public void ProcessListBox()
        {
            ListBox listbox = (ListBox)parentControl;
            try
            {
                if (listbox.Items.Count == 0)
                {
                    if (attributes.LookUpName != "")
                    {
                        listbox.Items.Add(new ListItem("", ""));
                        listProvider.PopulateList(listbox.Items, attributes.LookUpName, null, attributes.LookupCache);
                    }
                    else if (attributes.LookUpArray != "")
                    {
                        string[] array = (string[])GetSupportingProperty(attributes.LookUpArray);
                        AddItemToList(array, listbox.Items);
                    }
                    else if (attributes.LookUpXml != "")
                    {
                        listbox.Items.Add(new ListItem("", ""));
                        listProvider.PopulateXmlList(listbox.Items, attributes.LookUpXml);
                    }
                }
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity("WebControl", "ProcessListBox1", ex.Message, attributes.Id);
            }

            try
            {

                string Temp = CurrentValueString + ",";

                foreach (ListItem li in listbox.Items)
                {
                    if (Temp.IndexOf(li.Value + ",") > -1)
                    {
                        li.Selected = true;
                    }
                }
            }
            catch
            {
            }
        }

        public void ProcessRadioButton()
        {
            try
            {
                string Selectedvalue = attributes.SelectedValue;

                if (attributes.SelectedProperty != string.Empty)
                {
                    object obj = GetSupportingProperty(attributes.SelectedProperty);
                    if (obj != null)
                    {
                        Selectedvalue = obj.ToString();
                    }
                }

                if (Selectedvalue == CurrentValueString)
                {
                    PostbackValue = Selectedvalue;
                }

                ((RadioButton)parentControl).Checked = Selectedvalue == CurrentValueString || parentControl.ID == CurrentValueString;

            }
            catch (Exception ex)
            {
                KxLogger.LogActivity("WebControl", "ProcessRadioButton", ex.Message, attributes.Id);
            }
            ProcessControl();
        }

        public void ProcessRadioButtonList()
        {
            RadioButtonList rblist = (RadioButtonList)parentControl;

            if (attributes.CustomHandler)
            {
                if (attributes.LookUpName != "")
                {
                    ProcessRadioButtonLookupCustomHandler();
                }
                else
                {
                    ProcessCustomHandler();
                }
            }
            else
            {
                if (rblist.Items.Count == 0 || Attributes.RefreshList)
                {
                    rblist.Items.Clear();

                    try
                    {
                        if (attributes.LookUpName != "")
                        {
                            listProvider.PopulateList(rblist.Items, attributes.LookUpName, null, attributes.LookupCache);
                        }
                        else if (attributes.LookUpArray != "")
                        {
                            string[] array = (string[])GetSupportingProperty(attributes.LookUpArray);
                            AddItemToList(array, rblist.Items);
                        }
                        else if (attributes.LookUpXml != "")
                        {
                            listProvider.PopulateXmlList(rblist.Items, attributes.LookUpXml);
                        }
                        else if (attributes.LookUpItems != "")
                        {
                            ListItemCollection coll = (ListItemCollection)GetSupportingProperty(attributes.LookUpItems);
                            AddItemToList(coll, rblist.Items);
                        }
                    }
                    catch (Exception)
                    {
                        //KxLogger.LogActivity("WebControl", "ProcessRadioButtonList", ex.Message, attributes.Id);
                    }
                }

                try
                {
					rblist.SelectedValue = CurrentValueString;
                    if (rblist.SelectedIndex < 0 && attributes.DefaultIndex > -1)
                    {
                        rblist.SelectedIndex = attributes.DefaultIndex;
                    }
				}
				catch (Exception)
                {
					//KxLogger.LogActivity("WebControl", "ProcessRadioButtonList", ex.Message, attributes.Id);
					if (attributes.ForceBlank || attributes.Mandatory)
					{
						try
						{
							rblist.SelectedIndex = -1;
							PostbackValue = "";
						}
						catch (Exception)
						{
							//KxLogger.LogActivity("WebControl", "ProcessRadioButtonLookupCustomHandler2", ex.Message, attributes.Id);
						}
					}
				}
				try
				{
					if (rblist.SelectedValue == CurrentValueString)
					{
						PostbackValue = rblist.SelectedValue;
					}
				}
				catch { }
				ProcessControl();
            }
        }

        protected string GetGroupValue(string GroupName)
        {
            string result = "";
            try
            {
                foreach (string req in currentPage.Request.Form.AllKeys)
                {
                    if (req != null && req.IndexOf("$") > -1)
                    {
                        string[] array = req.Split('$');
                        if (array != null)
                        {
                            if (array[array.GetUpperBound(0)] == GroupName)
                            {
                                result = currentPage.Request[req];
                                break;
                            }
                        }
                    }
                }
            }
            catch 
            {
            }
            return result;
        }

        public void ProcessPlaceHolder()
        {
            if (ClassValue != null && ClassValue is Array)
            {
                if (Attributes.TableAction.ToUpper() == "CLONE")
                {
                    CloneTable();
                }
                else if (Attributes.PlaceHolderAction.ToUpper() == "CLONE")
                {
                     ClonePlaceHolder();
                }
            }
            try
            {
                bool visible = GetVisibleState();
                ((PlaceHolder)parentControl).Visible = visible;// WebApp.ObjectTranslator.GetMergeVisibleStateFlag(attributes);
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity("WebControl", "ProcessPlaceHolder", ex.Message, attributes.Id);
            }
        }

        public void ProcessTable()
        {
            try
            {
                if (ClassValue != null && ClassValue is Array)
                {
                    if (Attributes.TableAction.ToUpper() == "ADDROW")
                    {
                        CloneTableRows();
                        ((Table)parentControl).Visible = true;
                    }
                    else
                    {
                        ((Table)parentControl).Rows.Clear();

                        foreach (TableRow row in (TableRow[])ClassValue)
                        {
                            ((Table)parentControl).Rows.Add(row);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity("WebControl", "ProcessTable", ex.Message, attributes.Id);
            }

            ProcessControl();
        }

        public void ProcessTextBox()
        {
            ((TextBox)parentControl).Text = CurrentValueString;
            ProcessControl();
        }

        public void ProcessDropDownList()
        {
            DropDownList dropdown = (DropDownList)parentControl;

            if (attributes.CustomHandler)
            {
                if (attributes.LookUpName != "")
                {
                    ProcessLookupCustomHandler();
                }
                else if (attributes.LookUpXml != string.Empty)
                {
                    ProcessLookupXmlCustomHandler();
                }
                else
                {
                    ProcessCustomHandler();
                }
            }
            else
            {
                if (
                        (dropdown.Items.Count < 2 || attributes.RefreshList) && 
                        (
                            attributes.LookUpName != "" || attributes.LookUpArray != "" || attributes.LookUpItems != "" || attributes.NumericList > 0 || attributes.LookUpXml != ""
                        )
                    )
                {
                    dropdown.Items.Clear();
                    try
                    {
                        if (attributes.LookUpName != "")
                        {
                            AddDefaultListItem(dropdown.Items);
                            listProvider.PopulateList(dropdown.Items, attributes.LookUpName, null, attributes.LookupCache);
                        }
                        else if (attributes.LookUpXml != "")
                        {
                            AddDefaultListItem(dropdown.Items);
                            listProvider.PopulateXmlList(dropdown.Items, attributes.LookUpXml);
                        }
                        else if (attributes.LookUpArray != "")
                        {
                            string[] array = (string[])GetSupportingProperty(attributes.LookUpArray);
                            AddItemToList(array, dropdown.Items);
                        }
                        else if (attributes.LookUpItems != "")
                        {
                            AddDefaultListItem(dropdown.Items);
                            ListItemCollection coll = (ListItemCollection)GetSupportingProperty(attributes.LookUpItems);
                            AddItemToList(coll, dropdown.Items);
                        }
                        else if (attributes.NumericList > 0)
                        {
                            AddNumericItems(dropdown, attributes.NumericList);
                        }
                    }
                    catch 
                    {
                    }
                }

                try
                {
                    if (attributes.ForcedIndex > -1)
                    {
                        dropdown.SelectedIndex = attributes.ForcedIndex;
                    }
                    else
                    {
                        dropdown.SelectedValue = CurrentValueString;
                        if (dropdown.SelectedIndex < 1 && attributes.DefaultIndex > -1)
                        {
                            dropdown.SelectedIndex = attributes.DefaultIndex;
                        }
                    }
                }
                catch 
                {
					if (attributes.ForceBlank || attributes.Mandatory)
					{
						try
						{
							dropdown.SelectedIndex = -1;
							PostbackValue = "";
						}
						catch { }
					}
				}
				try
				{
					if (dropdown.SelectedValue == CurrentValueString)
					{
						PostbackValue = dropdown.SelectedValue;
					}
				}
				catch { }
			}
            ProcessControl();
        }

        private void ProcessLookupXmlCustomHandler()
        {
            DropDownList dropdown = (DropDownList)parentControl;

            dropdown.Items.Clear();

            object parentProperty1 = GetSupportingProperty(attributes.ParentProperty1);
            object parentProperty2 = GetSupportingProperty(attributes.ParentProperty2);
            object parentProperty3 = GetSupportingProperty(attributes.ParentProperty3);
            object parentProperty4 = GetSupportingProperty(attributes.ParentProperty4);
            object parentProperty5 = GetSupportingProperty(attributes.ParentProperty5);
            object parentProperty6 = GetSupportingProperty(attributes.ParentProperty6);
            object parentProperty7 = GetSupportingProperty(attributes.ParentProperty7);
            object parentProperty8 = GetSupportingProperty(attributes.ParentProperty8);
            object parentProperty9 = GetSupportingProperty(attributes.ParentProperty9);


            try
            {
                if (parentProperty1 != null)
                {
                    AddDefaultListItem(dropdown.Items);
                    listProvider.PopulateXmlList(dropdown.Items, attributes.LookUpXml, parentProperty1, parentProperty2, parentProperty3, parentProperty4, parentProperty5, parentProperty6, parentProperty7, parentProperty8, parentProperty9);
                }
                else
                {
                    AddDefaultListItem(dropdown.Items);
                    listProvider.PopulateXmlList(dropdown.Items, attributes.LookUpXml);
                }
            }
            catch
            {
            }

            try
            {
                if (attributes.ForcedIndex > -1)
                {
                    dropdown.SelectedIndex = attributes.ForcedIndex;
                }
                else
                {
                    dropdown.SelectedValue = CurrentValueString;
                    if (dropdown.SelectedIndex < 1 && attributes.DefaultIndex > -1)
                    {
                        dropdown.SelectedIndex = attributes.DefaultIndex;
                    }
                }
            }
            catch (Exception)
            {
                if (attributes.ForceBlank || attributes.Mandatory)
                {
                    try
                    {
                        dropdown.SelectedIndex = -1;
                        PostbackValue = "";
                    }
                    catch
                    {
                    }
                }
            }
            try
            {
                if (dropdown.SelectedValue == CurrentValueString)
                {
                    PostbackValue = dropdown.SelectedValue;
                }
            }
            catch { }
            ProcessControl();
        }

        protected void AddDefaultListItem(ListItemCollection coll)
        {
            if (!Attributes.NoBlankOption)
            {
                if (classValueType.Name == "Int16" || classValueType.Name == "Int32" || classValueType.Name == "Int64")
                {
                    coll.Add(new ListItem(attributes.DefaultValue, "0"));
                }
                else
                {
                    coll.Add(new ListItem(attributes.DefaultValue, ""));
                }
            }
        }

        protected void AddNumericItems(DropDownList ddl, int Items)
        {
            for (int i = 0; i < Items; i++)
            {
                ListItem li = new ListItem(i.ToString(), i.ToString());
                ddl.Items.Add(li);
            }
        }

        protected void ProcessLookupCustomHandler()
        {
            DropDownList dropdown = (DropDownList)parentControl;
#if (REMOVE)
			String sProp;
#endif

            dropdown.Items.Clear();

            object parentProperty1 = GetSupportingProperty(attributes.ParentProperty1);
            object parentProperty2 = GetSupportingProperty(attributes.ParentProperty2);
            object parentProperty3 = GetSupportingProperty(attributes.ParentProperty3);
            object parentProperty4 = GetSupportingProperty(attributes.ParentProperty4);
            object parentProperty5 = GetSupportingProperty(attributes.ParentProperty5);
            object parentProperty6 = GetSupportingProperty(attributes.ParentProperty6);
            object parentProperty7 = GetSupportingProperty(attributes.ParentProperty7);
            object parentProperty8 = GetSupportingProperty(attributes.ParentProperty8);
            object parentProperty9 = GetSupportingProperty(attributes.ParentProperty9);

#if (REMOVE)
			if (parentProperty1 != null)
			{
				sProp = String.Format("{0} = {1}", attributes.ParentProperty1, parentProperty1);
			}
			else
			{
				sProp = String.Format("{0} = null", attributes.ParentProperty1);
			}
			KxLogger.LogActivity("WebControl", "ProcessLookupCustomHandler(1)", attributes.LookUpName, sProp);
			if (parentProperty2 != null)
			{
				sProp = String.Format("{0} = {1}", attributes.ParentProperty2, parentProperty2);
			}
			else
			{
				sProp = String.Format("{0} = null", attributes.ParentProperty2);
			}
			KxLogger.LogActivity("WebControl", "ProcessLookupCustomHandler(2)", attributes.LookUpName, sProp);
			if (parentProperty3 != null)
			{
				sProp = String.Format("{0} = {1}", attributes.ParentProperty3, parentProperty3);
			}
			else
			{
				sProp = String.Format("{0} = null", attributes.ParentProperty3);
			}
			KxLogger.LogActivity("WebControl", "ProcessLookupCustomHandler(3)", attributes.LookUpName, sProp);
			if (parentProperty4 != null)
			{
				sProp = String.Format("{0} = {1}", attributes.ParentProperty4, parentProperty3);
			}
			else
			{
				sProp = String.Format("{0} = null", attributes.ParentProperty4);
			}
			KxLogger.LogActivity("WebControl", "ProcessLookupCustomHandler(4)", attributes.LookUpName, sProp);
			if (parentProperty5 != null)
			{
				sProp = String.Format("{0} = {1}", attributes.ParentProperty5, parentProperty5);
			}
			else
			{
				sProp = String.Format("{0} = null", attributes.ParentProperty5);
			}
			KxLogger.LogActivity("WebControl", "ProcessLookupCustomHandler(5)", attributes.LookUpName, sProp);
#endif

			try
            {
                if (parentProperty1 != null)
                {
                    AddDefaultListItem(dropdown.Items);
                    listProvider.PopulateList(dropdown.Items, attributes.LookUpName + "FILTER", parentProperty1, parentProperty2, parentProperty3, parentProperty4, parentProperty5, parentProperty6, parentProperty7, parentProperty8, parentProperty9, attributes.LookupCache);
                }
                else
                {
                    AddDefaultListItem(dropdown.Items);
                    listProvider.PopulateList(dropdown.Items, attributes.LookUpName, null, attributes.LookupCache);
                }
            }
            catch 
            {
            }

            try
            {
                if (attributes.ForcedIndex > -1)
                {
                    dropdown.SelectedIndex = attributes.ForcedIndex;
                }
                else
                {
                    dropdown.SelectedValue = CurrentValueString;
                    if (dropdown.SelectedIndex < 1 && attributes.DefaultIndex > -1)
                    {
                        dropdown.SelectedIndex = attributes.DefaultIndex;
                    }
                }
            }
			catch (Exception)
			{
				//KxLogger.LogActivity("WebControl", "ProcessRadioButtonLookupCustomHandler", ex.Message, attributes.Id);
				if (attributes.ForceBlank || attributes.Mandatory)
				{
					try
					{
						dropdown.SelectedIndex = -1;
						PostbackValue = "";
					}
					catch (Exception)
					{
						//KxLogger.LogActivity("WebControl", "ProcessRadioButtonLookupCustomHandler2", ex2.Message, attributes.Id);
					}
				}
			}
			try
			{
				if (dropdown.SelectedValue == CurrentValueString)
				{
					PostbackValue = dropdown.SelectedValue;
				}
			}
			catch { }
            ProcessControl();
        }

        protected void ProcessCustomHandler()
        {
            DropDownList dropdown = (DropDownList)parentControl;

            string Options = KxReflection.GetPropertyString(attributes.PrimaryObject, attributes.ParentProperty1);

            if (attributes.PrimaryObject != null)
            {
                if (dropdown.Items.Count == 0)
                {
                    if (Options != "")
                    {
                        string[] items = Options.Split('\r', '\n');

                        dropdown.Items.Add(new ListItem("", ""));

                        foreach (string str in items)
                        {
                            if (str != "")
                            {
                                string strText = str;
                                string strValue = str;
                                if (str.Contains("|"))
                                {
                                    string[] values = str.Split(new char[] {'|'});
                                    if(values.Length >= 2)
                                    {
                                        strText = values[1];
                                        strValue = values[0];
                                    }
                                }
                                ListItem li = new ListItem(strText, strValue);

                                if (strValue == CurrentValueString)
                                { li.Selected = true; }
                                dropdown.Items.Add(li);
                            }
                        }

                        try
                        {
                            if (attributes.ForcedIndex > -1)
                            {
                                dropdown.SelectedIndex = attributes.ForcedIndex;
                            }
                            else
                            {
                                if (dropdown.SelectedIndex < 1 && attributes.DefaultIndex > -1)
                                {
                                    dropdown.SelectedIndex = attributes.DefaultIndex;
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            KxLogger.LogActivity("WebControl", "ProcessCustomHandler", ex.Message, attributes.Id);
                        }
                    }
                }
            }
        }

        protected void ProcessRadioButtonLookupCustomHandler()
		{
			RadioButtonList radiobuttons = (RadioButtonList)parentControl;

			radiobuttons.Items.Clear();

			object parentProperty1 = GetSupportingProperty(attributes.ParentProperty1);
			object parentProperty2 = GetSupportingProperty(attributes.ParentProperty2);
			object parentProperty3 = GetSupportingProperty(attributes.ParentProperty3);
			object parentProperty4 = GetSupportingProperty(attributes.ParentProperty4);
			object parentProperty5 = GetSupportingProperty(attributes.ParentProperty5);
            object parentProperty6 = GetSupportingProperty(attributes.ParentProperty6);
            object parentProperty7 = GetSupportingProperty(attributes.ParentProperty7);
            object parentProperty8 = GetSupportingProperty(attributes.ParentProperty8);
            object parentProperty9 = GetSupportingProperty(attributes.ParentProperty9);

			try
			{
				if (parentProperty1 != null)
				{
					// AddDefaultListItem(radiobuttons.Items);
                    listProvider.PopulateList(radiobuttons.Items, attributes.LookUpName + "FILTER", parentProperty1, parentProperty2, parentProperty3, parentProperty4, parentProperty5, parentProperty6, parentProperty7, parentProperty8, parentProperty9, attributes.LookupCache);
				}
				else
				{
					// AddDefaultListItem(radiobuttons.Items);
					listProvider.PopulateList(radiobuttons.Items, attributes.LookUpName, null, attributes.LookupCache);
				}
			}
			catch { }

			try
			{
				if (attributes.ForcedIndex > -1)
				{
					radiobuttons.SelectedIndex = attributes.ForcedIndex;
				}
				else
				{
					radiobuttons.SelectedValue = CurrentValueString;
					if (radiobuttons.SelectedIndex < 1 && attributes.DefaultIndex > -1)
					{
						radiobuttons.SelectedIndex = attributes.DefaultIndex;
					}
				}
			}
			catch (Exception)
			{
				//KxLogger.LogActivity("WebControl", "ProcessRadioButtonLookupCustomHandler", ex.Message, attributes.Id);
				if (attributes.ForceBlank || attributes.Mandatory)
				{
					try
					{
						radiobuttons.SelectedIndex = -1;
						PostbackValue = "";
					}
					catch (Exception)
					{
						//KxLogger.LogActivity("WebControl", "ProcessRadioButtonLookupCustomHandler2", ex.Message, attributes.Id);
					}
				}
			}
			try
			{
				if (radiobuttons.SelectedValue == CurrentValueString)
				{
					PostbackValue = radiobuttons.SelectedValue;
				}
			}
			catch { }
			ProcessControl();
		}

        protected void ProcessRadioButtonCustomHandler()
        {
            RadioButtonList radiobuttons = (RadioButtonList)parentControl;

            string Options = KxReflection.GetPropertyString(attributes.PrimaryObject, attributes.ParentProperty1);

            if (attributes.PrimaryObject != null)
            {
                if (radiobuttons.Items.Count == 0)
                {
                    if (Options != "")
                    {
                        string[] items = Options.Split('\r', '\n');

//                        radiobuttons.Items.Add(new ListItem("", ""));

                        foreach (string str in items)
                        {
                            if (str != "")
                            {
                                ListItem li = new ListItem(str, str);

                                if (str == CurrentValueString)
                                { li.Selected = true; }
                                radiobuttons.Items.Add(li);
                            }
                        }

                        try
                        {
                            if (attributes.ForcedIndex > -1)
                            {
                                radiobuttons.SelectedIndex = attributes.ForcedIndex;
                            }
                            else
                            {
                                if (radiobuttons.SelectedIndex < 1 && attributes.DefaultIndex > -1)
                                {
                                    radiobuttons.SelectedIndex = attributes.DefaultIndex;
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            KxLogger.LogActivity("WebControl", "ProcessRadioButtonCustomHandler", ex.Message, attributes.Id);
							if (attributes.ForceBlank || attributes.Mandatory)
							{
								try
								{
									radiobuttons.SelectedIndex = -1;
									PostbackValue = "";
								}
								catch (Exception)
								{
									//KxLogger.LogActivity("WebControl", "ProcessRadioButtonLookupCustomHandler2", ex.Message, attributes.Id);
								}
							}
						}
                    }
					try
					{
						if (radiobuttons.SelectedValue == CurrentValueString)
						{
							PostbackValue = radiobuttons.SelectedValue;
						}
					}
					catch { }
				}
            }
        }

        protected void AddItemToList(string[] source, ListItemCollection items)
        {
            if (source != null)
            {
                foreach (string str in source)
                {
                    items.Add(str);
                }
            }
        }

        protected void AddItemToList(ListItemCollection source, ListItemCollection items)
        {
            if (source != null)
            {
                foreach (ListItem li in source)
                {
                    if (li.Text == "")
                    {
                        items.Clear();
                    }
                    ListItem newli = new ListItem();
                    newli.Text = li.Text;
                    newli.Value = li.Value;
                    newli.Selected = li.Selected;
                    items.Add(newli);
                }
            }
        }

        protected object GetSupportingProperty(string source)
        {
            object result = KxReflection.GetPropertyObject(attributes.TertiaryObject, source, true);

            if (result == null && attributes.SecondaryObject != null)
            {
                result = KxReflection.GetPropertyObject(attributes.SecondaryObject, source);
            }

            if (result == null)
            {
                result = KxReflection.GetPropertyObject(attributes.PrimaryObject, source);
            }

            return result;
        }

        protected string GetDisplayName()
        {
            string result = MakeDisplayName(attributes.PropertyName);


            if (attributes.DisplayText != "")
            {
                result = MakeDisplayName(KxLocalisationManager.GetSubstitution(attributes.DisplayText));
            }
            else if (attributes.PrimaryObject.GetType().GetProperty("FieldName") != null)
            {
                // Is User Defined Field
                result = KxLocalisationManager.GetSubstitution(KxReflection.GetPropertyString(attributes.PrimaryObject, "FieldName"));
            }
            return "\"" + result + "\"";
        }

        protected string MakeDisplayName(string source)
        {
            string result = "";
            string temp = source.Replace("ID", "").Replace("Id", "");

            foreach (char c in temp)
            {
                if (char.IsUpper(c))
                {
                    result += " ";
                }
                result += c;
            }
            return result.Trim();
        }

        protected void CloneTable()
        {
            try
            {
                Table template = (Table)KxPageUtility.FindControl(currentPage, Attributes.TableSource);
                
                template.Visible = true;

                ((PlaceHolder)parentControl).Controls.Clear();

                foreach (object obj in (Array)ClassValue)
                {
                    Table result = new Table();
                    result.CssClass = template.CssClass;

                    KxPageUtility.CloneTable(template, result, "_" + attributes.TableSuffix + obj.ToString());
                    result.Width = Unit.Percentage(100);
                    ((PlaceHolder)parentControl).Controls.Add(result);
                }
                template.Visible = false;
                //template.Dispose();
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "CloneTable", ex.Message, "");
            }
        }

        protected void ClonePlaceHolder()
        {
            try
            {
                PlaceHolder template = (PlaceHolder)KxPageUtility.FindControl(currentPage, Attributes.TableSource);

                template.Visible = true;

                ((PlaceHolder)parentControl).Controls.Clear();

                foreach (object obj in (Array)ClassValue)
                {
                    PlaceHolder result = new PlaceHolder();


                    string secondSuffix = null;
                    string thirdSuffix = null;
                    if(!string.IsNullOrEmpty(attributes.TableSecondSuffix))
                    {
                        secondSuffix = attributes.TableSecondSuffix +
                                       GetSupportingProperty(attributes.TableSecondSuffixProperty).ToString();

                        if(!string.IsNullOrEmpty(attributes.TableThirdSuffix))
                        {
                            thirdSuffix = attributes.TableThirdSuffix +
                                          GetSupportingProperty(attributes.TableThirdSuffixProperty).ToString();
                        }
                    }

                    KxPageUtility.ClonePlaceholder(template, result, "_" + attributes.TableSuffix + obj.ToString(), secondSuffix, thirdSuffix);

                    ((PlaceHolder)parentControl).Controls.Add(result);
                }
                template.Visible = false;
            }
            catch (Exception ex)
            {

                KxLogger.LogActivity(this, "ClonePlaceHolder", ex.Message, "");
            }
        }

        protected void CloneTableRows()
        {
            try
            {
                int idx = 0;
                Table template = (Table)KxPageUtility.FindControl(currentPage, Attributes.TableSource);
                Table emptytemplate = (Table)KxPageUtility.FindControl(currentPage, Attributes.TableEmpty);

                if (emptytemplate != null)
                {
                    emptytemplate.Visible = true;
                }

                if (template != null)
                {
                    template.Visible = true;

                    KxPageUtility.EmptyTableExceptHeader(((Table)ParentControl), Attributes.TableHeaderRows);

                    if (ClassValue != null && ((Array)ClassValue).Length > 0)
                    {
                        foreach (object obj in ((Array)ClassValue))
                        {
                            if (obj != null)
                            {
                                foreach (TableRow row in template.Rows)
                                {
                                    KxPageUtility.CloneRow(((Table)ParentControl), row, "_" + Attributes.TableSuffix + KxStringUtils.StringToAttributeSafeString(obj.ToString()), ref idx);
                                }
                            }
                        }
                    }
                    else
                    {
                        if (emptytemplate != null)
                        {
                            KxPageUtility.CloneRow(((Table)ParentControl), emptytemplate.Rows[0], "XX", ref idx);
                        }
                    }
                    template.Visible = false;
                    template.Dispose();
                }

                if (emptytemplate != null)
                {
                    emptytemplate.Visible = false;
                    emptytemplate.Dispose();
                }
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "CloneTableRows", ex.Message, Attributes.Id);
            }
        }

        protected string ValidateCustom(string Entry, string ValidationType)
        {
            string result = "";

            try
            {
                // Needs to be implemented when client requirement exists...
                if (!String.IsNullOrEmpty(Entry) && ValidationType == "EMAIL")
                {
                    if (!KxPageUtility.ValidateEmail(Entry))
                    {
                        result = " must be a valid email address.\r\n";
                    }
                }
                else if (!String.IsNullOrEmpty(Entry) && ValidationType == "TELEPHONE")
                {
                    if (!KxPageUtility.ValidatePhone(Entry))
                    {
                        result = " must be a valid phone number.\r\n";
                    }
                }
                else if (ValidationType == "ALPHANUM")
                {
                    if (!KxPageUtility.ValidateAlphaNumeric(Entry))
                    {
                        result = " must only contain alphanumeric characters.\r\n";
                    }
                }
                else if (ValidationType == "")
                {

                }
            }
            catch
            { }

            if (!string.IsNullOrWhiteSpace(result))
            {
                result = KxLocalisationManager.GetSubstitution(result.Trim());
            }

            return result;
        }

        public virtual string GetExtendedValid()
        {
            return "";
        }

        public virtual void InitialiseValues()
        {
            
        }

        public virtual object GetSecondaryValue(KxWebAttribute attrib, string alternateProperty)
        {
            return null;
        }

        public virtual string MergeTags(string tag)
        {
            return "";
        }

        public object GetPostbackOverride()
        {
            object result = postbackValue;

            if (attributes.UseClassValue != "" && (postbackValue == null || attributes.UseClassValue == postbackValue.ToString()))
            {
                result = ClassValue;
            }
            else if (attributes.UseClassValueWhenBlank)
            {
                if (result == null || result.ToString() == "" || result.ToString() == "0")
                {
                    result = ClassValue;
                }
            }
            return result;
        }

        public bool GetIsPostBack()
        {
            bool result = currentPage.IsPostBack || AjaxPostback;
            try
            {
                if (result && (ParentControl is CheckBoxList || ParentControl is RadioButton || ParentControl is RadioButtonList) || ParentControl is CheckBox || IsUserControl)
                {
                   
                }
                else if (result && HttpContext.Current.Request[parentControl.UniqueID] == null)
                {
                    result = false;
                }
            }
            catch
            {
                result = false;
            }
            return result;
        }

        public string GetSecondaryPropertyValue(string PropertyName)
        {
            string result = "";

            try
            {
                bool isTertiaryProperty = attributes.TertiaryObject != null && KxReflection.GetPropertyInfo(attributes.TertiaryObject, PropertyName) != null;
                bool isSecondaryProperty = attributes.SecondaryObject != null && KxReflection.GetPropertyInfo(attributes.SecondaryObject, PropertyName) != null;
                bool isPrimaryProperty = KxReflection.GetPropertyInfo(attributes.PrimaryObject, PropertyName) != null;

                if (isTertiaryProperty)
                {
                    result = KxReflection.GetPropertyString(attributes.TertiaryObject, PropertyName);
                }
                else if (isSecondaryProperty)
                {
                    result = KxReflection.GetPropertyString(attributes.SecondaryObject, PropertyName);
                }
                else if (isPrimaryProperty)
                {
                    result = KxReflection.GetPropertyString(attributes.PrimaryObject, PropertyName);
                }
            }
            catch { }

            return result;
        }


        public string ConvertToHtml(string source)
        {
            string result = source;

            result = result.Replace("\r\n", "<br/>");
            result = result.Replace("\r", "<br/>");
            result = result.Replace("\n", "<br/>");

            return result;
        }

        protected bool GetIsUserControl()
        {
            bool result = false;

            if (parentControl != null)
            {
                try
                {
                    if (parentControl.GetType().BaseType != null)
                    {
                        result = parentControl.GetType().BaseType == typeof(BaseUserControl);

                        if (!result && parentControl.GetType().BaseType.BaseType != null)
                        {
                            result = parentControl.GetType().BaseType.BaseType == typeof(BaseUserControl);

                            if (!result && parentControl.GetType().BaseType.BaseType.BaseType != null)
                            {
                                result = parentControl.GetType().BaseType.BaseType.BaseType == typeof(BaseUserControl);
                            }
                        }
                    
                    }
                }
                catch { }
            }
            return result;
        }
    }
}
