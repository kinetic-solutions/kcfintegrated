﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Web;
using Winnovative;
using Kx.Core.Configuration;
using Kx.Core.Exceptions;
using Kx.Core.Utility;
using Kx.Objects.Common;
using Kx.Objects.Student;
using System.Windows.Forms;
using Kx.Core.Resources;

namespace Kx.Core.Web.PageRenderer.Utility
{
    public class KxDocumentProcessor
    {

        public static byte[] GetTemplatePdfBytes(KxPageRenderer renderer, string emailtemplate, string pdfno)
        {
            byte[] result = null;

            try
            {
                Winnovative.PdfConverter pdfConverter = new Winnovative.PdfConverter();

                pdfConverter.PdfDocumentOptions.PdfPageSize = Winnovative.PdfPageSize.A4;
                pdfConverter.PdfDocumentOptions.PdfCompressionLevel = Winnovative.PdfCompressionLevel.NoCompression;
                pdfConverter.PdfDocumentOptions.ShowHeader = false;
                pdfConverter.PdfDocumentOptions.ShowFooter = false;
                pdfConverter.PdfDocumentOptions.LeftMargin = 15;
                pdfConverter.PdfDocumentOptions.RightMargin = 15;
                pdfConverter.PdfDocumentOptions.TopMargin = 35;
                pdfConverter.PdfDocumentOptions.BottomMargin = 35;
                //pdfConverter.PdfDocumentOptions.Width = 1024;
                //pdfConverter.PdfDocumentOptions.AdjustFontSize = true;
                pdfConverter.PdfDocumentOptions.PdfPageSize = Winnovative.PdfPageSize.A4;
                pdfConverter.PdfDocumentOptions.AvoidTextBreak = true;
                //pdfConverter.HtmlViewerWidth = 1024;
                pdfConverter.PdfDocumentOptions.FitWidth = true;

                pdfConverter.PdfSecurityOptions.CanEditContent = false;
                pdfConverter.PdfSecurityOptions.CanEditAnnotations = false;
                pdfConverter.PdfSecurityOptions.CanAssembleDocument = false;
                pdfConverter.PdfSecurityOptions.OwnerPassword = "cranf13ld";
                pdfConverter.JavaScriptEnabled = true;

                int conversionDelay = 2;

                if (int.TryParse(KxDbConfiguration.GetConfiguration("PDFConversionDelaySeconds", "2"), out conversionDelay))
                {
                    pdfConverter.ConversionDelay = conversionDelay;
                }

                string Source = (renderer.ResourceManager.ResourceProvider ?? new KxResourceProvider()).GetResource(renderer.ObjectTranslator.MergeTags(emailtemplate));

                string doc = renderer.ObjectTranslator.MergeTags(Source);

                if (!doc.ToLower().Contains("<html"))
                {
                    doc = doc.Replace("\r", "<br/>");
                    doc = doc.Replace("\n", "<br/>");
                }

                //pdfConverter.LicenseKey = "8tnA0sPSysTSxNzC0sHD3MPA3MvLy8s=";
                pdfConverter.LicenseKey = "Nbuouqu6oqmvuqurtKq6qau0q6i0o6Ojow==";
                
                string tempfilepath = KxDbConfiguration.GetConfiguration("TempFileLocation");

                string filename = Path.Combine(tempfilepath, emailtemplate + "_" + pdfno + ".pdf");


                if (!File.Exists(filename))
                {
                    try
                    {
                        result = pdfConverter.GetPdfBytesFromHtmlString(doc);
                    }
                    catch (Exception ex)
                    {
                        KxLogger.LogActivity("KxDocumentProcessor", "GetTemplatePdfBytes 1", ex.Message, "");
                    }
                }
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity("KxDocumentProcessor", "GetTemplatePdfBytes", ex.Message, "");
                result = null;
            }
            return result;
        }

        public static string GetTemplateString(KxPageRenderer renderer, string emailtemplate)
        {
            string result = string.Empty;

            try
            {
                string Source = (renderer.ResourceManager.ResourceProvider ?? new KxResourceProvider()).GetResource(renderer.ObjectTranslator.MergeTags(emailtemplate));

                string doc = renderer.ObjectTranslator.MergeTags(Source);

                /*
                if (!doc.ToLower().Contains("<html"))
                {
                    doc = doc.Replace("\r", "<br/>");
                    doc = doc.Replace("\n", "<br/>");
                }
                 * */
                result = doc;
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity("KxDocumentProcessor", "GetTemplateString", ex.Message, "");
                result = null;
            }
            return result;
        }

        public static string GetTemplatePdfAsFile(KxPageRenderer renderer, string emailtemplate, string pdfno,string fileName)
        {
            string result = "";
            string filename = "";

            string tempfilepath = KxDbConfiguration.GetConfiguration("TempFileLocation");

            if (fileName == "")
            {
                string tempName = renderer.ObjectTranslator.MergeTags(emailtemplate);
                 filename = Path.Combine(tempfilepath, tempName + "_" + pdfno + ".pdf");
            }
            else
            {
                if (Path.GetExtension(fileName).ToUpper() != "PDF")
                {
                    fileName = fileName + ".pdf";
                }
                filename = Path.Combine(tempfilepath, fileName);
            }

            byte[] pdffile = GetTemplatePdfBytes(renderer, emailtemplate, pdfno);

            if (!File.Exists(filename) && pdffile != null)
            {
                try
                {
                    int ArraySize = new int();
                    ArraySize = pdffile.GetUpperBound(0);
                    FileStream fs1 = new FileStream(filename, FileMode.OpenOrCreate, FileAccess.Write);
                    fs1.Write(pdffile, 0, ArraySize);
                    fs1.Close();
                }
                catch (Exception ex)
                {
                    KxLogger.LogActivity("KxDocumentProcessor", "GetTemplatePdfAsFile", ex.Message, filename);
                }
            }
            else
            {
                if (pdffile == null)
                {
                    KxLogger.LogActivity("KxDocumentProcessor", "GetTemplatePdfAsFile", "Zero Bytes", filename);
                }
                else
                {
                    KxLogger.LogActivity("KxDocumentProcessor", "GetTemplatePdfAsFile", "File Exists", filename);
                }
            }
            result = filename;

            return result;
        }

        public static string GetTemplateAsFile(KxPageRenderer renderer, string emailtemplate, string pdfno, string fileName)
        {
            string result = "";
            string filename = "";
            string extension = "";

            string document = GetTemplateString(renderer, emailtemplate);

            if (document.ToLower().Contains("<html"))
            {
                extension = ".htm";
            }
            else
            {
                extension = ".rtf";
            }

            string tempfilepath = KxDbConfiguration.GetConfiguration("TempFileLocation");

            if (fileName == "")
            {
                filename = Path.Combine(tempfilepath, String.Format("{0}_{1}{2}", emailtemplate, pdfno, extension));
            }
            else
            {
                filename = Path.Combine(tempfilepath, fileName);
            }

            byte[] docbytesfile = Encoding.UTF8.GetBytes(document);

            if (!File.Exists(filename) && docbytesfile != null)
            {
                try
                {
                    int ArraySize = new int();
                    ArraySize = docbytesfile.Length;
                    using (FileStream fs1 = new FileStream(filename, FileMode.OpenOrCreate, FileAccess.Write))
                    {
                        fs1.Write(docbytesfile, 0, ArraySize);
                        fs1.Close();
                    }
                }
                catch (Exception ex)
                {
                    KxLogger.LogActivity("KxDocumentProcessor", "GetTemplateAsFile", ex.Message, filename);
                }
            }
            result = filename;

            return result;
        }


        public static Winnovative.WnvRtfToPdf.PdfDocumentOptions StandardDocumentOption()
        {
            Winnovative.WnvRtfToPdf.PdfDocumentOptions result = new Winnovative.WnvRtfToPdf.PdfDocumentOptions();

            result.PdfPageSize = Winnovative.WnvRtfToPdf.PdfPageSize.A4;
            result.PdfCompressionLevel = Winnovative.WnvRtfToPdf.PdfCompressionLevel.NoCompression;
            result.ShowHeader = false;
            result.ShowFooter = false;

            return result;
        }

        public static byte[] GetWordTemplatePdfBytes(KxPageRenderer renderer, int wordtemplateid, string pdfno, string wordtemplateallocationtype)
        {
            return GetWordTemplatePdfBytes(renderer, wordtemplateid, pdfno, wordtemplateallocationtype, StandardDocumentOption());
        }

        public static byte[] ConvertRtfStringToPdfBytes(string source, Winnovative.WnvRtfToPdf.PdfDocumentOptions docOptions)
        {
            byte[] result = null;

            try
            {
                Winnovative.WnvRtfToPdf.PdfConverter pdfConverter = new Winnovative.WnvRtfToPdf.PdfConverter();
                pdfConverter.PdfDocumentOptions.BottomMargin = docOptions.BottomMargin;
                pdfConverter.PdfDocumentOptions.EmbedFonts = docOptions.EmbedFonts;
                pdfConverter.PdfDocumentOptions.FitWidth = docOptions.FitWidth;
                pdfConverter.PdfDocumentOptions.LeftMargin = docOptions.LeftMargin;
                pdfConverter.PdfDocumentOptions.PdfCompressionLevel = docOptions.PdfCompressionLevel;
                pdfConverter.PdfDocumentOptions.PdfPageOrientation = docOptions.PdfPageOrientation;
                pdfConverter.PdfDocumentOptions.PdfPageSize = docOptions.PdfPageSize;
                pdfConverter.PdfDocumentOptions.RightMargin = docOptions.RightMargin;
                pdfConverter.PdfDocumentOptions.ShowFooter = docOptions.ShowFooter;
                pdfConverter.PdfDocumentOptions.ShowHeader = docOptions.ShowHeader;
                pdfConverter.PdfDocumentOptions.TopMargin = docOptions.TopMargin;

                pdfConverter.LicenseKey = "Gvp4NrNd5kq4KSYM/ueNjjhqKiMR9J9eDGChmSf4W394F2uHligBj97MshWVUwN/";
                pdfConverter.LicenseFilePath = KxDbConfiguration.GetConfiguration("WorkflowObjectPath");

                result = pdfConverter.GetPdfBytesFromRtfString(source);
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity("KxDocumentProcessor", "ConvertRtfStringToPdfBytes", ex.Message, "");
                result = null;
            }
            return result;
        }

        public static string GetMergedWordTemplate(KxPageRenderer renderer, int wordtemplateid, string wordtemplateallocationtype)
        {
            string result = String.Empty;

            try
            {
                
                StringBuilder doc = new StringBuilder();

                KxWordTemplate wordTemplate = new KxWordTemplate(wordtemplateid);

                string template = Encoding.UTF8.GetString(wordTemplate.TemplateArray);

                //KxStringUtils.WriteStringToFileSafe(template, "c:\\generateddata\\merge1.txt");

                template = template.Replace("{", "~^");
                template = template.Replace("}", "¬^");

                doc.Append(template);

                int index = 0;

                while (doc.ToString().IndexOf("<", index) > -1)
                {
                    int idx = doc.ToString().IndexOf("<", index);
                    int endidx = doc.ToString().IndexOf(">", idx);

                    string element = doc.ToString().Substring(idx, (endidx - idx) + 1);

                    string mt = element;

                    if (doc.ToString().Substring(idx, 4) == "<SA:")
                    {
                        mt = "{KX_PCUSER"+wordtemplateallocationtype+"MERGE_PNMergeTag_MT"+doc.ToString().Substring(idx + 4, endidx - idx - 4)+"}";
                    }
                    else if (doc.ToString().Substring(idx, 4) == "<SD:")
                    {
                        mt = "{KX_PCSTUDENTMERGE_PNMergeTag_MT"+ doc.ToString().Substring(idx + 4, endidx - idx - 4)+"}";
                    }
                    else
                    {
                        mt = element; 
                    }
                                       
                    doc.Replace(element, mt);

                    index = idx + 1;
                }
                
                //KxStringUtils.WriteStringToFileSafe(doc.ToString(), "c:\\generateddata\\merge2.txt");

                result =  renderer.ObjectTranslator.MergeTags(doc.ToString());

                result = result.Replace("~^","{");
                result = result.Replace("¬^","}");

                //KxStringUtils.WriteStringToFileSafe(result, "c:\\generateddata\\merge3.txt");
               
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity("KxDocumentProcessor", "GetMergedWordTemplate", ex.Message, "");
                result = "";
            }
            return result;
        }

        public static byte[] GetWordTemplatePdfBytes(KxPageRenderer renderer, int wordtemplateid, string pdfno, string wordtemplateallocationtype, Winnovative.WnvRtfToPdf.PdfDocumentOptions docOptions)
        {
            byte[] result = null;

            try
            {
                string mergeddoc = GetMergedWordTemplate(renderer, wordtemplateid, wordtemplateallocationtype);

                result = ConvertRtfStringToPdfBytes(mergeddoc, docOptions);  
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity("KxDocumentProcessor", "GetWordTemplatePdfBytes", ex.Message, "");
                result = null;
            }
            return result;
        }

        public static string GetWordTemplatePdfAsFile(KxPageRenderer renderer, int wordtemplateid, string pdfno, string wordtemplateallocationtype,string fileName)
        {
            string result = "";
            string filename = "";
            

            string tempfilepath = KxDbConfiguration.GetConfiguration("TempFileLocation");

            if (fileName == "")
            {
                KxWordTemplate wordTemplate = new KxWordTemplate(wordtemplateid);
                filename = Path.Combine(tempfilepath, String.Format("{0}_{1}.pdf", wordTemplate.Description, pdfno));
            }
            else
            {
                if (Path.GetExtension(fileName).ToUpper() != "PDF")
                {
                    fileName = String.Format("{0}.pdf", fileName);
                }
                filename = Path.Combine(tempfilepath, fileName);
            }

            byte[] pdffile = GetWordTemplatePdfBytes(renderer, wordtemplateid, pdfno, wordtemplateallocationtype);

            if (!File.Exists(filename) && pdffile != null)
            {
                try
                {
                    int ArraySize = new int();
                    ArraySize = pdffile.GetUpperBound(0);
                    FileStream fs1 = new FileStream(filename, FileMode.OpenOrCreate, FileAccess.Write);
                    fs1.Write(pdffile, 0, ArraySize);
                    fs1.Close();
                }
                catch (Exception ex)
                {
                    KxLogger.LogActivity("KxDocumentProcessor", "GetWordTemplatePdfAsFile", ex.Message, filename);
                }
            }
            result = filename;

            return result;
        }

        public static string GetComplexWordTemplateAsFile(KxPageRenderer renderer, int wordtemplateid, string fileno, string wordtemplateallocationtype, string fileName)
        {
            string result = "";

            string mergedfilename = GetWordTemplateAsFile(renderer, wordtemplateid, fileno, wordtemplateallocationtype, fileName);

//#if OUTPUT_PDF
            if (mergedfilename != String.Empty)
            {

                string sExecutable = Path.Combine(KxDbConfiguration.GetConfiguration("TempFileLocation"), "RTF2PDF.exe");

                if (File.Exists(sExecutable))
                {

                    ProcessStartInfo startInfo = new ProcessStartInfo(sExecutable);

                    startInfo.Arguments = String.Format("\"{0}\"", mergedfilename);
                    startInfo.CreateNoWindow = true;

                    Process proc = Process.Start(startInfo);
                    proc.WaitForExit();
                    result = Path.ChangeExtension(mergedfilename, "pdf");
                }
                else
                {
                    KxLogger.LogActivity("KxDocumentProcessor", "GetComplexWordTemplateAsFile", "RTT2PDF not found...", sExecutable);
                }

            }
//#else
//			result = mergedfilename;
//#endif

            return result;
        }

        public static string GetWordTemplateAsFile(KxPageRenderer renderer, int wordtemplateid, string fileno, string wordtemplateallocationtype, string fileName)
        {
            string result = "";
            string filename = "";


            string tempfilepath = KxDbConfiguration.GetConfiguration("TempFileLocation");

            if (fileName == "")
            {
                KxWordTemplate wordTemplate = new KxWordTemplate(wordtemplateid);
                filename = Path.Combine(tempfilepath, String.Format("{0}_{1}.rtf", wordTemplate.Description, fileno));
            }
            else
            {
                if (Path.GetExtension(fileName).ToUpper() != "RTF")
                {
                    fileName = String.Format("{0}.rtf", fileName);
                }
                filename = Path.Combine(tempfilepath, fileName);
            }

             try
             {
                KxStringUtils.WriteStringToFile(GetMergedWordTemplate(renderer, wordtemplateid, wordtemplateallocationtype), filename);
                result = filename;
             }
             catch (Exception ex)
             {
                 KxLogger.LogActivity("KxDocumentProcessor", "GetWordTemplateAsFile", ex.Message, filename);
             }

            return result;
        }


        public static string FileNametoMimeType(string filename)
        {
            FileInfo fi = new FileInfo(filename);

            switch (fi.Extension.ToLower())
            {
                case ".doc": return "application/msword";
                case ".dot" : return "application/msword";
                case ".htm" : return "text/html";
                case ".html" : return "text/html";
                case ".ppt" : return "application/x-mspowerpoint";
                case ".pdf": return "Application/pdf";
                case ".rtf" : return "text/richtext";
                case ".text" : return "application/plain";
                case ".txt" : return "text/plain";
                case ".xls" : return "application/excel";
                case ".zip" : return "application/zip";
                default: return "binary/octet-stream";
            }
        }

        public static bool IsValidFile(string fileName)
        {
            FileInfo fi = new FileInfo(fileName);
            switch (fi.Extension.ToLower())
            {
                case ".jpg":
                case ".jpeg":
                case ".png":
                case ".gif":
                case ".bmp":
                    return true;
                default: return false;
            }
        }

        public static void DownloadDocumentPdf(byte[] document, string fileName)
        {
            try
            {
                FileInfo fi = new FileInfo(fileName);

                if (fi.Extension.ToLower() == ".rtf" || fi.Extension.ToLower() == ".doc")
                {
                    DownloadDocument(ConvertRtfStringToPdfBytes(System.Text.UTF8Encoding.UTF8.GetString(document), StandardDocumentOption()),Path.ChangeExtension(fileName, ".pdf"),false);
                }
                else
                {
                    DownloadDocument(document, fileName, true);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public static void DownloadDocument(byte[] document, string fileName, bool convertHTML)
        {
            try
            {
                FileInfo fi = new FileInfo(fileName);

                if (convertHTML && (fi.Extension.ToLower() == ".htm" || fi.Extension.ToLower() == ".html"))
                {
                    DownloadPdf(System.Text.UTF8Encoding.UTF8.GetString(document), Path.ChangeExtension(fileName, ".pdf"));
                }
                else
                {
                    System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;
                    response.ClearContent();
                    response.ClearHeaders();
                    response.ContentType = FileNametoMimeType(fileName);
                    response.AddHeader("Content-Disposition", "attachment; filename=" + fileName + "; size=" + document.Length.ToString());
                   // response.AddHeader("Content-Disposition", "filename=" + fileName + "; size=" + document.Length.ToString());
                    response.BinaryWrite(document);

                    response.Flush();
                    response.End();
                }
            }
            catch (Exception ex)
            {
                throw new Exception (ex.Message);
            }
        }

        public static void DownloadPdf(string file, string filename)
        {
            Winnovative.PdfConverter pdfConverter = new Winnovative.PdfConverter();

            pdfConverter.PdfDocumentOptions.PdfPageSize = Winnovative.PdfPageSize.A4;
            pdfConverter.PdfDocumentOptions.PdfCompressionLevel = Winnovative.PdfCompressionLevel.NoCompression;
            pdfConverter.PdfDocumentOptions.ShowHeader = false;
            pdfConverter.PdfDocumentOptions.ShowFooter = false;
            pdfConverter.PdfDocumentOptions.LeftMargin = 15;
            pdfConverter.PdfDocumentOptions.RightMargin = 15;
            pdfConverter.PdfDocumentOptions.TopMargin = 35;
            pdfConverter.PdfDocumentOptions.BottomMargin = 35;
            //pdfConverter.PdfDocumentOptions.Width = 1024;
            //pdfConverter.PdfDocumentOptions.AdjustFontSize = true;
            pdfConverter.PdfDocumentOptions.PdfPageSize = Winnovative.PdfPageSize.A4;
            pdfConverter.PdfDocumentOptions.AvoidTextBreak = true;
            //pdfConverter.HtmlViewerWidth = 1024;
            pdfConverter.PdfDocumentOptions.FitWidth = true;

            //pdfConverter.LicenseKey = "8tnA0sPSysTSxNzC0sHD3MPA3MvLy8s=";
            pdfConverter.LicenseKey = "Nbuouqu6oqmvuqurtKq6qau0q6i0o6Ojow==";

            pdfConverter.PdfSecurityOptions.CanEditContent = false;
            pdfConverter.PdfSecurityOptions.CanEditAnnotations = false;
            pdfConverter.PdfSecurityOptions.CanAssembleDocument = false;
            pdfConverter.PdfSecurityOptions.OwnerPassword = "cranf13ld";

            try
            {
                byte[] downloadBytes = pdfConverter.GetPdfBytesFromHtmlString(file);

                System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;
                response.Clear();

                response.AddHeader("Content-Type", FileNametoMimeType(filename));
                response.AddHeader("Content-Disposition", "attachment; filename=" + filename + "; size=" + downloadBytes.Length.ToString());
               // response.AddHeader("Content-Disposition", "filename=" + filename + "; size=" + downloadBytes.Length.ToString());

                response.Flush();
                response.BinaryWrite(downloadBytes);
                response.Flush();
                response.End();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

		public static void DownloadPdfLink(string file, string filename, string sGUID)
		{
			String sDownloadUrl = "";
			String sFilepath = "";

			try
			{
				sDownloadUrl = KxDbConfiguration.GetConfiguration("DownloadURL");
			}
			catch (Exception ex)
			{
				KxLogger.LogActivity("KxDocumentProcessor", "DownloadURL not set in config", ex.Message, "");
				sDownloadUrl = "";
			}
			try
			{
				sFilepath = KxDbConfiguration.GetConfiguration("DownloadLocation");
			}
			catch (Exception ex)
			{
				KxLogger.LogActivity("KxDocumentProcessor", "DownloadLocation not set in config", ex.Message, "");
				sFilepath = "";
			}
			KxLogger.LogActivity("KxDocumentProcessor", "Settings", sDownloadUrl, sFilepath);
			if (sDownloadUrl != "" && sFilepath != "")
			{
				KxLogger.LogActivity("KxDocumentProcessor", "Starting PDF creation", sDownloadUrl, sFilepath);
				try
				{
					Winnovative.PdfConverter pdfConverter = new Winnovative.PdfConverter();

					KxLogger.LogActivity("KxDocumentProcessor", "Created 001", "", "");
					pdfConverter.PdfDocumentOptions.PdfPageSize = Winnovative.PdfPageSize.A4;
					pdfConverter.PdfDocumentOptions.PdfCompressionLevel = Winnovative.PdfCompressionLevel.NoCompression;
					pdfConverter.PdfDocumentOptions.ShowHeader = false;
					pdfConverter.PdfDocumentOptions.ShowFooter = false;
					pdfConverter.PdfDocumentOptions.LeftMargin = 15;
					pdfConverter.PdfDocumentOptions.RightMargin = 15;
					pdfConverter.PdfDocumentOptions.TopMargin = 35;
					pdfConverter.PdfDocumentOptions.BottomMargin = 35;
                    //pdfConverter.PdfDocumentOptions.Width = 1024;
					//pdfConverter.PdfDocumentOptions.AdjustFontSize = true;
					pdfConverter.PdfDocumentOptions.PdfPageSize = Winnovative.PdfPageSize.A4;
                    pdfConverter.PdfDocumentOptions.AvoidTextBreak = true;
                    //pdfConverter.HtmlViewerWidth = 1024;
				    pdfConverter.PdfDocumentOptions.FitWidth = true;
					KxLogger.LogActivity("KxDocumentProcessor", "Created 002", "", "");

                    //pdfConverter.LicenseKey = "8tnA0sPSysTSxNzC0sHD3MPA3MvLy8s=";
                    pdfConverter.LicenseKey = "Nbuouqu6oqmvuqurtKq6qau0q6i0o6Ojow==";

                    pdfConverter.PdfSecurityOptions.CanEditContent = false;
                    pdfConverter.PdfSecurityOptions.CanEditAnnotations = false;
                    pdfConverter.PdfSecurityOptions.CanAssembleDocument = false;
                    pdfConverter.PdfSecurityOptions.OwnerPassword = "cranf13ld";
				    pdfConverter.JavaScriptEnabled = true;

                    String sBaseFileName = String.Format("{0}_{1}", sGUID, filename);
                    String sFilename = Path.Combine(sFilepath, sBaseFileName);
					KxLogger.LogActivity("KxDocumentProcessor", "Created 004", sFilename, "");
					try
					{

                        //String sHTML = "<HTML><BODY><P><H2>Version 2.1.10 - 2010-09-20</H2><UL><LI>Fix updating buttons, when not the active window refreshes.</LI></UL></P><HR></P></BODY></HTML>";
						KxLogger.LogActivity("KxDocumentProcessor", "Saving HTML to PDF", sFilename, file.Length.ToString());
						try
						{
							using (FileStream fs1 = new FileStream(sFilename + ".html", FileMode.OpenOrCreate, FileAccess.Write))
							{
								fs1.Write(Encoding.ASCII.GetBytes(file), 0, file.Length);
								fs1.Close();
							}
						}
						catch (Exception ex)
						{
                            KxLogger.LogActivity("KxDocumentProcessor", "File Error", ex.Message, sDownloadUrl + sBaseFileName);
						}
						try
						{
							KxLogger.LogActivity("KxDocumentProcessor", "Starting PDF Creation", "", "");

							byte[] downloadBytes = pdfConverter.GetPdfBytesFromHtmlString(file);
                            KxLogger.LogActivity("KxDocumentProcessor", "PDF Size", downloadBytes.Length.ToString(), "");

                            if (File.Exists(sFilename))
                            {
                                KxLogger.LogActivity("KxDocumentProcessor", "Previous Deleted", sFilename, "");
                                File.Delete(sFilename);
                            }

                            using (FileStream fs1 = new FileStream(sFilename, FileMode.OpenOrCreate, FileAccess.Write))
							{
								fs1.Write(downloadBytes, 0, downloadBytes.Length);
								fs1.Close();
							}
							Array.Resize<byte>(ref downloadBytes, 0);
						}
						catch (Exception ex)
						{
                            KxLogger.LogActivity("KxDocumentProcessor", "PDF Error", ex.Message, sDownloadUrl + sBaseFileName);
						}
#if REMOVE
						try
						{
							pdfConverter.SavePdfFromHtmlStringToFile(file, sFilename);
						}
						catch (Exception ex)
						{
							KxLogger.LogActivity("KxDocumentProcessor", "PDF Error", ex.Message, sDownloadUrl + sFilename);
						}
#endif
						//pdfConverter.SavePdfFromHtmlStringToFile(sHTML, sFilename);
                        KxLogger.LogActivity("KxDocumentProcessor", "URL to download file", sDownloadUrl + sBaseFileName, "");
                        HttpContext.Current.Response.Redirect(sDownloadUrl + sBaseFileName);
						//HttpContext.Current.Response.Redirect(sDownloadUrl + "Sample.pdf");
					}
					catch (Exception ex)
					{
                        KxLogger.LogActivity("KxDocumentProcessor", "File Error", ex.Message, sDownloadUrl + sBaseFileName);
						throw new Exception(ex.Message);
					}
				}
				catch (Exception ex)
				{
					KxLogger.LogActivity("KxDocumentProcessor", "Process Error", ex.Message, "");
				}
			}
		}
    }
}
