using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Kx.Core.Utility;
using Kx.Core.Configuration;
using Kx.Core.Exceptions;
using Kx.Core.Reflection;
using System.Web.UI.HtmlControls;


namespace Kx.Core.Web.PageRenderer.Utility
{

    [Serializable]
    public class KxPageUtility 
    {
        public static string PageName
        {
            get
            {
                return GetPageName();
            }
        }

        public static bool IsLocal
        {
            get
            {
                return GetIsLocal();
            }
        }

        public static Control FindControl(Control control, string name)
        {
            Control result = null;
            if (control.ID != null)
            {
                if (control.ID.ToUpper().StartsWith(name.ToUpper()))
                {
                    result = control;
                }
            }

            if ((result == null))
            {
                foreach (Control c in control.Controls)
                {
                    result = FindControl(c, name);
                    if (result != null)
                    {
                        break;
                    }
                }
            }
            return result;
        }

        public static void CloneTable(Table source, Table dest, string suffix)
        {
            int idx = 0;

            foreach (TableRow row in source.Rows)
            {
                CloneRow(dest, row, suffix, ref idx);
            }
        }

        public static int ClonePlaceholder(PlaceHolder source, PlaceHolder dest, string suffix, string secondSuffix = null, string thirdSuffix = null, int idx = 0)
        {
            foreach (Control control in source.Controls)
            {
                CloneControls(dest, control, suffix, ref idx, secondSuffix, thirdSuffix);
            }

            return idx;
        }

        public static TableRow CloneRow(Table Destination, TableRow Source, string suffix, ref int idx)
        {
            TableRow newrow = (TableRow)KxReflection.CloneObject(Source);
            if (Source != null && Source.ID != null && Source.ID.Contains("KX_"))
            {
                newrow.ID = Source.ID + suffix + "_XXrow" + idx ;
            }
            else
            {
                newrow.ID = suffix + "_XXrow" + idx + UniqueString();
            }
            idx++;
            foreach (TableCell cell in Source.Cells)
            {
                CloneCell(newrow, cell, suffix, ref idx);
            }
            newrow.Width = Source.Width; 
            Destination.Rows.Add(newrow);
            
            return newrow;
        }

        public static void CloneCell(TableRow Destination, TableCell Source, string suffix, ref int idx)
        {
            TableCell newcell = (TableCell)KxReflection.CloneObject(Source);
            if (Source != null && Source.ID != null && Source.ID.Contains("KX_"))
            {
                newcell.ID = Source.ID + suffix + "_XXcell" + idx;
            }
            else
            {
                newcell.ID = suffix + "_XXcell" + idx + UniqueString();
            }

            idx++;

            foreach (Control control in Source.Controls)
            {
                CloneControls(newcell, control, suffix, ref idx);
            }
            newcell.Width = Source.Width;
            Destination.Cells.Add(newcell);
        }

        public static void CloneControls(Control Destination, Control Source, string suffix, ref int idx, string secondSuffix = null, string thirdSuffix = null)
        {
            try
            {
                Control newcontrol = KxReflection.CloneControl(Source);

                if (newcontrol.ID != null && newcontrol.ID.Contains("KX_"))
                {
                    newcontrol.ID += suffix + ApplySuffix(secondSuffix) + ApplySuffix(thirdSuffix);
                }
                else  if (!String.IsNullOrEmpty(newcontrol.ID))
                {
                    newcontrol.ID += suffix + "_XX" + UniqueString() + ApplySuffix(secondSuffix) + ApplySuffix(thirdSuffix);
                }
                else
                {
                    newcontrol.ID = suffix + "control" + idx + UniqueString() + ApplySuffix(secondSuffix) + ApplySuffix(thirdSuffix);
                    idx++;
                }

                if (Source is HtmlControl)
                {
                    var div = Source as HtmlControl;
                    foreach (string attribKey in div.Attributes.Keys)
                    {
                        var newDiv = newcontrol as HtmlControl;
                        if (newDiv.Attributes[attribKey] == null)
                        {
                            newDiv.Attributes.Add(attribKey, div.Attributes[attribKey]);
                        }
                    }
                }

                Destination.Controls.Add(newcontrol);

                if (Source is Table)
                {
                    CloneTable(((Table)Source), ((Table)newcontrol), suffix+"_XX" + idx.ToString());
                }
                else if (Source is DropDownList)
                {
                    CopyListItems(((DropDownList)newcontrol).Items, ((DropDownList)Source).Items);
                }
                else if (Source is Label)
                {
                    CheckLabelAssociation(newcontrol, Destination);
                }
                else if (Source is RadioButton && Source.ID.ToUpper().Contains("OG1"))
                {
                    ((RadioButton)(newcontrol)).GroupName += suffix;
                }
                else if (Source is PlaceHolder)
                {
                    ClonePlaceholder(((PlaceHolder) Source), (PlaceHolder) newcontrol, suffix + "_XX" + idx.ToString());
                }

                if (Source.HasControls() && !(Source is Table))
                {
                    foreach (Control childControl in Source.Controls)
                    {
                        KxPageUtility.CloneControls(newcontrol, childControl, suffix, ref idx, secondSuffix, thirdSuffix);
                    }
                }
            }
            catch 
            {
               
            }

        }

        private static string ApplySuffix(string suffixString)
        {
            if (!string.IsNullOrEmpty(suffixString))
            {
                return string.Format("_{0}", suffixString);
            }

            return string.Empty;
        }

        public static void CheckLabelAssociation(Control Source, Control Destination)
        {
            try
            {
                Label source = (Label)Source;

                if (!String.IsNullOrEmpty(source.AssociatedControlID))
                {
                    Control destination = FindControl(Destination, source.AssociatedControlID);

                    if (destination != null)
                    {
                        source.AssociatedControlID = destination.ID;
                    }
                    else
                    {
                        source.AssociatedControlID = "";
                    }
                }
            }
            catch { }
        }

        public static void CopyListItems(ListItemCollection Destination, ListItemCollection Source)
        {
            Destination.Clear();

            foreach (ListItem li in Source)
            {
                ListItem newli = new ListItem(li.Text, li.Value);
                Destination.Add(newli);
            }
        }

    


        public static string FormatObject(object obj, string formatStr, string cultureFormat)
        {
            if (obj != null)
            {
                try
                {
                    IFormatProvider culture = new CultureInfo(KxDbConfiguration.Culture, false);
                    if (!string.IsNullOrWhiteSpace(cultureFormat))
                    {
                        try
                        {
                            culture = new CultureInfo(cultureFormat);
                        }
                        catch (CultureNotFoundException)
                        {
                            culture = new CultureInfo(KxDbConfiguration.Culture, false);
                        }
                    }

                    switch (obj.GetType().Name.ToLower())
                    {
                        case "datetime":
                        {
                            return ((DateTime) obj).ToString(formatStr, culture);
                        }
                        case "decimal":
                        {
                            return ((decimal)obj).ToString(formatStr,culture);
                        }
                        default: return obj.ToString();
                    }
                }
                catch (Exception ex)
                {
                    KxLogger.LogActivity("KxPageUtility", "FormatObject", ex.Message, "Format string: " + formatStr);
                    return obj.ToString();
                }
            }
            else
            {
                return "";
            }
        }

        public static void EmptyTableExceptHeader(Table table, int HeaderRows)
        {
            while (table.Rows.Count > HeaderRows)
            {
                table.Rows.Remove(table.Rows[HeaderRows]);
            }
        }

        public static bool ValidateEmail(string email)
        {
            
            try
            {
                string pattern = KxDbConfiguration.GetConfiguration("EmailValidationRegEx", @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z");

                Regex reg = new Regex(pattern, RegexOptions.IgnoreCase);
           
                return reg.IsMatch(email);
            }
            catch
            {
                return false;
            }
        }

        public static bool ValidatePhone(string phone)
        {
            if (String.IsNullOrEmpty(phone))
            {
                return true;
            }
            else
            {
                try
                {
                    string pattern = KxDbConfiguration.GetConfiguration("TelephoneValidationRegEx", @"^(1[ \-\+]{0,3}|\+1[ -\+]{0,3}|\+1|\+)?((\(\+?1-[2-9][0-9]{1,2}\))|(\(\+?[2-8][0-9][0-9]\))|(\(\+?[1-9][0-9]\))|(\(\+?[17]\))|(\([2-9][2-9]\))|([ \-\.]{0,3}[0-9]{2,4}))?([ \-\.][0-9])?([ \-\.]{0,3}[0-9]{2,4}){2,3}$");

                    Regex reg = new Regex(pattern, RegexOptions.None);
                    return reg.IsMatch(phone);
                }
                catch
                {
                    return false;
                }
            }
        }

        public static bool ValidateAlphaNumeric(string value)
        {
            if (String.IsNullOrEmpty(value))
            {
                return true;
            }
            else
            {
                try
                {
                    string pattern = @"^[A-Za-z0-9 ]+";

                    Regex reg = new Regex(pattern, RegexOptions.IgnoreCase);

                    return reg.IsMatch(value);
                }
                catch
                {
                    return false;
                }
            }
        }

        public static string UniqueString()
        {
            Guid guid = Guid.NewGuid();

            String result = guid.ToString();

            return result.Replace("-","");
        }

        public static string StripUniqueNaming(string source)
        {
            if (source != null)
            {
                if (source.IndexOf('$') > -1)
                {
                    string[] temp = source.Split('$');
                    string result =  temp[temp.GetUpperBound(0)];
                    if (result.ToUpper().EndsWith(".Y") || result.ToUpper().EndsWith(".X"))
                    {
                        result = result.Remove(result.Length - 2, 2);
                    }
                    return result;
                    
                }
                else
                {
                    return source;
                }
            }
            else
            {
                return "";
            }
        }

        public static string GetPageName()
        {
            string result = "";

            try
            {
                result = Path.GetFileNameWithoutExtension(HttpContext.Current.Request.FilePath);
            }
            catch
            {}
            return result;
        }

        public static string GetPageName(Page page)
        {
            string result = "";

            try
            {
                result = Path.GetFileNameWithoutExtension(page.AppRelativeVirtualPath);
            }
            catch
            { }
            return result;
        }

        public static bool GetIsLocal()
        {
            return (GetIsPhysicallyLocal() || IsAdminAuthenticated());
        }

        public static bool GetIsPhysicallyLocal()
        {
            return ((HttpContext.Current.Request.IsLocal || IsLocalFileSystemWebService()) && !KxDbConfiguration.GetConfigurationBool("IgnoreLocal", false));
        }

        public static bool IsAdminAuthenticated()
        {
            return (HttpContext.Current.Request.IsAuthenticated && HttpContext.Current.User.Identity.AuthenticationType == "KxAdmin" && HttpContext.Current.User.IsInRole("Admin")) || HttpContext.Current.Session["ADMNLOGIN"] != null;
        }

        private static bool IsLocalFileSystemWebService()
        {
            string url = HttpContext.Current.Request.Url.ToString();

            if (((url == null) || (url == string.Empty)))
            {
                return false;
            }
            System.Uri wsUri = new System.Uri(url);

            if (((wsUri.Port >= 1024) && (string.Compare(wsUri.Host, "localHost", System.StringComparison.OrdinalIgnoreCase) == 0)))
            {
                return true;
            }
            return false;
        }

    

        
    }
}
