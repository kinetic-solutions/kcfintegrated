using System;
using System.Collections.Generic;
using System.Text;
using Kx.Core.Exceptions;
using Kx.Core.Utility;

namespace Kx.Core.Web.PageRenderer.Utility
{
    [Serializable]
    public class KxWebAttribute
    {
        private string id = "";
        private object primaryObject = null;
        private object secondaryObject = null;
        private object tertiaryObject = null;

        public object PrimaryObject
        {
            get
            {
                return primaryObject;
            }
            set
            { primaryObject = value; }
        }
        public object SecondaryObject
        {
            get
            {
                if (HasAttribute("SC") && secondaryObject != null)
                {
                    return secondaryObject;
                }
                else
                {
                    return primaryObject;
                }
            }
            set
            { secondaryObject = value; }
        }

        public object TertiaryObject
        {
            get
            {
                if (HasAttribute("TC") && tertiaryObject != null)
                {
                    return tertiaryObject;
                }
                else
                {
                    return SecondaryObject;
                }
            }
            set
            {
            	tertiaryObject = value;
            }
        }

        public string Id
        {
            get
            { return id; }
        }

        public int AllocationDate
        {
            get
            { return GetValueInt("AD", 0); }
        }

        public string AllocationPricing
        {
            get
            { return GetValue("AP", ""); }
        }

        public int AcademicYear
        {
            get
            { 
                return GetValueInt("AY", 0); 
            }
        }

        public Guid? SpecificApplicationGuid
        {
            get
            {
                string guid = GetValue("AG");
                if (!string.IsNullOrWhiteSpace(guid))
                {
                    return new Guid(guid);
                }
                return null;
            }
        }

        public int AcademicYearSession
        {
            get { return GetValueInt("AS", -1); }
        }

        public Decimal AddValue
        {
            get
            { return GetValueDecimal("AV", 0.00M); }
        }

        public int BedroomType
        {
            get
            { return GetValueInt("BT", -1); }
        }

        public bool CheckConflicting
        {
            get
            { return GetValueBool("CC", false); }
        }

        public string ColourEven
        {
            get
            { return GetValue("CE"); }
        }

        public int Capitalise
        {
            get
            { return GetValueInt("CF", 0); }
        }

        public int CorrespondenceId
        {
            get
            { return GetValueInt("CI", 0); }
        }

        public string CultureFormat
        {
            get { return GetValue("CL"); }
        }

        public string ColourOdd
        {
            get
            { return GetValue("CO"); }
        }

        public string CompareWithProperty
        {
            get
            { return GetValue("CP"); }
        }
                   
        public bool CheckRequired
        {
            get
            { return GetValueBool("CR", false); }
        }

        public string CheckedValue
        {
            get
            {return GetValue("CV");}
        }
        public bool CustomHandler
        {
            get
            { return GetValueBool("CH", false); }
        }
       
        public string ComparisonType
        {
            get
            { return GetValue("CT").ToUpper(); }
        }
        public string ComparisonWith
        {
            get
            { return GetValue("CW"); }
        }

        public string ControlStudentValue
        {
            get
            { return GetValue("CX"); }
        }

        public string DatePart
        {
            get
            { return GetValue("DP"); }
        }

        public bool DefaultButton
        {
            get
            { return GetValueBool("DB", false); }
        }

        public string DueDate
        {
            get
            { return GetValue("DD"); }
        }


        public int DefaultIndex
        {
            get
            { return GetValueInt("DI", -1); }
        }

        public string DefaultValue
        {
            get
            { return GetValue("DV",""); }
        }

        public string DistinctFields
        {
            get
            { return GetValue("DF", ""); }
        }
        
        public string DisplayText
        {
            get
            { return GetValue("DT"); }
        }

		public bool ForceBlank
		{
			get
			{ return GetValueBool("FB", false); }
		}

        public int EventId
        {
            get
            { return GetValueInt("EI", 0); }
        }

        public int EnsuiteLimit
        {
            get
            { return GetValueInt("EL", -1); }
        }

        public bool Email
        {
            get
            { return GetValueBool("EM", false); }
        }
        public string FilterSeperator
        {
            get
            { return GetValue("FC", "A"); }
        }
        public int ForcedIndex
        {
            get
            { return GetValueInt("FI", -1); }
        }

        public bool ForcedUpdate
        {
            get
            { return GetValueBool("FU", false); }
        }

        public bool ForcedValue
        {
            get
            { return GetValueBool("FV", false); }
        }

       
        public string FormatStr
        {
            get
            { return GetValue("FS"); }
        }

        public int GenericIndex
        {
            get
            { return GetValueInt("GI", 0); }
        }

        public int GroupMember
        {
            get
            { return GetValueInt("GM", 0); }
        }   

        public int GenericIndex2
        {
            get
            { return GetValueInt("GX", 0); }
        }

        public int GenericIndex3
        {
            get { return GetValueInt("GY", 0); }
        }

        public string GenericStringIndex
        {
            get
            { return GetValue("GS", ""); }
        }

        public string GenericStringIndex2
        {
            get
            { return GetValue("GT", ""); }
        }

        public string HelpRow
        {
            get
            { return GetValue("HR"); }
        }

        public string Hyperlink
        {
            get
            { return GetValue("HL"); }
        }

        public bool IgnoreHidden
        {
            get
            { return GetValueBool("IH", false); }
        }

        public string LookUpArray
        {
            get
            { return GetValue("LA"); }
        }



        public bool LookupCache
        {
            get
            { return GetValueBool("LC", true); }
        }
        public string LookUpItems
        {
            get
            { return GetValue("LI"); }
        }

        public string LibertyRoom
        {
            get
            { return GetValue("LR"); }
        }

        public string LookUpName
        {
            get
            { return GetValue("LU"); }
        }

        public string LookUpXml
        {
            get
            { return GetValue("LX"); }
        }

        public int MaxLength
        {
            get
            { return GetValueInt("ML", 0); }
        }

        public int MinLength
        {
            get
            { return GetValueInt("MN", 0); }
        }

        public bool Mandatory
        {
            get
            { return GetValueBool("MO",false); }
        }

        public string MandatoryProperty
        {
            get
            { return GetValue("MP", ""); }
        }

        public int MeetingUDF
        {
            get
            { return GetValueInt("MU", 0); }
        }

        public int NumericList
        {
            get
            { return GetValueInt("NL", 0); }
        }

        public bool NoBlankOption
        {
            get
            { return GetValueBool("NO", false); }
        }

        public string NoteType
        {
            get
            { return GetValue("NT"); }
        }

        public bool OverrideGroupName
        {
            get
            { return GetValueBool("OG", false); }
        }

        public string OpalRoom
        {
            get
            { return GetValue("OR"); }
        }

        public Type OverrideType
        {
            get
            { return GetValueType("OT");}
        }
        public string ParentProperty1
        {
            get
            { return GetValue("P1"); }
        }
        public string ParentProperty2
        {
            get
            { return GetValue("P2"); }
        }
        public string ParentProperty3
        {
            get
            { return GetValue("P3"); }
        }

        public string ParentProperty4
        {
            get
            { return GetValue("P4"); }
        }

        public string ParentProperty5
        {
            get
            { return GetValue("P5"); }
        }

        public string ParentProperty6
        {
            get
            { return GetValue("P6"); }
        }
        public string ParentProperty7
        {
            get
            { return GetValue("P7"); }
        }
        public string ParentProperty8
        {
            get
            { return GetValue("P8"); }
        }
        public string ParentProperty9
        {
            get
            { return GetValue("P9"); }
        }
        public string PrimaryClass
        {
            get
            {
                if (HasAttribute("PC"))
                {
                    return GetValue("PC");
                }
                else
                {
                    return GetValue("BC");
                }
            }
        }

        public int Preference
        {
            get
            { return GetValueInt("PI", 0); }
        }
        public int PrefBasketIndex
        {
            get
            { return GetValueInt("PB", 0); }
        }

        public int PreferenceCFCount
        {
            get
            { return GetValueInt("PQ", -99); }
        }

        public int PrefNumber
        {
            get
            { return GetValueInt("PF", -99); }
        }
        public int PrefSearchIndex
        {
            get
            { return GetValueInt("PS", 0); }
        }

        public int PreferenceValidation
        {
            get
            { return GetValueInt("PV", -99); }
        }
        public int PaymentNumber
        {
            get
            { return GetValueInt("PY", -99); }
        }

        public string PropertyName
        {
            get
            { return GetValue("PN");}
        }

        public string RoomFilter
        {
            get
            { return GetValue("RF"); }
        }

        public int RequestIdentifier
        {
            get { return GetValueInt("RI", -99); }
        }

        public string RfpGuid
        {
            get
            { return GetValue("RG"); }
        }

        public bool ResetWhenHidden
        {
            get
            { return GetValueBool("RH", false); }
        }

        public bool ReadOnly
        {
            get
            { return GetValueBool("RO",false); }
        }
        public string ReadOnlyProperty
        {
            get
            { return GetValue("RP"); }
        }

        public bool RefreshList
        {
            get
            { return GetValueBool("RL", false); }
        }

        public string ReturnType
        {
            get
            { return GetValue("RX");}
        }

        public string ResourceName
        {
            get
            { return GetValue("RN"); }
        }

        public string ResourceProperty
        {
            get
            { return GetValue("RD"); }
        }

        public int ResourceType
        {
            get
            { return GetValueInt("RT", 0); }
        }

        public string ReturnValue
        {
            get
            { return GetValue("RV"); }
        }

        public string SecondaryClass
        {
            get { return GetValue("SC"); }
        }

        public string TertiaryClass
        {
            get { return GetValue("TC"); }
        }

        public string StyleEven
        {
            get { return GetValue("SE"); }
        }


        public string StyleOdd
        {
            get
            { return GetValue("SO"); }
        }

        public string SiteLookup
        {
            get
            { return GetValue("SL"); }
        }

        public string StyleProperty
        {
            get
            { return GetValue("SP"); }
        }



        public string StyleSuffix
        {
            get
            { return GetValue("SS"); }
        }

        public string StyleSuffixProperty
        {
            get
            { return GetValue("ST"); }
        }

        public string SelectedProperty
        {
            get
            { return GetValue("SR"); }
        }

        public string SelectedValue
        {
            get
            { return GetValue("SV"); }
        }
        public bool ShowNoData
        {
            get
            { return GetValueBool("ND",false); }
        }

        public string ShowNoDataValue
        {
            get
            { return GetValue("NV"); }
        }

        public string TableAction
        {
            get
            { return GetValue("TA"); }
        }

        public string PlaceHolderAction
        {
            get { return GetValue("PA"); }
        }

        public int StudentTermDate
        {
            get
            { return GetValueInt("TD", 0); }
        }

        public string TableEmpty
        {
            get
            { return GetValue("TE"); }
        }
        public int TableHeaderRows
        {
            get
            { return GetValueInt("TH",1); }
        }
        public string TablePlaceholder
        {
            get
            { return GetValue("TP"); }
        }
        public string TableSource
        {
            get
            { return GetValue("TS"); }
        }
        public string TableSuffix
        {
            get
            { return GetValue("TX"); }
        }

        public string TableSecondSuffix
        {
            get
            {
                return GetValue("T2");
            }
        }

        public string TableSecondSuffixProperty
        {
            get
            {
                return GetValue("TY");
            }
        }

        public string TableThirdSuffix
        {
            get
            {
                return GetValue("T3");
            }
        }

        public string TableThirdSuffixProperty
        {
            get
            {
                return GetValue("TZ");
            }
        }

        public string TemplateTable
        {
            get
            { return GetValue("TT"); }
        }

        public bool UseClassValueWhenBlank
        {
            get
            { return GetValueBool("UB", false); }
        }

        public string UseClassValue
        {
            get
            { return GetValue("UC", ""); }
        }

        public string UncheckedValue
        {
            get
            { return GetValue("UV"); }
        }
        
              
        public string ValidationType
        {
            get
            { return GetValue("VT").ToUpper(); }
        }

        
        public string ValidateCustom
        {
            get
            { return GetValue("VC"); }
        }

        public bool Visible
        {
            get
            { return GetValueBool("VB",true); }
        }
        public string VisibleProperty
        {
            get
            { return GetValue("VP"); }
        }
        public string VisibleMethod
        {
            get { return GetValue("VM"); }
        }

        public bool ValidateQuestion
        {
            get
            { return GetValueBool("VQ", false); }
        }

        public int CompareUDF
        {
            get
            { return GetValueInt("CU", -99); }
        }

        public string MergeTag
        {
            get
            { return GetValue("MT"); }
        }

        public int MergeTagIndex
        {
            get
            { return GetValueInt("MI", 0); }
        }

        public int UDF
        {
            get
            { return GetValueInt("UD", 0); }
        }

        public int UDFProcess
        {
            get
            { return GetValueInt("UP", 0); }
        }

        public int ValidateUDF
        {
            get
            { return GetValueInt("VU", -99); }
        }

        public int ValidateUDF2
        {
            get
            { return GetValueInt("VX", -99); }
        }
        
        public string WebResource
        {
            get
            { return GetValue("WR"); }
        }

        public string DocumentName
        {
            get
            { return GetValue("DN"); }
        }

        public string DocumentSuffix
        {
            get { return GetValue("DS"); }
        }

        public KxWebAttribute(string controlId)
        {
            if (controlId != null)
            {
                id = controlId;
            }
        }

        protected string GetValue(string value)
        {
            return GetValue(value.ToUpper(), "");
        }

        protected string GetValue(string value, string defaultvalue)
        {
            string temp = id+"_";
            string result = defaultvalue;
            
            if (HasAttribute(value))
            {
                int start = temp.ToUpper().IndexOf("_" + value) +3;
                int end = temp.IndexOf("_", start);

                if (end > start)
                {
                    result = temp.Substring(start, end - start);
                }
            }

            result = ReplacePlaceHolders(result);

            return result;
        }


        protected int GetValueInt(string value, int defaultvalue)
        {
            if (!HasAttribute(value))
            {
                return defaultvalue;
            }
            else
            {
                try
                {
                    return Convert.ToInt32(GetValue(value));
                }
                catch
                {
                    return defaultvalue;
                }
            }
        }

        protected Decimal GetValueDecimal(string value, Decimal defaultvalue)
        {
            if (!HasAttribute(value))
            {
                return defaultvalue;
            }
            else
            {
                try
                {
                    return Convert.ToDecimal(GetValue(value));
                }
                catch
                {
                    return defaultvalue;
                }
            }
        }

        protected bool GetValueBool(string value, bool defaultvalue)
        {
            if (!HasAttribute(value))
            {
                return defaultvalue;
            }
            else
            {
                string val = GetValue(value);
                try
                {
                    if (val == "1")
                    {
                        return true;
                    }
                    else if (val == "0")
                    {
                        return false;
                    }
                    else
                    {
                        return bool.Parse(value);
                    }
                }
                catch
                {
                    return defaultvalue;
                }
            }
        }

        protected Type GetValueType(string value)
        {
            switch (GetValue(value).ToUpper())
            {
                case "": return null;
                case "DATETIME": return typeof(DateTime);
                case "DECIMAL": return typeof(Decimal);
                case "INT": return typeof(Int32);
                case "STRING": return typeof(string);
                default: return null;
            }
        }

        /// <summary>
        /// Checks if there's an attribute, adjusted to try and reduce memory usage.
        /// </summary>
        /// <param name="value">Attribute to search for</param>
        /// <returns></returns>
        public bool HasAttribute(string value)
        {
            return id.ToUpper().Contains("_" + value);

           // return id.IndexOf("_" + value) > -1;
        }

        protected string ReplacePlaceHolders(string source)
        {
            return KxStringUtils.AttributeSafeStringToString(source);
        }
    }
}
