using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Kx.Core.Configuration;
using Kx.Core.Data;

namespace Kx.Core.Web.VirtualProvider
{
    public class KxVirtualPageData
    {
        public Hashtable virtualPages = null;
        public Hashtable virtualTemplates = null;
        
        public KxVirtualPageData() 
        {
            virtualPages = new Hashtable();
            virtualTemplates = new Hashtable();

            if (KxDbConfiguration.GetConfiguration("CacheVirtualPages") != "")
            {
                SetupVirtualPages(virtualPages,"PAGE");
                SetupVirtualPages(virtualTemplates, "PAGETEMPLATE");
            }
        }

        public Hashtable SetupVirtualPages(Hashtable source,string resourcetype)
        {
            SqlCommand cmd = new SqlCommand("select Identifier,Data from WebResources where ResourceType = '" + resourcetype + "'", KxDataAccess.Connection);
            SqlDataReader reader = cmd.ExecuteReader();
            try
            {
                while (reader.Read())
                {
                    string page = reader.GetString(0);
                    string data = reader.GetString(1);
                    source.Add(page.ToUpper(), data);
                }
            }
            finally
            {
                reader.Close();
                reader = null;
            }
            return source;
        }

        public string GetFileContents(string virPath)
        {
            if (virtualPages.Count > 0)
            {
                return virtualPages[ExtractFileName(virPath).ToUpper()].ToString();
            }
            else
            {
                string result = "";

                SqlCommand cmd = new SqlCommand("select Data from WebResources where Identifier = @Identifier", KxDataAccess.Connection);
                cmd.Parameters.Add(new SqlParameter("@Identifier", ExtractFileName(virPath).ToUpper()));
                    
                SqlDataReader reader = cmd.ExecuteReader();

                try
                {
                    while (reader.Read())
                    {
                        result =  reader.GetString(0);
                        break;
                    }
                }
                finally
                {
                    reader.Close();
                    reader = null;
                }

                result = CheckForTemplate(result);

                return result;
            }
        }

        private string ExtractFileName(string virPath)
        {
            FileInfo fi = new FileInfo(virPath);
            return fi.Name.ToUpper();
        }
        public string CheckForTemplate(string source)
        {
            string result = "";

            if (source != "" && source.IndexOf("{##") > -1)
            {
                // This page needs a template....
            }

            return result;
        }

        public bool CheckIfFileExists(string virPath)
        {
            if (virtualPages.Count > 0)
            {
                return virtualPages[ExtractFileName(virPath)] != null;   
            }
            else
            {
                SqlCommand cmd = new SqlCommand("select Data from WebResources where Identifier = @Identifier", KxDataAccess.Connection);
                cmd.Parameters.Add(new SqlParameter("@Identifier", ExtractFileName(virPath).ToUpper()));

                SqlDataReader reader = cmd.ExecuteReader();

                try
                {
                    return reader.HasRows;
                }
                finally
                {
                    reader.Close();
                    reader = null;
                }
            }
                
        }
    }

}
