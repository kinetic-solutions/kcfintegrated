using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Web.Hosting;

namespace Kx.Core.Web.VirtualProvider
{
    public class KxVirtualFile : VirtualFile
    {
        private KxVirtualProvider spp;
        private string virPath;

        public KxVirtualFile(string virtualPath, KxVirtualProvider provider) : base(virtualPath)
        {
            this.spp = provider;
            this.virPath = virtualPath;
        }

        public override Stream Open()
        {
            string fileContents = spp.GetFileContents(virPath);
            Stream stream = new MemoryStream();

            if (fileContents != null || fileContents.Equals(String.Empty))
            {
                StreamWriter writer = new StreamWriter(stream);
                writer.Write(fileContents);
                writer.Flush();
                stream.Seek(0, SeekOrigin.Begin);
            }
            return stream;
        }
    }
}
