using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Web.Hosting;

namespace Kx.Core.Web.VirtualProvider
{
    public class KxVirtualDirectory : VirtualDirectory
    {
       private KxVirtualProvider spp;
       private ArrayList children = new ArrayList();
       private ArrayList directories = new ArrayList();
       private ArrayList files = new ArrayList();

       public override IEnumerable Children
       {
           get
           { return children; }
       }

       public override IEnumerable Directories
       {
           get
           { return directories; }
       }

       public override IEnumerable Files
       {
           get
           { return files; }
       }

       public KxVirtualDirectory(string virtualDir, KxVirtualProvider provider) : base(virtualDir) 
       { 
           spp = provider; 
       }
    }
}
