using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.Hosting;

namespace Kx.Core.Web.VirtualProvider
{
    public class KxVirtualProvider : VirtualPathProvider
    {
        KxVirtualPageData utility = null;

        public Hashtable VirtualPages
        {
            get
            { return utility.virtualPages; }
        }

        public Hashtable VirtualData
        {
            get 
            { return this.VirtualPages; }
        }

        public static void AppInitialize()
        {
            HostingEnvironment.RegisterVirtualPathProvider(new KxVirtualProvider());
        }

        public KxVirtualProvider() : base() 
        {
           utility = new KxVirtualPageData();
        }

        private bool IsPathVirtual(string virtualPath)
        {
            String checkPath = VirtualPathUtility.ToAppRelative(virtualPath);
            return checkPath.StartsWith("~/".ToLower().ToString(), StringComparison.InvariantCultureIgnoreCase);
        }

        public override bool FileExists(string virtualPath)
        {
            if (IsPathVirtual(virtualPath))
            {
                KxVirtualFile file = (KxVirtualFile)GetFile(virtualPath);

                if (utility.CheckIfFileExists(virtualPath))
                    return true;
                else
                    return Previous.FileExists(virtualPath);
            }
            else
                return Previous.FileExists(virtualPath);
        }

        public override bool DirectoryExists(string virtualDir)
        {
            if (IsPathVirtual(virtualDir))
            {
                KxVirtualDirectory dir = (KxVirtualDirectory)GetDirectory(virtualDir);
                return true;
            }
            else
                return Previous.DirectoryExists(virtualDir);
        }

        public override VirtualFile GetFile(string virtualPath)
        {
            if (IsPathVirtual(virtualPath))
                return new KxVirtualFile(virtualPath, this);
            else
                return Previous.GetFile(virtualPath);
        }

        public override VirtualDirectory GetDirectory(string virtualDir)
        {
            if (IsPathVirtual(virtualDir))
                return new KxVirtualDirectory(virtualDir, this);
            else
                return Previous.GetDirectory(virtualDir);
        }

        public string GetFileContents(string virPath)
        {
            return utility.GetFileContents(virPath);
        }
    }
}
