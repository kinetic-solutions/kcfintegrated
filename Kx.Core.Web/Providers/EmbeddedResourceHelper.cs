﻿#region copyright

// =======================================================
// Product:   KCF.Student
// Project:   Kx.Core.Web.Student
// File:      EmbeddedResourceHelper.cs
// Created:   21/11/2016
// By:        Steve Ashley
// Modified:  21/11/2016
// 
// Copyright © Kinetic Solutions Ltd 2016
// All rights reserved.
// 
// No portion of this file may be re-created, copied or otherwise
// used without the express permission of Kinetic Solutions Ltd.
// =======================================================

#endregion

#region usings

using System;
using System.IO;
using System.Reflection;

#endregion

namespace Kx.Core.Web.Student.Classes
{
    public class EmbeddedResourceHelper : IDisposable
    {
        #region Interface Methods

        public void Dispose()
        {
        }

        #endregion

        #region Public Methods

        public string GetEmbeddedResource(string resourceName)
        {
            try
            {
                var thisExe = Assembly.GetCallingAssembly();
                StreamReader reader = new StreamReader(thisExe.GetManifestResourceStream(resourceName));
                string result = reader.ReadToEnd();
                reader.Close();
                return result;

            }
            catch
            {}

            return string.Empty;
        }

        #endregion
    }
}