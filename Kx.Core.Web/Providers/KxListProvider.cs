using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Web.UI;
using System.Web.UI.WebControls;
using Kx.Core.Data;
using Kx.Core.Exceptions;
using Kx.Core.Resources;
using Kx.Core.Utility;
using System.Web;

namespace Kx.Core.Web.Providers
{
    [Serializable]
    public class KxListProvider
    {
        private Hashtable Lists = new Hashtable();

        public void PopulateXmlList(ListItemCollection destination, string src, object criteria1 = null, object criteria2 = null, object criteria3 = null, object criteria4 = null, object criteria5 = null, object criteria6 = null, object criteria7 = null, object criteria8 = null, object criteria9 = null)
        {
            try
            {
                KxResourceProvider resProvider = null;

                try
                {
                    resProvider = (KxResourceProvider)HttpContext.Current.Application["ResourceProvider"];
                }
                catch
                { }

                if (resProvider == null)
                {
                    resProvider = (KxResourceProvider)HttpContext.Current.Session["ResourceProvider"];
                }
                
                string xml;

                if (resProvider == null)
                {
                    xml = new KxResourceProvider().GetResource(src);
                }
                else
                {
                    xml = resProvider.GetResource(src);
                }

                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xml);

                XmlNodeList list = doc.SelectNodes("//options/option");

                foreach (XmlNode node in list)
                {
                    if (
                        (criteria1 == null ||
                         (criteria1 != null && node.Attributes["filter1"] != null &&
                          criteria1.ToString().ToLower() == node.Attributes["filter1"].InnerText.ToLower()))
                        &&
                        (criteria2 == null ||
                         (criteria2 != null && node.Attributes["filter2"] != null &&
                          criteria2.ToString().ToLower() == node.Attributes["filter2"].InnerText.ToLower()))
                        &&
                        (criteria3 == null ||
                         (criteria3 != null && node.Attributes["filter3"] != null &&
                          criteria3.ToString().ToLower() == node.Attributes["filter3"].InnerText.ToLower()))
                        &&
                        (criteria4 == null ||
                         (criteria4 != null && node.Attributes["filter4"] != null &&
                          criteria4.ToString().ToLower() == node.Attributes["filter4"].InnerText.ToLower()))
                        &&
                        (criteria5 == null ||
                         (criteria5 != null && node.Attributes["filter5"] != null &&
                          criteria5.ToString().ToLower() == node.Attributes["filter5"].InnerText.ToLower()))
                        &&
                        (criteria6 == null ||
                         (criteria6 != null && node.Attributes["filter6"] != null &&
                          criteria6.ToString().ToLower() == node.Attributes["filter6"].InnerText.ToLower()))
                        &&
                        (criteria7 == null ||
                         (criteria7 != null && node.Attributes["filter7"] != null &&
                          criteria7.ToString().ToLower() == node.Attributes["filter7"].InnerText.ToLower()))
                        &&
                        (criteria8 == null ||
                         (criteria8 != null && node.Attributes["filter8"] != null &&
                          criteria8.ToString().ToLower() == node.Attributes["filter8"].InnerText.ToLower()))
                        &&
                        (criteria9 == null ||
                         (criteria9 != null && node.Attributes["filter9"] != null &&
                          criteria9.ToString().ToLower() == node.Attributes["filter9"].InnerText.ToLower()))
                        )
                    {
                        string text = node.Attributes["text"].InnerText;
                        string value = node.Attributes["value"].InnerText;

                        destination.Add(new ListItem(text, value));
                    }
                }
            }
            catch { }
        }

        public void PopulateList(ListItemCollection destination, string src, object criteria1,bool CacheList)
        {
            PopulateList(destination, src, criteria1, null, null, null, null, null, null, null, null, "", "", CacheList);
        }

        public void PopulateList(ListItemCollection destination, string src, object criteria1, object criteria2, bool CacheList)
        {
            PopulateList(destination, src, criteria1, criteria2, null, null, null, null, null, null, null, "", "", CacheList);
        }

        public void PopulateList(ListItemCollection destination, string src, object criteria1, object criteria2, object criteria3, bool CacheList)
        {
            PopulateList(destination, src, criteria1, criteria2, criteria3, null, null, null, null, null, null, "", "", CacheList);
        }

        public void PopulateList(ListItemCollection destination, string src, object criteria1, object criteria2, object criteria3, object criteria4, bool CacheList)
        {
            PopulateList(destination, src, criteria1, criteria2, criteria3, criteria4, null, null, null, null, null, "", "", CacheList);
        }

        public void PopulateList(ListItemCollection destination, string src, object criteria1, object criteria2, object criteria3, object criteria4, object criteria5, bool CacheList)
        {
            PopulateList(destination, src, criteria1, criteria2, criteria3, criteria4, criteria5, null, null, null, null, "", "", CacheList);
        }

        public void PopulateList(ListItemCollection destination, string src, object criteria1, object criteria2, object criteria3, object criteria4, object criteria5, object criteria6, bool CacheList)
        {
            PopulateList(destination, src, criteria1, criteria2, criteria3, criteria4, criteria5, criteria6, null, null, null, "", "", CacheList);
        }

        public void PopulateList(ListItemCollection destination, string src, object criteria1, object criteria2, object criteria3, object criteria4, object criteria5, object criteria6, object criteria7, bool CacheList)
        {
            PopulateList(destination, src, criteria1, criteria2, criteria3, criteria4, criteria5, criteria6, criteria7, null, null, "", "", CacheList);
        }

        public void PopulateList(ListItemCollection destination, string src, object criteria1, object criteria2, object criteria3, object criteria4, object criteria5, object criteria6, object criteria7, object criteria8, bool CacheList)
        {
            PopulateList(destination, src, criteria1, criteria2, criteria3, criteria4, criteria5, criteria6, criteria7, criteria8, null, "", "", CacheList);
        }

        public void PopulateList(ListItemCollection destination, string src, object criteria1, object criteria2, object criteria3, object criteria4, object criteria5, object criteria6, object criteria7, object criteria8, object criteria9, bool CacheList)
        {
            PopulateList(destination, src, criteria1, criteria2, criteria3, criteria4, criteria5, criteria6, criteria7, criteria8, criteria9, "", "", CacheList);
        }

        public void PopulateList(ListItemCollection destination, string src, object criteria1, object criteria2, object criteria3, object criteria4, object criteria5, object criteria6, object criteria7, object criteria8, object criteria9, string filter, string FilterChar, bool CacheList)
        {
            ListItem[] source = GetList(src, criteria1, criteria2, criteria3, criteria4, criteria5, criteria6, criteria7, criteria8, criteria9, CacheList);

            foreach (ListItem list in source)
            {
                ListItem newItem = new ListItem();
                newItem.Value = list.Value;
                newItem.Text = list.Text;

                if (filter != "")
                {
                    if (FilterChar != "")
                    {
                        if (filter.IndexOf(FilterChar + newItem.Value + FilterChar) > -1)
                        {
                            destination.Add(newItem);
                        }
                    }
                    else
                    {
                        if (filter.IndexOf("0" + newItem.Value + "0") > -1)
                        {
                            destination.Add(newItem);
                        }
                    }
                }
                else
                {
                    destination.Add(newItem);
                }
            }
        }

        private ListItem[] GetList(string source, object criteria1, object criteria2, object criteria3, object criteria4, object criteria5, object criteria6, object criteria7, object criteria8, object criteria9, bool cache)
        {
            string keyName = source.ToUpper();

			//KxLogger.LogActivity(this, "GetList Started", source, "");
			if (criteria1 != null)
            {
                keyName += "_" + criteria1.ToString();
            }

            if (criteria2 != null)
            {
                keyName += "_" + criteria2.ToString();
            }

            if (criteria3 != null)
            {
                keyName += "_" + criteria3.ToString();
            }

            if (criteria4 != null)
            {
                keyName += "_" + criteria4.ToString();
            }

            if (criteria5 != null)
            {
                keyName += "_" + criteria5.ToString();
            }

            if (criteria6 != null)
            {
                keyName += "_" + criteria6.ToString();
            }

            if (criteria7 != null)
            {
                keyName += "_" + criteria7.ToString();
            }

            if (criteria8 != null)
            {
                keyName += "_" + criteria8.ToString();
            }

            if (criteria9 != null)
            {
                keyName += "_" + criteria9.ToString();
            }
            
            if (Lists[keyName] != null && cache)
            {
				//KxLogger.LogActivity(this, "GetList Started", source, "from cache");
				return (ListItem[])Lists[keyName];
            }
            else
            {
				//KxLogger.LogActivity(this, "GetList Started", source, "from DB");
				KxData data = new KxData("list", source, TargetType.Dynamic, ActionType.Select);

				data.AddParameter("@Filter", CheckNull(criteria1), KxValueType.Unknown);
				data.AddParameter("@Filter1", CheckNull(criteria2), KxValueType.Unknown);
				data.AddParameter("@Filter2", CheckNull(criteria3), KxValueType.Unknown);
				data.AddParameter("@Filter3", CheckNull(criteria4), KxValueType.Unknown);
				data.AddParameter("@Filter4", CheckNull(criteria5), KxValueType.Unknown);
                data.AddParameter("@Filter5", CheckNull(criteria6), KxValueType.Unknown);
                data.AddParameter("@Filter6", CheckNull(criteria7), KxValueType.Unknown);
                data.AddParameter("@Filter7", CheckNull(criteria8), KxValueType.Unknown);
                data.AddParameter("@Filter8", CheckNull(criteria9), KxValueType.Unknown);

#if (REMOVE)
				if (criteria1 != null)
                {
                    data.AddParameter("@Filter", CheckNull(criteria1), KxValueType.Unknown);
                }

                if (criteria2 != null)
                {
                    data.AddParameter("@Filter1", CheckNull(criteria2), KxValueType.Unknown);
                }

                if (criteria3 != null)
                {
                    data.AddParameter("@Filter2", CheckNull(criteria3), KxValueType.Unknown);
                }

                if (criteria4 != null)
                {
                    data.AddParameter("@Filter3", CheckNull(criteria4), KxValueType.Unknown);
                }

                if (criteria5 != null)
                {
                    data.AddParameter("@Filter4", CheckNull(criteria5), KxValueType.Unknown);
                }
#endif

                ListItem[] result = null;

                if (data.Execute())
                {
                    result = ItemArrayFromXml(data.XmlResults);
                    if (cache)
                    {
                        Lists.Add(keyName, result);
                    }
                }
                return result;
            }
        }
   

        public ListItem[] ItemArrayFromXml(string Xml)
        {
            ListItem[] result = null;

            try
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(Xml);

                XmlNodeList nodes = doc.SelectNodes("//list/datarow");

                result = new ListItem[nodes.Count];

                int loop = 0;

                foreach (XmlNode node in nodes)
                {
                    string name = KxXmlUtils.GetNodeValue(node, "name", "");
                    string value = KxXmlUtils.GetNodeValue(node, "value", name);

                    if (name != "")
                    {
                        result[loop] = new ListItem(name, value);
                        loop++;
                    }
                }
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "ItemArrayFromXml", ex.Message, Xml);
            }
            return result;
        }

        private object CheckNull(object source)
        {
            object result = source;

            if ((result != null) && ((result is string && (string)result == "") || (result is int && (int)result == 0)))
            {
                result = null;
            }
            return result;
        }

    }
}
