using System;
using System.Collections.Generic;
using System.Text;

namespace Kx.Core.PaymentProcessor.Payments
{
    public partial class PaymentResponse
    {
        private string providerReference;
        private string providerReference2;
        private string internalReference;
        private DateTime ?transactionDate;
        private string currency;
        private decimal amount;
        private decimal amount2;
        private decimal surcharge;
        private bool authorised;
        private bool processed = false;
        private string authorisationMessage;
        private bool surchargeapplied = false;

        private string bankName;
        private string branchName;

        private PaymentRequest originalRequest = null;

        public string InternalReference
        {
            get { return internalReference; }
            set { internalReference = value; }
        }
        public string ProviderReference
        {
            get { return providerReference; }
            set { providerReference = value; }
        }

        public string ProviderReference2
        {
            get
            { return providerReference2;  }
            set
            { providerReference2 = value; }
        }

        public decimal Amount
        {
            get 
            {
                return amount;
            }
            set { amount = value; }
        }

        public decimal Amount2
        {
            get
            {
                return amount2;
            }
            set
            {
                amount2 = value;
            }
        }

        public decimal Surcharge
        {
            get
            {
                return surcharge;
            }
            set { surcharge = value; }
        }
        public DateTime? TransactionDate
        {
            get 
            {
                if (transactionDate == null)
                {
                    transactionDate = DateTime.Now;
                }
                return transactionDate; 
            }
            set { transactionDate = value; }
        }
        public string Currency
        {
            get { return currency; }
            set { currency = value; }
        }
        public string AuthorisationMessage
        {
            get { return authorisationMessage; }
            set { authorisationMessage = value; }
        }
        public bool Processed
        {
            get { return processed; }
            set { processed = value; }
        }
        public bool SurchargeApplied
        {
            get { return surchargeapplied; }
            set { surchargeapplied = value; }
        }
        public bool Authorised
        {
            get { return authorised; }
            set { authorised = value; }
        }
        public string AuthorisedText
        {
            get
            { return Authorised.ToString().ToLower(); }
        }
        public PaymentRequest OriginalRequest
        {
            get
            {
                return originalRequest;
            }
            set
            {
                if (originalRequest == value)
                    return;
                originalRequest = value;
            }
        }
        public string ProviderName
        {
            get
            {
                if (originalRequest != null)
                {
                    return originalRequest.PaymentProvider;
                }
                else
                {
                    return "Unknown";
                }
            }
        }

        public string BankName
        {
            get
            { return bankName; }
            set
            { bankName = value; }
        }
        public string BranchName
        {
            get
            { return branchName; }
            set
            { branchName = value; }
        }

        public Dictionary<string, string> RawResponseData { get; private set; }

        public PaymentResponse()
        {
            RawResponseData = new Dictionary<string, string>(StringComparer.InvariantCultureIgnoreCase);
        }
    
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            sb.Append(providerReference + "<br/>");
            sb.Append(internalReference + "<br/>");
            sb.Append(transactionDate+ "<br/>");
            sb.Append(currency+ "<br/>");
            sb.Append(amount.ToString()+ "<br/>");
            sb.Append(authorisationMessage+ "<br/>");
            sb.Append(authorised+ "<br/>");
          
            if (OriginalRequest != null)
            {
                sb.Append(OriginalRequest.BookingReference + "<br/>");
                sb.Append(OriginalRequest.Description + "<br/>");
                sb.Append(OriginalRequest.Currency + "<br/>");
                sb.Append(OriginalRequest.Amount.ToString() + "<br/>");
                sb.Append(OriginalRequest.Title + "<br/>");
                sb.Append(OriginalRequest.FirstName + "<br/>");
                sb.Append(OriginalRequest.Surname + "<br/>");
                sb.Append(OriginalRequest.HouseNumber + "<br/>");
                sb.Append(OriginalRequest.Address1 + "<br/>");
                sb.Append(OriginalRequest.Address2 + "<br/>");
                sb.Append(OriginalRequest.Town + "<br/>");
                sb.Append(OriginalRequest.County + "<br/>");
                sb.Append(OriginalRequest.Country + "<br/>");
                sb.Append(OriginalRequest.Postcode + "<br/>");
                sb.Append(OriginalRequest.Telephone + "<br/>");
                sb.Append(OriginalRequest.Fax + "<br/>");
                sb.Append(OriginalRequest.Email + "<br/>");
                sb.Append(OriginalRequest.PaymentProvider + "<br/>");
                sb.Append(OriginalRequest.PaymentMethod + "<br/>");
                sb.Append(OriginalRequest.ProviderAccountId + "<br/>");
                sb.Append(OriginalRequest.ProviderUrl + "<br/>");
                sb.Append(OriginalRequest.ConfirmationPageUrl + "<br/>");
                sb.Append(OriginalRequest.CancellationPageUrl + "<br/>");
                sb.Append(OriginalRequest.TestMode.ToString() + "<br/>");
                sb.Append(OriginalRequest.TestTransactionOutcome.ToString() + "<br/>");
            }

            return sb.ToString();

        }

    
    }
}
