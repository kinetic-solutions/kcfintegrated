﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Utility;

namespace Kx.Core.PaymentProcessor.Payments
{
    public class PaymentCustomField
    {
        protected string fieldName = "";
        protected string fieldData = "";

        public string FieldName
        {
            get
            {
                return fieldName;
            }
            set
            {
                fieldName = value;
            }
        }

        public string FieldData
        {
            get
            {
                return fieldData;
            }
            set
            {
                fieldData = value;
            }
        }

        public PaymentCustomField()
        {
        }


        public PaymentCustomField(XmlNode node)
        {
            LoadFromXml(node);
        }

        public string ToXml()
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine("		<CustomField>");
            sb.AppendLine(String.Format("			<FieldName><![CDATA[{0}]]></FieldName>",FieldName));
            sb.AppendLine(String.Format("			<FieldData><![CDATA[{0}]]></FieldData>", FieldData));
            sb.AppendLine("		</CustomField>");

            return sb.ToString();
        }

        protected void LoadFromXml(XmlNode node)
        {
            FieldName = KxXmlUtils.GetNormalCaseNodeValue(node, "FieldName", "");
            FieldData = KxXmlUtils.GetNormalCaseNodeValue(node, "FieldData", "");
        }
    }
}
