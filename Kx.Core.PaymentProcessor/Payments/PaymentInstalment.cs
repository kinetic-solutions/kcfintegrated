﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Utility;

namespace Kx.Core.PaymentProcessor.Payments
{
    [Serializable]
    public class PaymentInstalment
    {
        Decimal amount = 0.00M;
        DateTime? instalmentDate = null;
        string description = "";

        public Decimal Amount
        {
            get
            {
                return amount;
            }
            set
            {
                amount = value;
            }
        }

        public DateTime? InstalmentDate
        {
            get
            {
                return instalmentDate;
            }
            set
            {
                instalmentDate = value;
            }
        }

        public string Description
        {
            get
            {
                return description;
            }
            set
            {
                description = value;
            }
        }

        public PaymentInstalment()
        {
        }

        public PaymentInstalment(XmlNode node)
        {
            LoadFromXml(node);
        }

        public string ToXml()
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine("				<Instalment>");
            sb.AppendLine(String.Format("					<Amount><![CDATA[{0:0.00}]]></Amount>",Amount));	
			sb.AppendLine(String.Format("					<Date><![CDATA[{0}]]></Date>",KxDateTime.DateToWpmDate(InstalmentDate)));
            sb.AppendLine(String.Format("					<Description><![CDATA[{0}]]></Description>",Description));
            sb.AppendLine("				</Instalment>");
			
            return sb.ToString();
        }

        public void LoadFromXml(XmlNode node)
        {
            Amount = KxXmlUtils.GetNormalCaseNodeValue(node, "Amount", 0.00M);
            Description = KxXmlUtils.GetNormalCaseNodeValue(node, "Description", "");
            InstalmentDate = KxDateTime.WpmDateToDate(KxXmlUtils.GetNormalCaseNodeValue(node, "Date", ""));
        }


    }
}
