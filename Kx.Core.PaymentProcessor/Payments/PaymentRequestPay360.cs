using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Kx.Core.PaymentProcessor.Payments
{
    public partial class PaymentRequest
    {
        public int SCPId { get; set; }
        public int HMACKeyId { get; set; }
        public string HMACKey { get; set; }
        public int Identifier{ get; set; }
        public string UniqueReference { get; set; }
        public string Timestamp { get; set; }

    }
}
