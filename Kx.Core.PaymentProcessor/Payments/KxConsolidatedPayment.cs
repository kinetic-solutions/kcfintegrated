﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Kx.Core.PaymentProcessor.Payments
{
    public class KxConsolidatedPayment
    {
        protected int studentChargeId;
        protected string statementText;
        protected Decimal amount;

        public int StudentChargeId
        {
            get
            {
                return studentChargeId;
            }
            set
            {
                studentChargeId = value;
            }
        }
        public string StatementText
        {
            get
            {
                return statementText;
            }
            set
            {
                statementText = value;
            }
        }
        public Decimal Amount
        {
            get
            {
                return amount;
            }
            set
            {
                amount = value;
            }
        }

        public KxConsolidatedPayment()
        {

        }

        public KxConsolidatedPayment(int _studentchargeid, string _statementtext, Decimal _amount)
        {
            StudentChargeId = _studentchargeid;
            StatementText = _statementtext;
            Amount = _amount;
        }


    }
}
