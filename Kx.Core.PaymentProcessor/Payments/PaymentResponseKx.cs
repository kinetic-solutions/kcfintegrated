using System;
using System.Collections.Generic;
using System.Text;
using Kx.Core.Exceptions;
using Kx.Core.PaymentProcessor.Cache;

namespace Kx.Core.PaymentProcessor.Payments
{
    public partial class PaymentResponse
    {
        protected List<Payment> payments = null;
        protected List<PaymentCustomField> customFields = null;

        protected Decimal paymentAmount = 0.00M;
        protected Decimal depositAmount = 0.00M;

        protected KxWebPaymentCache cache = null;

        protected Decimal PaymentAmount
        {
            get
            { return paymentAmount; }
            set
            { paymentAmount = value; }
        }
        protected Decimal DepositAmount
        {
            get
            { return depositAmount; }
            set
            { depositAmount = value; }
        }

        public List<Payment> Payments
        {
            get
            {
                if (payments == null)
                {
                    payments = new List<Payment>();
                }
                return payments;
            }
            set
            {
                payments = value;
            }
        }

        public List<PaymentCustomField> CustomFields
        {
            get
            {
                if (customFields == null)
                {
                    customFields = new List<PaymentCustomField>();
                }
                return customFields;
            }
            set
            {
                customFields = value;
            }
        }

        public KxWebPaymentCache Cache
        {
            get
            {
                return cache;
            }
            set
            {
                cache = value;
            }
        }

        public string UniqueId
        {
            get
            {
                if (Authorised)
                {
                    return providerReference + InternalReference;
                }
                else
                {
                    return InternalReference;
                }
            }
        }

        public PaymentCustomField AddCustomField(string fieldName, string fieldData)
        {
            PaymentCustomField result = new PaymentCustomField();

            result.FieldName = fieldName;
            result.FieldData = fieldData;

            CustomFields.Add(result);

            return result;
        }

        public void GetResponseFromWpmRequest(PaymentRequest request)
        {
            try
            {
                Authorised = request.Success;
                InternalReference = request.BookingReference;

                Payments.Clear();
                foreach (Payment source in request.Payments)
                {
                    Payment pay = new Payment();
                    pay = (Payment)source.Clone();
                    Payments.Add(pay);
                }
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(null, "error processing", "", ex.Message);
            }
        }

       

      
    
    }
}
