using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Utility;
using Kx.Core.Exceptions;

namespace Kx.Core.PaymentProcessor.Payments
{
    public enum KxSystem { BandB, Student, Unspecified };

    public partial class PaymentRequest
    {
        protected KxSystem system = KxSystem.Unspecified;
        protected string customerId = "";

        protected List<Payment> payments = null;
        protected List<PaymentCustomField> customFields = null;
        protected List<KxConsolidatedPayment> consolidatedPayments = null;

        protected PaymentResponse response = null;

        protected bool success = false;

        protected string workflowGuid;

        public string CustomerId
        {
            get
            {
                return customerId;
            }   
            set
            {
                customerId = value;
            }
        }

        public List<Payment> Payments
        {
            get
            {
                if (payments == null)
                {
                    payments = new List<Payment>();
                }
                return payments;
            }
            set
            {
                payments = value;
            }
        }
        public List<PaymentCustomField> CustomFields
        {
            get
            {
                if (customFields == null)
                {
                    customFields = new List<PaymentCustomField>();
                }
                return customFields;
            }
            set
            {
                customFields = value;
            }
        }

        public PaymentResponse Response
        {
            get
            {
                return response;
            }
            set
            {
                response = value;
            }
        }

        public KxSystem System
        {
            get
            {
                return system;
            }
            set
            {
                system = value;
            }
        }

        public bool Success
        {
            get
            {
                return success;
            }
            set
            {
                success = value;
            }
        }

        public List<KxConsolidatedPayment> ConsolidatedPayments
        {
            get
            {
                if (consolidatedPayments == null)
                {
                    consolidatedPayments = new List<KxConsolidatedPayment>();
                }
                return consolidatedPayments;
            }
            set
            {
                consolidatedPayments = value;
            }
        }

        public string WorkflowGuid
        {
            get
            {
                return workflowGuid;
            }
            set
            {
                workflowGuid = value;
            }
        }

        public string PaymentPageID { get; set; }

        public KxConsolidatedPayment AddConsolidatedPayment(decimal amount,string statementText, int chargeId)
        {
            KxConsolidatedPayment result = new KxConsolidatedPayment();

            result.Amount = amount;
            result.StatementText = statementText;
            result.StudentChargeId = chargeId;

            ConsolidatedPayments.Add(result);

            return result;
        }

        public PaymentCustomField AddCustomField(string fieldName, string fieldData)
        {
            PaymentCustomField result = new PaymentCustomField();

            result.FieldName = fieldName;
            result.FieldData = fieldData;

            CustomFields.Add(result);

            return result;
        }

        public Payment AddPayment(string payType, decimal amount, string description,string transRef, DateTime? transDate)
        {
            Payment result = new Payment();
            result.PayType = Payment.StringToPaymentType(payType);
            result.Amount = amount;
            result.Description = description;
            result.TransactionDate = transDate;
            result.TransactionReference = transRef;

            Payments.Add(result);

            return result;
        }

        public string ToXml()
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
            sb.AppendLine("<KxPaymentProcessor>");
            sb.AppendLine(String.Format("	<System><![CDATA[{0}]]></System>", SystemToString()));
            sb.AppendLine(String.Format("	<CustomerID><![CDATA[{0}]]></CustomerID>", CustomerId));
            sb.AppendLine(String.Format("	<Forename><![CDATA[{0}]]></Forename>", firstName));
            sb.AppendLine(String.Format("	<Surname><![CDATA[{0}]]></Surname>", Surname));
        
            sb.AppendLine(String.Format("	<DOB><![CDATA[{0}]]></DOB>", KxDateTime.DateToWpmDate(Dob)));

            sb.AppendLine(String.Format("	<Address1><![CDATA[{0}]]></Address1>", Address1));
            sb.AppendLine(String.Format("	<Address2><![CDATA[{0}]]></Address2>", Address2));
            sb.AppendLine(String.Format("	<Address3><![CDATA[{0}]]></Address3>", Town));
            sb.AppendLine(String.Format("	<Address4><![CDATA[{0}]]></Address4>", County));
            sb.AppendLine(String.Format("	<Postcode><![CDATA[{0}]]></Postcode>", Postcode));
            sb.AppendLine(String.Format("	<EmailAddress><![CDATA[{0}]]></EmailAddress>", Email));
            sb.AppendLine(String.Format("	<BookingReference><![CDATA[{0}]]></BookingReference>", BookingReference));
            sb.AppendLine("	<Payments>");

            if (system == KxSystem.BandB)
            {
                if (GetFullPayment() != null)
                {
                    sb.AppendLine(GetFullPayment().ToXml());
                }
            }
            if (ConsolidatedPayments.Count > 0)
            {
                decimal totalAmount = 0;
                foreach (KxConsolidatedPayment pay in ConsolidatedPayments)
                {
                    totalAmount += pay.Amount;
                }

                sb.AppendLine(new Payment()
                {
                    Amount = totalAmount,
                    PayType = KxWpmPaymentType.PayNow,
                    Description = this.description
                }.ToXml());
            }
            else
            {
                foreach (Payment pay in Payments)
                {
                    sb.AppendLine(pay.ToXml());
                }
            }

            sb.AppendLine("	</Payments>");

            sb.AppendLine("	<CustomFields>");

            foreach (PaymentCustomField cf in CustomFields)
            {
                sb.AppendLine(cf.ToXml());
            }

            sb.AppendLine("	</CustomFields>");

            sb.AppendLine(String.Format("	<RedirectURL><![CDATA[{0}]]></RedirectURL>",ProviderCallbackUrl));

            if (TestMode)
            {
                sb.AppendLine("	<InterfaceStatus><![CDATA[TEST]]></InterfaceStatus>");
            }
            else
            {
                sb.AppendLine("	<InterfaceStatus><![CDATA[LIVE]]></InterfaceStatus>");
            }
            sb.AppendLine("</KxPaymentProcessor>");

            string result = sb.ToString();

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(result);

            return result;
        }

        protected Payment GetFullPayment()
        {
            Payment result = null;

            foreach (Payment pay in Payments)
            {
                if (pay.PayType == KxWpmPaymentType.PayNow)
                {
                    result = pay;
                    break;
                }
            }

            return result;
        }

        protected string SystemToString()
        {
            switch (system)
            {
                case KxSystem.BandB: return "B&B";
                default: return "Student";
            }
        }

        protected static KxSystem StringToSystem(string system)
        {
            switch (system)
            {
                case "B&B": return KxSystem.BandB;
                default: return KxSystem.Student;
            }
        }



        public static PaymentRequest LoadFromWpmXml(string xml)
        {
            PaymentRequest result = new PaymentRequest();

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);

            XmlNode node = doc.SelectSingleNode("//KxPaymentProcessor");

            result.System = PaymentRequest.StringToSystem(KxXmlUtils.GetNormalCaseNodeValue(node, "System", ""));
            result.CustomerId = KxXmlUtils.GetNormalCaseNodeValue(node, "CustomerID", "");
            result.FirstName = KxXmlUtils.GetNormalCaseNodeValue(node, "Forename", "");
            result.Surname = KxXmlUtils.GetNormalCaseNodeValue(node, "Surname", "");
            result.Dob = KxDateTime.WpmDateToDate(KxXmlUtils.GetNormalCaseNodeValue(node, "DOB", ""));
            result.Address1 = KxXmlUtils.GetNormalCaseNodeValue(node, "Address1", "");
            result.Address2 = KxXmlUtils.GetNormalCaseNodeValue(node, "Address2", "");
            result.Town = KxXmlUtils.GetNormalCaseNodeValue(node, "Address3", "");
            result.County = KxXmlUtils.GetNormalCaseNodeValue(node, "Address4", "");
            result.Postcode = KxXmlUtils.GetNormalCaseNodeValue(node, "Postcode", "");
            result.Email = KxXmlUtils.GetNormalCaseNodeValue(node, "EmailAddress", "");
            result.BookingReference = KxXmlUtils.GetNormalCaseNodeValue(node, "BookingReference", "");
            result.TestMode = KxXmlUtils.GetNormalCaseNodeValue(node, "InterfaceStatus", "") != "LIVE";
            result.ProviderUrl = KxXmlUtils.GetNormalCaseNodeValue(node, "RedirectURL", "");
            result.Success = KxXmlUtils.GetNormalCaseNodeValue(node, "Success", "").ToLower() == "true";

            XmlNodeList nodes = node.SelectNodes("Payments/Payment");

            foreach (XmlNode paynode in nodes)
            {
                Payment paymnt =  new Payment(paynode);
                
                result.Payments.Add(paymnt);
            }

            XmlNodeList cfnodes = node.SelectNodes("CustomFields/CustomField");

            foreach (XmlNode cfnode in cfnodes)
            {
                PaymentCustomField cf = new PaymentCustomField(cfnode);
                result.CustomFields.Add(cf);
            }
            return result;
        }

    }
}
