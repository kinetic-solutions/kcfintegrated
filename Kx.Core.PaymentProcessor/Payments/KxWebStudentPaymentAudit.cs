using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;
using System.Collections.Specialized;
using System.Web;

namespace Kx.Core.PaymentProcessor.Payments
{
    [Serializable]
    public class KxWebStudentPaymentAudit : KxBaseClass
    {
        #region Attributes & Properties

        protected string   _direction;
        protected string   _provider;
        protected string   _paymentType;
        protected string   _reference;
        protected int      _studentId;
        protected string   _Name;
        protected int      _applicationId;
        protected Guid     _applicationGuid;
        protected string   _chargeIds;
        protected decimal  _requestedAmount;
        protected decimal  _paidAmount;
        protected string   _currency;
        protected string   _authorisationCode;
        protected bool     _authorised;
        protected DateTime ?_timeStamp;
        protected string _raw;

        public int      AuditId
        {
            get
            { return base.dBIdentifier;}
        }

        public string   Direction
        {
            get
            { return _direction;}
            set
            { 
                AddToAudit("Direction", _direction, value);
                _direction = value;
            }
        }

        public string   Provider
        {
            get
            { return _provider;}
            set
            { 
                AddToAudit("Provider", _provider, value);
                _provider = value;
            }
        }

        public string   PaymentType
        {
            get
            { return _paymentType;}
            set
            { 
                AddToAudit("PaymentType", _paymentType, value);
                _paymentType = value;
            }
        }

        public string   Reference
        {
            get
            { return _reference;}
            set
            { 
                AddToAudit("Reference", _reference, value);
                _reference = value;
            }
        }

        public int      StudentId
        {
            get
            { return _studentId;}
            set
            { 
                AddToAudit("StudentId", _studentId, value);
                _studentId = value;
            }
        }

        public string   Name
        {
            get
            { return _Name;}
            set
            { 
                AddToAudit("Name", _Name, value);
                _Name = value;
            }
        }

        public int      ApplicationId
        {
            get
            { return _applicationId;}
            set
            { 
                AddToAudit("ApplicationId", _applicationId, value);
                _applicationId = value;
            }
        }

        public Guid ApplicationGuid
        {
            get
            { return _applicationGuid; }
            set
            {
                AddToAudit("ApplicationGuid", _applicationGuid, value);
                _applicationGuid = value;
            }
        }

        public string   ChargeIds
        {
            get
            { return _chargeIds;}
            set
            { 
                AddToAudit("ChargeIds", _chargeIds, value);
                _chargeIds = value;
            }
        }

        public decimal  RequestedAmount
        {
            get
            { return _requestedAmount;}
            set
            { 
                AddToAudit("RequestedAmount", _requestedAmount, value);
                _requestedAmount = value;
            }
        }

        public decimal  PaidAmount
        {
            get
            { return _paidAmount;}
            set
            { 
                AddToAudit("PaidAmount", _paidAmount, value);
                _paidAmount = value;
            }
        }

        public string   Currency
        {
            get
            { return _currency;}
            set
            { 
                AddToAudit("Currency", _currency, value);
                _currency = value;
            }
        }

        public string   AuthorisationCode
        {
            get
            { return _authorisationCode;}
            set
            { 
                AddToAudit("AuthorisationCode", _authorisationCode, value);
                _authorisationCode = value;
            }
        }

        public bool     Authorised
        {
            get
            { return _authorised;}
            set
            { 
                AddToAudit("Authorised", _authorised, value);
                _authorised = value;
            }
        }

        public DateTime ?TimeStamp
        {
            get
            { return _timeStamp;}
            set
            { 
                AddToAudit("TimeStamp", _timeStamp, value);
                _timeStamp = value;
            }
        }

        public string Raw
        {
            get { return _raw; }
            set
            {
                AddToAudit("Raw", _raw, value);
                _raw = value;
            }
        }

        private static string _hasRawColumn;
        #endregion 


        public KxWebStudentPaymentAudit(KxBaseClass parent):base(parent)
        {}


        public KxWebStudentPaymentAudit(KxBaseClass parent, XmlNode node):base(parent)
        {
             LoadFromXml("", node);
        }

        public KxWebStudentPaymentAudit(KxBaseClass parent,int      auditid):base(parent)
        {
             base.dBIdentifier = auditid;
             Load();
        }


        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("webstudentpaymentaudit", "WebStudentPaymentAudit", TargetType.Table);

            result.AutoKeyField = new KxDataValue("AuditId",AuditId,KxValueType.Int);


            if ((actiontype == ActionType.Update) && (AuditId == 0))
            {
                result.Action = ActionType.Insert;
            }
            else
            {
                result.Action = actiontype;
            }

            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {
                result.AddParameter("direction", _direction, KxValueType.String, 50, true);
                result.AddParameter("provider", _provider, KxValueType.String, 200, true);
                result.AddParameter("paymentType", _paymentType, KxValueType.String, 50, true);
                result.AddParameter("reference", _reference, KxValueType.String, 100, true);
                result.AddParameter("studentId", _studentId, KxValueType.Int, true);
                result.AddParameter("Name", _Name, KxValueType.String, 200, true);
                result.AddParameter("applicationId", _applicationId, KxValueType.Int, true);
                result.AddParameter("applicationguid", _applicationGuid, KxValueType.Guid, true);
                result.AddParameter("chargeIds", _chargeIds, KxValueType.String, 50, true);
                result.AddParameter("requestedAmount", _requestedAmount, KxValueType.Decimal, true);
                result.AddParameter("paidAmount", _paidAmount, KxValueType.Decimal, true);
                result.AddParameter("currency", _currency, KxValueType.String, 20, true);
                result.AddParameter("authorisationCode", _authorisationCode, KxValueType.String, 50, true);
                result.AddParameter("authorised", _authorised, KxValueType.Boolean, true);
                result.AddParameter("timeStamp", _timeStamp, KxValueType.DateTime, true);

                const string STR_No = "No";
                const string STR_Yes = "Yes";
                try
                {
                    if (string.IsNullOrEmpty(_hasRawColumn))
                    {
                        KxData data = new KxData("dd", "SELECT TOP 1 Raw FROM WebStudentPaymentAudit", TargetType.Static);
                        if (data.Execute())
                        {
                            _hasRawColumn = STR_Yes;
                        }
                        else
                        {
                            _hasRawColumn = STR_No;
                        }
                    }
                }
                catch (Exception ex)
                {
                    KxLogger.LogActivity(this, "Finding Raw column", ex.Message, "");
                    _hasRawColumn = STR_No;
                }

                if (_hasRawColumn == STR_Yes)
                {
                    result.AddParameter("raw", _raw, KxValueType.String);
                }
            }
            return result;
        }


        public override  void LoadFromXml(string xml,XmlNode node)
        {
            dBIdentifier  = KxXmlUtils.GetNodeValue(node,"auditid",0);
            _direction  = KxXmlUtils.GetNodeValue(node,"direction","");
            _provider  = KxXmlUtils.GetNodeValue(node,"provider","");
            _paymentType  = KxXmlUtils.GetNodeValue(node,"paymenttype","");
            _reference  = KxXmlUtils.GetNodeValue(node,"reference","");
            _studentId  = KxXmlUtils.GetNodeValue(node,"studentid",0);
            _Name  = KxXmlUtils.GetNodeValue(node,"name","");
            _applicationId = KxXmlUtils.GetNodeValue(node, "applicationid", 0);
            _applicationGuid = KxXmlUtils.GetNodeValue(node, "applicationguid", Guid.Empty);
            _chargeIds  = KxXmlUtils.GetNodeValue(node,"chargeids","");
            _requestedAmount  = KxXmlUtils.GetNodeValue(node,"requestedamount",(decimal)0.00M);
            _paidAmount  = KxXmlUtils.GetNodeValue(node,"paidamount",(decimal)0.00M);
            _currency  = KxXmlUtils.GetNodeValue(node,"currency","");
            _authorisationCode  = KxXmlUtils.GetNodeValue(node,"authorisationcode","");
            _authorised  = KxXmlUtils.GetNodeValue(node,"authorised",false);
            _timeStamp  = KxXmlUtils.GetNodeValueDT(node,"timestamp",null);
            _raw = KxXmlUtils.GetNodeValue(node, "raw", string.Empty);
        }

        public static void LogOutBoundPayment(PaymentRequest request, NameValueCollection inputs)
        {
            try
            {
                KxWebStudentPaymentAudit audit = new KxWebStudentPaymentAudit(null);
                
                audit.Direction = "OUT";
                try
                {
                    audit.ApplicationId = Convert.ToInt32(request.ApplicationId);
                }
                catch { }
                try
                {
                    audit.StudentId = Convert.ToInt32(request.StudentId);
                }
                catch { }

                if (!String.IsNullOrEmpty(request.Currency))
                {
                    audit.Currency = request.Currency;
                }
                audit.Reference = request.BookingReference;
                audit.RequestedAmount = request.Amount;
                audit.Provider = request.PaymentProvider;
                audit.PaymentType = request.PaymentType;
                audit.Name = request.FullName;
                audit.ApplicationGuid = request.ApplicationGuid;
                if (request.ConsolidatedPayments.Count > 0)
                {
                    string chargeids = "";

                    foreach (KxConsolidatedPayment pay in request.ConsolidatedPayments)
                    {
                        chargeids += pay.StudentChargeId+"="+pay.Amount.ToString()+"\r\n";
                    }

                    audit.ChargeIds = chargeids;
                }
                audit.TimeStamp = DateTime.Now;

                StringBuilder raw = new StringBuilder();
                raw.AppendLine(string.Format("{0}={1}", "url", request.ProviderUrl));
                foreach (string key in inputs.AllKeys)
                {
                    raw.AppendLine(string.Format("{0}={1}", key, inputs[key]));
                }

                audit.Raw = raw.ToString();

                audit.Save();
            }
            catch
            {

            }
        }

        public static void LogInBoundPayment(PaymentResponse response, Dictionary<string, string> inputs)
        {
            try
            {
                KxWebStudentPaymentAudit audit = new KxWebStudentPaymentAudit(null);

                audit.Direction = "IN";
                audit.Reference = response.InternalReference;
                audit.Authorised = response.Authorised;
                audit.AuthorisationCode = response.ProviderReference;
                if(response.OriginalRequest != null)
                {
                    audit.ApplicationGuid = response.OriginalRequest.ApplicationGuid;
                    int applicationId = 0;
                    if (int.TryParse(response.OriginalRequest.ApplicationId, out applicationId))
                    {
                        audit.ApplicationId = applicationId;
                    }
                    audit.RequestedAmount = response.OriginalRequest.Amount;
                    audit.Currency = response.OriginalRequest.Currency;
                    audit.Reference = response.OriginalRequest.UniqueIdentifier.ToString();
                    audit.Provider = response.OriginalRequest.PaymentProvider;
                    audit.PaymentType = response.OriginalRequest.PaymentType;
                }
                try
                {
                    audit.PaidAmount = response.Amount;
                }
                catch
                {
                    audit.PaidAmount = 0.00M;
                }
                
                audit.TimeStamp = DateTime.Now;

                StringBuilder raw = new StringBuilder();
                foreach (string key in inputs.Keys)
                {
                    raw.AppendLine(string.Format("{0}={1}", key, inputs[key]));
                }
                audit.Raw = raw.ToString();

                if(HttpContext.Current != null)
                {
                    if (HttpContext.Current.Session[string.Format("KxWebStudentPaymentAudit-{0}", audit.Reference)] != null)
                    {
                        return;
                    }
                }

                audit.Save();
                if(HttpContext.Current != null)
                {
                    HttpContext.Current.Session[string.Format("KxWebStudentPaymentAudit-{0}", audit.Reference)] = "Yes";
                }
            }
            catch
            {

            }
        }
    }
}

