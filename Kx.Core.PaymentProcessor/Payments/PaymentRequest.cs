using Kx.Core.Data;
using Kx.Core.Utility;
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Kx.Core.PaymentProcessor.Payments
{
    public partial class PaymentRequest
    {
        private string bookingReference = "";
        private string description = "";
        private string currency = "GBP";
        private decimal amount;
        private string title = "";
        private string firstName = "";
        private string surname = "";
        private string houseNumber = "";
        private string address1 = "";
        private string address2 = "";
        private string town = "";
        private string county = "";
        private string country  = "";
        private string countryCode = "";
        private string postcode  = "";
        private string telephone  = "";
        private string fax  = "";
        private string email  = "";
        private string paymentProvider = "";
        private string paymentMethod = "";
        private string paymentType = "";
        private string providerAccountId = "";
        private string providerUrl = "";
        private string confirmationPageUrl = "";
        private string cancellationPageUrl = "";
        private string providerCallbackUrl = "";
        private string providerPassword = "";
        private bool testMode = false;
        private bool testTransactionOutcome = true;
        private string providerMode = "";
        private string providerHash = "";
        private string providerEncriptionUrl = "";
        private string unknownPageUrl = "";
        private string merchantDisplayName = "";
        private string clientEmail = "";
        private string providerReference = "";
        private string providerStudentId = "";
        private string studentId = "";
        private string transactionType = "";
        private string secPayPassword = "";
        private int siteId = 0;
        private string sh1Key = "";
        private string cardPaymentMethod = "";
        private string ucasNumber = "";

        private Guid applicationGuid = Guid.Empty;

        private string eraDepartmentId = "";
        private string eraContactId = "";
        private string eraCustomerId = "";
        private string eraItemList = "";
        private string ledgerCode = "";
        private string nominalCode = "";
        private string systemId = "";
        private string templateFile = "";

        private string customValue1 = "";
        private string customValue2 = "";
        private string customValue3 = "";
        private string customValue4 = "";
        private string customValue5 = "";
        private string customValue6 = "";

        private string applicationId = "";
        private string siteCode = "";
        private int academicYearId = 0;

        private DateTime? dob = null;
        private DateTime transactionDate = DateTime.Now;
        private bool secPaySendEmail = false;

        private Guid uniqueIdentifier = Guid.NewGuid();

        public int WebPaymentCacheId = 0;

        //provider referernce string

        public static string SecretKey = "$MYDOGSARECALLEDSASCHAANDDAISY$";

        // Card Details
        private string cardholderName = "";
        private string cardNumber = "";
        private string cardType = "";
        private string expiryDate;
        private string cV2 = "";
        private int issueNumber = 0;
        private string validFrom;


        //extended properties
        private int installmentCount = 0;
        private DateTime endDate;
        private DateTime startDate;
        private DateTime firstDate;
        private bool recurringType;
        private bool directDebitType;
        private string paymentDates;
        private string paymentAmounts;

        private string sortCode;
        private string accountId;
        private string accountName;
        private string bankName;
        private string branchName;

        
		public string BookingReference
		{
			get{return bookingReference;}
			set{bookingReference = value;}
		}
		public string Description
		{
			get{return description;}
			set{description = value;}
		}
        public int AcademicYearId
        {
            get { return academicYearId; }
            set { academicYearId = value; }
        }
		public decimal Amount
		{
			get{return amount;}
			set{amount = value;}
		}
		public string Currency
		{
			get{return currency;}
			set{currency = value;}
		}

        public string MerchantCountry { get; set; } = "GBR"; // Used by KxPayments in conjunction with currency
		public string Title
		{
			get{return title;}
			set{title = value;}
		}

        public Guid ApplicationGuid
        {
            get
            { return applicationGuid; }
            set
            { applicationGuid = value; }
        }
		public string FirstName
		{
			get{return firstName;}
			set{firstName = value;}
		}
		public string Surname
		{
			get{return surname;}
			set{surname = value;}
		}
		public string HouseNumber
		{
			get{return houseNumber;}
			set{houseNumber = value;}
		}
		public string Address1
		{
			get{return address1;}
			set{address1 = value;}
		}
		public string Address2
		{
			get{return address2;}
			set{address2 = value;}
		}
		public string Town
		{
			get{return town;}
			set{town = value;}
		}
		public string County
		{
			get{return county;}
			set{county = value;}
		}
		public string Country
		{
			get{return country;}
			set{country = value;}
        }

        public string CountryCode
        {
            get
            {
                if (string.IsNullOrWhiteSpace(countryCode))
                {
                    countryCode = GetCountryCode(country) ?? "GB";
                }

                return countryCode;
            }
            set
            {
                countryCode = value;
            }
        }

        public string Postcode
		{
			get{return postcode;}
			set{postcode = value;}
		}
		public string Telephone
		{
			get{return telephone;}
			set{telephone = value;}
		}
		public string Fax
		{
			get{return fax;}
			set{fax = value;}
		}
		public string Email
		{
			get{return email;}
			set{email = value;}
		}
		public string ConfirmationPageUrl
		{
			get{return confirmationPageUrl;}
			set{confirmationPageUrl = value;}
		}
		public string CancellationPageUrl
		{
			get{return cancellationPageUrl;}
			set{cancellationPageUrl = value;}
		}

        public string UcasNumber
        {
            get { return ucasNumber; }
            set { ucasNumber = value; }
        }

        public DateTime TransactionDate
        {
            get
            { return transactionDate; }
            set
            { transactionDate = value; }
        }

        public string PaymentMethod
        {
            get
            { return paymentMethod; }
            set
            { paymentMethod = value; }
        }

        public string CardPaymentMethod
        {
            get
            { return cardPaymentMethod; }
            set
            { cardPaymentMethod = value; }
        }

        public string PaymentType
        {
            get
            { return paymentType; }
            set
            { paymentType = value; }
        }

        public string ProviderStudentId
        {
            get { return providerStudentId; }
            set { providerStudentId = value; }
        }

        public string StudentId
        {
            get { return studentId; }
            set { studentId = value; }
        }

        public string PaymentProvider
		{
			get{return paymentProvider;}
			set{paymentProvider = value;}
		}
        public string ProviderAccountId
        {
            get
            { return providerAccountId; }
            set
            { providerAccountId = value;}
        }

        public string ProviderCallbackUrl
        {
            get
            {
                return providerCallbackUrl;
            }
            set
            {
                providerCallbackUrl = value;
            }
        }

        public string ProviderMode
        {
            get
            {
                return providerMode;
            }
            set
            {
                if (providerMode == value)
                    return;
                providerMode = value;
            }
        }

        public string SecPayPassword
        {
            get
            {
                return secPayPassword;
            }
            set
            {
                if (secPayPassword == value)
                    return;
                secPayPassword = value;
            }
        }

        public string ProviderHash
        {
            get
            {
                return providerHash;
            }
            set
            {
                if (providerHash == value)
                    return;
                providerHash = value;
            }
        }

        public string ProviderPassword
        {
            get
            { return providerPassword; }
            set
            { providerPassword = value; }
        }

        public string ProviderUrl
        {
            get { return providerUrl; }
            set { providerUrl = value; }
        }

        public string SH1Key
        {
            get { return sh1Key; }
            set { sh1Key = value; }
        }

        public bool TestMode
        {
            get
            { return testMode; }
            set
            { testMode = value; }
        }
        public bool TestTransactionOutcome
        {
            get
            { return testTransactionOutcome; }
            set
            {testTransactionOutcome = value; }
        }

        public bool SecPaySendEmail
        {
            get
            {
                return secPaySendEmail;
            }
            set
            {
                secPaySendEmail = value;
            }
        }

        public string ProviderEncriptionUrl
        {
            get
            {
                return providerEncriptionUrl;
            }
            set
            {
                providerEncriptionUrl = value;
            }
        }
        public string UnknownPageUrl
        {
            get
            {
                return unknownPageUrl;
            }
            set
            {
                unknownPageUrl = value;
            }
        }
        public Guid UniqueIdentifier
        {
            get
            {
                return uniqueIdentifier;
            }
            set
            {
                uniqueIdentifier = value;
            }
        }

        public string UniqueId
        {
            get
            {
                return uniqueIdentifier.ToString();
            }
            
        }

        public string UniqueIdClean
        {
            get
            {
                return uniqueIdentifier.ToString("N");
            }
        }

        public string LedgerCode
        {
            get
            {
                return ledgerCode;
            }
            set
            {
                ledgerCode = value;
            }

        }

        public string NominalCode
        {
            get
            {
                return nominalCode;
            }
            set
            {
                nominalCode = value;
            }
        }

        public string SystemId
        {
            get
            {
                return systemId;
            }
            set
            {
                systemId = value;
            }
        }

        public string MerchantDisplayName
        {
            get
            {
                return merchantDisplayName;
            }
            set
            {
                merchantDisplayName = value;
            }
        }

        public string ClientEmail
        {
            get { return clientEmail; }
            set { clientEmail = value; }
        }

        /// <summary>
        /// "Auth" or "Capture"
        /// </summary>
        public string TransactionType
        {
            get { return transactionType; }
            set { transactionType = value; }
        }

        public string CardholderName
        {

            get
            { return cardholderName; }
            set
            { cardholderName = value; }
        }

        public string CardNumber
        {

            get
            { return cardNumber; }
            set
            { cardNumber = value; }
        }

        public string CardType
        {

            get
            { return cardType; }
            set
            { cardType = value; }
        }

        public string ExpiryDate
        {

            get
            { return expiryDate; }
            set
            { expiryDate = value; }
        }

        public string CV2
        {

            get
            { return cV2; }
            set
            { cV2 = value; }
        }

        public int IssueNumber
        {

            get
            { return issueNumber; }
            set
            { issueNumber = value; }
        }

        public string ValidFrom
        {

            get
            { return validFrom; }
            set
            { validFrom = value; }
        }

        public string FullName
        {
            get
            { 
                return  (Title == "" ? "" : Title + " ") + (FirstName == "" ? "" : FirstName + " ") + (Surname == "" ? "" : Surname + " ");
            }
        }

        public string FullAddress
        {
            get
            {
                return (houseNumber == "" ? "" : houseNumber + " ") + (Address1 == "" ? "" : Address1 + " ") + (Address2 == "" ? "" : Address2 + " ");
            }
        }

        public DateTime? Dob
        {
            get
            {
                return dob;
            }
            set
            {
                dob = value;
            }
        }

        public PaymentRequest()
        {
            //
            // TODO: Add constructor logic here
            //
        }
        public string ProviderReference
        {
            get
            {
                return providerReference;
            }
            set
            {
                providerReference = value;
            }
        }

        public string TemplateFile
        {
            get { return templateFile; }
            set { templateFile = value; }
        }

        //extended properties
        public int InstallmentCount
        {
            get
            { return installmentCount; }
            set
            { installmentCount = value; }
        }

        public DateTime EndDate
        {
            get
            { return endDate; }
            set
            { endDate = value; }
        }

        public DateTime StartDate
        {
            get
            { return startDate; }
            set
            { startDate = value; }
        }

        public DateTime FirstDate
        {
            get
            { return firstDate; }
            set
            { firstDate = value; }
        }

        public bool RecurringType
        {
            get
            { return recurringType; }
            set
            { recurringType = value; }
        }

        public bool DirectDebitType
        {
            get
            { return directDebitType; }
            set
            { directDebitType = value; }
        }

        // ERA Properties
        public string EraDepartmentId
        {
            get
            { return eraDepartmentId; }
            set
            { eraDepartmentId = value; }
        }

        public string EraContactId
        {
            get
            { return eraContactId; }
            set
            { eraContactId = value; }
        }

        public string EraCustomerId
        {
            get
            { return eraCustomerId; }
            set
            { eraCustomerId = value; }
        }

        public string EraItemList
        {
            get
            { return eraItemList; }
            set
            { eraItemList = value; }
        }

        public string PaymentDates
        {
            get
            { return paymentDates;  }
            set
            { paymentDates = value; }
        }
        public string PaymentAmounts
        {
            get
            { return paymentAmounts;  }
            set
            { paymentAmounts = value; }
        }

        public int SiteId
        {
            get
            { return siteId; }
            set
            {  siteId = value;  }
        }

        public string CustomValue1
        {
            get
            {
                return customValue1;
            }
            set
            {
                customValue1 = value;
            }
        }
        public string CustomValue2
        {
            get
            {
                return customValue2;
            }
            set
            {
                customValue2 = value;
            }
        }
        public string CustomValue3
        {
            get
            {
                return customValue3;
            }
            set
            {
                customValue3 = value;
            }
        }
        public string CustomValue4
        {
            get
            {
                return customValue4;
            }
            set
            {
                customValue4 = value;
            }
        }
        public string CustomValue5
        {
            get
            {
                return customValue5;
            }
            set
            {
                customValue5 = value;
            }
        }
        public string CustomValue6
        {
            get
            {
                return customValue6;
            }
            set
            {
                customValue6 = value;
            }
        }

        public string ApplicationId
        {
            get
            {
                return applicationId;
            }
            set
            {
                applicationId = value;
            }
        }
        public string SiteCode
        {
            get
            {
                return siteCode;
            }
            set
            {
                siteCode = value;
            }
        }

        public string SortCode
        {
            get
            { return sortCode; }
            set
            { sortCode = value; }
        }
        public string AccountId
        {
            get
            { return accountId; }
            set
            { accountId = value; }
        }

        public string AccountName
        {
            get
            { return accountName; }
            set
            { accountName = value; }
        }

        public string BankName
        {
            get
            { return bankName; }
            set
            { bankName = value; }
        }
        public string BranchName
        {
            get
            { return branchName; }
            set
            { branchName = value; }
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            sb.Append(BookingReference + "<br/>");
            sb.Append(Description + "<br/>");
            sb.Append(Currency + "<br/>");
            sb.Append(Amount.ToString() + "<br/>");
            sb.Append(Title + "<br/>");
            sb.Append(FirstName + "<br/>");
            sb.Append(Surname + "<br/>");
            sb.Append(HouseNumber + "<br/>");
            sb.Append(Address1 + "<br/>");
            sb.Append(Address2 + "<br/>");
            sb.Append(Town + "<br/>");
            sb.Append(County + "<br/>");
            sb.Append(Country + "<br/>");
            sb.Append(Postcode + "<br/>");
            sb.Append(Telephone + "<br/>");
            sb.Append(Fax + "<br/>");
            sb.Append(Email + "<br/>");
            sb.Append(PaymentProvider + "<br/>");
            sb.Append(PaymentMethod + "<br/>");
            sb.Append(ProviderAccountId + "<br/>");
            sb.Append(ProviderUrl + "<br/>");
            sb.Append(ConfirmationPageUrl + "<br/>");
            sb.Append(CancellationPageUrl + "<br/>");
            sb.Append(TestMode.ToString() + "<br/>");
            sb.Append(TestTransactionOutcome.ToString() + "<br/>");
            sb.Append(CustomValue1.ToString() + "<br/>");
            sb.Append(CustomValue2.ToString() + "<br/>");
    
            return sb.ToString();

        }

        public static string GetCountryCode(string country)
        {
            KxData data = new KxData("Countries", "dbo.Countries", TargetType.Table);
            data.AddKeyField("countryname", country, KxValueType.String);
            if (data.Execute())
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(data.XmlResults);
                XmlNodeList nodes = doc.SelectNodes(@"//countries/datarow");
                foreach (XmlNode node in nodes)
                {
                    return KxXmlUtils.GetNodeValue(node, "countrycode", string.Empty);
                }
            }

            return null;
        }

    }
}
