﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Xml;
using Kx.Core.Utility;
using Kx.Core.Exceptions;

namespace Kx.Core.PaymentProcessor.Payments
{
    public enum KxWpmPaymentType { PayNow, Deposit, RCP, DD };

   [Serializable]
    public class Payment : ICloneable
    {
        protected KxWpmPaymentType payType = KxWpmPaymentType.PayNow;

        protected Decimal amount = 0.00M;
        protected string description = "";
        protected string transactionReference = "";
        protected DateTime? transactionDate = null;

        protected List<PaymentInstalment> instalments = null;

        public Decimal Amount
        {
            get
            {
                return amount;
            }
            set
            {
                amount = value;
            }
        }

        public string Description
        {
            get
            {
                return description;
            }
            set
            {
                description = value;
            }
        }

        public string TransactionReference
        {
            get
            {
                return transactionReference;
            }
            set
            {
                transactionReference = value;
            }
        }

        public DateTime? TransactionDate
        {
            get
            {
                return transactionDate;
            }
            set
            {
                transactionDate = value;
            }
        }

        public List<PaymentInstalment> Instalments
        {
            get
            {
                if (instalments == null)
                {
                    instalments = new List<PaymentInstalment>();
                }
                return instalments;
            }
            set
            {
                instalments = value;
            }
        }

        public KxWpmPaymentType PayType
        {
            get
            {
                return payType;
            }
            set
            {
                payType = value;
            }
        }

        public decimal SurchargeAmount { get; set; }
        public bool SurchargeApplied
        {
            get
            {
                return SurchargeAmount > 0m;
            }
        }

        public Payment()
        {

        }

        public Payment(XmlNode node)
        {
            LoadFromXml(node);
        }

        public PaymentInstalment AddInstalment(Decimal amount, DateTime? date, string description)
        {
            PaymentInstalment result = new PaymentInstalment();

            result.Amount = amount;
            result.Description = description;
            result.InstalmentDate = date;

            Instalments.Add(result);

            return result;
        }

       

        public string ToXml()
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine(String.Format("            <Payment type=\"{0}\">",PaymentTypeToString(PayType)));
            if (payType == KxWpmPaymentType.PayNow || payType == KxWpmPaymentType.Deposit)
            {
                sb.AppendLine(String.Format("			<Amount><![CDATA[{0}]]></Amount>",Amount.ToString("F")));
                sb.AppendLine(String.Format("			<Description><![CDATA[{0}]]></Description>",Description));			
            }
            if (Instalments.Count > 0)
            {
                sb.AppendLine("			<Instalments>");

                foreach (PaymentInstalment instal in Instalments)
                {
                    sb.AppendLine(instal.ToXml());
                }

                sb.AppendLine("			</Instalments>");
            }
            sb.AppendLine("		</Payment>");		

            return sb.ToString();
        }

        public static string PaymentTypeToString(KxWpmPaymentType paytype)
        {
            switch (paytype)
            {
                case KxWpmPaymentType.DD: return "DD";
                case KxWpmPaymentType.RCP: return "RCP";
                case KxWpmPaymentType.Deposit: return "DEPOSIT";
                default: return "PAYNOW";
            }
        }

        public static KxWpmPaymentType StringToPaymentType(string paytype)
        {
            switch (paytype)
            {
                case "DD"      : return KxWpmPaymentType.DD;
                case "RCP"     : return KxWpmPaymentType.RCP;
                case "DEPOSIT" : return KxWpmPaymentType.Deposit;
                default        : return KxWpmPaymentType.PayNow;
            }
        }

        public void LoadFromXml(XmlNode node)
        {
            PayType = Payment.StringToPaymentType(node.Attributes["type"].Value);
            Amount = KxXmlUtils.GetNormalCaseNodeValue(node, "Amount", 0.00M);
            SurchargeAmount = KxXmlUtils.GetNormalCaseNodeValue(node, "CardCharge", 0.00M);
            Description = KxXmlUtils.GetNormalCaseNodeValue(node, "Description", "");
            TransactionReference = KxXmlUtils.GetNormalCaseNodeValue(node, "TransactionReference", "");
            TransactionDate = KxDateTime.WpmDateToDate(KxXmlUtils.GetNormalCaseNodeValue(node, "Date", ""));
            if (TransactionDate == null)
            {
                TransactionDate = DateTime.Now;

            }

            XmlNodeList nodes = node.SelectNodes("Instalments/Instalment");

            foreach (XmlNode instalnode in nodes)
            {
                PaymentInstalment instal = new PaymentInstalment(instalnode);
                Instalments.Add(instal);
            }

        }

        public object Clone()
        {
            MemoryStream ms = new MemoryStream();
            BinaryFormatter bf = new BinaryFormatter();
            bf.Serialize(ms, this);
            ms.Position = 0;
            object obj = bf.Deserialize(ms);
            ms.Close();
            return obj;
        }



    }
}
