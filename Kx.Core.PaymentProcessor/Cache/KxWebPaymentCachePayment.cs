using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;

namespace Kx.Core.PaymentProcessor.Cache
{
    // TODO: SJA: Add new field for logging the amount paid as well as the amount requested.
    //            make sure that the system works without the column there as well!
    [Serializable]
    public class KxWebPaymentCachePayment : KxBaseClass
    {
        #region Attributes & Properties

        protected Guid     cacheId;
        protected int      chargeId;
        protected decimal  requestedAmount;
        protected string   description;
        protected string paymentType;

        public KxWebPaymentCache Owner
        {
            get
            {
                return (KxWebPaymentCache)Parent;
            }
        }

        public int      PaymentCacheId
        {
            get
            { return base.dBIdentifier;}
        }

        public Guid     CacheId
        {
            get
            { return cacheId;}
            set
            { 
                cacheId = value;
            }
        }

        public int      ChargeId
        {
            get
            { return chargeId;}
            set
            { 
                chargeId = value;
            }
        }

        public decimal  RequestedAmount
        {
            get
            { return requestedAmount;}
            set
            { 
                requestedAmount = value;
            }
        }

        public string   Description
        {
            get
            { return description;}
            set
            { 
                description = value;
            }
        }

        public string PaymentType
        {
            get
            { return paymentType; }
            set
            {
                paymentType = value;
            }
        }


        #endregion 


		public KxWebPaymentCachePayment()
		{
		}

		public KxWebPaymentCachePayment(KxBaseClass parent)
			: base(parent)
        {}


        public KxWebPaymentCachePayment(KxBaseClass parent, XmlNode node):base(parent)
        {
             LoadFromXml("", node);
        }

        public KxWebPaymentCachePayment(KxBaseClass parent,int      paymentcacheid):base(parent)
        {
             base.dBIdentifier = paymentcacheid;
             Load();
        }
        
        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("webpaymentcachepayment", "WebPaymentCachePayment", TargetType.Table);


            if ((actiontype == ActionType.Update) && (guidIdentifier == Guid.Empty))
            {
                if (changed)
                {
                    guidIdentifier = Guid.NewGuid();
                }
                result.Action = ActionType.Insert;
            }
            else
            {
                result.Action = actiontype;
            }

            result.AutoKeyField = new KxDataValue("paymentcacheid", dBIdentifier, KxValueType.Int);
            
            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {
                result.AddParameter("cacheId", Owner.CacheId, KxValueType.Guid, true);
                result.AddParameter("chargeId", chargeId, KxValueType.Int, true);
                result.AddParameter("requestedAmount", requestedAmount, KxValueType.Decimal, true);
                result.AddParameter("description", description, KxValueType.String, 500, true);
                result.AddParameter("paymenttype", paymentType, KxValueType.String, 50, true);
            }
            return result;
        }


        public override  void LoadFromXml(string xml,XmlNode node)
        {
            dBIdentifier  = KxXmlUtils.GetNodeValue(node,"paymentcacheid",0);
            cacheId  = KxXmlUtils.GetNodeValue(node,"cacheid",Guid.Empty);
            chargeId  = KxXmlUtils.GetNodeValue(node,"chargeid",0);
            requestedAmount  = KxXmlUtils.GetNodeValue(node,"requestedamount",(decimal)0.00M);
            description  = KxXmlUtils.GetNodeValue(node,"description","");
            paymentType = KxXmlUtils.GetNodeValue(node, "paymenttype", "");
        }
    }
}

