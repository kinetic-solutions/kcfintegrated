using System;
using System.Linq;
using System.Collections.Generic;
using Kx.Core.PaymentProcessor.Payments;

namespace Kx.Core.PaymentProcessor.Cache
{
    public enum PaymentStatus
    {
        None,
        Authorised,
        Declined
    };

    public class PaymentCache
    {
        #region Fields

        private readonly SortedList<string, PaymentRequest> payments = new SortedList<string, PaymentRequest>();
        public int WebPaymentCacheID;

        #endregion

        #region Public Methods

        public bool AddPayment(PaymentRequest request)
        {
            Guid newguid = CreateDBCache(request);
            request.WebPaymentCacheId = WebPaymentCacheID;

            string name = request.BookingReference;

            if (newguid != Guid.Empty)
            {
                name = newguid.ToString("N");
            }

            request.UniqueIdentifier = newguid;
            request.BookingReference = name;

            if (payments.ContainsKey(name))
            {
                payments[name] = request;
            }
            else
            {
                payments.Add(name, request);
            }
            return true;
        }

        /// <summary>
        /// Gets the Original Request from payment cache dictionary
        /// </summary>
        /// <param name="internalReference"></param>
        /// <returns></returns>
        public PaymentRequest GetOriginalRequest(string internalReference)
        {
            PaymentRequest result = null;

            if (payments.ContainsKey(internalReference))
            {
                result = payments[internalReference];
            }

            return result;
        }

        /// <summary>
        /// Performs folowing actions
        /// - Updates the KxWebPaymentCache in database 
        /// - Get original request from payment cache 
        /// - Removes the payment from Payments cache dictionary
        /// </summary>
        /// <param name="response"></param>
        /// <returns></returns>
        public PaymentRequest GetPayment(PaymentResponse response)
        {
            KxWebPaymentCache cache = UpdateCache(response, false);

            PaymentRequest result = null;

            if (payments.ContainsKey(response.InternalReference))
            {
                result = payments[response.InternalReference];
                payments.Remove(response.InternalReference);
            }
            else if (cache != null && payments.ContainsKey(cache.Reference))
            {
                result = payments[cache.Reference];
                payments.Remove(cache.Reference);
            }

            if (cache != null && result == null)
            {
                result = RestoreRequestFromCache(cache);
            }

            if (cache != null && cache.CacheId != Guid.Empty)
            {
                response.Cache = cache;
                result.BookingReference = cache.Reference;
            }


            response.InternalReference = result.BookingReference;

            return result;
        }

        public PaymentRequest GetPaymentSafe(PaymentResponse response, bool payToStudyDoubleRefresh = false)
        {
            KxWebPaymentCache cache = UpdateCache(response, payToStudyDoubleRefresh);

            PaymentRequest result = null;

            if (payments.ContainsKey(response.InternalReference))
            {
                result = payments[response.InternalReference];
                payments.Remove(response.InternalReference);
            }
            else if (cache != null && payments.ContainsKey(cache.Reference))
            {
                result = payments[cache.Reference];
                //payments.Remove(cache.Reference);
            }

            if (cache != null && result == null)
            {
                result = RestoreRequestFromCache(cache);
            }


            if (cache != null)
            {
                result.BookingReference = cache.Reference;
                response.InternalReference = cache.Reference;
                response.Cache = cache;
            }

            return result;
        }



        public PaymentResponse GetResponseFromCache(string bookingref)
        {
            var cache = new KxWebPaymentCache(null, new Guid(bookingref));

            return RestoreResponseFromCache(cache);
        }

        #endregion

        #region Private Methods

        private PaymentResponse RestoreResponseFromCache(KxWebPaymentCache cache)
        {
            var response = new PaymentResponse();
            response.InternalReference = cache.Reference;
            response.Amount = cache.PaidAmount;
            response.Amount2 = cache.PaidAmount2;
            response.ProviderReference = cache.AuthorisationCode;
            response.ProviderReference2 = cache.AuthorisationCode2;
            response.TransactionDate = cache.TimeStampIn;
            response.Authorised = cache.Authorised;
            response.Currency = cache.Currency;
            response.OriginalRequest = RestoreRequestFromCache(cache);
            if (cache.Provider == "WPMKX" && cache.Name.StartsWith("SUR:"))
            {
                string amountString = cache.Name.Replace("SUR:", "");
                decimal surchargeAmount = 0m;
                if (decimal.TryParse(amountString, out surchargeAmount))
                {
                    response.Surcharge = surchargeAmount;
                    response.SurchargeApplied = surchargeAmount > 0m;
                }
            }
            if (cache.CustomFields != null && cache.CustomFields.Items.Count > 0)
            {
                foreach (KxWebPaymentCacheCustomField cf in cache.CustomFields.Items)
                {
                    response.AddCustomField(cf.Name, cf.Value);
                }
            }

            return response;
        }

        private PaymentRequest RestoreRequestFromCache(KxWebPaymentCache cache)
        {
            var result = new PaymentRequest();

            result.BookingReference = cache.Reference;
            result.PaymentProvider = cache.Provider;
            result.ApplicationGuid = cache.TransactionGuId;
            result.Currency = cache.Currency;
            result.CustomerId = cache.CustomerId;
            result.UniqueIdentifier = cache.CacheId;
            if (cache.CustomerIdentifier > 0)
            {
                result.StudentId = cache.CustomerId;
            }
            if (cache.TransactionId > 0)
            {
                result.ApplicationId = cache.TransactionId.ToString();
            }

            foreach (KxWebPaymentCachePayment pay in cache.Payments.Items)
            {
                if (pay.ChargeId > 0)
                {
                    result.AddConsolidatedPayment(pay.RequestedAmount, pay.Description, pay.ChargeId);
                }
                else
                {
                    result.Amount = pay.RequestedAmount;
                    result.Description = pay.Description;
                    result.PaymentType = pay.PaymentType;
                    result.AddPayment(pay.PaymentType, pay.RequestedAmount, pay.Description, cache.AuthorisationCode,
                        cache.TimeStampIn);
                }
            }

            if (cache.CustomFields.Items.Count > 0)
            {
                foreach (KxWebPaymentCacheCustomField cf in cache.CustomFields.Items)
                {
                    result.AddCustomField(cf.Name, cf.Value);
                }
            }

            return result;
        }

        private KxWebPaymentCache UpdateCache(PaymentResponse response, bool ignoreDupes)
        {
            KxWebPaymentCache result = null;
            try
            {
                var id = new Guid(response.InternalReference);

                result = new KxWebPaymentCache(null, id);

                if (result.CacheId == id && (result.TimeStampIn == null || ignoreDupes))
                {
                    result.Authorised = response.Authorised;
                    result.AuthorisationCode = response.ProviderReference;
                    result.AuthorisationCode2 = response.ProviderReference2;

                    result.PaidAmount = response.Amount;
                    result.PaidAmount2 = response.Amount2;
                    result.TimeStampIn = DateTime.Now;
                    if (result.Provider == "WPMKX" && response.Surcharge > 0m)
                    {
                        result.Name = string.Format("SUR:{0}", response.Surcharge);
                    }
                    result.Changed = true;
                    result.Save();
                }
                else
                {
                    result = null;
                }
            }
            catch
            {
                result = null;
            }

            return result;
        }

        public KxWebPaymentCache UpdateCache(PaymentRequest request, bool ignoreDupes)
        {
            KxWebPaymentCache result = null;
            try
            {
                var id = new Guid(request.BookingReference);

                result = new KxWebPaymentCache(null, id);

                if (result.CacheId == id && (result.TimeStampIn == null || ignoreDupes))
                {
                    if (request.CustomFields.Count > 0)
                    {
                        foreach (PaymentCustomField cf in request.CustomFields)
                        {
                            result.CustomFields.Add(cf.FieldName, cf.FieldData);
                        }
                    }
                    result.Changed = true;
                    result.Save();
                }
                else
                {
                    result = null;
                }
            }
            catch
            {
                result = null;
            }

            return result;
        }

        private Guid CreateDBCache(PaymentRequest request)
        {
            Guid result = Guid.Empty;

            try
            {
                var cache = new KxWebPaymentCache(null);

                cache.Reference = request.BookingReference;
                cache.Provider = request.PaymentProvider;
                cache.TransactionGuId = request.ApplicationGuid;
                cache.Currency = request.Currency;
                cache.TimeStampOut = DateTime.Now;
                cache.CustomerId = request.CustomerId;

                try
                {
                    cache.CustomerIdentifier = Convert.ToInt32(request.StudentId);
                }
                catch
                {
                }

                try
                {
                    cache.TransactionId = Convert.ToInt32(request.ApplicationId);
                }
                catch
                {
                }

                if (request.Payments.Count > 0)
                {
                    foreach (Payment pay in request.Payments)
                    {
                        if (string.IsNullOrEmpty(request.PaymentType))
                        {
                            cache.Payments.Add(0, pay.Amount, pay.Description, pay.PayType.ToString().ToUpper());
                        }
                        else
                        {
                            cache.Payments.Add(0, pay.Amount, pay.Description, request.PaymentType);
                        }
                    }
                }
                else if (request.ConsolidatedPayments.Count > 0)
                {
                    foreach (KxConsolidatedPayment pay in request.ConsolidatedPayments)
                    {
                        cache.Payments.Add(pay.StudentChargeId, pay.Amount, pay.StatementText, request.PaymentType);
                    }
                }
                else
                {
                    cache.Payments.Add(0, request.Amount, request.Description, request.PaymentType);
                }

                if (request.CustomFields.Count > 0)
                {
                    foreach (PaymentCustomField cf in request.CustomFields)
                    {
                        cache.CustomFields.Add(cf.FieldName, cf.FieldData);
                    }
                }
                cache.Changed = true;
                if (cache.Save())
                {
                    result = cache.CacheId;
                    WebPaymentCacheID = cache.WebPaymentCacheId;
                }
            }
            catch
            {
            }

            return result;
        }


        #endregion

        #region Obsolete

        [Obsolete]
        private bool AttachResponseToRequest(string name, PaymentResponse response)
        {
            bool result = false;

            KxWebPaymentCache cache = UpdateCache(response, true);

            if (payments.ContainsKey(name))
            {
                PaymentRequest pr = payments[name];
                response.OriginalRequest = pr;
                pr.Response = response;
                result = true;
            }
            else if (cache != null && payments.ContainsKey(cache.Reference))
            {
                PaymentRequest pr = payments[cache.Reference];
                response.OriginalRequest = pr;
                pr.Response = response;
                result = true;
            }

            return result;
        }

        #endregion
    }
}