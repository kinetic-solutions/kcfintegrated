using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Exceptions;
using Kx.Core.Utility;

namespace Kx.Core.PaymentProcessor.Cache
{
    public class KxWebPaymentCacheCustomFields : KxBaseClass
    {
        private List<KxWebPaymentCacheCustomField> items = new List<KxWebPaymentCacheCustomField>();

        public KxWebPaymentCache Owner
        {
            get
            {
                return (KxWebPaymentCache)Parent;
            }
        }

        public KxWebPaymentCacheCustomFields()
        {
            
        }

        public KxWebPaymentCacheCustomFields(KxBaseClass parent)
            : base(parent)
        {
            
        }                                                                                                  

        public List<KxWebPaymentCacheCustomField> Items
        {
            get
            { return items; }
        }

        public KxWebPaymentCacheCustomField this[int index]
        {
            get
            { return items[index]; }
            set
            { items[index] = value; }
        }

        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = null;

            if (actiontype == ActionType.Select)
            {
                result = new KxData("webpaymentcachecustomfield", "WebPaymentCacheCustomField", TargetType.Table, ActionType.Select);

                result.AddKeyField("CacheId", Owner.CacheId, KxValueType.Guid);
            }
            return result;
        }

        public override bool LoadFromXml(string xml, string xpath)
        {
            items.Clear();
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xml);

                XmlNodeList nodes = doc.SelectNodes(@"//results/" + xpath + "/datarow");

                foreach (XmlNode node in nodes)
                {
                    if (node != null)
                    {
                        KxWebPaymentCacheCustomField item = new KxWebPaymentCacheCustomField(Owner, node);
                        items.Add(item);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "LoadFromXml", ex.Message, xpath);
                LastError = ex.Message;
                return false;
            }
        }

        public KxWebPaymentCacheCustomField GetWebPaymentCacheCustomField(int id)
        {
            KxWebPaymentCacheCustomField result = null;

            foreach (KxWebPaymentCacheCustomField i in Items)
            {
                if (i.CustomFieldCacheId == id)
                {
                    result = i;
                    break;
                }
            }
            return result;
        }

        public void Add(string Name, string value)
        {
            KxWebPaymentCacheCustomField cf = new KxWebPaymentCacheCustomField(Owner);
            cf.Name = Name;
            cf.Value = value;
            cf.Changed = true;
            Items.Add(cf);
        }

    }
}

