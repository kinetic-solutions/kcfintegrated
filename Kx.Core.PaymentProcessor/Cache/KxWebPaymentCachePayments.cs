using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Exceptions;
using Kx.Core.Utility;

namespace Kx.Core.PaymentProcessor.Cache
{
    public class KxWebPaymentCachePayments : KxBaseClass
    {
        private List<KxWebPaymentCachePayment> items = new List<KxWebPaymentCachePayment>();

        public KxWebPaymentCache Owner
        {
            get
            {
                return (KxWebPaymentCache)Parent;
            }
        }

        public KxWebPaymentCachePayments()
        {
            
        }

        public KxWebPaymentCachePayments(KxBaseClass parent) : base(parent)
        {
            
        }              

        public List<KxWebPaymentCachePayment> Items
        {
            get
            { return items; }
        }

        public KxWebPaymentCachePayment this[int index]
        {
            get
            { return items[index]; }
            set
            { items[index] = value; }
        }

        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = null;

            if (actiontype == ActionType.Select)
            {
                result = new KxData("webpaymentcachepayment", "WebPaymentCachePayment", TargetType.Table, ActionType.Select);

                result.AddKeyField("CacheId", Owner.CacheId, KxValueType.Guid);
            }
            return result;
        }

        public override bool LoadFromXml(string xml, string xpath)
        {
            items.Clear();
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xml);

                XmlNodeList nodes = doc.SelectNodes(@"//results/" + xpath + "/datarow");

                foreach (XmlNode node in nodes)
                {
                    if (node != null)
                    {
                        KxWebPaymentCachePayment item = new KxWebPaymentCachePayment(Owner, node);
                        items.Add(item);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "LoadFromXml", ex.Message, xpath);
                LastError = ex.Message;
                return false;
            }
        }

        public KxWebPaymentCachePayment GetWebPaymentCachePayment(int id)
        {
            KxWebPaymentCachePayment result = null;

            foreach (KxWebPaymentCachePayment i in Items)
            {
                if (i.PaymentCacheId == id)
                {
                    result = i;
                    break;
                }
            }
            return result;
        }

        public void Add(int chargeId, decimal requestedAmount, string description,string paymentType)
        {
            KxWebPaymentCachePayment pay = new KxWebPaymentCachePayment(Owner);
            pay.ChargeId = chargeId;
            pay.PaymentType = paymentType;
            pay.RequestedAmount = requestedAmount;
            pay.Description = description;
            pay.Changed = true;
            Items.Add(pay);

        }

    }
}

