using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;

namespace Kx.Core.PaymentProcessor.Cache
{
    [Serializable]
    public class KxWebPaymentCacheCustomField : KxBaseClass
    {
        #region Attributes & Properties

        protected Guid     cacheId;
        protected string   name;
        protected string   value;

        public KxWebPaymentCache Owner
        {
            get
            {
                return (KxWebPaymentCache)Parent;
            }
        }

        public int      CustomFieldCacheId
        {
            get
            { return base.dBIdentifier;}
        }

        public Guid     CacheId
        {
            get
            { return cacheId;}
            set
            { 
                AddToAudit("CacheId", cacheId, value);
                cacheId = value;
            }
        }

        public string   Name
        {
            get
            { return name;}
            set
            { 
                AddToAudit("Name", name, value);
                name = value;
            }
        }

        public string   Value
        {
            get
            { return value;}
            set
            { 
                AddToAudit("Value", value, value);
                this.value = value;
            }
        }


        #endregion 


		public KxWebPaymentCacheCustomField()
		{
		}

		public KxWebPaymentCacheCustomField(KxBaseClass parent)
			: base(parent)
        {}


        public KxWebPaymentCacheCustomField(KxBaseClass parent, XmlNode node):base(parent)
        {
             LoadFromXml("", node);
        }

        public KxWebPaymentCacheCustomField(KxBaseClass parent,int      customfieldcacheid):base(parent)
        {
             base.dBIdentifier = customfieldcacheid;
             Load();
        }


        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("webpaymentcachecustomfield", "WebPaymentCacheCustomField", TargetType.Table);


            if ((actiontype == ActionType.Update) && (guidIdentifier == Guid.Empty))
            {
                if (changed)
                {
                    guidIdentifier = Guid.NewGuid();
                }
                result.Action = ActionType.Insert;
            }
            else
            {
                result.Action = actiontype;
            }

            result.AutoKeyField = new KxDataValue("customfieldcacheid", dBIdentifier, KxValueType.Int);


            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {
                result.AddParameter("cacheId", Owner.CacheId, KxValueType.Guid, true);
                result.AddParameter("Name", Name, KxValueType.String, 1000, true);
                result.AddParameter("value", value, KxValueType.String, 1000, true);
            }
            return result;
        }


        public override  void LoadFromXml(string xml,XmlNode node)
        {
            dBIdentifier  = KxXmlUtils.GetNodeValue(node,"customfieldcacheid",0);
            cacheId  = KxXmlUtils.GetNodeValue(node,"cacheid",Guid.Empty);
            Name  = KxXmlUtils.GetNodeValue(node,"name","");
            value  = KxXmlUtils.GetNodeValue(node,"value","");
        }
    }
}

