using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;

namespace Kx.Core.PaymentProcessor.Cache
{
    [Serializable]
    public class KxWebPaymentCache : KxBaseClass
    {
        #region Attributes & Properties

        protected bool Loaded = false;
        protected string   provider;
        protected string   reference;
        protected string   customerId;
        protected int      customerIdentifier;
        protected string   name;
        protected int      transactionId;
        protected Guid     transactionGuId;
        protected decimal  paidAmount;
        protected decimal paidAmount2;
        protected string   currency;
        protected string   authorisationCode;
        protected string authorisationCode2;
        protected bool     authorised;
        protected DateTime ?timeStampOut;
        protected DateTime ?timeStampIn;
        protected bool processed = false;
        protected int webPaymentCacheId;

        protected KxWebPaymentCacheCustomFields customFields = null;
        protected KxWebPaymentCachePayments payments = null;

        public Guid     CacheId
        {
            get
            { return base.guidIdentifier;}
        }

        public string   Provider
        {
            get
            { return provider;}
            set
            { 
                provider = value;
            }
        }

        public string   Reference
        {
            get
            { return reference;}
            set
            { 
                reference = value;
            }
        }

        public string CustomerId
        {
            get
            { return customerId; }
            set
            {
                customerId = value;
            }
        }

        public int      CustomerIdentifier
        {
            get
            { return customerIdentifier; }
            set
            {
                customerIdentifier = value;
            }
        }

        public string   Name
        {
            get
            { return name;}
            set
            { 
                name = value;
            }
        }

        public int      TransactionId
        {
            get
            { return transactionId; }
            set
            {
                transactionId = value;
            }
        }

        public Guid TransactionGuId
        {
            get
            { return transactionGuId; }
            set
            {
                transactionGuId = value;
            }
        }

        public decimal  PaidAmount
        {
            get
            { return paidAmount;}
            set
            { 
                paidAmount = value;
            }
        }

        public decimal PaidAmount2
        {
            get
            { return paidAmount2; }
            set
            {
                paidAmount2 = value;
            }
        }

        public string   Currency
        {
            get
            { return currency;}
            set
            { 
                currency = value;
            }
        }

        public string   AuthorisationCode
        {
            get
            { return authorisationCode;}
            set
            { 
                authorisationCode = value;
            }
        }

        public string AuthorisationCode2
        {
            get
            { return authorisationCode2; }
            set
            {
                authorisationCode2 = value;
            }
        }

        public bool     Authorised
        {
            get
            { return authorised;}
            set
            { 
                authorised = value;
            }
        }

        public DateTime ?TimeStampOut
        {
            get
            { return timeStampOut;}
            set
            { 
                timeStampOut = value;
            }
        }

        public DateTime ?TimeStampIn
        {
            get
            { return timeStampIn;}
            set
            { 
                timeStampIn = value;
            }
        }

        public KxWebPaymentCacheCustomFields CustomFields
        {
            get
            {
                if (customFields == null)
                {
                    customFields = new KxWebPaymentCacheCustomFields(this);
                }
                return customFields;
            }
            set
            {
                customFields = value;
            }
        }
        public KxWebPaymentCachePayments Payments
        {
            get
            {
                if (payments == null)
                {
                    payments = new KxWebPaymentCachePayments(this);
                }
                return payments;
            }
            set
            {
                payments = value;
            }
        }

        public bool Processed
        {
            get
            {
                return processed;
            }
            set
            {
                processed = value;
            }
        }

        public int WebPaymentCacheId
        {
            get
            {
                return webPaymentCacheId;
            }
        }

        #endregion 

		public KxWebPaymentCache()
		{
			guidIdentifier = Guid.NewGuid();
		}


		public KxWebPaymentCache(KxBaseClass parent):base(parent)
        {
            guidIdentifier = Guid.NewGuid();
        }


        public KxWebPaymentCache(KxBaseClass parent, XmlNode node):base(parent)
        {
             LoadFromXml("", node);
        }

        public KxWebPaymentCache(KxBaseClass parent,Guid cacheid):base(parent)
        {
             base.guidIdentifier = cacheid;
             Load();
        }


        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("webpaymentcache", "WebPaymentCache", TargetType.Table);


            if ((actiontype == ActionType.Update) && (!Loaded))
            {
                result.Action = ActionType.Insert;
            }
            else
            {
                result.Action = actiontype;
            }

            result.AddKeyField("cacheId",guidIdentifier,KxValueType.Guid);


            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {
                   result.AddParameter("provider", provider, KxValueType.String, 200, true);
                   result.AddParameter("reference", reference, KxValueType.String, 100, true);
                   result.AddParameter("customerId", customerId, KxValueType.String, true);
                   result.AddParameter("customerIdentifier", customerIdentifier, KxValueType.Int, true);
                   result.AddParameter("Name", Name, KxValueType.String, 200, true);
                   result.AddParameter("transactionId", transactionId, KxValueType.Int, true);
                   result.AddParameter("transactionGuId", transactionGuId, KxValueType.Guid, true);
                   result.AddParameter("paidAmount", paidAmount, KxValueType.Decimal, true);
                   result.AddParameter("paidAmount2", paidAmount2, KxValueType.Decimal, true);
                   result.AddParameter("currency", currency, KxValueType.String, 20, true);
                   result.AddParameter("authorisationCode", authorisationCode, KxValueType.String, 50, true);
                   result.AddParameter("authorisationCode2", authorisationCode2, KxValueType.String, 50, true);
                   result.AddParameter("authorised", authorised, KxValueType.Boolean, true);
                   result.AddParameter("timeStampOut", timeStampOut, KxValueType.DateTime, true);
                   result.AddParameter("timeStampIn", timeStampIn, KxValueType.DateTime, true);
                   result.AddParameter("processed", processed, KxValueType.Boolean, true);
            }
            return result;
        }


        public override  void LoadFromXml(string xml,XmlNode node)
        {
            guidIdentifier  = KxXmlUtils.GetNodeValue(node,"cacheid",Guid.Empty);
            provider  = KxXmlUtils.GetNodeValue(node,"provider","");
            reference  = KxXmlUtils.GetNodeValue(node,"reference","");
            customerId = KxXmlUtils.GetNodeValue(node, "customerid", "");
            customerIdentifier = KxXmlUtils.GetNodeValue(node, "customeridentifier", 0);
            Name  = KxXmlUtils.GetNodeValue(node,"name","");
            transactionId = KxXmlUtils.GetNodeValue(node, "transactionid", 0);
            transactionGuId = KxXmlUtils.GetNodeValue(node, "transactionguid", Guid.Empty);
            paidAmount  = KxXmlUtils.GetNodeValue(node,"paidamount",(decimal)0.00M);
            paidAmount2 = KxXmlUtils.GetNodeValue(node, "paidamount2", (decimal)0.00M);
            currency  = KxXmlUtils.GetNodeValue(node,"currency","");
            authorisationCode  = KxXmlUtils.GetNodeValue(node,"authorisationcode","");
            authorisationCode2 = KxXmlUtils.GetNodeValue(node, "authorisationcode2", "");
            authorised  = KxXmlUtils.GetNodeValue(node,"authorised",false);
            timeStampOut  = KxXmlUtils.GetNodeValueDT(node,"timestampout",null);
            timeStampIn  = KxXmlUtils.GetNodeValueDT(node,"timestampin",null);
            processed = KxXmlUtils.GetNodeValue(node, "processed", false);
            webPaymentCacheId = KxXmlUtils.GetNodeValue(node, "webpaymentcacheid", 0);

            Payments.Load();
            CustomFields.Load();

            Loaded = true;
        }

        private int GetWebPaymentCacheId()
        {
            KxData result = new KxData("WebPaymentCache", "WebPaymentCache", TargetType.Table, ActionType.Select);
            int iWebPaymentCacheID = 0;

            result.AddKeyField("CacheId", CacheId, KxValueType.String);

            if (result.Execute())
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(result.XmlResults);

                XmlNodeList nodes = doc.SelectNodes("//webpaymentcache/datarow/webpaymentcacheid");
                if (nodes.Count == 1)
                    iWebPaymentCacheID = Convert.ToInt32(nodes[0].InnerText);
            }
            return iWebPaymentCacheID;
        }

        public override bool Save()
        {
            bool result = base.Save();
            webPaymentCacheId = GetWebPaymentCacheId();
            return result;
        }

    }
}

