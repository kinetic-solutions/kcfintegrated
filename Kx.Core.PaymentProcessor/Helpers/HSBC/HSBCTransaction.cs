using System;
using CCCPICOMLib;
using System.Net;
using System.Text;
using System.Reflection;

using Kx.Core.PaymentProcessor.Payments;

namespace Kx.Core.PaymentProcessor.Helpers.HSBC
{

	public enum ReturnCodes {	Approved,UserCancelled,DeclinedUnknown,InvalidCard,NoResponse,InvalidAmount,InvalidCurrency,
								DuplicateOrderId,FraudshieldRejection,FraudshieldReview,InvalidData,CPIConfiguration,
								StorefrontConfiguration,ConnectionTimout,CookieError,BrowserError,CPICommunication}
	/// <summary>
	/// Summary description for HSBCTransaction.
	/// </summary>
	public class HSBCTransaction
	{
		#region Attributes
		
		private string merchantData;//O, 512 max
		private string orderDesc;//R, 1-54
		private string orderId;// R 1-36
		private string purchaseAmount;//R 1 to 9999999 no decimal point
		private string transactionType;//O Auth or Capture(default)
		private string userid;//O
		private string billingAddress1;//R 1-60
		private string billingAddress2;//O
		private string billingCity;//R
		private string billingCountry;//R
		private string billingCounty;//O
		private string billingFirstName;//R
		private string billingLastName;//R
		private string billingPostal;//R
		private string shopperEmail;//R

        private string directResultUrl = "";
        private string returnUrl = "";
        private string mode = "";//System.Configuration.ConfigurationManager.AppSettings["HSBC_Mode"];
        private string purchaseCurrency = "";//System.Configuration.ConfigurationManager.AppSettings["HSBC_Currency"];
        private string storefrontId = "";//System.Configuration.ConfigurationManager.AppSettings["HSBC_AccountID"];
        private string hashKey = "";// System.Configuration.ConfigurationManager.AppSettings["HSBC_HashKey"];
		#endregion

		public string GetOrderHash(string Source)
		{
			string source = Source.Remove(0,1);

			string[] Values = source.Split('|');

			var ohc = new OrderHash();

			return ohc.GenerateOrderHash(source,hashKey);
		}

		public string DebugGetOrderHash(string Source)
		{
			string test = Source;

			var ohc = new OrderHash();

			string testkey = "zEeWQNKelqPE2DRFueuDq1QrASjux2lM";

			return ohc.GenerateOrderHash(test,testkey);
		}

		public string getMilliseconds(DateTime dte)
		{
            long BaseTime = DateTime.Parse("1970-01-01T00:00:00Z").Ticks;

			TimeSpan st = new TimeSpan(dte.Ticks-BaseTime);

			return Convert.ToInt64(st.TotalMilliseconds).ToString();
		}

		private string getTimeStamp()
		{
			return getMilliseconds(DateTime.UtcNow);
		}

		#region Properties
                           
		public string CpiDirectResultUrl
		{
			get
			{ return directResultUrl;}
		}

		public string CpiReturnUrl
		{
			get
			{ return returnUrl;}
		}

		public string MerchantData
		{
			get
			{ return merchantData;}
			set
			{ merchantData = value;}
		}

		public string Mode
		{
			get
			{ return mode;}
		}

		public string OrderDesc
		{
			get
			{ return orderDesc;}
			set
			{ orderDesc = value;}
		}

		public string OrderId
		{
			get
			{ return orderId;}
			set
			{ orderId = value;}
		}

		public string PurchaseAmount
		{
			get
			{ return purchaseAmount;}
			set
			{ purchaseAmount = value;}
		}

		public string PurchaseCurrency
		{
			get
			{ return purchaseCurrency;}
		}

		public string StorefrontId
		{
			get
			{ return storefrontId;}
		}

		public string TimeStamp
		{
			get
			{ return getTimeStamp();}
		}

		public string TransactionType
		{
			get
			{ return transactionType;}
			set
			{ transactionType = value;}
		}

		public string Userid
		{
			get
			{ return userid;}
			set
			{ userid = value;}
		}

		public string BillingAddress1
		{
			get
			{ return billingAddress1;}
			set
			{ billingAddress1 = value;}
		}

		public string BillingAddress2
		{
			get
			{ return billingAddress2;}
			set
			{ billingAddress2 = value;}
		}

		public string BillingCity
		{
			get
			{ return billingCity;}
			set
			{ billingCity = value;}
		}

		public string BillingCountry
		{
			get
			{ return billingCountry;}
			set
			{ billingCountry = value;}
		}

		public string BillingCounty
		{
			get
			{ return billingCounty;}
			set
			{ billingCounty = value;}
		}

		public string BillingFirstName
		{
			get
			{ return billingFirstName;}
			set
			{ billingFirstName = value;}
		}

		public string BillingLastName
		{
			get
			{ return billingLastName;}
			set
			{ billingLastName = value;}
		}

		public string BillingPostal
		{
			get
			{ return billingPostal;}
			set
			{ billingPostal = value;}
		}

		public string ShippingAddress1
		{
			get
			{ return billingAddress1;}
		}

		public string ShippingAddress2
		{
			get
			{ return billingAddress2;}
		}

		public string ShippingCity
		{
			get
			{ return billingCity;}
		}

		public string ShippingCountry
		{
			get
			{ return billingCountry;}
		}

		public string ShippingCounty
		{
			get
			{ return billingCounty;}
		}

		public string ShippingFirstName
		{
			get
			{ return billingFirstName;}
		}

		public string ShippingLastName
		{
			get
			{ return billingLastName;}
		}

		public string ShippingPostal
		{
			get
			{ return billingPostal;}
		}

		public string ShopperEmail
		{
			get
			{ return shopperEmail;}
			set
			{ shopperEmail = value;}
		}

		public PaymentRequest RequestDetails
		{
			set
			{
				SetupProvider(value);
			}
		}
		#endregion


		#region Constructors

		/// <summary>
		/// Default Constructor. 
		/// </summary>
		public HSBCTransaction()
		{
		}

		#endregion
	
		private string GetPostData()
		{
			Type type = this.GetType();

			StringBuilder sb = new StringBuilder();

			foreach (PropertyInfo pi in type.GetProperties())
			{
				if (pi.CanRead)
				{
					string PName = pi.Name;
					object PValue = pi.GetValue(this, null);

					if (PValue != null)
					{
						sb.Append(PName.Trim()+"="+PValue.ToString().Trim()+"^");
					}
				}
			}
			string result = sb.ToString();
			return result;
		}
		
		private void SetupProvider(PaymentRequest requestDetails)
		{
            merchantData = "";
			orderDesc = requestDetails.Description;
			orderId = requestDetails.BookingReference;
			purchaseAmount = Convert.ToInt32(requestDetails.Amount * 100).ToString();
            transactionType = requestDetails.TransactionType;
			
			billingAddress1 = requestDetails.Address1;
			billingAddress2 = requestDetails.Address2;
			billingCity = requestDetails.Town;
			billingCountry = requestDetails.Country;
			billingCounty = requestDetails.County;
			billingFirstName = requestDetails.FirstName;
			billingLastName = requestDetails.Surname;
			billingPostal = requestDetails.Postcode;
			shopperEmail = requestDetails.Email;

            mode = requestDetails.ProviderMode;
            purchaseCurrency = requestDetails.Currency;
            storefrontId = requestDetails.ProviderAccountId;
            hashKey = requestDetails.ProviderHash;
            directResultUrl = requestDetails.ProviderUrl;
            returnUrl = requestDetails.ProviderUrl;


		}
	}
}
