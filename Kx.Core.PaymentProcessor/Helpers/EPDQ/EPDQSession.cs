using System;
using System.Net;
using System.Text;
using Kx.Core.PaymentProcessor.Payments;

namespace Kx.Core.PaymentProcessor.Helpers.EPDQ
{
	/// <summary>
	/// Summary description for EPDQSession.
	/// </summary>
	public class EPDQSession
	{
		#region properties
		// Members
		private string _serverUrl;
		private string _accountID;
		private string _password;
		private PaymentRequest _paymentRequestDetails;

		// Properties

		/// <summary>
		/// Payment Request Details
		/// </summary>
		public PaymentRequest PaymentRequestDetails
		{
			get{return _paymentRequestDetails;}
			set{_paymentRequestDetails = value;}
		}

		/// <summary>
		/// Server Url
		/// </summary>
		public string ServerUrl
		{
			get{return _serverUrl;}
			set{_serverUrl = value;}
		}

		/// <summary>
		/// Account ID
		/// </summary>
		public string AccountID
		{
			get{return _accountID;}
			set{_accountID = value;}
		}

		/// <summary>
		/// Password
		/// </summary>
		public string Password
		{
			get{return _password;}
			set{_password = value;}
		}
		#endregion

		/// <summary>
		/// Default Constructor
		/// </summary>
		public EPDQSession()
		{
		}

		/// <summary>
		/// Constructor
		/// </summary>
		public EPDQSession(string serverUrl, string accountID, string password)
		{
			_serverUrl = serverUrl;
			_accountID = accountID;
			_password = password;
		}


		/// <summary>
		/// Get Form Variable
		/// </summary>
		/// <returns></returns>
		public string GetFormVariable()
		{
			// Initialise
			using (WebClient client = new WebClient())
			{
				// Add the headers
				client.Headers.Add("Content-Type", "application/x-www-form-urlencoded");

				// Setup the post data
				string postData = string.Format("clientid={0}&password={1}&oid={2}&chargetype={3}&currencycode={4}&total={5}", _accountID, _password, PaymentRequestDetails.BookingReference, "Auth", "826", _paymentRequestDetails.Amount);

				// Convert Post Data to Bytes
				byte[] postDataBytes = Encoding.ASCII.GetBytes(postData);

				// Call the web server
				byte[] response = client.UploadData(ServerUrl, "POST", postDataBytes);

				// Return the Result
				return Encoding.ASCII.GetString(response);
			}
		}
	}
}
