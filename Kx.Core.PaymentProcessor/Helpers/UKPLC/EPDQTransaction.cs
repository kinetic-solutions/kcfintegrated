using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using EPDQCom;
using Kx.Core.Configuration;
using Kx.Core.PaymentProcessor.Payments;

namespace Kx.Core.PaymentProcessor.Helpers.UKPLC
{
	/// <summary>
	/// An Interface to @UKplc's EPDQ COM-Object, allowing us to make payments via Barclays ePDQ.
	/// </summary>
	public class EPDQTransaction
	{

		#region Configurable Variables

		// These are the configurable parameters which are presenty set up for Swansea
		// If we are in Test mode then set the processing mode as follows:
		//		Y = Approve All, 
		//		R = Random Results, 
		//		N = Reject All
		//		If we are not in test mode, processing mode is ignored
        bool live = KxDbConfiguration.GetConfigurationBool("TestMode",false);		// Set to to false to enable test mode.
        char processingMode = Convert.ToChar(KxDbConfiguration.GetConfiguration("UKPLCEPDQ_ProcessingMode"));			// Ignored if live = true.
        string storeID = KxDbConfiguration.GetConfiguration("UKPLCEPDQ_StoreID");
        string userName = KxDbConfiguration.GetConfiguration("UKPLCEPDQ_AccountID");
        string password = DecryptString(KxDbConfiguration.GetConfiguration("UKPLCEPDQ_Password"), PaymentRequest.SecretKey);	

		#endregion

		#region Other Declarations

		string filePath;				// Temporary file path for the payment provider. Swansea set this to "c:\windows\temp"
		int currencyID;					// 826 = UK Pounds.

		string cardHolderName;			// Name as it appears on the card.
		string creditCardNumber;		// Credit Card Number.
		string creditCardExpiry;		// mm/yy expiry date.
		string securityCode;			// Security code from the signature strip of the card.

		string address1;				// Address line 1.
		string address2;				// Address line 2.
		string address3;				// Address line 3.
		string address4;				// Address line 4.
		string postCode;				// Post Code.

		// Switch only
		int issueNumber;				// IssueNumber.
		string creditCardStartDate;		// mm/yy start date of the card.

		decimal orderAmount;			// Order amount.

		// Output variables
		string transactionID;
		string responseMessage;
		string transactionType;
		string errorMessage;

		clsTransactionParameters parameters = new clsTransactionParameters();
		clsEPDQEngine engine = null;

		#endregion

		#region Input Properties

		/// <summary>
		/// Test Mode.
		/// </summary>
		public bool Live
		{ 
			get { return live; }
			set { live = value; }
		}

		/// <summary>
		/// Processing Mode.
		/// If we are in Test mode then set the processingMode Y =Approve all, R = Random results, N = Reject all
		/// </summary>
		public char ProcessingMode
		{ 
			get { return processingMode; }
			set { processingMode = value; }
		}

		/// <summary>
		/// File path for temporary files.
		/// Swansea set this to "c:\windows\temp\"
		/// </summary>
		public string FilePath
		{ 
			get { return filePath; }
			set { filePath = value; }
		}

		/// <summary>
		/// ePDQ StoreID - which account we're paying into.
		/// </summary>
		public string StoreID
		{ 
			get { return storeID; }
			set { storeID = value; }
		}

		/// <summary>
		/// ePDQ Account Username.
		/// </summary>
		public string Username
		{ 
			get { return userName; }
			set { userName = value; }
		}

		/// <summary>
		/// ePDQ Account Password.
		/// </summary>
		public string Password
		{ 
			set { password = value; }
		}

		/// <summary>
		/// CurrencyID. 826 = UK Pounds.
		/// </summary>
		public int CurrencyID
		{ 
			get { return currencyID; }
			set { currencyID = value; }
		}

		/// <summary>
		/// Card Holder's Name.
		/// </summary>
		public string CardHolderName
		{ 
			get { return cardHolderName; }
			set { cardHolderName = value; }
		}

		/// <summary>
		/// Credit Card Number.
		/// </summary>
		public string CreditCardNumber 
		{
			get { return creditCardNumber; }
			set { creditCardNumber = value; }
		}

		/// <summary>
		/// Expiry Date of card.
		/// </summary>
		public string CreditCardExpiry
		{ 
			get { return creditCardExpiry; }
			set { creditCardExpiry = value; }
		}

		/// <summary>
		/// Security Code - The CV2 Number on back of card.
		/// </summary>
		public string SecurityCode
		{ 
			get { return securityCode; }
			set { securityCode = value; }
		}

		/// <summary>
		/// Issue Number - Switch Only.
		/// </summary>
		public int IssueNumber
		{ 
			get { return issueNumber; }
			set { issueNumber = value; }
		}

		/// <summary>
		/// Start Date of card.
		/// </summary>
		public string StartDate
		{ 
			get { return creditCardStartDate; }
			set { creditCardStartDate = value; }
		}

		/// <summary>
		/// Address Line 1.
		/// </summary>
		public string Address1
		{ 
			get { return address1; }
			set { address1 = value; }
		}

		/// <summary>
		/// Address Line 2.
		/// </summary>
		public string Address2
		{ 
			get { return address2; }
			set { address2 = value; }
		}

		/// <summary>
		/// Address Line 3.
		/// </summary>
		public string Address3
		{ 
			get { return address3; }
			set { address3 = value; }
		}

		/// <summary>
		/// Address Line 4.
		/// </summary>
		public string Address4
		{ 
			get { return address4; }
			set { address4 = value; }
		}

		/// <summary>
		/// Post Code.
		/// </summary>
		public string PostCode
		{ 
			get { return postCode; }
			set { postCode = value; }
		}

		/// <summary>
		/// Order total amount.
		/// </summary>
		public decimal OrderAmount
		{ 
			get { return OrderAmount; }
			set { OrderAmount = value; }
		}

		#endregion

		#region Output Properties

		/// <summary>
		/// TransactionID.
		/// </summary>
		public string TransactionID
		{ 
			get { return transactionID; }
		}

		/// <summary>
		/// Response message.
		/// </summary>
		public string ResponseMessage
		{ 
			get { return responseMessage; }
		}

		/// <summary>
		/// Transaction Type.
		/// </summary>
		public string TransactionType
		{ 
			get { return transactionType; }
		}

		/// <summary>
		/// Transaction Failure Error Message.
		/// </summary>
		public string ErrorMessage
		{ 
			get { return errorMessage; }
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Default Constructor. 
		/// If we are in Test mode then set the processingMode Y =Approve all, R = Random results, N = Reject all
		/// </summary>
		public EPDQTransaction(string nameOnCard, string cardNumber, string cv2, string startDate, string expiryDate, int issueNumber, string address1, string address2, string address3, string address4, string postCode, decimal chargeAmount)
		{
			this.filePath = "C:\\windows\\temp";
			this.currencyID = 826;	

			this.cardHolderName = nameOnCard;
			this.creditCardNumber = cardNumber;
			this.creditCardExpiry = expiryDate;
			this.address1 = address1;
			this.address2 = address2;
			this.address3 = address3;
			this.address4 = address4;
			this.postCode = postCode;
			// Switch only
			this.issueNumber = issueNumber;
			this.orderAmount = chargeAmount;
			// Set the currency to UK Pounds
			this.CurrencyID = 826;
		}

		#endregion

		#region Methods

		/// <summary>
		/// Makes a transaction using the parameters passed in the contructor.
		/// </summary>
		public bool MakeTransaction() 
		{
			// Get an instance of the engine
			engine = new clsEPDQEngine();

			// Populate the TransactionParameters class with this class's parameters.
			PopulateInputParameters();

			// Set this engine's parameters to be the TransactionParameters.
			engine.clsParams = parameters;

			// Make the transaction
			bool result = engine.MakeTransaction();

			// Populate Reference Number fields
			PopulateOutputProperties();

			// Debugging - Remove
			int returnVal    = engine.clsParams.intTrans;
			string error     = engine.clsParams.strSetUpError;
			string transType = engine.clsParams.strTransType;
			string orderID   = engine.clsParams.strOrderID;
			string response  = engine.clsParams.strResponse;
			string transID   = engine.clsParams.strTransID;

			// Return the result
			return result;
		}

		/// <summary>
		/// Populates this instance's TransactionParameters class with the parameters from this class.
		/// </summary>
		public void PopulateInputParameters() 
		{
			// TODO: Populate the ePDQ object's parameters.

			// Setup fields
			parameters.bolLive = live;
			parameters.ProcessingMode = processingMode.ToString();
			parameters.strFilePath = filePath;
			parameters.strStoreID = storeID;
			parameters.strUserName = userName;
			parameters.strPassword = password;
			parameters.strCurrencyID = currencyID.ToString();

			// Card fields
			parameters.strCardHolderName = cardHolderName;
			parameters.strCreditCardNumber = creditCardNumber;
			parameters.strCreditCardExpiry = creditCardExpiry;
			parameters.strCV2 = securityCode;

			// Switch only
			parameters.strCID = issueNumber.ToString();
			parameters.strCreditCardStartDate = creditCardStartDate;

			// Address fields
			parameters.strAddress1 = address1;
			parameters.strAddress2 = address2;
			parameters.strAddress3 = address3;
			parameters.strAddress4 = address4;
			parameters.strPostcode = postCode;

			parameters.OrderAmount = orderAmount.ToString();

		}

		/// <summary>
		/// Populates this class's Output Properties with the output values of the TransactionParameters.
		/// </summary>
		public void PopulateOutputProperties() 
		{
			transactionID   = parameters.strTransID;
			transactionType = parameters.strTransType;
			errorMessage    = parameters.strSetUpError;
			responseMessage = parameters.strResponse;
		}


        /// <summary>
        /// Decrypts the Text
        /// </summary>
        /// <param name="InputText"></param>
        /// <param name="Password"></param>
        /// <returns></returns>
        public static string DecryptString(string InputText, string Password)
        {
            // Initialise
            RijndaelManaged RijndaelCipher = new RijndaelManaged();
            int discarded = 0;

            // Get the Input Text and stick it in a Hex Byte Array
            byte[] EncryptedData = HexEncoding.GetBytes(InputText, out discarded);

            // Get the Salt
            byte[] Salt = Encoding.ASCII.GetBytes(Password.Length.ToString());

            // Get the Secret Key
            PasswordDeriveBytes SecretKey = new PasswordDeriveBytes(Password, Salt);

            // Create a decryptor from the existing SecretKey bytes.
            ICryptoTransform Decryptor = RijndaelCipher.CreateDecryptor(SecretKey.GetBytes(32), SecretKey.GetBytes(16));
            MemoryStream memoryStream = new MemoryStream(EncryptedData);

            // Create a CryptoStream. (always use Read mode for decryption).
            CryptoStream cryptoStream = new CryptoStream(memoryStream, Decryptor, CryptoStreamMode.Read);

            try
            {
                // Since at this point we don't know what the size of decrypted data
                // will be, allocate the buffer long enough to hold EncryptedData;
                // DecryptedData is never longer than EncryptedData.
                byte[] PlainText = new byte[EncryptedData.Length];

                // Start decrypting.
                int DecryptedCount = cryptoStream.Read(PlainText, 0, PlainText.Length);

                // Convert decrypted data into a string. 
                string DecryptedData = Encoding.Unicode.GetString(PlainText, 0, DecryptedCount);

                // Return decrypted string.   
                return DecryptedData;
            }
            finally
            {
                // Close the streams
                memoryStream.Close();
                cryptoStream.Close();
            }
        }
		#endregion

	}
}
