﻿using System.IO;
using System.Text;

namespace Kx.Core.PaymentProcessor.Helpers
{
    public class Utf8StringWriter : StringWriter
    {
        public override Encoding Encoding { get { return Encoding.UTF8; } }
    }
}
