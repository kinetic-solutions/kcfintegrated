﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Text;
using System.Web;
using System.Web.UI;
using Kx.Core.PaymentProcessor.Payments;
using Kx.Core.PaymentProcessor.Cache;
using System.Threading;

namespace Kx.Core.PaymentProcessor.Providers
{
    public class UKCBnBProvider : BasePaymentProvider
    {
        public UKCBnBProvider()
        {
        }

        /// <summary>
        /// Please note that keys are case sensetive.
        /// </summary>
        /// <param name="Inputs"></param>
        /// <param name="request"></param>
        public static void ToForm(NameValueCollection Inputs, PaymentRequest request)
        {
            Inputs.Add("g_redirectURL", request.ProviderCallbackUrl);
            Inputs.Add("g_post_url", request.ProviderCallbackUrl);
            Inputs.Add("g_reference", request.BookingReference);
            Inputs.Add("g_description", request.Description);
			string strAmount = request.Amount.ToString();

			// If current thread not in english then convert CurrencyDecimalSeperator to '.', otherwise WPM get wrong amount
			if (Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName != "en")
			{
			    strAmount = strAmount.Replace(Thread.CurrentThread.CurrentCulture.NumberFormat.CurrencyDecimalSeparator[0], '.');
			}

			Inputs.Add("g_amount", strAmount);

			// Addition field for WPMBASIC
			if (request.PaymentProvider == "WPMBASIC")
			{
				Inputs.Add("g_Accref", request.ProviderReference);
				Inputs.Add("g_Kx_ref", request.BookingReference);
				Inputs.Add("g_accountcode", request.ProviderAccountId);
				Inputs.Add("g_cardholder_name", request.FullName);
				Inputs.Add("g_email", request.Email);
				Inputs.Add("g_address", request.FullAddress);
				Inputs.Add("g_town", request.Town);
				Inputs.Add("g_county", request.County);
				Inputs.Add("g_country", request.Country);
				Inputs.Add("g_postcode", request.Postcode);
				Inputs.Add("g_telephone", request.Telephone);
				Inputs.Add("g_studentid", request.ProviderStudentId);
			}
		
			
			if (request.TestMode)
            {
                Inputs.Add("LiveStatus", "1");
            }
            else
            {
                Inputs.Add("LiveStatus", "0");
            }


        }

        /// <summary>
        /// Example postback
        /// g_amount 38.5000
        /// g_payment_ref CPG603
        /// g_reference 12426
        /// WPMReturnURL http://kenthospitality.kent.ac.uk/shortbreaks/bnb/inbound.aspx
        /// LiveStatus 1
        /// </summary>
        /// <param name="cache"></param>
        /// <returns></returns>
        public static PaymentResponse FromResponse(PaymentCache cache)
        {
            PaymentResponse response = new PaymentResponse();
            response.InternalReference = GetObjectValue(HttpContext.Current.Request["g_reference"]);
            response.Authorised = HttpContext.Current.Request["g_payment_ref"] != null;
            response.ProviderReference = GetObjectValue(HttpContext.Current.Request["g_payment_ref"]);

            try
            {
                response.Amount = GetObjectValueDecimal(HttpContext.Current.Request["g_amount"]);
            }
            catch
            { }
            
            response.OriginalRequest = cache.GetPayment(response);

            response.TransactionDate = DateTime.Now;

            return response;
        }
    }
}
