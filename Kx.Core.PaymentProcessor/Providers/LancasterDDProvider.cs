using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;
using System.Net;
using System.Web;
using System.Xml;

using Kx.Core.PaymentProcessor.Payments;
using Kx.Core.PaymentProcessor.Cache;
using System.Security.Cryptography;
using Kx.Core.PaymentProcessor.uk.ac.swan.intranettest;

namespace Kx.Core.PaymentProcessor.Providers
{
    public class LancasterDDProvider : BasePaymentProvider
    {
        public static void ToForm(NameValueCollection Inputs, PaymentRequest request)
        {
            Inputs.Add("Validation1", request.CustomValue1);
            Inputs.Add("Validation2", request.CustomValue2);
            Inputs.Add("Validation3", request.CustomValue3);
            Inputs.Add("Validation4", request.CustomValue4);
            Inputs.Add("Validation5", request.CustomValue5);
            Inputs.Add("Validation6", request.CustomValue6);

            Inputs.Add("student_id", request.ProviderStudentId);
            Inputs.Add("email_address", request.Email);
            Inputs.Add("student_fname", request.FirstName);
            Inputs.Add("student_sname", request.Surname);
            Inputs.Add("kx_reference", request.BookingReference);
            Inputs.Add("redirect_url", request.ProviderCallbackUrl);

        }

        public static PaymentResponse FromResponse(PaymentCache cache)
        {
            PaymentResponse response = new PaymentResponse();
            response.InternalReference = GetObjectValue(HttpContext.Current.Request["kx_reference"]);
            response.Authorised = GetObjectValue(HttpContext.Current.Request["kx_status"]) == "true";
            response.ProviderReference = GetObjectValue(HttpContext.Current.Request["kx_reference"]);

            try
            {
                response.Amount = 0.00M;
            }
            catch
            { }

            response.TransactionDate = DateTime.Now;
            response.OriginalRequest = cache.GetPayment(response);
            
            return response;
        }
    }
}
