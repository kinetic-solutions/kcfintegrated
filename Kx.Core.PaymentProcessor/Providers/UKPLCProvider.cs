using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Text;
using System.Net;
using System.Web;
using Kx.Core.PaymentProcessor.Payments;
using Kx.Core.PaymentProcessor.Helpers.UKPLC;
using Kx.Core.PaymentProcessor.Cache;

namespace Kx.Core.PaymentProcessor.Providers
{
    public class UKPLCProvider : BasePaymentProvider
    {

        public UKPLCProvider()
        {
        }

        public static void ToForm(NameValueCollection Inputs, PaymentRequest request,PaymentCache cache)
        {
            string errorMessage = "Failed to authorise the card";

            try
            {
                EPDQTransaction transaction = new EPDQTransaction(request.CardholderName, request.CardNumber, request.CV2.ToString(),
                                                                    request.ValidFrom, request.ExpiryDate, request.IssueNumber, 
                                                                    request.Address1, request.Town, request.County, request.Country, 
                                                                    request.Postcode, request.Amount);
                

                PaymentResponse response = new PaymentResponse();
                response.InternalReference = request.BookingReference;

                response.Amount = request.Amount;               

                if (request.TestMode)
                {
                    response.Authorised = request.TestTransactionOutcome;
                }
                else
                {
                    response.Authorised = transaction.MakeTransaction();
                }

                response.ProviderReference = transaction.TransactionID;
                string transactionType = transaction.TransactionType;
                errorMessage = transaction.ErrorMessage;
                response.AuthorisationMessage = transaction.ResponseMessage;

                response.OriginalRequest = cache.GetPayment(response);

                if (response.Authorised)
                {
                    //IG
                    // Store the response in the session
                   // Session["PaymentGatewayResponse"] = response;

                    // Redirect to the Callback
                    //Response.Redirect(System.Configuration.ConfigurationSettings.AppSettings["UKPLCEPDQ_CallbackUrl"]);
                }
            }
            catch (System.Threading.ThreadAbortException)
            {
                throw;
            }
            catch
            {
               
            }
        }

        public static PaymentResponse FromResponse(HttpContext context)
        {
            PaymentResponse response = new PaymentResponse();
            return response;
		}



    }
}
