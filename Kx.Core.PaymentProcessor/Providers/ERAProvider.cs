using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Text;
using System.Web;
using System.Web.UI;
using Kx.Core.Exceptions;
using Kx.Core.PaymentProcessor.Payments;
using Kx.Core.PaymentProcessor.Cache;

namespace Kx.Core.PaymentProcessor.Providers
{
    class ERAProvider : BasePaymentProvider
    {
        public ERAProvider()
        {
        }

        public static void ToForm(NameValueCollection Inputs, PaymentRequest request)
        {
            Inputs.Add("Dept_id", request.EraDepartmentId);
            Inputs.Add("Contact_id", request.EraContactId);
            Inputs.Add("Customer_email", request.Email);
            Inputs.Add("Customer_id", request.EraCustomerId);
            Inputs.Add("Spare_three", request.BookingReference);
            Inputs.Add("Item_list", request.EraItemList);
        }

        public static PaymentResponse FromResponse(PaymentCache cache)
        {
 
            string temp = "";

            try
            {
                temp += "txtOrder:"+GetObjectValue(HttpContext.Current.Request["txtOrder"])+"  "; 
            }
            catch { }

            try
            {
                temp += "txtCardtype:" + GetObjectValue(HttpContext.Current.Request["txtCardtype"]) + "  ";
            }
            catch { }

            try
            {
                temp += "txtCardId:" + GetObjectValue(HttpContext.Current.Request["txtCardId"]) + "  ";
            }
            catch { }

            try
            {
                KxLogger.LogActivity("ERAProvider", "ProcessPayment", temp, "");
            }
            catch { }
            
            PaymentResponse response = new PaymentResponse();
            response.InternalReference = GetObjectValue(HttpContext.Current.Request["Spare_three"]);
            response.Authorised = GetObjectValue(HttpContext.Current.Request["txtOrder"]) != ""
                && GetObjectValue(HttpContext.Current.Request["txtCardtype"]) != "";
                //&& GetObjectValue(HttpContext.Current.Request["txtCardId"]) != "";
            response.ProviderReference = GetObjectValue(HttpContext.Current.Request["txtOrder"]);
            
            try
            {
                response.Amount = GetObjectValueDecimal(HttpContext.Current.Request["txtAmount"]);
            }
            catch
            { }

            try
            {
                response.OriginalRequest = cache.GetPayment(response);
            }
            catch
            {
            }

            response.TransactionDate = DateTime.Now;

            return response;
        }
    }
}
