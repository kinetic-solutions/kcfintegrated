using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;
using System.Net;
using System.Web;
using Kx.Core.Exceptions;
using Kx.Core.PaymentProcessor.Payments;
using Kx.Core.PaymentProcessor.Cache;

namespace Kx.Core.PaymentProcessor.Providers
{
    public class WarwickTTSProvider : BasePaymentProvider
    {
        public WarwickTTSProvider()
        {
        }

        public static void ToForm(NameValueCollection Inputs, PaymentRequest request)
        {
			//KxLogger.LogActivity(request, "Warwick ToForm", "001", "");
			/// ttsTransId=094d40220489d92f0105581c867f03b2&
            /// transId=126448431&
            /// amount=150&
            /// cardType=Visa&
            /// address=THE+BIELD%0D%0ASUTTON+PLACE%0D%0AABINGER+HAMMER%0D%0ASURREY&
            /// postcode=RH5+6RL&
            /// tel=&fax=&
            /// email=johnny%40kinetic-solutions.co.uk&
            /// name=CATHARINE+RISEBOROUGH&
            /// cancelled=false&
            /// testMode=true&
            /// timeStamp=1122464781301&
            /// searchElement1=null&
            /// searchElement2=null&
            /// studentId=0000094&
            /// emailOnly=false&
            /// allocationid=463&
            /// postcode=RH5+6RL&
            /// currency=GBP&
            /// address=THE+BIELD%0D%0ASUTTON+PLACE%0D%0AABINGER+HAMMER%0D%0ASURREY&
            /// email=steve%40kinetic-solutions.co.uk&
            /// cartId=175923a3-2a7a-455e-9e4d-b85d4b56b3f1&
            /// paymentreason=2&
            /// name=CATHARINE+RISEBOROUGH&
            /// instId=64781
            /// </summary>
            /// 
            /*
             * 		p.SendData.Add("tradingId","094d4022045204ef010457320f6e0022");
			p.SendData.Add("instId","64781");
			p.SendData.Add("cartId","12345678");
			p.SendData.Add("amount","10");
			p.SendData.Add("emailOnly","false");
			p.SendData.Add("name","johnny spittle");
			p.SendData.Add("address","29 broomfield\r\nstacey bushes");
			p.SendData.Add("postcode","mk126ha");
			p.SendData.Add("email","johnny@kinetic-solutions.co.uk");
			p.SendData.Add("currency","GBP");
             * */

            Inputs.Add("tradingId", request.ProviderPassword);
            Inputs.Add("instId", request.ProviderAccountId);
            Inputs.Add("MC_callback", request.ProviderCallbackUrl);
            Inputs.Add("currency", request.Currency);
            Inputs.Add("cartId", request.BookingReference);
            //Inputs.Add("desc", request.Description);
            Inputs.Add("amount", request.Amount.ToString());
            Inputs.Add("name", request.FullName);
            Inputs.Add("address", request.FullAddress);
            Inputs.Add("postcode", request.Postcode);
            //Inputs.Add("tel", request.Telephone);
            Inputs.Add("email", request.Email);
            //Inputs.Add("fax", request.Fax);
            Inputs.Add("testMode", request.TestMode ? "100" : "0");

            Inputs.Add("MC_STUDENT_ID",request.ProviderStudentId);
            Inputs.Add("MC_STUDENT_NAME",request.FullName);
        }

        public static PaymentResponse FromResponse(PaymentCache cache)
        {
            /*
             *  orderKey=UNIWARWICK^WARWICKACCOMWEB^094d736823271c9b01249c2f54365ff2
                paymentStatus=AUTHORISED
                paymentAmount=25000
                paymentCurrency=GBP
                mac=0656b58631433732dc337c22c923c99f
             * 
             * */

			//KxLogger.LogActivity(cache, "Warwick FromResponse", "001", "");

			PaymentResponse response = new PaymentResponse();

			//KxLogger.LogActivity(cache, "Warwick FromResponse", "002", "");
			String reference = "";

			if (HttpContext.Current.Request["orderKey"] != null)
			{
				reference = GetObjectValue(HttpContext.Current.Request["orderKey"]);
			}
			if (HttpContext.Current.Request["cartId"] != null)
			{
				reference = GetObjectValue(HttpContext.Current.Request["cartId"]);
			}

			//KxLogger.LogActivity(cache, "Warwick FromResponse", "003", reference);
			try
			{
				if (reference.Contains("^"))
				{
					try
					{
						string[] values = reference.Split(new char[] { '^' });

						if (values.Length == 3)
						{
							reference = values[2];
						}
					}
					catch { }
				}
				//KxLogger.LogActivity(cache, "Payment ProcessResponse", "Reference", reference);
				response.InternalReference = reference;
				if (HttpContext.Current.Request["mac"] != null)
				{
					response.ProviderReference = GetObjectValue(HttpContext.Current.Request["mac"]);
				}
				if (HttpContext.Current.Request["transId"] != null)
				{
					response.ProviderReference = GetObjectValue(HttpContext.Current.Request["transId"]);
				}
				//KxLogger.LogActivity(cache, "Payment ProcessResponse", "Provider Reference", response.ProviderReference);
				if (HttpContext.Current.Request["paymentStatus"] != null)
				{
					response.Authorised = GetObjectValue(HttpContext.Current.Request["paymentStatus"]).ToUpper() == "AUTHORISED" ? true : false;
				}
				if (HttpContext.Current.Request["state"] != null)
				{
					response.Authorised = GetObjectValue(HttpContext.Current.Request["state"]).ToUpper() == "SUCCESSFUL" ? true : false;
				}
				//KxLogger.LogActivity(cache, "Payment ProcessResponse", "Provider Authorised", response.Authorised.ToString());
				if (HttpContext.Current.Request["paymentCurrency"] != null)
				{
					response.Currency = GetObjectValue(HttpContext.Current.Request["paymentCurrency"]);
				}
				else
				{
					response.Currency = "GBP";
				}
				//KxLogger.LogActivity(cache, "Payment ProcessResponse", "Provider Currency", response.Currency);
				if (HttpContext.Current.Request["paymentAmount"] != null)
				{
					response.Amount = GetObjectValueDecimal(HttpContext.Current.Request["paymentAmount"]) / 100;
				}
				if (HttpContext.Current.Request["amount"] != null)
				{
					response.Amount = GetObjectValueDecimal(HttpContext.Current.Request["amount"]);
				}
				//KxLogger.LogActivity(cache, "Payment ProcessResponse", "Provider Amount", response.Amount.ToString());
				response.TransactionDate = DateTime.Now;

				try
				{
					response.OriginalRequest = cache.GetPayment(response);
				}
				catch (Exception ex)
				{
					KxLogger.LogActivity(cache, "Payment OriginalRequest", ex.Message, "");
				}
				if (response.OriginalRequest != null)
				{
					response.OriginalRequest.PaymentProvider = response.InternalReference;
				}
			}
			catch (Exception ex)
			{
				KxLogger.LogActivity(cache, "Payment ProcessResponse", ex.Message, "");
			}
			//KxLogger.LogActivity(cache, "Warwick FromResponse", "004", "");
			return response;
		}
    }
}
