using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Net;
using System.Security.Cryptography;
using System.Text.RegularExpressions;
using System.Web;
using Kx.Core.Exceptions;
using Kx.Core.PaymentProcessor.Payments;
using Kx.Core.PaymentProcessor.Cache;

namespace Kx.Core.PaymentProcessor.Providers
{
    public class RealexProvider : BasePaymentProvider
    {
        public RealexProvider()
        {
        }

        public static void ToForm(NameValueCollection Inputs, PaymentRequest request)
        {
            /*
            --MERCHANT_ID 	AN50V R Supplied by Realex 
            ACCOUNT 	AN30V O The sub-account to use for this transaction.
            --ORDER_ID 	AN50V R A unique alphanumeric id that�s used to identify thetransaction.
            --AMOUNT 		N10V  R Total amount to authorise 
            --CURRENCY 	A3    R A three-letter code as per currency table in Appendix A.
            TIMESTAMP 	N14   R Date and time of the transaction. Entered in the following format: yyyymmddhhmmss
            MD5HASH 	AN32  R A digital signature generated using the MD5 algorithm. 
            SHA1HASH 	AN40  R A digital signature generated using the SHA-1 algorithm.
            AUTO_SETTLE_FLAG N1 R Used
            COMMENT1 	AN255V O A freeform comment to describe the transaction. Up to 255characters
            COMMENT2 	AN255V O A freeform comment to describe the transaction. Up to 255characters
            RETURN_TSS 	N1 O Used to signify wh0/1
            SHIPPING_CODE 	30V O The postcode or ZIP of the shipping address.
            SHIPPING_CO 	30V O The country of the shipping address. 
            BILLING_CODE 	30V O The postcode or ZIP of the billing address.
            BILLING_CO 	30V O The country of the billing address. 
            CUST_NUM 	ANS50V O The customer number of the customer.
            VAR_REF 	ANS50V O A variable reference also associated with this customer.
            PROD_ID 	ANS50V O A product id associated with this product.
            ANYTHING ELSE 	ANS255V O Anything else you send to Realex Payments will be returned inthe response (whatever other information you collected fromthe customer such as product or address/telephone numbers
                */
            request.TransactionDate = DateTime.Now;

            Inputs.Add("MERCHANT_ID", request.ProviderAccountId);
            Inputs.Add("MC_callback", request.ProviderCallbackUrl);
            Inputs.Add("MERCHANT_RESPONSE_URL", request.ProviderCallbackUrl);
            Inputs.Add("ORDER_ID", request.BookingReference);
            Inputs.Add("PROD_ID", request.PaymentType);
            Inputs.Add("COMMENT1", request.Description);
            Inputs.Add("CUST_NUM", request.StudentId);

            string transactionDate = ConvertDateToString(request.TransactionDate);
            string amount = Convert.ToInt32((request.Amount * 100)).ToString();
            string currency = "GBP";

            if (!String.IsNullOrEmpty(request.Currency))
            {
                currency = request.Currency;
            }

            Inputs.Add("CURRENCY", currency);
            Inputs.Add("TIMESTAMP", transactionDate);
            Inputs.Add("AMOUNT", amount);
            Inputs.Add("AMOUNTVALUE", amount);
            Inputs.Add("RETURN_TSS", "0");// Return Realscore data - not required...
            Inputs.Add("SHA1HASH", Sha1Hash(transactionDate + "." + request.ProviderAccountId + "." + request.BookingReference + "." + amount + "." + currency, request));
            Inputs.Add("AUTO_SETTLE_FLAG", "1");// 0 indicates that settlement should take place later i.e. when product is shipped...

            /* Card storage options for UoL Halls */
            if (!string.IsNullOrWhiteSpace(request.CustomValue1))
            {
                Inputs.Add("CARD_STORAGE_ENABLE", "1");
                Inputs.Add("OFFER_SAVE_CARD", "0");
                Inputs.Add("PAYER_REF", request.CustomValue1);
                if (!string.IsNullOrWhiteSpace(request.CustomValue2))
                {
                    Inputs.Add("PMT_REF", RealExSafeString(request.CustomValue2));
                }

                Inputs.Remove("SHA1HASH");
                Inputs.Add("SHA1HASH", Sha1Hash(transactionDate + "." + request.ProviderAccountId + "." + request.BookingReference + "." + amount + "." + currency + "." + request.CustomValue1 + "." + RealExSafeString(request.CustomValue2), request));
                Inputs.Add("PAYER_EXIST", "0");
            }
            /* END Card Storage */

            /* Begin 3D Secure 2 Mandatory Fields */
            Inputs.Add("HPP_CUSTOMER_EMAIL", request.Email);
            Inputs.Add("HPP_CUSTOMER_PHONENUMBER_MOBILE", $"{request.CountryCode}|{request.Telephone}");
            Inputs.Add("HPP_BILLING_STREET1", request.Address1);
            Inputs.Add("HPP_BILLING_STREET2", request.Address2);
            Inputs.Add("HPP_BILLING_STREET3", request.County);
            Inputs.Add("HPP_BILLING_CITY", request.Town);
            Inputs.Add("HPP_BILLING_POSTALCODE", request.Postcode);
            Inputs.Add("HPP_BILLING_COUNTRY", request.Country);
            /* End 3D Secure 2 Mandatory Fields */
        }

        private static string RealExSafeString(string input)
        {
            StringBuilder result = new StringBuilder();
            foreach (char c in input.ToArray())
            {
                string tmp = c.ToString();
                if (Regex.Match(tmp, "[A-Za-z0-9]").Success)
                {
                    result.Append(tmp);
                }
            }

            return result.ToString();
        }

        public static PaymentResponse FromResponse(PaymentCache cache)
        {
            /*
             MERCHANT_ID This is the merchant id that Realex Payments assign to you.
            ACCOUNT The sub-account used in the transaction.
            ORDER_ID The unique order id that you sent to us.
            AUTHCODE Will contain a valid authcode if the transaction was successful. Will beempty otherwise.
            RESULT The outcome of the transaction. Will contain �00� if the transactionwas a success or another value (depending on the error) if not. See
            MESSAGE Will contain a text message that describes the result code above.
            CVNRESULT The result of the Card Verification check (if enabled):
            M: CVV Matched.
            N: CVV Not Matched.
            I: CVV Not checked due to circumstances.
            U: CVV Not checked � issuer not certified.
            P: CVV Not Processed.
            PASREF A unique reference that Realex Payments assign to your transaction.
            BATCHID This is the Realex Payments batch that this transaction will be in.
            MD5HASH An MD5 digital signature created using the above fields and your shared secret. Needs to be sent in lowercase.
            SHA1HASH A SHA-1 digital signature created using the above fields and your shared secret. Needs to be sent in lowercase.
            TSS The Transaction Suitability Score for the transaction.
            TSS_idnum The realscore is comprised of various distinct tests. Using the
            ANYTHING ELSE Anything else you sent to us in the request will be returned to you.
             * */
            PaymentResponse response = new PaymentResponse();

            response.InternalReference = GetObjectValue(HttpContext.Current.Request["ORDER_ID"]);

            response.ProviderReference = GetObjectValue(HttpContext.Current.Request["AUTHCODE"]);
            response.AuthorisationMessage = GetObjectValue(HttpContext.Current.Request["MESSAGE"]);

            if (HttpContext.Current.Request["DCCMERCHANTAMOUNT "] != null)
            {
                try
                {
                    response.Amount = Decimal.Parse(GetObjectValue(HttpContext.Current.Request["DCCMERCHANTAMOUNT"])) / 100;
                }
                catch
                {
                    response.Amount = Decimal.Parse(GetObjectValue(HttpContext.Current.Request["DCCMERCHANTAMOUNT"]));
                }
            }
            else
            {
                try
                {
                    response.Amount = Decimal.Parse(GetObjectValue(HttpContext.Current.Request["AMOUNTVALUE"])) / 100;
                }
                catch
                {
                    response.Amount = Decimal.Parse(GetObjectValue(HttpContext.Current.Request["AMOUNTVALUE"]));
                }
            }

            response.InternalReference = GetObjectValue(HttpContext.Current.Request["ORDER_ID"]);
            response.Authorised = GetObjectValue(HttpContext.Current.Request["RESULT"]) == "00";
            response.TransactionDate = DateTime.Now;

            string paymentSetup = GetObjectValue(HttpContext.Current.Request["PMT_SETUP"]);

            if (!string.IsNullOrWhiteSpace(paymentSetup) && paymentSetup == "00")
            {
                string payerRef = GetObjectValue(HttpContext.Current.Request["SAVED_PAYER_REF"]);
                string paymentRef = GetObjectValue(HttpContext.Current.Request["SAVED_PMT_REF"]);
                response.ProviderReference2 = string.Format("{0}-{1}", payerRef, paymentRef);
            }

            try
            {
                var id = new Guid(response.InternalReference);
                var cacheRow = new KxWebPaymentCache(null, id);
                cacheRow.Name = HttpContext.Current.Request["ACCOUNT"];
                cacheRow.Changed = true;
                cacheRow.Save();
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity("RealexProvider", "FromResponse", ex.Message, "Update cache row Name with response[ACCOUNT]");
            }

            try
            {
                response.OriginalRequest = cache.GetPaymentSafe(response);
                response.Currency = response.OriginalRequest.Currency;
                //response.Amount = response.OriginalRequest.Amount;
            }
            catch { }


            if (response.OriginalRequest != null)
            {
                response.OriginalRequest.PaymentProvider = response.InternalReference;
            }

            return response;
        }

        public static string ConvertDateToString(DateTime source)
        {
            return source.ToString("yyyyMMddhhmmss");
        }

        public static string Sha1Hash(string source, PaymentRequest request)
        {
            SHA1 sha = new SHA1Managed();

            string hashStage1 = hexEncode(sha.ComputeHash(Encoding.UTF8.GetBytes(source))) + "." + request.ProviderPassword;

            string hashStage2 = hexEncode(sha.ComputeHash(Encoding.UTF8.GetBytes(hashStage1)));

            return hashStage2;
        }

        public static String hexEncode(byte[] data)
        {
            String result = "";
            foreach (byte b in data)
            {
                result += b.ToString("X2");
            }
            result = result.ToLower();

            return (result);
        }
    }
}
