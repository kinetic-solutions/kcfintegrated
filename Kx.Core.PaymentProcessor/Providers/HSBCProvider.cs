using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Reflection;
using System.Text;
using System.Net;
using System.Web;
using Kx.Core.PaymentProcessor.Payments;
using Kx.Core.PaymentProcessor.Helpers.HSBC;
using Kx.Core.PaymentProcessor.Cache;

namespace Kx.Core.PaymentProcessor.Providers
{
    public class HSBCProvider : BasePaymentProvider
    {

        public static void ToForm(NameValueCollection Inputs, PaymentRequest request)
        {
            HSBCTransaction provider = new HSBCTransaction();

            request.Address1 = request.Address1.Replace(System.Environment.NewLine, " ");

            provider.RequestDetails = request;

            string Values = "";

            foreach (PropertyInfo pi in provider.GetType().GetProperties())
            {
                if (pi.CanRead)
                {
                    string PName = pi.Name;
                    object PValue = pi.GetValue(provider, null);
                    if ((PValue != null) && (PValue.ToString() != ""))
                    {
                        Inputs.Add(PName, PValue.ToString());
                        Values += "|" + PValue.ToString();
                    }
                }
            }
            Inputs.Add("OrderHash", provider.GetOrderHash(Values));
        }

        public static PaymentResponse FromResponse(PaymentCache cache)
        {
            PaymentResponse response = new PaymentResponse();
            response.InternalReference = GetObjectValue(HttpContext.Current.Request["OrderId"]);

            response.AuthorisationMessage = GetObjectValue(HttpContext.Current.Request["CpiResultsCode"]);
            response.Authorised = response.AuthorisationMessage == "0" ? true : false; 
            response.TransactionDate = DateTime.Now;

            try
            {
                int payAmount = GetObjectValueInt(HttpContext.Current.Request["PurchaseAmount"]);
                response.Amount = Convert.ToDecimal(payAmount / 100);
            }
            catch
            { }

            response.OriginalRequest = cache.GetPayment(response);

            return response;
        }
    }
}
