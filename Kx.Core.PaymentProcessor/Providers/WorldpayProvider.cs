using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;
using System.Net;
using System.Web;
using Kx.Core.PaymentProcessor.Payments;
using Kx.Core.PaymentProcessor.Cache;

namespace Kx.Core.PaymentProcessor.Providers
{
    public class WorldpayProvider : BasePaymentProvider
    {
        public WorldpayProvider()
        {
        }

        public static void ToForm(NameValueCollection Inputs, PaymentRequest request)
        {
            Inputs.Add("g_accountcode", request.ProviderAccountId);
            Inputs.Add("instId", request.ProviderAccountId);
            Inputs.Add("MC_callback", request.ProviderCallbackUrl);
            Inputs.Add("currency", request.Currency);
            Inputs.Add("cartId", request.BookingReference);
            Inputs.Add("desc", request.Description);
            Inputs.Add("amount", request.Amount.ToString());
            Inputs.Add("name", request.FullName);
            Inputs.Add("address", request.FullAddress);
            Inputs.Add("town", request.Town);
            Inputs.Add("county", request.County);
            Inputs.Add("country", request.Country);
            Inputs.Add("postcode", request.Postcode);
            Inputs.Add("tel", request.Telephone);
            Inputs.Add("email", request.Email);
            Inputs.Add("fax", request.Fax);
            Inputs.Add("testMode", request.TestMode ? "100" : "0");

            Inputs.Add("MC_STUDENT_ID",request.ProviderStudentId);
            Inputs.Add("MC_STUDENT_NAME",request.FullName);
        }

        public static PaymentResponse FromResponse(PaymentCache cache)
        {
            PaymentResponse response = new PaymentResponse();
            response.InternalReference = GetObjectValue(HttpContext.Current.Request["cartId"]);
            
            response.ProviderReference = GetObjectValue(HttpContext.Current.Request["transId"]);
            response.AuthorisationMessage = GetObjectValue(HttpContext.Current.Request["rawAuthMessage"]);
            response.Currency = GetObjectValue(HttpContext.Current.Request["authCurrency"]);
            response.Amount = GetObjectValueDecimal(HttpContext.Current.Request["authAmount"]);
            response.InternalReference = GetObjectValue(HttpContext.Current.Request["cartId"]);
            response.Authorised = GetObjectValue(HttpContext.Current.Request["transStatus"]).ToUpper() == "Y" ? true : false;

            response.OriginalRequest = cache.GetPayment(response);

            try
            {
                double transtime = GetObjectValueDouble(HttpContext.Current.Request["transTime"]);
                response.TransactionDate = new DateTime(1970, 1, 1, 0, 0, 0, 0).AddMilliseconds(transtime);
            }
            catch
            {
                response.TransactionDate = DateTime.Now;
            }
            return response;
		}
    }
}
