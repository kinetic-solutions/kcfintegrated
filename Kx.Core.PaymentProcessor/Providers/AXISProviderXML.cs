﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Text;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Xml;
using Kx.Core.Data;
using Kx.Core.Exceptions;
using Kx.Core.PaymentProcessor;
using Kx.Core.PaymentProcessor.Payments;
using Kx.Core.PaymentProcessor.Cache;
using Kx.Core.Utility;

namespace Kx.Core.PaymentProcessor.Providers
{
    public class AXISProviderXML : BasePaymentProvider
    {
        static string sHashKey = "";//"9jP4RKtHwqYWMizy9CPOBW0p/wxLMa6pyQRabi5CNXk=";
        static string sSchemaId = "APPXML_V2";

        public AXISProviderXML()
        {
        }

        private static string GetTransactionID(PaymentRequest request)
        {
            return request.WebPaymentCacheId.ToString("000000000000");
        }

        private static string GetXML(PaymentRequest request)
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine("<?xml version=\"1.0\"?>");
            sb.AppendLine("<portalApacsAPIReq xmlns=\"http://www.capita-software-services.com/portal-api\">");
            sb.AppendLine("  <header>");
            sb.AppendLine("    <sourceDetails>");
            sb.AppendLine("      <systemCode>APP</systemCode>");
            sb.AppendLine("      <uniqueTranID>{TransactionID}</uniqueTranID>");
            sb.AppendLine("      <userCode>KXS</userCode>");
            sb.AppendLine("    </sourceDetails>");
            sb.AppendLine("    <portalOnly>");
            sb.AppendLine("      <backURL>{BackURL}</backURL>");
            sb.AppendLine("      <returnURL>{ReturnURL}</returnURL>");
            sb.AppendLine("    </portalOnly>");
            sb.AppendLine("  </header>");
            sb.AppendLine("  <invoice>");
            sb.AppendLine("    <items>");
            sb.AppendLine("      <item>");
            sb.AppendLine("        <fundCode>{FundCode}</fundCode>");
            sb.AppendLine("        <reference>{Reference}</reference>");
            sb.AppendLine("        <amountInMinorUnits>{AmountInPence}</amountInMinorUnits>");
            sb.AppendLine("        <fundDescription>{Description}</fundDescription>");
            sb.AppendLine("        <additionalReference>{AdditionalReference}</additionalReference>");
            sb.AppendLine("        <accountDetails>");
            sb.AppendLine("          <accountName>");
            sb.AppendLine("            <surname>{Surname}</surname>");
            sb.AppendLine("            <forename>{Forename}</forename>");
            sb.AppendLine("          </accountName>");
            sb.AppendLine("        </accountDetails>");
            sb.AppendLine("        <additionalInfo>");
            sb.AppendLine("          <userString5>{StudentID}</userString5>");
            sb.AppendLine("        </additionalInfo>");
            sb.AppendLine("      </item>");
            sb.AppendLine("    </items>");
            sb.AppendLine("    <customerReceiptEmails>");
            sb.AppendLine("      <customerEmail>{CustomerEmail}</customerEmail>");
            sb.AppendLine("    </customerReceiptEmails>");
            sb.AppendLine("  </invoice>");
            sb.AppendLine("</portalApacsAPIReq>");

            string result = sb.ToString();
            int amountInPence = (int)(request.Amount * 100);
            result = result.Replace("{AmountInPence}", amountInPence.ToString());
            result = result.Replace("{TransactionID}", GetTransactionID(request));
            result = result.Replace("{BackURL}", request.CancellationPageUrl);
            result = result.Replace("{ReturnURL}", request.CancellationPageUrl);
            result = result.Replace("{CustomerEmail}", request.Email);
            result = result.Replace("{Description}", request.Description);
            result = result.Replace("{Reference}", request.BookingReference);
            result = result.Replace("{Surname}", request.Surname);
            result = result.Replace("{Forename}", request.FirstName);
            result = result.Replace("{FundCode}", request.PaymentMethod);
            result = result.Replace("{AdditionalReference}", request.PaymentType);
            result = result.Replace("{StudentID}", request.StudentId);

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(result);

            return result;
        }

        private static string GetHash(string sTextToHash, string sHashKey)
        {
            byte[] bKey = Convert.FromBase64String(sHashKey);
            System.Security.Cryptography.HMACSHA256 hasher = new System.Security.Cryptography.HMACSHA256(bKey);
            byte[] bBefore = System.Text.Encoding.UTF8.GetBytes(sTextToHash);
            byte[] bAfter = hasher.ComputeHash(bBefore);
            return Convert.ToBase64String(bAfter);
        }

        public static void ToForm(NameValueCollection Inputs, PaymentRequest request)
        {
            sHashKey = request.SH1Key;
            string sXML = GetXML(request);
            string sHash = GetHash(sXML, sHashKey);
            Inputs.Add("portal_id", request.ProviderAccountId);
            Inputs.Add("portalReq", HttpContext.Current.Server.HtmlEncode(sXML));
            Inputs.Add("hash", sHash);
            Inputs.Add("schemaId", sSchemaId);
        }

        private static string GetNodeValue(XmlDocument doc, string sTagName)
        {
            string result;
            XmlNodeList nodes = doc.GetElementsByTagName(sTagName);

            if (nodes.Count > 0)
                result = nodes[0].InnerXml.ToString();
            else
                result = "";

            return result;
        }

        public static PaymentResponse FromResponse(PaymentCache cache)
        {
            string formstring = "";
            try
            {
                formstring = HttpContext.Current.Request.Form.ToString();
            }
            catch { }

            string sXML = (HttpContext.Current.Request["status"] ?? "").ToString();
            string sHash = (HttpContext.Current.Request["hash"] ?? "").ToString();

            sXML = HttpContext.Current.Server.UrlDecode(sXML);

            try
            {
                KxData data = new KxData();
                data.ExecuteAuditLog("Processing Xml","", sXML);
                data.CloseConnection();
            }
            catch { }

            string sTestHash = GetHash(sXML, sHashKey);

            PaymentResponse response = new PaymentResponse();

            if ((sTestHash == sHash))
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(sXML);
                string sSessionID = GetNodeValue(doc, "sessionID");
                string sTranID = GetNodeValue(doc, "uniqueTranID");
                string sStatusCode = GetNodeValue(doc, "statusCode");
                string sStatusCodeDescription = GetNodeValue(doc, "statusCodeDescription");
                string sMessage = GetNodeValue(doc, "message");
                string sAmount = GetNodeValue(doc, "amountInMinorUnits");
                string sReference = GetNodeValue(doc, "reference");
                response.ProviderReference = sTranID;
                response.InternalReference = sReference;
                response.AuthorisationMessage = String.Format("{0}\n{1}", sStatusCodeDescription, sMessage);
                response.Authorised = (sStatusCode == "0");
                response.Amount = Convert.ToDecimal(sAmount) / 100;
                response.TransactionDate = DateTime.Now;
                response.OriginalRequest = cache.GetPayment(response);
            }

            return response;
        }
    }
}
