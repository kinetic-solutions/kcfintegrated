using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;
using System.Net;
using System.Web;
using Kx.Core.PaymentProcessor.Payments;
using Kx.Core.PaymentProcessor.Cache;

namespace Kx.Core.PaymentProcessor.Providers
{
    class DummyProvider : BasePaymentProvider
    {

        public DummyProvider()
        {
        }

        public static void ToForm(NameValueCollection Inputs, PaymentRequest request)
        {
            PaymentResponse response = new PaymentResponse();

            request.ProviderUrl = request.ProviderCallbackUrl;

            if (request.TestTransactionOutcome)
            {
                Inputs.Add("transaction_status", "1");
            }
            else
            {
                Inputs.Add("transaction_status", "0");
            }

            Inputs.Add("authorisation_reference", Guid.NewGuid().ToString());
            Inputs.Add("currency",request.Currency);
            Inputs.Add("amount",request.Amount.ToString());
            Inputs.Add("g_reference",request.BookingReference);
            Inputs.Add("dummy", "True");

            HttpContext.Current.Response.Redirect(String.Format("{0}?dummy={1}", request.ProviderCallbackUrl, request.BookingReference));

        }

        public static PaymentResponse FromResponse(PaymentCache cache)
        {
            PaymentResponse response = new PaymentResponse();
            response.InternalReference = GetObjectValue(HttpContext.Current.Request["dummy"]);
                    
            response.ProviderReference = Guid.NewGuid().ToString("N");
            response.TransactionDate = DateTime.Now;

            response.Authorised = true;
            response.Currency = "GBP";
            response.Amount = 200.00M;

            response.OriginalRequest = cache.GetPayment(response);
            response.Authorised = response.OriginalRequest.TestTransactionOutcome;
            response.Currency = response.OriginalRequest.Currency;
            response.Amount = response.OriginalRequest.Amount;
            
            return response;
		}
    }
}
