using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;
using System.Net;
using System.Web;
using Kx.Core.PaymentProcessor.Payments;
using Kx.Core.PaymentProcessor.Cache;

namespace Kx.Core.PaymentProcessor.Providers
{
    public class MentecProvider : BasePaymentProvider
    {
        public MentecProvider()
        {
        }

        public static void ToForm(NameValueCollection Inputs, PaymentRequest request)
        {
            StringBuilder sb = new StringBuilder();

            sb.Append("?PmtTyp=" + request.NominalCode);

            sb.Append("&Val1=" + request.StudentId);
            sb.Append("&Val2=" + request.SiteCode);

            sb.Append("&Cmt=" + HttpUtility.UrlEncode(request.StudentId, Encoding.UTF8));

            sb.Append("&Am=" + HttpUtility.UrlEncode(request.Amount.ToString(), Encoding.UTF8));
            
            
            sb.Append("&MUID=" + request.BookingReference.ToString());

            string returnurl = HttpUtility.UrlEncode(request.ProviderCallbackUrl,Encoding.UTF8);

            sb.Append("&Rurl=" + returnurl);
            sb.Append("&Burl=" + returnurl);
            sb.Append("&Email=" + HttpUtility.UrlEncode(request.Email, Encoding.UTF8));

            string url = sb.ToString();

            HttpContext.Current.Response.Redirect(request.ProviderUrl + url);

        }

        public static PaymentResponse FromResponse(PaymentCache cache)
        {
            /*
            http://91.84.17.201:1093/workflowweb/glasgow/payment.aspx?
            MUID=c7e36f59-4ddb-4a8c-ac68-2452813291c6?
            Rc=A&
            RecNo=11179&
            AuthCode=999999&
            CardTyp=Visa%20Credit%20Card&
            CardNo=xxxxxxxxxxxx1111&
            Pmt=250&
            Pmt1=4.38
             * */
            PaymentResponse response = new PaymentResponse();

            string temp = GetObjectValue(HttpContext.Current.Request.QueryString["MUID"]);

            Guid gud = new Guid(temp);

            response.InternalReference = gud.ToString();
            
            response.ProviderReference = GetObjectValue(HttpContext.Current.Request.QueryString["RecNo"])+"-"+GetObjectValue(HttpContext.Current.Request.QueryString["AuthCode"]);
            response.Amount = GetObjectValueDecimal(HttpContext.Current.Request.QueryString["Pmt"]);

            
            response.Authorised = GetObjectValue(HttpContext.Current.Request.QueryString["Rc"]).ToUpper() == "A" ? true : false;

            response.OriginalRequest = cache.GetPayment(response);

            response.TransactionDate = DateTime.Now;
            
            return response;
		}
    }
}
