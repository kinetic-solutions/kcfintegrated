using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Text;
using System.Web;
using System.Web.UI;
using Kx.Core.PaymentProcessor.Payments;
using Kx.Core.PaymentProcessor.Cache;

namespace Kx.Core.PaymentProcessor.Providers
{
    class WPMExtendedProvider : BasePaymentProvider
    {
        public WPMExtendedProvider()
        {
        }

        public static void ToForm(NameValueCollection Inputs, PaymentRequest request)
        {
            Inputs.Add("Student_ID", request.ProviderStudentId);
            Inputs.Add("Student_fName", request.FirstName);
            Inputs.Add("Student_sName", request.Surname);
            Inputs.Add("SubLedger", "S07");
            Inputs.Add("No_Instals", request.InstallmentCount.ToString());
            Inputs.Add("EMail_Address", request.Email);
            Inputs.Add("Address_1", request.Address1);
            Inputs.Add("Address_2", request.Address2);
            Inputs.Add("Address_3", "");
            Inputs.Add("Town", request.Town);
            Inputs.Add("County", request.County);
            Inputs.Add("Postcode", request.Postcode);
            Inputs.Add("Country", request.Country);
            Inputs.Add("Tenancy_Start_Date", request.StartDate.ToString("d"));// (dd mmm yyyy)
            Inputs.Add("Tenancy_End_Date", request.EndDate.ToString("d"));// (dd mmm yyyy)
            Inputs.Add("Rent_Amount", request.Amount.ToString("F"));
            Inputs.Add("First_Payment_Date", request.FirstDate.ToString("d"));// (dd mmm yyyy)
            Inputs.Add("rcp_Setup", request.RecurringType ? "2" : "");// if the value is 2 they will be directed to the RCP setup screens
            Inputs.Add("dd_Setup", request.DirectDebitType ? "1" : "");//if the value of this field is 1 the student will be directed to the DD setup, 
            Inputs.Add("account_code", request.ProviderAccountId);
            Inputs.Add("redirect_url", request.ProviderCallbackUrl);
            Inputs.Add("post_url", request.ProviderCallbackUrl);
            Inputs.Add("KX_CUSTOM_customfield1", request.BookingReference);
            Inputs.Add("KX_CUSTOM_customfield2", "");
        }

        public static PaymentResponse FromResponse(PaymentCache cache)
        {
            PaymentResponse response = new PaymentResponse();
            try
            {
                response.InternalReference = GetObjectValue(HttpContext.Current.Request["KX_CUSTOM_customfield1"]);
            }
            catch { }
            try
            {
                response.Authorised = GetObjectValueInt(HttpContext.Current.Request["transaction_status"]) == 1;
            }
            catch { response.Authorised = false; }

            try
            {
                response.ProviderReference = GetObjectValue(HttpContext.Current.Request["ReferenceNumber"]);
            }
            catch { response.ProviderReference = "Unknown"; }

            
            response.OriginalRequest = cache.GetPayment(response);
            response.TransactionDate = DateTime.Now;
            return response;
        }
    }
}
