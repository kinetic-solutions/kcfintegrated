using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Text;
using System.Web;
using System.Web.UI;
using Kx.Core.PaymentProcessor.Payments;
using Kx.Core.PaymentProcessor.Cache;
using Kx.Core.Configuration;


namespace Kx.Core.PaymentProcessor.Providers
{
    public class WPM3WayProvider : BasePaymentProvider
    {
        public WPM3WayProvider()
        {
        }

        public static void ToForm(NameValueCollection Inputs, PaymentRequest request)
        {
            Inputs.Add("Student_ID", request.StudentId);
            Inputs.Add("Student_fName", request.FirstName);
            Inputs.Add("Student_sName", request.Surname);
            Inputs.Add("EMail_Address", request.Email);
            Inputs.Add("Address_1", (request.Address1 ?? "").Length > 100 ? (request.Address1 ?? "").Substring(0, 100) : (request.Address1 ?? ""));
            Inputs.Add("Address_2", (request.Address2 ?? "").Length > 100 ? (request.Address2 ?? "").Substring(0, 100) : (request.Address2 ?? ""));
            Inputs.Add("Address_3", "");
            Inputs.Add("Town", (request.Town ?? "").Length > 100 ? (request.Town ?? "").Substring(0, 100) : (request.Town ?? ""));
            Inputs.Add("County", "");
            Inputs.Add("Country", (request.Country ?? "").Length > 100 ? (request.Country ?? "").Substring(0, 100) : (request.Country ?? ""));
            Inputs.Add("Postcode", (request.Postcode ?? "").Length > 100 ? (request.Postcode ?? "").Substring(0, 100) : (request.Postcode ?? ""));
            Inputs.Add("redirect_url", request.ProviderCallbackUrl);
            Inputs.Add("Kx_ref", request.BookingReference);
            Inputs.Add("Deposit_amount", request.Amount.ToString());
            Inputs.Add("Payment_Amount", request.PaymentAmounts);// for UWE payment & deposit in 1...

            if (!String.IsNullOrEmpty(request.CustomValue1))
            {
                Inputs.Add("AcademicYear", request.CustomValue1);
            }

            Inputs.Add("No_Instals", request.InstallmentCount.ToString());
            Inputs.Add("residence_id", request.SiteId.ToString());
            if (String.IsNullOrEmpty(request.PaymentDates))
            {
                request.PaymentDates = "";
            }
            Inputs.Add("Payment_Dates", request.PaymentDates);
            if (request.DirectDebitType)
            {
                Inputs.Add("DDSetup", "1");
            }
            else
            {
                Inputs.Add("DDSetup", "0");
            }

            if (request.InstallmentCount == 1 && (KxDbConfiguration.GetConfiguration("ApplicationId").ToUpper() == "LIBERTY"))
            {
                request.PaymentAmounts = Convert.ToString(Convert.ToDecimal(request.PaymentAmounts) * 0.98M);
            }

            if (String.IsNullOrEmpty(request.PaymentAmounts))
            {
                request.PaymentAmounts = "0";
            }

            Inputs.Add("Payment_amounts", request.PaymentAmounts);

            if (request.TestMode)
            {
                Inputs.Add("LiveStatus", "1");
            }
            else
            {
                Inputs.Add("LiveStatus", "0");
            }
        }

        public static PaymentResponse FromResponse(PaymentCache cache)
        {
            PaymentResponse response = new PaymentResponse();
            response.SurchargeApplied = false;
            response.InternalReference = GetObjectValue(HttpContext.Current.Request["Kx_ref"]);
            response.Authorised = GetObjectValueInt(HttpContext.Current.Request["Status"]) == 1;

            try
            {
                response.Amount = GetObjectValueDecimal(HttpContext.Current.Request["Deposit_amount"]);
            }
            catch
            { }

            try
            {
                response.Amount2 = GetObjectValueDecimal(HttpContext.Current.Request["Payment_Amount"]);
            }
            catch
            { }

            try
            {
                response.Surcharge = GetObjectValueDecimal(HttpContext.Current.Request["Deposit_ref_charge"]);
                if (response.Surcharge != 0) {
                    response.SurchargeApplied = true;
                }
            }
            catch
            { }

            if (HttpContext.Current.Request["Deposit_ref"] != null)
            {
                response.ProviderReference = GetObjectValue(HttpContext.Current.Request["Deposit_ref"]);
            }

            if (HttpContext.Current.Request["Rent_ref"] != null)
            {
                response.ProviderReference2 = GetObjectValue(HttpContext.Current.Request["Rent_ref"])+"D";
            }

            if (HttpContext.Current.Request["payment_Ref"] != null)
            {
                response.ProviderReference2 = GetObjectValue(HttpContext.Current.Request["payment_Ref"])+"P";
            }

            response.OriginalRequest = cache.GetPayment(response);

            if (response.OriginalRequest != null && response.OriginalRequest.DirectDebitType && HttpContext.Current.Request["DD_ref"] != null)
            {
                response.ProviderReference2 = GetObjectValue(HttpContext.Current.Request["DD_ref"]);
            }

            if (string.IsNullOrEmpty(response.ProviderReference))
            {
                response.ProviderReference = "None";
            }
                        
            response.TransactionDate = DateTime.Now;

            return response;
        }
    }
}
