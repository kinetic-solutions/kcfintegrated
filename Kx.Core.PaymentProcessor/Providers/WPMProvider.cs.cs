using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Text;
using System.Web;
using System.Web.UI;
using Kx.Core.PaymentProcessor.Payments;
using Kx.Core.PaymentProcessor.Cache;
using Kx.Core.Exceptions;

namespace Kx.Core.PaymentProcessor.Providers
{
    public class WPMProvider : BasePaymentProvider
    {
        public WPMProvider()
        {
        }

        public static void ToForm(NameValueCollection Inputs, PaymentRequest request)
        {
            Inputs.Add("g_cardholder_name", request.FullName);
			Inputs.Add("g_Accref", request.ProviderReference);
            Inputs.Add("g_email", request.Email);
            Inputs.Add("g_accountcode", request.ProviderAccountId);
            Inputs.Add("g_redirecturl", request.ProviderCallbackUrl);
            Inputs.Add("g_post_url", request.ProviderCallbackUrl);
            Inputs.Add("g_reference", request.BookingReference);
            Inputs.Add("g_description", request.Description);
			Inputs.Add("g_amount", request.Amount.ToString());

			//
			// Ignore extra fields for RHUL provider users as this causes them problems in their BnB.
			//
			if (request.PaymentProvider != "WPMRHUL")
			{
				if (request.PaymentType == "DEPOSIT")
				{
					Inputs.Add("g_feedescription", "");
					Inputs.Add("g_depdescription", request.Description);
					Inputs.Add("g_depamount", request.Amount.ToString());
					Inputs.Add("g_feeamount", "0.00");
				}
				else
				{
					Inputs.Add("g_depdescription", "");
					Inputs.Add("g_feedescription", request.Description);
					Inputs.Add("g_feeamount", request.Amount.ToString());
					Inputs.Add("g_depamount", "0.00");
				}

                if (!String.IsNullOrEmpty(request.CustomValue1))
                {
                    Inputs.Add("Kx_AcademicYear", request.CustomValue1);
                }
                else if (request.AcademicYearId > 0)
				{
					Inputs.Add("Kx_AcademicYear", request.AcademicYearId.ToString());
				}

				// all these are for UKC's WPM provider...
				if (request.Dob != null)
				{
					DateTime dte = request.Dob.Value;

					Inputs.Add("kxdobday", dte.ToString("dd"));
					Inputs.Add("kxdobmonth", dte.ToString("MM"));
					Inputs.Add("kxdobyear", dte.ToString("yyyy"));
				}
				Inputs.Add("kxfirstname", request.FirstName);
				Inputs.Add("kxsurname", request.Surname);

				Inputs.Add("kxfamilyname", request.Surname);

				if (String.IsNullOrEmpty(request.Telephone))
				{
					Inputs.Add("kxtelephone", "0000-0000000");
				}
				else
				{
					Inputs.Add("kxtelephone", request.Telephone);
				}

				Inputs.Add("kxstudentid", request.ProviderStudentId);

				Inputs.Add("kxemail", request.Email);

				Inputs.Add("kx7502d", request.Amount.ToString());

				Inputs.Add("kxreference", request.BookingReference);

				Inputs.Add("kxredirecturl", request.ProviderCallbackUrl);

				// end of UKC fields...
			}

            Inputs.Add("g_address", request.FullAddress);
            Inputs.Add("g_town", request.Town);
            Inputs.Add("g_county", request.County);
            Inputs.Add("g_country", request.Country);
            Inputs.Add("g_postcode", request.Postcode);
            Inputs.Add("g_telephone", request.Telephone);
            Inputs.Add("g_studentid", request.ProviderStudentId);

			//
			// Ignore extra fields for RHUL provider users as this causes them problems in their BnB.
			//
			if (request.PaymentProvider != "WPMRHUL")
			{
				Inputs.Add("KX_ConfirmationPage", request.ProviderCallbackUrl);
				Inputs.Add("KX_CancellationPage", request.ProviderCallbackUrl);
				Inputs.Add("KX_ApplicationID", request.ProviderStudentId);
				Inputs.Add("KX_CUSTOM_customfield1", "");
				Inputs.Add("KX_CUSTOM_customfield2", "");

				if (request.TestMode)
				{
					Inputs.Add("LiveStatus", "1");
					Inputs.Add("kxlive", "1");
				}
				else
				{
					Inputs.Add("LiveStatus", "0");
					Inputs.Add("kxlive", "0");
				}
			}
            
        }

        public static PaymentResponse FromResponse(PaymentCache cache)
        {
            PaymentResponse response = new PaymentResponse();

            if (HttpContext.Current.Request["kx7502d"] != null)
            {
                response.InternalReference = GetObjectValue(HttpContext.Current.Request["kxreference"]);
                response.Authorised = GetObjectValueInt(HttpContext.Current.Request["status"]) == 1;
                response.ProviderReference = GetObjectValue(HttpContext.Current.Request["Deposit_ref"]);

                try
                {
                    response.Amount = GetObjectValueDecimal(HttpContext.Current.Request["kx7502d"]);
                    KxLogger.LogActivity("WPM Provider", "From response, get kx7502d value", response.Amount.ToString("F"), string.Format("Field value {0}", HttpContext.Current.Request["kx7502d"]));
                }
                catch (Exception ex)
                {
                    KxLogger.LogActivity("WPM Provider", "From response, get kx7502d error", ex.Message, "");
                }

                try
                {
                    response.Currency = "GBP";
                }
                catch
                { }
                response.OriginalRequest = cache.GetPayment(response);
            }
            else
            {
                response.InternalReference = GetObjectValue(HttpContext.Current.Request["g_reference"]);
                response.Authorised = GetObjectValueInt(HttpContext.Current.Request["transaction_status"]) == 1;
                response.ProviderReference = GetObjectValue(HttpContext.Current.Request["authorisation_reference"]);

                
                try
                {
                    response.Amount = GetObjectValueDecimal(HttpContext.Current.Request["amount"]);
                }
                catch
                { }

                try
                {
                    response.Currency = GetObjectValue(HttpContext.Current.Request["currency"]);
                }
                catch
                { }
                response.OriginalRequest = cache.GetPayment(response);

            }
            response.TransactionDate = DateTime.Now;

            return response;
        }
    }
}
