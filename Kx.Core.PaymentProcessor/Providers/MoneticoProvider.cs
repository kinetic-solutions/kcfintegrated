using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;
using System.Net;
using System.Security.Cryptography;
using System.Web;
using Kx.Core.PaymentProcessor.Payments;
using Kx.Core.PaymentProcessor.Cache;

namespace Kx.Core.PaymentProcessor.Providers
{
    public class MoneticoProvider : BasePaymentProvider
    {
        public MoneticoProvider()
        {
        }

        public static void ToForm(NameValueCollection Inputs, PaymentRequest request)
        {
            var date = DateTime.Now;
            string dateString = date.ToString("dd/MM/yyyy:HH:mm:ss");
            string amountString = request.Amount.ToString("F") + request.Currency;
            string reference = request.BookingReference.Substring(0, 4) + date.ToString("ddHHmmss");

            string returnUrl = request.ProviderCallbackUrl + string.Format("?monetico={0}", request.BookingReference);

            Inputs.Add("version", "3.0");
            Inputs.Add("TPE", request.ProviderAccountId);
            Inputs.Add("date", dateString);
            Inputs.Add("montant", amountString);
            Inputs.Add("reference", reference);
            Inputs.Add("texte-libre", request.BookingReference);
            Inputs.Add("mail", request.Email);
            Inputs.Add("lgue", request.CustomValue1);
            Inputs.Add("societe", request.CustomValue2);
            Inputs.Add("url_retour", returnUrl);
            Inputs.Add("url_retour_ok", returnUrl);
            Inputs.Add("url_retour_err", returnUrl);
            Inputs.Add("MAC", GetSeal(request, dateString, amountString, reference));
        }

        private static string GetSeal(PaymentRequest request, string dateString, string amountString, string reference)
        {
            StringBuilder code = new StringBuilder();
            code.Append(request.ProviderAccountId); // TPE
            code.Append("*");
            code.Append(dateString); // date
            code.Append("*");
            code.Append(amountString); // montant
            code.Append("*");
            code.Append(reference); // reference
            code.Append("*");
            code.Append(request.BookingReference); // texte-libre
            code.Append("*");
            code.Append("3.0"); // version
            code.Append("*");
            code.Append(request.CustomValue1); // lgue
            code.Append("*");
            code.Append(request.CustomValue2); // societe
            code.Append("*");
            code.Append(request.Email); // mail
            code.Append("*");
            code.Append(""); // nbrech
            code.Append("*");
            code.Append(""); // dataech1
            code.Append("*");
            code.Append(""); // montantech1
            code.Append("*");
            code.Append(""); // dataech2
            code.Append("*");
            code.Append(""); // montantech2
            code.Append("*");
            code.Append(""); // dataech3
            code.Append("*");
            code.Append(""); // montantech3
            code.Append("*");
            code.Append(""); // dataech4
            code.Append("*");
            code.Append(""); // montantech4
            code.Append("*");
            code.Append(""); // options

            return Seal(request, code);
        }

        private static string Seal(PaymentRequest request, StringBuilder code)
        {
            string key = request.SH1Key;
            byte[] bkey = GetUsableKey(key);
            HMACSHA1 hash = new HMACSHA1(bkey);
            hash.Initialize();

            byte[] bcode = Encoding.ASCII.GetBytes(code.ToString());

            var hashValue = hash.ComputeHash(bcode);
            StringBuilder result = new StringBuilder();

            for (int i = 0; i < hashValue.Length; i++)
            {
                result.Append(hashValue[i].ToString("X2"));
            }

            return result.ToString();
        }

        private static byte[] GetUsableKey(string key)
        {

            string hexStrKey = key.Substring(0, 38);
            string hexFinal = key.Substring(38, 2) + "00";

            int cca0 = (int)hexFinal[0];

            if (cca0 > 70 && cca0 < 97)
            {
                hexStrKey += (char)(cca0 - 23) + hexFinal.Substring(1, 1);
            }
            else
            {
                if (hexFinal.Substring(1, 1) == "M")
                    hexStrKey += hexFinal.Substring(0, 1) + "0";
                else
                    hexStrKey += hexFinal.Substring(0, 2);
            }


            return (byte[])HexStringToByteArray(hexStrKey);

        }

        private static object HexStringToByteArray(string hs)
        {

            string str = "";
            if (hs.StartsWith("0x"))
            {
                str = str + hs.Substring(2, hs.Length - 2);
            }
            else
            {
                str = str + hs;
            }

            int num = (int)Math.Round((double)(((double)str.Length) / 2.0));

            if (str.Length != (2 * num))
            {
                str = "0" + str;
            }

            byte[] buffer = new byte[((int)Math.Round((double)((((double)str.Length) / 2.0) - 1.0))) + 1];

            int num3 = buffer.Length - 1;
            for (int i = 0; i <= num3; i++)
            {

                buffer[i] = Convert.ToByte(str.Substring(2 * i, 2), 0x10);
            }

            return (byte[])buffer.Clone();
        }
        
        public static PaymentResponse FromResponse(PaymentCache cache)
        {

            PaymentResponse response = new PaymentResponse();

            if (!string.IsNullOrWhiteSpace(HttpContext.Current.Request["monetico"]))
            {
                response = cache.GetResponseFromCache(HttpContext.Current.Request.QueryString["monetico"]);
                if (response.Cache == null)
                {
                    KxWebPaymentCache dbCacheRow = new KxWebPaymentCache(null, new Guid(HttpContext.Current.Request.QueryString["monetico"]));
                    response.Cache = dbCacheRow;
                }
            }
            else
            {
                try
                {
                    string temp = GetObjectValue(HttpContext.Current.Request["texte-libre"]);

                    Guid gud = new Guid(temp);

                    response.InternalReference = gud.ToString();

                    response.ProviderReference = GetObjectValue(HttpContext.Current.Request["numauto"]);
                    response.Amount =
                        GetObjectValueDecimal(HttpContext.Current.Request["montant"].Replace("CAD", ""));

                    string responseCode = GetObjectValue(HttpContext.Current.Request["code-retour"]).ToLower();
                    response.Authorised = responseCode == "payetest" || responseCode == "paiement" ? true : false;

                    response.OriginalRequest = cache.GetPayment(response);

                    response.TransactionDate = DateTime.Now;

                    var audit = new Dictionary<string, string>();
                    foreach (string key in HttpContext.Current.Request.Form.AllKeys)
                    {
                        audit.Add(key, HttpContext.Current.Request[key]);
                    }
                    KxWebStudentPaymentAudit.LogInBoundPayment(response, audit);
                    DoConfirmationPostback();
                }
                catch (Exception)
                { }
            }

            return response;
		}

        public static void DoConfirmationPostback()
        {
            string result = "version=2\ncdr=0";

            HttpContext.Current.Response.Write(result);
            HttpContext.Current.Response.End();
        }
    }
}
