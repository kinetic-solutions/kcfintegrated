using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Text;
using System.Web;
using System.Web.UI;
using Kx.Core.PaymentProcessor.Payments;
using Kx.Core.PaymentProcessor.Cache;
using System.Threading;

namespace Kx.Core.PaymentProcessor.Providers
{
    class SHProvider:BasePaymentProvider
    {
        public SHProvider()
        {
        }
        /// <summary>
        /// note that secure hosting is highly customzied and the fields could be different for
        /// different cleints...
        /// </summary>
        /// <param name="Inputs"></param>
        /// <param name="request"></param>
        public static void ToForm(NameValueCollection Inputs, PaymentRequest request)
        {
            //clinet account inforamtion
            Inputs.Add("shreference", request.ProviderAccountId);
            Inputs.Add("checkcode", request.ProviderPassword);
            Inputs.Add("filename", request.TemplateFile);

			string strAmount = request.Amount.ToString("0.00");

			// If current thread not in english then convert CurrencyDecimalSeperator to '.', otherwise WPM get wrong amount
			if (Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName != "en")
			{
				strAmount = strAmount.Replace(Thread.CurrentThread.CurrentCulture.NumberFormat.CurrencyDecimalSeparator[0], '.');
			}

            //transaction information
			Inputs.Add("secuitems", string.Format("[||{0}|{1}|1|{2}]", request.Description, strAmount, strAmount));
            //values for shopping cart on secure hosting.
            //Inputs.Add("itemcode", "01");
            //Inputs.Add("itemdesc", request.Description);
            //Inputs.Add("itemquan", "1");
            //Inputs.Add("itempric", request.Amount.ToString());
            //Inputs.Add("itemtota", request.Amount.ToString());
            //Inputs.Add("shippingcharge", "0");
            //Inputs.Add("transactiontax", "0");


			Inputs.Add("transactionamount", strAmount);
            Inputs.Add("bill_addr_1", request.FullAddress);
            Inputs.Add("bill_city", request.Town);
            Inputs.Add("bill_state", request.County);
            Inputs.Add("bill_post_code", request.Postcode);
            Inputs.Add("bill_country", request.Country);
            Inputs.Add("bill_tel", request.Telephone);
            Inputs.Add("bill_name", request.FullName);

            Inputs.Add("cardholdersemail", request.Email);

            //call back inforamtion
            Inputs.Add("callbackurl", request.ProviderCallbackUrl);
            Inputs.Add("success_url", request.ConfirmationPageUrl);
            Inputs.Add("callbackdata", string.Format("bookingReference|{0}|transactionamount|{1}", request.BookingReference, request.Amount));

        }

        public static PaymentResponse FromResponse(PaymentCache cache)
        {
            PaymentResponse response = new PaymentResponse();
            response.InternalReference = GetObjectValue(HttpContext.Current.Request["bookingReference"]);
            response.Authorised = (GetObjectValueInt(HttpContext.Current.Request["transactionnumber"]) > 0);
            response.ProviderReference = GetObjectValue(HttpContext.Current.Request["transactionnumber"]);
            response.TransactionDate = Convert.ToDateTime(GetObjectValue(HttpContext.Current.Request["transactiontime"]));
            
            try
            {
                response.Amount = GetObjectValueDecimal(HttpContext.Current.Request["transactionamount"]);
            }
            catch
            { }

            try
            {
                response.Currency = GetObjectValue(HttpContext.Current.Request["transactioncurrency"]);
            }
            catch
            { }

            //response.OriginalRequest = cache.GetPayment(response);

            return response;


        }
    }
}
