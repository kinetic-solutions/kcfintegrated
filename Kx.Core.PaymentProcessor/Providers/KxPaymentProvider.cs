﻿using Kx.Core.PaymentProcessor.Cache;
using Kx.Core.PaymentProcessor.Payments;
using Kx.PaymentsSdk;
using Kx.PaymentsSdk.Models;
using Kx.PaymentsSdk.Schemas.Addresses;
using Kx.PaymentsSdk.Schemas.Names;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;
using System.Text;
using System.Web;
using System.Linq;
using Kx.Core.Configuration;
using Kx.Core.Data;
using Kx.Core.Exceptions;
using Payment = Kx.Core.PaymentProcessor.Payments.Payment;
using PaymentRequest = Kx.Core.PaymentProcessor.Payments.PaymentRequest;
using PaymentResponse = Kx.Core.PaymentProcessor.Payments.PaymentResponse;

namespace Kx.Core.PaymentProcessor.Providers
{
    /// <summary>
    /// !!!
    /// KxPayments requires an externally visible URL to post back to and so will NOT
    /// work under localhost.
    /// !!!
    /// </summary>
    public class KxPaymentProvider : BasePaymentProvider
    {
        // PGP Keys for Payments
        private static string _privateKey;
        private static string _publicKey;
        private static string privateKeyPhrase = "";

        /// <summary>
        /// Constructor
        /// </summary>
        public KxPaymentProvider()
        {
        }

        /// <summary>
        /// Method for reading in a string from a file, used for reading in public/private keys.
        /// </summary>
        /// <param name="filename">Name of file to read from</param>
        /// <returns>String with contents</returns>
        private static string ReadFile(string filename)
        {
            string fileContent = string.Empty;
            if (File.Exists(filename))
            {
                using (StreamReader r = new StreamReader(filename))
                {
                    fileContent = r.ReadToEnd();
                }
            }
            return fileContent;
        }

        /// <summary>
        /// Creates a Stream from a string read in from a file. Used for making a memory stream containing public/private keys. 
        /// </summary>
        /// <param name="key">Key to read</param>
        /// <returns>MemoryStream with UTF8 contents</returns>
        private static Stream GetStream(string key)
        {

            string fileContent = "";
            if (key == "private")
            {
                if (_privateKey == null)
                {
                    string filepath = ConfigurationManager.AppSettings["KxPaymentKeys"];
                    _privateKey = ReadFile($"{filepath}/privatekey.asc");
                }
                fileContent = _privateKey;
            }

            if (key == "public")
            {
                if (_publicKey == null)
                {
                    string filepath = ConfigurationManager.AppSettings["KxPaymentKeys"];
                    _publicKey = ReadFile($"{filepath}/publickey.asc");
                }
                fileContent = _publicKey;
            }
            return new MemoryStream(Encoding.UTF8.GetBytes(fileContent));
        }

        /// <summary>
        /// Gets public and private keys for KxPayments 
        /// </summary>
        /// <param name="privateKey">Private Key</param>
        /// <param name="publicKey">Public Key</param>
        private static void GetKxPaymentStreams(out Stream privateKey, out Stream publicKey)
        {
            privateKey = GetStream("private");
            publicKey = GetStream("public");
        }

        /// <summary>
        /// Outbound Payment method (ToForm)
        /// </summary>
        /// <param name="Inputs"></param>
        /// <param name="pmtRequest"></param>
        public static void ToForm(NameValueCollection Inputs, Payments.PaymentRequest pmtRequest)
        {
            privateKeyPhrase = pmtRequest.ProviderPassword;
            GetKxPaymentStreams(out Stream privateKey, out Stream publicKey);
            var client = new KxPaymentsRequestClient(new PGPConfiguration(privateKey, privateKeyPhrase, publicKey));
            var merchantConfiguration = new MerchantConfiguration(pmtRequest.Currency, pmtRequest.MerchantCountry, pmtRequest.ProviderUrl);
            var customer = new Customer(
                new DefaultName(new[] { pmtRequest.FirstName, pmtRequest.Surname }),
                new DefaultAddress(new[] { pmtRequest.Address1, "", pmtRequest.Address2, pmtRequest.County }, pmtRequest.Postcode, pmtRequest.Country), pmtRequest.Email,
                pmtRequest.StudentId);

            ImmediatePayment[] immediatePayments = pmtRequest.Amount > 0m ? new[] { new ImmediatePayment(pmtRequest.Description, DecimalToInt(pmtRequest.Amount)) } : Array.Empty<ImmediatePayment>();

            var scheduledPayments = (pmtRequest.Payments != null && pmtRequest.Payments.Count > 0)
                ? pmtRequest.Payments[0].Instalments.Select(i => new ScheduledPayment(
                    i.Description,
                    DecimalToInt(i.Amount),
                    i.InstalmentDate ?? DateTime.Now.Date.AddDays(1)
                )).ToArray()
                : Array.Empty<ScheduledPayment>();

            Dictionary<string, string> metaData = ParseCustomFields(pmtRequest.CustomValue1);

            var urls = new Urls(
                new Uri($"{pmtRequest.ProviderCallbackUrl}?kxpaymentwebhook=true"),
                new Uri($"{pmtRequest.ConfirmationPageUrl}?kxpayment={pmtRequest.BookingReference}")
            );

            string redirectUrl = "";
            
            // Try catch added in case the PaymentRequest SDK returns an exception due to dodgy data
            try
            {
                redirectUrl = pmtRequest.ConfirmationPageUrl;
                //pha
                var request = new PaymentsSdk.Models.PaymentRequest(
                pmtRequest.BookingReference,
                merchantConfiguration,
                customer,
                immediatePayments,
                scheduledPayments,
                urls,
                metaData);

                // Try catch added so that we capture when keys are incorrect or other failures coming from Payments SDK Request Client when trying to form a new payment request.
                try
                {
                    redirectUrl = client.NewPaymentRequest(request).ToString();
                }
                catch (Exception e)
                {
                    KxLogger.LogActivity("KxPaymentProvider", "PostPayment", "NewPaymentRequest Exception (incorrect keys?)", e.ToString());
                }

                //  AuditOutboundPaymentData(pmtRequest, request);
            }
            catch(Exception e)
            {
                KxLogger.LogActivity("KxPaymentProvider", "PostPayment", "Failure forming Payment Request - Erroneous data?", e.ToString());
            }

            // If the payment makes it out successfully
            if (!String.IsNullOrEmpty(redirectUrl)) {
                KxLogger.LogActivity("KxPaymentProvider", "PostPayment", "SessionID.OUT", HttpContext.Current.Session.SessionID);
                HttpContext.Current.Response.Redirect(redirectUrl);
            }
            else
            {
                KxLogger.LogActivity("KxPaymentProvider", "PostPayment", "Redirect URL not formed", "This likely means the KxPayments Payment Request was incorrectly formed and/or unable to return a redirect url.");
            }
        }

        /// <summary>
        /// Method for breaking custom fields out of the customValue1 part of the payment request
        /// </summary>
        /// <param name="customValue1">customValue1 from the Payment Request</param>
        /// <returns>Dictionary of custom fields to send</returns>
        private static Dictionary<string, string> ParseCustomFields(string customValue1)
        {
            Dictionary<string, string> dctCustomFields = new Dictionary<string, string>();
            if (!string.IsNullOrEmpty(customValue1))
            {
                var fieldSeperator = '|';
                var propertySeperator = ':';

                var fieldsArray = customValue1.Split(fieldSeperator);

                if (fieldsArray.Length > 0)
                {
                    foreach (var field in fieldsArray)
                    {
                        var property = field.Split(propertySeperator);
                        if (property.Length != 2)
                            continue;

                        dctCustomFields.Add(property[0], property[1]);
                    }
                }
            }

            return dctCustomFields;
        }

        /// <summary>
        /// Audits Outbound Payment Details IF "AuditPayments" setting is true. 
        /// </summary>
        /// <param name="pmtRequest">Payment request (original)</param>
        /// <param name="kxPaymentsRequest">Kxpayments request object</param>
        private static void AuditOutboundPaymentData(PaymentRequest pmtRequest, PaymentsSdk.Models.PaymentRequest kxPaymentsRequest)
        {
            
            if (KxDbConfiguration.GetConfigurationBool("AuditPayments", true))
            {
                try
                {
                    NameValueCollection auditFields = new NameValueCollection();
                    auditFields.Add("WebHookUrl", kxPaymentsRequest.Urls.WebhookUrl.ToString());
                    auditFields.Add("RedirectUrl", kxPaymentsRequest.Urls.RedirectUrl.ToString());
                    auditFields.Add("Currency", kxPaymentsRequest.MerchantConfiguration.Currency);
                    auditFields.Add("Country", kxPaymentsRequest.MerchantConfiguration.Country);
                    foreach (var payment in kxPaymentsRequest.ImmediatePayments)
                    {
                        auditFields.Add("Immediate Payment Amount", IntToDecimal(payment.Amount).ToString("F2"));
                        auditFields.Add("Immediate Payment Name", payment.Name);
                    }

                    int paymentNo = 1;
                    foreach (var payment in kxPaymentsRequest.ScheduledPayments)
                    {
                        auditFields.Add($"Scheduled Payment {paymentNo} Amount", IntToDecimal(payment.Amount).ToString("F2"));
                        auditFields.Add($"Scheduled Payment {paymentNo} Name", payment.Name);
                        auditFields.Add($"Scheduled Payment {paymentNo} Date", payment.DateToBeTaken.ToString("yyyy-MM-dd"));
                        ++paymentNo;
                    }

                    foreach (var key in kxPaymentsRequest.Metadata.Keys)
                    {
                        auditFields.Add($"CustomField:{key}", kxPaymentsRequest.Metadata[key]);
                    }
                    
                    KxWebStudentPaymentAudit.LogOutBoundPayment(pmtRequest, auditFields);
                }
                catch (Exception e)
                {
                    new KxData().LogException("KxPaymentProvider", "AuditLogOutboundPayment", "Log outbound payment", e.Message);
                }             
            }
           
        }

        private static int DecimalToInt(decimal amount)
        {
            return Convert.ToInt32(amount * 100.0m);
        }

        private static decimal IntToDecimal(int amount)
        {
            return Convert.ToDecimal(amount / 100.0m);
        }

        /// <summary>
        /// Payment Response mechanism, KxPayments has two post backs, one from a 'webhook' the other from the user coming back in. 
        /// </summary>
        /// <param name="cache">Cache entry</param>
        /// <returns>A Payment Response</returns>
        public static Payments.PaymentResponse FromResponse(PaymentCache cache)
        {
            Payments.PaymentResponse response = null;

            // If it's the user returning
            if ((HttpContext.Current.Request.QueryString["kxpayment"] != null))
            {
                response = cache.GetResponseFromCache(HttpContext.Current.Request.QueryString["kxpayment"]);
                if (response.Cache == null)
                {
                    KxWebPaymentCache dbCacheRow = new KxWebPaymentCache(null, new Guid(HttpContext.Current.Request.QueryString["kxpayment"]));
                    response.Cache = dbCacheRow;
                    if (!response.Cache.TimeStampIn.HasValue)
                    {
                        response.InternalReference = HttpContext.Current.Request.QueryString["kxpayment"];
                        response.OriginalRequest = cache.GetPayment(response);
                    }
                }
            }
            // If it's the webhook
            else
            {
                var stream = new MemoryStream();
                HttpContext.Current.Request.InputStream.CopyTo(stream);
                stream.Position = 0;
                string body = string.Empty;
                using (StreamReader inputStream = new StreamReader(stream))
                {
                    body = inputStream.ReadToEnd();
                }

                PaymentResponseModel responseModel = JsonConvert.DeserializeObject<PaymentResponseModel>(body);
                GetKxPaymentStreams(out Stream privateKey, out Stream publicKey);
                var client = new KxPaymentsRequestClient(new PGPConfiguration(privateKey, privateKeyPhrase, publicKey));
                var validresponse = client.VerifyResponse(responseModel);

                List<Payment> lstPayments = null;

                if (validresponse.ScheduledPayments != null)
                {
                    lstPayments = new List<Payment>()
                    {
                        new Payment()
                        {
                            Instalments = validresponse.ScheduledPayments.Select
                                (sp => new PaymentInstalment {Amount = IntToDecimal(sp.Amount), Description = sp.Name, InstalmentDate = sp.DateToBeTaken}).ToList()
                        }
                    };
                }


                response = new Payments.PaymentResponse()
                {
                    InternalReference = validresponse.BookingReference,
                    ProviderReference = validresponse.PaymentPlanId,
                    Authorised = true,
                    Amount = IntToDecimal(validresponse.ImmediatePayments?.ElementAtOrDefault(0)?.Amount ?? 0),
                    Currency = "GBP",
                    TransactionDate = DateTime.Now,
                    Payments = lstPayments
                };

                response.OriginalRequest = cache.GetPayment(response);
                if (response.OriginalRequest != null)
                {
                    KxWebPaymentCache dbCacheRow = new KxWebPaymentCache(null, new Guid(validresponse.BookingReference));
                    response.Cache = dbCacheRow;
                    response.InternalReference = response.Cache.CacheId.ToString();

                    AuditInboundPaymentData(response, validresponse);
                }
            }
            return response;
        }

        private static void AuditInboundPaymentData(PaymentResponse response, IPaymentResponse validResponse)
        {
            try
            {
                var auditInputs = new Dictionary<string, string>();

                auditInputs.Add("Booking Reference", validResponse.BookingReference);
                auditInputs.Add("Payment Plan ID", validResponse.PaymentPlanId);

                if (validResponse.ImmediatePayments != null)
                {
                    foreach (var paymentResponse in validResponse.ImmediatePayments)
                    {
                        auditInputs.Add("Immediate Payment ID", paymentResponse.PaymentId);
                        auditInputs.Add("Immediate Payment Name", paymentResponse.Name);
                        auditInputs.Add("Immediate Payment Amount", IntToDecimal(paymentResponse.Amount).ToString("F2"));
                    }
                }

                if (validResponse.ScheduledPayments != null)
                {
                    int paymentNo = 1;
                    foreach (var paymentResponse in validResponse.ScheduledPayments)
                    {
                        auditInputs.Add($"Scheduled Payment {paymentNo} Name", paymentResponse.Name);
                        auditInputs.Add($"Scheduled Payment {paymentNo} Amount", IntToDecimal(paymentResponse.Amount).ToString("F2"));
                        auditInputs.Add($"Scheduled Payment {paymentNo} Date", paymentResponse.DateToBeTaken.ToString("yyyy-MM-dd"));
                        ++paymentNo;
                    }
                }

                if (validResponse.Metadata != null)
                {
                    foreach (var key in validResponse.Metadata.Keys)
                    {
                        auditInputs.Add($"Custom Field {key}", validResponse.Metadata[key]);
                    }
                }

                KxWebStudentPaymentAudit.LogInBoundPayment(response, auditInputs);
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity("KxPaymentProvider", "AuditInboundPaymentData", ex.Message, "");
            }
        }
    }
}
