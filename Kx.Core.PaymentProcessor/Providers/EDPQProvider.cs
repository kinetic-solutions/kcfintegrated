using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Text;
using System.Net;
using System.Web;
using Kx.Core.PaymentProcessor.Payments;
using Kx.Core.PaymentProcessor.Helpers.UKPLC;
using Kx.Core.PaymentProcessor.Cache;

namespace Kx.Core.PaymentProcessor.Providers
{
    public class EDPQProvider : BasePaymentProvider
    {
        public EDPQProvider()
        {
        }

        public static void ToForm(NameValueCollection Inputs, PaymentRequest request)
        {
            string password = EPDQTransaction.DecryptString(request.ProviderPassword, PaymentRequest.SecretKey);
            string encriptedData = "";
            using (WebClient client = new WebClient())
            {
                // Add the headers
                client.Headers.Add("Content-Type", "application/x-www-form-urlencoded");

                // Setup the post data
                string postData = string.Format("clientid={0}&password={1}&oid={2}&chargetype={3}&currencycode={4}&total={5}",
                    request.ProviderAccountId, password, request.BookingReference, "Auth", request.Currency, request.Amount.ToString());

                // Convert Post Data to Bytes
                byte[] postDataBytes = Encoding.ASCII.GetBytes(postData);

                // Call the web server
                byte[] responseDataBytes = client.UploadData(request.ProviderEncriptionUrl, "POST", postDataBytes);

                // Return the Result
                encriptedData = Encoding.ASCII.GetString(responseDataBytes);
                //return Encoding.ASCII.GetString(responseDataBytes);
            }
            Inputs.Add("merchantdisplayname", request.MerchantDisplayName);
            Inputs.Add("returnurl", request.ProviderCallbackUrl);
            
            //PaymentResponse response = ProcessResponse();
            HttpContext.Current.Response.Clear();
            //HttpContext.Current.Response.Write("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\" >");
            HttpContext.Current.Response.Write("<html><head>");
            //HttpContext.Current.Response.ContentType = "application/x-www-form-urlencoded";
            HttpContext.Current.Response.Write(string.Format("</head><body onload=\"document.{0}.submit()\">", "form1"));
            HttpContext.Current.Response.Write(string.Format("<form name=\"{0}\" method=\"{1}\" action=\"{2}\" >", "form1", "POST", request.ProviderUrl));
            HttpContext.Current.Response.Write(encriptedData);//.Replace("epdqdata", "\"epdqdata\"").Replace("hidden", "\"hidden\""));
            //HttpContext.Current.Response.Write("</INPUT>");
            if (Inputs != null)
            {
                for (int i = 0; i < Inputs.Keys.Count; i++)
                {
                    HttpContext.Current.Response.Write(string.Format("<input name=\"{0}\" type=\"hidden\" value=\"{1}\"/>", Inputs.Keys[i], Inputs[Inputs.Keys[i]]));
                }
            }
            HttpContext.Current.Response.Write("<p>You will be now redirected to EPDQ for processing of your payment, if you have ");
			HttpContext.Current.Response.Write("not redirected within the next 10 seconds, please click on the redirect button</p>");
			HttpContext.Current.Response.Write("<INPUT type=\"submit\" value=\"Redirect\" ID=\"Submit1\" NAME=\"Submit1\" />");
            HttpContext.Current.Response.Write("</form>");
            HttpContext.Current.Response.Write("</body></html>");
            HttpContext.Current.Response.End();

        }

        public static PaymentResponse FromResponse(HttpContext context, PaymentCache cache)
        {
            PaymentResponse response = new PaymentResponse();
            response.InternalReference = GetObjectValue(HttpContext.Current.Request["oid"]);
            
            response.Authorised = GetObjectValue(HttpContext.Current.Request["transactionstatus"].ToUpper()) == "SUCCESS" ? true : false;
            response.ProviderReference = GetObjectValue(HttpContext.Current.Request["oid"]);
            response.Amount = GetObjectValueDecimal(HttpContext.Current.Request["total"]);
            response.TransactionDate = DateTime.Now;

            response.OriginalRequest = cache.GetPayment(response);
         
            return response;
		}
    }
}
