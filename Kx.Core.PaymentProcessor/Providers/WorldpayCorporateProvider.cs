﻿using System;
using System.Collections.Specialized;
using System.Text;
using System.Web;
using Kx.Core.Exceptions;
using Kx.Core.PaymentProcessor.Payments;
using Kx.Core.PaymentProcessor.Cache;
using System.Xml.Linq;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Xml;
using Kx.Core.PaymentProcessor.Helpers;
using Kx.Core.Configuration;
using Kx.Core.Data;
using System.Linq;

namespace Kx.Core.PaymentProcessor.Providers
{
    public class WorldpayCorporateProvider : BasePaymentProvider
    {
        public WorldpayCorporateProvider()
        {
        }

        public static void ToForm(NameValueCollection Inputs, PaymentRequest request)
        {
            //https://en.wikipedia.org/wiki/Basic_access_authentication
            var basicAuthString = string.Format("{0}:{1}", request.ProviderAccountId, request.ProviderPassword);
            var basicAuthBytes = Encoding.ASCII.GetBytes(basicAuthString);
            var basicAuthentication = Convert.ToBase64String(basicAuthBytes);
            //var b64BasicAuthentication = $"Basic {basicAuthentication}";

            XDocument xmlRequest = new XDocument(
                new XDeclaration("1.0", "utf-8", null),
                new XDocumentType("paymentService", "-//Worldpay//DTD Worldpay PaymentService v1//EN", "http://dtd.worldpay.com/paymentService_v1.dtd", null),
                new XElement("paymentService",
                    new XAttribute("version", 1.4),
                    new XAttribute("merchantCode", request.MerchantDisplayName),// request.ProviderAccountId),
                    new XElement("submit",
                        new XElement("order",
                                new XAttribute("orderCode", request.BookingReference),
                                //new XAttribute("installationId", request.NominalCode),
                                new XElement("description", request.Description
                                ),
                                new XElement("amount",
                                    new XAttribute("currencyCode", request.Currency ?? "GBP"),
                                    new XAttribute("exponent", 2),
                                    new XAttribute("value", (int)(request.Amount * 100))
                                ),
                                new XElement("paymentMethodMask",
                                    new XElement("include", new XAttribute("code", "ALL"))
                                ),

                                new XElement("shopper", 
                                    new XElement("shopperEmailAddress", request.Email),
                                    new XElement("shopperDetails",
                                        new XElement("firstName", request.FirstName),
                                        new XElement("lastName", request.Surname)
                                    )
                                ),
                                new XElement("shippingAddress",
                                    new XElement("address",
                                        new XElement("address1", request.Address1),
                                        new XElement("address2", request.Address2),
                                        new XElement("address3", string.Empty),
                                        new XElement("postalCode", request.Postcode),
                                        new XElement("city", request.Town),
                                        new XElement("state", request.County),
                                        new XElement("countryCode", request.CountryCode)
                                    )
                                ),
                                new XElement("billingAddress",
                                    new XElement("address",
                                        new XElement("address1", request.Address1),
                                        new XElement("address2", request.Address2),
                                        new XElement("address3", string.Empty),
                                        new XElement("postalCode", request.Postcode),
                                        new XElement("city", request.Town),
                                        new XElement("state", request.County),
                                        new XElement("countryCode", request.CountryCode)
                                    )
                                )
                            )
                        )
                    )
                );


            if (!string.IsNullOrWhiteSpace(request.PaymentPageID))
            {
                var element = xmlRequest.Root.Descendants("order").FirstOrDefault();
                element.SetAttributeValue("installationId", request.PaymentPageID);
            }

            using (var client = new HttpClient())
            {
                // Add an Accept header for JSON format.
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/xml"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", basicAuthentication);// "VEZDWjZTTlFGRDBYTzNKNUhGRjI6N2NmUHc2TjVNRlFGNzRjNVlZSDI=");

                var strxmldata = XMLDocToString(xmlRequest);
                var content = new StringContent(strxmldata, Encoding.UTF8, "application/xml");
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
                // List data response
                using (var response = client.PostAsync(request.ProviderUrl, content).Result)
                {
                    if (response.IsSuccessStatusCode)
                    {
                        // Parse the response body.
                        var formData = response.Content.ReadAsStringAsync().Result;

                        var xmldoc = new XmlDocument();
                        xmldoc.LoadXml(formData);

                        KxLogger.LogActivity("WorldpayCorporateProvider", "PostPayment", "Reference", formData);
                        new KxData().LogException("WorldpayCorporate", "PostPayment", "Reference XML data", formData);


                        var refNode = xmldoc.SelectSingleNode(".//reference");
                        var successURL = HttpUtility.UrlEncode(request.ConfirmationPageUrl);
                        var errorURL = HttpUtility.UrlEncode(request.CancellationPageUrl);
                        var redirectURL = refNode.InnerText;
                        redirectURL += $"&successURL={successURL}&failureURL={errorURL}&errorURL={errorURL}&cancelURL={errorURL}&pendingURL={errorURL}";

                        var referenceid = refNode.Attributes["id"].Value;
                        request.CustomValue1 = referenceid;
                        AuditOutboundPaymentData(request, strxmldata, redirectURL);
                        HttpContext.Current.Response.Redirect(redirectURL);
                    }
                    else
                    {
                        KxLogger.LogActivity("Worldpay Corporate Provider", "ToFormOnline", "Exception on call to API", response.ReasonPhrase);
                    }
                }
            }
        }

        public static string XMLDocToString(XDocument xDoc)
        {
            using (StringWriter writer = new Utf8StringWriter())
            {
                xDoc.Save(writer, SaveOptions.None);
                return writer.ToString();
            }
        }

        public static PaymentResponse FromResponse(PaymentCache cache)
        {
            var queryString = HttpContext.Current.Request.QueryString;
            var orderKey = queryString["orderkey"];
            var orderKeyParams = orderKey.Split('^');
            var bookingReference = orderKeyParams[2];

            var response = new PaymentResponse()
            {
                ProviderReference2 = GetObjectValue(queryString["mac2"]),
                InternalReference = bookingReference,
                Authorised = queryString["paymentStatus"] == "AUTHORISED",
                Amount = GetObjectValueDecimal(queryString["paymentAmount"]) / 100,
                Currency = GetObjectValue(queryString["paymentCurrency"]),
                TransactionDate = DateTime.Now
            };

            response.OriginalRequest = cache.GetOriginalRequest(bookingReference);

            if (response.OriginalRequest != null)
            {
                response.ProviderReference = response.OriginalRequest.CustomValue1;
                response.OriginalRequest = cache.GetPayment(response);
                KxWebPaymentCache dbCacheRow = new KxWebPaymentCache(null, new Guid(bookingReference));
                response.Cache = dbCacheRow;
                response.InternalReference = response.Cache.CacheId.ToString();
            }

            return response;
        }


        private static void AuditOutboundPaymentData(PaymentRequest pmtRequest, string strxmldata, string redirectUrl)
        {
            try
            {
                NameValueCollection auditFields = new NameValueCollection();
                auditFields.Add("orderCode", pmtRequest.BookingReference);
                auditFields.Add("Currency", pmtRequest.Currency);
                auditFields.Add("Amount", Convert.ToDecimal(pmtRequest.Amount).ToString("F2"));
                auditFields.Add("XML Data", strxmldata);
                auditFields.Add("RedirectUrl", redirectUrl);

                if (KxDbConfiguration.GetConfigurationBool("AuditPayments", true))
                {
                    KxWebStudentPaymentAudit.LogOutBoundPayment(pmtRequest, auditFields);
                }
            }
            catch (Exception e)
            {
                new KxData().LogException("Worldpay Corporate Provider", "AuditLogOutboundPayment", "Log outbound payment", e.Message);
            }
        }
    }
}
