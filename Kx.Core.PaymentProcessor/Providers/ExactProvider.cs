﻿using System;
using System.Collections.Specialized;
using System.Text;
using System.Web;
using Kx.Core.PaymentProcessor.Payments;
using Kx.Core.PaymentProcessor.Cache;
using System.Security.Cryptography;

namespace Kx.Core.PaymentProcessor.Providers
{
    public class ExactProvider : BasePaymentProvider
    {
        public ExactProvider()
        {
        }

        public static void ToForm(NameValueCollection Inputs, PaymentRequest request)
        {
            StringBuilder sb = new StringBuilder();
            // x_login^x_fp_sequence^x_fp_timestamp^x_amount^x_currency   

            TimeSpan span = DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1, 0, 0, 0));
            string timestamp = span.TotalSeconds.ToString();
            timestamp = timestamp.Substring(0, 10);

            String x_login = request.ProviderAccountId;
            String x_fp_sequence = request.StudentId;
            String x_fp_timestamp = timestamp;
            String x_amount = request.Amount.ToString();
            String x_currency = request.Currency;

            sb.Append(x_login).Append("^")
                .Append(x_fp_sequence)
                .Append("^")
                .Append(x_fp_timestamp)
                .Append("^")
                .Append(x_amount)
                .Append("^")
                .Append(x_currency);

            // Convert string to array of bytes.   
            byte[] data = Encoding.UTF8.GetBytes(sb.ToString());

            // Transaction Key - Web.Config
            String transactionKey = Kx.Core.Configuration.KxDbConfiguration.GetConfiguration("TransactionKey");
            byte[] key = Encoding.UTF8.GetBytes(transactionKey);

            // Create HMAC-MD5 Algorithm;   
            HMACMD5 hmac = new HMACMD5(key);
            byte[] hashBytes = hmac.ComputeHash(data);

            // Convert to HEX string.   
            String x_fp_hash = BitConverter.ToString(hashBytes).Replace("-", "");
            x_fp_hash = x_fp_hash.ToLower();


            // Required
            Inputs.Add("x_login", x_login);
            Inputs.Add("x_fp_sequence", x_fp_sequence);
            Inputs.Add("x_fp_timestamp", x_fp_timestamp);
            Inputs.Add("x_amount", x_amount);
            Inputs.Add("x_currency_code", x_currency);
            Inputs.Add("x_fp_hash", x_fp_hash);
            Inputs.Add("x_show_form", "PAYMENT_FORM");
            

            // Optional Customer Information
            Inputs.Add("x_cust_id", request.StudentId);
            Inputs.Add("x_first_name", request.FirstName);
            Inputs.Add("x_last_name", request.Surname);
            Inputs.Add("x_email", request.Email);

            if (!String.IsNullOrEmpty(request.CustomValue1))
            {
                Inputs.Add("x_po_num", request.CustomValue1);
            }

            Inputs.Add("x_description", request.BookingReference);
            Inputs.Add("x_receipt_link_url",request.ProviderCallbackUrl);
            

            // Optional Test Mode tag - only post if needed, otherwise will fail.
            if (request.TestMode)
            {
                Inputs.Add("x_test_request", "TRUE");
            }

            // Optional Order information
            String item = String.Format("1<|>{0}<|>{0}<|>1<|>{1}<|>YES", request.Description, request.Amount);
            Inputs.Add("x_line_item", item);

         
        }

        public static PaymentResponse FromResponse(PaymentCache cache)
        {
            PaymentResponse response = new PaymentResponse();
            response.InternalReference = GetObjectValue(HttpContext.Current.Request["x_description"]);
            response.ProviderReference = GetObjectValue(HttpContext.Current.Request["x_trans_id"]);
            
            response.AuthorisationMessage = GetObjectValue(HttpContext.Current.Request["Authorization_Num"]);
            response.Currency = GetObjectValue(HttpContext.Current.Request["x_currency_code"]);
            response.Amount = GetObjectValueDecimal(HttpContext.Current.Request["x_amount"]);
            response.Authorised = GetObjectValue(HttpContext.Current.Request["Transaction_Approved"]).ToUpper() == "YES" ? true : false;

            response.InternalReference = GetObjectValue(HttpContext.Current.Request["x_description"]);
            response.OriginalRequest = cache.GetPayment(response);
            response.Cache.TransactionId = GetObjectValueInt(HttpContext.Current.Request["x_trans_id"]);
            response.Cache.Name = GetObjectValue(HttpContext.Current.Request["CardHoldersName"]);

            String temp = GetObjectValue(HttpContext.Current.Request["exact_ctr"]);
            temp = temp.Replace(Environment.NewLine, "<br />");

            HttpContext.Current.Session["exact_ctr"] = temp;

            try
            {
                double transtime = GetObjectValueDouble(HttpContext.Current.Request["x_fp_timestamp"]);
                response.TransactionDate = new DateTime(1970, 1, 1, 0, 0, 0, 0).AddMilliseconds(transtime);
            }
            catch
            {
                response.TransactionDate = DateTime.Now;
            }
            return response;
        }
    }
}
