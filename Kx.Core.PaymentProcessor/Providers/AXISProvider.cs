using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;
using System.Net;
using System.Web;
using Kx.Core.PaymentProcessor.Payments;
using Kx.Core.PaymentProcessor.Cache;

namespace Kx.Core.PaymentProcessor.Providers
{
    public class AXISProvider : BasePaymentProvider
    {
        public AXISProvider()
        {
        }

        public static void ToForm(NameValueCollection Inputs, PaymentRequest request)
        {
            Inputs.Add("portal_id", request.ProviderAccountId);
            Inputs.Add("rurl", request.ProviderCallbackUrl);
            Inputs.Add("back.url", request.CancellationPageUrl);
            Inputs.Add("amt", request.Amount.ToString());
            Inputs.Add("ref", request.BookingReference);
            Inputs.Add("fund.code", request.CustomValue1);
            Inputs.Add("fund.name", request.CustomValue2);
            Inputs.Add("requid", request.BookingReference);
        }

        public static PaymentResponse FromResponse(PaymentCache cache)
        {
            PaymentResponse response = new PaymentResponse();
            response.InternalReference = (HttpContext.Current.Request["requid"] ?? "").ToString();
            response.AuthorisationMessage = (HttpContext.Current.Request["auth"] ?? "").ToString();
            response.Authorised = ((HttpContext.Current.Request["status"] ?? "").ToString() == "0");
            response.OriginalRequest = null;
            response.TransactionDate = DateTime.Now;
            response.Processed = true;

            return response;
		}
    }
}
