﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Text;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Xml;
using Kx.Core.Data;
using Kx.Core.Exceptions;
using Kx.Core.PaymentProcessor;
using Kx.Core.PaymentProcessor.Payments;
using Kx.Core.PaymentProcessor.Cache;
using Kx.Core.Utility;
using Kx.Core.Configuration;

namespace Kx.Core.PaymentProcessor.Providers
{
    public class WpmKxProvider : BasePaymentProvider
    {
        public WpmKxProvider()
        {
        }

        public static void ToForm(NameValueCollection Inputs, PaymentRequest request)
        {
           
            if (request.Payments.Count == 2 && (KxDbConfiguration.GetConfiguration("ApplicationId").ToUpper() == "LIBERTY"))
                
            {
                if (request.Payments[0].PayType == KxWpmPaymentType.RCP &&
                    request.Payments[1].PayType == KxWpmPaymentType.Deposit)
                {

                    if (request.Payments[0].Instalments.Count == 1)
                    {
                        request.Payments[0].Instalments[0].Amount =
                            (Convert.ToDecimal(request.Payments[0].Instalments[0].Amount) * 0.98M);
                    }
                }                

            }

            Inputs.Add( "XMLRequest",HttpContext.Current.Server.HtmlEncode( request.ToXml() ) );
        }

        public static PaymentResponse FromResponse(PaymentCache cache,PaymentProcessor pp)
        {
            PaymentResponse response = new PaymentResponse();

            string formstring = "";
            try
            {
                formstring = HttpContext.Current.Request.Form.ToString();
            }
            catch { }

            //(formstring.ToUpper().Contains("KXPAYMENTPROCESSOR"))
            if ((HttpContext.Current.Request.QueryString["bookingreference"] != null) &&
                !(formstring.ToUpper().Contains("KXPAYMENTPROCESSOR")))
            {
                response = cache.GetResponseFromCache(HttpContext.Current.Request.QueryString["bookingreference"]);
                if (response.Cache == null)
                {
                    KxWebPaymentCache dbCacheRow = new KxWebPaymentCache(null, new Guid(HttpContext.Current.Request.QueryString["bookingreference"]));
                    response.Cache = dbCacheRow;
                }
            }
            else
            {
                string xml = "";

                try
                {
                    xml = HttpContext.Current.Server.UrlDecode(HttpContext.Current.Request.Form.ToString());

                    try
                    {
                        KxData data = new KxData();
                        data.ExecuteAuditLog("Processing Xml","", xml);
                        data.CloseConnection();
                    }
                    catch { }

                    PaymentRequest result = PaymentRequest.LoadFromWpmXml(xml);

                    response.GetResponseFromWpmRequest(result);

                    var payment1 = response.Payments.Find(s => s.PayType == KxWpmPaymentType.Deposit);
                    payment1 = payment1 ?? response.Payments.Find(s => s.PayType == KxWpmPaymentType.PayNow);

                    Payment payment2 = null;
                    if (payment1 != null)
                    {
                        payment2 = response.Payments.Find(s => s.PayType != payment1.PayType);
                    }
                    else
                    {
                        payment1 = response.Payments[0];
                    }

                    if (payment1 != null)
                    {
                        response.ProviderReference = payment1.TransactionReference;
                        response.Amount = payment1.Amount;
                        response.Surcharge = payment1.SurchargeAmount;
                        response.SurchargeApplied = payment1.SurchargeApplied;
                    }
                    if (payment2 != null)
                    {
                        response.ProviderReference2 = payment2.TransactionReference;
                        response.Amount2 = payment2.Amount;
                    }

                    if (HttpContext.Current.Session["LastTrans"] == null || HttpContext.Current.Session["LastTrans"].ToString() != result.UniqueId)
                    {
                        HttpContext.Current.Session["LastTrans"] = result.UniqueId;
                        try
                        {
                            if (cache.GetPayment(response) != null)
                            {

                                KxWebPaymentCache dbCacheRow = new KxWebPaymentCache(null, new Guid(result.BookingReference));
                                response.Cache = dbCacheRow;

                                try
                                {
                                    var audit = new Dictionary<string, string>();
                                    audit.Add("Xml", formstring);
                                    response.InternalReference = response.Cache.CacheId.ToString();
                                    KxWebStudentPaymentAudit.LogInBoundPayment(response, audit);
                                }
                                catch
                                {   
                                }
                                DoConfirmationPostback(result.BookingReference);
                            }
                            else
                            {
                                //ERROR!!!!
                                KxLogger.LogActivity(null, "Unable to bind payment", result.BookingReference, "");
                            }
                        }
                        catch 
                        { 
                        }
                    }
                    else
                    {
                        KxLogger.LogActivity(null, "Duplicate post", result.BookingReference, "");
                    }
                    //response.OriginalRequest = cache.GetPayment(result.BookingReference);
                }
                catch (Exception ex)
                {
                    KxLogger.LogActivity("WPMKxProvider", "FromResponse", ex.Message, "");
                }
            }

            return response;
        }

        public static void DoConfirmationPostback(string BookingReference)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
            sb.AppendLine("<WPMPostback>");
            sb.AppendLine(String.Format("    <BookingReference>{0}</BookingReference>", BookingReference));
            sb.AppendLine(              "    <Success>True</Success>");
            sb.AppendLine(String.Format("    <Date>{0}</Date>",KxDateTime.DateToWpmDate(DateTime.Now)));
            sb.AppendLine("</WPMPostback>");

            HttpContext.Current.Response.Write(sb.ToString());
            HttpContext.Current.Response.End();
        }
    }
}
