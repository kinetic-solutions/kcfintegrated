﻿using System;
using System.Security.Cryptography;
using System.Text;
using Kx.Core.PaymentProcessor.Cache;
using Kx.Core.PaymentProcessor.Payments;
using System.Collections.Specialized;
using System.Web;
using Kx.Core.Data;

namespace Kx.Core.PaymentProcessor.Providers
{
    public class BarclaycardSmartPay : BasePaymentProvider
    {
        public static void ToForm(NameValueCollection Inputs, PaymentRequest request)
        {
            var hmacKey = string.Empty;
            var sessionValidity = DateTime.UtcNow.AddHours(2).ToString("s") +"Z";

            //DO NOT REMOVE THE BLANKS!!!!!
            hmacKey += (request.Amount * 100m).ToString("F0");
            hmacKey += "GBP";
            hmacKey += request.CustomValue1;
            hmacKey += String.Format("ACCOM_{0}_{1}_{2}", request.StudentId, request.Amount, request.CustomValue3);  //merchantReference
            hmacKey += request.CustomValue2;
            hmacKey += request.ProviderAccountId;
            hmacKey += sessionValidity;
            hmacKey += HttpContext.Current.Session.SessionID + "_" + request.BookingReference.ToString() + "_" + (request.Amount).ToString(); //merchantReturnData
            hmacKey += "";                                                                                                                    //allowedMethods
            hmacKey += "";                                                                                                                    //blockedMethods
            hmacKey += "";                                                                                                                    //shopperStatement
            hmacKey += "";                                                                                                                    //billingAddressType

            hmacKey = GetHmac(hmacKey, request.ProviderPassword);

            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.Write("<html><head>");
            HttpContext.Current.Response.Write(string.Format("</head><body onload=\"document.{0}.submit()\">", "barclayCardSmartPay"));
            HttpContext.Current.Response.Write(string.Format("<form name=\"{0}\" id=\"{0}\" method=\"{1}\" action=\"{2}\" >", "barclayCardSmartPay", "POST", request.ProviderUrl));
            HttpContext.Current.Response.Write(string.Format("<input name=\"{0}\" type=\"hidden\" value=\"{1}\"/>", "merchantReference", String.Format("ACCOM_{0}_{1}_{2}", request.StudentId, request.Amount, request.CustomValue3)));
            HttpContext.Current.Response.Write(string.Format("<input name=\"{0}\" type=\"hidden\" value=\"{1}\"/>", "paymentAmount", (request.Amount * 100m).ToString("F0")));
            HttpContext.Current.Response.Write(string.Format("<input name=\"{0}\" type=\"hidden\" value=\"{1}\"/>", "currencyCode", "GBP"));
            HttpContext.Current.Response.Write(string.Format("<input name=\"{0}\" type=\"hidden\" value=\"{1}\"/>", "shipBeforeDate", request.CustomValue1));
            HttpContext.Current.Response.Write(string.Format("<input name=\"{0}\" type=\"hidden\" value=\"{1}\"/>", "skinCode", request.CustomValue2));
            HttpContext.Current.Response.Write(string.Format("<input name=\"{0}\" type=\"hidden\" value=\"{1}\"/>", "merchantAccount", request.ProviderAccountId));
            HttpContext.Current.Response.Write(string.Format("<input name=\"{0}\" type=\"hidden\" value=\"{1}\"/>", "shopperLocale", "en_GB"));
            HttpContext.Current.Response.Write(string.Format("<input name=\"{0}\" type=\"hidden\" value=\"{1}\"/>", "sessionValidity", sessionValidity));
            HttpContext.Current.Response.Write(string.Format("<input name=\"{0}\" type=\"hidden\" value=\"{1}\"/>", "merchantSig", hmacKey));
            HttpContext.Current.Response.Write(string.Format("<input name=\"{0}\" type=\"hidden\" value=\"{1}\"/>", "merchantReturnData", HttpContext.Current.Session.SessionID + "_" + request.BookingReference.ToString() + "_" + (request.Amount).ToString()));

            HttpContext.Current.Response.Write("</form>");
            HttpContext.Current.Response.Write("</body></html>");
            HttpContext.Current.Response.End();
        }

        public static PaymentResponse FromResponse(HttpContext context, PaymentCache cache)
        {      
            string[] strMerchantReturnData = context.Request.QueryString["merchantReturnData"].ToString().Split('_');
            var response = new PaymentResponse();

            response.Authorised = (context.Request.QueryString["authResult"] == "AUTHORISED") ? true : false;
            response.ProviderReference = context.Request.QueryString["pspReference"];
            response.InternalReference = strMerchantReturnData[1];
            response.Amount = decimal.Parse(strMerchantReturnData[2]);
            response.OriginalRequest = cache.GetPaymentSafe(response);

            return response;
        }

        private static string GetHmac(string data, string key)
        {
            KxData data1 = new KxData();
            data1.ExecuteAuditLog("BarclaycardSmartPay", "GetHmac", data);

            var encoding = new ASCIIEncoding();

            var keyByte = encoding.GetBytes(key);

            var hmacsha1 = new HMACSHA1(keyByte);

            var messageBytes = encoding.GetBytes(data);
            var hashmessage = hmacsha1.ComputeHash(messageBytes);

            return Convert.ToBase64String(hashmessage);
        }
    }
}