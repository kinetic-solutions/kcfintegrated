﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;
using System.Collections.Specialized;
using System.Web;
using Kx.Core.Configuration;
using Kx.Core.Exceptions;
using Kx.Core.PaymentProcessor.Payments;
using Kx.Core.PaymentProcessor.Cache;

namespace Kx.Core.PaymentProcessor.Providers
{
    public class CaledonProvider : BasePaymentProvider
    {
        private const string STR_CALEDON_TEMP_LANG = "CALEDON_TEMP_LANG";

        public static void ToForm(NameValueCollection Inputs, PaymentRequest request, HttpContext context)
        {
            string lang = context.Request["lang"] ?? string.Empty;

            if (!string.IsNullOrWhiteSpace(lang))
            {
                context.Session[STR_CALEDON_TEMP_LANG] = lang;
            }

            string url = request.ProviderCallbackUrl;
            Inputs.Add("terminal_id", request.ProviderAccountId);
            Inputs.Add("reference_number", request.BookingReference);
            Inputs.Add("return_url", HttpUtility.UrlEncode(url));

            if (KxDbConfiguration.GetConfiguration("Profiler", "off").ToLower() == "on")
            {
                KxLogger.LogActivity("CaledonProvider", "ToForm", "ProviderCallbackUrlPassed", string.Format("CacheID: {0}, CallbackUrl: {1}", request.BookingReference, HttpUtility.UrlEncode(url)));
            }
            Inputs.Add("amount", HttpUtility.UrlEncode(request.Amount.ToString("F")));

            if (!string.IsNullOrEmpty(lang))
            {
                if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings[string.Format("CaledonLocale{0}", lang)]))
                {
                    Inputs.Add("locale", ConfigurationManager.AppSettings[string.Format("CaledonLocale{0}", lang)]);
                }
            }

            StringBuilder queryString = new StringBuilder();
            foreach (string key in Inputs)
            {
                queryString.AppendFormat("{0}{1}={2}", queryString.Length == 0 ? "?" : "&", key, Inputs[key]);
            }

            string redirectUrl = request.ProviderUrl + queryString.ToString();

            KxLogger.LogActivity("CaledonProvider", "ToForm", "Outbound payment GET", redirectUrl);

            context.Response.Redirect(redirectUrl);
        }

        public static PaymentResponse FromResponse(HttpContext httpContext, PaymentCache cache)
        {
            string lang = (string)httpContext.Session[STR_CALEDON_TEMP_LANG];
            if (!string.IsNullOrWhiteSpace(lang))
            {
                httpContext.Session.Remove(STR_CALEDON_TEMP_LANG);
                httpContext.Response.Redirect(string.Format("{0}&lang={1}", httpContext.Request.Url.AbsoluteUri, lang));
                return null;
            }
            var response = new PaymentResponse();
            response.InternalReference = GetObjectValue(httpContext.Request["reference_number"]);
            string authResult = GetObjectValue(httpContext.Request["auth_result"]).ToLower();
            response.Authorised = authResult == "a";
            if (authResult != "c")
            {
                response.ProviderReference = GetObjectValue(httpContext.Request["result_text"]);
            }
            if (response.Authorised)
            {
                response.Amount = GetObjectValueDecimal(httpContext.Request["amount"]);
            }

            KxLogger.LogActivity("CaledonProvider", "FromResponse", "Payment response received", string.Format("CacheID: {0}; Auth_Result:{1}; Result_Text:{2}", response.InternalReference, authResult, response.ProviderReference));
            response.OriginalRequest = cache.GetPayment(response);
            response.TransactionDate = DateTime.Now;

            return response;
        }

    }
}
