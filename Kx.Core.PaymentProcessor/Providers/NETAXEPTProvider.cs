﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.Specialized;
using Kx.Core.PaymentProcessor.Payments;
using System.Web;
using System.Net;
using System.Xml;
using Kx.Core.Exceptions;
using Kx.Core.Configuration;

namespace Kx.Core.PaymentProcessor.Providers
{
    internal class Buffer<T> : Queue<T>
    {
        private int? maxCapacity { get; set; }

        public Buffer() { maxCapacity = null; }
        public Buffer(int capacity) { maxCapacity = capacity; }

        public void Add(T newElement)
        {
            if (this.Count == (maxCapacity ?? -1)) this.Dequeue(); // no limit if maxCapacity = null
            this.Enqueue(newElement);
        }
    }

    public class NETAXEPTProvider : BasePaymentProvider
    {
        private static Buffer<string> TransactionIDs = new Buffer<string>(250);

        public NETAXEPTProvider()
        {
        }
        #region Attributes & Properties

        private static string RESTURL
        {
            get
            {
                string url = KxDbConfiguration.GetConfiguration("NETAXEPT.REST.URL");
                if (url[url.Length - 1] != '/')
                {
                    url += "/";
                }
                return url;
            }
        }

        private static string Token
        {
            get { return KxDbConfiguration.GetConfiguration("NETAXEPT.Token"); }
        }

        private static string MerchantID
        {
            get { return KxDbConfiguration.GetConfiguration("NETAXEPT.MerchantID"); }
        }

        #endregion

        public static void ToForm(NameValueCollection Inputs, PaymentRequest request)
        {
            string transactionId = RegisterTransaction(request);

            HttpContext.Current.Session.Remove("ErrorText");

            KxLogger.LogActivity("NETAXEPTProvider", "ToForm", "TRANSACTIONID:", transactionId);

            if (!string.IsNullOrEmpty(transactionId))
            {

                HttpContext.Current.Response.Redirect(string.Format("{0}?merchantId={1}&transactionId={2}",
                                                                    request.ProviderUrl, MerchantID,
                                                                    transactionId));
            }
            else
            {
                KxLogger.LogActivity("NETAXEPTProvider", "ToForm", "TRANSACTIONID is empty", "CacheID = " + request.BookingReference);
                throw new Exception("TransactionID is empty");
            }
        }

        private static string RegisterTransaction(PaymentRequest request)
        {
            string postData = string.Format("merchantId={0}", MerchantID);
            //KxLogger.LogActivity("NETAXEPTProvider", "RegisterTransaction", "MerchantId", "");

            postData += string.Format("&token={0}", Token);
            //KxLogger.LogActivity("NETAXEPTProvider", "RegisterTransaction", "Token", "");

            postData += string.Format("&transactionId={0}", request.BookingReference);
            //KxLogger.LogActivity("NETAXEPTProvider", "RegisterTransaction", "TransId", "");

            postData += string.Format("&orderNumber={0}", request.BookingReference);
            //KxLogger.LogActivity("NETAXEPTProvider", "RegisterTransaction", "BookingRef", "");

            postData += string.Format("&orderDescription={0}", request.Description);
            //KxLogger.LogActivity("NETAXEPTProvider", "RegisterTransaction", "Description", "");

            postData += string.Format("&amount={0:######}", request.Amount * 100);
            postData += string.Format("&currencyCode={0}", request.Currency);

            postData += string.Format("&redirectUrl={0}", request.ProviderCallbackUrl);
            //KxLogger.LogActivity("NETAXEPTProvider", "RegisterTransaction", "Description", "");

            postData += "&serviceType=B";
            //KxLogger.LogActivity("NETAXEPTProvider", "RegisterTransaction", "ServiceType", "");

            postData += string.Format("&customerNumber={0}", request.StudentId);
            //KxLogger.LogActivity("NETAXEPTProvider", "RegisterTransaction", "StudentId", "");

            postData += string.Format("&customerEmail={0}", request.Email);
            //KxLogger.LogActivity("NETAXEPTProvider", "RegisterTransaction", "Email", "");

            postData += string.Format("&customerPhoneNumber={0}", request.Telephone);
            //KxLogger.LogActivity("NETAXEPTProvider", "RegisterTransaction", "Telephone", "");

            postData += string.Format("&customerFirstName={0}", request.FirstName);
            //KxLogger.LogActivity("NETAXEPTProvider", "RegisterTransaction", "FirstName", "");

            postData += string.Format("&customerLastName={0}", request.Surname);
            //KxLogger.LogActivity("NETAXEPTProvider", "RegisterTransaction", "FirstName", "");

            if (KxDbConfiguration.GetConfiguration("Profiler").ToLower() == "on")
            {
                KxLogger.LogActivity("NETAXEPTProvider", "ToForm-RegisterTransaction",
                    "REGISTRATION START - Log - PostData", postData);
            }

            try
            {
                var xmlResponse = CallRESTService(postData, "Register.aspx");
                if (KxDbConfiguration.GetConfiguration("Profiler").ToLower() == "on")
                {
                    KxLogger.LogActivity("NETAXEPTProvider", "ToForm-RegisterTransaction", "REGISTRATION RESPONSE",
                        xmlResponse);
                }

                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xmlResponse);
                XmlNode node = doc.SelectSingleNode(@"//RegisterResponse/TransactionId");
                return node.InnerText;
            }
            catch (Exception e)
            {
                KxLogger.LogActivity("NETAXEPTProvider", "ToForm-RegisterTransaction", "REGISTRATION EXCEPTION:" + e.Message, "DATAPOSTED:" + postData);
                throw;
            }
        }

        private static string CallRESTService(string postData, string page)
        {

            if (KxDbConfiguration.GetConfiguration("Profiler").ToLower() == "on")
            {
                KxLogger.LogActivity("NETAXEPTProvider", "CallRESTService", RESTURL + page, postData);
            }

            string xmlResponse;

            using (WebClient wc = new WebClient())
            {
                ServicePointManager.SecurityProtocol =  SecurityProtocolType.Tls12 | SecurityProtocolType.Ssl3;
                wc.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
                xmlResponse = wc.UploadString(RESTURL + page, postData);
            }

            if (KxDbConfiguration.GetConfiguration("Profiler").ToLower() == "on")
            {
                KxLogger.LogActivity("NETAXEPTProvider", "CallRESTService", "Response", xmlResponse);
            }

            return xmlResponse;
        }

        public static PaymentResponse FromResponse(HttpContext httpContext, Cache.PaymentCache cache)
        {
            PaymentResponse response = new PaymentResponse();
            response.Authorised = false;

            string transactionId = httpContext.Request.QueryString["transactionId"];
            string responseCode = httpContext.Request.QueryString["responseCode"];

            if (KxDbConfiguration.GetConfiguration("Profiler").ToLower() == "on")
            {
                KxLogger.LogActivity("NETAXEPTProvider", "FromResponse",
                    "Im attempting to process transaction: " + transactionId, "");
            }


            if (!string.IsNullOrEmpty(transactionId) && !NETAXEPTProvider.TransactionIDs.Contains(transactionId))
            {
                try
                {
                    TransactionIDs.Add(transactionId);
                }
                catch
                { }

                decimal amountCaptured = 0.00m;
                string cacheId = string.Empty;

                try
                {
                    QueryTransaction(transactionId, out amountCaptured, out cacheId);
                }
                catch (Exception e)
                {
                    KxLogger.LogActivity("NETAXEPTProvider", "FromResponse", "ERROR for TransactionId during first Query: " + transactionId, e.Message);
                    amountCaptured = 0.00m;
                }

                response.Authorised = false;
                response.InternalReference = cacheId;

                if (amountCaptured != 0.00m)
                {
                    response.Amount = amountCaptured / 100m;

                    if (responseCode.ToLower() == "ok")
                    {

                        string authCode = AuthSale(transactionId); // TODO: This is being called twice instead of once and causes an Exception from the provider.
                        if (!string.IsNullOrEmpty(authCode))
                        {
                            response.Authorised = true;
                            response.ProviderReference = authCode;
                            response.TransactionDate = DateTime.Now;
                        }
                    }
                    else
                    {
                        KxLogger.LogActivity("NETAXEPTProvider", "FromResponse", "Response Code NOT OK", responseCode);
                    }

                }
                else
                {
                    KxLogger.LogActivity("NETAXEPTProvider", "FromResponse", "Amount paid has been returned as 0", "");
                }
            }
            else
            {
                if (string.IsNullOrEmpty(transactionId))
                {
                    KxLogger.LogActivity("NETAXEPTProvider", "FromResponse", "No transaction ID received", string.Empty);
                }

                response.InternalReference = transactionId;

                if (responseCode.ToLower() != "ok")
                {
                    HttpContext.Current.Session["ErrorText"] = responseCode;
                }
            }
            response.OriginalRequest = cache.GetPayment(response);

            return response;
        }

        private static string AuthSale(string transactionId)
        {
            try
            {

                string postData = string.Format("merchantId={0}&token={1}&transactionId={2}&operation=SALE", MerchantID, Token, transactionId);
                string response = CallRESTService(postData, "Process.aspx");
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(response);



                XmlNode responseCode = doc.SelectSingleNode(@"//ProcessResponse/ResponseCode");

                if (responseCode == null)
                {
                    KxLogger.LogActivity("NETAXEPTProvider", "AuthSale", "ERROR for transaction Id: " + transactionId, response);
                    XmlNode errorResponse = doc.SelectSingleNode(@"//Exception/Error/Result/ResponseText");

                    HttpContext.Current.Session["ErrorText"] = errorResponse.InnerText.ToUpper();
                    return string.Empty;
                }

                if (responseCode.InnerText.ToLower() == "ok")
                {
                    XmlNode authCode = doc.SelectSingleNode(@"//ProcessResponse/AuthorizationId");
                    return authCode.InnerText;
                }
            }
            catch (Exception e)
            {

                KxLogger.LogActivity("NETAXEPTProvider", "AuthSale", "ERROR for transaction Id: " + transactionId, e.Message);
            }

            return string.Empty;
        }

        private static void QueryTransaction(string transactionId, out decimal amount, out string orderNumber)
        {
            string temp = string.Format("merchantId={0}&token={1}&transactionId={2}", MerchantID, Token, transactionId);
            string postData = temp;
            string response = CallRESTService(postData, "Query.aspx");

            KxLogger.LogActivity("NETAXEPTProvider", "QueryTransaction", "RESPONSE", response);

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(response);

            XmlNode amountPaid = doc.SelectSingleNode(@"//OrderInformation/Total");
            if (amountPaid != null)
            {
                amount = Convert.ToDecimal(amountPaid.InnerText);
            }
            else
            {
                amount = 0m;
            }

            XmlNode node = doc.SelectSingleNode(@"//PaymentInfo/OrderInformation/OrderNumber");
            orderNumber = node.InnerText;
        }

        private static decimal QuerySale(string transactionId)
        {
            string temp = string.Format("merchantId={0}&token={1}&transactionId={2}", MerchantID, Token, transactionId);
            string postData = temp;
            string response = CallRESTService(postData, "Query.aspx");
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(response);

            XmlNode amountPaid = doc.SelectSingleNode(@"//OrderInformation/Total");
            if (amountPaid != null)
            {
                return Convert.ToDecimal(amountPaid.InnerText);

            }

            return 0.00m;
        }

        private static string QueryOrderNumber(string transactionId)
        {
            string postData = string.Format("merchantId={0}&token={1}&transactionId={2}", MerchantID, Token, transactionId);
            string response = CallRESTService(postData, "Query.aspx");

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(response);
            XmlNode node = doc.SelectSingleNode(@"//PaymentInfo/OrderInformation/OrderNumber");
            return node.InnerText;
        }


    }
}
