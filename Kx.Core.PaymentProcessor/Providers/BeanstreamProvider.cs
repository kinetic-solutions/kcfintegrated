﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Web;
using Kx.Core.PaymentProcessor.Cache;
using Kx.Core.PaymentProcessor.Payments;

namespace Kx.Core.PaymentProcessor.Providers
{
    public class BeanstreamProvider : BasePaymentProvider
    {

        public static void ToForm(NameValueCollection Inputs, PaymentRequest request)
        {
            Inputs.Add("ref1", request.BookingReference);
            Inputs.Add("trnAmount", request.Amount.ToString("F"));
            Inputs.Add("approvedPage", request.ProviderCallbackUrl);
            Inputs.Add("declinedPage", request.ProviderCallbackUrl);
            Inputs.Add("errorPage", request.ProviderCallbackUrl);
            Inputs.Add("merchant_id", request.ProviderAccountId);
            Inputs.Add("trnCardOwner", request.FirstName + " " + request.Surname);
            Inputs.Add("ordName", request.FirstName + " " + request.Surname);
            Inputs.Add("ordAddress1", request.Address1);
            Inputs.Add("ordAddress2", request.Address2);
            Inputs.Add("ordCity", request.Town);
            Inputs.Add("ordPostalCode", request.Postcode);
            Inputs.Add("ordPhoneNumber", request.Telephone);
            Inputs.Add("ordEmailAddress", request.Email);
        }

        public static PaymentResponse FromResponse(PaymentCache cache)
        {
            PaymentResponse response = new PaymentResponse();

            string temp = GetObjectValue(HttpContext.Current.Request["ref1"]);

            Guid gud = new Guid(temp);

            response.InternalReference = gud.ToString();

            response.ProviderReference = GetObjectValue(HttpContext.Current.Request["trnId"]);
            response.Amount = GetObjectValueDecimal(HttpContext.Current.Request["trnAmount"]);
            response.Authorised = GetObjectValue(HttpContext.Current.Request["trnApproved"]).ToUpper() == "1" ? true : false;

            response.OriginalRequest = cache.GetPayment(response);

            response.TransactionDate = DateTime.Now;

            return response;
        }

    }
}
