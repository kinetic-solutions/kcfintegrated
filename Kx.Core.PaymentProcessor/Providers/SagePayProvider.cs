﻿using System;
using System.Collections.Specialized;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using Kx.Core.PaymentProcessor.Cache;
using Kx.Core.PaymentProcessor.Payments;
using Kx.Core.Utility;
using Kx.Core.Exceptions;

namespace Kx.Core.PaymentProcessor.Providers
{
	/// <summary>
	/// 
	/// </summary>
	public class SagePayProvider : BasePaymentProvider
	{
		#region Private Objects

		private static String vendorName = String.Empty;
		private static String encryptionPassword = String.Empty;
		private static String currency = "GBP";
		private static String vendorEmail = String.Empty;
		private static String transactionType = "PAYMENT";
		private static String partnerId = String.Empty;
		private static Int16 sendEmail = 1; /* 0 = no email, 1 = send customer and vendor email, 2 send vendor email only */
		private static String encryptionType = "AES";
		private static String protocolVersion = "2.23";

		#endregion

		/// <summary>
		/// 
		/// </summary>
		/// <param name="Inputs"></param>
		/// <param name="request"></param>
		public static void ToForm(NameValueCollection Inputs, PaymentRequest request)
		{
			vendorName = request.MerchantDisplayName;
			vendorEmail = request.Email;
			encryptionPassword = request.ProviderPassword;
			currency = request.Currency;


            Inputs.Add("VPSProtocol",protocolVersion);
            Inputs.Add("TxType",transactionType);
            Inputs.Add("Vendor",request.ProviderAccountId);
            Inputs.Add("Crypt",BuildCryptField(request));
            /*

			HttpContext.Current.Response.Clear();
			HttpContext.Current.Response.Write("<html><head>");
			HttpContext.Current.Response.Write(String.Format("</head><body onload=\"document.{0}.submit()\">", "form1"));
            //HttpContext.Current.Response.Write(String.Format("</head><body>"));
			HttpContext.Current.Response.Write(String.Format("<form name=\"{0}\" method=\"{1}\" action=\"{2}\" >", "form1", "POST", request.ProviderUrl));

			HttpContext.Current.Response.Write(String.Format("<input type=\"hidden\" name=\"VPSProtocol\" value=\"{0}\" />", protocolVersion));
			HttpContext.Current.Response.Write(String.Format("<input type=\"hidden\" name=\"TxType\" value=\"{0}\" />", transactionType));
			HttpContext.Current.Response.Write(String.Format("<input type=\"hidden\" name=\"Vendor\" value=\"{0}\" />", request.ProviderAccountId));
			HttpContext.Current.Response.Write(String.Format("<input type=\"hidden\" name=\"Crypt\" value=\"{0}\" />", BuildCryptField(request)));

			HttpContext.Current.Response.Write("<p>You will be now redirected to SagePay for processing of your payment, if you have ");
			HttpContext.Current.Response.Write("not redirected within the next 10 seconds, please click on the redirect button</p>");
			HttpContext.Current.Response.Write("<INPUT type=\"submit\" value=\"Redirect\" ID=\"Submit1\" NAME=\"Submit1\" />");
			HttpContext.Current.Response.Write("</form>");
			HttpContext.Current.Response.Write("</body></html>");
			HttpContext.Current.Response.End();*/
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="context"></param>
		/// <param name="cache"></param>
		/// <returns></returns>
		public static PaymentResponse FromResponse(HttpContext context, PaymentCache cache)
		{
			PaymentResponse response = new PaymentResponse();

            #region Check Crypt
            if (String.IsNullOrEmpty(context.Request.QueryString["Crypt"]))
			{
				response.Authorised = false;
				response.AuthorisationMessage = "Crypt message returned was invalid!";
                KxLogger.LogActivity("SagePay Payment Response", "Inbound Post Error", "Crypt message returned was invalid!", "Unable to process"); 
			}

			if (context.Request.QueryString["Crypt"].Substring(0, 1) != "@")
			{
				response.Authorised = false;
				response.AuthorisationMessage = "Crypt message returned was not AES encrypted!";
                KxLogger.LogActivity("SagePay Payment Response", "Inbound Post Error", "Crypt message returned was not AES encrypted!", "Unable to decrypt - Unable to process"); 
			}
            #endregion

			String encryptedResponse = context.Request.QueryString["Crypt"].Substring(1);
			String decryptedResponse = aesDecrypt(HexStringToByteArray(encryptedResponse));
            response.InternalReference = GetToken(decryptedResponse, "VendorTxCode");
            response.ProviderReference = GetToken(decryptedResponse, "VPSTxId");
            

            switch (GetToken(decryptedResponse, "Status"))
			{
				case "OK":
					response.Authorised = true;
					response.AuthorisationMessage = GetToken(decryptedResponse, "StatusDetail");
                    response.ProviderReference2 = GetToken(decryptedResponse, "TxAuthNo");
                    response.Amount = GetObjectValueDecimal(GetToken(decryptedResponse, "Amount"));
                    response.Currency = GetToken(decryptedResponse, "Currency");
					break;
				default:
					response.Authorised = false;
					response.AuthorisationMessage = GetToken(decryptedResponse, "StatusDetail");
                    response.Amount = 0.0M;
                    KxLogger.LogActivity("SagePay Payment Response", "Response Processed", GetToken(decryptedResponse, "StatusDetail"), "WebPaymentCacheId: " + response.InternalReference); 
					break;
			}

            response.InternalReference = GetToken(decryptedResponse, "VendorTxCode");
            response.OriginalRequest = cache.GetPayment(response);
            
            

			return response;
		}

		#region Private Methods

		private static String BuildCryptField(PaymentRequest request)
		{
			StringBuilder cryptField = new StringBuilder();

			cryptField.Append("VendorTxCode=");
			cryptField.Append(request.BookingReference);

            cryptField.Append("&VendorEmail=");
			cryptField.Append(request.ClientEmail);

			cryptField.Append("&Amount=");
			cryptField.Append(request.Amount);

			cryptField.Append("&Currency=");
			cryptField.Append(currency);

			cryptField.Append("&Description=");
			cryptField.Append(request.Description);

			cryptField.Append("&SuccessURL=");
			cryptField.Append(request.ConfirmationPageUrl);

			cryptField.Append("&FailureURL=");
			cryptField.Append(request.ConfirmationPageUrl);

            cryptField.Append("&CustomerName=");
            cryptField.Append(request.FullName);

            cryptField.Append("&CustomerEmail=");
            cryptField.Append(request.Email);

			cryptField.Append("&BillingSurname=");
			cryptField.Append(request.Surname);

			cryptField.Append("&BillingFirstnames=");
			cryptField.Append(request.FirstName);

			cryptField.Append("&BillingAddress1=");
			cryptField.Append(request.Address1);

			cryptField.Append("&BillingAddress2=");
			cryptField.Append(request.Address2);

			cryptField.Append("&BillingCity=");
			cryptField.Append(request.Town);

			cryptField.Append("&BillingPostCode=");
			cryptField.Append(request.Postcode);

			cryptField.Append("&BillingCountry=");
			cryptField.Append(request.Country);

			cryptField.Append("&DeliverySurname=");
			cryptField.Append(request.Surname);

			cryptField.Append("&DeliveryFirstnames=");
			cryptField.Append(request.FirstName);

			cryptField.Append("&DeliveryAddress1=");
			cryptField.Append(request.Address1);

			cryptField.Append("&DeliveryAddress2=");
			cryptField.Append(request.Address2);

			cryptField.Append("&DeliveryCity=");
			cryptField.Append(request.Town);

			cryptField.Append("&DeliveryPostCode=");
			cryptField.Append(request.Postcode);

			cryptField.Append("&DeliveryCountry=");
			cryptField.Append(request.Country);

			return String.Format("@{0}", ByteArrayToHexString(aesEncrypt(cryptField.ToString())));
		}

		private static String ReadCryptField(String cryptData)
		{
			return "";
		}

		private static Byte[] aesEncrypt(String inputText)
		{
			RijndaelManaged AES = new RijndaelManaged();

			// set the mode, padding and block size for the key
			AES.Padding = PaddingMode.PKCS7;
			AES.Mode = CipherMode.CBC;
			AES.KeySize = 128;
			AES.BlockSize = 128;

			// convert key and plain text input into byte arrays
			Byte[] passwordBytes = System.Text.Encoding.UTF8.GetBytes(encryptionPassword);
			Byte[] inputBytes = System.Text.Encoding.UTF8.GetBytes(inputText);

			// create streams and encryptor object
			MemoryStream memoryStream = new MemoryStream();
			CryptoStream cryptoStream = new CryptoStream(memoryStream, AES.CreateEncryptor(passwordBytes, passwordBytes), CryptoStreamMode.Write);

			// perform encryption
			cryptoStream.Write(inputBytes, 0, inputBytes.Length);
			cryptoStream.FlushFinalBlock();

			// get encrypted stream into byte array
			Byte[] outBytes = memoryStream.ToArray();

			// close streams
			memoryStream.Close();
			cryptoStream.Close();
			AES.Clear();

			return outBytes;
		}

		private static String aesDecrypt(Byte[] inputBytes)
		{
			RijndaelManaged AES = new RijndaelManaged();
			Byte[] passwordBytes = System.Text.Encoding.UTF8.GetBytes(encryptionPassword);
			Byte[] outputBytes = new Byte[inputBytes.Length];

			// set the mode, padding and block size
			AES.Padding = PaddingMode.PKCS7;
			AES.Mode = CipherMode.CBC;
			AES.KeySize = 128;
			AES.BlockSize = 128;

			// create streams and decryptor object
			MemoryStream memoryStream = new MemoryStream(inputBytes);
			CryptoStream cryptoStream = new CryptoStream(memoryStream, AES.CreateDecryptor(passwordBytes, passwordBytes), CryptoStreamMode.Read);

			// perform decryption
			cryptoStream.Read(outputBytes, 0, outputBytes.Length);

			// close streams
			memoryStream.Close();
			cryptoStream.Close();
			AES.Clear();

			// convert decryted data into string, assuming original text was UTF-8 encoded
			return System.Text.Encoding.UTF8.GetString(outputBytes);
		}

		private static String ByteArrayToHexString(Byte[] bytes)
		{
			return BitConverter.ToString(bytes).Replace("-", "");
		}

		private static Byte[] HexStringToByteArray(String hexString)
		{
			Int32 numBytes = hexString.Length / 2;
			Byte[] bytes = new Byte[numBytes];

			for (Int32 i = 0; i < numBytes; i++)
			{
				bytes[i] = System.Convert.ToByte(hexString.Substring(i * 2, 2), 16);
			}

			return bytes;
		}

		private static String GetToken(String strList, String strRequired)
		{
			String[] tokens = strList.Split('&');

			foreach (String token in tokens)
			{
				String[] tokenDetails = token.Replace("&", "").Split('=');

				String tokenName = tokenDetails[0];
				String tokenData = tokenDetails[1];

				if (tokenName.ToUpper() == strRequired.ToUpper())
				{
					return tokenData;
				}
			}

			return String.Empty;
		}

		#endregion
	}
}