using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;
using System.Net;
using System.Web;
using Kx.Core.PaymentProcessor.Payments;
using Kx.Core.PaymentProcessor.Cache;
using System.Security.Cryptography;
using System.Web.Configuration;

namespace Kx.Core.PaymentProcessor.Providers
{
    public class SecureTradingProvider : BasePaymentProvider
    {
        public SecureTradingProvider()
        {
        }

        private const string STR_SECURETRADING_TEMP_LANG = "SECURETRADING_TEMP_LANG";
        public static void ToForm(NameValueCollection Inputs, PaymentRequest request, HttpContext context)
        {

            string lang = context.Request["lang"] ?? string.Empty;

            if (!string.IsNullOrWhiteSpace(lang))
            {
                context.Session[STR_SECURETRADING_TEMP_LANG] = lang;
            }

            Inputs.Add("sitereference", request.ProviderAccountId);
            Inputs.Add("kxcartid", request.BookingReference);
            Inputs.Add("mainamount", request.Amount.ToString("F"));
            Inputs.Add("returnurl", request.ProviderCallbackUrl);
            Inputs.Add("currencyiso3a", "GBP");
            Inputs.Add("orderreference", request.Description);


            Inputs.Add("billingfirstname", request.FirstName);
            Inputs.Add("billinglastname", request.Surname);
            Inputs.Add("billingprefixname", request.Title);
            Inputs.Add("billingpremise", request.Address1);
            Inputs.Add("billingstreet", request.Address2);
            Inputs.Add("billingtown", request.Town);
            Inputs.Add("billingcounty", request.County);
            Inputs.Add("billingpostcode", request.Postcode);
            Inputs.Add("billingtelephone", request.Telephone);
            Inputs.Add("billingtelephonetype", "W");
            Inputs.Add("billingemail", request.Email);
            Inputs.Add("version", "1");

            Inputs.Add("callback", request.ProviderCallbackUrl);
        }

        public static PaymentResponse FromResponse(HttpContext httpContext, PaymentCache cache)
        {
            string lang = (string)httpContext.Session[STR_SECURETRADING_TEMP_LANG];
            if (!string.IsNullOrWhiteSpace(lang))
            {
                httpContext.Session.Remove(STR_SECURETRADING_TEMP_LANG);
                /* Do some magic to re-post the data back into KCF with the lang suffixed if we've gone out with a lang */
                httpContext.Response.Clear();

                StringBuilder sb = new StringBuilder();
                sb.Append("<html>");
                sb.AppendFormat(@"<body onload='document.forms[""form""].submit()'>");
                sb.AppendFormat("<form name='form' action='{0}' method='post'>", string.Format("{0}{2}lang={1}", httpContext.Request.Url.AbsoluteUri, lang, httpContext.Request.Url.AbsoluteUri.Contains("?") ? "&" : "?").Replace("kxcartid", "oldkxcartid"));
                sb.AppendFormat("<input type='hidden' name='kxcartid' value='{0}'>", GetObjectValue(httpContext.Request["kxcartid"]));
                try
                {
                    sb.AppendFormat("<input type='hidden' name='mainamount' value='{0}'>", GetObjectValueDecimal(httpContext.Request["mainamount"]));
                }
                catch
                {
                }
                try
                {
                    sb.AppendFormat("<input type='hidden' name='authcode' value='{0}'>",
                        GetObjectValue(httpContext.Request["authcode"]));
                }
                catch
                {
                }

                // Other params go here
                sb.Append("</form>");
                sb.Append("</body>");
                sb.Append("</html>");

                httpContext.Response.Write(sb.ToString());
                httpContext.Response.End();
                return null;
            }
            PaymentResponse response = new PaymentResponse();
            response.InternalReference = GetObjectValue(HttpContext.Current.Request["kxcartid"]);
            response.Authorised = !string.IsNullOrWhiteSpace(GetObjectValue(HttpContext.Current.Request["authcode"]));

            try
            {
                response.ProviderReference = GetObjectValue(HttpContext.Current.Request["authcode"]) + "-" + response.InternalReference;
            }
            catch
            {
                response.ProviderReference = GetObjectValue(HttpContext.Current.Request["authcode"]) + "-" + new Random(DateTime.Now.Millisecond).Next(10000, 99000).ToString();
            }

            try
            {
                response.Amount = GetObjectValueDecimal(HttpContext.Current.Request["mainamount"]);
            }
            catch
            { }

            response.OriginalRequest = cache.GetPayment(response);

            response.TransactionDate = DateTime.Now;

            return response;
        }

    }
}
