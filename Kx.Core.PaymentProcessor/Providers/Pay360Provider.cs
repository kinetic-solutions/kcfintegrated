﻿using System;
using System.Collections.Specialized;
using System.Text;
using System.Web;
using Kx.Core.Exceptions;
using Kx.Core.PaymentProcessor.Payments;
using Kx.Core.PaymentProcessor.Cache;
using System.Xml.Linq;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Xml;
using Kx.Core.PaymentProcessor.Helpers;
using Kx.Core.Configuration;
using Kx.Core.Data;
using System.Linq;
using System.Security.Cryptography;
using System.Collections.Generic;

namespace Kx.Core.PaymentProcessor.Providers
{
    public class Pay360Provider : BasePaymentProvider
    {
        const string SubjectType = "CapitaPortal";
        const string SystemCode = "SCP";
        const string Algorithm = "Original";

        public Pay360Provider()
        {
        }

        public static void ToForm(NameValueCollection Inputs, PaymentRequest request)
        {
            var URL = request.ProviderUrl;
            var SiteID = request.SiteId;
            var SCPID = request.SCPId;
            var HMACKeyID = request.HMACKeyId;
            var HMACKey = request.HMACKey;
            int amountInPence = (int)(request.Amount * 100); // amount in pence

            request.Identifier = SCPID;
            request.UniqueReference = Guid.NewGuid().ToString();
            request.Timestamp = GenerateTimestamp();

            //SecretKey = Concatenate subjectType, identifier, uniqueReference, timestamp, algorithm and hmacKeyID into a single string, with a '!'
            var secretKey = $"{SubjectType}!{request.Identifier}!{request.UniqueReference}!{request.Timestamp}!{Algorithm}!{request.HMACKeyId}";

            string xmlMessage = $@"<?xml version='1.0'?>
<SOAP-ENV:Envelope xmlns:SOAP-ENV='http://schemas.xmlsoap.org/soap/envelope/'>
                        <SOAP-ENV:Header/><SOAP-ENV:Body>
                <simple:scpSimpleInvokeRequest xmlns:simple='http://www.capita-software-services.com/scp/simple' xmlns:scpbase='http://www.capita-software-services.com/scp/base'>
                    <credentials xmlns='https://support.capita-software.co.uk/selfservice/?commonFoundation'>
                        <subject>
                            <subjectType>{SubjectType}</subjectType>
                            <identifier>{request.Identifier}</identifier>
                            <systemCode>{SystemCode}</systemCode>
                        </subject>
                        <requestIdentification>
                            <uniqueReference>{request.UniqueReference}</uniqueReference>
                            <timeStamp>{request.Timestamp}</timeStamp>
                        </requestIdentification>
                        <signature>
                            <algorithm>{Algorithm}</algorithm>
                            <hmacKeyID>{request.HMACKeyId}</hmacKeyID>
                            <digest>{CalculateDigest(HMACKey, secretKey)}</digest>
                        </signature>
                    </credentials>
                    <scpbase:requestType>payOnly</scpbase:requestType>
	                <scpbase:requestId>SR-123456</scpbase:requestId>
	                <scpbase:routing>
		                <scpbase:returnUrl>{request.ProviderCallbackUrl}?pay360_scp={request.BookingReference}</scpbase:returnUrl>
		                <scpbase:backUrl>{request.CancellationPageUrl}?pay360_scp_cancelled={request.BookingReference}</scpbase:backUrl>
		                <scpbase:siteId>{request.SiteId}</scpbase:siteId>
		                <scpbase:scpId>{request.SCPId}</scpbase:scpId>
	                </scpbase:routing>
	                <scpbase:panEntryMethod>ECOM</scpbase:panEntryMethod>
	                <simple:sale>
		                <scpbase:saleSummary>
			                <scpbase:description>{request.Description}</scpbase:description>
			                <scpbase:amountInMinorUnits>{amountInPence}</scpbase:amountInMinorUnits>
		                </scpbase:saleSummary>
		                <scpbase:lgSaleDetails>
			                <scpbase:userCode>KXS</scpbase:userCode>
                        </scpbase:lgSaleDetails>
		                <simple:items>
			                <simple:item>
				                <scpbase:itemSummary>
					                <scpbase:description>{request.Description}</scpbase:description>
					                <scpbase:amountInMinorUnits>{amountInPence}</scpbase:amountInMinorUnits>
					                <scpbase:reference>{request.StudentId}</scpbase:reference>
				                </scpbase:itemSummary>
				                <scpbase:lgItemDetails>
					                <scpbase:fundCode>{request.PaymentMethod}</scpbase:fundCode>
					                <scpbase:narrative>{request.PaymentType}</scpbase:narrative>
                                    <scpbase:additionalReference>{request.PaymentType}</scpbase:additionalReference>
                                    <scpbase:accountName>
                                        <scpbase:surname>{request.Surname}</scpbase:surname>
                                        <scpbase:forename>{request.FirstName}</scpbase:forename>
                                    </scpbase:accountName>
				                </scpbase:lgItemDetails>
				                <scpbase:customerInfo>
					                <scpbase:customerString5>{request.StudentId}</scpbase:customerString5>
				                </scpbase:customerInfo>
				                <scpbase:lineId>{request.StudentId}-{amountInPence}-{request.PaymentMethod}-{request.PaymentType}</scpbase:lineId>
			                </simple:item>
		                </simple:items>
	                </simple:sale>
                </simple:scpSimpleInvokeRequest>
            </SOAP-ENV:Body>
            </SOAP-ENV:Envelope>
";

            using (var client = new HttpClient())
            {
                var payLoad = new StringContent(xmlMessage, Encoding.UTF8, "text/xml");

                KxLogger.LogActivity("Pay360Provider", "PostPayment", "Payload", xmlMessage);
                new KxData().LogException("Pay360Provider", "PostPayment", "Payload XML data", xmlMessage);

                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
                using (var response = client.PostAsync(URL, payLoad).Result)
                {
                    if (response.IsSuccessStatusCode)
                    {
                        var contents = response.Content.ReadAsStringAsync().Result;

                        var xmldoc = new XmlDocument();
                        xmldoc.LoadXml(contents);

                        KxLogger.LogActivity("Pay360Provider", "PostPayment", "Reference", contents);
                        new KxData().LogException("Pay360Provider", "PostPayment", "Reference XML data", contents);

                        var status = GetNodeValue(xmldoc, "status");

                        if (status == "SUCCESS")
                        {
                            var redirectURL = GetNodeValue(xmldoc, "redirectUrl");
                            if (!string.IsNullOrEmpty(redirectURL))
                            {
                                var scpReference = GetNodeValue(xmldoc, "scpReference");

                                request.AddCustomField("ProviderURL", request.ProviderUrl);
                                request.AddCustomField("SiteId", request.SiteId.ToString());
                                request.AddCustomField("SCPId", request.SCPId.ToString());
                                request.AddCustomField("HMACKeyId", request.HMACKeyId.ToString());
                                request.AddCustomField("HMACKey", request.HMACKey);
                                request.AddCustomField("scpReference", scpReference);

                                PaymentCache cache = new PaymentCache();
                                cache.UpdateCache(request, true);

                                AuditOutboundPaymentData(request, xmlMessage, redirectURL, scpReference);
                                HttpContext.Current.Response.Redirect(redirectURL);
                            }
                            else
                            {
                                KxLogger.LogActivity("Pay360Provider", "ToFormOnline", "Redirect URL not found", contents);
                            }
                        }
                        else
                        {
                            KxLogger.LogActivity("Pay360Provider", "ToFormOnline", "Error in response", contents);
                        }

                    }
                    else
                    {
                        KxLogger.LogActivity("Pay360Provider", "ToFormOnline", "Exception on call to API", response.ReasonPhrase);
                    }
                }
            }
        }

        public static PaymentResponse FromResponse(PaymentCache cache)
        {
            Payments.PaymentResponse response = null;

            if (HttpContext.Current.Request.QueryString["pay360_scp"] != null)
            {
                response = cache.GetResponseFromCache(HttpContext.Current.Request.QueryString["pay360_scp"]);

                var URL = response.CustomFields.FirstOrDefault(c => c.FieldName == "ProviderURL").FieldData;
                var SiteID = response.CustomFields.FirstOrDefault(c => c.FieldName == "SiteId").FieldData;
                var SCPID = response.CustomFields.FirstOrDefault(c => c.FieldName == "SCPId").FieldData;
                var HMACKeyID = response.CustomFields.FirstOrDefault(c => c.FieldName == "HMACKeyId").FieldData;
                var HMACKey = response.CustomFields.FirstOrDefault(c => c.FieldName == "HMACKey").FieldData;
                var scpReference = response.CustomFields.FirstOrDefault(c => c.FieldName == "scpReference").FieldData;

                var Identifier = SCPID;
                var UniqueReference = Guid.NewGuid().ToString();
                var Timestamp = GenerateTimestamp();

                //SecretKey = Concatenate subjectType, identifier, uniqueReference, timestamp, algorithm and hmacKeyID into a single string, with a '!'
                var secretKey = $"{SubjectType}!{Identifier}!{UniqueReference}!{Timestamp}!{Algorithm}!{HMACKeyID}";

                #region SCP Query
                string xmlMessage = $@"<?xml version='1.0' encoding='UTF-8'?>
                <SOAP-ENV:Envelope xmlns:SOAP-ENV='http://schemas.xmlsoap.org/soap/envelope/'>
                            <SOAP-ENV:Header/><SOAP-ENV:Body>
                                        <scpSimpleQueryRequest xmlns='http://www.capita-software-services.com/scp/simple'>
                                            <credentials xmlns='https://support.capita-software.co.uk/selfservice/?commonFoundation'>
                                                <subject>
                                                    <subjectType>{SubjectType}</subjectType>
                                                    <identifier>{Identifier}</identifier>
                                                    <systemCode>{SystemCode}</systemCode>
                                                </subject>
                                                <requestIdentification>
                                                    <uniqueReference>{UniqueReference}</uniqueReference>
                                                    <timeStamp>{Timestamp}</timeStamp>
                                                </requestIdentification>
                                                <signature>
                                                    <algorithm>{Algorithm}</algorithm>
                                                    <hmacKeyID>{HMACKeyID}</hmacKeyID>
                                                    <digest>{CalculateDigest(HMACKey, secretKey)}</digest>
                                                </signature>
                                            </credentials>
                                            <siteId xmlns='http://www.capita-software-services.com/scp/base'>{SiteID}</siteId>
                                            <scpReference xmlns='http://www.capita-software-services.com/scp/base'>{scpReference}</scpReference>
                                        </scpSimpleQueryRequest>
                            </SOAP-ENV:Body>
                </SOAP-ENV:Envelope>


                            ";

                using (var client = new HttpClient())
                {
                    var payLoad = new StringContent(xmlMessage, Encoding.UTF8, "text/xml");

                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
                    using (var queryResponse = client.PostAsync(URL, payLoad).Result)
                    {
                        if (queryResponse.IsSuccessStatusCode)
                        {
                            var contents = queryResponse.Content.ReadAsStringAsync().Result;

                            var xmldoc = new XmlDocument();
                            xmldoc.LoadXml(contents);

                            KxLogger.LogActivity("Pay360Provider", "FromResponse", "Reference", contents);
                            new KxData().LogException("Pay360Provider", "FromResponse", "Reference XML data", contents);

                            var paymentResult = new XmlDocument();
                            paymentResult.LoadXml("<paymentDetails>" + GetNode(xmldoc, "paymentResult") + "</paymentDetails>");

                            var status = GetNodeValue(paymentResult, "status");

                            if (status == "SUCCESS")
                            {
                                var bookingReference = Convert.ToString(HttpContext.Current.Request.QueryString["pay360_scp"]);
                                var uniqueTranId = GetNodeValue(paymentResult, "uniqueTranId");
                                var authCode = GetNodeValue(paymentResult, "authCode");
                                var amountInMinorUnits = GetNodeValue(paymentResult, "amountInMinorUnits");

                                response.InternalReference = bookingReference;
                                response.ProviderReference = uniqueTranId;
                                response.Authorised = true;
                                response.Amount = Convert.ToDecimal(amountInMinorUnits) / 100;
                                response.TransactionDate = DateTime.Now;

                                response.OriginalRequest = cache.GetOriginalRequest(bookingReference);

                                if (response.OriginalRequest != null)
                                {
                                    response.OriginalRequest = cache.GetPayment(response);
                                    KxWebPaymentCache dbCacheRow = new KxWebPaymentCache(null, new Guid(bookingReference));
                                    response.Cache = dbCacheRow;
                                }

                                AuditInboundPaymentData(response);
                            }
                            else
                            {
                                KxLogger.LogActivity("Pay360Provider", "FromResponse", "Error in response", contents);
                            }

                        }
                        else
                        {
                            KxLogger.LogActivity("Pay360Provider", "FromResponse", "Exception on call to API", queryResponse.ReasonPhrase);
                        }
                    }
                }

                #endregion
            }

            return response;
        }

        private static void AuditOutboundPaymentData(PaymentRequest pmtRequest, string strxmldata, string redirectUrl, string scpReference)
        {
            try
            {
                NameValueCollection auditFields = new NameValueCollection();
                auditFields.Add("orderCode", pmtRequest.BookingReference);
                auditFields.Add("Currency", pmtRequest.Currency);
                auditFields.Add("Amount", Convert.ToDecimal(pmtRequest.Amount).ToString("F2"));
                auditFields.Add("XML Data", strxmldata);
                auditFields.Add("RedirectUrl", redirectUrl);
                auditFields.Add("scpReference", scpReference);

                if (KxDbConfiguration.GetConfigurationBool("AuditPayments", true))
                {
                    KxWebStudentPaymentAudit.LogOutBoundPayment(pmtRequest, auditFields);
                }
            }
            catch (Exception e)
            {
                new KxData().LogException("Pay360Provider", "AuditLogOutboundPayment", "Log outbound payment", e.Message);
            }
        }
        private static void AuditInboundPaymentData(PaymentResponse response)
        {
            try
            {
                var auditInputs = new Dictionary<string, string>();

                auditInputs.Add("Internal Reference", response.InternalReference);
                auditInputs.Add("Provider Reference", response.ProviderReference);
                auditInputs.Add("Amount", response.Amount.ToString("F2"));

                if (response.CustomFields != null)
                {
                    foreach (var cf in response.CustomFields)
                    {
                        auditInputs.Add($"Custom Field {cf.FieldName}", cf.FieldData);
                    }
                }

                KxWebStudentPaymentAudit.LogInBoundPayment(response, auditInputs);
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity("Pay360Provider", "AuditInboundPaymentData", ex.Message, "");
            }
        }

        private static string GenerateTimestamp()
        {
            DateTime now = DateTime.UtcNow;
            return string.Format("{0:d4}{1:d2}{2:d2}{3:d2}{4:d2}{5:d2}", now.Year, now.Month, now.Day, now.Hour, now.Minute, now.Second);
        }

        private static string CalculateDigest(string secretkey, string credentialsToHash)
        {
            byte[] keyBytes = Convert.FromBase64String(secretkey);
            byte[] bytesToHash = (new UTF8Encoding()).GetBytes(credentialsToHash);
            HMACSHA256 hmac = new HMACSHA256(keyBytes);
            byte[] hash = hmac.ComputeHash(bytesToHash);
            return Convert.ToBase64String(hash);
        }

        private static string GetNodeValue(XmlDocument doc, string sTagName)
        {
            string result = string.Empty;
            XmlNodeList nodes = doc.GetElementsByTagName(sTagName);

            if (nodes.Count > 0)
                result = nodes[0].InnerXml.ToString();

            return result;
        }

        private static string GetNode(XmlDocument doc, string sTagName)
        {
            string result = string.Empty; ;
            XmlNodeList nodes = doc.GetElementsByTagName(sTagName);

            if (nodes.Count > 0)
                result = nodes[0].InnerXml.ToString();

            return result;
        }
    }
}
