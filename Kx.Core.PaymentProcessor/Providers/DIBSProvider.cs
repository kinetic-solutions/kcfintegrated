﻿using System;
using System.Text;
using System.Web;
using System.Security.Cryptography;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.IO;
using System.Globalization;
using System.Collections.Specialized;
using System.Collections.Generic;
using Kx.Core.PaymentProcessor.Payments;
using Kx.Core.PaymentProcessor.Cache;
using Kx.Core.Exceptions;


namespace Kx.Core.PaymentProcessor.Providers
{
    public class DIBSProvider : BasePaymentProvider
    {
        public DIBSProvider()
        {
        }

        public static void ToForm(NameValueCollection Inputs, PaymentRequest request)
        {
            Decimal amount = (request.Amount * 100);
            long amountToPay = Convert.ToInt64(amount);
            
            Inputs.Add("acceptReturnUrl", request.ProviderCallbackUrl);
            Inputs.Add("cancelReturnUrl", request.ProviderCallbackUrl);
            //Inputs.Add("callbackUrl", request.ProviderCallbackUrl);

            Inputs.Add("amount", amountToPay.ToString());
            Inputs.Add("currency", request.Currency);
            Inputs.Add("merchant", request.ProviderAccountId);
            Inputs.Add("orderId", request.BookingReference);
            Inputs.Add("billingAddress", request.FullAddress);
            Inputs.Add("billingFirstName", request.FirstName);
            Inputs.Add("billingLastName", request.Surname);
            Inputs.Add("billingEmail", request.Email );
            Inputs.Add("captureNow", "1");
            Inputs.Add("s_kx_cacheid", request.BookingReference);

            if (request.TestMode)
            {
                Inputs.Add("test", "1");
            }

            Dictionary<string, string> dict = new Dictionary<string, string>();

            foreach (var k in Inputs.AllKeys)
            {
                dict.Add(k, Inputs[k]);
            }  
            

            String key = request.ProviderPassword;
            
            String mac = calculateMac(dict,key);

            Inputs.Add("MAC", mac);



        }

        public static PaymentResponse FromResponse(PaymentCache cache)
        {

            KxLogger.LogActivity("DIBSProvider", "From Response", "Starting", "");
            KxLogger.LogActivity("DIBSProvider", "From Response", "Status", GetObjectValue(HttpContext.Current.Request["captureStatus"]).ToUpper());

            PaymentResponse response = new PaymentResponse();
            response.InternalReference = GetObjectValue(HttpContext.Current.Request["s_kx_cacheid"]);
            response.ProviderReference = GetObjectValue(HttpContext.Current.Request["transaction"]);

            response.Amount = (GetObjectValueDecimal(HttpContext.Current.Request["amount"])/100);
            response.Authorised = GetObjectValue(HttpContext.Current.Request["captureStatus"]).ToUpper() == "ACCEPTED" ? true : false;

            response.InternalReference = GetObjectValue(HttpContext.Current.Request["s_kx_cacheid"]);
            response.OriginalRequest = cache.GetPayment(response);
            response.Cache.TransactionId = GetObjectValueInt(HttpContext.Current.Request["transaction"]);
            response.Cache.Name = GetObjectValue(HttpContext.Current.Request["billingFirstName"]) + " " + GetObjectValue(HttpContext.Current.Request["billingLastName"]);

            response.TransactionDate = DateTime.Now;
            
            KxLogger.LogActivity("DIBSProvider", "From Response", "Ended", "");

            return response;
        }


        /**
        * calculateMac
        * Calculates the MAC key from a Dictionary<string, string> and a secret key
        * @param params_dict The Dictionary<string, string> object containing all keys and their values for MAC calculation
        * @param K_hexEnc String containing the hex encoded secret key from DIBS Admin
        * @return String containig the hex encoded MAC key calculated
        **/
        static string calculateMac(Dictionary<string, string> params_dict, string K_hexEnc)
        {
            //Create the message for MAC calculation sorted by the key
            var keys = params_dict.Keys.ToList();
            keys.Sort();
            string msg = "";
            foreach (var key in keys)
            {
                if (key != keys[0]) msg += "&";
                msg += key + "=" + params_dict[key];
            }

            //Decoding the secret Hex encoded key and getting the bytes for MAC calculation
            var K_bytes = new byte[K_hexEnc.Length / 2];
            for (int i = 0; i < K_bytes.Length; i++)
            {
                K_bytes[i] = byte.Parse(K_hexEnc.Substring(i * 2, 2), NumberStyles.HexNumber);
            }

            //Getting bytes from message
            var encoding = new UTF8Encoding();
            byte[] msg_bytes = encoding.GetBytes(msg);

            //Calculate MAC key
            var hash = new HMACSHA256(K_bytes);
            byte[] mac_bytes = hash.ComputeHash(msg_bytes);
            string mac = BitConverter.ToString(mac_bytes).Replace("-", "").ToLower();

            return mac;
        }


    }
}
