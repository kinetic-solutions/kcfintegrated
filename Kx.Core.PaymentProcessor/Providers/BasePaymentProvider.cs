using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using Kx.Core.PaymentProcessor.Payments;
using System.Threading;
using Kx.Core.Data;
using Kx.Core.Exceptions;

namespace Kx.Core.PaymentProcessor.Providers
{
    public class BasePaymentProvider
    {
        public static string GetObjectValue(object source)
        {
            string result = "";

            if (source != null)
            {
                try
                {
                    result = source.ToString();
                }
                catch
                {}
            }
            return result;
        }

        public static bool GetObjectValueBoolean(object source)
        {
            bool result = false;

            if (source != null)
            {
                try
                {
                    result = Boolean.Parse(source.ToString());
                }
                catch
                { }
            }
            return result;
        }

        public static double GetObjectValueDouble(object source)
        {
            double result = 0;

            if (source != null)
            {
                try
                {
                    result = Convert.ToDouble(source);
                }
                catch
                {
                    
                }
            }
            return result;
        }

        public static int GetObjectValueInt(object source)
        {
            int result = 0;
            if (source != null)
            {
                try
                {
                    result = Convert.ToInt32(source);
                }
                catch
                {}
            }
            return result;
        }

        public static Decimal GetObjectValueDecimal(object source)
        {
            Decimal result = 0.00M;
			String sSource = "Unassigned";

            if (source != null)
            {
                try
                {
					sSource = source.ToString();
					if (Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName != "en")
					{
						sSource = sSource.Replace('.', Thread.CurrentThread.CurrentCulture.NumberFormat.CurrencyDecimalSeparator[0]);
					}
					result = Convert.ToDecimal(sSource);
                }
				catch (Exception ex)
                {
					KxLogger.LogActivity(Thread.CurrentThread, "BasePaymentProvider", ex.Message, sSource);
				}
            }
            return result;
        }

        public static void AddHiddenField(Page page,string fieldName, string fieldValue)
        {
            // Initialise
            HtmlInputHidden field = new HtmlInputHidden();

            // Set the ID, Name, and Value
            field.ID = fieldName;
            field.Name = fieldName;
            field.Value = fieldValue;

            page.Controls.Add(field);
        }

        public static string FormToString(NameValueCollection Inputs)
        {
            StringBuilder sb = new StringBuilder();

            foreach (string s in Inputs.AllKeys)
            {
                try
                {
                    sb.AppendLine(s + " - " + Inputs.GetValues(s)[0]);
                }
                catch
                {
                    sb.AppendLine(s + " - null");
                }
            }
            return sb.ToString();
        }
    }
}
