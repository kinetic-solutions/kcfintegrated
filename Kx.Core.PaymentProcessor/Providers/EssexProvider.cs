using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;
using System.Net;
using System.Web;
using System.Xml;

using Kx.Core.PaymentProcessor.Payments;
using Kx.Core.PaymentProcessor.Cache;
using System.Security.Cryptography;
using Kx.Core.PaymentProcessor.uk.ac.swan.intranettest;

namespace Kx.Core.PaymentProcessor.Providers
{
    public class EssexProvider : BasePaymentProvider
    {
        public static void ToForm(NameValueCollection Inputs, PaymentRequest request)
        {
            string url = HttpUtility.UrlEncode(request.ProviderCallbackUrl,Encoding.UTF8);

            HttpContext.Current.Response.Redirect(
                String.Format("{0}?productname={5}&productid={1}&quant=M7XeUOIPZlo=&var1={2}&var2={3}&var3={4}", request.ProviderUrl, request.NominalCode, request.BookingReference, url, request.StudentId, request.Description));
        }

        public static PaymentResponse FromResponse(PaymentCache cache)
        {
            PaymentResponse response = new PaymentResponse();

            try { response.ProviderReference = HttpContext.Current.Request["netbanxref"]; }
            catch { }
            try { response.InternalReference = HttpContext.Current.Request["guid"]; }
            catch { }
            try {  }
            catch { }

            try 
            { 
                response.Amount = Convert.ToDecimal(HttpContext.Current.Request["total"]);
                response.Amount = response.Amount / 100;
            }
            catch
            { response.Amount = 0.0M; }

            response.Authorised = response.Amount > 0.0M;

            response.TransactionDate = DateTime.Now;

            response.OriginalRequest = cache.GetPayment(response);
                        
            return response;
        }
    }
}
