using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;
using System.Net;
using System.Web;
using Kx.Core.PaymentProcessor.Payments;
using Kx.Core.PaymentProcessor.Cache;
using System.Security.Cryptography;

namespace Kx.Core.PaymentProcessor.Providers
{
    public class SecPayProvider : BasePaymentProvider
    {
        public SecPayProvider()
        {
        }

        public static void ToForm(NameValueCollection Inputs, PaymentRequest request)
        {
            
            Inputs.Add("mail_customer", request.SecPaySendEmail.ToString().ToLower());
            Inputs.Add("merchant",request.ProviderAccountId);
            Inputs.Add("trans_id",request.BookingReference);
            Inputs.Add("amount",request.Amount.ToString("F"));
            Inputs.Add("digest",GenerateHash(request.BookingReference+request.Amount.ToString("F")+request.SecPayPassword));
            Inputs.Add("order", request.Description);
            //Inputs.Add("template",request
            Inputs.Add("ship_name",request.FullName);
            Inputs.Add("ship_addr_1",request.Address1);
            Inputs.Add("ship_addr_2",request.Address2);
            Inputs.Add("ship_city",request.Town);
            Inputs.Add("ship_state",request.County);
            Inputs.Add("ship_country",request.Country);
            Inputs.Add("ship_postcode",request.Postcode);
            Inputs.Add("ship_tel",request.Telephone);
            Inputs.Add("ship_email",request.Email);
            Inputs.Add("bill_name",request.FullName);
            Inputs.Add("bill_addr_1", request.Address1);
            Inputs.Add("bill_addr_2",request.Address2);
            Inputs.Add("bill_city",request.Town);
            Inputs.Add("bill_state",request.County);
            Inputs.Add("bill_country",request.Country);
            Inputs.Add("bill_postcode",request.Postcode);
            Inputs.Add("bill_tel",request.Telephone);
	        Inputs.Add("bill_email",request.Email);


            Inputs.Add("callback", request.ProviderCallbackUrl);

            if (request.ProviderCallbackUrl.ToUpper().Contains("HTTPS"))
            {
                Inputs.Add("ssl_cb", "true");
            }
            else
            {
                Inputs.Add("cb_post", "true");
            }
     
            if (request.TestMode)
            {
                Inputs.Add("test_status","true");
            }
        }

        public static PaymentResponse FromResponse(PaymentCache cache)
        {
            PaymentResponse response = new PaymentResponse();
            response.InternalReference = GetObjectValue(HttpContext.Current.Request["trans_id"]);
            response.Authorised = GetObjectValue(HttpContext.Current.Request["code"]) =="A";
           
            try
            {
                response.ProviderReference = GetObjectValue(HttpContext.Current.Request["auth_code"]) + "-" + response.InternalReference;
            }
            catch
            {
                response.ProviderReference = GetObjectValue(HttpContext.Current.Request["auth_code"]) + "-" + new Random(DateTime.Now.Millisecond).Next(10000,99000).ToString();
            }

            try
            {
                response.Amount = GetObjectValueDecimal(HttpContext.Current.Request["amount"]);
            }
            catch
            { }

            response.OriginalRequest = cache.GetPayment(response);

            response.TransactionDate = DateTime.Now;

            return response;
        }

        public static string GenerateHash(string input)
        {
            
            System.Security.Cryptography.MD5CryptoServiceProvider x = new System.Security.Cryptography.MD5CryptoServiceProvider();
            byte[] bs = System.Text.Encoding.UTF8.GetBytes(input);
            bs = x.ComputeHash(bs);
            System.Text.StringBuilder s = new System.Text.StringBuilder();
            foreach (byte b in bs)
            {
                s.Append(b.ToString("x2").ToLower());
            }
            return s.ToString();
        }
    }
}
