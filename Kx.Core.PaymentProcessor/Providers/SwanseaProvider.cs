using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;
using System.Net;
using System.Web;
using System.Xml;

using Kx.Core.PaymentProcessor.Payments;
using Kx.Core.PaymentProcessor.Cache;
using System.Security.Cryptography;
using Kx.Core.PaymentProcessor.uk.ac.swan.intranettest;
using Kx.Core.PaymentProcessor.uk.ac.swansea.pgs;
using Kx.Core.PaymentProcessor.SwanseaUniversityDDGateway;
using Kx.Core.Exceptions;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.Remoting.Lifetime;
using Kx.Core.Configuration;
using Kx.Core.Data;
using Newtonsoft.Json;

namespace Kx.Core.PaymentProcessor.Providers
{
    public class SwanseaProvider : BasePaymentProvider
    {
        public static PaymentResponse ToForm(NameValueCollection Inputs, PaymentRequest request)
        {
            PaymentResponse result = null;

            StringBuilder sb = new StringBuilder();

            int pgsVersion = int.Parse(ConfigurationManager.AppSettings["PGSVersion"] ?? "1");

            decimal orderAmount = request.Amount;
            decimal netValue = orderAmount;
            decimal surchargeAmount = 0m;
            decimal surchargePercent = 0m;

            if (pgsVersion > 1)
            {
                if (!string.IsNullOrEmpty(HttpContext.Current.Request.Form["SurchargeAmount"]))
                {
                    surchargeAmount = decimal.Parse(HttpContext.Current.Request.Form["SurchargeAmount"]);
                }
                if (!string.IsNullOrEmpty(HttpContext.Current.Request.Form["SurchargePercent"]))
                {
                    surchargePercent = decimal.Parse(HttpContext.Current.Request.Form["SurchargePercent"]);
                }

                orderAmount += surchargeAmount;
            }

            sb.AppendLine("<?xml version=\"1.0\" encoding=\"utf-8\" ?>");
            sb.AppendLine("<Payment>");
            sb.AppendLine("  <Config>");
            sb.AppendLine(string.Format("    <VersionID>{0}</VersionID>", pgsVersion));
            //sb.AppendLine("    <SystemID>PAOSDJK098JLA098098**8*(!u8908</SystemID>");//Home
            //sb.AppendLine("    <SystemID>OihBr^k*2-43KzxlKtGuF9iIIi223z</SystemID>");//Office

            sb.AppendLine("    <SystemID>" + request.SystemId + "</SystemID>");
            
            if (request.TestMode)
            {
                sb.AppendLine("    <Live>False</Live>");

                if (request.TestTransactionOutcome)
                {
                    sb.AppendLine("    <ProcessingMode>Y</ProcessingMode>");
                }
                else
                {
                    sb.AppendLine("    <ProcessingMode>N</ProcessingMode>");
                }
            }
            else
            {
                sb.AppendLine("    <Live>True</Live>");
                sb.AppendLine("    <ProcessingMode></ProcessingMode>");
            }
            sb.AppendLine("  </Config>");
            sb.AppendLine("  <Order>");
            sb.AppendLine("    <OrderNumber></OrderNumber>");
            sb.AppendLine("    <CardHolderName>"+request.CardholderName+"</CardHolderName>");
            sb.AppendLine("    <Street1>" + request.Address1+"</Street1>");
            sb.AppendLine("    <Street2>" + request.Address2 + "</Street2>");
            sb.AppendLine("    <City>" + request.Town + "</City >");
            sb.AppendLine("    <StateProv>" + request.County + "</StateProv >");
            sb.AppendLine("    <PostCode>" + request.Postcode + "</PostCode>");
            sb.AppendLine("    <Email>" + request.Email + "</Email>");
            sb.AppendLine("    <CardNumber>" + request.CardNumber + "</CardNumber>");
            sb.AppendLine("    <CV2>" + request.CV2.ToString() + "</CV2>");
            sb.AppendLine("    <CardStartDate>" + request.ValidFrom + "</CardStartDate>");
            sb.AppendLine("    <CardExpiryDate>" + request.ExpiryDate + "</CardExpiryDate>");
            sb.AppendLine("    <CardIssueNumber>" + request.IssueNumber.ToString() + "</CardIssueNumber>");
            sb.AppendLine("    <OrderAmount>" + orderAmount.ToString("#0.00") + "</OrderAmount>");
            if (pgsVersion > 1)
            {
                sb.AppendLine("    <AppliedFeePercentage>" + surchargePercent.ToString() + "</AppliedFeePercentage>");
            }
            sb.AppendLine("    <Comments>" + request.Description + "</Comments>");
            sb.AppendLine("  </Order>");
            sb.AppendLine("  <Transactions>");
            sb.AppendLine("    <Transaction>");
            sb.AppendLine("      <JournalID>"+request.ProviderAccountId+"</JournalID>");
            sb.AppendLine("      <LedgerID>"+request.LedgerCode+"</LedgerID>");
            sb.AppendLine("      <AccountCode>" + request.ProviderStudentId + "</AccountCode>");
            if (pgsVersion == 1)
            {
                sb.AppendLine("      <NetValue>" + netValue.ToString("#0.00") + "</NetValue>");
            }
            else
            {
                sb.AppendLine("      <Gross>" + netValue.ToString("#0.00") + "</Gross>");
            }
            sb.AppendLine("      <Discount></Discount>");
            if (pgsVersion > 1)
            {
                if (surchargeAmount > 0m)
                {
                    sb.AppendLine("      <Fee>" + surchargeAmount.ToString("#0.00") + "</Fee>");
                }
                else
                {
                    sb.AppendLine("      <Fee></Fee>");
                }
            }
            sb.AppendLine("      <VAT></VAT>");
            sb.AppendLine("      <NominalCode>"+request.NominalCode+"</NominalCode>");
            sb.AppendLine("      <VATCode></VATCode>");
            sb.AppendLine("    </Transaction>"); 
            sb.AppendLine("  </Transactions>");
            sb.AppendLine("  <Response>");
            sb.AppendLine("    <ParametersValid></ParametersValid>");
            sb.AppendLine("    <ParametersError></ParametersError>");
            sb.AppendLine("    <PaymentProcessed></PaymentProcessed>");
            sb.AppendLine("    <PaymentError></PaymentError>");
            sb.AppendLine("    <EmailSent></EmailSent>");
            sb.AppendLine("    <EmailError></EmailError>");
            sb.AppendLine("    <FinanceSynchronised></FinanceSynchronised>");
            sb.AppendLine("    <FinanceError></FinanceError>");
            sb.AppendLine("    <GenericError></GenericError>");
            sb.AppendLine("  </Response>");
            sb.AppendLine("</Payment>");


            string audit = sb.ToString().Replace('<', '[').Replace('>', ']').Replace(request.CardNumber, "XXXXXXXXXXXXXXXX").Replace(request.CV2, "XXX");
            KxLogger.LogActivity("Swansea Provider", "Outbound Payment Request", audit, request.StudentId);

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(sb.ToString());
          

            XmlNode node = doc.SelectSingleNode("//Payment");

            if (node != null)
            {
                string OrderNumber = "";
                bool ParametersValid = false;
                bool PaymentProcessed = false;
                bool EmailSent = false;
                bool FinanceSynchronised = false;
                bool UnexpectedErrorOccurred = false;
                string ResponseMessage = "";

                if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["SwanseaPGS"]))
                {
                    uk.ac.swansea.pgs.SwanseaUniversityPaymentGateway gateway = new uk.ac.swansea.pgs.SwanseaUniversityPaymentGateway();

                    try
                    {
                        gateway.MakePayment(node, ref OrderNumber, ref ParametersValid, ref PaymentProcessed, ref EmailSent, ref FinanceSynchronised, ref UnexpectedErrorOccurred, ref ResponseMessage);
                        result = FromResponse(node, request, OrderNumber, ParametersValid, PaymentProcessed, EmailSent, FinanceSynchronised, UnexpectedErrorOccurred, ResponseMessage);
                    }
                    catch (Exception ex)
                    {
                        KxLogger.LogActivity("Swansea provider", "ToForm", ex.Message, "");
                    }
                }
                else
                {
                    uk.ac.swan.intranettest.SwanseaUniversityPaymentGateway gateway = new uk.ac.swan.intranettest.SwanseaUniversityPaymentGateway();

                    try
                    {
                        gateway.MakePayment(node, ref OrderNumber, ref ParametersValid, ref PaymentProcessed, ref EmailSent, ref FinanceSynchronised, ref UnexpectedErrorOccurred, ref ResponseMessage);
                        result = FromResponse(node, request, OrderNumber, ParametersValid, PaymentProcessed, EmailSent, FinanceSynchronised, UnexpectedErrorOccurred, ResponseMessage);
                    }
                    catch (Exception ex)
                    {
                        KxLogger.LogActivity("Swansea provider", "ToForm", ex.Message, "");
                    }
                }
            }
            return result;            
        }

        public static PaymentResponse ToDDCheckForm(NameValueCollection Inputs, PaymentRequest request)
        {
            PaymentResponse result = null;
                       
            string SortCode = request.SortCode;
            string AccountID = request.AccountId;
            string SystemID = request.SystemId;
            string BankName = "";
            string BranchName = "";
            bool BankDetailsVerified = false;
            bool UnexpectedErrorOccurred = false;
            string ResponseMessage = "";

            KxLogger.LogActivity("Swansea Provider", "ToDDCheckForm", "Swansea Check Params", string.Format("SystemID: {0}", request.SystemId));

                SwanseaUniversityDirectDebitGateway gateway = new SwanseaUniversityDirectDebitGateway();
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
                try
                {
                    gateway.VerifyBankDetails(SortCode, AccountID, SystemID, ref BankName, ref BranchName, ref BankDetailsVerified, ref UnexpectedErrorOccurred, ref ResponseMessage);
                    KxLogger.LogActivity("Swansea Provider", "ToDDCheckForm", "Swansea Response Message", ResponseMessage);

                    result = FromResponse(request,BankName, BranchName, BankDetailsVerified, UnexpectedErrorOccurred, ResponseMessage);
                }
                catch (Exception ex)
                {
                    KxLogger.LogActivity("Swansea Provider", "ToDDCheckForm", "Error captured", ex.Message);
                }
          
            return result;
        }

        public static PaymentResponse ToDDForm(NameValueCollection Inputs, PaymentRequest request)
        {
            PaymentResponse result = null;

            StringBuilder sb = new StringBuilder();

            sb.AppendLine("<?xml version=\"1.0\" encoding=\"utf-8\" ?>");
            sb.AppendLine("<DirectDebit>");
            sb.AppendLine("  <Config>");
            sb.AppendLine(String.Format("    <VersionID>{0}</VersionID>",1));
            sb.AppendLine(String.Format("    <SystemID>{0}</SystemID>",request.SystemId));
            sb.AppendLine("  </Config>");
            sb.AppendLine("  <DebitDetails>");
            sb.AppendLine(String.Format("    <UserID>{0}</UserID>",request.StudentId));
            sb.AppendLine(String.Format("    <LedgerID>{0}</LedgerID>",request.ProviderAccountId));
            sb.AppendLine(String.Format("    <BankName>{0}</BankName>",request.BankName));
            sb.AppendLine(String.Format("    <BranchName>{0}</BranchName>",request.BranchName));
            sb.AppendLine(String.Format("    <SortCode>{0}</SortCode>",request.SortCode));
            sb.AppendLine(String.Format("    <AccountID>{0}</AccountID>",request.AccountId));
            sb.AppendLine(String.Format("    <PayeeName>{0}</PayeeName>",request.AccountName));
            sb.AppendLine(String.Format("    <Note>{0}</Note>",""));
            sb.AppendLine(String.Format("    <Email>{0}</Email>", request.Email));
            sb.AppendLine("  </DebitDetails>");
            sb.AppendLine("</DirectDebit>");

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(sb.ToString());

            KxLogger.LogActivity("Swansea Provider", "ToDDForm", "Swansea DD Xml OUT", sb.ToString());


            XmlNode node = doc.SelectSingleNode("//DirectDebit");

            if (node != null)
            {
                bool ParametersValid = false;
                bool DirectDebitProcessed = false;
                bool EmailSent = false;
                bool UnexpectedErrorOccurred = false;
                string ResponseMessage = "";

                SwanseaUniversityDirectDebitGateway gateway = new SwanseaUniversityDirectDebitGateway();
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;

                try
                {
                    gateway.CreateDirectDebit(node,ref ParametersValid, ref DirectDebitProcessed,ref EmailSent, ref UnexpectedErrorOccurred,ref ResponseMessage);
                    KxLogger.LogActivity("Swansea Provider", "ToDDForm", "Swansea DD Parameters Valid", ParametersValid.ToString());
                    KxLogger.LogActivity("Swansea Provider", "ToDDForm", "Swansea DD Direct Debit Processed", DirectDebitProcessed.ToString());
                    KxLogger.LogActivity("Swansea Provider", "ToDDForm", "Swansea DD Unexpected Error Occurred", UnexpectedErrorOccurred.ToString());
                    KxLogger.LogActivity("Swansea Provider", "ToDDForm", "Swansea DD ResponseMessage", ResponseMessage);

                    result = FromResponse(request, ParametersValid, DirectDebitProcessed, ResponseMessage);
                }
                catch (Exception ex)
                {
                    KxLogger.LogActivity("Swansea Provider", "ToDDForm", "Exception on WS call", ex.Message);

                }
            }
            return result;            
        }

        public static void ToForm(NameValueCollection inputs, PaymentRequest request, HttpContext ctx)
        {
            try
            {
                var requestData = new SwanseaRequest()
                {
                    UserId = request.StudentId,
                    Language = string.IsNullOrWhiteSpace(request.CustomValue1) ? "en" : request.CustomValue1,
                    TotalAmount = request.Amount,
                    Email = request.Email,
                    MobilePhoneNumber = string.Empty,
                    HomePhoneNumber = request.Telephone,
                    CustomerName = request.FullName,
                    BillingAddressLine1 = request.Address1,
                    BillingAddressLine2 = request.Address2,
                    BillingAddressLine3 = string.Empty,
                    BillingAddressCity = request.Town,
                    BillingAddressCountry = request.Country,
                    BillingAddressPostCode = request.Postcode,
                    SystemId = request.SystemId,
                    ThirdPartyRedirectUrl = request.ProviderCallbackUrl + "?thirdpartyid={{ThirdPartyUniqueId}}&orderid={{OrderId}}&PaymentSuccessful={{PaymentSuccessful}}",
                    ThirdPartyUniqueId = request.BookingReference
                };

                var transaction = new SwanseaTransaction()
                {
                    JournalId = request.ProviderAccountId,
                    LedgerId = request.LedgerCode,
                    AccountCode = request.StudentId,
                    GlCode = request.NominalCode,
                    Gross = request.Amount,
                    Discount = 0m,
                    Vat = 0m,
                    VatCode = string.Empty
                };

                requestData.Transactions = new[] { transaction };

                using (var client = new HttpClient())
                {
                    // Add an Accept header for JSON format.
                    client.DefaultRequestHeaders.Accept.Add(
                        new MediaTypeWithQualityHeaderValue("text/html"));

                    var content = new StringContent(JsonConvert.SerializeObject(requestData), Encoding.UTF8,
                        "application/json");
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                    // List data response
                    using (var response = client.PostAsync(request.ProviderUrl, content).Result)
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            // Parse the response body.
                            var formData =
                                response.Content.ReadAsStringAsync().Result;

                            AuditOutboundPayment(request, requestData);

                            ctx.Response.Clear();
                            ctx.Response.ContentType = "text/html; charset=UTF-8";
                            ctx.Response.Write(formData);
                            ctx.Response.End();
                        }
                        else
                        {
                            var formData =
                                response.Content.ReadAsStringAsync().Result;

                            if (response.Content.Headers.ContentType.MediaType == "application/json")
                            {
                                var errors = JsonConvert.DeserializeObject<SwanseaApiErrors>(formData);

                                KxLogger.LogActivity("Swansea Provider", "ToFormOnline", "Exception on call to API",
                                    errors.ToString());
                            }
                            else
                            {
                                KxLogger.LogActivity("Swansea Provider", "ToFormOnline", "Exception on call to API",
                                    formData);
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                KxLogger.LogActivity("Swansea Provider", "ToForm", e.Message, e.ToString());
            }
            
        }

        public static PaymentResponse FromResponse(HttpContext httpContext, Cache.PaymentCache cache)
        {
            var bookingReference = httpContext.Request.QueryString["thirdpartyid"];

            bool paymentSuccessful;

            bool.TryParse(httpContext.Request.QueryString["PaymentSuccessful"], out paymentSuccessful);

            var providerUniqueOrderId = GetObjectValue(httpContext.Request.QueryString["orderid"]);
            var returnCode = GetObjectValue(httpContext.Request.QueryString["errormessage"]);

            var response = cache.GetResponseFromCache(bookingReference);

            response.AuthorisationMessage = httpContext.Request.RawUrl;
            response.ProviderReference = providerUniqueOrderId;
            response.InternalReference = bookingReference;
            response.Authorised = paymentSuccessful;
            response.TransactionDate = DateTime.Now;

            if (paymentSuccessful)
            {
                response.Amount = response.OriginalRequest.Amount;
            }

            response.OriginalRequest = cache.GetPayment(response);
            if (response.OriginalRequest != null)
            {
                var dbCacheRow = new KxWebPaymentCache(null, new Guid(bookingReference));
                response.Cache = dbCacheRow;
                response.InternalReference = response.Cache.CacheId.ToString();

                AuditInboundPayment(response);
            }

            if (!paymentSuccessful)
            {
                KxLogger.LogActivity("Swansea Provider", "Swansea Online Provider", $"Payment Cancelled",
                $"{providerUniqueOrderId} : {bookingReference} : {returnCode}");
            }

            return response;
        }

        private static void AuditOutboundPayment(PaymentRequest payRequest, SwanseaRequest request)
        {
            if (!KxDbConfiguration.GetConfigurationBool("AuditPayments", true)) return;

            try
            {
                NameValueCollection auditFields = new NameValueCollection();
                auditFields.Add(nameof(request.SystemId), request.SystemId);
                auditFields.Add(nameof(request.CustomerName), request.CustomerName);
                auditFields.Add(nameof(request.Email), request.Email);
                auditFields.Add(nameof(request.Language), request.Language);
                auditFields.Add(nameof(request.ThirdPartyUniqueId), request.ThirdPartyUniqueId);
                auditFields.Add("JournalId", request.Transactions[0].JournalId);
                auditFields.Add("LedgerId", request.Transactions[0].LedgerId);
                auditFields.Add("AccountCode", request.Transactions[0].AccountCode);
                auditFields.Add("Gross", request.Transactions[0].Gross.ToString(CultureInfo.InvariantCulture));
                auditFields.Add("PaymentType", payRequest.PaymentType);

                KxWebStudentPaymentAudit.LogOutBoundPayment(payRequest, auditFields);
            }
            catch (Exception e)
            {
                new KxData().LogException("KxPaymentProvider", "AuditLogOutboundPayment",
                    "Log outbound payment", e.Message);
            }
        }

        private static void AuditInboundPayment(PaymentResponse payResponse)
        {
            if (!KxDbConfiguration.GetConfigurationBool("AuditPayments", true)) return;

            try
            {
                var auditInputs = new Dictionary<string, string>();

                auditInputs.Add(nameof(payResponse.AuthorisationMessage), payResponse.AuthorisationMessage);
                auditInputs.Add(nameof(payResponse.ProviderReference), payResponse.ProviderReference);
                auditInputs.Add(nameof(payResponse.InternalReference), payResponse.InternalReference);
                auditInputs.Add(nameof(payResponse.Authorised), payResponse.Authorised.ToString());
                auditInputs.Add(nameof(payResponse.Amount), payResponse.Amount.ToString("F2"));

                if (payResponse.TransactionDate.HasValue)
                {
                    auditInputs.Add(nameof(payResponse.TransactionDate),
                        payResponse.TransactionDate.Value.ToString("yyyy-MM-dd"));
                }

                KxWebStudentPaymentAudit.LogInBoundPayment(payResponse, auditInputs);
            }
            catch (Exception e)
            {
                new KxData().LogException("KxPaymentProvider", "AuditInboundPayment",
                    "Log inbound payment", e.Message);
            }
        }

        public static PaymentResponse FromResponse(XmlNode node,PaymentRequest request, string OrderNumber, bool ParametersValid, bool PaymentProcessed, bool EmailSent, bool FinanceSynchronised, bool UnexpectedErrorOccurred,string ResponseMessage)
        {
            PaymentResponse response = new PaymentResponse();
            response.OriginalRequest = request;

            response.AuthorisationMessage = ResponseMessage;
            response.ProviderReference = OrderNumber;
            response.Authorised = PaymentProcessed;
            response.Amount = request.Amount;
            response.InternalReference = request.BookingReference;
            response.TransactionDate = DateTime.Now;

            return response;
        }

        public static PaymentResponse FromResponse(PaymentRequest request,string BankName,string BranchName,bool BankDetailsVerified,bool UnexpectedErrorOccurred,string ResponseMessage)
        {
            PaymentResponse response = new PaymentResponse();
            response.OriginalRequest = request;

            response.AuthorisationMessage = ResponseMessage;
            response.BankName = BankName;
            response.BranchName = BranchName;
            response.Authorised = BankDetailsVerified && !UnexpectedErrorOccurred;
            response.Amount = 0.00M;
            response.InternalReference = request.BookingReference;
            response.TransactionDate = DateTime.Now;

            return response;
        }

        public static PaymentResponse FromResponse(PaymentRequest request, bool ParametersValid, bool DirectDebitProcessed, string ResponseMessage)
        {
            PaymentResponse response = new PaymentResponse();
            response.OriginalRequest = request;

            response.AuthorisationMessage = ResponseMessage;
            response.Authorised = DirectDebitProcessed;
            response.Amount = 0.00M;
            response.InternalReference = request.BookingReference;
            response.TransactionDate = DateTime.Now;
            response.ProviderReference = String.Format("{0}-{1:N}", request.StudentId, request.UniqueIdentifier);
            return response;
        }

    }

    public class SwanseaRequest
    {
        public string UserId { get; set; }

        public string Language { get; set; }

        public decimal TotalAmount { get; set; }

        public string Email { get; set; }

        public string CustomerName { get; set; }

        public string MobilePhoneNumber { get; set; }

        public string HomePhoneNumber { get; set; }

        public string BillingAddressLine1 { get; set; }

        public string BillingAddressLine2 { get; set; }

        public string BillingAddressLine3 { get; set; }

        public string BillingAddressCity { get; set; }

        public string BillingAddressCountry { get; set; }

        public string BillingAddressPostCode { get; set; }

        public string SystemId { get; set; }

        public string ThirdPartyRedirectUrl { get; set; }

        public string ThirdPartyUniqueId { get; set; }

        public SwanseaTransaction[] Transactions { get; set; }
    }

    public class SwanseaTransaction
    {
        public string JournalId { get; set; }

        public string LedgerId { get; set; }

        public string GlCode { get; set; }

        public string AccountCode { get; set; }

        public decimal Gross { get; set; }

        public decimal Discount { get; set; }

        public decimal Vat { get; set; }

        public string VatCode { get; set; }
    }

    public class SwanseaApiErrors
    {
        public string[] UserId { get; set; }

        public string[] Language { get; set; }

        public string[] Email { get; set; }

        public string[] CustomerName { get; set; }

        public string[] BillingAddressLine1 { get; set; }

        public string[] BillingAddressLine2 { get; set; }

        public string[] BillingAddressLine3 { get; set; }

        public string[] BillingAddressCity { get; set; }

        public string[] BillingAddressCountry { get; set; }

        public string[] BillingAddressPostCode { get; set; }

        public string[] SystemId { get; set; }

        public override string ToString()
        {
            var dataErrors = new StringBuilder();

            PropertyInfo[] properties = typeof(SwanseaApiErrors).GetProperties();

            foreach (PropertyInfo property in properties)
            {
                if (property.GetValue(this) is string[] value)
                {
                    for (var i = 0; i < value.Length; i++)
                    {
                        AddErrorHeading(dataErrors, property.Name);
                        dataErrors.AppendLine($"  {i + 1}. {value[i]}");
                    }
                }

            }

            return dataErrors.ToString();
        }

        private void AddErrorHeading(StringBuilder data, string propertyName)
        {
            var errorsAlreadyFound = data.Length > 0;

            if (!errorsAlreadyFound)
                data.Append($"{propertyName}:").AppendLine();
            else
                data.AppendLine().Append($"{propertyName}:").AppendLine();
        }
    }
}
