using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Kx.Core.PaymentProcessor")]
[assembly: AssemblyDescription("Kinetic Common Framework - Online payment processor functions")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Kinetic Solutions Ltd")]
[assembly: AssemblyProduct("Kx.Core.PaymentProcessor")]
[assembly: AssemblyCopyright("Copyright ©  2020")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("2709763f-ebf3-4677-8d18-8e0f6f63da41")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Revision and Build Numbers 
// by using the '*' as shown below:
[assembly: AssemblyVersion("2020.1.*")]
[assembly: AssemblyFileVersion("2020.1.5.0")]
