using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Xml;
using Kx.Core.Configuration;
using Kx.Core.Data;
using Kx.Core.Exceptions;
using Kx.Core.PaymentProcessor.Payments;
using Kx.Core.PaymentProcessor.Providers;
using Kx.Core.PaymentProcessor.Cache;
using System.Runtime.Serialization;
using System.Configuration;
using Kx.PaymentsSdk.Exceptions;

namespace Kx.Core.PaymentProcessor
{
    public class PaymentProcessor
    {
        public PaymentCache Cache
        {
            get
            {
                if (HttpContext.Current.Application["paymentcache"] == null)
                {
                    HttpContext.Current.Application["paymentcache"] = new PaymentCache();
                }
                return (PaymentCache)HttpContext.Current.Application["paymentcache"];
            }
        }

        public static bool IsPaymentReceived
        {
            get
            { 
                return GetIsPaymentReceived();
            }
        }

        public PaymentProcessor()
        {
        }

        public PaymentResponse PostPayment(PaymentRequest request)
        {
            KxData data = new KxData();
            
            data.ExecuteAuditLog("Outbound Payment", request.BookingReference, request.ToString());
            
            PaymentResponse result = null;
            string contentType = "";
            bool CustomHandler = false;

            if (Cache.AddPayment(request))
            {
                NameValueCollection Inputs = new NameValueCollection();
                switch (request.PaymentProvider.ToUpper())
                {
                    // WPMRHUL Added to exclude Student Fields for RHUL Commercial WPM payment (BnB)
                    // WPMBASIC Added for Exeter as this needs not quite RHUL but nearly.
                    case "AXIS": AXISProvider.ToForm(Inputs, request); break;
                    case "AXISXML": AXISProviderXML.ToForm(Inputs, request); break;
                    case "DUMMY": CustomHandler = true; DummyProvider.ToForm(Inputs, request); break;
                    case "EPDQ": { contentType = "application/x-www-form-urlencoded"; CustomHandler = true; EDPQProvider.ToForm(Inputs, request); }; break;
                    case "ERA": ERAProvider.ToForm(Inputs, request); break;
                    case "ESSEX": { CustomHandler = true; EssexProvider.ToForm(Inputs, request); }; break;
                    case "HSBC": HSBCProvider.ToForm(Inputs, request); break;
                    case "LANCASTERDD": LancasterDDProvider.ToForm(Inputs, request); break;
                    case "MENTEC": { CustomHandler = true; MentecProvider.ToForm(Inputs, request); }; break;
                    case "MONETICO": MoneticoProvider.ToForm(Inputs, request); break;
                    case "REALEX": RealexProvider.ToForm(Inputs, request); break;
                    case "SECPAY": SecPayProvider.ToForm(Inputs, request); break;
                    case "SECURETRADING": SecureTradingProvider.ToForm(Inputs, request, HttpContext.Current); break;
                    case "SH": { SHProvider.ToForm(Inputs, request); }; break;        
                    case "SWANSEA": { CustomHandler = true; result = SwanseaProvider.ToForm(Inputs, request); }; break;
                    case "SWANSEADDCHECK": { CustomHandler = true; result = SwanseaProvider.ToDDCheckForm(Inputs, request); }; break;
                    case "SWANSEADD": { CustomHandler = true; result = SwanseaProvider.ToDDForm(Inputs, request); }; break;
                    case "SWANSEAONLINE": { CustomHandler = true; SwanseaProvider.ToForm(Inputs, request, HttpContext.Current); }; break;
                    case "UKCBNB": UKCBnBProvider.ToForm(Inputs, request); break;
                    case "UKPLC": { CustomHandler = true; UKPLCProvider.ToForm(Inputs, request, Cache); }; break;
                    case "WARWICK": WarwickTTSProvider.ToForm(Inputs, request); break;
                    case "WORLDPAY": WorldpayProvider.ToForm(Inputs, request); break;
                    case "WPMKX": WpmKxProvider.ToForm(Inputs, request); break;
                    case "WPM": WPMProvider.ToForm(Inputs, request); break;
                    case "WPMRHUL": WPMProvider.ToForm(Inputs, request); break;
                    case "WPMBASIC": UKCBnBProvider.ToForm(Inputs, request); break;
                    case "WPM3WAY": WPM3WayProvider.ToForm(Inputs, request); break;
                    case "WPMEXT": WPMExtendedProvider.ToForm(Inputs, request); break;
                    case "WPMLIBERTY": WPM3WayProvider.ToForm(Inputs, request); break;
                    case "BCSP" : BarclaycardSmartPay.ToForm(Inputs, request); break;
                    case "BEANSTREAM" : BeanstreamProvider.ToForm(Inputs, request); break;
                    case "EXACT": ExactProvider.ToForm(Inputs, request); break;
                    case "DIBS": DIBSProvider.ToForm(Inputs, request); break;
					case "SAGEPAY": SagePayProvider.ToForm(Inputs, request); break;
                    case "WORLDPAYCORPORATE":
                        {
                            CustomHandler = true;
                            WorldpayCorporateProvider.ToForm(Inputs, request); 
                            break;
                        }
                    case "CALEDON":
                        {
                            CustomHandler = true;
                            CaledonProvider.ToForm(Inputs, request, HttpContext.Current);
                        }
                        break;
                    case "NETAXCEPT":
                        {
                            CustomHandler = true;
                            NETAXEPTProvider.ToForm(Inputs, request);
                        }
                        break;
                    case "NETAXEPT":
                        {
                            CustomHandler = true;
                            NETAXEPTProvider.ToForm(Inputs, request);
                        }
                        break;
                    case "KXPAYMENT":
                        {
                            CustomHandler = true;
                            KxPaymentProvider.ToForm(Inputs, request);
                        }
                        break;
                    case "PAY360":
                        {
                            CustomHandler = true;
                            Pay360Provider.ToForm(Inputs, request);
                            break;
                        }
                }

                if (KxDbConfiguration.GetConfigurationBool("AuditPayments", true))
                {
                    try
                    {
                        KxWebStudentPaymentAudit.LogOutBoundPayment(request, Inputs);
                    }
                    catch (Exception e)
                    {
                        new KxData().LogException("Payment Processor", "Process Response", "Log outbound payment", e.Message);
                    }
                }

                KxLogger.LogActivity(this, "PostPayment", "SessionID.OUT", HttpContext.Current.Session.SessionID);

                if (!CustomHandler)
                {
                    KxData newdata = new KxData();
                    newdata.ExecuteAuditLog("Outbound Payment", "Detail", BasePaymentProvider.FormToString(Inputs));

                    try
                    {
                        Transfer(request.ProviderUrl, Inputs, contentType);
                    }
                    catch (Exception ex)
                    {
                        KxLogger.LogActivity(this, "Payment Transfer", request.ProviderUrl, ex.Message);
                    }
                }
            }

            return result;
        }

        private static bool IsAXISXML()
        {
            bool result = false;
            string sXML = (HttpContext.Current.Request["status"] ?? "").ToString();

            if (sXML != "")
            {
                try
                {
                    sXML = HttpContext.Current.Server.UrlDecode(sXML);
                    XmlDocument doc = new XmlDocument();
                    doc.LoadXml(sXML);

                    XmlNodeList nodes = doc.GetElementsByTagName("portalApacsAPIRes");

                    if (nodes.Count > 0)
                    {
                        nodes = doc.GetElementsByTagName("sessionID");

                        if (nodes.Count > 0)
                        {
                            result = (nodes[0].InnerText != "");
                        }
                    }
                }
                catch { }
            }

            return result;
        }

        public static bool GetIsPaymentReceived()
        {
            KxData data = new KxData();
            string formstring = "";
            bool result = false;
            try
            {
                formstring = HttpContext.Current.Request.Form.ToString();
            }
            catch { }
           
            // IMPORTANT, any new providers must be updated in here to identify a payment...
            //KxLogger.LogActivity(HttpContext.Current, "Payment GetIsPaymentReceived", formstring, "");
            try
            {
                result = HttpContext.Current.Request["paymentProvider"] != null || // KxPayment provider response
                        HttpContext.Current.Request["dummy"] != null ||
                        HttpContext.Current.Request["kxpayment"] != null || // KxPayments
                        HttpContext.Current.Request["kxpaymentwebhook"] != null || // KxPaymentsHook
                         HttpContext.Current.Request["x_login"] != null || // E-Xact (Part of Chase Paymentech)
                         HttpContext.Current.Request["requid"] != null || //AXIS
                         IsAXISXML() || //AXISXML
                         HttpContext.Current.Request["CpiResultsCode"] != null ||
                         HttpContext.Current.Request["ttsTransId"] != null ||
                        (HttpContext.Current.Request["transaction_status"] != null && HttpContext.Current.Request["g_reference"] != null && HttpContext.Current.Request["currency"] != null) ||
                        (HttpContext.Current.Request["cartId"] != null && HttpContext.Current.Request["transStatus"] != null) ||
                        HttpContext.Current.Request["transactionstatus"] != null ||
                        HttpContext.Current.Request["txtOrder"] != null ||
                        HttpContext.Current.Request["code"] != null ||
                        HttpContext.Current.Request["orderkey"] != null ||
                        HttpContext.Current.Request["spare_three"] != null ||
                        HttpContext.Current.Request["KX_CUSTOM_customfield1"] != null ||
                        HttpContext.Current.Request["Kx_ref"] != null || // WPM Liberty...
                        HttpContext.Current.Request["kxcartid"] != null || // Secure Trading...
                        HttpContext.Current.Request["guid"] != null || // Essex...
                        HttpContext.Current.Request["g_payment_ref"] != null || //UKCBnB
                        HttpContext.Current.Request["kx_status"] != null || //LancasterDD
                        HttpContext.Current.Request["transactionnumber"] != null ||
                        HttpContext.Current.Request["monetico"] != null ||  //Monetico second postback
                        HttpContext.Current.Request["texte-libre"] != null ||  //Monetico first postback
                        HttpContext.Current.Request["ref1"] != null ||  //Beanstream first postback
                        formstring.ToUpper().Contains("KXPAYMENTPROCESSOR") || // WPMXml first postback
                        HttpContext.Current.Request["PASREF"] != null ||
                        HttpContext.Current.Request["kx7502d"] != null ||
                        HttpContext.Current.Request.QueryString["bookingreference"] != null || // WPM Xml redirect
                        HttpContext.Current.Request.QueryString["Rc"] != null ||
                        (HttpContext.Current.Request.QueryString["merchantReference"] != null) || //Barclaycard Smartpay
                        (HttpContext.Current.Request.QueryString["transactionId"] != null && HttpContext.Current.Request.QueryString["responseCode"] != null) || // NETAXEPT
                        HttpContext.Current.Request.QueryString["Crypt"] != null || // SagePay
                        HttpContext.Current.Request["captureStatus"] != null || // DIBS                        
                        HttpContext.Current.Request["reference_number"] != null || // Caledon
                        (HttpContext.Current.Request["thirdpartyid"] != null && HttpContext.Current.Request.AppRelativeCurrentExecutionFilePath.Contains("PaymentResponse")) || // Swansea Online
                        HttpContext.Current.Request["pay360_scp"] != null; // Pay360 SCP

            }
            catch (Exception ex)
            {
                data.ExecuteAuditLog("PaymentProcessor", "PaymentProcessor.GetIsPaymentReceived() error", ex.Message);
            }

            return result;
        }

        public PaymentResponse ProcessResponse()
        {
			KxData data1 = new KxData();
            try
            {
                if (!String.IsNullOrEmpty(HttpContext.Current.Request.Form.ToString()))
                {

                    data1.ExecuteAuditLog("Inbound Payment Post", "Form String", HttpContext.Current.Request.Form.ToString());
                }
                else
                {
                    data1.ExecuteAuditLog("Inbound Payment Post", "Request String", HttpContext.Current.Request.ToString());
                }

                String allParams = String.Empty;
                foreach (String key in HttpContext.Current.Request.Params.AllKeys)
                {
                    allParams += String.Format("{0}\r\n",HttpContext.Current.Request[key]);
                }

                data1.ExecuteAuditLog("ProcessResponse()", "HttpContext.Current.Request.Params", allParams);

                data1.CloseConnection();
            }
            catch { }

			PaymentResponse result = ProcessResponse("");
            if (result != null)
            {
                KxData data = new KxData();
				data.ExecuteAuditLog("Inbound Payment", result.InternalReference, result.ToString());
				data.CloseConnection();
            }
            return result;
        }

        public PaymentResponse ProcessResponse(string input)
        {
            string formstring = "";
            try
            {
                formstring = HttpContext.Current.Request.Form.ToString();
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "Payment ProcessResponse Failure", ex.Message, "");
            }

            PaymentResponse result = null;


            try
            {
                // DummyProvider
                if (HttpContext.Current.Request["dummy"] != null)
                {
                    result = DummyProvider.FromResponse(Cache);
                } // E-Xact (Part of ChasePaymentech) Provider
                else if (HttpContext.Current.Request["x_login"] != null)
                {
                    result = ExactProvider.FromResponse(Cache);
                }
                else if (HttpContext.Current.Request["kxpayment"] != null ||
                         HttpContext.Current.Request["kxpaymentwebhook"] != null)
                {
                    result = KxPaymentProvider.FromResponse(Cache);
                }
                // SagePay
                else if (HttpContext.Current.Request["Crypt"] != null)
                {
                    result = SagePayProvider.FromResponse(HttpContext.Current, Cache);
                } // DIBS
                else if (HttpContext.Current.Request["captureStatus"] != null)
                {
                    result = DIBSProvider.FromResponse(Cache);
                } // AXIS Provider
                else if (HttpContext.Current.Request["requid"] != null)
                {
                    result = AXISProvider.FromResponse(Cache);
                } // AXIS XML Provider
                else if (IsAXISXML())
                {
                    result = AXISProviderXML.FromResponse(Cache);
                }
                else if (HttpContext.Current.Request["monetico"] != null)
                {
                    result = MoneticoProvider.FromResponse(Cache);
                }
                else if (HttpContext.Current.Request["texte-libre"] != null)
                {
                    result = MoneticoProvider.FromResponse(Cache);
                }
                else if (HttpContext.Current.Request["ref1"] != null)
                {
                    result = BeanstreamProvider.FromResponse(Cache);
                }
                else if (HttpContext.Current.Request["ttsTransId"] != null)
                {
                    result = WarwickTTSProvider.FromResponse(Cache);
                } // Warwick TTS provider
                else if (HttpContext.Current.Request["CpiResultsCode"] != null)
                {
                    result = HSBCProvider.FromResponse(Cache);
                } // WPM provider
                else if (HttpContext.Current.Request["transaction_status"] != null &&
                         HttpContext.Current.Request["g_reference"] != null &&
                         HttpContext.Current.Request["currency"] != null)
                {
                    result = WPMProvider.FromResponse(Cache);
                } //WPM Kx Provider
                else if (HttpContext.Current.Request.QueryString["bookingreference"] != null &&
                         HttpContext.Current.Request["transactionnumber"] == null)
                {
                    result = WpmKxProvider.FromResponse(Cache, this);
                }
                else if (formstring.ToUpper().Contains("KXPAYMENTPROCESSOR"))
                {
                    result = WpmKxProvider.FromResponse(Cache, this);
                }
                else if (HttpContext.Current.Request["kx7502d"] != null)
                {
                    result = WPMProvider.FromResponse(Cache);
                } // WorldpayProvider
                else if (HttpContext.Current.Request["cartId"] != null &&
                         HttpContext.Current.Request["transStatus"] != null)
                {
                    result = WorldpayProvider.FromResponse(Cache);
                } // ePDQ Provider
                else if (HttpContext.Current.Request.Form["transactionstatus"] != null ||
                         HttpContext.Current.Request["oid"] != null)
                {
                    result = EDPQProvider.FromResponse(HttpContext.Current, Cache);
                }
                else if (HttpContext.Current.Request.Form["txtOrder"] != null)
                {
                    result = ERAProvider.FromResponse(Cache);
                }
                else if (HttpContext.Current.Request["orderkey"] != null && HttpContext.Current.Request["mac2"] != null)
                {
                    result = WorldpayCorporateProvider.FromResponse(Cache);
                }
                else if (HttpContext.Current.Request["code"] != null)
                {
                    result = SecPayProvider.FromResponse(Cache);
                }
                else if (HttpContext.Current.Request["kxcartid"] != null)
                {
                    result = SecureTradingProvider.FromResponse(HttpContext.Current, Cache);
                }
                else if (HttpContext.Current.Request["guid"] != null)
                {
                    result = EssexProvider.FromResponse(Cache);
                }
                else if (HttpContext.Current.Request.Form["KX_CUSTOM_customfield1"] != null)
                {
                    result = WPMExtendedProvider.FromResponse(Cache);
                }
                else if ((HttpContext.Current.Request.Form["g_payment_ref"] != null) &&
                         (HttpContext.Current.Request.Form["g_reference"] != null)) //WPMBasic
                {
                    result = UKCBnBProvider.FromResponse(Cache);
                }
                else if (HttpContext.Current.Request.Form["Kx_ref"] != null)
                {
                    result = WPM3WayProvider.FromResponse(Cache);
                }
                else if (HttpContext.Current.Request.Form["kx_status"] != null)
                {
                    result = LancasterDDProvider.FromResponse(Cache);
                }
                else if (HttpContext.Current.Request["transactionnumber"] != null)
                {
                    //security hosting provider
                    result = SHProvider.FromResponse(Cache);
                }
                else if (HttpContext.Current.Request.QueryString["Rc"] != null)
                {
                    result = MentecProvider.FromResponse(Cache);
                }
                else if (HttpContext.Current.Request.Form["Spare_three"] != null)
                {
                    result = ERAProvider.FromResponse(Cache);
                }
                else if (HttpContext.Current.Request.Form["g_payment_ref"] != null) //wpm ukc
                {
                    result = UKCBnBProvider.FromResponse(Cache);
                }
                else if (HttpContext.Current.Request.Form["PASREF"] != null) //wpm ukc
                {
                    result = RealexProvider.FromResponse(Cache);
                }
                else if (HttpContext.Current.Request.QueryString["merchantReference"] != null)
                {
                    result = BarclaycardSmartPay.FromResponse(HttpContext.Current, Cache);
                }
                else if (HttpContext.Current.Request.QueryString["transactionId"] != null &&
                         HttpContext.Current.Request["responseCode"] != null &&
                         HttpContext.Current.Session["PaymentProcessed"] != "1")
                {
                    result = NETAXEPTProvider.FromResponse(HttpContext.Current, Cache);
                }
                else if (HttpContext.Current.Request["reference_number"] != null)
                {
                    result = CaledonProvider.FromResponse(HttpContext.Current, Cache);
                }
                else if (HttpContext.Current.Request["thirdpartyid"] != null)
                {
                    result = SwanseaProvider.FromResponse(HttpContext.Current,
                        Cache); // Change to SwanseaProvider.FromResponse.
                }
                else if (HttpContext.Current.Request.QueryString["pay360_scp"] != null)
                {
                    result = Pay360Provider.FromResponse(Cache);
                }

                if (result != null)
                {
                    Dictionary<string, string> inputVariables =
                        new Dictionary<string, string>(StringComparer.InvariantCultureIgnoreCase);
                    foreach (string key in HttpContext.Current.Request.Form.AllKeys)
                    {
                        inputVariables.Add(key, HttpUtility.HtmlEncode(HttpContext.Current.Request.Form[key]));
                        result.RawResponseData.Add(key, HttpUtility.HtmlEncode(HttpContext.Current.Request.Form[key]));
                    }

                    foreach (string key in HttpContext.Current.Request.QueryString.AllKeys)
                    {
                        if (!inputVariables.ContainsKey(key))
                        {
                            inputVariables.Add(key,
                                HttpUtility.HtmlEncode(HttpContext.Current.Request.QueryString[key]));
                            result.RawResponseData.Add(key,
                                HttpUtility.HtmlEncode(HttpContext.Current.Request.QueryString[key]));
                        }
                    }

                    if (KxDbConfiguration.GetConfigurationBool("AuditPayments", true))
                    {
                        try
                        {
                            KxWebStudentPaymentAudit.LogInBoundPayment(result, inputVariables);
                        }
                        catch (Exception e)
                        {
                            new KxData().LogException("Payment Processor", "Process Response", "Log inbound payment",
                                e.Message);
                        }
                    }

                    try
                    {
                        HttpContext.Current.Session["KxWebLastPayment"] = result.OriginalRequest.UniqueIdClean;
                    }
                    catch
                    {
                    }
                }
            }
            catch (KxPaymentsException ex)
            {
                KxLogger.LogActivity(this, "Payment ProcessResponse", ex.InnerException?.ToString() ?? "", ex.Message);
            }
			catch (Exception ex)
			{
                KxLogger.LogActivity(this, "Payment ProcessResponse", formstring, ex.Message);
			}
			
            return result;
        }

        public void ReturnTransaction()
        {
            string confirmationPageUrl = KxDbConfiguration.GetConfiguration("ConfirmationPageUrl");
            string cancellationPageUrl = KxDbConfiguration.GetConfiguration("CancellationPageUrl");
            PaymentResponse response = ProcessResponse();
            HttpContext.Current.Session["paymentresponse"] = response;
            string transferUrl = response.Authorised ? confirmationPageUrl : cancellationPageUrl;
            // If using the ERA provider, the paymentresponse session value is lost.
            // So we must send  any required values to the succesful page so we can
            // set the paymentresponse value again.
            if (HttpContext.Current.Request.Form["Spare_three"] != null)
            {
                NameValueCollection Inputs = new NameValueCollection();
                Inputs.Add("Spare_three", HttpContext.Current.Request["Spare_three"].ToString());
                Inputs.Add("txtOrder", HttpContext.Current.Request["txtOrder"].ToString());
                Inputs.Add("txtAmount", HttpContext.Current.Request["txtAmount"].ToString());
                Inputs.Add("txtCardtype", HttpContext.Current.Request["txtCardtype"].ToString());
                //Transfer(transferUrl, Inputs, "");
                DisplayPageInline(transferUrl, Inputs);
            }
            else if (HttpContext.Current.Request.Form["PASREF"] != null)
            {
                NameValueCollection Inputs = new NameValueCollection();
                Inputs.Add("PASREF", HttpContext.Current.Request.Form["PASREF"].ToString());
                Inputs.Add("ORDER_ID", HttpContext.Current.Request["ORDER_ID"].ToString());
                Inputs.Add("AUTHCODE", HttpContext.Current.Request["AUTHCODE"].ToString());
                Inputs.Add("MESSAGE", HttpContext.Current.Request["MESSAGE"].ToString());
                Inputs.Add("RESULT", HttpContext.Current.Request["RESULT"].ToString());
                Inputs.Add("AMOUNTVALUE", HttpContext.Current.Request["AMOUNTVALUE"].ToString());
                Transfer(transferUrl, Inputs, "");
            }
            else
            {
                Transfer(transferUrl, null, "");
            }
        }
        
        public PaymentResponse ReturnTransactionWorkflow()
        {
            PaymentResponse response = ProcessResponse();
            return response;            
        }

        public string Transfer(string transferUrl, NameValueCollection Inputs, string contentType)
        {
            return Transfer(transferUrl, Inputs, contentType, HttpContext.Current);
        }

        public string Transfer(string transferUrl, NameValueCollection Inputs, string contentType, HttpContext ctx)
        {
            StringBuilder result = new StringBuilder();

            PaymentResponse response = ProcessResponse();
            ctx.Response.Clear();
            ctx.Response.Write("<html><head>");
            if (contentType != "")
            {
                ctx.Response.ContentType = contentType;
            }
            result.AppendLine(string.Format("</head><body onload=\"document.{0}.submit()\">", "form1"));
            result.AppendLine(string.Format("<form name=\"{0}\" method=\"{1}\" action=\"{2}\" >", "form1", "POST", transferUrl));

            ctx.Response.Write(string.Format("</head><body onload=\"document.{0}.submit()\">", "form1"));
            ctx.Response.Write(string.Format("<form name=\"{0}\" method=\"{1}\" action=\"{2}\" >", "form1", "POST", transferUrl));
            if (Inputs != null)
            {
                for (int i = 0; i < Inputs.Keys.Count; i++)
                {
                    HttpContext.Current.Response.Write(string.Format("<input name=\"{0}\" type=\"hidden\" value=\"{1}\"/>", Inputs.Keys[i], Inputs[Inputs.Keys[i]]));
                    result.AppendLine(string.Format("<input name=\"{0}\" type=\"hidden\" value=\"{1}\"/>", Inputs.Keys[i], Inputs[Inputs.Keys[i]]));
                }
            }
            ctx.Response.Write("</form>");
            ctx.Response.Write("</body></html>");

            result.AppendLine("</form>");
            result.AppendLine("</body></html>");

            ctx.Response.End();

            return result.ToString();
        }

        private void DisplayPageInline(string transferUrl, NameValueCollection Inputs)
        {
            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(new Uri(transferUrl));
            req.Method = "POST";
            req.ContentType = "application/x-www-form-urlencoded";

            ASCIIEncoding encoding = new ASCIIEncoding();
            StringBuilder postData = new StringBuilder();

            for (int i = 0; i < Inputs.Keys.Count; i++)
            {
                if (postData.Length > 0)
                {
                    postData.Append("&");
                }
                postData.AppendFormat("{0}={1}", Inputs.Keys[i], Inputs[Inputs.Keys[i]]);
            }

            byte[] data = encoding.GetBytes(postData.ToString());

            using (Stream reqStream = req.GetRequestStream())
            {
                reqStream.Write(data, 0, data.Length);
                reqStream.Close();
            }

            string content = string.Empty;

            HttpWebResponse res = (HttpWebResponse)req.GetResponse();
            using (StreamReader rdr = new StreamReader(res.GetResponseStream()))
            {
                content = rdr.ReadToEnd();
                rdr.Close();
            }

            if (!string.IsNullOrEmpty(content))
            {
                content = content.Replace("<head>", string.Format("<head><base href=\"{0}\" />", transferUrl));
            }

            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.Write(content);
            HttpContext.Current.Response.End();
        }

        public virtual PaymentStatus ProcessPayment()
        {
            return PaymentStatus.None;
        }
    }
}
