using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Globalization;

namespace Kx.Core.Utility
{
    public class KxStringUtils
    {
        public static string ObjectToString(object obj)
        {
            string result = "";

            if (obj != null)
            {
                result = obj.ToString();
            }
            return result;
        }

        public static string ObjectToString(object obj, string defaultvalue)
        {
            string result = ObjectToString(obj);

            if (result == "")
            {
                result = defaultvalue;
            }
            return result;
        }

        public static string GetBody(string source,bool IncludeHeader)
        {
            StringBuilder rb = new StringBuilder(source);

            string result = source;

            int bodyStart = result.ToUpper().IndexOf("<BODY");
            int bodyEnd = result.ToUpper().IndexOf("</BODY");

            StringBuilder sb = new StringBuilder();

            if (bodyStart > -1 && bodyEnd > -1)
            {
                sb.Append(source.Substring(bodyStart, (bodyEnd - bodyStart)));
                sb.Remove(0, sb.ToString().IndexOf(">") + 1);
            }

            if (IncludeHeader)
            {
                sb.Insert(0, @"<html><head><meta http-equiv='content-type' content='text/html;charset=UTF-8'></head><body>");
                sb.Append(@"</body></html>");
                sb.Replace("\r\n", "");
            }
            return sb.ToString();
        }

        public static string PageToDocument(string source)
        {
            try
            {
                string result;
                System.Text.RegularExpressions.RegexOptions options = System.Text.RegularExpressions.RegexOptions.IgnoreCase;

                result = source.Replace("\r", " ");
                result = result.Replace("\n", " ");
                result = result.Replace("\t", string.Empty);
                result = System.Text.RegularExpressions.Regex.Replace(result, @"( )+", " ");
                result = System.Text.RegularExpressions.Regex.Replace(result, @"<( )*head([^>])*>", "<head>", options);
                result = System.Text.RegularExpressions.Regex.Replace(result, @"(<( )*(/)( )*head( )*>)", "</head>",options);
                result = System.Text.RegularExpressions.Regex.Replace(result, @"(<head>).*(</head>)", string.Empty,options);
                result = System.Text.RegularExpressions.Regex.Replace(result, @"<( )*script([^>])*>", "<script>",options);
                result = System.Text.RegularExpressions.Regex.Replace(result, @"(<( )*(/)( )*script( )*>)", "</script>",options);
                result = System.Text.RegularExpressions.Regex.Replace(result, @"(<script>).*(</script>)", string.Empty,options);
                result = System.Text.RegularExpressions.Regex.Replace(result, @"<( )*style([^>])*>", "<style>",options);
                result = System.Text.RegularExpressions.Regex.Replace(result, @"(<( )*(/)( )*style( )*>)", "</style>", options);
                result = System.Text.RegularExpressions.Regex.Replace(result, "(<style>).*(</style>)", string.Empty, options);
                result = System.Text.RegularExpressions.Regex.Replace(result, @"<( )*td([^>])*>", "\t",options);
                result = System.Text.RegularExpressions.Regex.Replace(result, @"<( )*br( )*>", "\r", options);
                result = System.Text.RegularExpressions.Regex.Replace(result, @"<( )*li( )*>", "\r", options);
                result = System.Text.RegularExpressions.Regex.Replace(result, @"<( )*div([^>])*>", "\r\r",options);
                result = System.Text.RegularExpressions.Regex.Replace(result, @"<( )*tr([^>])*>", "\r\r",options);
                result = System.Text.RegularExpressions.Regex.Replace(result, @"<( )*p([^>])*>", "\r\r",options);
                result = System.Text.RegularExpressions.Regex.Replace(result, @"<[^>]*>", string.Empty,options);
                result = System.Text.RegularExpressions.Regex.Replace(result, @"&nbsp;", " ",options);
                result = System.Text.RegularExpressions.Regex.Replace(result, @"&bull;", " * ",options);
                result = System.Text.RegularExpressions.Regex.Replace(result, @"&lsaquo;", "<",options);
                result = System.Text.RegularExpressions.Regex.Replace(result, @"&rsaquo;", ">",options);
                result = System.Text.RegularExpressions.Regex.Replace(result, @"&trade;", "(tm)",options);
                result = System.Text.RegularExpressions.Regex.Replace(result, @"&frasl;", "/",options);
                result = System.Text.RegularExpressions.Regex.Replace(result, @"<", "<",options);
                result = System.Text.RegularExpressions.Regex.Replace(result, @">", ">",options);
                result = System.Text.RegularExpressions.Regex.Replace(result, @"&copy;", "(c)",options);
                result = System.Text.RegularExpressions.Regex.Replace(result, @"&reg;", "(r)",options);
                result = System.Text.RegularExpressions.Regex.Replace(result, @"&(.{2,6});", string.Empty,options);

                result = result.Replace("\n", "\r");

                result = System.Text.RegularExpressions.Regex.Replace(result, "(\r)( )+(\r)", "\r\r",options);
                result = System.Text.RegularExpressions.Regex.Replace(result, "(\t)( )+(\t)", "\t\t",options);
                result = System.Text.RegularExpressions.Regex.Replace(result, "(\t)( )+(\r)", "\t\r",options);
                result = System.Text.RegularExpressions.Regex.Replace(result, "(\r)( )+(\t)", "\r\t",options);
                result = System.Text.RegularExpressions.Regex.Replace(result, "(\r)(\t)+(\r)", "\r\r",options);
                result = System.Text.RegularExpressions.Regex.Replace(result, "(\r)(\t)+", "\r\t",options);

                string breaks = "\r\r\r";
                string tabs = "\t\t\t\t\t";

                for (int index = 0; index < result.Length; index++)
                {
                    result = result.Replace(breaks, "\r\r");
                    result = result.Replace(tabs, "\t\t\t\t");
                    breaks = breaks + "\r";
                    tabs = tabs + "\t";
                }
                return result;
            }
            catch
            {
                return source;
            }
        }


        public static string HardTrim(string source)
        {
            string result = source;

            result = result.Replace("\t","");
            result = result.Replace("\r", "");
            result = result.Replace("\n", "");
            result = result.Replace(" ", "");
            return result;
        }

        public static string UniqueString()
        {
            Guid guid = Guid.NewGuid();

            String result = guid.ToString();

            return result.Replace("-", "");
        }

        public static void WriteStringToFileSafe(string source, string filename)
        {
            try
            {
                FileInfo fi = new FileInfo(filename);
                StreamWriter writer = fi.CreateText();
                writer.Write(source);
                writer.Close();
            }
            catch
            {

            }
        }

        public static void WriteStringToFile(string source, string filename)
        {
            FileInfo fi = new FileInfo(filename);
            StreamWriter writer = fi.CreateText();
            writer.Write(source);
            writer.Close();
        }

        public static string PadString(string source, int Length, int tabs)
        {
            string result = source;

            try
            {
                if (source.Length > Length)
                {
                    result = source.Substring(0, Length - 3) + "...";
                }

                while (result.Length < Length)
                {
                    result += " ";
                }

                for (int i = 0; i < tabs; i++)
                {
                    result += "\t";
                }
            }
            catch { }

            return result;
        }

        public static bool ConvertToBool(string source, bool Default)
        {
            bool result = Default;

            try
            {
                bool.TryParse(source, out result);
            }
            catch
            {}
            return result;
        }

        public static int ConvertToInt(string source, int Default)
        {
            int result = Default;

            try
            {
                int.TryParse(source, out result);
            }
            catch
            { }
            return result;
        }

        public static Decimal ConvertToBool(string source, Decimal Default)
        {
            Decimal result = Default;

            try
            {
                decimal.TryParse(source, out result);
            }
            catch
            { }
            return result;
        }

        public static string StringToAttributeSafeString(string source)
        {
            string result = source;
            try
            {

                result = result.Replace("-", "HYPHEN");
                result = result.Replace(" ", "SPC");
                result = result.Replace(" ", "SPACE");
                result = result.Replace("'", "APOSTROPHE");
                result = result.Replace("&", "ASAND");
                result = result.Replace(",", "COMMA");
                result = result.Replace(".", "PERIOD");
                result = result.Replace("/", "FSLASH");
                result = result.Replace("\\", "BSLASH");
                result = result.Replace("#", "HASH");
                result = result.Replace("$", "BUCK");
                result = result.Replace("�", "QUID");
                result = result.Replace(":", "COLON");
                result = result.Replace("%", "PCENT");
                result = result.Replace("_", "USCR");
                result = result.Replace("@", "ATSIGN");

                result = result.Replace("<", "LTHAN");
                result = result.Replace(">", "GTHAN");
                result = result.Replace("(", "OPBR");
                result = result.Replace(")", "CLBR");
            }
            catch { }

            return result;
        }

        public static string AttributeSafeStringToString(string source)
        {
            string result = source;
            try
            {
                result = result.Replace("HYPHEN", "-");
                result = result.Replace("SPC", " ");
                result = result.Replace("SPACE", " ");
                result = result.Replace("APOSTROPHE", "'");
                result = result.Replace("ASAND", "&");
                result = result.Replace("COMMA", ",");
                result = result.Replace("PERIOD", ".");
                result = result.Replace("BSLASH", "\\");
                result = result.Replace("FSLASH", "/");
                result = result.Replace("HASH", "#");
                result = result.Replace("BUCK", "$");
                result = result.Replace("QUID", "�");
                result = result.Replace("COLON", ":");
                result = result.Replace("PCENT", "%");
                result = result.Replace("USCR", "_");
                result = result.Replace("ATSIGN", "@");

                result = result.Replace("LTHAN", "<");
                result = result.Replace("GTHAN", ">");
                result = result.Replace("OPBR", "(");
                result = result.Replace("CLBR", ")");
            }
            catch { }

            return result;
        }

        public static string RemoveFileCharacters(string source)
        {
            string result = source;

            result = result.Replace("\\", "-");
            result = result.Replace("/", "-");
            result = result.Replace(".", "_");
            result = result.Replace(":", "-");
            result = result.Replace("?", "-");
            result = result.Replace("\"", "-");
            result = result.Replace("<", "-");
            result = result.Replace(">", "-");
            result = result.Replace("|", "-");

            return result;
        }

        public static string Capitalise(string source, int type)
        {
            string result = source;

            if (type == 1)
            {
                CultureInfo cin = new CultureInfo("en-GB", true);
                TextInfo tin = cin.TextInfo;

                result = tin.ToTitleCase(source);
            }
            else if (type == 2)
            {
                // All uppercase
                result = source.ToUpper();
            }
            return result;
        }

        public static string GetObjectValue(object source)
        {
            string result = "";

            if (source != null)
            {
                try
                {
                    result = source.ToString();
                }
                catch
                { }
            }
            return result;
        }


    }
}
