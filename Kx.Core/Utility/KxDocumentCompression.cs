using System;
using System.IO;
using System.Text;
using ICSharpCode.SharpZipLib;
using ICSharpCode.SharpZipLib.Checksum;
using ICSharpCode.SharpZipLib.Zip;



namespace Kx.Core.Utility
{
    public class KxDocumentCompression
    {
        private static byte[] DeCompress(byte[] bytesToDecompress)
        {
            byte[] writeData = new byte[4096];

            ZipInputStream s2 = new ZipInputStream(new MemoryStream(bytesToDecompress));

            ZipEntry entry = s2.GetNextEntry();

            if (entry != null) // Is this a valid zip??
            {
                MemoryStream outStream = new MemoryStream();

                while (true)
                {
                    int size = s2.Read(writeData, 0, writeData.Length);

                    if (size > 0)
                    {
                        outStream.Write(writeData, 0, size);
                    }
                    else
                    { break; }
                }

                s2.Close();
                byte[] outArr = outStream.ToArray();
                outStream.Close();
                return outArr;
            }
            else
                return bytesToDecompress;
        }

        private static byte[] Compress(byte[] bytesToCompress)
        {
            MemoryStream ms = new MemoryStream();

            ZipOutputStream s = new ZipOutputStream(ms);

            s.Write(bytesToCompress,0, bytesToCompress.Length);
            s.Close();

            return  ms.ToArray();
        }


        public static byte[] CompressDocument(byte[] Source)
        {
            return Compress(Source);
        }
        /// <summary>
        /// This method takes a document and decompresses it (if necessary)
        /// </summary>
        /// <param name="Source"> The byte array containing the source document</param>

        public static byte[] DecompressDocument(byte[] Source)
        {
            UTF8Encoding ae = new UTF8Encoding();
            string test = ae.GetString(Source);

            if (test.ToUpper().IndexOf("PK") == 0)
            {
                return DeCompress(Source);
            }
            else
            {
                return Source;
            }
        }

        public static byte[] CompressBytes(byte[] input, string filename)
        {
            using (MemoryStream buf = new MemoryStream())
            using (ZipOutputStream zip = new ZipOutputStream(buf))
            {
                Crc32 crc = new Crc32();
                zip.SetLevel(9);

                ZipEntry entry = new ZipEntry(filename);
                entry.DateTime = DateTime.Now;
                entry.Size = input.Length;

                crc.Reset();
                crc.Update(input);

                entry.Crc = crc.Value;

                zip.PutNextEntry(entry);
                zip.Write(input, 0, input.Length);
                zip.Finish();

                byte[] c = new byte[buf.Length];
                buf.Seek(0, SeekOrigin.Begin);
                buf.Read(c, 0, c.Length);

                zip.Close();

                return c;
            }
        }
    }
}
