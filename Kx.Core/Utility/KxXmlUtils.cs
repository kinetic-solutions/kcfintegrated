using System;
using System.Drawing;
using System.IO;
using System.Xml;
using System.Data.SqlTypes;

namespace Kx.Core.Utility
{
	public class KxXmlUtils
	{
		public static XmlDocument CreateXMLDoc()
		{
			return new XmlDocument();
		}

		public static XmlDocument CreateXMLDoc(string sRootNode )
		{
			XmlDocument newXML = new XmlDocument();
			newXML.CreateElement(sRootNode);
			return newXML;
		}

		public static XmlDocument CreateAndLoadXMLDoc( string sSourceXML)
		{
			XmlDocument newXML = new XmlDocument();
			newXML.Load(sSourceXML);
			return newXML;
		}	

		public static XmlNode CreateNode( XmlDocument ADocument,XmlNode ANode, string AElement)
		{
			return CreateNode(ADocument,ANode,AElement,"");
		}

		public static XmlNode CreateNode( XmlDocument ADocument,XmlNode ANode, string AElement,string sValue)
		{
			XmlNode newNode;
			newNode = ADocument.CreateElement(AElement);
			if (ANode != null)
			{
				ANode.AppendChild(newNode);
			}
			else
			{
				ADocument.AppendChild(newNode);
			}

			newNode.InnerText =  sValue;
			return newNode;
		}

		public static XmlNode CreateCDATANode( XmlDocument ADocument,XmlNode ANode, string AElement)
		{
			return CreateCDATANode(ADocument,ANode, AElement,"");
		}

		public static XmlNode CreateCDATANode( XmlDocument ADocument,XmlNode ANode, string AElement,string sValue)
		{
			XmlNode newNode;
			newNode = ADocument.CreateNode(XmlNodeType.CDATA,AElement,sValue);
			return newNode;
		}

		public static XmlNode CreateOrFindNode(XmlDocument ADocument,XmlNode ANode, string AElement)
		{
			return CreateOrFindNode(ADocument,ANode, AElement,"");
		}

		public static XmlNode CreateOrFindNode(XmlDocument ADocument,XmlNode ANode, string AElement, string sValue)
		{
			XmlNode newNode;
			newNode = ANode.SelectSingleNode(AElement);

			if (newNode ==  null)
			{
				newNode = ADocument.CreateNode(XmlNodeType.Element,AElement,sValue);
			}
			return newNode;
		}

		public static string GetNodeValue(XmlNode ANode, string sNodeName, string sDefaultValue)
		{
			bool decode = true;

			return GetNodeValue(ANode, sNodeName, sDefaultValue, decode,true);
		}

        public static string GetNormalCaseNodeValue(XmlNode ANode, string sNodeName, string sDefaultValue)
        {
            bool decode = true;

            return GetNodeValue(ANode, sNodeName, sDefaultValue, decode,false);
        }

        public static string GetNodeValue(XmlNode ANode, string sNodeName, string sDefaultValue, bool decode)            
        {
            return GetNodeValue(ANode, sNodeName, sDefaultValue, decode, false);
        }


        public static string GetNodeValue(XmlNode ANode, string sNodeName, string sDefaultValue, bool decode, bool forceLowercase)
		{
			XmlNode oNode;

            string nodename = sNodeName;

            if (forceLowercase)
            {
                nodename = sNodeName.ToLower();
            }
            
			if (ANode != null)
			{
                oNode = ANode.SelectSingleNode(nodename);
			
				if (oNode != null)
				{
                    if (KxBase64.IsB64Encoded(oNode.InnerText) && decode)
                    {
                        return KxBase64.Base64Decode(oNode.InnerText);
                    }
                    else
                    {
                        return oNode.InnerText;
                    }
				}
				else
				{
					return sDefaultValue;
				}
			}
			else
				return sDefaultValue;
		}

     

        public static Guid GetNodeValue(XmlNode ANode, string sNodeName, Guid sDefaultValue)
        {
            string Temp = GetNodeValue(ANode, sNodeName, "");
            Guid Result = sDefaultValue;

            if (Temp != "")
            {
                Result = new Guid(Temp);
            }
            return Result;
        }

        public static byte[] GetNodeValueBA(XmlNode ANode, string sNodeName, byte[] sDefaultValue)
        {
            string Temp = GetNodeValue(ANode, sNodeName, "",false,true);

            try
            {
                return KxDocumentCompression.DecompressDocument(Kx.Core.Utility.KxBase64.Base64DecodeToByte(Temp));
            }
            catch
            {
                return sDefaultValue;
            }
        }


        public static long GetNodeValue(XmlNode ANode, string sNodeName, long sDefaultValue)
        {
            try
            {
                string temp = GetNodeValue(ANode, sNodeName, System.Convert.ToString(sDefaultValue));
                if (temp != "")
                {
                    return System.Convert.ToInt64(temp);
                }
                else
                {
                    return sDefaultValue;
                }
            }
            catch
            {
                return sDefaultValue;
            }
        }

        public static int GetNodeValueBoolInt(XmlNode ANode, string sNodeName, int sDefaultValue)
        {
            int result = sDefaultValue;

            try
            {
                string strtemp = GetNodeValue(ANode, sNodeName, "");

                try
                {
                    result = System.Convert.ToInt32(strtemp);
                }
                catch
                {
                    try
                    {
                        bool temp = Boolean.Parse(strtemp);

                        result = temp ? 1 : 0;
                    }
                    catch
                    {

                    }
                }
                return result;
            }
            catch
            {
                return result;
            }
        }

		public static int GetNodeValue(XmlNode ANode,string sNodeName,int sDefaultValue)
		{
			try
			{
                string temp = GetNodeValue(ANode, sNodeName, System.Convert.ToString(sDefaultValue));

                if (temp != "")
                {
                    return System.Convert.ToInt32(temp);
                }
                else
                {
                    return sDefaultValue;
                }
			}
			catch
			{
				return sDefaultValue;
			}
		}

        public static short GetNodeValue(XmlNode ANode, string sNodeName, short sDefaultValue)
        {
            try
            {
                string temp = GetNodeValue(ANode, sNodeName, System.Convert.ToString(sDefaultValue));

                if (temp != "")
                {
                    return System.Convert.ToInt16(temp);
                }
                else
                {
                    return sDefaultValue;
                }
            }
            catch
            {
                return sDefaultValue;
            }
        }


        public static decimal GetNodeValue(XmlNode ANode, string sNodeName, decimal sDefaultValue)
        {
            try
            {
                string temp = GetNodeValue(ANode, sNodeName, System.Convert.ToString(sDefaultValue));
                if (temp != "")
                {
                    return System.Convert.ToDecimal(temp);
                }
                else
                {
                    return sDefaultValue;
                }
            }
            catch 
            {
                return sDefaultValue;
            }
        }

        public static decimal GetNormalCaseNodeValue(XmlNode ANode, string sNodeName, decimal sDefaultValue)
        {
            try
            {
                string temp = GetNodeValue(ANode, sNodeName, System.Convert.ToString(sDefaultValue),false,false);
                if (temp != "")
                {
                    return System.Convert.ToDecimal(temp);
                }
                else
                {
                    return sDefaultValue;
                }
            }
            catch
            {
                return sDefaultValue;
            }
        }


        public static Image GetNodeValue(XmlNode ANode, string sNodeName, Image sDefaultValue)
        {
            Image result = sDefaultValue;
            try
            {
                string temp = GetNodeValue(ANode, sNodeName, "", false,true);

                if (temp != "")
                {
                    byte[] buffer = KxBase64.Base64DecodeToByte(temp);

                    MemoryStream stream = new MemoryStream(buffer);
                    result = Image.FromStream(stream);
                    stream.Dispose();
                    stream = null;
                }
            }
            catch
            {
                return sDefaultValue;
            }
            return result;
        }


		public static bool GetNodeValue(XmlNode ANode,string sNodeName,bool sDefaultValue)
		{
			string result = GetNodeValue(ANode,sNodeName,"", false);

            if (result != "")
            {
                try
                {
                    try
                    {
                        if (result.ToUpper() == "FALSE" || result.ToUpper() == "TRUE")
                        {
                            return Boolean.Parse(result);
                        }
                        else
                        {
                            return Convert.ToBoolean(System.Convert.ToInt32(result));
                        }
                    }
                    catch
                    {
                        return Boolean.Parse(result);
                    }
                }
                catch
                {
                    return sDefaultValue;
                }
            }
            else
            {
                return sDefaultValue;
            }
		}
		public static bool GetNormalCaseNodeValue(XmlNode ANode,string sNodeName,bool sDefaultValue)
		{
			string result = GetNodeValue(ANode,sNodeName,"", false, false);

            if (result != "")
            {
                try
                {
                    try
                    {
                        if (result.ToUpper() == "FALSE" || result.ToUpper() == "TRUE")
                        {
                            return Boolean.Parse(result);
                        }
                        else
                        {
                            return Convert.ToBoolean(System.Convert.ToInt32(result));
                        }
                    }
                    catch
                    {
                        return Boolean.Parse(result);
                    }
                }
                catch
                {
                    return sDefaultValue;
                }
            }
            else
            {
                return sDefaultValue;
            }
		}

		public static double GetNodeValue(XmlNode ANode,string sNodeName,double sDefaultValue)
		{
            try
            {
                string temp = GetNodeValue(ANode, sNodeName, System.Convert.ToString(sDefaultValue));
                if (temp != "")
                {
                    return System.Convert.ToDouble(temp);
                }
                else
                {
                    return sDefaultValue;
                }
            }
            catch
            {
                return sDefaultValue;
            }
		}

		public static DateTime? GetNodeValueDT(XmlNode ANode,string sNodeName,DateTime ?sDefaultValue)
		{
			//return System.Convert.ToSQLDateTime(GetNodeValue(ANode,sNodeName,System.Convert.ToString(sDefaultValue)));
			try
			{
                string temp = GetNodeValue(ANode, sNodeName, System.Convert.ToString(sDefaultValue));
                if (temp != "")
                {
                    return DateTime.Parse(temp);
                }
                else
                {
                    return sDefaultValue;
                }
			}
			catch
			{
				return sDefaultValue;
			}
		}

		public static bool SetNodeValue(XmlNode ANode,string sNodeName, string sValue)
		{
			XmlNode oNode = ANode.SelectSingleNode(sNodeName);
			if (oNode != null)
			{
				oNode.InnerText = sValue;
				return true;
			}
			else
			{
				CreateNode(ANode.OwnerDocument,ANode,sNodeName,sValue);
				return false;
			}
		}

		public static bool SetNodeValue(XmlNode ANode,string sNodeName, int sValue)
		{
			return SetNodeValue(ANode,sNodeName,System.Convert.ToString(sValue));
		}

		public static bool SetNodeValue(XmlNode ANode,string sNodeName, double sValue)
		{
			return SetNodeValue(ANode,sNodeName,System.Convert.ToString(sValue));
		}

		public static bool SetNodeValue(XmlNode ANode,string sNodeName, DateTime sValue)
		{
			return SetNodeValue(ANode,sNodeName,System.Convert.ToString(sValue));
		}

		public static string getNodeAttr( XmlNode ANode, string sAttr,string sDefaultValue)
		{
			string locResult = sDefaultValue;
			foreach (XmlAttribute attrib in ANode.Attributes)
			{
				if (attrib.Name == sAttr)
				{
					locResult = attrib.InnerText;
					break;
				}
			}
			return locResult;
		}

		public static XmlDocument GetFirstChildXML( XmlDocument ADoc)
		{
			return GetFirstChildXML(ADoc,1);
		}

		public static XmlDocument GetFirstChildXML( XmlDocument ADoc,int iChildLevel)
		{
			string sChildSearch = "/*";
			XmlNode oFirstChild = null;

			for (int i = 1;i <=5; i++)
			{
				sChildSearch = sChildSearch + "/*";
				oFirstChild = ADoc.SelectSingleNode(sChildSearch+"[1]");

				if (oFirstChild != null) 
				{
					break;
				}
			}

			if (oFirstChild != null)
			{
				return CreateAndLoadXMLDoc(oFirstChild.InnerXml);
			}
			else
			{
				return ADoc;
			}
		}

		public static int CountNodes( XmlNode oXML,string sSrch)
		{
			XmlNodeList oNodeList;
			oNodeList = oXML.SelectNodes(sSrch);
			return oNodeList.Count;
		}
	}
}
