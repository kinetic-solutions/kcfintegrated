using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Data;


namespace Kx.Core.Utility
{
    
    public class KxResource
    {

        public static string GetResourceById(string id,string ApplicationId)
        {
            string result = "";
            try
            {
                KxData data = new KxData("resource", "WebResources", TargetType.Table, ActionType.Select);

                data.KeyFields.Add(new KxDataValue("applicationId", ApplicationId, KxValueType.String));
                data.KeyFields.Add(new KxDataValue("identifier", id, KxValueType.String));

                if (data.Execute())
                {
                    XmlDocument doc = new XmlDocument();
                    doc.LoadXml(data.XmlResults);

                    XmlNode node = doc.SelectSingleNode("//resource/datarow");

                    result = Translate(KxXmlUtils.GetNodeValue(node, "data", ""));
                }
            }
            catch{}
            return result;
        }

        public static string GetResourceById(string id)
        {
            string result = "";
            try
            {
                KxData data = new KxData("resource", "WebResources", TargetType.Table, ActionType.Select);

                data.KeyFields.Add(new KxDataValue("identifier", id, KxValueType.String));

                if (data.Execute())
                {
                    
                        XmlDocument doc = new XmlDocument();
                        doc.LoadXml(data.XmlResults);

                        XmlNode node = doc.SelectSingleNode("//resource/datarow");

                        result = Translate(KxXmlUtils.GetNodeValue(node, "data", ""));
                }
            }
            catch { }
            return result;
        }

        public static XmlDocument GetResourcesByType(string resourceType)
        {
            XmlDocument doc = null;
            try
            {
                KxData data = new KxData("resources", "WebResources", TargetType.Table, ActionType.Select);

                data.KeyFields.Add(new KxDataValue("ResourceType", resourceType));

                if (data.Execute())
                {
                    doc = new XmlDocument();
                    doc.LoadXml(data.XmlResults);
                }
            }
            catch { }
            return doc;
        }

        public static string Translate(string source)
        {
            try
            {
                return KxBase64.Base64Decode(source);
            }
            catch
            {
                return "";
            }
        }
    }
}
