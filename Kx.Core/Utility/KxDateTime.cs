using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using Kx.Core.Configuration;

namespace Kx.Core.Utility
{
    public class KxDateTime
    {

        public static DateTime NullDate
        {
            get
            {
                return new DateTime(1900, 1, 1, 0, 0, 0, 0);
            }
        }

        public static DateTime KxNullDate
        {
            get
            {
                return new DateTime(1899, 12, 30, 0, 0, 0, 0);
            }
        }

        public static DateTime KxApplicableDate
        {
            get
            {
                return new DateTime(1980, 01, 01, 0, 0, 0, 0);
            }
        }

        public static object KxSqlCompliantDate(object value)
        {
            object result = null;
            DateTime validDatetime;

            try
            {
                validDatetime = Convert.ToDateTime(value);

                if (validDatetime > DateTime.Parse("1753/01/01") && validDatetime < DateTime.Parse("9999/12/31"))
                {
                    result = validDatetime;
                }
            }
            catch
            {
            }
            return result;
        }

        public static bool IsNull(DateTime dte)
        {
            return dte == NullDate;
        }

        public static bool IsKxNull(DateTime dte)
        {
            return dte == KxNullDate;
        }

        public static DateTime Parse(string value)
        {
            try
            {
                IFormatProvider culture = new CultureInfo(KxDbConfiguration.Culture, true);
                return DateTime.Parse(value, culture);
            }
            catch
            {
                try
                {
                    IFormatProvider culture = new CultureInfo("en-GB", true);
                    return DateTime.Parse(value, culture);
                }
                catch
                {
                    IFormatProvider culture = new CultureInfo("en-US", true);
                    return DateTime.Parse(value, culture);
                }

            }
        }


        public static string ToString(DateTime value)
        {
            try
            {
                IFormatProvider culture = new CultureInfo(KxDbConfiguration.Culture, true);
                return value.ToString(culture);
            }
            catch
            {
                return "";
            }
        }

        public static string ToString(DateTime? value, string format)
        {
            string result = "";

            try
            {
                if (value != null)
                {
                    IFormatProvider culture = new CultureInfo(KxDbConfiguration.Culture, true);
                    result = value.Value.ToString(format, culture);
                }
            }
            catch
            {
            }
            return result;
        }

        public static DateTime EasterSunday(int year)
        {
            int month = 0;
            int day = 0;

            int g = year % 19;
            int c = year / 100;
            int h = h = (c - (int)(c / 4) - (int)((8 * c + 13) / 25) + 19 * g + 15) % 30;
            int i = h - (int)(h / 28) * (1 - (int)(h / 28) * (int)(29 / (h + 1)) * (int)((21 - g) / 11));

            day = i - ((year + (int)(year / 4) + i + 2 - c + (int)(c / 4)) % 7) + 28;

            month = 3;

            if (day > 31)
            {
                month++;
                day -= 31;
            }
            return new DateTime(year, month, day);
        }

        public static int CalculateAge(DateTime? dob)
        {
            return CalculateAge(dob, DateTime.Now);
        }

        public static int CalculateAge(DateTime? dob, DateTime eventDate)
        {
            int result = 0;

            if (dob != null)
            {
                try
                {
                    DateTime birthDate = dob.Value;

                    int years = eventDate.Year - birthDate.Year;

                    if (eventDate.Month < birthDate.Month || (eventDate.Month == birthDate.Month && eventDate.Day < birthDate.Day))
                    {
                        --years;
                    }
                    result = years;
                }
                catch
                { }
            }
            return result;
        }

        public static string HourToString(int hour)
        {
            return HourAndMinuteToString(hour, 0);
        }

        public static string HourAndMinuteToString(int hour, int minute)
        {
            return hour.ToString().PadLeft(2, '0') + ":" + minute.ToString().PadLeft(2, '0');
        }
        public static DateTime? WpmDateToDate(string wpmDate)
        {
            DateTime? result = null;

            if (!String.IsNullOrEmpty(wpmDate))
            {
                CultureInfo provider = CultureInfo.InvariantCulture;

                result = DateTime.ParseExact(wpmDate, "yyyy-MM-dd", provider);
            }

            return result;
        }

        public static string DateToWpmDate(DateTime? wpmDate)
        {
            string result = "";

            if (wpmDate != null)
            {
                result = wpmDate.Value.ToString("yyyy-MM-dd");
            }

            return result;

        }
    }
}
