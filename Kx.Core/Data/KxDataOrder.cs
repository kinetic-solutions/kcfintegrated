using System;
using System.Collections.Generic;
using System.Text;

namespace Kx.Core.Data
{
    public enum KxDataOrderType {Ascending,Descending }

    public class KxDataOrder
    {
        private KxDataOrderType orderType = KxDataOrderType.Ascending;
        private string fieldName;

        public KxDataOrderType OrderType
        {
            get
            { return orderType; }
            set
            { orderType = value; }
        }

        public string FieldName
        {
            get
            { return fieldName; }
            set
            { fieldName = value; }
        }

        public KxDataOrder(string FieldName)
        {
            this.FieldName = FieldName;
        }

        public KxDataOrder(string FieldName,KxDataOrderType orderType)
        {
            this.FieldName = FieldName;
            this.OrderType = orderType;
        }



    }
}
