using System;
using System.Collections.Generic;
using System.Text;
using Kx.Core.Resources;
using Kx.Core.Configuration;
using System.Web;

namespace Kx.Core.Data
{
    public class KxDataDynamics
    {
        // TODO: Remove link to HTTP CONTEXT
        // TODO: Remove this method if it isn't used.
        public static string SqlStatement(string SqlName)
        {
            KxResourceProvider SqlProvider = null;

            try
            {
                SqlProvider = (KxResourceProvider)HttpContext.Current.Application["ResourceProvider"];
            }
            catch
            {}

            try
            {
                if (SqlProvider == null)
                {
                    SqlProvider = (KxResourceProvider)HttpContext.Current.Session["ResourceProvider"];
                }
            }
            catch
            { }
             
            if (SqlProvider == null)
            {
                SqlProvider = new KxResourceProvider();
            }

            return SqlProvider.GetResource(SqlName);
        }

        public static string MakeSql(KxData data)
        {
            string Sql = "";

            StringBuilder fields = new StringBuilder();
            StringBuilder values = new StringBuilder();
            StringBuilder update = new StringBuilder();
            StringBuilder where = new StringBuilder();

            //pha
            if (data.TargetName == System.Configuration.ConfigurationManager.AppSettings["DebugDataTarget"])
                System.Diagnostics.Debugger.Break();

            if (data.AutoKeyField != null)
            {
                where.Append(data.AutoKeyField.Name + " = @" + data.AutoKeyField.Name);
            }
            else
            {
                if (data.KeyFields.Count > 0)
                {
                    foreach (KxDataValue value in data.KeyFields)
                    {
                        if (value.Value == DBNull.Value)
                        {
                            where.Append(string.Format(" AND {0} IS {1}NULL", value.Name, value is KxNEDataValue ? "NOT " : string.Empty));
                        }
                        else if (value is KxBETWEENDataValue)
                        {
                            where.Append(string.Format(" AND {0} BETWEEN @{0}1 AND @{0}2", value.Name));
                        }
                        else
                        {
                            where.Append(string.Format(" AND {0} {1} @{0}", value.Name, value is KxNEDataValue ? "<>" : "="));
                        }
                    }
                    where = where.Remove(0, 4);
                }
            }

            foreach (KxDataValue value in data.Parameters)
            {
                fields.Append("," + value.Name);
                values.Append(",@" + value.Name);
                update.Append(", " + value.Name + " = @" + value.Name);
            }

            if (data.Action == ActionType.ScalarCount)
            {
                Sql = "Select count(1) FROM " + data.TargetName;

                if (where.Length > 0)
                {
                    Sql += " WHERE " + where;
                }
            }
            else if (data.Action == ActionType.Select)
            {
                Sql = "Select * FROM " + data.TargetName;

                if (where.Length > 0)
                {
                    Sql += " WHERE " + where;
                }
                Sql += BuildOrderBy(data);
            }
            else if (data.Action == ActionType.Scalar)
            {
                Sql = "Select MAX("+data.ScalarField+") FROM " + data.TargetName;
                
                if (where.Length > 0)
                {
                    Sql += " WHERE " + where;
                }
            }
            else if (data.Action == ActionType.Delete)
            {
                Sql = "DELETE FROM " + data.TargetName + " WHERE " + where;
            }
            else if (data.Action == ActionType.Insert)
            {
                foreach (KxDataValue value in data.KeyFields)
                {
                    fields.Append("," + value.Name);
                    values.Append(",@" + value.Name);
                }
                if ((fields.Length > 0) && (values.Length > 0))
                {
                    Sql = "INSERT INTO " + data.TargetName + " ( " + fields.ToString().Remove(0, 1) + " ) VALUES ( " + values.ToString().Remove(0, 1) + " )";

                    if (data.AutoKeyField != null && Convert.ToInt32(data.AutoKeyField.Value) == 0)
                    {
                        Sql += "; select @@IDENTITY;";
                    }
                }
                else
                {
                    throw new System.Exception("No Fields or values to insert.");
                }
            }
            else if (data.Action == ActionType.Update)
            {
                if (update.Length > 0)
                {
                    Sql = "UPDATE " + data.TargetName + " SET " + update.ToString().Remove(0, 1) + " WHERE " + where;
                }
                else
                {
                    throw new System.Exception("No parameters to update.");
                }
            }
            return Sql;
        }


        static string BuildOrderBy(KxData data)
        {
            string result = "";

            if (data.OrderFields.Count > 0)
            {
                result += " ORDER BY ";

                foreach (KxDataOrder order in data.OrderFields)
                {
                    if (order.OrderType == KxDataOrderType.Ascending)
                    {
                        result += order.FieldName+" ASC,";
                    }
                    else
                    {
                        result += order.FieldName + " DESC,";
                    }
                }

                result = result.TrimEnd(',');

            }

            return result;
        }
    }
}
