using System;
using System.Collections.Generic;
using System.Text;

namespace Kx.Core.Data
{
    public enum KxValueType { Short, Int, Long, Boolean, String, DateTime, Image,Decimal,Float,Guid,Text,Unknown }

    public class KxDataValue
    {
        #region Attributes & Properties
        private KxValueType valueType;
        private string name;
        private object value;
        private int fieldSize = 0;
        private bool nullable = true;

        public KxValueType ValueType
        {
            get
            { return valueType;}
            set
            { valueType = value;}
        }

        public string Name
        {
            get
            { return name; }
            set
            { name = value; }
        }

        public object Value
        {
            get
            { return value; }
            set
            { this.value = value; }
        }

        public int FieldSize
        {
            get
            { return fieldSize; }
            set
            { fieldSize = value; }
        }

        public bool Nullable
        {
            get
            { return nullable; }
            set
            { nullable = value; }
        }
        
        #endregion

        #region Constructors

        public KxDataValue(string name, object value)
        {
            this.name = name;
            this.value = value;
        }

        public KxDataValue(string name, object value,KxValueType dataType) : this(name,value)
        {
            valueType = dataType;
        }

        public KxDataValue(string name, object value, KxValueType dataType, bool allowsNull) : this(name, value, dataType)
        {
            nullable = allowsNull;
        }

        public KxDataValue(string name, object value, KxValueType dataType, int size) : this(name, value, dataType)
        {
            fieldSize = size;
        }

        public KxDataValue(string name, object value, KxValueType dataType, int size, bool allowsNull) : this(name, value, dataType, size)
        {
            nullable = allowsNull;
        }
        
        #endregion

        #region Methods

        public string ToXml()
        {
            return "               <" + name.ToLower() + ">" + value.ToString() + "</" + name.ToLower() + ">";
        }

        #endregion
    }
}
