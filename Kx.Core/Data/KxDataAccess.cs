using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Kx.Core.Utility;

namespace Kx.Core.Data
{
    public class KxDataAccess
    {
        public static string ConnectionString
        {
            get
            {
                return KxConnectionProvider.ConnectionProvider.DefaultConnectionString;
            }
        }

        public static int CommandTimeout
        {
            get
            {
                return KxConnectionProvider.ConnectionProvider.CurrentConnection.CommandTimeout;
            }
        }

        public static SqlConnection Connection
        {
            get
            {
                return KxConnectionProvider.ConnectionProvider.DefaultConnection;
            }
        }

        public static string FieldToString(SqlDataReader reader, int idx)
        {
            string fieldname = reader.GetName(idx).ToLower();
            if (reader.IsDBNull(idx))
            {
                return "          <" + fieldname + "><![CDATA[]]></" + fieldname + ">";
            }
            else
            {
                return "          <" + fieldname + "><![CDATA[" + DBObjectToString(reader, idx) + "]]></" + fieldname + ">";
            }
        }

        public static string DBObjectToString(SqlDataReader reader, int idx)
        {
            switch (reader.GetDataTypeName(idx).ToLower())
            {
                case "binary": 
                {
                    return DbBinaryEncode(reader, idx);         
                }
                case "bit"   : return reader.GetBoolean(idx).ToString();
                case "datetime": return reader.GetDateTime(idx).ToString("F");
                case "image":
                {
                    return DbBinaryEncode(reader, idx);                   
                }
                case "smalldatetime" : return reader.GetDateTime(idx).ToString("F");
                case "timestamp" : return reader.GetGuid(idx).ToString();
                case "uniqueidentifier" : return reader.GetGuid(idx).ToString();
                case "varbinary":
                {
                    return DbBinaryEncode(reader, idx);         
                }
                case "xml":
                {
                    return KxBase64.Base64Encode(reader.GetValue(idx).ToString());
                }
                case "text":
                {
                    return KxBase64.Base64Encode(reader.GetValue(idx).ToString());
                }
                case "ntext":
                {
                    return KxBase64.Base64Encode(reader.GetValue(idx).ToString());
                }
                
                default: return reader.GetValue(idx).ToString();
            }
        }

        public static string DbBinaryEncode(SqlDataReader reader, int idx)
        {
            byte[] buffer = new Byte[Convert.ToInt32((reader.GetBytes(idx, 0, null, 0, Int32.MaxValue)))];
            reader.GetBytes(idx, 0, buffer, 0, buffer.Length);
            return KxBase64.Base64Encode(buffer);  
        }
    }
}
