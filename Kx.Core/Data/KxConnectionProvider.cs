using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using Kx.Core.Database;
using System.Configuration;

namespace Kx.Core.Data
{
    public class KxConnectionProvider
    {
        private static KxConnectionProvider _connectionProvider;      
        private Connection _currentConnection;

        public static KxConnectionProvider ConnectionProvider
        {
            get
            {
                if (_connectionProvider == null)
                {
                    _connectionProvider = new KxConnectionProvider();
                }
                return _connectionProvider;
            }
        }

        public string DefaultConnectionString
        {
            get
            {
                if (CurrentConnection.HasConnectionString)
                {
                    return CurrentConnection.ConnectionString;
                }
                else if (CurrentConnection.HasWebConnectionString)
                {
                    return CurrentConnection.WebConnectionString;
                }
                else
                {
                    throw new Exception("No connection settings found");
                }
            }
        }

        public SqlConnection DefaultConnection
        {
            get
            {
                SqlConnection result = new SqlConnection(DefaultConnectionString);
                result.Open();

                return result;
            }
        }

        public string WebConnectionString
        {
            get
            {
                if (CurrentConnection.HasWebConnectionString)
                {
                    return CurrentConnection.WebConnectionString;
                }
                else
                {
                    throw new Exception("No connection settings found");
                }
            }
        }

        public SqlConnection WebConnection
        {
            get
            {
                SqlConnection result = new SqlConnection(WebConnectionString);
                result.Open();

                return result;
            }
        }

        public string KxConnectionString
        {
            get
            {
                if (CurrentConnection.HasConnectionString)
                {
                    return CurrentConnection.ConnectionString;
                }
                else
                {
                    throw new Exception("No connection settings found");
                }
            }
        }

        public SqlConnection KxConnection
        {
            get
            {
                SqlConnection result = new SqlConnection(KxConnectionString);
                result.Open();

                return result;
            }
        }

        public Connection CurrentConnection
        {
            get
            {
                if (_currentConnection == null)
                {
                    InitialiseDefaultConnection();
                }
                return _currentConnection;
            }
            set
            {
                _currentConnection = value;
            }
        }

        /// <summary>
        /// Altered 2021 to pick up 'UseEncryptedConnectionStrings' setting and not encrypt/decrypt each time 
        /// if they're not encrypted. 
        /// </summary>
        private void InitialiseDefaultConnection()
        {
            if (_currentConnection == null)
            {
                string connectionName = ConfigurationManager.AppSettings["ConnectionName"];
                if (string.IsNullOrEmpty(connectionName))
                {
                    ConnectionStringSettings kxConnection = ConfigurationManager.ConnectionStrings["Kx"];
                    ConnectionStringSettings webConnection = ConfigurationManager.ConnectionStrings["Web"];

                    if ((kxConnection != null) || (webConnection != null))
                    {
                        _currentConnection = new Connection();
                        if (ConfigurationManager.AppSettings["UseEncryptedConnectionStrings"] == "false")
                        {
                            _currentConnection = new Connection(true);
                        }
                        
                        if (kxConnection != null && !string.IsNullOrEmpty(kxConnection.ConnectionString))
                        {
                            _currentConnection.ConnectionString = kxConnection.ConnectionString;
                        }
                        if (webConnection != null && !string.IsNullOrEmpty(webConnection.ConnectionString))
                        {
                            _currentConnection.WebConnectionString = webConnection.ConnectionString;
                        }
                    }
                }
                else
                {
                    ConnectionManager connMgr;

                    if (System.Web.HttpContext.Current != null)
                    {
                        connMgr = new ConnectionManager(System.Web.HttpContext.Current.Server.MapPath(@"~\"));
                    }
                    else
                    {
                        connMgr = ServiceLocator.ServiceLocator.ResolveService<ConnectionManager>() ?? new ConnectionManager();
                    }
                    
                    if (connMgr.Connections.Count == 0)
                        throw new Exception("No connection settings found");

                    _currentConnection = connMgr.GetConnection(connectionName);
                }
                int commandTime = 0;
                string commandTimeout = ConfigurationManager.AppSettings["CommandTimeout"];
                if (!(int.TryParse(commandTimeout, out commandTime)))
                {
                    commandTime = 30; 
                };

                if (commandTime < 15)
                {
                    commandTime = 15;
                }

                if (_currentConnection != null)
                {
                    _currentConnection.CommandTimeout = commandTime;
                }
            }
        }
    }
}
