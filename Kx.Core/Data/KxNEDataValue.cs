﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Kx.Core.Data
{
    public class KxNEDataValue : KxDataValue
    {

        public KxNEDataValue(string name, object value)
            : base(name, value)
        {
            
        }
        public KxNEDataValue(string name, object value, KxValueType dataType)
            : base(name, value, dataType)
        {
            
        }
        public KxNEDataValue(string name, object value, KxValueType dataType, bool allowsNull)
            : base(name, value, dataType, allowsNull)
        {
            
        }
        public KxNEDataValue(string name, object value, KxValueType dataType, int size)
            : base(name, value, dataType, size)
        {
            
        }
        public KxNEDataValue(string name, object value, KxValueType dataType, int size, bool allowsNull)
            : base(name, value, dataType, size, allowsNull)
        {
            
        }

    }
}
