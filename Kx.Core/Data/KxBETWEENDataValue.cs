﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Kx.Core.Data
{
    public class KxBETWEENDataValue : KxDataValue
    {
        public object FromValue
        {
            get { return base.Value; }
            set { base.Value = value; }
        }
        public object ToValue { get; set; }

        public KxBETWEENDataValue(string name, object fromValue, object toValue)
            : base(name, fromValue)
        {
            ToValue = toValue;
        }
        
        public KxBETWEENDataValue(string name, object fromValue, object toValue, KxValueType dataType)
            : base(name, fromValue, dataType)
        {
            ToValue = toValue;
        }
        
        public KxBETWEENDataValue(string name, object fromValue, object toValue, KxValueType dataType, bool allowsNull)
            : base(name, fromValue, dataType, allowsNull)
        {
            ToValue = toValue;
        }
        
        public KxBETWEENDataValue(string name, object fromValue, object toValue, KxValueType dataType, int size)
            : base(name, fromValue, dataType, size)
        {
            ToValue = toValue;
        }

        public KxBETWEENDataValue(string name, object fromValue, object toValue, KxValueType dataType, int size, bool allowsNull)
            : base(name, fromValue, dataType, size, allowsNull)
        {
            ToValue = toValue;
        }
    }
}
