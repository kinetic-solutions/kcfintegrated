using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Kx.Core.Base;
using Kx.Core.Exceptions;
using Kx.Core.Utility;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using System.Xml;
using System.Configuration;
using Kx.Core.Configuration;

namespace Kx.Core.Data
{
    public enum ActionType {Insert,Update,Delete,Scalar,Select,ExecuteProcedure, ScalarCount, DirtySelect};
    public enum TargetType {Table, StoredProc,Dynamic,NoTarget,CustomTarget,Static};

    [Serializable]
    public class KxData
    {
        #region Properties & Attributes

        private Dictionary<string, string> _webresources = new Dictionary<string, string>();
        private ActionType action;
        private TargetType target = TargetType.StoredProc;
        private string rootName;
        private string lastError = "";
        private string targetName;
        private string scalarField = "";

        private bool connectionCreated = false;
        
        private int    identifier;
        private string xmlResults;
        private object ScalarResult;

        private SqlConnection connection = null;
        private SqlTransaction transaction = null;

        private KxDataValue autoKeyField;
        private List<KxDataValue> keyFields = new List<KxDataValue>();
        private List<KxDataValue> parameters = new List<KxDataValue>();
        private List<KxDataValue> outputParameters = new List<KxDataValue>();
        private List<KxDataOrder> orderFields = new List<KxDataOrder>();
        // pha
        public bool _norecords = false;
        public string GetSQLFromCommand(SqlCommand cmd, bool storedproc = true)
        {
            string sql = "", sqlsuffix1 = "", sqlsuffix2 = "";
            if (!storedproc)
                sql = "exec sp_executesql N'" + cmd.CommandText + "',";
            else 
                sql += cmd.CommandText + " ";
            foreach (SqlParameter sp in cmd.Parameters)
            {
                if (sp.Direction == ParameterDirection.Input)
                {
                    if (sp.ParameterName.StartsWith("@") == false)
                    {
                        sqlsuffix1 += "@";
                        sqlsuffix2 += "@";
                    }
                    if (sp.DbType == DbType.String || sp.DbType == DbType.Guid || sp.DbType == DbType.DateTime)
                    {
                        if (sp.Size == 0)
                            sqlsuffix1 += sp.ParameterName + " nvarchar(MAX),";
                        else
                            sqlsuffix1 += sp.ParameterName + " nvarchar(" + sp.Size + "),";
                        sqlsuffix2 += sp.ParameterName + "='" + sp.Value + "',";
                    }
                    else if (sp.DbType == DbType.Boolean)
                    {
                        sqlsuffix1 += sp.ParameterName + " bit,";
                        if (sp.Value.ToString().ToLower() == "true")
                            sqlsuffix2 += sp.ParameterName + "=1,";
                        else
                            sqlsuffix2 += sp.ParameterName + "=0,";
                    }
                    else if (sp.DbType == DbType.Int32 || sp.DbType == DbType.Decimal)
                    {
                        sqlsuffix1 += sp.ParameterName + " int,";
                        sqlsuffix2 += sp.ParameterName + "=" + sp.Value + ",";
                    }
                    else
                        System.Diagnostics.Debugger.Break();    //pha
                }
            }
            if (!string.IsNullOrEmpty(sqlsuffix1))
                sqlsuffix1 = sqlsuffix1.Substring(0, sqlsuffix1.Length - 1);
            if (!string.IsNullOrEmpty(sqlsuffix2))
                sqlsuffix2 = sqlsuffix2.Substring(0, sqlsuffix2.Length - 1);
            if (storedproc)
                sqlsuffix1 = "";
            else
                sqlsuffix1 = "N'" + sqlsuffix1 + "',";
            sql += sqlsuffix1 + sqlsuffix2;
            return sql;
        }
        public Object DoSelectScalar(string CommandText)
        {
            SqlCommand cmd = CreateCommand(CommandText, CommandType.Text);

            try
            {
                SqlDataReader reader = cmd.ExecuteReader();
                ScalarResult = ResultsToScalar(reader);
                return ScalarResult;
            }
            catch 
            {
                System.Diagnostics.Debugger.Break();
                return "";
            }
        }

       
        private bool LogDebug(SqlCommand cmd, bool storedproc)    //pha
        {
            try
            {
                if (ConfigurationManager.AppSettings["DebugSQL"] == "True")
                {
                    using (StreamWriter sw = new StreamWriter(@"c:\temp\workflow.xml", true))
                    {
                        string sql = GetSQLFromCommand(cmd, storedproc);
                        if (sql.ToLower().Contains(" webresources ") == false || ConfigurationManager.AppSettings["DebugResources"] == "True")
                        {
                            sw.WriteLine("<SQL>");
                            sw.WriteLine("\t<!--");
                            sw.WriteLine("\t" + sql);
                            sw.WriteLine("\t-->");
                            sw.WriteLine("</SQL>");
                        }
                    }
                    return true;
                }
                return false;
            }
            catch {
                // System.Diagnostics.Debugger.Break();
                return false;
            }
        }

        public void LogDebug(string node = "", string output = "")   //pha
        {
            try
            {
                using (StreamWriter sw = new StreamWriter(@"c:\temp\workflow.xml", true))
                {
                    sw.WriteLine("<" + node + ">");
                    sw.WriteLine("\t" + output);
                    sw.WriteLine("</" + node + ">");
                }
            }
            catch
            {
                //System.Diagnostics.Debugger.Break();
            }
        }
        public ActionType Action
        {
            get
            { return action; }
            set
            { 
                action = value;
            }
        }
        
        public TargetType Target
        {
            get
            { return target; }
            set
            { target = value; }
        }

        public string RootName
        {
            get
            { return rootName; }
            set
            { rootName = value; }
        }

        public string ScalarField
        {
            get
            { return scalarField; }
            set
            { scalarField = value; }
        }

        public string TargetName
        {
            get
            { return targetName; }
            set
            { targetName = value; }
        }

        public int Identifier
        {
            get
            { return identifier; }
            set
            { identifier = value; }
        }
        
        public SqlConnection Connection
        {
            get
            {
                if (connection == null)
                {
                    connection = new SqlConnection(KxDataAccess.ConnectionString);
                    connection.Open();
                    connectionCreated = true;
                }
                return connection;
            }
            set
            {connection = value;}
        }

        public SqlTransaction Transaction { get; set; }

        public KxDataValue AutoKeyField
        {
            get
            { return autoKeyField; }
            set
            { 
                autoKeyField = value;
            }
        }

        public List<KxDataValue> KeyFields
        {
            get
            { return keyFields; }
            set
            { keyFields = value; }
        }

        public List<KxDataValue> Parameters
        {
            get
            {return parameters;}
            set
            {parameters = value;}
        }

        public List<KxDataValue> OutputParameters
        {
            get
            { return outputParameters; }
            set
            { outputParameters = value; }
        }

        public List<KxDataOrder> OrderFields
        {
            get
            { return orderFields; }
            set
            { orderFields = value; }
        }

        public string LastError
        {
            get
            { return lastError; }
        }

        public string XmlResults
        {
            get
            {
                return xmlResults; 
            }
        }

        [Obsolete]// Is not used by Applications and will not support Multi User
        public SqlConnection PreferredConnection
        {
            get
            {
                if (connection != null)
                {
                    if (connection.State != ConnectionState.Open)
                    {
                        connection.Open();
                    }
                    return connection;
                }
                else
                {
                    return KxDataAccess.Connection;
                }
            }
        }

        
        #endregion

        public KxDataValue AddParameter(string name, object value, KxValueType dataType)
        {
            KxDataValue result = new KxDataValue(name, value, dataType);
            parameters.Add(result);
            return result;
        }

        public KxDataValue AddParameter(string name, object value, KxValueType dataType, bool allowsNull)
        {
            KxDataValue result = new KxDataValue(name, value, dataType, allowsNull);
            parameters.Add(result);
            return result;
        }

        public KxDataValue AddParameter(string name, object value, KxValueType dataType, int size)
        {
            KxDataValue result = new KxDataValue(name, value, dataType, size);
            parameters.Add(result);
            return result;
        }

        public KxDataValue AddParameter(string name, object value, KxValueType dataType, int size, bool allowsNull)
        {
            KxDataValue result = new KxDataValue(name, value, dataType, size, allowsNull);
            parameters.Add(result);
            return result;
        }
        
        public KxDataValue AddKeyField(string name, object value, KxValueType dataType)
        {
            KxDataValue result = new KxDataValue(name, value, dataType);
            keyFields.Add(result);
            return result;
        }

        public KxDataValue AddNEKeyField(string name, object value, KxValueType dataType)
        {
            var result = new KxNEDataValue(name, value, dataType);
            keyFields.Add(result);
            return result;
        }

        public KxDataValue AddBETWEENKeyField(string name, object fromValue, object toValue, KxValueType dataType)
        {
            var result = new KxBETWEENDataValue(name, fromValue, toValue, dataType);
            keyFields.Add(result);
            return result;
        }

        public KxDataOrder AddOrderField(string fieldname, KxDataOrderType orderType)
        {
            KxDataOrder result = new KxDataOrder(fieldname, orderType);
            orderFields.Add(result);
            return result;
        }

        #region Constructors

        public KxData()
        {
        }

        public KxData(string rootName,string targetName, TargetType target)
            : this (rootName, targetName, target, ActionType.Select)
        {
        }

        public KxData(string rootName, string targetName, TargetType target, ActionType action)
        {
            this.rootName = rootName;
            this.targetName = targetName;
            this.target = target;
            this.action = action;
        }

        #endregion

        public void Initialise()
        {
            if (this.Action == ActionType.DirtySelect)
            {
                Transaction = Connection.BeginTransaction(IsolationLevel.ReadUncommitted);
            }
            else
            {
                Transaction = Connection.BeginTransaction();
            }
        }

        public void Finalise()
        {
            Transaction.Commit();
            Transaction = null;
            Connection.Close();
        }

        public void Cancel()
        {
            Transaction.Rollback();
            Transaction = null;
                ;
        }

        public void ExecuteAuditLog(KxBaseClass sender)
        {
            try
            {
                if (!string.IsNullOrWhiteSpace(sender.AuditDetails))
                {
                    try
                    {
                        ExecuteAuditLog(sender.GetType().Name, sender.GetId(), sender.AuditDetails);                       
                    }
                    finally
                    {
                        //sender.ResetAudit();
                    }
                }
            }
            catch
            { 
            }
        }

        /// <summary>
        /// Logs web audit entries to database if app setting NoDBLogging != true, logs to AppAI if AppInsightConnectionString is set. 
        /// </summary>
        /// <param name="ClassName"></param>
        /// <param name="Identifier"></param>
        /// <param name="AuditDetails"></param>
        public void ExecuteAuditLog(string ClassName,string Identifier,string AuditDetails)
        {
            // pha
            //if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["AppInsightConnectionString"]))
            //{
            //    KxLogger.LogAuditToAI(ClassName, Identifier, AuditDetails);
            //}
            //if (ConfigurationManager.AppSettings["NoDBLogging"] != "true")
            //{
            //    try
            //    {
            //        SqlConnection tempConnection = new SqlConnection(KxDataAccess.ConnectionString);
            //        SqlCommand cmd = new SqlCommand();
            //        cmd.CommandText = "KxWebAuditInsert";
            //        cmd.CommandType = CommandType.StoredProcedure;
            //        cmd.Connection = tempConnection;
            //        cmd.Parameters.Add(new SqlParameter("@ClassName", ClassName));
            //        cmd.Parameters.Add(new SqlParameter("@Identifier", Identifier));
            //        cmd.Parameters.Add(new SqlParameter("@AuditText", AuditDetails));
            //        try
            //        {
            //            tempConnection.Open();
            //            cmd.ExecuteNonQuery();
            //        }
            //        finally
            //        {
            //            tempConnection.Close();
            //        }
            //    }
            //    catch
            //    {
            //    }
            //}
        }

        public bool Execute(KxData master)
        {
            Connection = master.Connection;
            Transaction = master.Transaction;
            return Execute();
        }

        public bool Execute()
        {
            bool Result = true;
            _norecords = false;// pha
            try
            {
                Result = CommandExecute();

                CloseConnection();

                return Result;
            }
            catch (Exception ex)
            {
                lastError = ex.Message;
                KxLogger.LogActivity(this, "Execute", LastError, targetName, true);
                CloseConnection();
                return Result;
            }
        }
        
        public object ExecuteScalar()
        {
            action = ActionType.Scalar;
            bool noRecords = false; // pha
            CommandExecute();
            
            return ScalarResult;
        }

        public int ExecuteScalarCount()
        {
            action = ActionType.ScalarCount;
            bool noRecords = false; // pha
            CommandExecute();
            try
            {
                return Convert.ToInt32(ScalarResult);
            }
            catch
            {
                return 0;
            }
        }

        protected bool CommandExecute()
        {
            bool Result = true;

            if (target == TargetType.StoredProc)
            {
                Result = ExecuteStoredProc();
            }
            else if (target == TargetType.NoTarget)
            {
                Result = true;
            }
            else if (target == TargetType.Dynamic)
            {
                Result = ExecuteDynamicSql();
            }
            else if (target == TargetType.Static)
            {
                Result = ExecuteStaticSql();
            }
            else
            {
                Result = ExecuteTableChange();
            }
            return Result;
        }

        protected bool ExecuteStoredProc()
        {
            SqlCommand cmd = CreateCommand(GetProcedureName(targetName), CommandType.StoredProcedure);
            cmd.CommandTimeout = KxDataAccess.CommandTimeout;
          
            if (autoKeyField != null)
            {
                cmd.Parameters.Add(new SqlParameter("@" + autoKeyField.Name, autoKeyField.Value));
            }
            
            AddParameters(cmd, KeyFields, false);
            AddParameters(cmd, parameters,false);
            AddParameters(cmd, outputParameters,true);

            SqlParameter param = cmd.Parameters.Add("RETURN_VALUE",SqlDbType.Int);
            param.Direction = ParameterDirection.ReturnValue;

            // pha
            bool logcount = LogDebug(cmd,true);

            try
            {
                if (action == ActionType.Scalar || action == ActionType.ScalarCount)
                {
                    SqlDataReader reader = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    try
                    {
                        ScalarResult = ResultsToScalar(reader);
                        return true;
                    }
                    finally
                    {
                        reader.Close();
                        reader = null;
                    }
                }
                else if (action == ActionType.Select || action == ActionType.ExecuteProcedure)
                {

                    SqlDataReader reader = cmd.ExecuteReader();
                    try
                    {
                        if (action == ActionType.Select && !reader.HasRows)
                            _norecords = true;
                        xmlResults = ResultsToXml(reader, logcount);
                        return true;
                    }
                    finally
                    {
                        reader.Close();
                        reader = null;
                    }
                }
                else if ((action == ActionType.Insert) && (AutoKeyField != null))
                {
                     cmd.ExecuteNonQuery();
                     identifier = (int)cmd.Parameters["RETURN_VALUE"].Value;
                     return true;
                }
                else
                {
                    cmd.ExecuteNonQuery();
                    return ((int)cmd.Parameters["RETURN_VALUE"].Value) == 0; 
                }
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "ExecuteStoredProc", ex.Message, cmd.CommandText);
                lastError = "ExecuteStoredProc " + ex.Message + " in " + cmd.CommandText;
                return false;
            }
        }

        // pha
        protected string GetWebResource()
        {
            if (_webresources.Count == 0)
            {
                SqlCommand cmd = CreateCommand(string.Format("SELECT Identifier, Data FROM WebResources WHERE ApplicationId = {0} AND SiteVersion = {1}", keyFields[1].Value, keyFields[2].Value), CommandType.Text);
                SqlDataReader reader = cmd.ExecuteReader();
                try
                {
                    while (reader.Read())
                    {
                        _webresources.Add(reader[0].ToString(), reader[1].ToString());
                    }
                }
                catch (Exception ex)
                {

                }
            }
            return "";
        }

        protected bool ExecuteTableChange()
        {
            bool result = true;

            SqlCommand cmd = CreateCommand(KxDataDynamics.MakeSql(this), CommandType.Text);

            if (autoKeyField != null) 
            {
                cmd.Parameters.Add(new SqlParameter("@"+autoKeyField.Name,autoKeyField.Value));
            }

            AddParameters(cmd, keyFields, false);
            AddParameters(cmd, parameters, false);

            // pha
            bool logcount = LogDebug(cmd, false);

            try
            {
                if (action == ActionType.Scalar || action == ActionType.ScalarCount)
                {
                    SqlDataReader reader = cmd.ExecuteReader();
                    try
                    {
                        ScalarResult = ResultsToScalar(reader);
                        return true;
                    }
                    finally
                    {
                        reader.Close();
                        reader = null;
                    }
                }
                else  if (action == ActionType.Select) 
                {
                    SqlDataReader reader = cmd.ExecuteReader();
                    try
                    {
                        xmlResults = ResultsToXml(reader, logcount);
                        result = true;
                    }
                    finally
                    {
                        reader.Close();
                        reader = null;
                    }
                }
                else
                {
                    if (action == ActionType.Insert && AutoKeyField != null && Convert.ToInt32(AutoKeyField.Value) == 0)
                    {
                        SqlDataReader reader = cmd.ExecuteReader();
                        try
                        {
                            while (reader.Read())
                            {
                                try
                                {
                                    identifier = Convert.ToInt32(reader.GetDecimal(0));
                                    result = true;
                                }
                                catch
                                {
                                    identifier = 0;
                                    result = false;
                                }
                            }
                        }
                        finally
                        {
                            reader.Close();
                            reader = null;
                        }
                    }
                    else
                    {
                        cmd.ExecuteNonQuery();
                        result = true;
                    }  
                }
                return result;
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "ExecuteTableChange", ex.Message, cmd.CommandText);
                lastError = "ExecuteTableChange : " + ex.Message;
                return false;
            }
        }

        protected bool ExecuteDynamicSql()
        {
            SqlCommand cmd = CreateCommand(KxDataDynamics.SqlStatement(targetName), CommandType.Text);
            //KxLogger.LogActivity(this, "ExecuteDynamicSql SQL", KxDataDynamics.SqlStatement(targetName), String.Format("Target : {0}", targetName));

            // pha

            try
            {
                string ddfr = ConfigurationManager.AppSettings["DebugDynamicFindReplace"];
                if (string.IsNullOrEmpty(ddfr) == false)
                {
                    string[] arr = ddfr.Split('|');
                    if (targetName.ToLower() == arr[0].ToLower())
                    {
                        cmd.CommandText = cmd.CommandText.Replace(arr[1], arr[2]);
                    }
                }
            }
            catch { }
               

            AddParameters(cmd, parameters,false);
            bool logcount = LogDebug(cmd, false);
            try
            {
                if (action == ActionType.Scalar)
                {
                    SqlDataReader reader = cmd.ExecuteReader();
                    try
                    {
                        ScalarResult = ResultsToScalar(reader);
                        return true;
                    }
                    finally
                    {
                        reader.Close();
                        reader = null;
                    }
                }
                else if (action == ActionType.Select || action == ActionType.DirtySelect)
                {

                    SqlDataReader reader = cmd.ExecuteReader();
                    try
                    {
                        xmlResults = ResultsToXml(reader, logcount);
                        return true;
                    }
                    finally
                    {
                        reader.Close();
                        reader = null;
                    }
                }
                else
                {
                    cmd.ExecuteNonQuery();
                    return true;
                }
            }
            catch (Exception ex)
            {
				KxLogger.LogActivity(this, "ExecuteDynamicSql", ex.Message, String.Format("Target : {0} Command Text : {1}",
					targetName, cmd.CommandText));
                lastError = "ExecuteDynamicSql : " + ex.Message;
                return false;
            }
        }

        protected bool ExecuteStaticSql()
        {
            SqlCommand cmd = CreateCommand(targetName, CommandType.Text);
            AddParameters(cmd, Parameters, false);
            bool logcount =  LogDebug(cmd, false);
            try
            {
                if (action == ActionType.Scalar)
                {
                    SqlDataReader reader = cmd.ExecuteReader();
                    try
                    {
                        ScalarResult = ResultsToScalar(reader);
                        return true;
                    }
                    finally
                    {
                        reader.Close();
                        reader = null;
                    }
                }
                else if (action == ActionType.Select || action == ActionType.DirtySelect)
                {
                    SqlDataReader reader = cmd.ExecuteReader();
                    try
                    {
                        xmlResults = ResultsToXml(reader, logcount);
                        return true;
                    }
                    finally
                    {
                        reader.Close();
                        reader = null;
                    }
                }
                else
                {
                    cmd.ExecuteNonQuery();
                    return true;
                }
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "ExecuteDynamicSql", ex.Message, cmd.CommandText);
                lastError = "ExecuteDynamicSql : " + ex.Message;
                return false;
            }
        }
        
        protected void AddParameters(SqlCommand cmd, List<KxDataValue> list,bool Output)
        {
            foreach (KxDataValue param in list)
            {
                if (Target != TargetType.Table  || (Target == TargetType.Table  && (cmd.CommandText.Contains("@" + param.Name) || cmd.CommandText.Contains("@" + param.Name))))
                {
                    AddParameter(cmd, Output, param, param is KxBETWEENDataValue ? "1" : string.Empty);
                    if (param is KxBETWEENDataValue)
                    {
                        AddParameter(cmd, Output, param, "2");
                    }
                }
            }
        }

        private void AddParameter(SqlCommand cmd, bool Output, KxDataValue param, string suffix)
        {
            SqlParameter parameter = new SqlParameter();
            parameter.ParameterName = param.Name + suffix;

            if (param.ValueType == KxValueType.Image)
            {
                parameter.SqlDbType = SqlDbType.Image;
            }

            parameter.SqlValue = GetDbCompliantValue(param);

            if (Output)
            {
                parameter.Direction = ParameterDirection.Output;
            }

            cmd.Parameters.Add(parameter);
        }

        protected SqlCommand CreateCommand(string CommandText,CommandType cmdType, SqlConnection connection = null)
        {
            SqlCommand result = new SqlCommand();
            result.Transaction = Transaction;

            result.Connection = connection ?? Connection;

            
            result.CommandText = CommandText;
            result.CommandType = cmdType;
            result.Prepare();

            return result;
        }

        protected string GetProcedureName(string className)
        {
            if (action == ActionType.ExecuteProcedure)
            {
                return targetName; 
            }
            else if (action == ActionType.Scalar || action == ActionType.ScalarCount)
            {
                return "Kx" + className + "Select";
            }
            else if (action == ActionType.Select || action == ActionType.DirtySelect)
            {
                return "Kx" + className + "Select";
            }
            else if (action == ActionType.Update)
            {
                return "Kx" + className + "Update";
            }
            else if (action == ActionType.Insert)
            {
                return "Kx" + className + "Create";
            }
            else
            {
                return "Kx" + className + "Delete";
            }
        }

        protected object ResultsToScalar(SqlDataReader reader)
        {
            object result = null;

            while (reader.Read())
            {
                if (reader.FieldCount == 1)
                {
                    try
                    {
                        result = result = reader[0]; 
                    }
                    catch 
                    {
                        result = null;
                    }
                }
            }
            return result;
        }

        protected string ResultsToXml(SqlDataReader reader, bool logcount)
        {
            int rc = 0;
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("   <" + rootName.ToLower() + ">");

            while (reader.Read())
            {
                sb.AppendLine("     <datarow>");

                for (int idx = 0; idx < reader.FieldCount; idx++)
                {
                    sb.AppendLine(KxDataAccess.FieldToString(reader, idx));
                }
                sb.AppendLine("     </datarow>");
                rc++;
            }
            if (logcount)
            {
                new Data.KxData().LogDebug("SQLResultsToXml", rc + " records processed");
            }
            sb.AppendLine("   </" + rootName.ToLower() + ">");
            return sb.ToString();
        }

        /// <summary>
        /// Logs exceptions to the data and Application Insights if set up. If AppInsighConnectionString is present it'll log exceptions to AI, If NoDBLogging != true it will also log to CoreExceptions.
        /// </summary>
        /// <param name="owner">Class</param>
        /// <param name="method">Method in class</param>
        /// <param name="message">Message</param>
        /// <param name="extrainfo">Any additional info</param>
        public void LogException(string owner, string method, string message, string extrainfo)
        {
            if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["AppInsightConnectionString"])){
                KxLogger.LogExceptionToAI(owner, method, message, extrainfo);
            }
            if (ConfigurationManager.AppSettings["NoDBLogging"] != "true") {
                try
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = new SqlConnection(KxDataAccess.ConnectionString);
                    cmd.CommandText = "Insert into CoreExceptions(ClassName,Method,Error,ExtraInfo,[TimeStamp]) Values (@ClassName,@Method,@Error,@ExtraInfo,GETDATE())";
                    cmd.CommandType = CommandType.Text;
                    cmd.Prepare();

                    cmd.Parameters.Add(new SqlParameter("@ClassName", owner));
                    cmd.Parameters.Add(new SqlParameter("@Method", method));
                    cmd.Parameters.Add(new SqlParameter("@Error", message));
                    cmd.Parameters.Add(new SqlParameter("@ExtraInfo", extrainfo));
                    try
                    {
                        cmd.Connection.Open();
                        cmd.ExecuteNonQuery();
                    }
                    finally
                    {
                        cmd.Connection.Close();
                    }
                }
                catch
                { }
            }
        }

        [Obsolete]
        public void BreadcrumbTrail(string Referrer, string HostIP, string userName, string currentUrl)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = new SqlConnection(KxDataAccess.ConnectionString);
                cmd.Connection.Open();
                try
                {
                    cmd.CommandText = "AddBreadcrumb";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Prepare();

                    cmd.Parameters.Add(new SqlParameter("@ClientIP", HostIP));
                    cmd.Parameters.Add(new SqlParameter("@UserName", userName));
                    cmd.Parameters.Add(new SqlParameter("@FromUrl", Referrer));
                    cmd.Parameters.Add(new SqlParameter("@CurrentUrl", currentUrl));
                    cmd.ExecuteNonQuery();
                }
                finally
                {
                    cmd.Connection.Close();
                }
            }
            catch
            {
            }
        }

        public void CloseConnection()
        {
            if (connectionCreated && Transaction == null)
            {
                Connection.Close();
            }
        }

        public object GetDbCompliantValue(KxDataValue value)
        {
            object result = null;
           
            if (!value.Nullable && value.Value == null)
            {
                // Do Null Substitution
                switch (value.ValueType) 
                {
                    case KxValueType.Short : result = 0;break;
                    case KxValueType.Int: result = 0;break;
                    case KxValueType.Long: result = 0;break;
                    case KxValueType.Boolean : result = false;break;
                    case KxValueType.String : result = "";break;
                    case KxValueType.DateTime: result = KxDateTime.KxNullDate; break;
                    case KxValueType.Image: result = DBNull.Value; break;
                    case KxValueType.Decimal: result = 0;break;
                    case KxValueType.Guid: result = new Guid();  break;
                    case KxValueType.Text : result = "";break;
                    default: result = ""; break;
                }
            }
            else if (value.Value == null)
            {
                result = DBNull.Value;
            }
            else 
            {
                if (value.ValueType == KxValueType.DateTime)
                {
                    result = KxDateTime.KxSqlCompliantDate(value.Value);
                }
                else if (value.ValueType == KxValueType.String)
                {
                    if (value.FieldSize > 0 && value.FieldSize < 2147483647)
                    {
                        string strvalue = value.Value.ToString();

                        if (strvalue.Length > value.FieldSize)
                        {
                            try
                            {
                                result = strvalue.Substring(0, value.FieldSize);
                            }
                            catch
                            { }
                        }  
                    }
                }
                else if (value.ValueType == KxValueType.Image)
                {
                    byte[] imageBytes;
                    MemoryStream ms = new MemoryStream();

                    Image image = (value.Value as Image);
                    if (image != null)
                    {
                        try
                        {
                            if (ImageFormat.Equals(image.RawFormat, ImageFormat.MemoryBmp))
                            {
                                image.Save(ms, ImageFormat.Jpeg);
                            }
                            else
                            {
                                image.Save(ms, image.RawFormat);
                            }
                            try
                            {
                                imageBytes = new byte[Convert.ToInt32(ms.Length)];
                                ms.Position = 0;
                                ms.Read(imageBytes, 0, Convert.ToInt32(ms.Length));

                                result = imageBytes;
                            }
                            finally
                            {
                                ms.Close();
                            }
                        }
                        catch 
                        { }
                    }
                }
            }

            if (result == null)
            {
                result = value.Value;
            }

            return result;
        }

        /// <summary>
        /// Takes the XML data results and returns as a nodes list for processing
        /// </summary>
        /// <returns>XmlNodeList</returns>
        public XmlNodeList GetNodesFromData()
        {
            var doc = new XmlDocument();
            doc.LoadXml(this.XmlResults);
            return doc.SelectNodes(string.Format(@"//{0}/datarow", this.RootName.ToLower()));
        }

    }
}
