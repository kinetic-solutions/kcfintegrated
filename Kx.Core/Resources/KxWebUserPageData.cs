using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;

namespace Kx.Core.Resources
{
    [Serializable]
    public class KxWebUserPageData : KxBaseClass
    {
        #region Attributes & Properties
        public bool Loaded = false;

        protected string   _pageName;
        protected string   _resourceName;
        protected string   _editorText;
        protected string   _updateUser;
        protected int _version;
        protected DateTime ?_updateTimestamp;

        public string   PageName
        {
            get
            { return _pageName;}
            set
            { 
                AddToAudit("PageName", _pageName, value);
                _pageName = value;
            }
        }

        public string   ResourceName
        {
            get
            { return _resourceName;}
            set
            { 
                AddToAudit("ResourceName", _resourceName, value);
                _resourceName = value;
            }
        }

        public string   EditorText
        {
            get
            { return _editorText;}
            set
            { 
                AddToAudit("EditorText", _editorText, value);
                _editorText = value;
            }
        }

        public int Version
        {
            get
            { return _version; }
            set
            {
                AddToAudit("Version", _version, value);
                _version = value;
            }
        }

        public string Data
        {
            get
            {
                return EditorText;
            }
        }

        public string   UpdateUser
        {
            get
            { return _updateUser;}
            set
            { 
                AddToAudit("UpdateUser", _updateUser, value);
                _updateUser = value;
            }
        }

        public DateTime ?UpdateTimestamp
        {
            get
            { return _updateTimestamp;}
            set
            { 
                AddToAudit("UpdateTimestamp", _updateTimestamp, value);
                _updateTimestamp = value;
            }
        }

        public string UniqueId
        {
            get
            {
                return String.Format("{0}:{1}", PageName.ToUpper(), ResourceName.ToUpper());
            }
        }


        #endregion 
        public KxWebUserPageData(KxBaseClass parent): base(parent)
        {
        }

        public KxWebUserPageData(KxBaseClass parent, XmlNode node)
            : base(parent)
        {
             LoadFromXml("", node);
        }

        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("webuserpagedata", "WebUserPageData", TargetType.Table);

            result.AddKeyField("PageName", PageName, KxValueType.String);
            result.AddKeyField("ResourceName", ResourceName, KxValueType.String);

            if (actiontype == ActionType.Update && !Loaded)
             {
                 result.Action = ActionType.Insert;
             }
             else
             {
                 result.Action = actiontype;
             }

            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {
                AddHistoryEntry();
                result.AddParameter("editorText", _editorText, KxValueType.String, 2147483647, true);
                result.AddParameter("editorData", KxBase64.Base64Encode(_editorText), KxValueType.String, 2147483647, true);
                result.AddParameter("updateUser", _updateUser, KxValueType.String, 100, true);
                result.AddParameter("version", _version, KxValueType.Int, true);
                result.AddParameter("updateTimestamp", _updateTimestamp, KxValueType.DateTime, true);
            }
            return result;
        }


        public override  void LoadFromXml(string xml,XmlNode node)
        {
            _pageName  = KxXmlUtils.GetNodeValue(node,"pagename","");
            _resourceName  = KxXmlUtils.GetNodeValue(node,"resourcename","");
            _editorText  = KxBase64.Base64Decode(KxXmlUtils.GetNodeValue(node,"editordata",""));
            _updateUser  = KxXmlUtils.GetNodeValue(node,"updateuser","");
            _version = KxXmlUtils.GetNodeValue(node, "version", 0);
            _updateTimestamp  = KxXmlUtils.GetNodeValueDT(node,"updatetimestamp",null);

            Loaded = true;
        }

        public void AddHistoryEntry()
        {
            KxWebUserPageDataHistory history = new KxWebUserPageDataHistory(this, PageName, ResourceName, Version);
            if (history.UpdateTimestamp == null)
            {
                history.Loaded = false;
            }
            history.UpdateTimestamp = UpdateTimestamp;
            history.UpdateUser = UpdateUser;
            history.EditorText = EditorText;
        }
    }
}

