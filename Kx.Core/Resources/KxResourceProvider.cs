using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Web;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Exceptions;
using Kx.Core.Utility;
using Kx.Core.Configuration;

namespace Kx.Core.Resources
{
    [Serializable]
    public class KxResourceProvider : KxBaseClass
    {
        private KxWebUserPageDataManager pageDataManager = null;


        public string[] ResourceIdentifiers
        {
            get
            {
                return GetResourceIdentifiers();
            }
        }

        private string applicationId  = "";

        public string ApplicationId
        {
            get
            {
                if (String.IsNullOrEmpty(applicationId))
                {
                    applicationId = KxDbConfiguration.GetConfiguration("ApplicationId");
                }
                return applicationId;
            }
            set
            {
                applicationId = value;
            }
        }

        public KxWebUserPageDataManager PageDataManager
        {
            get
            {
                if (pageDataManager == null)
                {
                    pageDataManager = new KxWebUserPageDataManager(null);
                }
                return pageDataManager;
            }
        }

        public KxResourceProvider()
        {
            Load();

        }

        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = null;

            if (actiontype == ActionType.Select)
            {
                result = new KxData("webresources", "webresources", TargetType.Table, ActionType.Select);

                result.AddKeyField("applicationid", ApplicationId, KxValueType.String);

            }
            return result;
        }

        public override bool LoadFromXml(string xml, string xpath)
        {
            return true;
        }


        private int SiteVersion
        {
            get
            {
                string versionString = HttpContext.Current?.Request["siteversion"] ?? KxDbConfiguration.GetConfiguration("siteversion", "1");
                int result = 1;
                if (int.TryParse(versionString, out result))
                {
                    return result;
                }

                return 1;
            }
        }

        public virtual KxWebResource GetResourceObject(string id)
        {
            return new KxWebResource(this, ApplicationId, id, SiteVersion);
        }

        public virtual string GetResource(string identifier)
        {
            string result = "";

            KxWebResource resrc= GetResourceObject(identifier);

            if (resrc != null)
            {
                result = resrc.Data;
            }

            return result;
        }

        public string[] GetResourceIdentifiers()
        {
            return new string[0];
        }

        public bool AddResource(string id, string data)
        {
            return true;
        }
    }
}
