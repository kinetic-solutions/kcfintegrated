using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;

namespace Kx.Core.Resources
{
    [Serializable]
    public class KxWebUserPageDataHistory : KxBaseClass
    {
        #region Attributes & Properties
        public bool Loaded = false;

        protected string   _pageName;
        protected string   _resourceName;
        protected string   _editorText;
        protected string   _updateUser;
        protected int _version;
        protected DateTime ?_updateTimestamp;

        public string   PageName
        {
            get
            { return _pageName;}
            set
            { 
                AddToAudit("PageName", _pageName, value);
                _pageName = value;
            }
        }

        public string   ResourceName
        {
            get
            { return _resourceName;}
            set
            { 
                AddToAudit("ResourceName", _resourceName, value);
                _resourceName = value;
            }
        }

        public string   EditorText
        {
            get
            { return _editorText;}
            set
            { 
                AddToAudit("EditorText", _editorText, value);
                _editorText = value;
            }
        }

        public string   UpdateUser
        {
            get
            { return _updateUser;}
            set
            { 
                AddToAudit("UpdateUser", _updateUser, value);
                _updateUser = value;
            }
        }

        public int Version
        {
            get
            { return _version; }
            set
            {
                AddToAudit("Version", _version, value);
                _version = value;
            }
        }

        public DateTime ?UpdateTimestamp
        {
            get
            { return _updateTimestamp;}
            set
            { 
                AddToAudit("UpdateTimestamp", _updateTimestamp, value);
                _updateTimestamp = value;
            }
        }


        #endregion 

        public KxWebUserPageDataHistory(KxBaseClass parent)
            : base(parent)
        {
        }

        public KxWebUserPageDataHistory(KxBaseClass parent,string pageName, string resourceName, int version)
            : base(parent)
        {
            _pageName = pageName;
            _resourceName = resourceName;
            _version = version;
            Load();
        }

        public override KxData ConfigureData(ActionType actiontype)
        {
            IsKxClass = false;

            KxData result = new KxData("webuserpagedatahistory", "WebUserPageDataHistory", TargetType.Table);

            result.AddKeyField("PageName", PageName, KxValueType.String);
            result.AddKeyField("ResourceName", ResourceName, KxValueType.String);
            result.AddKeyField("version", _version, KxValueType.Int);

            if (actiontype == ActionType.Update && !Loaded)
            {
                result.Action = ActionType.Insert;
            }
            else
            {
                result.Action = actiontype;
            }
           
            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {
                result.AddParameter("editorText", _editorText, KxValueType.String, 2147483647, true);
                result.AddParameter("editorData", KxBase64.Base64Encode(_editorText), KxValueType.String, 2147483647, true);
                result.AddParameter("updateUser", _updateUser, KxValueType.String, 100, true);
                result.AddParameter("updateTimestamp", _updateTimestamp, KxValueType.DateTime, true);
            }
            return result;
        }


        public override  void LoadFromXml(string xml,XmlNode node)
        {
            _pageName  = KxXmlUtils.GetNodeValue(node,"pagename","");
            _resourceName  = KxXmlUtils.GetNodeValue(node,"resourcename","");
            _editorText = KxBase64.Base64Decode(KxXmlUtils.GetNodeValue(node, "editordata", ""));
            _updateUser  = KxXmlUtils.GetNodeValue(node,"updateuser","");
            _version = KxXmlUtils.GetNodeValue(node, "version", 0);
            _updateTimestamp  = KxXmlUtils.GetNodeValueDT(node,"updatetimestamp",null);

            Loaded = true;
        }
    }
}

