using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Exceptions;
using Kx.Core.Utility;

namespace Kx.Core.Resources
{
    public class KxWebUserPageDataManager : KxBaseClass
    {
        private List<KxWebUserPageData> items = new List<KxWebUserPageData>();

        public List<KxWebUserPageData> Items
        {
            get
            { return items; }
        }

        public KxWebUserPageData this[int index]
        {
            get
            { return items[index]; }
            set
            { items[index] = value; }
        }

        public KxWebUserPageDataManager(KxBaseClass parent): base(parent)
        {
            Load();
        }

        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = null;

            if (actiontype == ActionType.Select)
            {
                result = new KxData("webuserpagedata", "WebUserPageData", TargetType.Table, ActionType.Select);

            }
            return result;
        }

        public override bool LoadFromXml(string xml, string xpath)
        {
            items.Clear();
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xml);

                XmlNodeList nodes = doc.SelectNodes(@"//results/" + xpath + "/datarow");

                foreach (XmlNode node in nodes)
                {
                    if (node != null)
                    {
                        KxWebUserPageData item = new KxWebUserPageData(this, node);
                        items.Add(item);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "LoadFromXml", ex.Message, xpath);
                LastError = ex.Message;
                return false;
            }
        }

        public KxWebUserPageData GetWebUserPageData(string pageName,string resourceName)
        {
            KxWebUserPageData result = null;

            foreach (KxWebUserPageData i in Items)
            {
                if (String.Compare(i.PageName,pageName,true) == 0  && String.Compare(i.ResourceName,resourceName,true) == 0)
                {
                    result = i;
                    break;
                }
            }
            return result;
        }

        public KxWebUserPageData UpdatePageData(string pageName, string resourceName, string newValue,string UpdateUser)
        {
            KxWebUserPageData result = GetWebUserPageData(pageName, resourceName);

            if (result == null)
            {
                result = new KxWebUserPageData(this);
                result.PageName = pageName.ToUpper();
                result.ResourceName = resourceName.ToUpper();
                result.Version = 1;
            }

            if (result.EditorText != newValue)
            {
                result.UpdateUser = UpdateUser;
                result.EditorText = newValue;
                result.UpdateTimestamp = DateTime.Now;
                result.Version += 1;
                result.Save();
            }

            return result;
        }

        public KxWebUserPageData RollbackPageData(string pageName, string resourceName)
        {
            KxWebUserPageData current = GetWebUserPageData(pageName, resourceName);

            if (current != null)
            {
                KxWebUserPageDataHistory history = new KxWebUserPageDataHistory(this, pageName, resourceName, current.Version);
                KxWebUserPageDataHistory rollback = new KxWebUserPageDataHistory(this, pageName, resourceName, current.Version-1);

                if (rollback.Loaded)
                {
                    current.Version = rollback.Version;
                    current.EditorText = rollback.EditorText;
                    if (current.Save())
                    {
                        history.Delete();
                    }
                }
                else
                {
                    current.EditorText = "";
                    current.Save();
                }
            }
            return current;
        }

    }
}

