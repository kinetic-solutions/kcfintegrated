using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;

namespace Kx.Core.Resources
{
    [Serializable]
    public class KxWebResource : KxBaseClass
    {
        #region Attributes & Properties

        protected string   applicationId;
        protected string   identifier;
        protected string   resourceType;
        protected string   data;
        protected string _resourcePath;
        protected int _siteVersion;
        protected bool loaded = false;

        public string   ApplicationId
        {
            get
            { return applicationId;}
            set
            { 
                AddToAudit("ApplicationId", applicationId, value);
                applicationId = value;
            }
        }

        public string   Identifier
        {
            get
            {
                try
                {
                    return identifier.ToUpper();
                }
                catch
                {
                    return "";
                }
            }
            set
            { 
                AddToAudit("Identifier", identifier, value);
                identifier = value;
            }
        }

        public string   ResourceType
        {
            get
            { return resourceType;}
            set
            { 
                AddToAudit("ResourceType", resourceType, value);
                resourceType = value;
            }
        }

        public string   Data
        {
            get
            { return data;}
            set
            { 
                AddToAudit("Data", data, value);
                data = value;
            }
        }

        public string ResourcePath
        {
            get { return _resourcePath; }
        }

        public int SiteVersion
        {
            get { return _siteVersion; }
        }

        #endregion 

        public KxWebResource(KxBaseClass parent)
            : base(parent)
        {
        }
        
        public KxWebResource(KxBaseClass parent, string appId, string id, int siteVersion)
            : base(parent)
        {
            applicationId = appId;
            identifier = id;
            _siteVersion = siteVersion;
            Load();
        }

        public KxWebResource(KxBaseClass parent, XmlNode node)
            : base(parent)
        {
             LoadFromXml("", node);
        }

        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("webresources", "WebResources", TargetType.Table);

           
            if ((actiontype == ActionType.Update) && (!loaded))
            {
                result.Action = ActionType.Insert;
                loaded = true;
            }
            else
            {
                result.Action = actiontype;
            }

            if (actiontype == ActionType.Select)
            {
                result.AddKeyField("applicationid", applicationId, KxValueType.String);
                result.AddKeyField("identifier", identifier, KxValueType.String);
                result.AddKeyField("siteversion", _siteVersion, KxValueType.Int);
            }
            else if (actiontype == ActionType.Insert) 
            {
                result.AddParameter("applicationid", applicationId, KxValueType.String, 50, false);
                result.AddParameter("identifier", identifier, KxValueType.String, 50, false);

                result.AddParameter("resourcetype", resourceType, KxValueType.String, 100, true);

                if (resourceType == "CONFIG" || resourceType.Contains("Environment") && applicationId == "naff")
                {
                    result.AddParameter("data", data, KxValueType.String, 2147483647, true);
                }
                else
                {
                    result.AddParameter("data", KxBase64.Base64Encode(data), KxValueType.String, 2147483647, true);
                }
                
            }
            else if (actiontype == ActionType.Update)
            {
                result.AddKeyField("applicationid", applicationId, KxValueType.String);
                result.AddKeyField("identifier", identifier, KxValueType.String);

                result.AddParameter("resourcetype", resourceType, KxValueType.String, 100, true);

                if (resourceType == "CONFIG" || resourceType.Contains("Environment") && applicationId == "naff")
                {
                    result.AddParameter("data", data, KxValueType.String, 2147483647, true);
                }
                else
                {
                    result.AddParameter("data", KxBase64.Base64Encode(data), KxValueType.String, 2147483647, true);
                }
            }
            return result;
        }


        public override  void LoadFromXml(string xml,XmlNode node)
        {   
            applicationId  = KxXmlUtils.GetNodeValue(node,"applicationid","");
            identifier  = KxXmlUtils.GetNodeValue(node,"identifier","");
            resourceType  = KxXmlUtils.GetNodeValue(node,"resourcetype","");
            data  = KxBase64.Base64DecodeWithCheck(KxXmlUtils.GetNodeValue(node,"data",""));

            if (resourceType == "RegResetPassword")
                System.Diagnostics.Debugger.Break();
            // pha
            if (Core.Configuration.KxDbConfiguration.GetConfiguration("DebugResources").ToLower() == "true")
            {
                using (System.IO.StreamWriter sw = new System.IO.StreamWriter(@"c:\temp\workflow.xml", true))
                {
                    sw.WriteLine("<LoadResource>");
                    sw.WriteLine("\t<Identifier>" + identifier + "</Identifier>");
                    sw.WriteLine("\t<Type>" + resourceType + "</Type>");
                    sw.WriteLine("\t<Data>");
                    sw.WriteLine("\t\t<!--");
                    sw.WriteLine("\t\t" + data);
                    sw.WriteLine("\t\t" + "-->");
                    sw.WriteLine("\t</Data>");
                    sw.WriteLine("</LoadResource>");
                }
            }
            _resourcePath = KxXmlUtils.GetNodeValue(node, "resourcepath", string.Empty);
            _siteVersion = KxXmlUtils.GetNodeValue(node, "siteversion", 1);

            loaded = true;
        }

        public static IEnumerable<KxWebResource> GetAllResourcesOfType(string applicationId, string resourceType)
        {
            KxData data = new KxData("resource", "WebResources", TargetType.Table);
            data.AddKeyField("applicationId", applicationId, KxValueType.String);
            data.AddKeyField("resourcetype", resourceType, KxValueType.String);
            if (data.Execute())
            {
                XmlNodeList nodes = data.GetNodesFromData();
                foreach (XmlNode node in nodes)
                {
                    yield return new KxWebResource(null, node);
                }
            }
            else
            {
                data.LogException("KxWebResource", "GetAllResourcesOfType", data.LastError, string.Format("{0}, {1}", applicationId, resourceType));
            }
        }
    }
}

