using System;
using System.Text;
using System.Xml;
using System.Collections.Generic;
using Kx.Core.Reflection;
using Kx.Core.Exceptions;

namespace Kx.Core.Workflow
{
	/// <summary>
    ///   When setting object property
	///   <SetProperty objectid="idofobject" property="propertyname">
    ///       <ValueSource/>
    ///   </SetProperty>
    /// 
    ///   for setting the value of the object itself to another value
    ///   (either base types or entire objects)
    ///   <SetProperty objectid="idofobject">
    ///      <ValueSource/>
    ///   </SetProperty>
	/// </summary>
	public class WorkflowSetPropertyAction : WorkflowAction
	{
		protected string _objectID = string.Empty;
		protected string _property = string.Empty;
        protected bool _safe = false;

        protected WorkflowValueSource _value = null;


        public string ObjectID
        {
            get
            { return _objectID; }
            set
            { _objectID = value; }
        }

        public string Property
        {
            get
            { return _property; }
            set
            { _property = value; }
        }

        public bool Safe
        {
            get
            { return _safe; }
            set
            { _safe = value; }
        }

	    private object LastValue = string.Empty;

        public WorkflowValueSource Value
        {
            get
            { return _value; }
            set
            { _value = value; }
        }

		public WorkflowSetPropertyAction(XmlNode xmlNode, Workflow parentWorkflow) : base (xmlNode, parentWorkflow)
		{
		}

		protected override void GetXmlInfo()
		{
			base.GetXmlInfo ();

			_objectID = GetAttribute("objectid");
			_property = GetAttribute("property");
            _safe = GetAttribute("safe").ToUpper() == "TRUE";

           
			LoadValueSource ();
		}

		protected void LoadValueSource ()
		{
			foreach (XmlNode n in _xmlNode.ChildNodes)
			{
				if (n.Name == "ValueSource")
				{
                    _value = new WorkflowValueSource(n, _parentWorkflow);
				}
			}
		}

	    protected override string DebugText()
	    {
	        return string.Format("Set Property \"{0}\" on \"{1}\" to \"{2}\"", Property, ObjectID, LastValue ?? "No Value");
	    }

	    public override bool Execute()
		{
			bool result = false;

			try
			{
				if (_value != null && _objectID != string.Empty)
				{
					WorkflowObject obj = _parentWorkflow.GetObjectByID(_objectID);
					if (obj != null)
					{
                        if (!String.IsNullOrEmpty(_property))
                        {
                            LastValue = _value.GetValue();
                            result = KxReflection.SetProperty(obj.InternalObject, _property, LastValue);//obj.SetProperty(_property, val.GetValue());
                        }
                        else
                        {
                            try
                            {
                                LastValue = _value.GetValue();
                                obj.InternalObject = LastValue;
                                result = true;
                            }
                            catch 
                            {
                                result = false;
                                throw;
                            }
                        }
					}
					result = true;
				}
			}
			catch (Exception ex)
			{
                KxLogger.LogActivity(this, "Execute", ex.Message, _objectID);
                if (_safe)
                {
                    result = true;
                }
			}

            base.Execute();

            return result;
		}

        public override string ToXml()
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine(Indent + "<SetProperty objectid=\"" + _objectID + "\" property=\"" + _property + "\" safe=\"" + _safe.ToString() + "\">");
            sb.AppendLine(Indent+_value.AsXml);
            sb.AppendLine(Indent+"</SetProperty>");

            return sb.ToString();
        }

        public override string ValidateVerbose()
        {
            string result = "";
             
            if (_parentWorkflow != null)
            {
                if (_parentWorkflow.GetObjectByID(_objectID) == null)
                {
                    result += "Invalid Object name, this object does not exist in this workflow\r\n";
                }
            }

            if (_property == "")
            {
                 result += "Property cannot be blank...\r\n";
            }
            
            string sv = _value.ValidateVerbose();

            if (sv != "")
            {
                result += sv;
            }
            return result;
        }

        public override void AddWorkflowItems(ref List<WorkflowItem> list)
        {
            base.AddWorkflowItems(ref list);
            _value.AddWorkflowItems(ref list);
        }

        public override string ToString()
        {
            if (_objectID == "" || _property == "")
            {
                return " Undefined";
            }
            else
            {
                return "Workflow Object: " + _objectID + ", Property: " + _property;
            }
        }

	}
}
