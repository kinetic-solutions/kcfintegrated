using System;
using System.Xml;
using Kx.Core.Exceptions;

namespace Kx.Core.Workflow
{
    /// <summary>
    /// Summary description for WorkflowExternal.
    /// </summary>
    public class WorkflowExternal : WorkflowAction
    {
        //protected string _actionID = string.Empty;
        protected string _objectId = string.Empty;

        public string ObjectId
        {
            get
            { return _objectId; }
            set
            { _objectId = value; }
        }

        public WorkflowExternal(XmlNode xmlNode, Workflow parentWorkflow)
            : base(xmlNode, parentWorkflow)
        {
        }

        protected override void GetXmlInfo()
        {
            base.GetXmlInfo();

           // _actionID = GetAttribute("actionid");
            _objectId = GetAttribute("objectid");
        }

        public override bool Execute()
        {
            base.Execute();
            bool result = false;
            
            if (_objectId != string.Empty)
            {
                _parentWorkflow.CurrentState = Workflow.WorkflowState.wfsPaused;
                _parentWorkflow.ExecuteProxy(_objectId);
            }

            return result;
        }

        public override string ToXml()
        {
            //return "                <ExternalWorkflow objectid=\"" + _objectId + "\" actionid=\"" + _actionID + "\" config=\"\" rule=\"\" />";
            return Indent+"<ExternalWorkflow objectid=\"" + _objectId + "\" config=\"\" rule=\"\" />";
        }

        protected override string DebugText()
        {
            return string.Format("External workflow on \"{0}\"", ObjectId);
        }

        public override string ValidateVerbose()
        {
            if (_objectId == "")
            {
                return "External Workflow name must not be blank...";
            }
            else
            {
                return "";
            }
        }
    }
}
