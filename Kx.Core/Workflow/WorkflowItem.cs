using System;
using System.Collections.Generic;
using System.Configuration;
using System.Xml;
using System.IO;
using System.Web;
using Kx.Core.Configuration;
using Kx.Core.Exceptions;
using Kx.Core.Reflection;
using Kx.Core.Utility;

namespace Kx.Core.Workflow
{
	/// <summary>
	/// Summary description for WorkflowItem.
	/// </summary>
	/// 
	[Serializable]
	public abstract class WorkflowItem : MarshalByRefObject, IDisposable
	{
	    protected XmlNode _xmlNode = null;
		protected string _IDATTRIBUTENAME = "id";
		protected Workflow _parentWorkflow = null;
	    protected object server = null;

	    public object UIPrimaryObject = null;
        public object UISecondaryObject = null;
        public object UITertiaryObject = null;

        public int IndentValue = 1;

        public string Indent
        {
            get
            {
                string temp = "";
                return temp.PadLeft(IndentValue * 4, ' ');
            }
        }

	    public bool TestMode { get; set; }

	    public bool Debug { get; protected set; }

	    public string ItemID { get; set; }

	    public string AsXml
        {
            get
            {
                return ToXml();
            }
        }

	    public bool IsNew { get; set; }

	    public WorkflowItem(string itemID, XmlNode xmlNode, Workflow parentWorkflow)
		{
            IsNew = false;
            Debug = false;
            TestMode = true;
            ItemID = itemID;
			_xmlNode = xmlNode;
			_parentWorkflow = parentWorkflow;
			GetXmlInfo();
		}

		public WorkflowItem(XmlNode xmlNode, Workflow parentWorkflow)
		{
		    IsNew = false;
		    ItemID = string.Empty;
		    Debug = false;
		    TestMode = true;
		    _parentWorkflow = parentWorkflow;
           if (xmlNode != null)
           {
    			_xmlNode = xmlNode;
	    		GetXmlInfo ();
           }
		}

		protected virtual void GetXmlInfo ()
		{
			if (ItemID == string.Empty)
			{
				ItemID = GetAttribute(_IDATTRIBUTENAME);
			}

            TestMode = KxStringUtils.ConvertToBool(GetAttribute("scope"),false);
            
		}

        public Workflow ParentWorkflow
        {
            get
            { return _parentWorkflow; }
        }

		protected string GetAttribute (string attribute, string defaultValue)
		{
			string result = defaultValue;

            if (_xmlNode != null)
            {
                if (_xmlNode.Attributes[attribute] != null)
                {
                    result = _xmlNode.Attributes[attribute].Value;

                    if (result == string.Empty)
                    {
                        result = defaultValue;
                    }
                }
            }
			return result;
		}

		protected XmlNodeList SelectNodesInMyNode (string xPath)
		{
			XmlNodeList result = null;

			XmlDocument doc =  new XmlDocument();
			doc.LoadXml(_xmlNode.OuterXml);
			result = doc.SelectNodes(xPath);

			return result;
		}

		protected string GetAttribute (string attribute)
		{
			return GetAttribute(attribute, string.Empty);
		}

		protected virtual void LogDebug ()
		{
		    try
		    {
				// pha
				string line = DebugText();
				if (line.Contains("-lang-"))
                {
					string defaultLang = System.Configuration.ConfigurationManager.AppSettings["DefaultLanguage"];
					line = line.Replace("-lang-", defaultLang);
				}
				if (System.Configuration.ConfigurationManager.AppSettings["DebugWorkflow"] == "True")
					new Data.KxData().LogDebug("Workflow", line); // pha
				
		    }
		    catch (Exception ex)
		    {
                KxLogger.LogActivity(this, "Workflow.DEBUG exception", ex.Message, AsXml);
            }
        }

	    private string GetDebugID()
	    {
	        string uniqueId = string.Empty;
	        object webuser = null;

            if (this is Workflow)
	        {
	            uniqueId = (this as Workflow).Identifier.ToString("D");
	            webuser = (this as Workflow).GetObjectByID("webuser");
	        }
            else if (this.ParentWorkflow != null)
	        {
                uniqueId = this.ParentWorkflow.Identifier.ToString("D");
                webuser = ParentWorkflow.GetObjectByID("webuser");
            }
            else
	        {
                uniqueId = HttpContext.Current.Session.SessionID;
	        }

	        string sourceAddress = HttpContext.Current.Request.UserHostAddress;
	        string sourceRequest = HttpContext.Current.Request.Url.AbsoluteUri;
	        string webUserId = string.Empty;
	        string username = string.Empty;

	        if (webuser != null)
	        {
                webUserId = KxReflection.GetPropertyString(((WorkflowObject) webuser).InternalObject, "UserGuid");
                username = KxReflection.GetPropertyString(((WorkflowObject) webuser).InternalObject, "UserName");
            }


            return string.Format("Username: \"{4}\" WebUserID: \"{3}\" SessionID: \"{5}\" WorkflowID: \"{0}\", from \"{1}\" URL \"{2}\"", uniqueId, sourceAddress, sourceRequest, webUserId, username, HttpContext.Current.Session.SessionID);
	    }

		public virtual bool Execute ()
		{
			//pha
			//if ((_parentWorkflow != null && _parentWorkflow.Debug) || (this is Workflow && (this as Workflow).Debug) || KxDbConfiguration.GetConfigurationBool("WorkflowDebug", false))
			//{
				LogDebug ();
			//}
			return true;
		}

        public virtual string ToXml()
        {
            return "";
        }

	    protected virtual string DebugText()
	    {
	        return string.Empty;
	    }

        public virtual bool Validate()
        {
            return ValidateVerbose() == "";
        }

        public virtual string ValidateVerbose()
        {
            return "";
        }

        public virtual void AddWorkflowItems(ref List<WorkflowItem> list)
        {
            list.Add(this);
        }

        public virtual void Dispose()
        {

        }
	}
}
