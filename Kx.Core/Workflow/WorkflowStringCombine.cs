﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Kx.Core.Workflow
{
    public class WorkflowStringCombine : WorkflowItem
    {
        private List<WorkflowValueSource> Values { get; set; } = new List<WorkflowValueSource>();

        public WorkflowStringCombine(XmlNode xmlNode, Workflow parentWorkflow) : base(xmlNode, parentWorkflow)
        {
        }

        protected override void GetXmlInfo()
        {
            base.GetXmlInfo();

            Values.Clear();
            XmlNodeList values = SelectNodesInMyNode("/StringCombine/ValueSource");
            foreach (XmlNode node in values)
            {
                Values.Add(new WorkflowValueSource(node, _parentWorkflow));
            }
        }
    }
}
