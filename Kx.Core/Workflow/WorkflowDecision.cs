using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;


namespace Kx.Core.Workflow
{
	/// <summary>
	/// Each decision has a type and should also contain values on which the decision is based, can be left or right values:
    /// 
    ///   <WorkflowDecision type="[switch|equality]">
    ///     <DecisionLeft>
    ///       <ValueSource/>
    ///     </DecisionLeft>
    ///     required for equality decisions
    ///    [<DecisionRight>
    ///       <ValueSource/>
    ///     </DecisionRight>]
    /// 
    ///    See WorkflowEqualityDecision for equality elements
    ///    See WorkflowSwitchDecision for switch elements
    /// 
    ///   </WorkflowDecision>
	/// </summary>
	public abstract class WorkflowDecision : WorkflowAction
	{
		protected WorkflowValueSource _leftValue = null;
		protected WorkflowValueSource _rightValue = null;

        public WorkflowValueSource LeftValue
        {
            get 
            {
                return _leftValue; 
            }
            set
            { _leftValue = value; }
        }

        public WorkflowValueSource RightValue
        {
            get 
            {
                return _rightValue; 
            }
            set
            { _rightValue = value; }
        }

		public WorkflowDecision(XmlNode xmlNode, Workflow parentWorkflow) : base (xmlNode, parentWorkflow)
		{
		}

		protected override void GetXmlInfo()
		{
			base.GetXmlInfo ();

			XmlNodeList leftNodes = SelectNodesInMyNode("/Decision/DecisionLeft/ValueSource");

			foreach(XmlNode node in leftNodes)
			{
				_leftValue = new WorkflowValueSource(node, _parentWorkflow);
			}
	
			XmlNodeList rightNodes = SelectNodesInMyNode("/Decision/DecisionRight/ValueSource");

			foreach(XmlNode node in rightNodes)
			{
				_rightValue = new WorkflowValueSource(node, _parentWorkflow);
			}
		}

        public override string ToXml()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(Indent + "<Decision >");

            if (_leftValue != null)
            {
                sb.AppendLine(Indent + "    <DecisionLeft>");
                _leftValue.IndentValue = IndentValue + 2;
                sb.AppendLine(_leftValue.AsXml);
                sb.AppendLine(Indent + "    </DecisionLeft>");
            }

            if (_rightValue != null)
            {
                sb.AppendLine(Indent + "    <DecisionRight>");
                _rightValue.IndentValue = IndentValue + 2;
                sb.AppendLine(_rightValue.AsXml);
                sb.AppendLine(Indent + "    </DecisionRight>");
            }
            sb.AppendLine(Indent + "</Decision>");

            return sb.ToString();
        }

        public override string ValidateVerbose()
        {
            string LeftString = "";
            
            if (LeftValue != null)
            {
                LeftValue.ValidateVerbose();
            }
            string RightString = "";
            
            if (RightValue != null)
            {
                RightValue.ValidateVerbose();
            }

            if (LeftString == "" && RightString == "")
            {
                return "";
            }
            else
            {
                return "Left Value : \r\n"+LeftString+"\r\n\r\n"+"Right Value : \r\n"+RightString;
            }
        }

        public bool UsesObject(string ObjectName)
        {
            if ((_leftValue != null && _leftValue.ObjectID == ObjectName) || (_rightValue != null && _rightValue.ObjectID == ObjectName))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public override void AddWorkflowItems(ref List<WorkflowItem> list)
        {
            base.AddWorkflowItems(ref list);
            if (LeftValue != null)
            {
                LeftValue.AddWorkflowItems(ref list);
            }
            if (RightValue != null)
            {
                RightValue.AddWorkflowItems(ref list);
            }
        }
	}
}
