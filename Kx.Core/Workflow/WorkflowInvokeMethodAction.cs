using System;
using System.Reflection;
using System.Text;
using System.Xml;
using System.Collections.Generic;
using Kx.Core.Reflection;
using Kx.Core.Exceptions;

namespace Kx.Core.Workflow
{
	/// <summary>
	/// Invokes a method on the object specified:
    /// 
    ///   <InvokeMethod objectid="idofobject" method="methodtoinvoke">
    ///       Optional list of value sources in order of any parameters that are required for the method.
    ///       [<ValueSource/>...]
    ///   </InvokeMethod>
	/// </summary>
	public class WorkflowInvokeMethodAction : WorkflowAction
	{
		protected string _objectID = string.Empty;
		protected string _method = string.Empty;

		protected List<WorkflowValueSource> _parameters = new List<WorkflowValueSource>();

        public string ObjectID
        {
            get
            { return _objectID; }
            set
            { _objectID = value; }
        }

        public Type ReturnType
        {
            get
            { return GetReturnType(); }
        }

        public string Method
        {
            get
            { return _method; }
            set
            { _method = value; }
        }

        public List<WorkflowValueSource> Parameters
        {
            get
            { return _parameters; }
            set
            { 
                _parameters = value; 
            }
        }

	    private object[] LastParameters = null;

		public WorkflowInvokeMethodAction (XmlNode xmlNode, Workflow parentWorkflow) : base (xmlNode, parentWorkflow)
		{}

		protected override void GetXmlInfo()
		{
			base.GetXmlInfo ();

			_objectID = GetAttribute("objectid");
			_method = GetAttribute("method");

			LoadParameters ();
		}

		void LoadParameters ()
		{
			_parameters.Clear ();
			foreach (XmlNode node in _xmlNode.ChildNodes)
			{
				if (node.Name == "ValueSource")
				{
					_parameters.Add(new WorkflowValueSource (node, _parentWorkflow));
				}
			}
		}

		object [] GetParameters ()
		{
			object [] result = new object [_parameters.Count];

			for(int i = 0; i < _parameters.Count; i++)
			{
				WorkflowValueSource vs = _parameters[i];
				result[i] = vs.GetValue ();
			}

			return result;
		}

	    protected override string DebugText()
	    {
	        return string.Format("Invoke Method \"{0}\" on \"{1}\" with Parameters \"{2}\"", Method, ObjectID, GetDebugParams());
	    }

	    private string GetDebugParams()
	    {
            StringBuilder result = new StringBuilder();
	        result.Append("(");
	        int index = 0;
	        foreach (WorkflowValueSource parameter in Parameters)
	        {
	            result.AppendFormat("Type: \"{0}\"; ObjectID \"{1}\"; Property \"{2}\"; Value\"{3}\",", parameter.Type, parameter.ObjectID, parameter.Value, LastParameters[index++]);
	        }
            result.Append(")");

	        return result.ToString();
	    }

        public object ExecuteResult ()
		{
			object result = null;

			try
			{
				if (_objectID != string.Empty && _method != string.Empty)
				{
					WorkflowObject obj = _parentWorkflow.GetObjectByID(_objectID);

					if (obj != null)
					{
					    LastParameters = GetParameters();
                        result = KxReflection.InvokeMethod(obj.InternalObject,_method,LastParameters);
					}
				}
			}
			catch (Exception ex)
			{
                KxLogger.LogActivity(this, "ExecuteResult", ex.Message, String.Format("{0}.{1}", _objectID, _method));
			}
			return result;
		}

		public override bool Execute()
		{
			bool result = false;

			try
			{
				if (_objectID != string.Empty && _method != string.Empty)
				{
					WorkflowObject obj = _parentWorkflow.GetObjectByID(_objectID);

					if (obj != null)
					{
                        LastParameters = GetParameters();
                        base.Execute();
                        KxReflection.InvokeMethod(obj.InternalObject,_method,LastParameters);//obj.InvokeMethod(_method, GetParameters ());
                        result = true;
					}
				}

			}
			catch (Exception ex)
			{
                KxLogger.LogActivity(this, "Execute", ex.Message, "");
                // pha
                if (!ex.Message.StartsWith("Thread was being aborted"))
                {
                    if (_parameters != null && _parameters.Count > 0)
                        throw new Exception("WorkflowInvokeMethodAction.Execute. Method: " + _method + ", Parameters[0] " + _parameters[0] + ", XML: " + ToXml().Trim() + ".\r\n\r\nError: " + ex.Message);    // pha
                    else
                        throw new Exception("WorkflowInvokeMethodAction.Execute. Method: " + _method + ", XML: " + ToXml().Trim() + ".\r\n\r\nError: " + ex.Message);    // pha
                }
			}
            return result;
		}

        public override string ToXml()
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine(Indent+"<InvokeMethod objectid=\"" + _objectID + "\" method=\"" + _method + "\">");

            foreach (WorkflowValueSource valsource in _parameters)
            {
                valsource.IndentValue = IndentValue + 1;
                sb.AppendLine(valsource.AsXml);
            }
            sb.AppendLine(Indent+"</InvokeMethod>");

            return sb.ToString();
        }

        public override string ValidateVerbose()
        {
            string result = "";

            if (_parentWorkflow != null)
            {
                if (_parentWorkflow.GetObjectByID(_objectID) == null)
                {
                    result += "Invalid Object name, this object does not exist in this workflow\r\n";
                }
            }

            if (_method == "")
            {
                 result += "Method cannot be blank...\r\n";
            }

            foreach (WorkflowValueSource src in _parameters)
            {
                string sv = src.ValidateVerbose();

                if (sv != "")
                {
                    result += sv;
                }
            }
            return result;
        }

        public override void AddWorkflowItems(ref List<WorkflowItem> list)
        {
            base.AddWorkflowItems(ref list);

            foreach (WorkflowValueSource wvs in _parameters)
            {
                wvs.AddWorkflowItems(ref list);
            }
        }

        public override string ToString()
        {
            if (_objectID == "" || _method == "")
            {
                return " Undefined";
            }
            else
            {
                return "Workflow Object: " + _objectID + ", Method: " + _method + " using " + _parameters.Count.ToString() + " parameters";
            }
        }

        private MethodInfo GetMethodInfo()
        {
            MethodInfo result = null;

            try
            {
                if (_objectID != string.Empty && _method != string.Empty)
                {
                    WorkflowObject obj = _parentWorkflow.GetObjectByID(_objectID);

                    if (obj != null)
                    {
                        result = KxReflection.GetMethodInfo(obj.InternalObject, _method);
                    }
                }
            }
            catch
            {
                return null;
            }
            return result;
        }

        private Type GetReturnType()
        {
            Type result = null;

            MethodInfo returnmethod = GetMethodInfo();

            if (returnmethod != null)
            {
                result = returnmethod.ReturnType;
            }

            return result;
        }
	}
}
