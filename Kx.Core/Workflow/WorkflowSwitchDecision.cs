using System;
using System.Text;
using System.Xml;
using System.Collections.Generic;

namespace Kx.Core.Workflow
{
	/// <summary>
	/// Following initial declaration of the decision, further elements of each case statement follow:
    /// 
    ///   <Case/>...
    /// 
    ///   See WorkflowSwitchCase for details of case declaration.
	/// </summary>
	public class WorkflowSwitchDecision : WorkflowDecision
	{
        protected List<WorkflowSwitchCase> _cases = new List<WorkflowSwitchCase>();

	    private string LeftDerivedValue = null;

        public List<WorkflowSwitchCase> Cases
        {
            get
            {
                return _cases;
            }
        }

		public WorkflowSwitchDecision(XmlNode xmlNode, Workflow parentWorkflow) : base (xmlNode, parentWorkflow)
		{
		}

		protected override void GetXmlInfo()
		{
			base.GetXmlInfo ();

			LoadCases ();
		}

	    protected override string DebugText()
	    {
	        return string.Format("Switch \"{0}\"", LeftDerivedValue);
	    }

	    protected void LoadCases()
		{
			_cases.Clear ();

			XmlNodeList nodes = SelectNodesInMyNode("/Decision/Case");

			foreach (XmlNode node in nodes)
			{
				_cases.Add(new WorkflowSwitchCase(node, _parentWorkflow));
			}
		}

		public override bool Execute()
		{
			bool result = false;

		    LeftDerivedValue = (_leftValue.GetValue() ?? "").ToString();
            base.Execute();

            foreach (WorkflowSwitchCase c in _cases)
			{
				if (c.IsCase(LeftDerivedValue))
				{
					c.Execute ();
					result = true;
					break;
				}
			}
            return result;
		}

        public override string ToXml()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(Indent + "<Decision type=\"switch\" >");

            if (_leftValue != null)
            {
                sb.AppendLine(Indent+"    <DecisionLeft>");
                _leftValue.IndentValue = IndentValue + 2;
                sb.AppendLine(_leftValue.AsXml);
                sb.AppendLine(Indent + "    </DecisionLeft>"); 
            }

            if (_rightValue != null)
            {
                sb.AppendLine(Indent + "    <DecisionRight>");
                _rightValue.IndentValue = IndentValue + 2;
                sb.AppendLine(_rightValue.AsXml);
                sb.AppendLine(Indent +  "    </DecisionRight>");
            }

            foreach (WorkflowSwitchCase switchcase in _cases)
            {
                switchcase.IndentValue = IndentValue + 1;
                sb.AppendLine(switchcase.AsXml);
            }


            sb.AppendLine(Indent + "</Decision>");

            return sb.ToString();
        }

        public override string ValidateVerbose()
        {
            string result = "";

            foreach (WorkflowSwitchCase c in _cases)
            {
                string av = c.ValidateVerbose();
                if (av != "")
                {
                    result += av;
                }
            }
            return result;
        }

        public override void AddWorkflowItems(ref List<WorkflowItem> list)
        {
            base.AddWorkflowItems(ref list);

            foreach (WorkflowSwitchCase cse in _cases)
            {
                cse.AddWorkflowItems(ref list);
            }
        }
	}
}
