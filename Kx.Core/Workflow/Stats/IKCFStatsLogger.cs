﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Kx.Core.Workflow.Stats
{
    public interface IKCFStatsLogger
    {
        void LogStats(string activity);
    }
}
