using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Kx.Core.Workflow
{
    /// <example>
    ///   Used to set the return value of the workflow.
    ///   <SetReturnValue>
    ///     <ValueSource/>
    ///   </SetReturnValue>
    /// </example>
    public class WorkflowSetReturnValueAction : WorkflowAction
    {
        private WorkflowValueSource _returnValue = null;

        public WorkflowValueSource ReturnValue
        {
            get
            {
                if (_returnValue == null)
                {
                    _returnValue = new WorkflowValueSource(null, null);
                }
                return _returnValue; 
            }
            set
            { _returnValue = value; }
        }

        public WorkflowSetReturnValueAction(XmlNode xmlNode, Workflow parentWorkflow): base(xmlNode, parentWorkflow)
        {

        }

        protected override void GetXmlInfo()
        {
            base.GetXmlInfo();

            XmlNodeList valueNodes = _xmlNode.ChildNodes;

            foreach (XmlNode node in valueNodes)
            {
                if ((node.Name == "ValueSource"))
                    _returnValue = new WorkflowValueSource(node, _parentWorkflow);
            }
        }

        public override bool Execute()
        {
            base.Execute();
            _parentWorkflow.ReturnValue = _returnValue.GetValue();
            return true;
        }

        public override string ToXml()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(Indent+"<ReturnValue >");
            sb.AppendLine(Indent + Indent + "<ValueSource type=\"external\" />");
            sb.AppendLine(Indent + "</ReturnValue>");

            return sb.ToString();
        }

        protected override string DebugText()
        {
            return string.Format("Return Value", ReturnValue);
        }

        public override string ValidateVerbose()
        {
            return _returnValue.ValidateVerbose();
        }

        public override void AddWorkflowItems(ref List<WorkflowItem> list)
        {
            base.AddWorkflowItems(ref list);
            _returnValue.AddWorkflowItems(ref list);
        }

    }
}
