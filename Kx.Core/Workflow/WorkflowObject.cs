using System;
using System.IO;
using System.Reflection;
using System.Diagnostics;
using System.Xml;
using Kx.Core.Reflection;
using Kx.Core.Utility;
using Kx.Core.Exceptions;
using System.Collections.Generic;
using Kx.Core.Configuration;

namespace Kx.Core.Workflow
{
	/// <summary>
	/// Declares an object for use within the workflow:
    ///   For base types (string, date etc) the assembly should be declared as base and the type specified in the class.
    ///   <WorkflowObject id="idobject" [assembly="nameofassembly|base"] class="fullnamespaceclass|int|integer|date|datetime|string"/>
    /// 
    /// If parameters are required within the constructor then base values can be assigned at this point
    ///   <WorkflowObject id="idobject" [assembly="nameofassembly|base"] class="fullnamespaceclass|int|integer|date|datetime|string">
    ///     <ValueSource type="typeofvalue" value="value/property"/>
    ///   </WorkflowObject>
    /// </summary>
	/// 
	[Serializable]
	public class WorkflowObject : WorkflowItem
	{
		string _assembly = string.Empty;
		string _className = string.Empty;
		Type _type;

		const string _ASSEMBLYATTRIBUTENAME = "assembly";
		const string _CLASSATTRIBUTENAME = "class";

        protected List<WorkflowValueSource> _parameters = new List<WorkflowValueSource>();
        
        object	_internalObject = null;

        public string AssemblyFile
        {
            get
            { return _assembly; }
            set
            { _assembly = value;}
        }

        public string ClassName
        {
            get
            { return _className; }
            set
            { _className = value; }
        }

        public Type InternalObjectType
        {
            get
            {
                return GetInternalObjectType();
            }
        }

        public object InternalObject
		{
			get 
            {
                if (_internalObject == null)
                {
                    LoadObject();
                }
                return _internalObject;
            }
            set
            {
                _internalObject = value;
            }
		}

        public bool IsExtender
        {
            get
            {
                try
                {
                    return InternalObject.GetType().Namespace.Contains("WorkflowExtenders");
                }
                catch (Exception ex)
                {
                    KxLogger.LogActivity(this, "IsExtender", ex.Message, _assembly+" "+_className); 
                    return false;
                }
            }
        }

        public List<WorkflowValueSource> Parameters
        {
            get
            { return _parameters; }
            set
            { _parameters = value; }
        }

        public WorkflowObject(XmlNode xmlNode, Workflow parentWorkflow)
            : base(xmlNode, parentWorkflow)
		{
		}

		protected override void GetXmlInfo ()
		{
			_assembly = GetAttribute(_ASSEMBLYATTRIBUTENAME);
			_className = GetAttribute(_CLASSATTRIBUTENAME);
			ItemID = GetAttribute(_IDATTRIBUTENAME);

            LoadParameters();
		}

        private void LoadParameters()
        {
            _parameters.Clear();
            foreach (XmlNode node in _xmlNode.ChildNodes)
            {
                if (node.Name == "ValueSource")
                {
                    _parameters.Add(new WorkflowValueSource(node, _parentWorkflow));
                }
            }
        }

		string GetValidAssembly (string sAssem)
		{
			string sAssemName;

			if (0 == sAssem.Length)
			{
				Process pr = Process.GetCurrentProcess();
				sAssemName = pr.ProcessName + ".exe";
			}
			else
			{
                string CurrentPath = KxDbConfiguration.GetConfiguration("WorkflowObjectPath");
                // pha
                if (String.IsNullOrEmpty(CurrentPath) == false && !System.IO.Directory.Exists(CurrentPath))
                {
                    System.Diagnostics.Debugger.Break();
                    throw new System.Exception(CurrentPath + " not found");
                }

                if (String.IsNullOrEmpty(CurrentPath))
                {
                    CurrentPath = Path.GetDirectoryName(System.Reflection.Assembly.GetAssembly(this.GetType()).Location);
                }

                sAssemName = Path.Combine(CurrentPath,sAssem);

				//sAssemName = sAssem;
			}
			return sAssemName;
		}

		private void LoadObject ()
		{
			if (_assembly == "base")
			{
				_internalObject = CreateBaseObject ();
				_type = _internalObject.GetType ();
			}
			else
			{
				Assembly asm = Assembly.LoadFrom (GetValidAssembly (_assembly));
				_type = asm.GetType(_className);

                if (!_type.IsAbstract)
                {
                    if (_parameters.Count > 0)
                    {
                        _internalObject = Activator.CreateInstance(_type, GetParameters());
                    }
                    else
                    {
                        _internalObject = Activator.CreateInstance(_type);
                    }
                }
            }
		}

        private Type GetInternalObjectType()
        {
            Type internalType;

            if (_assembly == "base")
            {
                if (_internalObject == null)
                {
                    _internalObject = CreateBaseObject();
                }
                internalType = _internalObject.GetType();
            }
            else
            {
                Assembly asm = Assembly.LoadFrom(GetValidAssembly(_assembly));
                internalType = asm.GetType(_className);
            }

            return internalType;
        }


        public void ResetObject()
        {
            _internalObject = null;
            LoadObject();
        }

        object[] GetParameters()
        {
            object[] result = new object[_parameters.Count];

            for (int i = 0; i < _parameters.Count; i++)
            {
                WorkflowValueSource vs = _parameters[i];
                result[i] = vs.GetValue();
            }

            return result;
        }

		private object CreateBaseObject ()
		{
			object result = null;
			switch (_className)
			{
				case "int":
				case "integer":
					result = new System.Int32();
					break;
				case "datetime":
					result = new DateTime();
					break;
				case "bool":
					result = new System.Boolean();
					break;
				default:
					result = new System.String(' ', 1);
					break;
			}
			return result;
		}
      
		protected bool SetBaseObjectValue (object value)
		{
			bool result = false;
			try
			{
                switch (InternalObject.GetType().ToString())
				{
					case "System.String":
                        _internalObject = value.ToString();
						break;
					case "System.Int32":
						if (value is System.Int32)
						{
                            _internalObject = value;
						}
						else
						{
                            _internalObject = int.Parse(value.ToString());
						}
						break;
					case "System.DateTime":
						if (value is DateTime)
						{
                            _internalObject = value;
						}
						else
						{
                            _internalObject = KxDateTime.Parse(value.ToString());
						}
						break;
				}

				result = true;
			}
			catch (Exception ex)
			{
                KxLogger.LogActivity(this, "SetBaseObjectValue", ex.Message, "");
            }
			return result;
		}

        public override string ToXml()
        {
            return Indent+"<WorkflowObject " + _IDATTRIBUTENAME + "=\"" + ItemID + "\" " + _ASSEMBLYATTRIBUTENAME + "=\"" + _assembly + "\" " + _CLASSATTRIBUTENAME + "=\"" + _className + "\" />";
        }

        public override string ToString()
        { 
           return ItemID;
        }

        public override string ValidateVerbose()
        {
            string result = "";
            
            try
            {
                if (InternalObject == null)
                {
                result += "Object is invalid, either assembly: "+_assembly+", or class name: "+_className+" is invalid";
                }
            }
            catch (Exception ex)
            {
                result += "Object is invalid, reason: "+ex.Message;
            }
            return result;
        }
	}
}
