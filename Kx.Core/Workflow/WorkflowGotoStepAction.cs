using System;
using System.Web;
using System.Xml;

namespace Kx.Core.Workflow
{
	/// <summary>
	/// Permits moving of workflow into a new step.
    ///  
    ///   <GotoStep stepid="targetstepid"/>
	/// </summary>
	public class WorkflowGotoStepAction : WorkflowAction
	{
		protected string _targetStep = string.Empty;

        public string TargetStep
        {
            get
            { return _targetStep; }
            set
            { _targetStep = value; }
        }

		public WorkflowGotoStepAction(XmlNode xmlNode, Workflow parentWorkflow) : base(xmlNode, parentWorkflow)
		{
		}

		protected override void GetXmlInfo()
		{
			base.GetXmlInfo ();
			_targetStep = GetAttribute("stepid");
		}

		public override bool Execute()
		{
		    base.Execute();
            string defaultLang = System.Configuration.ConfigurationManager.AppSettings["DefaultLanguage"];
            string overrideLang = HttpContext.Current.Request["lang"];
            
            if (_parentWorkflow != null && _targetStep != string.Empty)
			{
				_parentWorkflow.ExecuteStep(_targetStep.Replace("-lang-", (overrideLang ?? defaultLang ?? string.Empty).ToLower()));
			}

			return false;
		}

        public override string ToXml()
        {
            return String.Format("{0}<GotoStep stepid=\"{1}\" />", Indent, _targetStep);
        }

	    protected override string DebugText()
	    {
	        return string.Format("Goto step \"{0}\"", TargetStep);
	    }

	    public override string ValidateVerbose()
        {
            string result = "";

            if (_parentWorkflow != null)
            {
                if (_parentWorkflow.FindStepByName(_targetStep) == null)
                {
                    result =  "Invalid Step specified, this step does not exist in this workflow\r\n";
                }
            }
            return result;
        }
	}
}
