using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using System.Xml;
using Kx.Core.Exceptions;
using Kx.Core.Merge;
using Kx.Core.Reflection;

namespace Kx.Core.Workflow
{
    /// <summary>
    ///   <WorkflowStep id="idofstep">
    ///       ...
    ///   </WorkflowStep>
    /// </summary>
	[Serializable]
	public class WorkflowStep : WorkflowItem
    {
        private const string STR_StatsAttribute = "statlog";

        private string _statsText { get; set; }

        List<WorkflowAction> _actions = new List<WorkflowAction>();
        int _currentActionIndex = -1;

        private WorkflowStep _previousStep;

        public WorkflowStep PreviousStep
        {
            get
            { return _previousStep; }
            set
            { _previousStep = value; }
        }

        public List<WorkflowAction> Actions
        {
            get
            { return _actions; }
            set
            { _actions = value; }
        }

        public bool HasExternalActions
        {
            get
            {
                return GetHasExternalActions();
            }
        }

        public string ActionsToString
        {
            get
            {
                return GetActionsToString();
            }
        }

		public WorkflowStep(XmlNode xmlNode, Workflow parentWorkflow) : base(xmlNode, parentWorkflow)
		{
			LoadActions ();
		}

        protected override void GetXmlInfo()
        {
            base.GetXmlInfo();
            _statsText = GetAttribute(STR_StatsAttribute);
        }

        protected override string DebugText()
        {
            return string.Format("Workflow Step \"{0}\"", ItemID);
        }

        protected void LoadActions ()
		{
            if (_xmlNode != null)
            {
                _actions.Clear();
                foreach (XmlNode n in _xmlNode.ChildNodes)
                {
                    _actions.Add(_parentWorkflow.GetWorkflowAction(n));
                }
            }
		}

		public override bool Execute()
		{
		    base.Execute();
		    try
		    {
		        if (!string.IsNullOrWhiteSpace(_statsText))
		        {
		            ParentWorkflow.StatsLog(GetObjectStatsText());
		        }
		    }
		    catch (Exception e)
		    {
		        KxLogger.LogActivity(this, "Execute", "StatsLog", e.Message);
		    }

		    foreach (WorkflowAction action in _actions)
			{
				++_currentActionIndex;
				if (action != null && !action.Execute ())
				{
					break;
				}
			}
			return true;
		}

        private string GetObjectStatsText()
        {
            string result = _statsText;
            if (_statsText.Contains("{"))
            {
                try
                {
                    KxTemplateMerge merger = new KxTemplateMerge();
                    foreach (WorkflowObject workflowObject in ParentWorkflow.Objects)
                    {
                        merger.AddObject(workflowObject.ItemID, workflowObject.InternalObject);
                    }

                    result = merger.MergeTemplate(_statsText);
                }
                catch
                {}
            }

            return result;
        }

		public bool Resume ()
		{
			for (int i = _currentActionIndex + 1; i < _actions.Count; i++)
			{
				WorkflowAction action = _actions[i];
               
				++_currentActionIndex;
				if (action != null && !action.Execute ())
				{
					break;
				}
			}
			return true;
		}

        public bool Repeat()
        {
            WorkflowAction action = _actions[_currentActionIndex];
            action.Execute();
           
            return true;
        }

        public void Reset()
        {
            _currentActionIndex = -1;
        }

        public bool Restart()
        {
            Reset();
            return Resume();
        }

        public override string ToXml()
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine(Indent + "<WorkflowStep " + _IDATTRIBUTENAME + "=\"" + ItemID + "\">");

            foreach (WorkflowAction action in _actions)
            {
                action.IndentValue = IndentValue + 1;
                sb.AppendLine(action.AsXml);
            }
            sb.AppendLine(Indent + "</WorkflowStep>");

            return sb.ToString();
        }

        public override string ToString()
        {
            return ItemID;
        }
        private bool GetHasExternalActions()
        {
            bool result = false;
            foreach (WorkflowAction action in Actions)
            {
                if (action is WorkflowExternal || action is WorkflowExternalAction)
                {
                    result = true;
                    break;
                }
            }
            return result;
        }

        public override string ValidateVerbose()
        {
            string result = "";

            if (ItemID == "")
            {
                result += "Step Name must be specified...\r\n";
            }

            foreach (WorkflowAction action in _actions)
            {
                string av = action.ValidateVerbose();
                if (av != "")
                {
                    result += av;
                }
            }
            return result;
        }

        private string GetActionsToString()
        {
            string result = "";

            foreach (WorkflowAction action in Actions)
            {
                if (action != null)
                {
                    result += action.GetType().Name + ", ";
                }
                else
                {

                }
            }
            return result.Trim(',', ' ');
        }

        public override void AddWorkflowItems(ref List<WorkflowItem> list)
        {
            base.AddWorkflowItems(ref list);

            foreach (WorkflowAction action in _actions)
            {
                try
                {
                    action.AddWorkflowItems(ref list);
                }
                catch { }
            }
        }
	}
}
