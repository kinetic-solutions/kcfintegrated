using System;
using System.Text;
using System.Xml;
using System.Collections.Generic;
using Kx.Core.Exceptions;
using Kx.Core.Configuration;
using Kx.Core.Workflow.Stats;

namespace Kx.Core.Workflow
{
	/// <summary>
	/// <KxWorkflow id="idofworkflow" startstep="initialstepinworkflow" errorstep"notimplemented">
    ///   Array of workflow objects that are created within this workflow
    ///   <WorkflowObjects>
    ///     <WorkflowObject/>...
    ///   </WorkflowObjects>
    /// 
    ///   Array of steps within the workflow
    ///   <WorkflowSteps>
    ///     <WorkflowStep/>
    ///   </WorkflowSteps>
    /// </KxWorkflow>
	/// </summary>
	/// 
	[Serializable]
	public class Workflow : WorkflowItem
	{
        private const string STR_Invokemethod = "invokemethod";
        private const string STR_Gotostep = "gotostep";
        private const string STR_Setproperty = "setproperty";
        private const string STR_Externalaction = "externalaction";
        private const string STR_Externalworkflow = "externalworkflow";
        private const string STR_Decision = "decision";
        private const string STR_Complete = "complete";
        private const string STR_Returnvalue = "returnvalue";
        private const string STR_Debug = "debug";
        private const string STR_Type = "type";
        private const string STR_Switch = "switch";
        private const string STR_Equality = "equality";
        private const string STR_LIVE = "LIVE";
        private const string STR_StartStepAttribute = "startstep";
        private const string STR_ErrorStepAttribute = "errorstep";
        private const string STR_BlockStepAttribute = "blockstep";
        private const string STR_LogoutStepAttribute = "logoutstep";
        private const string STR_DebugAttribute = "debug";
        private const string STR_ScopeAttribute = "scope";
        private const string STR_StatsLoggerAttribute = "statslogger";

		public enum WorkflowState {wfsWaiting, wfsExecuting, wfsPaused, wfsComplete};

        #region Attibutes & Properties

        List<WorkflowObject> _objects = new List<WorkflowObject>();
        List<WorkflowStep> _steps = new List<WorkflowStep>();


	    // Added for multiple login detection...
	    bool _terminated = false;
        bool _blocked = false;

	    Guid _identifier = Guid.NewGuid();      

		public Guid Identifier
		{
			get {return _identifier;}
		}

        public string WorkflowName
        {
            get { return ItemID; }
            set { ItemID = value; }
        }

	    public string ErrorStep { get; set; }
	    public string StartStep { get; set; }
	    public string BlockStep { get; set; }
	    public string LogoutStep { get; set; }
	    public bool Expired { get; set; }
        public object ReturnValue { get; set; }
        public DateTime LastAccessed { get; set; }
        public Guid KeyObjectIdentifier { get; set; }
        private IKCFStatsLogger StatsLogger { get; set; }

	    public bool Terminated
        {
            get
            {
                return _terminated;
            }
            set
            {
                _terminated = value;
                if (_terminated)
                {
                    _currentStep = FindStepByName(ErrorStep);
                }
            }
        }

        public bool Blocked
        {
            get
            {
                return _blocked;
            }
            set
            {
                _blocked = value;
                if (_blocked)
                {
                    _currentStep = FindStepByName(BlockStep);
                }
            }
        }

	    public bool IsInternal
        {
            get
            {
                return GetIsInternal();
            }
        }

	    public object ExternalResult = null;

		WorkflowStep _currentStep = null;

	    Workflow _proxyWorkflow = null;

        public WorkflowStep CurrentStep
        {
            get
            { return Proxy._currentStep; }
        }

        public string CurrentStepName
        {
            get
            {
                if (CurrentStep != null)
                {
                    return CurrentStep.ItemID;
                }
                else
                {
                    return "None";
                }
            }
        }

	    public EventHandler OnComplete { get; set; }
	    public ExternalAction OnExternalAction { get; set; }
	    public WorkflowState CurrentState { get; set; }

	    public WorkflowManager Manager { get; private set; }

	    public Workflow Parent { get; set; }
	    public string Scope { get; set; }

	    public List<WorkflowObject> Objects
        {
            get { return _objects; }
        }

        public List<WorkflowStep> Steps
        {
            get { return _steps; }
        }

        public Workflow Proxy
        {
            get 
            {
                if (_proxyWorkflow != null)
                { return _proxyWorkflow.Proxy;}
                else
                { return this; }            
            }
        }

	    public bool LoggededInState { get; set; }

	    public WorkflowExternalAction LogExternalAction { get; set; }

	    #endregion

        public Workflow(XmlNode xmlNode, WorkflowManager manager, Workflow parent) : base(xmlNode, null)
		{
            LoadObjects ();
			LoadSteps ();
            Parent = parent;
			Manager = manager;
		}

        public string ExternalAction(WorkflowExternalAction action)
		{

            string result = "";

            if (OnExternalAction != null)
            {
                result = OnExternalAction(Proxy, action);

                if (result != "")
                {
                    Resume(result);
                }
            }
            return result;
		}

        public void ExecuteProxy(string Id)
        {
            _proxyWorkflow = Manager.GetWorkflow(Id, this);
            _proxyWorkflow.OnComplete = ProxyComplete;
            _proxyWorkflow.OnExternalAction = OnExternalAction;
            _proxyWorkflow.Execute();
        }
		protected override void GetXmlInfo()
		{
		    LoadDefaults();

			base.GetXmlInfo ();
			StartStep = GetAttribute(STR_StartStepAttribute);
			ErrorStep = GetAttribute(STR_ErrorStepAttribute);
            BlockStep = GetAttribute(STR_BlockStepAttribute);
            LogoutStep = GetAttribute(STR_LogoutStepAttribute);

            DetermineWorkflowScope();
		    DetermineWorkflowDebugging();
		    LoadWebStatsLogger(GetAttribute(STR_StatsLoggerAttribute));
		}

        private void LoadDefaults()
        {
            LogExternalAction = null;
            LoggededInState = false;
            Scope = "TEST";
            CurrentState = WorkflowState.wfsWaiting;
            OnExternalAction = null;
            OnComplete = null;
            KeyObjectIdentifier = Guid.Empty;
            LastAccessed = DateTime.Now;
            BlockStep = string.Empty;
            ReturnValue = null;
            Expired = false;
            LogoutStep = string.Empty;
            StartStep = string.Empty;
            ErrorStep = string.Empty;
        }

	    protected override string DebugText()
	    {
	        return string.Format("=============== Workflow {0} ===============", this.ItemID);
	    }

	    private void LoadWebStatsLogger(string loggerType)
        {
            StatsLogger = new NULLStatsLogger();
            if (!string.IsNullOrWhiteSpace(loggerType))
            {
                StatsLogger = ServiceLocator.ServiceLocator.ResolveServiceByTypeString<IKCFStatsLogger>(loggerType, null) ?? new NULLStatsLogger();
            }
        }

	    private void DetermineWorkflowDebugging()
	    {
	        string isdebug = GetAttribute(STR_DebugAttribute);

	        if (isdebug != string.Empty)
	        {
	            try
	            {
	                Debug = bool.Parse(isdebug);
	            }
	            catch
	            {
	                Debug = true;
	            }
	        }
	    }

	    private void DetermineWorkflowScope()
	    {
	        string scope = string.Empty;

	        try
	        {
	            scope = System.Configuration.ConfigurationManager.AppSettings["workflowscope"];
	        }
	        catch
	        {
	            scope = "";
	        }

	        if (!string.IsNullOrWhiteSpace(scope))
	        {
	            Scope = scope;
	        }
	        else
	        {
	            Scope = GetAttribute(STR_ScopeAttribute);
	        }

	        if (string.IsNullOrWhiteSpace(Scope))
	        {
                Scope = STR_LIVE;
            }

            TestMode = Scope.ToUpper() != STR_LIVE;
	    }

	    void LoadObjects ()
		{
			_objects.Clear();

            if (_xmlNode != null)
            {
                XmlNodeList nodes = _xmlNode.SelectNodes("/KxWorkflow/WorkflowObjects/WorkflowObject");

                foreach (XmlNode n in nodes)
                {
                    _objects.Add(new WorkflowObject(n, this));
                }
            }
		}

		void LoadSteps ()
		{
			_steps.Clear ();
			XmlNodeList nodes = _xmlNode.SelectNodes("/KxWorkflow/WorkflowSteps/WorkflowStep");

			foreach (XmlNode n in nodes)
			{
				_steps.Add(new WorkflowStep(n, this));
			}
		}

		public WorkflowAction GetWorkflowAction (XmlNode node)
		{
			WorkflowAction action = null;
			switch (node.Name.ToLower())
			{
				case STR_Invokemethod:
					action = new WorkflowInvokeMethodAction(node, this);
					break;
				case STR_Gotostep:
					action = new WorkflowGotoStepAction (node, this);
					break;
				case STR_Setproperty:
					action = new WorkflowSetPropertyAction (node, this);
					break;
				case STR_Externalaction:
                    action = new WorkflowExternalAction(node, this);
					break;
                case STR_Externalworkflow:
                    action = new WorkflowExternal(node, this);
                    break;
				case STR_Decision:
					action = GetWorkflowDecision (node);
					break;
				case STR_Complete:
					action = new WorkflowCompleteAction(node, this);
					break;
                case STR_Returnvalue :
                    action = new WorkflowSetReturnValueAction(node,this);
                    break;
                case STR_Debug:
                    action = new WorkflowDebugAction(node, this);
                    break;
				default:
					action = new WorkflowNullAction (node, this);
					break;
			}
			return action;
		}

		protected WorkflowDecision GetWorkflowDecision (XmlNode node)
		{
			WorkflowDecision result = null;
			if (node.Attributes[STR_Type] != null)
			{
				switch (node.Attributes[STR_Type].Value)
				{
					case STR_Switch:
						result = new WorkflowSwitchDecision(node, this);
						break;
                    case STR_Equality:
                        result = new WorkflowEqualityDecision(node, this);
                        break;
				}
			}
			return result;
		}

		/// <summary>
		/// Execute Step, processes the workflow step passed by the ID
		/// </summary>
		/// <param name="stepID">Name of the step to execute</param>
		public bool ExecuteStep (string stepID)
		{
            //System.Diagnostics.Debug.WriteLine("ExecuteStep:" + stepID);PHA
            LastAccessed = DateTime.Now;

			bool result = false;

            WorkflowStep _previousStep = _currentStep;
           
			_currentStep = FindStepByName (stepID);

            if (_currentStep != null && CurrentState != WorkflowState.wfsComplete)
            {
                _currentStep.Reset();
                _currentStep.PreviousStep = _previousStep;
                CurrentState = WorkflowState.wfsExecuting;
                result = _currentStep.Execute();
            }
            else
            {
                KxLogger.LogActivity(this, "ExecuteStep", "Step not Found:" + stepID, "");
            }

			return result;
		}

		public WorkflowStep FindStepByName (string stepID)
		{
			WorkflowStep result = null;
            foreach (WorkflowStep step in _steps)
			{
				if (step.ItemID.ToUpper() == stepID.ToUpper())
				{
					result = step;
					break;
				}
			}
			return result;
		}

		public override bool Execute()
		{
		    base.Execute();
            LastAccessed = DateTime.Now;

			bool result = false;
			if (StartStep != string.Empty)
			{
				result = ExecuteStep(StartStep);
			}

			return result;
		}

        public bool Execute(string startStep)
        {
            base.Execute();
            LastAccessed = DateTime.Now;

            bool result = false;
            if (startStep != string.Empty)
            {
                result = ExecuteStep(startStep);
            }

            return result;
        }

        public bool Resume(object Result)
        {
            LastAccessed = DateTime.Now;

            Proxy.ExternalResult = Result;

            if (Result is string && ((string)Result).ToUpper() == "LOGOUT" && LogoutStep != String.Empty)
            {
                Proxy._currentStep = FindStepByName(LogoutStep);
            }
            
            if (Proxy._currentStep != null && Proxy.CurrentState == WorkflowState.wfsPaused)
            {
                Proxy.CurrentState = WorkflowState.wfsExecuting;
                Proxy._currentStep.Resume();
            }
            return true;
        }

        public bool Repeat()
        {
            LastAccessed = DateTime.Now;

            if (Proxy._currentStep != null && Proxy.CurrentState == WorkflowState.wfsPaused)
            {
                Proxy._currentStep.Repeat();
            }
            return true;
        }
       
        public bool Back()
        {
            LastAccessed = DateTime.Now;

            if (Proxy._currentStep != null && Proxy.CurrentState == WorkflowState.wfsPaused)
            {
                Proxy._currentStep.Reset();
                if (Proxy._currentStep.PreviousStep != null)
                {
                    Proxy._currentStep = Proxy._currentStep.PreviousStep;
                    Proxy.CurrentState = WorkflowState.wfsExecuting;
                    Proxy._currentStep.Restart();
                }
                else
                {
                    Proxy.CurrentState = WorkflowState.wfsExecuting;
                    Proxy._currentStep.Restart();
                }
            }
            return true;
        }

		public bool Cancel ()
		{
            LastAccessed = DateTime.Now;

            if (Proxy._currentStep != null && Proxy.CurrentState == WorkflowState.wfsPaused)
			{
                Proxy.CurrentState = WorkflowState.wfsExecuting;
                Proxy._currentStep.Resume();
			}
			return true;
		}

		public WorkflowObject GetObjectByID (string objectID)
		{
			WorkflowObject result = null;
            try
            {
                foreach (WorkflowObject obj in _objects)
                {
                    if (obj.ItemID.ToUpper() == objectID.ToUpper())
                    {
                        result = obj;
                        break;
                    }
                }

                if (Parent != null && result == null)
                {
                    result = Parent.GetObjectByID(objectID);
                }
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity("KxWorkflow", "GetObjectByID", ex.Message, objectID);
            }
			return result;
		}

        public void ProxyComplete(Object sender, EventArgs e)
        {
            object Result = _proxyWorkflow.ReturnValue;
            _proxyWorkflow = null;
            Resume(Result);
        }

        public override string ToXml()
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine("<?xml version=\"1.0\" encoding=\"utf-8\" ?>");
            sb.AppendLine(String.Format("<KxWorkflow id=\"{0}\" {1}=\"{2}\" {3}=\"{4}\" {5}=\"{6}\" {7}=\"{8}\" {9}=\"{10}\">", ItemID, STR_BlockStepAttribute, BlockStep, STR_StartStepAttribute, StartStep, STR_ErrorStepAttribute, ErrorStep, STR_ScopeAttribute, Scope,STR_LogoutStepAttribute,LogoutStep));
            sb.AppendLine(Indent + "<WorkflowObjects>");

            foreach (WorkflowObject obj in _objects)
            {
                obj.IndentValue = 2;
                sb.AppendLine(obj.AsXml);
            }

            sb.AppendLine(Indent + "</WorkflowObjects>");

            sb.AppendLine(Indent + "<WorkflowSteps>");

            foreach (WorkflowStep step in _steps)
            {
                step.IndentValue = 2;
                string temp = step.AsXml;
                sb.AppendLine(temp.Replace("\r\n\r\n","\r\n"));
               // sb.AppendLine("    ");
            }

            sb.AppendLine(Indent + "</WorkflowSteps>");
            sb.AppendLine("</KxWorkflow>");
            return sb.ToString();
        }

        private bool GetIsInternal()
        {
            bool result = true;

            foreach (WorkflowStep step in Steps)
            {
                if (step.HasExternalActions)
                {
                    result = false;
                    break;
                }
            }

            return result;
        }

        public override string ValidateVerbose()
        {
            string result = "";

            if (ItemID == "")
            {
                result += "Workflow must have a name...\r\n";
            }

            if (StartStep == "")
            {
                result += "Start Step must be specified...\r\n";
            }
            else
            {
                if (FindStepByName(StartStep) == null)
                {
                    result += "Start Step must be a valid step...\r\n";
                }
            }

            if (ErrorStep == "")
            {
                result += "Error Step must be specified...\r\n";
            }
            else
            {
                if (FindStepByName(ErrorStep) == null)
                {
                    result += "Error Step must be a valid step...\r\n";
                }
            }

            foreach (WorkflowObject obj in _objects)
            {
                string ov = obj.ValidateVerbose();
                if (ov != "")
                {
                    result += obj.ItemID+":"+ov;
                }
            }

            foreach (WorkflowStep step in _steps)
            {
                string sv = step.ValidateVerbose();
                if (sv != "")
                {
                    result += step.ItemID + ":" + sv;
                }
            }

            return result;
        }
    
        public override void AddWorkflowItems(ref List<WorkflowItem> list)
        {
            base.AddWorkflowItems(ref list);

            foreach (WorkflowStep step in _steps)
            {
                step.AddWorkflowItems(ref list);
            }
        }

        internal void StatsLog(string activity)
        {
            if (ParentWorkflow != null)
            {
                ParentWorkflow.StatsLog(activity);
            }
            else
            {
                StatsLogger.LogStats(activity);
            }
        }

    }
}
