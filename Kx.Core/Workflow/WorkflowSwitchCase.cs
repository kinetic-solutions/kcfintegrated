using System;
using System.Text;
using System.Xml;
using System.Collections;
using System.Collections.Generic;
using Kx.Core.Exceptions;

namespace Kx.Core.Workflow
{
    public enum CaseComparisonType { EqualTo, NotEqualTo, GreaterThan,GreaterThanEqualTo, LessThan, LessThanOrEqualTo, StartsWith, NotStartsWith, EndsWith, NotEndsWith, IsBlank, IsNotBlank, In, NotIn };
	
    /// <summary>
	/// Declaration of each case within a switch statement.  The value is a single string comparison value.
    ///   <Case value="Case Value">
    ///     If case statement is met, an array of actions which should then be carried out.
    ///     ...
    ///   </Case>
	/// </summary>
	/// 
	[Serializable]
	public class WorkflowSwitchCase : WorkflowItem
	{

        protected List<WorkflowAction> _actions = new List<WorkflowAction>();
		protected string _value = null;
        protected CaseComparisonType _comparisonType = CaseComparisonType.EqualTo;

        public List<WorkflowAction> Actions
        {
            get
            {
                return _actions;
            }
        }

        public string Value
        {
            get
            { return _value; }
            set
            { _value = value; }
        }

		public WorkflowSwitchCase(XmlNode xmlNode, Workflow parentWorkflow) : base (xmlNode, parentWorkflow)
		{
		}

		protected override void GetXmlInfo()
		{
			base.GetXmlInfo ();

			_value = GetAttribute("value");
            _comparisonType = StringToCaseComparisonType(GetAttribute("comparisontype"));


			foreach (XmlNode node in _xmlNode.ChildNodes)
			{
				_actions.Add(_parentWorkflow.GetWorkflowAction(node));
			}
		}

	    protected override string DebugText()
	    {
	        return string.Format("Case: Value \"{0}\"", Value);
	    }

	    public bool IsCase (string sourceValue)
		{
			bool result = false;

			try
			{
                if (_value.ToUpper() == "DEFAULT")
                {
                    result = true;
                }
				else if (_value != null )
				{
                    if (sourceValue != null)
                    {
                        result = CaseCompare(sourceValue, _value);
                    }
                    else
                    {
                        result = _value == "";
                    }
				}
			}
			catch
			{
			    result = false;
			}
			return result;
		}

        public bool CaseCompare(string left, string right)
        {
            left = left.ToUpper();
            right = right.ToUpper();

            switch (_comparisonType)
            {
                case CaseComparisonType.EqualTo: return left == right;
                case CaseComparisonType.NotEqualTo: return left != right;
                case CaseComparisonType.GreaterThan: return String.Compare(left,right) > 0;
                case CaseComparisonType.GreaterThanEqualTo: return String.Compare(left,right) >= 0;
                case CaseComparisonType.LessThan: return String.Compare(left,right) < 0;
                case CaseComparisonType.LessThanOrEqualTo: return String.Compare(left,right) <= 0;
                case CaseComparisonType.StartsWith: return left.StartsWith(right);
                case CaseComparisonType.NotStartsWith: return !left.StartsWith(right);
                case CaseComparisonType.EndsWith: return left.EndsWith(right);
                case CaseComparisonType.NotEndsWith: return !left.EndsWith(right);
                case CaseComparisonType.IsBlank: return left == String.Empty;
                case CaseComparisonType.IsNotBlank: return left != String.Empty;
                case CaseComparisonType.In: return left.Contains(right);
                case CaseComparisonType.NotIn: return !left.Contains(right);
                default: return left == right;
            }
        }

		public override bool Execute()
		{
            base.Execute();
            bool result = false;

			try
			{
				foreach (WorkflowAction a in _actions)
				{
					if (!a.Execute())
					{
						break;
					}
				}

                result = true;
			}
			catch (Exception ex)
			{
                KxLogger.LogActivity(this, "Execute", ex.Message, "Current step: " + ParentWorkflow.CurrentStepName + ", XML: " + ParentWorkflow.CurrentStep.AsXml); // pha
                // pha
                if (!ex.Message.StartsWith("Thread was being aborted"))
                    if (ex.Message.Contains("Workflow: "))
                        throw new Exception(ex.Message);
                    else
                        throw new Exception(ex.Message + ". Workflow: " + ParentWorkflow.ItemID + ", Current step: " + ParentWorkflow.CurrentStepName);    // pha
                result = false;
			}

            return result;
		}

        public override string ToXml()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(Indent + "<Case value=\"" + _value + "\"");
             
            if (_comparisonType != CaseComparisonType.EqualTo)
            {
                sb.Append(" comparisontype=\""+ CaseComparisonTypeToString(_comparisonType)+"\"");
            }
                

            sb.Append(">");

            foreach (WorkflowAction action in _actions)
            {
                action.IndentValue = IndentValue + 1;
                sb.AppendLine(action.AsXml);
            }

            sb.AppendLine(Indent + "</Case>");

            return sb.ToString();
        }

        public override string ValidateVerbose()
        {
            string result = "";

            foreach (WorkflowAction action in _actions)
            {
                string av = action.ValidateVerbose();
                if (av != "")
                {
                    result += av;
                }
            }
            return result;
        }

        public override void AddWorkflowItems(ref List<WorkflowItem> list)
        {
            base.AddWorkflowItems(ref list);

            foreach (WorkflowAction action in _actions)
            {
                action.AddWorkflowItems(ref list);
            }
        }

        public override string ToString()
        {
            return _value;  
        }


        public string CaseComparisonTypeToString(CaseComparisonType source)
        {
            string result = "";

            switch (source)
            {
                case CaseComparisonType.EqualTo: result = "equalto";break;
                case CaseComparisonType.NotEqualTo: result = "notequal";break;
                case CaseComparisonType.GreaterThan: result = "greaterthan";break;
                case CaseComparisonType.GreaterThanEqualTo: result = "greaterthanequal";break;
                case CaseComparisonType.LessThan: result = "lessthan";break;
                case CaseComparisonType.LessThanOrEqualTo: result = "lessthanequal";break;
                case CaseComparisonType.StartsWith: result = "startswith";break;
                case CaseComparisonType.NotStartsWith: result = "notstartswith";break;
                case CaseComparisonType.EndsWith: result = "endswith";break;
                case CaseComparisonType.NotEndsWith: result = "notendswith";break;
                case CaseComparisonType.IsBlank: result = "isblank";break;
                case CaseComparisonType.IsNotBlank: result = "isnotblank";break;
                case CaseComparisonType.In: result = "contains";break;
                case CaseComparisonType.NotIn: result = "notcontains"; break;
                default: result = "eq";break;
            }

            return result;
        }

        public CaseComparisonType StringToCaseComparisonType(string source)
        {
            CaseComparisonType result = CaseComparisonType.EqualTo;

            switch (source.ToLower())
            {
                case "equalto": result = CaseComparisonType.EqualTo; break;
                case "notequal": result=CaseComparisonType.NotEqualTo; break;
                case "greaterthan": result=CaseComparisonType.GreaterThan; break;
                case "greaterthanequal": result=CaseComparisonType.GreaterThanEqualTo; break;
                case "lessthan": result=CaseComparisonType.LessThan; break;
                case "lessthanequal": result=CaseComparisonType.LessThanOrEqualTo; break;
                case "startswith": result=CaseComparisonType.StartsWith; break;
                case "notstartswith": result=CaseComparisonType.NotStartsWith; break;
                case "endswith": result=CaseComparisonType.EndsWith; break;
                case "notendswith": result=CaseComparisonType.NotEndsWith; break;
                case "isblank": result=CaseComparisonType.IsBlank; break;
                case "isnotblank": result=CaseComparisonType.IsNotBlank; break;
                case "contains": result=CaseComparisonType.In; break;
                case "notcontains": result=CaseComparisonType.NotIn; break;
                default: result = CaseComparisonType.EqualTo; break;
            }

            return result;
        }

	}
}
