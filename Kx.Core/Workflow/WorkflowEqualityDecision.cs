using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Kx.Core.Workflow
{
    /// <summary>
    ///   Following declaration of the decision, there follows an array of EqualityType decisions:
    ///   <EqualityType/>...
    /// 
    ///   NOTE: The equality type requires a left AND right value source.
    /// </summary>
    public class WorkflowEqualityDecision : WorkflowDecision
    {
        protected List<WorkflowEqualityType> _equalityTypes = new List<WorkflowEqualityType>();

        private object DerivedLeftValue = null;
        private object DerivedRightValue = null;

        public List<WorkflowEqualityType> EqualityTypes
        {
            get
            {
                return _equalityTypes;
            }
        }

        public WorkflowEqualityDecision(XmlNode xmlNode, Workflow parentWorkflow)
            : base(xmlNode, parentWorkflow)
        {
        }

        protected override void GetXmlInfo()
        {
            base.GetXmlInfo();

            LoadEqualityTypes();
        }

        protected void LoadEqualityTypes()
        {
            _equalityTypes.Clear();

            XmlNodeList nodes = SelectNodesInMyNode("/Decision/Equality");

            foreach (XmlNode node in nodes)
            {
                _equalityTypes.Add(new WorkflowEqualityType(node, _parentWorkflow, this));
            }
        }

        public override bool Execute()
        {
            bool result = false;

            DerivedLeftValue = LeftValue.GetValue();
            DerivedRightValue = RightValue.GetValue();

            base.Execute();
            foreach (WorkflowEqualityType et in _equalityTypes)
            {
                if (et.ComparisonMatch(DerivedLeftValue, DerivedRightValue))
                {
                    et.Execute();
                    break;
                }
            }

            result = true;

            return result;
        }

        protected override string DebugText()
        {
            return string.Format("Equality descision: {0} - {1}", DerivedLeftValue, DerivedRightValue);
        }

        public override string ToXml()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(Indent + "<Decision type=\"equality\" >");

            foreach (WorkflowEqualityType eqtype in _equalityTypes)
            {
                eqtype.IndentValue = IndentValue + 1;
                sb.AppendLine(eqtype.AsXml);
            }

            sb.AppendLine(Indent + "</Decision>");

            return sb.ToString();
        }

        public override string ValidateVerbose()
        {
            string result = "";

            foreach (WorkflowEqualityType et in _equalityTypes)
            {
                string vv = et.ValidateVerbose();
                if (vv != "")
                {
                    result += vv;
                }
            }
            return result;
        }

        public override void AddWorkflowItems(ref List<WorkflowItem> list)
        {
            base.AddWorkflowItems(ref list);
            foreach (WorkflowEqualityType eqtype in _equalityTypes)
            {
                eqtype.AddWorkflowItems(ref list);
            }
        }
    }
}
