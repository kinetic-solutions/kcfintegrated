using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Exceptions;

namespace Kx.Core.Workflow
{
    /// <summary>
    ///   Within an equality decision, it is important to cover each type of equality required:
    ///   <Equality type="[EQ|LT|LE|GT|GE]">
    ///     Within the equality type declaration, an array of actions to be carried out if the equality is met.
    ///      ...
    ///   </Equality>
    /// </summary>
    public class WorkflowEqualityType : WorkflowItem
    {
        /// <summary>
        /// Acceptable values for equality:
        /// EQ : Equals (DEFAULT)
        /// LT : Less than
        /// LE : Less than or equal to
        /// GT : Greater than
        /// GE : Greater than or equal to
        /// </summary>
        protected string _comparisonType = "EQ";
        protected WorkflowDecision _parentdecision = null;

        protected List<WorkflowAction> _actions = new List<WorkflowAction>();

        public string ComparisonType
        {
            get
            { return _comparisonType; }
            set
            { _comparisonType = value; }
        }

        public List<WorkflowAction> Actions
        {
            get
            {
                return _actions;
            }
        }
        
        public WorkflowEqualityType(XmlNode xmlNode, Workflow parentWorkflow, WorkflowDecision parentDecision)
            : base(xmlNode, parentWorkflow)
        {
            _parentdecision = parentDecision;
        }

        protected override void GetXmlInfo()
        {
            base.GetXmlInfo();

            _comparisonType = GetAttribute("type");

            foreach (XmlNode node in _xmlNode.ChildNodes)
            {
                _actions.Add(_parentWorkflow.GetWorkflowAction(node));
            }
        }

        protected override string DebugText()
        {
            return ComparisonType;
        }

        public bool ComparisonMatch(object leftValue, object rightValue)
        {
            switch (_comparisonType)
            {
                case "EQ":
                    return CompareEquals(leftValue, rightValue);
                case "LT":
                    return CompareLessThan(leftValue, rightValue);
                case "LE":
                    return CompareLessThan(leftValue, rightValue) || CompareEquals(leftValue, rightValue);
                case "GT":
                    return CompareGreaterThan(leftValue, rightValue);
                case "GE":
                    return CompareGreaterThan(leftValue, rightValue) || CompareEquals(leftValue, rightValue);
                default:
                    return false;
            }
        }

        protected bool CompareGreaterThan(object leftValue, object rightValue)
        {
            return CompareLessThan(rightValue, leftValue);
        }

        protected bool CompareLessThan(object leftValue, object rightValue)
        {
            bool result = false;

            if (leftValue != null && rightValue != null)
            {
                if (leftValue is IComparable)
                {
                    try
                    {
                        result = CompareIComparable((IComparable)leftValue, rightValue) < 0;
                    }
                    catch (ArgumentException ex)
                    {
                        KxLogger.LogActivity(this, "CompareLessThan", ex.Message, "");
                        result = false; 
                    }
                }
                else if (rightValue is IComparable)
                {
                    try
                    {
                        result = CompareIComparable((IComparable)rightValue, leftValue) > 0;
                    }
                    catch (ArgumentException ex)
                    {
                        KxLogger.LogActivity(this, "CompareLessThan2", ex.Message, ""); 
                        result = false;
                    }
                }
                else
                {
                    result = CompareIComparable(leftValue.ToString(), rightValue.ToString()) < 0;
                }
            }
            return result;
        }

        protected int CompareIComparable(IComparable leftValue, object rightValue)
        {
            return leftValue.CompareTo(rightValue);
        }

        protected bool CompareEquals(object leftValue, object rightValue)
        {
            bool result = false;

            if (leftValue != null && rightValue != null)
            {
                if (leftValue is IComparable)
                {
                    try
                    {
                        result = CompareIComparable((IComparable)leftValue, rightValue) == 0;
                    }
                    catch (ArgumentException ex )
                    {
                        KxLogger.LogActivity(this, "CompareEquals", ex.Message, ""); 
                        result = false;
                    }
                }
                else if (rightValue is IComparable)
                {
                    try
                    {
                        result = CompareIComparable((IComparable)rightValue, leftValue) == 0;
                    }
                    catch (ArgumentException ex)
                    {
                        KxLogger.LogActivity(this, "CompareEquals", ex.Message, ""); 
                        result = false;
                    }
                }
                else if (leftValue.GetType() == rightValue.GetType())
                {
                    result = leftValue == rightValue;
                }
                else
                {
                    result = leftValue.ToString() == rightValue.ToString();
                }
            }

            return result;
        }

        public override bool Execute()
        {
            base.Execute();
            bool result = false;

            try
            {
                foreach (WorkflowAction action in _actions)
                {
                    if ((!action.Execute()))
                        break;
                }

                result = true;
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "Execute", ex.Message, "");
            }

            return result;
        }

        public override string ToXml()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(Indent+"<Equality type=\""+_comparisonType+"\" >");

            foreach (WorkflowAction action in _actions)
            {
                action.IndentValue = IndentValue + 1;
                sb.AppendLine(action.AsXml);
            }
            sb.AppendLine(Indent+"</Equality>");
            return sb.ToString();
        }

        public override string ValidateVerbose()
        {
            string result = "";

            if (_comparisonType != "EQ" && _comparisonType != "LT" && _comparisonType != "LE" &&
                _comparisonType != "GT" && _comparisonType != "GE")
            {
                result += "Invalid Comparison Type : "+_comparisonType+"\r\n";
            }

            foreach (WorkflowAction action in _actions)
            {
                string av = action.ValidateVerbose();
                if (av != "")
                {
                    result += av + "\r\n";
                }
            }
            return result;
        }

        public override void AddWorkflowItems(ref List<WorkflowItem> list)
        {
            base.AddWorkflowItems(ref list);

            foreach (WorkflowAction action in _actions)
            {
                action.AddWorkflowItems(ref list);
            }
        }

        public override string ToString()
        {
            switch (_comparisonType)
            {
                case "EQ": return " = ";
                case "LT": return " < ";
                case "LE": return " <= ";
                case "GT": return " > ";
                case "GE": return " >= ";
                default: return "Unknown";
            }
        }
    }
}
