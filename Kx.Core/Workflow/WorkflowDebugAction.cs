using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.IO;
using Kx.Core.Exceptions;

namespace Kx.Core.Workflow
{
    /// <summary>
    /// Debug action in order to output to a file the values of any value source contained within the workflow.
    /// 
    /// Xml output is
    /// 
    /// <Debug debugfilename="filename">
    ///   <ValueSource   ></ValueSource>
    ///   <ValueSource   ></ValueSource>
    ///   <ValueSource   ></ValueSource>
    /// </Debug>
    /// 
    /// </summary>
    [Serializable]
    public class WorkflowDebugAction : WorkflowAction
    {
        private List<WorkflowValueSource> _valueSources = null;

        public WorkflowDebugAction(XmlNode xmlNode, Workflow parentWorkflow)
            : base(xmlNode, parentWorkflow)
        {
        }

        protected override void GetXmlInfo()
        {
            base.GetXmlInfo();

            XmlNodeList valueSourceNodes = SelectNodesInMyNode("/Debug/ValueSource");
            _valueSources = new List<WorkflowValueSource>();
            foreach (XmlNode node in valueSourceNodes)
            {
                _valueSources.Add(new WorkflowValueSource(node, _parentWorkflow));
            }
        }

        public override bool Execute()
        {
            base.Execute();
            try
            {
                KxLogger.LogActivity(this, "Workflow.DEBUG", this.ToString(), this.ParentWorkflow.CurrentStep.ItemID);
                foreach (WorkflowValueSource value in _valueSources)
                {
                    KxLogger.LogActivity(this, "Workflow.DEBUG", string.Format("Type=\"{2}\" ObjectID=\"{3}\" Property=\"{5}\" Value=\"{4}\"", DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss"), ItemID, value.Type, value.ObjectID, value.GetValue(), value.Value), this.ParentWorkflow.CurrentStep.ItemID);
                }
            }
            catch (Exception e)
            {
                KxLogger.LogActivity(this, "Execute", e.Message, string.Empty);
            }

            return true;
        }

        protected override string DebugText()
        {
            return "Debug";
        }

        public override string ToXml()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(Indent + string.Format("<Debug>"));

            if (_valueSources != null)
            {
                foreach (WorkflowValueSource source in _valueSources)
                {
                    sb.AppendLine(source.AsXml);
                }
            }
            sb.AppendLine(Indent + "</Debug>");

            return sb.ToString();
        }
    }
}
