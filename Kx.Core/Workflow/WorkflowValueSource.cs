using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Reflection;
using Kx.Core.Utility;
using Kx.Core.Exceptions;

namespace Kx.Core.Workflow
{
	/// <summary>
    ///   When retrieving a value from a known object property
	///   <ValueSource type="object" objectid="idofobject" value="propertyofobject"/>
    /// 
    ///   When retrieving a value from a base object (string, int, date etc)
    ///   or reference to another object already existing
    ///   <ValueSource type="object" objectid="idofobject"/>
    /// 
    ///   When value is a fixed value in a base format (string, int, date etc)
    ///   <ValueSource type="[string|int|integer|date|datetime]" value="valuetoupdateofsametype" format"notimplemented"/>
    /// 
    ///   The valuesource can also be a method of an object sets the value to be the return value of the method
    ///   <ValueSource type="invoke" objectid="idofobject" method="methodname"/>
    /// 
    ///   The invoke option can also be passed parameters (See WorkflowInvokeMethodAction)
    ///   <ValueSource type="invoke" objectid="idofobject" method="parammethodname">
    ///       <ValueSource/>...
    ///   </ValueSource>
	/// </summary>
    /// 

	public class WorkflowValueSource : WorkflowItem
	{
		protected string _type = "string";
        protected string _value = string.Empty;
        protected string _format = string.Empty;
        protected string _objectID = string.Empty;
        protected WorkflowInvokeMethodAction _invokeSource = null;

        public string Type
        {
            get
            { return _type; }
            set
            { _type = value; }
        }

        public string Value
        {
            get
            { return _value; }
            set
            { _value = value; }
        }

        public string Format
        {
            get
            { return _format; }
            set
            { _format = value; }
        }

        public string ObjectID
        {
            get
            { return _objectID; }
            set
            { _objectID = value; }
        }

        public WorkflowInvokeMethodAction InvokeSource
		{
            get
            { return _invokeSource; }
            set
            { _invokeSource = value; }
        }

		public WorkflowValueSource(XmlNode xmlNode, Workflow parentWorkflow) : base (xmlNode, parentWorkflow)
		{
			if (_type == "invoke")
			{
				_invokeSource = new WorkflowInvokeMethodAction(xmlNode, parentWorkflow);
			}
		}

		protected override void GetXmlInfo()
		{
			base.GetXmlInfo ();
			_type = GetAttribute("type");
			_value = GetAttribute("value");
			_format = GetAttribute("format");
			_objectID = GetAttribute("objectid");
		}

	    protected override string DebugText()
	    {
	        return string.Format("Value Source: property \"{0}\" on \"{1}\"", Value, ObjectID);
	    }

	    public object GetValue ()
		{
			object result = null;

			if (_invokeSource != null)
			{
				result = _invokeSource.ExecuteResult ();
			}
			else
			{
				result = DeriveValue ();
			}

            if (result != null && !String.IsNullOrEmpty(_format))
            {
                result = FormatObject(result);
            }

			return result;
		}

        private object FormatObject(object source)
        {
            object result = source;
            try
            {
                switch (_format.ToLower())
                {
                    case "string":
                        result = Convert.ToString(result);
                        break;
                    case "bool":
                        result = Convert.ToBoolean(result);
                        break;
                    case "int":
                    case "integer":
                        result = Convert.ToInt32(result);
                        break;
                    case "date":
                    case "datetime":
                        result = Convert.ToDateTime(result);
                        break;
                    case "decimal":
                        result = Convert.ToDecimal(result);
                        break;
                }
            }
            catch { }

            return result;
        }

        public bool IsTypeEquivalent(Type type)
        {
            Type _type = GetValueType();

            return _type == null || _type.Name.ToString() == type.Name.ToString();
        }

        public Type GetValueType()
        {
            Type result = null;

            object obj = GetValue();
            if (obj != null)
            {
                result = obj.GetType();
            }
            return result;
        }

		string GetStringValue ()
		{
			return _value;
		}

        Guid GetGuidValue()
        {
            return new Guid(_value);
        }

		int GetIntegerValue ()
		{
			int result = 0;
			try
			{
				result = int.Parse(_value);
			}
			catch
			{

			}
			return result;
		}

        bool GetBooleanValue()
        {
            bool result = false;
            try
            {
                result = Boolean.Parse(_value);
            }
            catch
            {}
            return result;
        }

		DateTime GetDateTimeValue ()
		{
			DateTime result = DateTime.Now;

			try
			{
                result = KxDateTime.Parse(_value);
			}
			catch
			{
			}

			return result;
		}

        Decimal GetDecimalValue()
        {
            Decimal result = 0.00M;

            try
            {
                result = Convert.ToDecimal(_value);
            }
            catch
            { }
            return result;
        }

		object GetObjectValue ()
		{
			object result = null;

			if (_objectID != string.Empty)
			{
				WorkflowObject o = _parentWorkflow.GetObjectByID(_objectID);
				if (o != null)
				{
                    if (_value != string.Empty)
                    {
                        result = KxReflection.GetPropertyObject(o.InternalObject, _value);
                    }
                    else
                    {
                        result = o.InternalObject;
                    }
				}
			}
			return result;
		}

		object DeriveValue ()
		{
			object result = null;
			try
			{
				switch (_type.ToLower())
				{
                    case "null":
                        result = null;
                        break;
					case "string":
						result = GetStringValue ();
						break;
                    case "bool":
                        result = GetBooleanValue();
                        break;
					case "int":
					case "integer":
						result = GetIntegerValue ();
						break;
					case "date":
					case "datetime":
						result = GetDateTimeValue ();
						break;
                    case "guid":
                        result = GetGuidValue();
                        break;
                    case "decimal":
                        result = GetDecimalValue();
                        break;
					case "object":
                        result = GetObjectValue();
                        break;
                    case "workflow":
                        result = GetWorkflowValue();
                        break;
                    case "external":
                        result = _parentWorkflow.ExternalResult;
                        break;
				}
			}
			catch (Exception ex)
			{
                KxLogger.LogActivity(this, "DeriveValue", ex.Message, _type + ':' + _objectID + ", value: " + _value);
                throw new Exception(ex.Message);    // pha
			}
           
			return result;
		}

        protected object GetWorkflowValue()
        {
            return _parentWorkflow.Manager.ExecuteRule(_objectID, _parentWorkflow);
        }

        public override string ToXml()
        {
            if (_invokeSource != null)
            {
                StringBuilder sb = new StringBuilder();

                sb.AppendLine(Indent + "<ValueSource type=\"" + _type + "\" objectid=\"" + _objectID + "\" method=\"" + _invokeSource.Method + "\">");
                _invokeSource.IndentValue = IndentValue + 2;

                foreach (WorkflowValueSource src in _invokeSource.Parameters)
                {
                    sb.AppendLine(Indent + src.ToXml());
                }

                sb.AppendLine(Indent + "</ValueSource>");

                return sb.ToString();
            }
            else
            {
                return Indent + "<ValueSource type=\"" + _type + "\" objectid=\"" + _objectID + "\" value=\"" + _value + "\" format=\"" + _format + "\" />";
            }
        }
         
        public override string ValidateVerbose()
        {
            string result = "";

            switch (_type)
            {
                case "int":
                case "integer":
                    try
                    {
                        Convert.ToInt32(_value);
                    }
                    catch
                    {
                        result += "Value ie not a valid integer\r\n";
                    }
                    break;
                case "date":
                case "datetime":
                    try
                    {
                        KxDateTime.Parse(_value);
                    }
                    catch
                    {
                        result += "Value ie not a valid Date\r\n";
                    }
                    break;
                case "guid":
                    try
                    {
                        Guid dummy = new Guid(_value);
                    }
                    catch
                    { result += "Value ie not a valid Guid\r\n"; }
                    break;
                case "object":
                    if (_objectID == string.Empty || _value == string.Empty)
                    {
                        result += "Get property requires a workflow object and a property to be specified\r\n";
                    }
                    break;
                case "workflow":
                    if (_objectID == "")
                    {
                        result += "Workflow name must be specified\r\n";
                    }
                    break;
                case "string": break;
                case "external": break;
                default :
                    if (_invokeSource == null)
                    {
                        result += "Type must be specified\r\n";
                    }
                    break;
            }

            if (_invokeSource != null)
            { 
                result += _invokeSource.ValidateVerbose(); 
            }
            return result;
        }

        public override void AddWorkflowItems(ref List<WorkflowItem> list)
        {
            base.AddWorkflowItems(ref list);

            if (_invokeSource != null)
            {
                _invokeSource.AddWorkflowItems(ref list);
            }
        }

        public override string  ToString()
        {
            if (InvokeSource != null)
            {
                return InvokeSource.ToString();
            }
            else if (Type == "workflow")
            {
                return "Workflow: " + Value;
            }
            else if (Type == "object")
            {
                return "Property: " +ObjectID+"."+Value;
            }
            else if (Type == "external")
            {
                return "External";
            }
            else if (Type == "null")
            {
                return "Null";
            }
            else
            {
                return "Value: " + Value;
            }
        }   
	}
}
