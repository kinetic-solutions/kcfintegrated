using System;
using System.Xml;

namespace Kx.Core.Workflow
{
	/// <summary>
    ///   This fires the OnComplete event within the workflow.
	///   <Complete/>
	/// </summary>
	/// 
	
    [Serializable]
	public class WorkflowCompleteAction : WorkflowAction
	{
		public WorkflowCompleteAction(XmlNode xmlNode, Workflow parentWorkflow) : base (xmlNode, parentWorkflow)
		{
		}

		public override bool Execute()
		{
		    base.Execute();
			_parentWorkflow.CurrentState = Workflow.WorkflowState.wfsComplete;

			if (_parentWorkflow.OnComplete != null)
			{
				_parentWorkflow.OnComplete(_parentWorkflow, null);
			}

			return true;
		}

	    protected override string DebugText()
	    {
	        return "Workflow completed";
	    }

	    public override string ToXml()
        {
            return Indent+"<Complete />";
        }
	}
}
