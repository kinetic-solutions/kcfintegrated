using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Exceptions;
using Kx.Core.Utility;
using System.Reflection;

namespace Kx.Core.Workflow
{
	/// <summary>
	/// Summary description for WorkflowShowFormAction.
	/// </summary>
	public class WorkflowExternalAction : WorkflowAction
	{
        protected SortedList<string, WorkflowExternalValueSource> _values = new SortedList<string, WorkflowExternalValueSource>();
        protected string _actionID = string.Empty;
		protected string _objectId = string.Empty;
        protected string _config = string.Empty;
        protected string _rule = string.Empty;
        protected string _title = string.Empty;
        protected bool _log = false;

        protected int _progress = 0;
        protected DateTime? timeStarted = null;

        public string ActionID
        {
            get
            { return _actionID; }
            set
            { 
                _actionID = value; 
            }
        }

        public string ObjectId
        {
            get
                 { return _objectId; }
            set
                 { _objectId = value; }
        }

        public string Config
        {
            get
            { return _config; }
            set
            { _config = value; }
        }

        public int Progress
        {
            get
            { return _progress; }
            set
            { _progress = value; }
        }

        public string Rule
        {
            get
            { return _rule; }
            set
            { _rule = value; }
        }

        public string Title
        {
            get
            { return _title; }
            set
            { _title = value; }
        }

        public SortedList<string,WorkflowExternalValueSource> Values
        {
            get
            {
                return _values;      
            }
            set
            {
                _values = value;
            }
        }

        public DateTime? TimeStarted
        {
            get
            {
                return timeStarted;
            }
            set
            {
                timeStarted = value;
            }
        }

        public bool Log
        {
            get
            {
                return _log;
            }
            set
            {
                _log = value;
            }
        }


        public WorkflowExternalAction(XmlNode xmlNode, Workflow parentWorkflow) : base(xmlNode, parentWorkflow)
		{
		}

		protected override void GetXmlInfo()
		{
			base.GetXmlInfo ();

            _actionID = GetAttribute("actionid");
			_objectId = GetAttribute("objectid");
            _config = GetAttribute("config");
            _rule = GetAttribute("rule");
            _title = GetAttribute("title");
            _log = KxStringUtils.ConvertToBool(GetAttribute("log"),false);
            _progress = KxStringUtils.ConvertToInt(GetAttribute("progress"),0);

            LoadValues();

            
		}

	    protected override string DebugText()
	    {
	        return string.Format("External action \"{0}\" using \"{1}\" title \"{2}\"", ActionID, ObjectId, Title);
	    }

	    void LoadValues()
        {
            _values.Clear();

            if (_xmlNode != null)
            {
                XmlNodeList nodes = _xmlNode.SelectNodes("Values/extenderValue");

                foreach (XmlNode n in nodes)
                {
                    WorkflowExternalValueSource esv = new WorkflowExternalValueSource(n, ParentWorkflow);

                    if (!_values.ContainsKey(esv.UniqueKey))
                    {
                        _values.Add(esv.UniqueKey, esv);
                    }
                }
            }

            if (!String.IsNullOrEmpty(_config) && Values.Count == 0)
            {
                ConvertValues();
            }

            object provider = ParentWorkflow.GetObjectByID(_objectId).InternalObject;

            if (provider != null)
            {
                Type typ = provider.GetType();

                foreach (PropertyInfo pi in typ.GetProperties())
                {
                    if (pi.CanWrite && pi.CanRead && !pi.Name.ToUpper().StartsWith("UI") && !pi.Name.ToUpper().StartsWith("PAGEREND") && !(pi.Name.ToUpper() == "WORKFLOWRESULT") && !(pi.Name.ToUpper() == "PROGRESS"))
                    {
                         if (!_values.ContainsKey(pi.Name.ToUpper()+":BOTH") && !_values.ContainsKey(pi.Name.ToUpper()+":LIVE") && !_values.ContainsKey(pi.Name.ToUpper()+":TEST") )
                         {
                            WorkflowExternalValueSource wsv = new WorkflowExternalValueSource(ParentWorkflow);
                            wsv.ExtenderPropertyName = pi.Name;
                            wsv.Applicability = ApplicabilityMode.Both;
                            wsv.Type = "string";
                            try
                            {
                                wsv.Value = pi.GetValue(provider, null).ToString();
                            }
                            catch (TargetInvocationException)
                            {
                                KxLogger.LogActivity(this, "LoadValues", string.Format("Property {0} has not been set from workflow, missing extender property?", pi.Name), "");
                            }

                            _values.Add(wsv.UniqueKey, wsv);                           
                         }
                    }
                }

            }

        }

        void ConvertValues()
        {
            string xml = KxBase64.Base64Decode(Config);
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);

            XmlNodeList nodes = doc.SelectNodes("/configuration/property");

            foreach (XmlNode node in nodes)
            {
                string name = KxXmlUtils.GetNodeValue(node, "name", "");
                string value = KxXmlUtils.GetNodeValue(node, "value", "");

                WorkflowExternalValueSource wevs = new WorkflowExternalValueSource(ParentWorkflow);
                wevs.ApplicabilityString = "BOTH";
                wevs.Type = "string";
                wevs.Value = value;
                wevs.ExtenderPropertyName = name;

                Values.Add(wevs.UniqueKey, wevs);
            }

            Config = "";
        }

		public override bool Execute()
		{
		    base.Execute();
			bool result = false;
            _parentWorkflow.CurrentState = Workflow.WorkflowState.wfsPaused;
            _parentWorkflow.ExternalAction(this);
                
			return result;
		}

        public override string ToXml()
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine(Indent + "<ExternalAction objectid=\"" + _objectId + "\" actionid=\"" + _actionID + "\" rule=\"" + _rule + "\" title=\"" + _title + "\" progress=\"" + Progress.ToString() + "\" log=\""+Log.ToString()+"\" >");

            sb.AppendLine(Indent + "    <Values>");

            foreach (WorkflowExternalValueSource evs in Values.Values)
            {
                sb.AppendLine(Indent + "    " + evs.ToXml());
            }

            sb.AppendLine(Indent + "    </Values>");

            sb.AppendLine(Indent + "</ExternalAction>");

            return sb.ToString();
        }

        public override string ValidateVerbose()
        {
            string result = "";

            if (_actionID == "")
            {
                result += "Extender name must be supplied (use standard extender name)...\r\n";
            }
                      
            if (_objectId == "")
            {
                result += "Page name name must be supplied...\r\n";
            }
            
            if (_title == "")
            {
                result += "Page title must not be blank...\r\n";
            }

            return result;
        }

        public WorkflowExternalValueSource GetValueSource(string PropertyName, string applicability)
        {
            WorkflowExternalValueSource result = null;

            string Key = PropertyName.ToUpper() + ":" + applicability.ToUpper();

            if (Values.ContainsKey(Key))
            {
                result = Values[Key];
            }

            if (result == null)
            {
                if (Values.ContainsKey(PropertyName.ToUpper() + ":BOTH"))
                {
                    result = Values[PropertyName.ToUpper() + ":BOTH"];
                }
            }
            return result;
        }

        public bool IsApplicable(WorkflowExternalValueSource exsrc)
        {
            bool result = false;

            if ((ParentWorkflow.TestMode && exsrc.Applicability == ApplicabilityMode.Test) ||
               (!ParentWorkflow.TestMode && exsrc.Applicability == ApplicabilityMode.Live))
            {
                result = true;
            }
            else if (exsrc.Applicability == ApplicabilityMode.Both)
            {
                if (ParentWorkflow.TestMode)
                {
                    result = !Values.ContainsKey(exsrc.ExtenderPropertyName + ":TEST");
                }
                else
                {
                    result = !Values.ContainsKey(exsrc.ExtenderPropertyName + ":LIVE");
                }
            }
            return result;
        }
	}
}
