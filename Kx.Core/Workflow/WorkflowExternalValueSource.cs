﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Kx.Core.Workflow
{
    public enum ApplicabilityMode { Test, Live, Both };

    public class WorkflowExternalValueSource : WorkflowValueSource
    {
        protected string extenderPropertyName = "";
        protected ApplicabilityMode applicability = ApplicabilityMode.Both;

        public string ExtenderPropertyName
        {
            get
            {
                return extenderPropertyName;
            }
            set
            {
                extenderPropertyName = value;
            }
        }

        public ApplicabilityMode Applicability
        {
            get
            {
                return applicability;
            }
            set
            {
                applicability = value;
            }
        }

        public string ApplicabilityString
        {
            get
            {
                return ApplicabilityToString(applicability);
            }
            set
            {
                applicability = StringToApplicability(value);
            }
        }

        public string UniqueKey
        {
            get
            {
                return ExtenderPropertyName.ToUpper() + ":" + ApplicabilityString.ToUpper();
            }
        }

        public string UniqueKeyBoth
        {
            get
            {
                return ExtenderPropertyName.ToUpper() + ":BOTH";
            }
        }

    
        public WorkflowExternalValueSource(Workflow parentWorkflow)
            : base(null,parentWorkflow)
        {
          
        }

        public WorkflowExternalValueSource(XmlNode xmlNode, Workflow parentWorkflow)
            : base(xmlNode, parentWorkflow)
		{
			
		}

        protected override void GetXmlInfo()
        {
            base.GetXmlInfo();
            extenderPropertyName = GetAttribute("property");

            ApplicabilityString = GetAttribute("applicability").ToUpper();
        }

        public ApplicabilityMode StringToApplicability(string source)
        {
            switch (source.ToUpper())
            {
                case "TEST": return ApplicabilityMode.Test;
                case "LIVE": return ApplicabilityMode.Live;
                default: return ApplicabilityMode.Both;
            }
        }

        public string ApplicabilityToString(ApplicabilityMode source)
        {
            switch (applicability)
            {
                case ApplicabilityMode.Test: return "TEST";
                case ApplicabilityMode.Live:  return "LIVE";
                default: return "BOTH";
            }
        }

        public override string ToXml()
        {
            if (_invokeSource != null)
            {
                StringBuilder sb = new StringBuilder();

                sb.AppendLine(Indent + "<extenderValue property=\"" + extenderPropertyName + "\" type=\"" + _type + 
                    "\" objectid=\"" + _objectID + "\" method=\"" + _invokeSource.Method +  
                    "\" applicability=\"" + ApplicabilityToString(applicability) + "\">");
                _invokeSource.IndentValue = IndentValue + 2;

                foreach (WorkflowValueSource src in _invokeSource.Parameters)
                {
                    sb.AppendLine(Indent + src.ToXml());
                }

                sb.AppendLine(Indent + "</extenderValue>");

                return sb.ToString();
            }
            else
            {
                return Indent + "<extenderValue property=\"" + extenderPropertyName + "\" value=\"" + _value + "\" type=\"" + _type + "\" objectid=\"" + _objectID + 
                     "\" format=\"" + _format + "\" applicability=\"" + ApplicabilityToString(applicability) + "\" />";
            }
        }


    }
}
