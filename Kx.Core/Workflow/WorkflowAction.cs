using System;
using System.Xml;

namespace Kx.Core.Workflow
{
	/// <summary>
	/// Summary description for WorkflowAction.
	/// </summary>
	/// 
	[Serializable]
	public abstract class WorkflowAction : WorkflowItem
	{
		public WorkflowAction (XmlNode xmlNode, Workflow parentWorkflow) : base (xmlNode, parentWorkflow)
		{
		}
	}
}
