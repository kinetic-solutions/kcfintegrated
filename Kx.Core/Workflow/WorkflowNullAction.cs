using System;
using System.Xml;

namespace Kx.Core.Workflow
{
	/// <summary>
	/// This is a non-declarative element which is used as a placeholder and can't be specified in the workflow document.
	/// </summary>
	public class WorkflowNullAction : WorkflowAction
	{
		public WorkflowNullAction(XmlNode xmlNode, Workflow parentWorkflow) : base (xmlNode, parentWorkflow)
		{
		}
	}
}
