using System;
using System.IO;
using System.Xml;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using Kx.Core.Configuration;
using Kx.Core.Data;
using Kx.Core.Security;
using Kx.Core.Utility;
using Kx.Core.Exceptions;
using System.Windows.Forms;

namespace Kx.Core.Workflow
{
	/// <summary>
	/// Workflow Manager responsible for holding all
    /// the cached workflows for each user (gross)
    /// plus the overall workflow cache for site.
	/// </summary>
	/// 

    public delegate string ExternalAction(Workflow callingWorkflow, WorkflowExternalAction action);

    [Serializable]
    public class WorkflowManager
    {
        // List containing all the current workflows (basically users..)
        SortedList<Guid, Workflow> _workflows = new SortedList<Guid, Workflow>();
        // Hashtable of the workflow schemas
        static Hashtable _workflowSchema = new Hashtable(StringComparer.OrdinalIgnoreCase);
        KxBruteForceBlocker bruteForce = new KxBruteForceBlocker();
        protected int cleardownPeriod = -1;
        protected DateTime LastCleardown = DateTime.Now;

        /// <summary>
        /// Determines how long user workflows remain before cleardown
        /// </summary>
        public int CleardownPeriod
        {
            get
            {
                if (cleardownPeriod < 5)
                {
                    try
                    {
                        cleardownPeriod = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CleardownPeriod"]);
                    }
                    catch
                    { }
                }

                if (cleardownPeriod < 5)
                {
                    cleardownPeriod = 60;
                }

                return cleardownPeriod;
            }
            set
            {
                cleardownPeriod = value;
            }
        }

        public KxBruteForceBlocker BruteForce
        {
            get
            {
                return bruteForce;
            }
            set
            {
                bruteForce = value;
            }
        }


        public WorkflowManager()
        {
            if (KxDbConfiguration.GetConfigurationBool("LoadWorkflowfromDb",false))
            {
                LoadWorkflowFromDb();
            }
        }


        /// <summary>
        /// Add workflow to schema collection
        /// </summary>
        /// <param name="xmlText">workflow xml loaded from file</param>
        /// <returns></returns>
        public string AddWorkflow(string xmlText)
        {
            try
            {
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(xmlText);
                XmlNode kxWorkFlow = xmlDoc.SelectSingleNode("/KxWorkflow");
                return LoadWorkflow(kxWorkFlow);
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "AddWorkflow", ex.Message, xmlText); 
                return null;
            }
        }
        
        /// <summary>
        /// Loading workflow into the schema hashtable
        /// </summary>
        /// <param name="workflowDocument">workflow xml</param>
        /// <returns></returns>
        string LoadWorkflow(XmlNode workflowNode)
		{
            string workflowId = "";
            try
            {
                workflowId = workflowNode.Attributes["id"].Value.ToUpper();
                if (!_workflowSchema.ContainsKey(workflowId))
                {
                    _workflowSchema.Add(workflowId, workflowNode);
                }
               
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "LoadWorkflow", ex.Message, "");
            }
            return workflowId;
		}

       /// <summary>
       /// Loading workflow from database to be added to the schema cache
       /// </summary>
       void LoadWorkflowFromDb()
        {
            XmlDocument doc = KxResource.GetResourcesByType("WORKFLOW");

            XmlNodeList nodes = doc.SelectNodes(@"//resources/datarow");

            foreach (XmlNode node in nodes)
            {
                if (node != null)
                {
                    string Wfxml = KxXmlUtils.GetNodeValue(node, "data", "");

                    Wfxml = KxResource.Translate(Wfxml);

                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.LoadXml(Wfxml);
                    XmlNode kxWorkFlow = xmlDoc.SelectSingleNode("/KxWorkflow");

                    LoadWorkflow(kxWorkFlow);
                }
            }
        }

        /// <summary>
        /// Executes workflow rule - unchanged.
        /// </summary>
        /// <param name="rulename">Rule</param>
        /// <param name="parentworkflow">Workflow</param>
        /// <returns></returns>
        public object ExecuteRule(string rulename, Workflow parentworkflow)
        {
            object result = null;

            if (rulename != string.Empty)
            {
                Workflow wflow = GetWorkflow(rulename, parentworkflow);
                wflow.Execute();
                result = wflow.ReturnValue;
            }
            return result;
        }

        public string GetWorkflowId(string source)
        {
            string result = "";

            try
            {
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(source);

                Workflow workflow = new Workflow(xmlDoc.SelectSingleNode("/KxWorkflow"), this, null);
                result = workflow.ItemID;
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "GetWorkflowId", ex.Message, "");
            }

            return result;
        }


        /// <summary>
        /// Gets a workflow from the schema cache
        /// </summary>
        /// <param name="id">Id (name) of workflow</param>
        /// <param name="parent">parent (seems to usually be null..)</param>
        /// <returns>A shiny new workflow object</returns>
        public Workflow GetWorkflow(string id, Workflow parent)
        {
            return LoadWorkflowFromCache(id, parent);
        }

        /// <summary>
        /// Create a new workflow based on the XML held in the cache
        /// </summary>
        /// <param name="Id">Id (name) of workflow</param>
        /// <param name="parent"> Usually null afaik</param>
        /// <returns>A shiny new workflow.. </returns>
        protected Workflow LoadWorkflowFromCache(string Id,Workflow parent)
        {
            Workflow result = null;

            if (_workflowSchema.ContainsKey(Id))
            {
                XmlNode workflowNode = _workflowSchema[Id] as XmlNode;
                result = new Workflow(workflowNode, this, parent);
            }
            return result;
        }

        /// <summary>
        /// Gets a new workflow started, key mechanism of KCF. 
        /// Enters the workflow into a list. Workflows Can't be shared or you get user login woes.. 
        /// </summary>
        /// <param name="name">Name of workflow to load</param>
        /// <param name="onComplete">Event handler for completion</param>
        /// <param name="onExternalAction">And for external actions.. </param>
        /// <returns>Guid of the new workflow</returns>
        public Guid StartWorkflow(string name, EventHandler onComplete, ExternalAction onExternalAction)
        {
            Guid result = Guid.Empty;

            Workflow workflow = LoadWorkflowFromCache(name, null);

            if (workflow != null)
            {
                if (onComplete != null)
                {
                    workflow.OnComplete += onComplete;
                }

                if (onExternalAction != null)
                {
                    workflow.OnExternalAction += onExternalAction;
                }

                result = workflow.Identifier;

                _workflows.Add(workflow.Identifier, workflow);
            }

            return result;
        }

        /// <summary>
        /// Gets a workflow (user) from the cache, checks if it's expired.
        /// </summary>
        /// <param name="identifier">Guid of workflow</param>
        /// <returns>Null if expired/not in cache, else the workflow</returns>
        public Workflow GetActiveWorkflowByIdentifier(Guid identifier)
        {
            Workflow flow = GetWorkflowByIdentifier(identifier);

            if (flow != null && !flow.Expired)
            {
                return flow;
            }
            else
            {
                return null;
            }
            
        }

        /// <summary>
        /// Gets a workflow (user) from the list
        /// </summary>
        /// <param name="identifier">Guid of the workflow</param>
        /// <returns>The workflow</returns>
		public Workflow GetWorkflowByIdentifier (Guid identifier)
        {
            ClearWorkflowCache();

            Workflow result = null;

            if (_workflows.ContainsKey(identifier))
            {
                result = _workflows[identifier];
            }

            return result;
        }

        /// <summary>
        /// Executes a user workflow in list
        /// </summary>
        /// <param name="identifier">Guid of the workflow</param>
        /// <returns>True if successful, false if no such workflow or fail.</returns>
		public bool ExecuteWorkFlow (Guid identifier)
		{
			Workflow workflow = GetWorkflowByIdentifier (identifier);

			bool result = false;

			if (workflow != null)
			{
				result = workflow.Execute ();
			}
			return result;
		}

        /// <summary>
        /// As with the prior method but with an override start?
        /// </summary>
        /// <param name="identifier">Guid of workflow</param>
        /// <param name="overrideStart">?</param>
        /// <returns>True if successful, false if no such workflow or fail.</returns>
        public bool ExecuteWorkFlow(Guid identifier, string overrideStart)
        {
            Workflow workflow = GetWorkflowByIdentifier(identifier);

            bool result = false;

            if (workflow != null)
            {
                result = workflow.Execute(overrideStart);
            }
            return result;
        }

        /// <summary>
        /// Nukes a particular workflow
        /// </summary>
        /// <param name="identifier">Guid of the workflow to kill off.</param>
		public void ClearWorkflow (Guid identifier)
        {
            try
            {
                if (_workflows.ContainsKey(identifier))
                {
                    _workflows[identifier] = null;
                }
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "ClearWorkflow", ex.Message, "");
            }
        }

        /// <summary>
        /// Reloads a workflow. 
        /// </summary>
        /// <param name="identifier"></param>
		public void ResetWorkflow (Guid identifier)
		{
			Workflow wf = GetWorkflowByIdentifier(identifier);
		}

        /// <summary>
        /// Loads in the workflow schema XMLs
        /// </summary>
        /// <param name="filename">Workflow file</param>
        /// <returns>The XML contents</returns>
        public string LoadWorkflowFile(string filename)
        {
            string result = "";

            try
            {
                if ((filename != "") && (filename != null))
                {
                    if (File.Exists(filename))
                    {

                        using (StreamReader r = new StreamReader(filename))
                        {
                            string xml = "";
                            xml = r.ReadToEnd();
                            r.Close();
                            result = AddWorkflow(xml);
                        }
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "LoadWorkflowFile", ex.Message, ""); 
                return result;
            }
        }

        /// <summary>
        /// Hacky routine job that checks for expired workflows and zaps them
        /// </summary>
        public void ClearWorkflowCache()
        {

            if (LastCleardown < DateTime.Now.AddMinutes(-CleardownPeriod))
            {
                LastCleardown = DateTime.Now;

                List<Guid> removes = new List<Guid>();

                try
                {
                    foreach (Workflow wflw in _workflows.Values)
                    {
                        if (wflw != null && (wflw.LastAccessed < DateTime.Now.AddMinutes(-CleardownPeriod) || wflw.Terminated))
                        {
                            wflw.Expired = true;
                            removes.Add(wflw.Identifier);
                        }
                    }
                    foreach (Guid Id in removes)
                    {
                        _workflows.Remove(Id);
                    }
                }
                catch (Exception ex)
                {
                    KxLogger.LogActivity(this, "ClearWorkflowCache", ex.Message, "");
                }
            }
        }

        /// <summary>
        /// Hacky mechanism to logout workflows when a user logs out
        /// </summary>
        /// <param name="userGuid">User guid</param>
        /// <param name="WorkflowId">Workflow not to be logged out?</param>
        /// <returns>True if able to terminate workflows.</returns>
        public bool LogOutOldUser(Guid userGuid, Guid WorkflowId)
        {
            bool result = false;

            foreach (Workflow wflw in _workflows.Values)
            {
                if (wflw.Identifier != WorkflowId && wflw.KeyObjectIdentifier == userGuid && wflw.LoggededInState)
                {
                    result = TerminateWorkflow(wflw.Identifier);
                }
            }

            return result;
        }

        /// <summary>
        /// Kills off a particular workflow (workflow.Terminate) and runs the ClearWorkflowCache routine.
        /// </summary>
        /// <param name="identifier">Guid of Workflow</param>
        /// <returns>True if nuked</returns>
        public bool TerminateWorkflow(Guid identifier)
        {
            bool result = false;
            try
            {
                Workflow wflw = GetWorkflowByIdentifier(identifier);

                if (wflw != null)
                {
                    wflw.Terminated = true;
                    ClearWorkflowCache();
                    result = true;
                }
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "TerminateWorkflow", ex.Message, "");
            }

            return result;
        }


        public bool AddStrike(string ipaddress)
        {
            return BruteForce.AddStrike(ipaddress);
        }

        /// <summary>
        /// Checks if workflow schema is present
        /// </summary>
        /// <param name="WrkfID">Name of workflow</param>
        /// <returns>True if present</returns>
        public bool WorkflowSchemaLoaded(string WrkfID)
        {
            bool result = false;

            if (_workflowSchema.ContainsKey(WrkfID))
            {
                result = true;
            }
            return result;
        }

        /// <summary>
        /// Replaces workflow with one guid with another shiny new one.. 
        /// Given that the internal identifier (guid) is set once this seems dangerous/hacky... 
        /// </summary>
        /// <param name="old">Old guid</param>
        /// <param name="newguid">New guid</param>
        /// <returns>True.. unless there's an exception.</returns>
        public bool ChangeWorkflowGuid(Guid old, Guid newguid)
        {
            bool result = true;

            try
            {
                if (_workflows.ContainsKey(old))
                {
                    Workflow wf = GetWorkflowByIdentifier(old);

                    if (wf != null)
                    {
                        _workflows.Add(newguid, wf);

                        _workflows[old] = null;
                    }
                }
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "ChangeWorkflowGuid", ex.Message, "");
                result = false;
            }
            return result;
        }
    }
}
