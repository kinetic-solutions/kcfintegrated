﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;

namespace Kx.Core.Configuration
{
    [Serializable]
    public class KxConfigurationItem : KxBaseClass
    {
        #region Attributes & Properties

        protected string customSetting;
        protected string value;
        protected string string1 = "";


        public int CustomSettingId
        {
            get { return base.dBIdentifier; }
        }


        public string CustomSetting
        {
            get { return customSetting; }
            set
            {
                AddToAudit("CustomSetting", customSetting, value);
                customSetting = value;
            }
        }

        public string Value
        {
            get { return value; }
            set
            {
                AddToAudit("Value", value, value);
                this.value = value;
            }
        }

        public string String1
        {
            get { return string1; }
            set
            {
                AddToAudit("String1", string1, value);
                string1 = value;
            }
        }

        public string UniqueKey
        {
            get
            {
                return CustomSetting.ToLower() + "." + Value.ToLower();
            }
        }

        #endregion

        public KxConfigurationItem(KxBaseClass parent)
            : base(parent)
        {

        }

        public KxConfigurationItem(KxBaseClass parent, XmlNode node)
            : base(parent)
        {
            LoadFromXml("",node);
        }

        public KxConfigurationItem(KxBaseClass parent, string customsetting, string Value)
            : base(parent)
        {
            CustomSetting = customsetting;
            value = Value;
            Load();
        }


        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("customsettings", "CustomSettings", TargetType.Table);

            result.AddKeyField("CustomSetting", CustomSetting, KxValueType.String);
            result.AddKeyField("value", value, KxValueType.String);
            
            if ((actiontype == ActionType.Update) && (CustomSettingId == 0))
            {
                result.Action = ActionType.Insert;
            }
            else
            {
                result.Action = actiontype;
            }

            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {
                result.AddParameter("string1", string1, KxValueType.String);
            }
            return result;
        }

        public override void LoadFromXml(string xml, XmlNode node)
        {
            dBIdentifier = KxXmlUtils.GetNodeValue(node, "customsettingid", 0);
            customSetting = KxXmlUtils.GetNodeValue(node, "customsetting", "");
            value = KxXmlUtils.GetNodeValue(node, "value", "");
            string temp = KxXmlUtils.GetNodeValue(node, "string1", "") +
                            KxXmlUtils.GetNodeValue(node, "string2", "") +
                            KxXmlUtils.GetNodeValue(node, "string3", "") +
                            KxXmlUtils.GetNodeValue(node, "string4", "");

            string1 = temp.Trim();
        }
    }
}
