﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.IO;
using System.Text;
using System.Xml;
using System.Configuration;
using System.Web.Configuration;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Exceptions;
using Kx.Core.Resources;
using Kx.Core.Utility;
using System.Windows.Forms;

namespace Kx.Core.Configuration
{
    public class KxDbConfiguration
    {
        [Obsolete]
        public static bool DontUseDB { get; set; }
        public static string culture = "";

        public static string Culture
        {
            get
            {
                if (String.IsNullOrEmpty(culture))
                {
                    culture = GetConfiguration("Culture","en-GB");
                }
                return culture;
            }
            set
            {
                culture = value;
                
            }
        }

        static Dictionary<string, KxWebResource> _items = null;

        private static Dictionary<string, KxWebResource> Items
        {
            get
            {
                if (_items == null)
                {
                    _items = new Dictionary<string, KxWebResource>(StringComparer.InvariantCultureIgnoreCase);
                    LoadConfiguration();
                }
                return _items;
            }
        }

        private static void LoadConfiguration()
        {
            foreach (var resource in KxWebResource.GetAllResourcesOfType("naff", "CONFIG"))
            {
                Items.Add(resource.Identifier.ToLower(), resource);
            }
        }

        public static void Reset()
        {
            _items.Clear();
        }

        public static string GetConfiguration(string key)
        {
            return GetConfiguration(key, "");
        }

        public static string GetConfiguration(string key, string Default)
        {
            string result = GetConfiguration(key, true);

            if (String.IsNullOrEmpty(result))
            {
                result = Default;
            }
            return result;
        }

        public static bool GetConfigurationBool(string key, bool Default)
        {
            // pha
            //try
            //{
            if (bool.TryParse(GetConfiguration(key, true), out bool retval))
                return retval;
            else
                return Default;
            //    return Convert.ToBoolean(GetConfiguration(key, true));
            //}
            //catch
            //{
            //    return Default;
            //}
        }

        public static int GetConfigurationInt(string key, int Default)
        {
            // pha
            if (Int32.TryParse(GetConfiguration(key, true), out int retval))
                return retval;
            else
                return Default;


            try
            {
                int result = Convert.ToInt32(GetConfiguration(key, true));

                if (result == 0)
                {
                    result =  Default;
                }
                return result;
            }
            catch
            {
                return Default;
            }
        }

        public static Decimal GetConfigurationDecimal(string key, decimal Default)
        {
            try
            {
                Decimal result = Convert.ToDecimal(GetConfiguration(key, true));

                if (result == 0.00M)
                {
                    result = Default;
                }
                return result;

            }
            catch
            {
                return Default;
            }
        }

        public static DateTime GetConfigurationDateTime(string key, DateTime Default)
        {
            try
            {
                return KxDateTime.Parse(GetConfiguration(key, true));
            }
            catch
            {
                return Default;
            }
        }

        public static string GetConfiguration(string key, bool useConfig)
        {
            
            string actualKey = key.ToLower();

            string result = "";

            try
            {
                result = GetSettingFromConfig(actualKey);
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity("KxDbConfiguration", "GetConfiguration", ex.Message, actualKey);
            }

            return result;
        }

        public static bool AddSetting(string id,string value)
        {
            bool result = true;
            if (!Items.ContainsKey(id.ToLower()))
            {
                KxWebResource cfg = new KxWebResource(null, "naff", id, 1);
                cfg.ApplicationId = "naff";
                cfg.ResourceType = "CONFIG";
                cfg.Identifier = id;
                cfg.Data = value;

                Items.Add(id.ToLower(), cfg);
                result = true;
            }
            return result;
        }

        public static string GetSettingFromConfig(string key)
        {
            string result = "";

            if (System.Configuration.ConfigurationManager.AppSettings[key] != null)
            {
                result = System.Configuration.ConfigurationManager.AppSettings[key];

                AddSetting(key, result);
            }
            return result;
        }

        public static bool SetConfiguration(string key, string value)
        {
            bool result = true;

            string searchKey = key.ToLower();

            try
            {
                if (Items.ContainsKey(searchKey))
                {
                    KxWebResource cfg = ((KxWebResource)Items[searchKey]);
                    if (cfg.Data != value)
                    {
                        cfg.Data = value;
                        cfg.Changed = true;
                        cfg.ResourceType = "CONFIG";
                        result = true;
                    }
                }
                else
                {
                    AddSetting(key, value);
                }
            }
            catch
            {
                result = false;
            }
            return result;
        }

        public static bool SetConfiguration(string key, bool value)
        {
            return SetConfiguration(key, value.ToString());

        }

        public static bool SetConfiguration(string key, int value)
        {
            return SetConfiguration(key, value.ToString());

        }

        public static bool SetConfiguration(string key, DateTime value)
        {
            return SetConfiguration(key, KxDateTime.ToString(value));

        }

        [Obsolete]
        public static bool UpdateConfig(KxWebResource configitem)
        {
            bool result = false;

            try
            {
                System.Configuration.Configuration config = ConfigurationManager.OpenExeConfiguration(Application.ExecutablePath);

                //config.AppSettings.Settings.Remove("test");  
                if (config.AppSettings.Settings[configitem.Identifier] != null)
                {
                    config.AppSettings.Settings.Remove(configitem.Identifier);
                    config.Save(ConfigurationSaveMode.Modified, true);
                }

                config.AppSettings.Settings.Add(configitem.Identifier, configitem.Data);
                config.Save(ConfigurationSaveMode.Modified, true);
            }
            catch (Exception)
            {
                
            }
            
            return result;
        }

        [Obsolete]
        public static string GetSettingFromConfigNoAddSetting(string key)
        {
            string result = "";

            if (System.Configuration.ConfigurationManager.AppSettings[key] != null)
            {
                result = System.Configuration.ConfigurationManager.AppSettings[key];
            }
            else 
            {
                result = "";
            }
            return result;
        }
    }
}
