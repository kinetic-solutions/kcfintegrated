﻿#region Copyright

// =======================================================
// 	Product:	NAFF.Core
// 	Project: 	Kx.Core
// 	File:		ServiceLocator.cs
// 	Created:	08/04/2013
// 	By:			Steve Ashley
// 	Modified:	08/04/2013
// 
// 	Copyright © Kinetic Solutions Ltd 2013
// 	All rights reserved.
// 
// 	No portion of this file may be re-created, copied or otherwise
// 	used without the express permission of Kinetic Solutions Ltd.
// =======================================================

#endregion

#region usings

using System;
using System.Configuration;

#endregion

namespace Kx.Core.ServiceLocator
{
    public static class ServiceLocator
    {
        public static T ResolveService<T>(params object[] args)
        {
            return ResolveServiceByTypeString<T>(ConfigurationManager.AppSettings[typeof (T).Name], args);
        }

        public static T ResolveServiceByTypeString<T>(string typeString, params object[] args)
        {
            T result = default(T);
            if (!string.IsNullOrEmpty(typeString))
            {
                Type serviceType = Type.GetType(typeString);
                if (serviceType != null)
                {
                    result = (T)Activator.CreateInstance(serviceType, args);
                }
            }

            return result;
        }
    }
}