﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using Kx.Core.Utility;

namespace Kx.Core.Database
{
    public class KxDelphiConnectionManager : ConnectionManager
    {

        public KxDelphiConnectionManager()
            : base()
        {
        }

        public KxDelphiConnectionManager(string connectionPath)
            : base (connectionPath)
        {
        }

        protected override string GetConnectionFileName()
        {
            return "Kx.Connections.xml";
        }

        protected override void InternalLoadConnections()
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(ConnectionFile);
            XmlNodeList nodes = doc.SelectNodes(@"//Connections/Connection");
            foreach (XmlNode node in nodes)
            {
                LoadConnection(node);
            }
        }

        protected override void InternalSaveConnections()
        {
            // Do nothing as we don't want to destroy the connection file.
        }

        private void LoadConnection(XmlNode node)
        {
            Connection connection = new DelphiConnection();
            connection.Name = KxXmlUtils.GetNormalCaseNodeValue(node, "Title", string.Empty);
            connection.EncryptedConnectionString = KxXmlUtils.GetNormalCaseNodeValue(node, "String", string.Empty);
            connection.IsDefault = KxXmlUtils.GetNormalCaseNodeValue(node, "Default", false);

            _connections.Add(connection);
        }
    }
}
