using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using System.Xml;
using System.IO;

namespace Kx.Core.Database
{
    public class ConnectionManager
    {
        #region Members
        protected List<Connection> _connections = new List<Connection>();
        string _connectionFilePath = string.Empty;
        protected string ConnectionFileName
        {
            get { return GetConnectionFileName(); }
        }

        #endregion

        #region Properties
        protected string ConnectionFile
        {
            get
            {
                return Path.Combine(_connectionFilePath, ConnectionFileName);
            }
        }

        public List<Connection> Connections
        {
            get
            {
                return _connections;
            }
        }

        public Connection DefaultConnection
        {
            get
            {
                foreach (Connection conn in _connections)
                {
                    if (conn.IsDefault)
                    {
                        return conn;
                    }
                }

                return null;
            }
        }

        #endregion

        #region Constructors
        public ConnectionManager()
        {
            InternalInit();
        }

        public ConnectionManager(string connectionPath)
        {
            _connectionFilePath = connectionPath;

            InternalInit();
        }
        #endregion

        #region Methods

        protected virtual string GetConnectionFileName()
        {
            return "Kx2.Connections.XML";
        }

        void InternalInit()
        {
            if (_connectionFilePath == string.Empty)
            {
                _connectionFilePath = System.Reflection.Assembly.GetExecutingAssembly().Location;
            }

            if (File.Exists(ConnectionFile))
            {
                InternalLoadConnections();
            }
        }

        protected virtual void InternalLoadConnections()
        {
            XmlSerializer loader = new XmlSerializer(typeof(List<Connection>));
            XmlReader reader = XmlReader.Create(ConnectionFile);
            _connections = (List<Connection>)loader.Deserialize(reader);
            reader.Close();
        }

        protected virtual void InternalSaveConnections()
        {
            StreamWriter streamWriter = new StreamWriter(ConnectionFile);
            XmlTextWriter writer = new XmlTextWriter(streamWriter);
            writer.Formatting = Formatting.Indented;

            XmlSerializer saver = new XmlSerializer(typeof(List<Connection>));

            saver.Serialize(writer, _connections);
            streamWriter.Close();
        }

        public void AddConnection(string connectionName, string connectionString, bool isDefault, string skinName, string webConnectionString)
        {
            Connection newConnection = new Connection();
            newConnection.Name = connectionName;
            newConnection.ConnectionString = connectionString;
            newConnection.IsDefault = isDefault;
            newConnection.SkinName = skinName;
            newConnection.WebConnectionString = webConnectionString;

            if (isDefault)
            {
                ResetIsDefault();
            }

            if (_connections.IndexOf(newConnection) < 0)
            {
                _connections.Add(newConnection);

                InternalSaveConnections();
            }
            else
            {
                throw new Exception("Connection already exists with this name");
            }
        }

        public void UpdateConnection(string connectionName, string connectionString, bool isDefault, string skinName, string webConnectionString)
        {
            Connection editConnection = GetConnection(connectionName);
            if (editConnection != null)
            {
                /* Set the other connections to not be the default */
                if (isDefault)
                {
                    ResetIsDefault();
                }

                editConnection.ConnectionString = connectionString;
                editConnection.IsDefault = isDefault;
                editConnection.SkinName = skinName;
                editConnection.WebConnectionString = webConnectionString;

                InternalSaveConnections();
            }
        }

        private void ResetIsDefault()
        {
            foreach (Connection conn in _connections)
            {
                conn.IsDefault = false;
            }
        }

        public void DeleteConnection(string connectionName)
        {
            Connection conn = GetConnection(connectionName);
            _connections.Remove(conn);

            InternalSaveConnections();
        }

        public Connection this [string name]
        {
            get
            {
                return GetConnection(name);
            }
        }

        public Connection GetConnection(string name)
        {
            Connection finder = new Connection();
            Connection result = null;
            finder.Name = name;

            if (_connections.IndexOf(finder) >= 0)
            {
                result = _connections[_connections.IndexOf(finder)];
            }

            return result;
        }
        #endregion
    }
}
