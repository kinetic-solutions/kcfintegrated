using System;
using System.Collections.Generic;
using System.Text;

namespace Kx.Core.Database
{
    public class ConnectionStringBuilder
    {
        public static string BuildConnectionString(string server, string database, bool sqlAuthentication, string userName, string password, string minPoolsize, string maxPoolSize, string timeout)
        {
            string result = string.Empty;

            if (database == string.Empty)
            {
                database = "master";
            }

            if (sqlAuthentication)
            {
                // result = string.Format("Data Source={0};UID={1};PWD={2};Database={3}", server, userName, password, database);   
                result = string.Format("Data Source={0};UID={1};PWD={2};Database={3};Min Pool Size={4};Max Pool Size={5};Connect Timeout={6}", server, userName, EncodeSpecial(password), database, minPoolsize, maxPoolSize, timeout);
            }
            else
            {
                //result = string.Format("Data Source={0};Integrated Security=SSPI;Database={1}", server, database);
                result = string.Format("Data Source={0};Integrated Security=SSPI;Database={1};Min Pool Size={2};Max Pool Size={3};Connect Timeout={4}", server, database, minPoolsize, maxPoolSize, timeout);
            }
             //Min Pool Size=5;Max Pool Size=60;Connect Timeout=45;"
            return result;
        }

        /// <summary>
        /// Calls BuildConnectionString using some default values for minPoolsize, maxPoolSize, timeout
        /// </summary>
        public static string BuildConnectionString(string server, string database, bool sqlAuthentication, string userName, string password)
        {
            return BuildConnectionString(server, database, sqlAuthentication, userName, password, "5", "60", "45");
        }


        public static void DecodeConnectionString(string connectionString, out string server, out string database)
        {
            bool sqlAuth;
            string userName, password, minPoolsize, maxPoolSize, timeout;

            DecodeConnectionString(connectionString, out server, out database, out sqlAuth, out userName, out password, out minPoolsize, out maxPoolSize, out timeout);
        }

        /// <summary>
        /// Calls DecodeConnectionString without minPoolsize, maxPoolSize, timeout
        /// </summary>
        public static void DecodeConnectionString(string connectionString, out string server, out string database, out bool sqlAuthentication, out string userName, out string password)
        {
            string minPoolsize, maxPoolSize, timeout;

            DecodeConnectionString(connectionString, out server, out database, out sqlAuthentication, out userName, out password, out minPoolsize, out maxPoolSize, out timeout);
        }

        public static void DecodeConnectionString(string connectionString, out string server, out string database, out bool sqlAuthentication, out string userName, out string password, out string minPoolsize, out string maxPoolSize, out string timeout)
        {
            string[] sections = connectionString.Split(';');

            sqlAuthentication = true;
            userName = string.Empty;
            password = string.Empty;
            server = string.Empty;
            database = string.Empty;
            minPoolsize= string.Empty;
            maxPoolSize= string.Empty;
            timeout = string.Empty;

            foreach (string str in sections)
            {
                string[] elements = str.Split(new char[] {'='}, 2);
                switch (elements[0])
                {
                    case "Data Source":
                        server = elements[1];
                        break;
                    case "Integrated Security":
                        sqlAuthentication = false;
                        break;
                    case "Database":
                        database = elements[1];
                        break;
                    case "UID":
                        userName = elements[1];
                        break;
                    case "PWD":
                        password = DecodeSpecial(elements[1]);
                        break;
                    case "Min Pool Size":
                        minPoolsize = elements[1];
                        break;
                    case "Max Pool Size":
                        maxPoolSize = elements[1];
                        break;
                    case "Connect Timeout":
                        timeout = elements[1];
                        break;
                }
            }
        }

        private static string EncodeSpecial(string value)
        {
            return value.Replace("=", "==");
        }

        private static string DecodeSpecial(string value)
        {
            return value.Replace("==", "=");
        }
    }
}
