using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Kx.Core.Database
{
    public class MasterDatabases
    {
        private string _connectionString = string.Empty;
        public List<string> ListAllDatabases(string connectionString)
        {
            _connectionString = connectionString;

            return InternalListAllDatabases();
        }

        protected List<string> InternalListAllDatabases()
        {
            List<string> result = new List<string>();

            SqlConnection conn = new SqlConnection(_connectionString);
            SqlCommand comm = new SqlCommand();
            comm.Connection = conn;

            comm.CommandText = "SELECT name FROM master..sysdatabases ORDER BY name";
            comm.CommandType = CommandType.Text;

            SqlDataAdapter adapt = new SqlDataAdapter(comm);
            DataTable table = new DataTable();
            conn.Open();
            adapt.Fill(table);
            conn.Close();

            foreach (DataRow row in table.Rows)
            {
                result.Add(row["name"].ToString());
            }

            return result;
        }
    }
}
