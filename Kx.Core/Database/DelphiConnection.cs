﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Kx.Core.Database
{
    public class DelphiConnection : Connection
    {
        private IDelphiDecrypter _crypto = null;
        private string _decryptedString = null;
        
        protected override string DecryptConnectionString()
        {
            if (string.IsNullOrWhiteSpace(_decryptedString))
            {
                _crypto = _crypto ?? ServiceLocator.ServiceLocator.ResolveService<IDelphiDecrypter>();
                if (_crypto != null)
                {
                    _decryptedString = _crypto.DecryptString(this._encryptedConnectionString);
                }
            }

            return _decryptedString;
        }

        protected override void EncryptConnectionString(string value)
        {
            // do nothing
        }
    }
}
