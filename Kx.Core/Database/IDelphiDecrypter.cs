﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Kx.Core.Database
{
    public interface IDelphiDecrypter
    {
        string DecryptString(string input);
    }
}
