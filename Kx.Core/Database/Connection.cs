using System;
using System.Xml.Serialization;
using Kx.Core.Encryption;

namespace Kx.Core.Database
{
    [Serializable]
    public class Connection : IComparable
    {
        #region Properties
        private string _name;
        protected string _encryptedConnectionString;
        private bool _isDefault;
        private string _skinName;

        private string _server;
        private string _database;

        private string _sessionId;

        private string _minPoolsize;
        private string _maxPoolSize;
        private string _timeout;
        private int _commandTimeout;

        private string _webEncryptedConnectionString;
        private string _webServer;
        private string _webDatabase;

        private bool unencrypted = false;

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public string EncryptedConnectionString
        {
            get { return _encryptedConnectionString; }
            set { _encryptedConnectionString = value; }
        }

        [XmlIgnore()]
        public string SessionID
        {
            get
            {
            	return _sessionId;
            }
            set
            {
            	_sessionId = value;
            }
        }

        [XmlIgnore()]
        public string Server
        {
            get
            {
                if (_server == null)
                {
                    ConnectionStringBuilder.DecodeConnectionString(ConnectionString, out _server, out _database);
                }
                return _server;
            }
        }

        [XmlIgnore()]
        public string Database
        {
            get
            {
                if (_database == null)
                {
                    ConnectionStringBuilder.DecodeConnectionString(ConnectionString, out _server, out _database);
                }
                return _database;
            }
        }


        [XmlIgnore()]
        public string MinPoolsize
        {
            get
            {
                return _minPoolsize;
            }
            set
            {
                _minPoolsize = value;
            }
        }

        [XmlIgnore()]
        public string MaxPoolSize
        {
            get
            {
                return _maxPoolSize;
            }
            set
            {
                _maxPoolSize = value;
            }
        }

        [XmlIgnore()]
        public string Timeout
        {
            get
            {
                return _timeout;
            }
            set
            {
                _timeout = value;
            }
        }

        [XmlIgnore()]
        public int CommandTimeout
        {
            get
            {
                return _commandTimeout;
            }
            set
            {
                _commandTimeout = value;
            }
        }

        [XmlIgnore()]
        public string ConnectionString
        {
            get { return DecryptConnectionString(); }
            set 
            { 
                EncryptConnectionString(value);
                _server = null;
                _database = null;
            }
        }

        public bool IsDefault
        {
            get { return _isDefault; }
            set { _isDefault = value; }
        }

        public string SkinName
        {
            get { return _skinName; }
            set { _skinName = value; }
        }

        public string WebEncryptedConnectionString
        {
            get
            {
                return _webEncryptedConnectionString;
            }
            set
            {
                _webEncryptedConnectionString = value;
            }
        }
        #endregion
        
        /// <summary>
        /// Constructor with Disable encrypt/decrypt routine on Connection strings
        /// </summary>
        /// <param name="noEncrypt">True if encryption not needed - recommended</param>
        public Connection(bool noEncrypt)
        {
            if(noEncrypt == true) { unencrypted = true; }
        }

        /// <summary>
        /// Base Connection constructor
        /// </summary>
        public Connection() {
        }

        [XmlIgnore()]
        public string WebServer
        {
            get
            {
                if (_webServer == null)
                {
                    ConnectionStringBuilder.DecodeConnectionString(WebConnectionString, out _webServer, out _webDatabase);
                }
                return _webServer;
            }
        }

        [XmlIgnore()]
        public string WebDatabase
        {
            get
            {
                if (_webDatabase == null)
                {
                    ConnectionStringBuilder.DecodeConnectionString(WebConnectionString, out _webServer, out _webDatabase);
                }
                return _webDatabase;
            }
        }

        [XmlIgnore()]
        public string WebConnectionString
        {
            get
            {
                if (_webEncryptedConnectionString != null)
                {
                    if(unencrypted == true)
                    {
                        return _webEncryptedConnectionString;
                    }
                    return Rijndael.DecryptString(_webEncryptedConnectionString, "WebConnectionString");
                }
                else
                {
                    return String.Empty;
                }
            }
            set
            {
                if (value != null)
                {
                    if (unencrypted == true)
                    {
                        _webEncryptedConnectionString = value;
                    }
                    else { _webEncryptedConnectionString = Rijndael.EncryptString(value, "WebConnectionString"); }
                    _webServer = null;
                    _webDatabase = null;
                }
            }
        }

        [XmlIgnore()]
        public bool HasConnectionString
        {
            get
            {
                return !string.IsNullOrEmpty(_encryptedConnectionString);
            }
        }

        [XmlIgnore()]
        public bool HasWebConnectionString
        {
            get
            {
                return !string.IsNullOrEmpty(_webEncryptedConnectionString);
            }
        }

        #region IComparable

        public override bool Equals(object obj)
        {
            if (obj is Connection)
            {
                Connection objConnection = (Connection)obj;
                return this.Name.Equals(objConnection.Name);
            }

            return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public int CompareTo(object obj)
        {
            if (obj is Connection)
            {
                Connection objConnection = (Connection)obj;
                return this.Name.CompareTo(objConnection.Name);
            }

            return -1;
        }
        #endregion

        public override string ToString()
        {
            return _name;
        }
        protected virtual string DecryptConnectionString()
        {
            if(unencrypted == true)
            {
                return _encryptedConnectionString;
            }
            return Rijndael.DecryptString(_encryptedConnectionString, "ConnectionString");
        }

        protected virtual void EncryptConnectionString(string value)
        {
            if (unencrypted == true)
            {
                _encryptedConnectionString = value;
            }
            else { 
            _encryptedConnectionString = Rijndael.EncryptString(value, "ConnectionString");
            }
        }

    }
}
