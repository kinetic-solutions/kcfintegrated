using System;
using System.Collections.Generic;
using System.Text;

namespace Kx.Core.Exceptions
{
    public class KxDataException : KxException
    {
        public KxDataException(string message)
            : base(message)
        {

        }

        public KxDataException(string message, Exception innerException)
            : base(message, innerException)
        {
            
        }
         
    }
}
