using System;
using System.Collections.Generic;
using System.Text;

namespace Kx.Core.Exceptions
{
    public class KxAuthenticationException : KxException
    {
        public KxAuthenticationException(string message)
            : base(message)
        {

        }
    }
}
