using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Text;
using System.Xml;
using Kx.Core.Data;
using System.Diagnostics;
using System.Runtime.Serialization;
using Kx.Core.Configuration;
using Microsoft.ApplicationInsights;
using Microsoft.ApplicationInsights.Extensibility;
using Microsoft.ApplicationInsights.DataContracts;

namespace Kx.Core.Exceptions
{
    public class KxLogger
    {
        private static string latestErrors = "";
        private static bool currentlyLogging = false; // prevent the re-occuring loop from occuring
        private static string environment;
        private static TelemetryClient _telemetry;
        /// <summary>
        /// Single App insights telemtry client with connection string
        /// </summary>
        private static TelemetryClient telemetry
        {
            get
            {
                if (_telemetry == null) {
                    TelemetryConfiguration telemetryConfig = TelemetryConfiguration.CreateDefault();
                    telemetryConfig.ConnectionString = KxDbConfiguration.GetConfiguration("AppInsightConnectionString");
                    _telemetry = new TelemetryClient(telemetryConfig); }
                return _telemetry;
            }
        }


        public static string LatestErrors
        {
            get
            { return latestErrors; }
            set
            { latestErrors = value; }
        }




        public static void LogActivity(object owner, string method, string message, string extrainfo, bool severe = false)
        {
            try
            {
                string ClassName = owner.GetType().Name;
                LogActivity(ClassName, method, message, extrainfo, severe);
            }
            catch { }
        }

        /// <summary>
        /// Logs 'Activity' (often exceptions) from various sources throughout KCF.
        /// </summary>
        /// <param name="classname">Calling class</param>
        /// <param name="method">Calling method</param>
        /// <param name="message">Associated 'message'</param>
        /// <param name="extrainfo">Any extra info</param>
        public static void LogActivity(string classname, string method, string message, string extrainfo, bool severe = false)
        {
            bool errorLogged = false; //if exceptions are thrown and nothing is logged finally log to the EventLogger

            /*FIX*/
            if (!KxLogger.currentlyLogging) //use the static field to ensure we don't   enter the 'Infinate loop' bug.
            //only log if we have not already started.
            {
                KxLogger.currentlyLogging = true;
                try
                {
                    if (!IsExcludedError(message))
                    {
                        if (!string.IsNullOrEmpty(message) && method != "GetServiceUri" && method != "PostPayment")
                        {
                            //if (severe)
                                Debugger.Break();   // pha added
                        }


                        // Only used by 'show debug' option, otherwise a useless string in memory!
                        //if (KxDbConfiguration.GetConfigurationBool("showDebug", false)) { 
                        //latestErrors += "<br/>Class: " + classname + " Method: " + method + " Message: " + message + " Extra: " + extrainfo;
                        //}
                        // TODO: Remove references to WebLogging URL
                        //string WebServicePath = KxDbConfiguration.GetConfiguration("WebLoggingUrl");

                        //if (WebServicePath != "")
                        //{
                        //    Weblogging(WebServicePath, classname, method, message, extrainfo);
                        //    errorLogged = true;
                        //}

                        //if(!String.IsNullOrEmpty(KxDbConfiguration.GetConfiguration("AppInsightConnectionString")))
                        //{
                        //    if (String.IsNullOrEmpty(environment))
                        //    {
                        //        environment = KxDbConfiguration.GetConfiguration("WorkflowScope", "UNKNOWN");
                        //    }
                        //    LogToAI(classname, method, message, extrainfo);
                        //    errorLogged = true;
                        //}

                        // pha commented out

                        //if (!String.IsNullOrEmpty(KxDbConfiguration.GetConfiguration("DBErrorLogging")))
                        //{
                        //    DBlogging(classname, method, message, extrainfo);
                        //    errorLogged = true;
                        //}


                        //string XmlPath = KxDbConfiguration.GetConfiguration("XmlLogDir");

                        //if (!String.IsNullOrEmpty(XmlPath))
                        //{
                        //    XmlLogging(XmlPath, classname, method, message, extrainfo);
                        //    errorLogged = true;
                        //}

                        //if (KxDbConfiguration.GetConfiguration("EventLogLogging") != "")
                        //{
                        //    LogToEventLog(classname, method, message, extrainfo);
                        //    errorLogged = true;
                        //}
                    }
                }
                catch
                {

                    try
                    {
                        if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["FileLoggerDir"]))
                        {
                            //use only .Net classes here.. everything else has broken..
                            string file = String.Format(@"{0}\Log.txt", ConfigurationManager.AppSettings["FileLoggerDir"]);


                            File.AppendAllText(file, string.Format("Class:{0}; Method:{1}; Message:{2}; ExtraInfo:{3}", classname, method, message, extrainfo));
                        }
                    }
                    catch
                    {
                        //We can't really do much else..
                    }
                }

                KxLogger.currentlyLogging = false;
            }
        }

        // TODO: Remove references to WebLogging
        //protected static void Weblogging(string url, string owner, string method, string message, string extrainfo)
        //{
        //    try
        //    {
        //        ErrorLog.ErrorLog errorService = new ErrorLog.ErrorLog();
        //        errorService.Url = url;
        //        errorService.LogError(owner, method, "", message, message, DateTime.Now, extrainfo);
        //    }
        //    catch (Exception ex)
        //    {
        //        DBlogging("KxLogger", "Weblogging", "Webservice is not not configured for error logging.", ex.Message);
        //    }
        //}

        // TODO : Needs to be smarter with severity levels and different logging. 
        /// <summary>
        /// Logs a trace entry to Application Insights
        /// </summary>
        /// <param name="owner">Class</param>
        /// <param name="method">Method</param>
        /// <param name="message">What has happened</param>
        /// <param name="extrainfo">More on what has happened..</param>
        protected static void LogToAI(string owner, string method, string message, string extrainfo)
        {
            try
            {
                telemetry.TrackTrace(extrainfo, SeverityLevel.Warning, new Dictionary<string, string> { { "Class", owner }, { "Method", method }, { "Error", message }, { "Environment", environment } });
            }
            catch (Exception e)
            {
                DBlogging("KxLogger", "ApplicationInsights", "Something went horribly wrong trying to log to Azure App Insights.", e.Message);
            }
        }

        /// <summary>
        /// Public method which logs Exceptions (from KxData typically) to AppAI
        /// </summary>
        /// <param name="owner">Class</param>
        /// <param name="method">Method</param>
        /// <param name="message">What has happened</param>
        /// <param name="extrainfo">More on what has happened..</param>
        public static void LogExceptionToAI(string owner, string method, string message, string extrainfo)
        {
            try
            {
                telemetry.TrackTrace(extrainfo, SeverityLevel.Error, new Dictionary<string, string> { { "Class", owner }, { "Method", method }, { "Error", message }, { "Environment", environment } });
            }
            catch (Exception e)
            {
                DBlogging("KxLogger", "ApplicationInsights", "Something went horribly wrong trying to log an exception to Azure App Insights.", e.Message);
            }
        }

        /// <summary>
        /// Public method which logs Audits (from KxData typically) to AppAI
        /// </summary>
        /// <param name="className">Class</param>
        /// <param name="audit">Audit entry regarding</param>
        /// <param name="auditText">details of audit..</param>
        public static void LogAuditToAI(string className, string audit, string auditText)
        {
            try
            {
                telemetry.TrackTrace(auditText, SeverityLevel.Information, new Dictionary<string, string> { { "Class", className }, { "Identifier", audit }, { "Environment", environment } });
            }
            catch (Exception e)
            {
                DBlogging("KxLogger", "ApplicationInsights", "Something went horribly wrong trying to log an audit entry to Azure App Insights.", e.Message);
            }
        }



        protected static void DBlogging(string owner, string method, string message, string extrainfo)
        {
            KxData data = new KxData();
            data.LogException(owner, method, message, extrainfo);
        }

        protected static void LogToEventLog(string owner, string method, string message, string extrainfo)
        {
            try
            {

                if (!System.Diagnostics.EventLog.SourceExists("KX Framework"))
                {
                    System.Diagnostics.EventLog.CreateEventSource("KX Framework", "Application");
                }

                System.Diagnostics.EventLog log = new System.Diagnostics.EventLog("KX Framework");

                log.Source = "KX Framework";
                log.Log = "Application";

                string msg = "Error Details: \r\n\r\nObject Name: " + owner + "\r\nMethod: " + method + "\r\nError: " + message;

                if (extrainfo != "")
                {
                    msg += "\r\nSupporting Information: " + extrainfo;
                }
                else
                {
                    msg += "\r\nNo Supporting Information was supplied.";
                }

                log.WriteEntry(msg, EventLogEntryType.Error);
            }
            catch (Exception ex)
            {
                throw ex; //catch and throw to be caught higher up.
            }
        }

        protected static void XmlLogging(string path, string owner, string method, string message, string extrainfo)
        {
            XmlDocument doc = new XmlDocument();
            XmlNode root = null;

            string filename = path + "KxError" + DateTime.Today.ToString("yyyyMMdd") + ".xml";

            FileInfo fi = new FileInfo(filename);

            if (fi.Exists)
            {
                doc.Load(filename);
                root = doc.DocumentElement;
            }
            else
            {
                XmlDeclaration dec = doc.CreateXmlDeclaration("1.0", "utf-8", "yes");
                doc.AppendChild(dec);

                root = doc.CreateElement("errors");
                doc.AppendChild(root);
            }

            XmlElement errornode = doc.CreateElement("error");
            root.AppendChild(errornode);

            AddElement(doc, errornode, "classname", owner);
            AddElement(doc, errornode, "method", method);
            AddElement(doc, errornode, "message", message);
            AddElement(doc, errornode, "extrainfo", extrainfo);
            AddElement(doc, errornode, "timestamp", DateTime.Now.ToString("f"));

            try
            {
                doc.Save(filename);
            }
            catch { }
        }

        protected static void AddElement(XmlDocument doc, XmlNode parent, string nodename, string value)
        {
            XmlElement elem = doc.CreateElement(nodename);
            XmlCDataSection sect = doc.CreateCDataSection(nodename);
            sect.Value = value;

            elem.AppendChild(sect);

            parent.AppendChild(elem);
        }

        public static bool IsExcludedError(string message)
        {
            bool result = false;

            result = message.ToUpper().IndexOf("THREAD WAS BEING ABORTED") != -1;

            return result;
        }
    }
}
