using System;
using System.Collections.Generic;
using System.Text;

namespace Kx.Core.Exceptions
{
    [Serializable]
    public class KxException : Exception 
    {
        public KxException(string message)
            : base(message) 
        {
        }

        public KxException(string message, Exception innerException)
            : base (message, innerException) 
        {
            
        }
    }
}
