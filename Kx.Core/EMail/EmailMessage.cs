using System;
using System.Net;
using System.Net.Mail;
using System.Collections;
using System.IO;
using System.Security;
using System.Security.Permissions;
using Kx.Core.Data;
using Kx.Core.Exceptions;
using Kx.Core.Configuration;

namespace Kx.Core.Email
{

	public delegate void EmailTransmitEvent(MailMessage msg, bool sendStatus);

	/// <summary>
	/// Summary description for Email.
	/// </summary>

	[Serializable]
	public class EmailMessage 
	{		
        private string toAddress;
		private string ccAddress = "";
		private string bccAddress = "";
		private string fromEmailAddress;
		private string smtpServer;
		private int port;
		private string smtpUsername;
		private string smtpPassword;
		private string textBody;
		private string htmlBody;
		private string subject;
		private MailMessage message;
        protected bool _enableSSL = false;
	    public bool MessageIsUnicode { get; set; }

		private ArrayList attachmentFiles;

		private EmailTransmitEvent onEmailTransmit = null;
		
		public ArrayList AttachmentFiles
		{
			get
			{
				return attachmentFiles;
			}
			set
			{
				attachmentFiles = value;
			}
		}

		public EmailTransmitEvent OnEmailTransmit
		{
			get
			{
				return onEmailTransmit;
			}
			set
			{
				onEmailTransmit = value;
			}
		}

		/// <summary>
		/// Default Constructor
		/// </summary>
		public EmailMessage() : base()
		{
			// Initialise Message
			message = new MailMessage();
			AttachmentFiles = new ArrayList(5);

			smtpServer = KxDbConfiguration.GetConfiguration("SmtpServer");
			smtpUsername = KxDbConfiguration.GetConfiguration("SMTPUsername");
			smtpPassword = KxDbConfiguration.GetConfiguration("SMTPPassword");
			port = KxDbConfiguration.GetConfigurationInt("SmtpServerPort",25);
		    _enableSSL = KxDbConfiguration.GetConfigurationBool("SMTPSSL", false);
		}

		/// <summary>
		/// Constructor that does not use webresources to get email server settings
		/// </summary>
		public EmailMessage(string smtp_server, string smtp_username, string smtp_password, int smtp_port)
			: base()
		{
			// Initialise Message
			message = new MailMessage();
			AttachmentFiles = new ArrayList(5);

			smtpServer = smtp_server;
			smtpUsername = smtp_username;
			smtpPassword = smtp_password;
			port = smtp_port;
            _enableSSL = KxDbConfiguration.GetConfigurationBool("SMTPSSL", false);
        }

		public string CcAddress
		{
			get
			{
				return ccAddress;
			}
			set
			{
				ccAddress = value;
			}
		}

		public string BccAddress
		{
			get
			{
				return bccAddress;
			}
			set
			{
				bccAddress = value;
			}
		}

		public string Subject
		{
			get
			{
				return subject;
			}
			set
			{
				subject = value;
			}
		}

		public string ToAddress
		{
			get
			{
				return toAddress;
			}
			set
			{
				toAddress = value;
			}
		}
		
		public string FromAddress
		{
			get
			{
				return fromEmailAddress;
			}
			set
			{
				fromEmailAddress = value;
			}
		}
		
		public string SmtpServer
		{
			get
			{
				return smtpServer;
			}
			set
			{
				smtpServer = value;
			}
		}

		public int Port
		{
			get
			{
				return port;
			}
			set
			{
				port = value;
			}
		}

		public string SmtpUsername
		{
			get
			{
				return smtpUsername;
			}
			set
			{
				smtpUsername = value;
			}
		}

		public string SmtpPassword
		{
			get
			{
				return smtpPassword;
			}
			set
			{
				smtpPassword = value;
			}
		}

		public string TextBody
		{
			get
			{
				return textBody;
			}
			set
			{
				textBody = value;
			}
		}

		public string HtmlBody
		{
			get
			{
				return htmlBody;
			}
			set
			{
				htmlBody = value;
			}
		}

		public void AddAttachment(string filePath)
		{

			AttachmentFiles.Add(filePath);
        }


        public bool EnableSSL
        {
            get { return _enableSSL; }
            set { _enableSSL = value; }
        }
        

		public override string ToString()
		{
			return string.Format("To:{0} Subject:{1} From:{2}",ToAddress,subject,fromEmailAddress);
		}

		private int GetTimeout()
		{
			String sTimeout;
			int iTimeout = 30000;

			try
			{
				sTimeout = System.Configuration.ConfigurationManager.AppSettings["email-timeout"];
				if (!Int32.TryParse(sTimeout, out iTimeout))
				{
					iTimeout = 30000;
				}
			}
			catch (Exception ex)
			{
				KxLogger.LogActivity(this, "GetTimeout", ex.Message, "");
			}
			return iTimeout;
		}

		public void Send()
		{
			// pha

			if (System.Configuration.ConfigurationManager.AppSettings["DebugEmail"] == "True")
			{
				using (StreamWriter sw = new StreamWriter(@"c:\temp\workflow.xml", true))
				{
					sw.WriteLine("<SendEmail>");
					sw.WriteLine("\t<ToAddress>" + toAddress + "</ToAddress>");
					sw.WriteLine("\t<FromAddress>" + FromAddress + "</FromAddress>");
					sw.WriteLine("\t<Subject>" + subject + "</Subject>");
					sw.WriteLine("\t<Body>");
					sw.WriteLine("\t\t<!--");
					sw.WriteLine("\t\t" + textBody);
					sw.WriteLine("\t\t-->");
					sw.WriteLine("\t</Body>");
					sw.WriteLine("</SendEmail>");
				}
			}
			return;

			//KxLogger.LogActivity(this, "Send", toAddress, FromAddress);
			try 
			{
				if (smtpServer == null || (smtpServer != null && smtpServer.Length == 0)) 
				{
					throw new SmtpException("smtpserver is not set");
				}

				if(port==0) 
				{
					throw new SmtpException("smtp server port is not set");
				}

			    if (MessageIsUnicode && KxDbConfiguration.GetConfigurationBool("AllowUnicodeEmail", true))
			    {
			        message.BodyEncoding = System.Text.Encoding.UTF8;
			        message.SubjectEncoding = System.Text.Encoding.UTF8;
			    }

				//KxLogger.LogActivity(this, "Send", "001", toAddress);
				message.To.Add(new MailAddress(toAddress));
				//KxLogger.LogActivity(this, "Send", "002", FromAddress);
				message.From = new MailAddress(FromAddress);
				//KxLogger.LogActivity(this, "Send", "003", "");

				if (!string.IsNullOrEmpty((BccAddress ?? "").Trim()))
				{
					string[] addressesBC = BccAddress.Split(';');
					foreach (string addressBC in addressesBC)
					{
						if (addressBC != string.Empty)
						{
							message.Bcc.Add(new MailAddress(addressBC));
						}
					}
				}
				//KxLogger.LogActivity(this, "Send", "004", "");

                if (!string.IsNullOrEmpty((CcAddress ?? "").Trim()))
                {
					string[] addressesCC = CcAddress.Split(';');
					foreach (string addressCC in addressesCC)
					{
						if (addressCC != string.Empty)
						{
							message.CC.Add(new MailAddress(addressCC));
						}

					}
				}
				//KxLogger.LogActivity(this, "Send", "005", subject);

				message.Subject = subject;
				//KxLogger.LogActivity(this, "Send", "006", textBody);
				message.Body = textBody;
				//KxLogger.LogActivity(this, "Send", "007", "");
				if (textBody.ToLower().Contains("<html"))
				{
					KxLogger.LogActivity(this, "htmlBody", htmlBody, "");
					htmlBody = textBody;
				}
				//KxLogger.LogActivity(this, "Send", "008", "");
				if (htmlBody != null) 
				{
					KxLogger.LogActivity(this, "htmlBody1", htmlBody, "");
					message.IsBodyHtml = true;
					message.Body = htmlBody;
				}
				//KxLogger.LogActivity(this, "Send", "009", "");

				foreach (string filePath in AttachmentFiles) 
				{
					if (File.Exists(filePath)) 
					{						
						message.Attachments.Add(new Attachment(filePath));
					}					
				}
				//KxLogger.LogActivity(this, "Send", "010", "");

				SmtpClient s = new SmtpClient();
				s.Host = smtpServer;
				s.Port = port;
				s.Timeout = GetTimeout();
			    s.EnableSsl = _enableSSL;
				s.Credentials = new NetworkCredential(smtpUsername, smtpPassword);
				//KxLogger.LogActivity(this, "Send", "011", "");
				s.Send(message);
				//KxLogger.LogActivity(this, "Send", "012", "");
				
				KxData data = new KxData();
				data.ExecuteAuditLog("EmailMessage", ToAddress, TextBody);
				EmailTransmit(message,true);
				message.Dispose();
				//KxLogger.LogActivity(this, "Send", "013", "");
			}
			catch(Exception ex) 
			{							
				string msg = string.Format("Email To:{0} Failed\\r\\n{1}\\r\\n{2}", toAddress, ex.Message, ex.StackTrace);
				KxLogger.LogActivity(this, "SendMail", msg, "");
				EmailTransmit(message,false);
				message.Dispose();
				throw new SmtpException(msg, ex);
			}
		}

		private void EmailTransmit(MailMessage msg,bool status)
		{
			try
			{
				if (OnEmailTransmit != null)
				{
					OnEmailTransmit(msg, status);
				}
			}
			catch { }
		}
	}
}
