using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.IO;

namespace Kx.Core.Serialization
{
    public class KxSerialization<T>
    {
        public T GetObjectFromXml(XmlDocument xml)
        {
            T result = default(T);

            XmlSerializer serializer = new XmlSerializer(typeof(T));
            XmlNodeReader reader = new XmlNodeReader(xml.DocumentElement);
            result = (T)serializer.Deserialize(reader);

            return result;
        }

        public XmlDocument GetXmlFromObject(T obj)
        {
            XmlDocument result = null;

            if (obj != null)
            {
                result = new XmlDocument();
                XmlSerializer serializer = new XmlSerializer(typeof(T));
                StringBuilder builder = new StringBuilder();
                StringWriter writer = new StringWriter(builder);
                serializer.Serialize(writer, obj);
                result.LoadXml(builder.ToString());
            }

            return result;
        }
    }
}
