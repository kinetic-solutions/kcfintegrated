using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using System.Xml;
using Kx.Core.Exceptions;
using Kx.Core.Utility;
using System.Web.UI.WebControls;
using System.Web.UI;

namespace Kx.Core.Reflection
{
    public class KxReflection
    {
        public static PropertyInfo GetPropertyInfo(object originalobject, string propertyName)
        {
            PropertyInfo result = null;

            if (originalobject != null)
            {
                object obj = GetFinalObject(originalobject, ref propertyName);

                if (obj != null)
                {
                    Type _type = obj.GetType();

                    try
                    {
                        result = obj.GetType().GetProperty(propertyName);
                    }
                    catch (Exception)
                    {
                        //KxLogger.LogActivity("KxReflection", "GetPropertyObject", ex.Message, propertyName);
                        return null;
                    }
                }
                else
                {
                    //KxLogger.LogActivity("KxReflection", "GetPropertyObject", string.Format("{0} cannot be found in object {1}", propertyName, originalobject.ToString()), string.Empty);
                    return null;
                }
            }
            return result;
        }
        
        public static object GetPropertyObject(object originalobject, string propertyName)
        {
            return GetPropertyObject(originalobject, propertyName, false);
        }

        public static object GetPropertyObject(object originalobject,string propertyName,bool IgnoreError)
        {
            if (originalobject != null && propertyName != "")
            {
                object obj = GetFinalObject(originalobject, ref propertyName);

                if (obj != null)
                {
                    Type _type = obj.GetType();

                    try
                    {
                        PropertyInfo pi = obj.GetType().GetProperty(propertyName);
                        if (pi == null)
                        {
                            pi = obj.GetType().GetProperty("?" + propertyName);
                        }

                        if (pi != null)
                        {
                            if (pi.CanRead)
                            {
                                Object o = pi.GetValue(obj, null);

                                if (System.Configuration.ConfigurationManager.AppSettings["DebugReflection"] == "True")
                                {
                                    using (System.IO.StreamWriter sw = new System.IO.StreamWriter(@"c:\temp\workflow.xml", true))
                                    {
                                        sw.WriteLine("<Reflection>");
                                        sw.WriteLine("\t<DataType>" + pi.PropertyType.Name + "</DataType>");
                                        sw.WriteLine("\t<ObjectType>" + _type.FullName + "</ObjectType>");
                                        sw.WriteLine("\t<Property>" + propertyName + "</Property>");
                                        if (pi.PropertyType.Name == "String[]")
                                        {
                                            sw.WriteLine("\t<Value><!--");
                                            foreach (String s in (string[])o)
                                            {
                                                sw.WriteLine(s);
                                            }
                                            sw.WriteLine("--></Value>");
                                        }
                                        else
                                            sw.WriteLine("\t<Value><!--" + (o == null ? "" : o.ToString()) + "--></Value>");
                                        sw.WriteLine("</Reflection>");
                                    }
                                }
                                return o;
                            }
                            else
                            {
                                return null;
                            }
                        }
                        else
                        {
                            if (!IgnoreError)
                            {
                                throw new Exception(_type.FullName + " does not contain property: " + propertyName);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        if (!IgnoreError)
                        {
                            //pha
                            KxLogger.LogActivity("KxReflection", "GetPropertyObject", ex.Message, propertyName);
                            throw new Exception(ex.Message);
                        }
                    }
                    
                }
                else
                {
                //KxLogger.LogActivity("KxReflection","GetPropertyObject",string.Format("{0} cannot be found in object {1}",propertyName,originalobject.ToString()),string.Empty);
                return null;
                }
            }
            return null;
        }

        public static string GetPropertyString(object originalobject, string propertyName)
        {
            string result = "";

            if (originalobject != null)
            {
                object obj = GetFinalObject(originalobject, ref propertyName);

                object obj1 = GetPropertyObject(obj, propertyName);
                result = string.Empty;
                if (obj1 != null)
                {
                    result = obj1.ToString();
                }
            }
            return result;
        }

        public static bool SetProperty(object originalobject, string propertyName, object value)
        {
            bool updated = false;

            if (originalobject != null && !String.IsNullOrEmpty(propertyName))
            {
                object obj = GetFinalObject(originalobject, ref propertyName);

                PropertyInfo pi = null;

                try
                {
                    pi = obj.GetType().GetProperty(propertyName);
                }
                catch (Exception ex)
                {
                    KxLogger.LogActivity("KxReflection", "SetProperty:1", ex.Message, originalobject.ToString());
                }

                if (pi != null)
                {
                    if (pi.CanWrite)
                    {
                        // Have created datetimes as nullable, so they arent datetimes anymore...
                        if (pi.PropertyType.Name == "Nullable`1")
                        {
                            try
                            {
                                pi.SetValue(obj, KxDateTime.Parse(value.ToString()), null);
                                updated = true;
                            }
                            catch
                            {

                            }
                        }
                        else
                        {
                            try
                            {
                                if (value != null && (value.GetType() != pi.GetType()))
                                {
                                    try
                                    {
                                        Type _type = pi.PropertyType;
                                        // If it can't be converted, don't bother trying.
                                        if(value is IConvertible) { 
                                            value = Convert.ChangeType(value, _type);
                                        }
                                    }
                                    catch
                                    {
                                    }
                                }

                                pi.SetValue(obj, value, null);
                                updated = true;
                            }
                            catch 
                            {
                               
                            }
                        }
                      
                    }
                }
                else
                {
                    KxLogger.LogActivity("KxReflection", "SetProperty:3", originalobject.GetType().Name + " does not contain property: " + propertyName, "");
                }
            }
            return updated;
        }

        public static object GetFinalObject(object originalobject, ref string propertyName)
        {
            if (propertyName.IndexOf('.') < 0)
            {
                return originalobject;
            }
            else
            {
                object result = null;

                int idx = propertyName.IndexOf('.');
                string propname = propertyName.Substring(0, idx);
                propertyName = propertyName.Remove(0, idx+1);

                Type type = originalobject.GetType();

                PropertyInfo pi = type.GetProperty(propname);

                if (pi != null)
                {
                    result = pi.GetValue(originalobject, null);
                    result = GetFinalObject(result, ref propertyName);
                }
                return result;
            }
        }

        public static MethodInfo GetMethodInfo(object originalobject, string methodName)
        {
            MethodInfo result = null;

            if (originalobject != null)
            {
                object obj = GetFinalObject(originalobject, ref methodName);

                Type _type = obj.GetType();

                try
                {
                    result =  _type.GetMethod(methodName);
                }
                catch (Exception ex)
                {
                    KxLogger.LogActivity("KxReflection", "InvokeMethod", ex.Message, methodName);
                    return null;
                }
            }
            return result;
        }

        public static object InvokeMethod(object originalobject, string methodName, object[] parameters)
        {
            object result = null;

            if (originalobject != null)
            {
                object obj = GetFinalObject(originalobject, ref methodName);
                             
                Type _type = obj.GetType();

                try
                {
                    MethodInfo mi = _type.GetMethod(methodName);
                    result = mi.Invoke(obj, parameters);
                }
                catch (Exception ex)
                {
                    KxLogger.LogActivity("KxReflection", "InvokeMethod", ex.Message, methodName);
                    return null;
                }
            }
            return result;
        }

        public static object CloneObject(object source)
        {
            Type _typ = source.GetType();
            object dest = Activator.CreateInstance(_typ);
 
            Type _type = source.GetType();

            foreach(PropertyInfo pi in _type.GetProperties())
            {
                if (pi.CanRead && pi.CanWrite)
                {
                    object value = GetPropertyObject(source, pi.Name, true);

                    SetProperty(dest, pi.Name, value);
                }
            }

            return dest;
        }

        /// <summary>
        /// This was added to reduce the number of casting exceptions when cloning controls
        /// </summary>
        /// <param name="source">Source control to clone</param>
        /// <returns>Cloned control</returns>
        public static Control CloneControl(Control source)
        {
            var dest = Activator.CreateInstance(source.GetType()) as Control;

            Type _type = source.GetType();

            foreach (PropertyInfo pi in _type.GetProperties())
            {
                if (pi.CanRead && pi.CanWrite && pi.Name != "InnerHtml" && pi.Name != "InnerText")
                {
                    object value = GetPropertyObject(source, pi.Name, true);

                    SetProperty(dest, pi.Name, value);
                }
            }

            return dest;
        }

        public static void CloneObject(object source,object destination)
        {
            if (source != null && destination != null && source.GetType() == destination.GetType())
            {
                Type _type = source.GetType();

                foreach (PropertyInfo pi in _type.GetProperties())
                {
                    if (pi.CanRead && pi.CanWrite)
                    {
                        object value = GetPropertyObject(source, pi.Name);

                        SetProperty(destination, pi.Name, value);
                    }
                }
            }
        }
    }
}
