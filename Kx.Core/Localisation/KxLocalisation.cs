﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Xml;
using Kx.Core.Configuration;
using Kx.Core.Data;
using System.Threading;

namespace Kx.Core.Localisation
{
    public class KxLocalisation
    {
        protected string keyValue;
        protected SortedDictionary<string, string> substitutions;

        public string KeyValue
        {
            get
            {
                return keyValue;
            }
            set
            {
                keyValue = value;
            }
        }

        public SortedDictionary<string, string> Substitutions
        {
            get
            {
                if (substitutions == null)
                {
                    substitutions = new SortedDictionary<string, string>();
                }
                return substitutions;
            }
        }

        public KxLocalisation()
        {
            
        }

        public KxLocalisation(string source)
        {
            keyValue = source;
            Load();
        }


        public string GetSubstitution()
        {
            if (Substitutions.Count == 0)   // pha
                return keyValue;
            string result = keyValue;

            string culture = Thread.CurrentThread.CurrentUICulture.Name.ToLower();

            if (KxDbConfiguration.GetConfigurationBool("LocaliseFromQueryParamLANG", false))
            {
                culture = (HttpContext.Current.Request.Params["lang"] ?? KxDbConfiguration.GetConfiguration("DefaultLanguage", culture)).ToLower();
            }
                        
            if (Substitutions.ContainsKey(culture))
            {
                result = Substitutions[culture];
            }
        
            return result;
        }

        public void Load()
        {
            KxData result = new KxData("websubstitution", "websubstitution", TargetType.Table, ActionType.Select);

            result.AddKeyField("keyvalue", keyValue, KxValueType.String);

            if (result.Execute())
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(result.XmlResults);

                XmlNodeList nodes = doc.SelectNodes("//websubstitution/datarow");

                foreach (XmlNode node in nodes)
                {
                    foreach (XmlNode node2 in node.ChildNodes)
                    {
                        if (node2.Name != "keyvalue" && !String.IsNullOrEmpty(node2.InnerText))
                        {
                            Substitutions.Add(node2.Name, node2.InnerText);
                        }
                    }
                }

                if (Substitutions.Count == 0 && KxLocalisationManager.CreateSubstitutions)
                {
                    Save();
                }
            }
        }

        public void Save()
        {
            try
            {
                KxData result = new KxData("websubstitution", "websubstitution", TargetType.Table, ActionType.Insert);

                result.AddKeyField("keyvalue", keyValue, KxValueType.String);

                result.Execute();
            }
            catch { }
        }
    }
}
