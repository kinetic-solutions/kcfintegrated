﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Resources;
using System.Reflection;
using Kx.Core.Configuration;

namespace Kx.Core.Localisation
{
    public class KxLocalisationManager
    {
        private static SortedDictionary<string, KxLocalisation> substitutions = null;
        private static object useSubstitutions = null;
        private static object createSubstitutions = null;

        public static SortedDictionary<string, KxLocalisation> Substitutions
        {
            get
            {
                if (substitutions == null)
                {
                    substitutions = new SortedDictionary<string, KxLocalisation>();
                }
                return substitutions;
            }
        }

        public static bool UseSubstitutions
        {
            get
            {
                if (useSubstitutions == null)
                {
                    try
                    {
                        useSubstitutions = KxDbConfiguration.GetConfigurationBool("UseSubstitutions",false);
                    }
                    catch
                    {
                        useSubstitutions = false;
                    }
                }
                return (bool)useSubstitutions;
            }
        }

        public static bool CreateSubstitutions
        {
            get
            {
                if (createSubstitutions == null)
                {
                    try
                    {
                        createSubstitutions = KxDbConfiguration.GetConfigurationBool("CreateSubstitutions",false);
                    }
                    catch
                    {
                        createSubstitutions = false;
                    }
                }
                return (bool)createSubstitutions;
            }
        }
                
        public static string GetSubstitution(string Source)
        {
            string result = Source;

            if (UseSubstitutions)
            {
                if (Substitutions.ContainsKey(Source))
                {
                    if (Substitutions[Source] != null)
                    {
                        result = Substitutions[Source].GetSubstitution();
                    }
                }
                else
                {
                    KxLocalisation loc = new KxLocalisation(Source);
                    Substitutions.Add(Source, loc);
                    result = Substitutions[Source].GetSubstitution();
                }
            }       
            return result;
        }
    }
}
