﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Kx.Core.Security
{
    public class AuthenticateResponse
    {
        public bool Authenticated { get; set; }
        public string AuthenticationMessage { get; set; }

        public string UserIdentifier { get; set; }
        public string UserName { get; set; }
        public string Forename { get; set; }
        public string Surname { get; set; }

        public string DisplayName
        {
            get
            {
                return string.Format("{0} {1}", Forename, Surname);
            }
        }

        public string EmailAddress { get; set; }
    }
}
