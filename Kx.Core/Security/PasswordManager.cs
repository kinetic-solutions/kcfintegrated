﻿using Kx.Core.Encryption;
using System;
using System.Collections.Generic;
using System.Text;

namespace Kx.Core.Security
{
    public class PasswordManager
    {
        #region Fields and Attributes
        public string Password { get; private set; }
        public string Salt { get; private set; }
        public string HashedPassword { get; private set; }
        public bool IsValid { get; private set; }
        #endregion


        #region Constructors
        /// <summary>
        /// Constructor to generate a new password hash and salt based upon entered password.
        /// Use the HashedPassword and Salt properties to get the generated values
        /// </summary>
        /// <param name="newPassword">The required password to be hashed.</param>
        public PasswordManager(string newPassword)
        {
            Reset();
            Password = newPassword;
            GeneratePasswordHashFromNewPassword();
        }

        /// <summary>
        /// Automatically validates the password against the entered value and the salt for the operator.
        /// Check the IsValid after construction
        /// </summary>
        /// <param name="enteredPassword">The password entered by the operator</param>
        /// <param name="currentSalt">The current SALT for the operator</param>
        /// <param name="currentHashedPassword">The currently hashed password</param>
        public PasswordManager(string enteredPassword, string currentSalt, string currentHashedPassword)
        {
            Reset();
            Password = enteredPassword;
            Salt = currentSalt;
            HashedPassword = currentHashedPassword;
            ValidatePasswordEntered();
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Sets the HashedPassword and Salt values based upon new password
        /// being entered into the Password field
        /// </summary>
        private void GeneratePasswordHashFromNewPassword()
        {
            Salt = Guid.NewGuid().ToString();
            HashedPassword = KxSHA1.Hash(Password, Salt);
            IsValid = true;
        }

        /// <summary>
        /// Reset the class to be able to populate new values
        /// </summary>
        private void Reset()
        {
            Password = string.Empty;
            Salt = string.Empty;
            HashedPassword = string.Empty;
            IsValid = false;
        }

        /// <summary>
        /// Function to validate the password which has been entered by the operator
        /// matches the Hashed password stored.
        /// </summary>
        /// <returns>True if password matches</returns>
        private void ValidatePasswordEntered()
        {
            IsValid = KxSHA1.Hash(Password, Salt).Equals(HashedPassword);
        }
        #endregion
    }
}
