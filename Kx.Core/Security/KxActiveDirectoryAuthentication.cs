﻿using System;
using System.Collections.Generic;
using System.Text;
using System.DirectoryServices;
using Kx.Core.Configuration;

namespace Kx.Core.Security
{
    public class KxActiveDirectoryAuthentication
    {


        public static bool Authenticate(string userName, string password)
        {
            string domain = KxDbConfiguration.GetConfiguration("ActiveDirectoryDomain");

            bool SSL = KxDbConfiguration.GetConfiguration("NoSSL").ToLower() != "true";

            return Authenticate(userName, password, domain, SSL);
        }

        public static bool Authenticate(string userName, string password, string domain,bool SSL)
        {
            AuthenticationTypes AT = AuthenticationTypes.None;

            AT |= AuthenticationTypes.Secure;

            if (SSL)
            {
                AT |= AuthenticationTypes.SecureSocketsLayer;
            }

            AT |= AuthenticationTypes.ReadonlyServer;

            bool result = false;

            try
            {
                using (DirectoryEntry entry = new DirectoryEntry("LDAP://" + domain, userName, password))
                {
                    if (AT != AuthenticationTypes.None)
                    {
                        entry.AuthenticationType = AT;
                    }
                    object nativeObject = entry.NativeObject;
                    result = true;
                }
            }
            catch (System.Runtime.InteropServices.COMException)
            {
            }
            return result;
        }
    }
}
