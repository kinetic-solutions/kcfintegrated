using System;
using System.Security.Cryptography;
using System.IO;
using System.Text;
using Kx.Core.Exceptions;

namespace Kx.Core.Security
{

    /// <summary>
    /// Summary description for TripleDes.
    /// </summary>
    public class KxWebSecurity
    {
        /// <summary>
        /// Encrypt the data
        /// </summary>
        /// <param name="InputText"></param>
        /// <param name="Password"></param>
        /// <returns></returns>
        public static string EncryptString(string InputText, string Password)
        {
            return Kx.Core.Encryption.Rijndael.EncryptString(InputText, Password);
        }

        /// <summary>
        /// Decrypts the Text
        /// </summary>
        /// <param name="InputText"></param>
        /// <param name="Password"></param>
        /// <returns></returns>
        public static string DecryptString(string InputText, string Password)
        {
            try
            {
                return Kx.Core.Encryption.Rijndael.DecryptString(InputText, Password);
            }
            catch(Exception ex)
            {
                KxLogger.LogActivity("KxWebSecurity", "DecryptString", ex.Message, "");
                return string.Empty;
            }
        }
    }
}
