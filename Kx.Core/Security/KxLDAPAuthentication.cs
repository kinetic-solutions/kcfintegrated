﻿using System;
using System.Collections.Generic;
using System.Text;
using Kx.Core.Configuration;
using System.DirectoryServices;
using System.DirectoryServices.Protocols;

namespace Kx.Core.Security
{
    public class KxLDAPAuthentication
    {
        const string LDAP_PASSWORD = "M@ryH@d@L1ttleL@m8";

        protected string server1 = "";
        protected string server1UserName = "";
        protected string server1Password = "";
        protected string server1AuthenticationMethod = "";
        protected int server1Port = 0;
        protected string server2 = "";
        protected string server2UserName = "";
        protected string server2Password = "";
        protected string server2AuthenticationMethod = "";
        protected int server2Port = 0;
        protected string userNameField = "";
        protected List<string> idPropertyName = new List<string>();
        protected string objectClass = "";
        protected string serverDomain = "";
        protected string lastError = "";

        protected bool logActivity = false;
        protected StringBuilder log = null;

        protected string searchPath = "";
        protected bool useSearchPath = false;


        public string Server1
        {
            get
            {
                server1 = KxDbConfiguration.GetConfiguration("LDAPServer1");
                return server1;
            }
            set
            {
                server1 = value;
                KxDbConfiguration.SetConfiguration("LDAPServer1", server1);
            }
        }
        public string Server1UserName
        {
            get
            {
                server1UserName = KxDbConfiguration.GetConfiguration("LDAPServer1UserName");
                return server1UserName;
            }
            set
            {
                server1UserName = value;
                KxDbConfiguration.SetConfiguration("LDAPServer1UserName", server1UserName);
            }
        }
        public string Server1Password
        {
            get
            {
                server1Password = KxWebSecurity.DecryptString(KxDbConfiguration.GetConfiguration("LDAPServer1Password"), LDAP_PASSWORD);
                return server1Password;
            }
            set
            {
                server1Password = value;
                KxDbConfiguration.SetConfiguration("LDAPServer1Password", KxWebSecurity.EncryptString(server1Password, LDAP_PASSWORD));
            }
        }
        public string Server1AuthenticationMethod
        {
            get
            {
                server1AuthenticationMethod = KxDbConfiguration.GetConfiguration("LDAPServer1AuthenticationMethod");
                return server1AuthenticationMethod;
            }
            set
            {
                server1AuthenticationMethod = value;
                KxDbConfiguration.SetConfiguration("LDAPServer1AuthenticationMethod", server1AuthenticationMethod);
            }
        }
        public int Server1Port
        {
            get
            {
                server1Port = KxDbConfiguration.GetConfigurationInt("LDAPServer1Port", 0);
                return server1Port;
            }
            set
            {
                server1Port = value;
                KxDbConfiguration.SetConfiguration("LDAPServer1Port", server1Port);
            }
        }

        public string Server2
        {
            get
            {
                server2 = KxDbConfiguration.GetConfiguration("LDAPServer2");
                return server2;
            }
            set
            {
                server2 = value;
                KxDbConfiguration.SetConfiguration("LDAPServer2", server2);
            }
        }
        public string Server2UserName
        {
            get
            {
                server2UserName = KxDbConfiguration.GetConfiguration("LDAPServer2UserName");
                return server2UserName;
            }
            set
            {
                server2UserName = value;
                KxDbConfiguration.SetConfiguration("LDAPServer2UserName", server2UserName);
            }
        }
        public string Server2Password
        {
            get
            {
                server2Password = KxWebSecurity.DecryptString(KxDbConfiguration.GetConfiguration("LDAPServer2Password"), LDAP_PASSWORD);
                return server2Password;
            }
            set
            {
                server2Password = value;
                KxDbConfiguration.SetConfiguration("LDAPServer2Password", KxWebSecurity.EncryptString(server2Password, LDAP_PASSWORD));
            }
        }
        public string Server2AuthenticationMethod
        {
            get
            {
                server2AuthenticationMethod = KxDbConfiguration.GetConfiguration("LDAPServer2AuthenticationMethod", "");
                return server2AuthenticationMethod;
            }
            set
            {
                server2AuthenticationMethod = value;
                KxDbConfiguration.SetConfiguration("LDAPServer2AuthenticationMethod", server2AuthenticationMethod);
            }
        }
        public int Server2Port
        {
            get
            {
                server2Port = KxDbConfiguration.GetConfigurationInt("LDAPServer2Port", 0);
                return server2Port;
            }
            set
            {
                server2Port = value;
                KxDbConfiguration.SetConfiguration("LDAPServer2Port", server2Port);
            }
        }

        public string UserNameField
        {
            get
            {
                userNameField = KxDbConfiguration.GetConfiguration("LDAPUserNameField", "anr");
                return userNameField;
            }
            set
            {
                userNameField = value;
                KxDbConfiguration.SetConfiguration("LDAPUserNameField", userNameField);
            }
        }
        public List<string> IdPropertyName
        {
            get
            {
                List<string> values = new List<String>();
                values.AddRange(KxDbConfiguration.GetConfiguration("LDAPIdProperty").Split(','));
                idPropertyName = values;
                return idPropertyName;
            }
            set
            {
                idPropertyName = value;
                KxDbConfiguration.SetConfiguration("LDAPIdProperty", string.Join(",", idPropertyName.ToArray()));
            }
        }
        public string ObjectClass
        {
            get
            {
                objectClass = KxDbConfiguration.GetConfiguration("LDAPObjectClass", "user");
                return objectClass;
            }
            set
            {
                objectClass = value;
                KxDbConfiguration.SetConfiguration("LDAPObjectClass", objectClass);
            }
        }
        public string ServerDomain
        {
            get
            {
                serverDomain = KxDbConfiguration.GetConfiguration("LDAPServerDomain");
                return serverDomain;
            }
            set
            {
                serverDomain = value;
                KxDbConfiguration.SetConfiguration("LDAPServerDomain", serverDomain);
            }
        }

        public string SearchPath
        {
            get
            {
                searchPath = KxDbConfiguration.GetConfiguration("SearchPath");
                return searchPath;
            }
            set
            {
                searchPath = value;
                KxDbConfiguration.SetConfiguration("SearchPath", searchPath);
            }
        }

        public bool UseSearchPath
        {
            get
            {
                useSearchPath = KxDbConfiguration.GetConfigurationBool("UseSearchPath", false);
                return useSearchPath;
            }
            set
            {
                useSearchPath = value;
                KxDbConfiguration.SetConfiguration("UseSearchPath", useSearchPath.ToString());
            }
        }


        public string LastError
        {
            get
            {
                return lastError;
            }
            set
            {
                lastError = value;
            }
        }

        public bool LogActivity
        {
            get
            {
                return logActivity;
            }
            set
            {
                logActivity = value;
            }
        }

        public StringBuilder Log
        {
            get
            {
                if (log == null)
                {
                    log = new StringBuilder();
                }
                return log;
            }
            set
            {
                log = value;
            }
        }

        public KxLDAPAuthentication()
        {
        }

        public DirectoryEntry GetServerConnection(string server, string username, string password, string authentication, int serverport)
        {
            DirectoryEntry result = null;
            try
            {
                string serverPath = server;

                if (serverport > 0)
                {
                    serverPath += String.Format(":{0}", serverport);
                }

                if (serverDomain != string.Empty)
                {
                    serverPath += String.Format("/{0}", serverDomain);
                }

                if (username != "")
                {
                    result = new DirectoryEntry(String.Format("LDAP://{0}", serverPath));
                    result.AuthenticationType = AuthenticationToType(authentication);
                    result.Username = username;
                    result.Password = password;
                }
                else
                {
                    result = new DirectoryEntry(String.Format("LDAP://{0}", serverPath));
                }

                if (result.Name != null)
                {
                    Log.AppendLine(String.Format("Connection succeeded to '{0}' found directory entry '{1}'", serverPath, result.Name));
                }
                else
                {
                    Log.AppendLine(String.Format("Connection failed to '{0}'", serverPath));
                }
            }
            catch (Exception ex)
            {
                Log.AppendLine(String.Format("Connection attempt failed for server '{0}', user '{1}', '{2}'", server, username, ex.Message));
                LastError = ex.Message;
                result = null;
            }
            return result;
        }


        public DirectoryEntry GetDefaultServer()
        {
            DirectoryEntry result = null;

            result = GetServerConnection(Server1, Server1UserName, Server1Password, Server1AuthenticationMethod, Server1Port);

            if (result == null)
            {
                Log.AppendLine("Failed to get connection to server 1, trying server 2.");
                result = GetServerConnection(Server2, Server2UserName, Server2Password, Server2AuthenticationMethod, Server2Port);

                if (result == null)
                {
                    Log.AppendLine("Failed to get connection to server 2.");
                }
            }
            else
            {
                result.RefreshCache();
                Log.AppendLine(String.Format("Using Connection to: {0}", result.Name));
            }
            return result;
        }

        public string GetStudentIdForUser(string username, string password)
        {
            string result = "";

            LastError = "";

            Log.AppendLine(String.Format("Getting Student Id for {0}", username));

            DirectoryEntry de = GetDefaultServer();

            if (de != null)
            {
                try
                {
                    DirectoryEntry user = FindEntry(username);

                    if (user == null)
                    {
                        LastError = "User Name not found";
                    }
                    else
                    {
                        if (Authenticate(username, password))
                        {
                            if (user.Properties != null)
                            {
                                Log.AppendLine(String.Format("User has {0} Properties", user.Properties.Count));
                            }
                            else
                            {
                                Log.AppendLine(String.Format("Properties{0}", username));
                            }

                            user.RefreshCache();

                            Log.AppendLine(String.Format("Schema Classname : \"{0}\"", user.SchemaClassName));

                            string idProperty = GetUserPropertyMatchingIdPropertyName(user);

                            if (idProperty != null)
                            {
                                Log.AppendLine(String.Format("Found Id: {0}", IdPropertyName));
                                result = (string)user.Properties[idProperty][0];
                            }
                            else
                            {
                                Log.AppendLine(String.Format("No property found for: \"{0}\"", IdPropertyName));
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Log.AppendLine(String.Format("Error Occurred: \"{0}\"", ex.Message));
                }
                finally
                {
                    de.Close();
                }
            }
            return result;
        }

        public DirectoryEntry FindEntry(string source)
        {
            Log.AppendLine(String.Format("Attempting Single Level Search for: {0}", source));
            return FindEntry(source, System.DirectoryServices.SearchScope.OneLevel);
        }

        public DirectoryEntry FindEntry(string source, System.DirectoryServices.SearchScope scope)
        {
            LastError = "";

            DirectoryEntry result = null;

            DirectoryEntry de = GetDefaultServer();

            if (de != null)
            {
                try
                {
                    try
                    {
                        using (DirectorySearcher ds = new DirectorySearcher())
                        {
                            ds.SearchRoot = de;
                            ds.Filter = String.Format("(&(objectclass={0})({1}={2}))", ObjectClass, UserNameField, source);
                            Log.AppendLine(String.Format("Filter: {0}", ds.Filter));
                            ds.SearchScope = scope;
                            ds.CacheResults = true;
                            ds.ReferralChasing = ReferralChasingOption.None;

                            int index = 0;
                            SearchResult src = null;

                            while (src == null && index < IdPropertyName.Count)
                            {
                                ds.PropertiesToLoad.Add(IdPropertyName[index]);
                                src = ds.FindOne();
                                index++;
                            }

                            if (src != null)
                            {
                                Log.AppendLine(String.Format("Entry Found: {0}", src.Path));
                                if (UseSearchPath)
                                {
                                    searchPath = GetExtension(src.Path);
                                }
                                result = src.GetDirectoryEntry();
                            }
                            else
                            {
                                if (src == null && scope == System.DirectoryServices.SearchScope.OneLevel)
                                {
                                    Log.AppendLine(String.Format("Attempting Subtree Search for: {0}", source));
                                    result = FindEntry(source, System.DirectoryServices.SearchScope.Subtree);
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Log.AppendLine(String.Format("Exception: {0}", ex.Message));
                        LastError = "User not found #1:";
                        result = null;
                    }
                }
                finally
                {
                    de.Close();
                }
            }
            else
            {
                LastError = "Unable to connect to specified server(s)";

            }
            return result;
        }

        public bool Authenticate(string userName, string password)
        {
            bool result = false;
            Log.AppendLine(String.Format("Authenticating: {0}", userName));
            DirectoryEntry de = GetDefaultServer();

            if (de != null)
            {
                try
                {
                    try
                    {
                        DirectoryEntry entry = new DirectoryEntry(de.Path);
                        entry.AuthenticationType = de.AuthenticationType;
                        entry.Username = ExtendUserName(userName);
                        entry.Password = password;

                        object nativeObject = entry.NativeObject;
                        result = true;
                        Log.AppendLine(String.Format("User Authenticated for {0}", userName));
                    }
                    catch (System.Runtime.InteropServices.COMException ex)
                    {
                        Log.AppendLine(String.Format("Authentication failed: {0} {1}", userName, ex.Message));
                        LastError = String.Format("Login failed: {0}", ex.Message);
                    }
                }
                finally
                {
                    de.Close();
                }
            }

            return result;
        }

        protected string ExtendUserName(string userName)
        {
            string result = userName;


            if (!String.IsNullOrEmpty(searchPath))
            {
                result = String.Format("{0}={1},{2}", UserNameField, userName, searchPath);
                Log.AppendLine(String.Format("Extending Username : {0} to {1}", userName, result));
            }
            else if (ServerDomain != string.Empty)
            {

                result = String.Format("{0}={1},{2}", UserNameField, userName, ServerDomain);
                Log.AppendLine(String.Format("Extending Username : {0} to {1}", userName, result));
            }

            return result;
        }

        protected string GetExtension(string path)
        {
            string result = "";

            Log.AppendLine(String.Format("Original Path = {0}", path));

            int idx = path.ToUpper().IndexOf("OU=");

            if (idx > -1)
            {
                result = path.Remove(0, idx);
            }

            Log.AppendLine(String.Format("Revised Path = {0}", result));

            return result;
        }

        protected AuthenticationTypes AuthenticationToType(string auth)
        {
            switch (auth.ToUpper())
            {
                case "NONE": return AuthenticationTypes.None;
                case "SECURE": return AuthenticationTypes.Secure;
                case "SECURESOCKETSLAYER": return AuthenticationTypes.SecureSocketsLayer;
                default: return AuthenticationTypes.None;
            }
        }

        protected AuthType AuthTypeToType(string auth)
        {
            switch (auth.ToUpper())
            {
                case "BASIC": return AuthType.Basic;
                case "DIGEST": return AuthType.Digest;
                case "DPA": return AuthType.Dpa;
                case "EXTERNAL": return AuthType.External;
                case "KERBEROS": return AuthType.Kerberos;
                case "MSN": return AuthType.Msn;
                case "NEGOTIATE": return AuthType.Negotiate;
                case "SICILY": return AuthType.Sicily;

                case "ANONYMOUS": return AuthType.Anonymous;
                case "NTLM": return AuthType.Ntlm;

                case "NONE": return AuthType.Anonymous;
                case "SECURE": return AuthType.Basic;
                case "SECURESOCKETSLAYER": return AuthType.Ntlm;

                default: return AuthType.Anonymous;
            }
        }

        //-------------------------- Extended Methods (created for compatibility with OpenLDAP --------------------------------------

        public LdapConnection GetServerConnectionExtended(string server, string username, string password, string authentication, int serverport)
        {
            LdapConnection result = null;
            try
            {
                LdapDirectoryIdentifier dir = null;

                if (serverport != 0)
                {
                    dir = new LdapDirectoryIdentifier(server, serverport);
                }
                else
                {
                    dir = new LdapDirectoryIdentifier(server);
                }

                result = new LdapConnection(dir);

                result.AuthType = AuthTypeToType(authentication);

                System.Net.NetworkCredential credentials = new System.Net.NetworkCredential(username, password);
                result.Bind(credentials);
            }
            catch (Exception ex)
            {
                LastError = ex.Message;
                result = null;
            }
            return result;
        }

        public LdapConnection GetDefaultServerExtended()
        {
            LdapConnection result = null;

            string server = "Server 1";

            result = GetServerConnectionExtended(Server1, Server1UserName, Server1Password, Server1AuthenticationMethod, Server1Port);

            if (result == null)
            {
                result = GetServerConnectionExtended(Server2, Server2UserName, Server2Password, Server2AuthenticationMethod, Server2Port);
                server = "Server 2";
            }

            if (result != null)
            {
                Log.AppendLine(String.Format("Using Connection to: {0}", server));
            }
            return result;
        }

        public bool AuthenticateExtended(string userName, string password)
        {
            bool result = true;

            SearchResultEntry user = FindEntryExtended(userName);

            LdapConnection conn = GetServerConnectionExtended(Server1, ExtendUserName(user, userName), password, Server1AuthenticationMethod, Server1Port);

            if (conn == null)
            {
                conn = GetServerConnectionExtended(Server2, ExtendUserName(user, userName), password, Server2AuthenticationMethod, Server2Port);
            }

            if (conn == null)
            {
                Log.AppendLine(String.Format("Authentication failed for User {0}", userName));
                result = false;
            }
            else
            {
                Log.AppendLine(String.Format("User {0} Authenticated", userName));
            }

            return result;
        }

        public SearchResultEntry FindEntryExtended(string source)
        {
            SearchResultEntry result = null;

            LdapConnection ldapConn =  GetDefaultServerExtended();

            if (ldapConn != null)
            {
                using (ldapConn)
                {
                    try
                    {
                        SearchRequest findme = new SearchRequest();

                        Log.AppendLine(String.Format("Path to Search: {0}", SearchPath));

                        findme.DistinguishedName = SearchPath;

                        findme.Filter = String.Format("(&(objectclass={0})({1}={2}))", ObjectClass, UserNameField, source);

                        Log.AppendLine(String.Format("Filter Applied: {0}", findme.Filter));

                        findme.Scope = System.DirectoryServices.Protocols.SearchScope.Subtree;

                        SearchResponse results = (SearchResponse)ldapConn.SendRequest(findme);

                        try
                        {
                            result = results.Entries[0];
                            Log.AppendLine(String.Format("Object Found: {0}", result.DistinguishedName));
                        }
                        catch
                        {
                            Log.AppendLine("No search result found");
                            result = null;
                        }
                    }
                    catch (Exception ex)
                    {
                        Log.AppendLine(String.Format("Error Occurred: \"{0}\"", ex.Message));
                    }
                }
            }
            return result;
        }



        public string GetStudentIdForUserExtended(string username, string password)
        {
            string result = "";

            LastError = "";

            Log.AppendLine(String.Format("Getting Student Id for {0} ", username));

            SearchResultEntry user = FindEntryExtended(username);

            if (user == null)
            {
                LastError = "User Name not found";
            }
            else
            {
                if (AuthenticateExtended(username, password))
                {
                    Log.AppendLine(String.Format("User Found and authenticated: {0}", user.DistinguishedName));

                    string idAttribute = GetUserAttributeMatchingIdPropertyName(user);

                    if (idAttribute != null)
                    {
                        result = user.Attributes[idAttribute][0].ToString();
                        Log.AppendLine(String.Format("Id property found returning: {0}", result));
                    }
                    else
                    {
                        Log.AppendLine("Id property NOT found");
                    }
                }
            }
            return result;
        }

        protected string ExtendUserName(SearchResultEntry search, string userName)
        {
            string result = userName;

            if (search != null)
            {
                result = String.Format("{0}={1},{2}", UserNameField, userName, GetExtension(search.DistinguishedName));
                Log.AppendLine(String.Format("Extending Username : {0} to {1}", userName, result));
            }
            else if (!String.IsNullOrEmpty(searchPath))
            {
                result = String.Format("{0}={1},{2}", UserNameField, userName, searchPath);
                Log.AppendLine(String.Format("Extending Username : {0} to {1}", userName, result));
            }
            else if (ServerDomain != string.Empty)
            {
                result = String.Format("{0}={1},{2}", UserNameField, userName, ServerDomain);
                Log.AppendLine(String.Format("Extending Username : {0} to {1}", userName, result));
            }

            return result;
        }

        private string GetUserPropertyMatchingIdPropertyName(DirectoryEntry user)
        {
            int index = 0;

            while (index < IdPropertyName.Count)
            {
                if (user.Properties.Contains(IdPropertyName[index]))
                {
                    return IdPropertyName[index];
                }
                index++;
            }

            return null;
        }

        private string GetUserAttributeMatchingIdPropertyName(SearchResultEntry user)
        {
            int index = 0;

            while (index < IdPropertyName.Count)
            {
                if (user.Attributes.Contains(IdPropertyName[index]))
                {
                    return IdPropertyName[index];
                }
                index++;
            }

            return null;
        }

		public bool UserExists(String Username)
		{
			bool result = false;
			Log.AppendLine(String.Format("Checking username {0} exists on AD", Username));

			//DirectoryEntry de = GetDefaultServer();

			using (DirectoryEntry de = GetDefaultServer())
			{
				if (de != null)
				{
					try
					{
						DirectoryEntry user = FindEntry(Username);
						result = (user == null) ? false : true;
					}
					catch (Exception ex)
					{
						Log.AppendLine(String.Format("Error Occurred: \"{0}\"", ex.Message));
					}
					finally
					{
						de.Close();
					}
				}
			}

			return result;
		}
    }
}
