﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Kx.Core.Security
{
    public enum AuthenticationSource
    {
        KxInternal = 0,
        LDAP = 1,
        MixedMode = 2
    }

    public enum AuthenticationLDAPType
    {
        AD = 0,
        OpenLDAP = 1
    }

    public abstract class AuthenticationStore
    {
        #region Constructors
        /// <summary>
        /// Base object to entire system, containing the factory and auditor used throughout.
        /// </summary>
        public AuthenticationStore()
        {
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Authenticates the user against the store
        /// </summary>
        /// <param name="username">The username entered</param>
        /// <param name="password">The password entered</param>
        /// <returns>A response is return, or a KxAuthenticationException on any errors</returns>
        public abstract AuthenticateResponse Authenticate(string username, string password);

        /// <summary>
        /// Check to see if the username already exists in the store
        /// </summary>
        /// <param name="username">The user who is to be checked</param>
        /// <returns>True if the username exists</returns>
        public abstract bool UsernameExists(string username);
        #endregion
    }

}
