﻿using System;
using System.Collections.Generic;
using System.Text;
using Kx.Core.Configuration;

namespace Kx.Core.Security
{
    public class KxBruteForce
    {
        protected string iPAddress = "";
        protected int strikes = 0;
        protected DateTime lastStrike;
        protected int strikeLimit = 0;
        protected int blockTime = 0;


        public string IPAddress
        {
            get
            {
                return iPAddress;
            }
            set
            {
                iPAddress = value;
            }
        }
        public int Strikes
        {
            get
            {
                return strikes;
            }
            set
            {
                strikes = value;
            }
        }
        public DateTime LastStrike
        {
            get
            {
                return lastStrike;
            }
            set
            {
                lastStrike = value;
            }
        }

        public int StrikeLimit
        {
            get
            {
                if (strikeLimit == 0)
                {
                    strikeLimit = KxDbConfiguration.GetConfigurationInt("BruteForceAttempts", 6);//Carlys fat finger compensator
                }
                return strikeLimit;
            }
            set
            {
                strikeLimit = value;
            }
        }
        public int BlockTime
        {
            get
            {
                if (blockTime == 0)
                {
                    blockTime = KxDbConfiguration.GetConfigurationInt("BruteForceTimeout", 30);
                }
                return blockTime;
            }
            set
            {
                blockTime = value;
            }
        }

        public bool IsBlocked
        {
            get
            {
                return AreStikesExceeded && !IsTimeoutPassed;
            }
        }

        public bool IsTimeoutPassed
        {
            get
            {
                return LastStrike.AddMinutes(BlockTime) < DateTime.Now;
            }
        }

        public bool AreStikesExceeded
        {
            get
            {
                return Strikes >= StrikeLimit;
            }
        }

    }

    public class KxBruteForceBlocker
    {
        protected SortedList<string, KxBruteForce> items = new SortedList<string, KxBruteForce>();

  
        public SortedList<string, KxBruteForce> Items
        {
            get
            {
                return items;
            }
            set
            {
                items = value;
            }
        }

    

        public bool AddStrike(string ipaddress)
        {
            bool result = false;

            if (items.ContainsKey(ipaddress.Trim()))
            {
                KxBruteForce bf = items[ipaddress.Trim()];

                if (bf.IsTimeoutPassed)
                {
                    bf.Strikes = 0;
                }

                bf.Strikes += 1;
                bf.LastStrike = DateTime.Now;

                return bf.IsBlocked;
            }
            else
            {
                KxBruteForce bf = new KxBruteForce();
                bf.IPAddress = ipaddress;
                bf.Strikes = 1;
                bf.LastStrike = DateTime.Now;
                Items.Add(ipaddress, bf);
            }

            return result;
        }

        public bool IsBlocked(string ipaddress)
        {
            bool result = false;

            if (items.ContainsKey(ipaddress.Trim()))
            {
                KxBruteForce bf = items[ipaddress.Trim()];

                return bf.IsBlocked;
            }
            
            return result;
        }
    }
}
