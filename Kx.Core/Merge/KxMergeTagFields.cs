﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Exceptions;
using Kx.Core.Reflection;
using Kx.Core.Utility;
using System.Xml;

namespace Kx.Core.Merge
{
    public class KxMergeTagFields : KxBaseClass, IEnumerable<KxMergeTagField>
    {
        private List<KxMergeTagField> _items = new List<KxMergeTagField>();

        /// <summary>
        /// 
        /// </summary>
        public int TagId
        {
            get { return base.dBIdentifier; }
        }
	
		/// <summary>
		/// Returns a KxMergeTagProvider from the items list at the specified position
		/// </summary>
		public KxMergeTagField this[int index]
		{
			get { return _items[index]; }  
            set { _items[index] = value; }   
		}

		/// <value>
		/// Contians a collection of KxMergeTagProvider
		/// </value>
        public List<KxMergeTagField> Items
        {
            get { return _items; }
        }
		
		/// <summary>
		/// Creates an instance of this class
		/// </summary>
		public KxMergeTagFields(KxBaseClass parent)
			: this(parent, 0)
		{
		}

		/// <summary>
		/// Creates an instance of this class
		/// </summary>
		public KxMergeTagFields(KxBaseClass parent, int TagId)
			: base(parent)
		{            
            base.dBIdentifier = TagId;

            Load();
		}

		/// <summary>
		/// 
		/// </summary>
        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = null;

            if (actiontype == ActionType.Select)
            {
                result = new KxData("mergetagfileds", "MergeTagFields", TargetType.Table);
                result.AddKeyField("TagId", TagId, KxValueType.Int);
            }

            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        public override bool LoadFromXml(string xml, string xpath)
        {
            _items.Clear();
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xml);

                XmlNodeList nodes = doc.SelectNodes(@"//results/" + xpath + "/datarow");

                foreach (XmlNode node in nodes)
                {
                    if (node != null)
                    {
                        KxMergeTagField tagField = new KxMergeTagField(this, node);
                        Items.Add(tagField);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "LoadFromXml", ex.Message, xpath);
                LastError = ex.Message;
                return false;
            }
        }





        #region IEnumerable<KxMergeTagField> Members

        public IEnumerator<KxMergeTagField> GetEnumerator()
        {
            return _items.GetEnumerator();
        }

        #endregion

        #region IEnumerable Members

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _items.GetEnumerator();
        }

        #endregion

    }
}





