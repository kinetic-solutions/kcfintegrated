using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;
using Kx.Core.Data;
using Kx.Core.Utility;

namespace Kx.Core.Merge
{
    public enum KxMergeTagSetName
    {
        GetST2Students,
        GetST2StudentAllocations
    }

    public class KxMergeTagProvider
    {        
        //private string mergeTagName      = string.Empty;
        private string parameterName     = string.Empty;
        private object parameterValue    = string.Empty;
        protected Dictionary<string, string> _tags          = new Dictionary<string, string>();

        private string currentMergeTag   = string.Empty;
        private int currentMergeTagIndex = 0;
        
        private int    _tagId            = 0;
        protected string _name             = string.Empty;
        protected string _nameTag          = string.Empty;
        private string _alias            = string.Empty;
        private string _sqlScript        = string.Empty;

        /// <summary>
        /// 
        /// </summary>
        public int    TagId
        {
            get { return _tagId; }         
        }
        /// <summary>
        /// 
        /// </summary>
        public string Name
        {
            get { return _name; }         
        }
        /// <summary>
        /// 
        /// </summary>
        public string NameTag
        {
            get { return _nameTag; }         
        }
        /// <summary>
        /// 
        /// </summary>
        public string Alias
        {
            get { return _alias; }         
        }
        /// <summary>
        /// 
        /// </summary>
        public string SQLScript
        {
            get { return _sqlScript; }
        }


        /// <summary>
        /// 
        /// </summary>
        public string CurrentMergeTag
        {
            get { return currentMergeTag; }
            set { currentMergeTag = value.ToUpper(); }
        }
        /// <summary>
        /// 
        /// </summary>
        public int CurrentMergeTagIndex
        {
            get
            { return currentMergeTagIndex; }
            set
            { currentMergeTagIndex = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string MergeTagName
        {
            get { return _nameTag; }
            set { _nameTag = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string ParameterName
        {
            get
            { return parameterName; }
            set
            { parameterName = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public object ParameterValue
        {
            get
            { return parameterValue; }
            set
            { parameterValue = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string[] MergeTagArray
        {
            get { return GetMergeTagArray(); }
        }
        /// <summary>
        /// 
        /// </summary>
        public bool MergeTagExists
        {
            get { return GetMergeTagExists(); }
        }
        /// <summary>
        /// 
        /// </summary>
        public string MergeTag
        {
            get { return GetMergeTag(); }
        }
        /// <summary>
        /// 
        /// </summary>
        public string MergeTagUpper
        {
            get { return GetMergeTag().ToUpper(); }
        }

        public IEnumerable<string> MergeTagNames
        {
            get { return _tags.Keys.AsEnumerable(); }
        }
        /// <summary>
        /// 
        /// </summary>
        public KxMergeTagProvider(string mergeName, string paramName, object paramValue)
        {
            _nameTag       = mergeName;
            ParameterName  = paramName;
            ParameterValue = paramValue;
            try
            {
                Load();
            }
            catch 
            {                
            }
        }

        protected KxMergeTagProvider()
        {
            
        }

        /// <summary>
        /// 
        /// </summary>
        public virtual void Load()
        {
            if (ParameterValue != null)
            {
                string sql = GetSqlScript();

                if (ParameterValue.GetType() == typeof(string))
                {
                    sql = ReplaceParameter(sql,":" + ParameterName, "'" + ParameterValue.ToString() + "'");
                }
                else
                {
                    sql = ReplaceParameter(sql,":" + ParameterName, ParameterValue.ToString());
                }

                KxData data;
                if (_sqlScript == string.Empty)
                    data = new KxData("tags", sql, TargetType.StoredProc);
                else data = new KxData("tags", sql, TargetType.Static);

                if (data.Execute())
                {
                    XmlDocument doc = new XmlDocument();
                    doc.LoadXml(data.XmlResults);

                    XmlNode node = doc.SelectSingleNode("//tags/datarow");

                    _tags.Clear();

                    foreach (XmlNode childnode in node.ChildNodes)
                    {
                         try
                        {
                            _tags.Add(childnode.Name.ToUpper(), childnode.InnerText);
                        }
                        catch 
                        {                                
                        }
                    }
                }              
            }
        }

        private string ReplaceParameter(string source,string paramName, string paramValue)
        {
            return Regex.Replace(source, paramName, paramValue, RegexOptions.IgnoreCase);
        }

        /// <summary>
        /// 
        /// </summary>
        private string GetSqlScript()
        {
            string result = "";

            KxData data = new KxData("list", "MergeTags", TargetType.Table, ActionType.Select);

            data.AddKeyField("NameTag", MergeTagName, KxValueType.String);

            if (data.Execute())
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(data.XmlResults);

                XmlNode node = doc.SelectSingleNode("//list/datarow");

                _tagId     = KxXmlUtils.GetNodeValue(node, "TagId", 0);
                _name      = KxXmlUtils.GetNodeValue(node, "Name", string.Empty);
                _nameTag   = KxXmlUtils.GetNodeValue(node, "NameTag", string.Empty);
                _alias     = KxXmlUtils.GetNodeValue(node, "Alias", string.Empty);
                _sqlScript = KxXmlUtils.GetNodeValue(node, "SQLScript", string.Empty);

                if (_sqlScript != string.Empty)
                    result = _sqlScript;
                else result = _nameTag;
            }

            return result;
        }
        /// <summary>
        /// 
        /// </summary>
        public string[] GetMergeTagArray()
        {
            string result = "";

            for (int i = 1;i < 10000;i++)
            {
                string key = currentMergeTag.Replace("#",i.ToString());
                if (_tags[key] != null)
                {
                    result += i.ToString()+",";
                }
                else
                {
                    break;
                }
            }

            return result.Split(new string[] { "," } , StringSplitOptions.RemoveEmptyEntries);
        }

        /// <summary>
        /// 
        /// </summary>
        public bool GetMergeTagExists()
        {
            return _tags[currentMergeTag] != null && _tags[currentMergeTag].ToString() != "";
        }

        /// <summary>
        /// 
        /// </summary>
        public string GetMergeTag()
        {
            try
            {
                return _tags[currentMergeTag.ToUpper()].ToString();
            }
            catch
            {
                return "?"+currentMergeTag;
            }
        }

        public string GetMergeTagWF(string tag)
        {
            try
            {
                return _tags[tag.ToUpper()].ToString();
            }
            catch
            {
                return "?" + tag;
            }
        }
    }
}
