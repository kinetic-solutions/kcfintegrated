﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Exceptions;
using Kx.Core.Reflection;
using Kx.Core.Utility;
using System.Xml;

namespace Kx.Core.Merge
{
    [Serializable]
    public class KxMergeTagObject : KxBaseClass
    {        
      //  private int _mergeTagId;
        
        #region Attributes & Properties

        protected string           _name;
        protected string           _nameTag;
        protected string           _alias;
        protected string           _sqlscript;
        protected KxMergeTagFields _fields;

        
        /// <summary>
        /// 
        /// </summary>
        public int TagId
        {
            get { return base.dBIdentifier; }
        }
        
        /// <summary>
        /// 
        /// </summary>
        public string Name
        {
            get { return _name; }
            set 
            {
                AddToAudit("Name", _name, value);
                _name = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public string NameTag
        {
            get { return _nameTag; }
            set
            {
                AddToAudit("NameTag", _nameTag, value);
                _nameTag = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public string Alias
        {
            get { return _alias; }
            set
            {
                AddToAudit("Alias", _alias, value);
                _alias = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public string Sqlscript
        {
            get { return _sqlscript; }
            set
            {
                AddToAudit("Sqlscript", _sqlscript, value);
                _sqlscript = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public KxMergeTagFields Fields
        {
            get 
            {
                if (_fields == null)
                    _fields = new KxMergeTagFields(null, TagId);
                
                return _fields;             
            }            
        }
        
        #endregion

        /// <summary>
		/// Creates an instance of this class
		/// </summary>
		public KxMergeTagObject(KxBaseClass parent)
            : base(parent)
        { 
        }

        /// <summary>
		/// Creates an instance of this class
		/// </summary>
		public KxMergeTagObject(KxBaseClass parent, XmlNode node)
            : base(parent)
        {
            LoadFromXml("", node);
        }

        /// <summary>
		/// Creates an instance of this class
		/// </summary>
		public KxMergeTagObject(KxBaseClass parent, int TagId)
            : base(parent)
        {
            base.dBIdentifier = TagId;
            Load();
        }

        /// <summary>
        /// Creates an instance of this class
        /// </summary>
        public KxMergeTagObject(KxBaseClass parent, string MergeTagName)
            : base(parent)
        {
            _name = MergeTagName;
                        
            Load();
        }

        /// <summary>
        /// 
        /// </summary>
        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("mergetags", "MergeTags", TargetType.Table);

            if (_name == "")
                result.AutoKeyField = new KxDataValue("TagId", TagId, KxValueType.Int);
            else result.AutoKeyField = new KxDataValue("Name", _name, KxValueType.String);


            if ((actiontype == ActionType.Update) && (TagId == 0))
                result.Action = ActionType.Insert;
            else result.Action = actiontype;


            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {
                result.AddParameter("TagId", base.dBIdentifier, KxValueType.Int);
                result.AddParameter("NameTag",   _nameTag,   KxValueType.String, 50, false);
                result.AddParameter("alias",     _alias,     KxValueType.String, 5, false);
                result.AddParameter("sqlscript", _sqlscript, KxValueType.String, 2147483647, true);
            }
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        public override void LoadFromXml(string xml, XmlNode node)
        {
            dBIdentifier = KxXmlUtils.GetNodeValue(node, "tagid", 0);
            _name        = KxXmlUtils.GetNodeValue(node, "name", "");
            _nameTag     = KxXmlUtils.GetNodeValue(node, "nametag", "");
            _alias        = KxXmlUtils.GetNodeValue(node, "alias", "");
            _sqlscript    = KxXmlUtils.GetNodeValue(node, "sqlscript", "");
        }
    }
}





