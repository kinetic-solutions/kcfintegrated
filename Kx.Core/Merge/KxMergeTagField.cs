﻿using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;


namespace Kx.Core.Merge
{
    [Serializable]
    public class KxMergeTagField : KxBaseClass
    {
        public enum KxMergeIDType
        {
            FieldId,
            MergeTagId
        }

        #region Attributes & Properties

        protected int           _tagId;
        protected string        _field;
        protected string        _fieldTag;
        protected KxMergeIDType _idType;

        /// <summary>
        /// 
        /// </summary>
        public int FieldId
        {
            get { return base.dBIdentifier;}
        }

        /// <summary>
        /// 
        /// </summary>
        public int TagId
        {
            get { return _tagId; }
            set
            { 
                AddToAudit("TagId", _tagId, value);
                _tagId = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Field
        {
            get { return _field; }
            set
            { 
                AddToAudit("Field", _field, value);
                _field = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public string FieldTag
        {
            get { return _fieldTag; }
            set
            { 
                AddToAudit("FieldTag", _fieldTag, value);
                _fieldTag = value;
            }
        }


        #endregion 

        /// <summary>
        /// 
        /// </summary>
        public KxMergeTagField(KxBaseClass parent) : base(parent)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        public KxMergeTagField(KxBaseClass parent, XmlNode node)
            :base(parent)
        {
             LoadFromXml("", node);
        }

        /// <summary>
        /// 
        /// </summary>
        public KxMergeTagField(KxBaseClass parent, int FieldId)
            : base(parent)
        {
            _idType = KxMergeIDType.FieldId;
            base.dBIdentifier = FieldId;

            Load();
        }

        /// <summary>
        /// 
        /// </summary>
        public KxMergeTagField(KxBaseClass parent, int Id, KxMergeIDType IdType)
            : base(parent)
        {
            _idType = IdType;
             base.dBIdentifier = Id;

             Load();
        }

        /// <summary>
        /// 
        /// </summary>
        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("mergetagfields", "MergeTagFields", TargetType.Table);

            if (_idType == KxMergeIDType.FieldId)
                result.AutoKeyField = new KxDataValue("FieldId", FieldId, KxValueType.Int);
            else result.AutoKeyField = new KxDataValue("TagId", TagId, KxValueType.Int);

            
            if ((actiontype == ActionType.Update) && (FieldId == 0))
                result.Action = ActionType.Insert;
            else result.Action = actiontype;


            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {
                   result.AddParameter("field", _field, KxValueType.String, 50, false);
                   result.AddParameter("fieldTag", _fieldTag, KxValueType.String, 50, false);
            }

            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        public override void LoadFromXml(string xml,XmlNode node)
        {
            dBIdentifier = KxXmlUtils.GetNodeValue(node,"fieldid",0);
            _tagId       = KxXmlUtils.GetNodeValue(node,"tagid",0);
            _field       = KxXmlUtils.GetNodeValue(node,"field","");
            _fieldTag    = KxXmlUtils.GetNodeValue(node,"fieldtag","");
        }
    }
}
