﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Kx.Core.Merge
{
    /// <summary>
    /// 
    /// </summary>
    public interface IMergeObject
    {
        KxMergeTagProvider MergeTags { get; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class KxDocumentMerger 
    {
        private Dictionary<string, object> _providers = new Dictionary<string, object>();

        /// <summary>
        /// 
        /// </summary>
        public KxDocumentMerger()
        {
        }

        /// <summary>
        /// Add a KxMergeTagProvider object to the class, the KxMergeTagProviders are used to source the replacement values 
        /// when MergeTagsWithDoc is called.
        /// </summary>
        public void AddMergeTagProvider(IMergeObject MergeObject)
        {
            if (MergeObject != null)
            {
                if (_providers.ContainsKey(MergeObject.MergeTags.Alias))
                {
                    _providers[MergeObject.MergeTags.Alias] = MergeObject.MergeTags;
                }
                else
                {
                    _providers.Add(MergeObject.MergeTags.Alias, MergeObject.MergeTags);
                }
            }
        } 

        /// <summary>
        /// Merges the specified string with the KxMergeTagProvider objects added to the class using AddMergeTagProvider().
        /// </summary>        
        public string MergeDocument(string Doc)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(Doc);

            int index = 0;
            while (sb.ToString().IndexOf("<", index) > -1)
            {
                int idx = sb.ToString().IndexOf("<", index);
                int endidx = sb.ToString().IndexOf(">", idx);

                string element = sb.ToString().Substring(idx, (endidx - idx) + 1);

                sb.Replace(element, GetMergeData(element));

                index = idx+1;
            }

            return sb.ToString();
        }    

        /// <summary>
        /// Returns the coresponding value for the specified Tag. If no value is found it returns the original Tag.
        /// </summary>
        public virtual string GetMergeData(string Tag)
        {
            string result = Tag;
            string tmp = Tag.Replace("<", "").Replace(">", "");

            int pos = tmp.IndexOf(":");

            if (pos > -1)
            {
                string objAlias = tmp.Substring(0, pos);
                string objTag   = tmp.Substring(pos+1);
                
                if (_providers.ContainsKey(objAlias))
                {
                    KxMergeTagProvider mtp = _providers[objAlias] as KxMergeTagProvider;
                    
                    if (mtp != null) 
                    {
                        mtp.CurrentMergeTag = objTag;

                        if (mtp.GetMergeTagExists())
                        {                      
                            result = mtp.GetMergeTag();
                        }
                    }           
                }
            }

            return result;            
        }       
    }
}
