﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using Kx.Core.Reflection;
using System.Reflection;

namespace Kx.Core.Merge
{
    public enum KxTemplateExceptionStatus
    {
        Undefined,
        MissingEndBrace
    }

    public class KxMergeIndex
    {
        public int startIndex;
        public int endIndex;
        
        public KxMergeIndex(int start, int end)
        {
            startIndex = start;
            endIndex = end;   
        }        
    }

    [Serializable]
    public class KxTemplateException : Exception
    {

        private KxTemplateExceptionStatus status;

        /// <summary>
        /// 
        /// </summary>
        public KxTemplateException(string msg, Exception ex)
            : base(msg, ex)
        {
            this.status = KxTemplateExceptionStatus.Undefined;
        }

        /// <summary>
        /// 
        /// </summary>
        public KxTemplateException(KxTemplateExceptionStatus status, string msg, Exception ex)
            : base(msg, ex)
        {
            this.status = status;
        }

        /// <summary>
        /// 
        /// </summary>
        public KxTemplateExceptionStatus Status
        {
            get
            {
                return status;
            }
        }
    }

    /// <summary>
    /// Summary description for Class1.
    /// </summary>
    public class KxTemplateMerge
    {
        private enum MergeTagPart
        {
            ObjectName,
            ParameterName,
            Format
        }

        private char _stTag = '{';
        private char _enTag = '}';

        private Dictionary<string, object> _objects = new Dictionary<string, object>();
        private string _template;

        /// <summary>
        /// 
        /// </summary>
        public char StartTag
        {
            get { return _stTag; }
            set { _stTag = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public char EndTag
        {
            get { return _enTag; }
            set { _enTag = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string MergedTemplate
        {
            get { return _template; }
        }

        public bool DoNotClearUnfoundMergeTags { get; set; } = false;

        /// <summary>
        /// 
        /// </summary>
        public KxTemplateMerge()
        {
            AddObject("CurrentDate", DateTime.Now);
        }

        /// <summary>
        /// 
        /// </summary>
        public void AddObject(string name, object obj)
        {
            if (_objects.ContainsKey(name))
            {
                _objects[name] = obj;
            }
            else
            {
                _objects.Add(name, obj);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public string MergeTemplate(string template)
        {
            _template = template;
            if (!string.IsNullOrEmpty(_template))
            {
                List<string> mergeTags = new List<string>();
                int startidx = _template.IndexOf(_stTag);

                while (startidx >= 0)
                {
                    mergeTags.Add(_template.Substring(startidx, _template.IndexOf(_enTag, startidx) - startidx + 1));
                    startidx = _template.IndexOf(_stTag, startidx + 1);
                }

                foreach (string s in mergeTags)
                {
                    string objectName = GetTagPart(s, MergeTagPart.ObjectName);
                    string paramName = GetTagPart(s, MergeTagPart.ParameterName);
                    string format = GetTagPart(s, MergeTagPart.Format);

                    object propValue = null;

                    if (_objects.ContainsKey(objectName))
                    {
                        if (!string.IsNullOrEmpty(paramName))
                        {
                            propValue = KxReflection.GetPropertyObject(_objects[objectName], paramName);
                        }
                        else
                        {
                            propValue = _objects[objectName];
                        }
                    }

                    string sValue = propValue != null ? GetFormattedValue(propValue, format) : DoNotClearUnfoundMergeTags ? s : "";

                    _template = _template.Replace(s, sValue);
                }
            }

            return _template;
        }


        /// <summary>
        /// 
        /// </summary>
        public bool MergeTemplatePreflight(string template, Dictionary<string, Type> types, out List<KxMergeIndex> mergeTags)
        {            
            mergeTags = new List<KxMergeIndex>();

            if (!string.IsNullOrEmpty(template))
            {   
                int startidx = template.IndexOf(_stTag);
                int endindex = -1;
                string tmp = string.Empty;

                while (startidx >= 0)
                {
                    endindex = template.IndexOf(_enTag, startidx) - startidx + 1;

                    if (endindex > 0)
                    {
                        tmp = template.Substring(startidx, endindex);

                        string objectName = GetTagPart(tmp, MergeTagPart.ObjectName);
                        string paramName  = GetTagPart(tmp, MergeTagPart.ParameterName);
                        string format     = GetTagPart(tmp, MergeTagPart.Format);

                        if (types.ContainsKey(objectName))
                        {
                            if (!string.IsNullOrEmpty(paramName))
                            {
                                if (!PropertyExists(types[objectName], paramName))
                                {
                                    mergeTags.Add(new KxMergeIndex(startidx, endindex));
                                }


                                //PropertyInfo pInfo = types[objectName].GetProperty(paramName);

                                //if (pInfo == null)
                                //{
                                //    mergeTags.Add(new KxMergeIndex(startidx, endindex));
                                //}
                            }                            
                        }
                        else
                        {
                            mergeTags.Add(new KxMergeIndex(startidx, endindex));
                        }
                                                
                    }

                    startidx = template.IndexOf(_stTag, startidx + 1);
                }                
            }

            return mergeTags.Count == 0;
        }

        
        /// <summary>
        /// 
        /// </summary>
        private bool PropertyExists(Type type, string property)
        {
            bool result = false;
            
            string       tmp = property;
            int          index = tmp.IndexOf('.');
            PropertyInfo pInfo = null;

            if (tmp.IndexOf('.') > 0)
            {
                tmp = tmp.Substring(0, index);

                pInfo = type.GetProperty(tmp);

                if (pInfo != null)
                    result = PropertyExists(pInfo.PropertyType, property.Substring(index + 1));
            }
            else
            {
                pInfo = type.GetProperty(property);

                result = pInfo != null;
            }

            return result;
        }



        /// <summary>
        /// ObjectName everything between { and first . or : or }. ParameterName everything between first . and : or } 
        /// Format everything between : and }
        /// </summary>
        private string GetTagPart(string s, MergeTagPart mergeTagPart)
        {
            int startIdx = -1;
            int endIdx = -1;
            string result = "";

            switch (mergeTagPart)
            {
                case MergeTagPart.ObjectName:
                    startIdx = 0;
                    endIdx = s.IndexOfAny(new char[] { '.', ':', _enTag });
                    break;
                case MergeTagPart.ParameterName:
                    startIdx = s.IndexOf('.');
                    endIdx = s.IndexOfAny(new char[] { ':', _enTag });
                    break;
                case MergeTagPart.Format:
                    startIdx = s.IndexOf(':');
                    endIdx = s.IndexOf(_enTag);
                    break;
            }

            if (startIdx > -1)
            {
                if (endIdx - startIdx < 0)
                    endIdx = s.Length - 1;

                result = s.Substring(startIdx + 1, endIdx - startIdx - 1);
            }

            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        public string GetFormattedValue(object value, string format)
        {
            string result = "";

            if (!string.IsNullOrEmpty(format))
            {
                if (value is DateTime)
                {
                    result = ((DateTime)value).ToString(format);
                }
                if (value is int)
                {
                    result = ((int)value).ToString(format);
                }
                if (value is decimal)
                {
                    result = ((decimal)value).ToString(format);
                }
                if (value is float)
                {
                    result = ((float)value).ToString(format);
                }
                if (value is double)
                {
                    result = ((double)value).ToString(format);
                }
                if (value is string)
                {
                    int iLen;
                    if (int.TryParse(format, out iLen))
                    {
                        string sValue = value.ToString();

                        if (sValue.Length > iLen)
                        {
                            result = sValue.Substring(0, iLen - 3);
                            result += "...";
                        }
                    }
                }
            }

            if (string.IsNullOrEmpty(result))
            {
                result = value.ToString();
            }

            return result;
        }

        /*public KxTemplateMerge()
        {
            fields = new Hashtable(20);
            result = new StringBuilder(4096);
        }

        public string TemplateName
        {
            set
            {
                templateName = value;
            }
        }

        /// <summary>
        /// Add String value to field list
        /// </summary>
        /// <param name="field"></param>
        /// <param name="fieldValue"></param>
        public void Add(string field, string fieldValue)
        {
            field = field.ToLower();
            if (fields.ContainsKey(field))
            {
                fields[field] = fields[field] + fieldValue;
            }
            else
            {
                fields.Add(field, fieldValue);
            }
        }

        public void Set(string field, string fieldValue)
        {
            field = field.ToLower();
            if (fields.ContainsKey(field))
            {
                fields[field] = fieldValue;
            }
            else
            {
                fields.Add(field, fieldValue);
            }
        }

        public void Add(string field, string fieldValue, string navalue)
        {

            if (fieldValue == null || fieldValue.Length == 0 || fieldValue == " ")
            {
                fieldValue = navalue;
            }


            field = field.ToLower();
            if (fields.ContainsKey(field))
            {
                fields[field] = fields[field] + fieldValue;
            }
            else
            {
                fields.Add(field, fieldValue);
            }
        }

        public string ParseString(string template)
        {
            for (int t = 0; t < template.Length; t++)
            {
                char currentChar = template[t];

                if (currentChar == '{')
                {
                    int nextPos = t + 1;
                    currentChar = template[nextPos];
                    if (currentChar == '#')
                    {
                        bool endFound = false;
                        int endPos = t + 3;
                        int subBrackets = 0;
                        int strlen = template.Length;
                        while (!endFound && endPos < strlen)
                        {
                            if (template[endPos] == '{')
                            {
                                endPos++;
                                if (template[endPos] == '@')
                                {
                                    subBrackets++;
                                }
                            }
                            if (template[endPos] == '}')
                            {
                                if (subBrackets > 0)
                                {
                                    subBrackets--;
                                    if (subBrackets < 0)
                                        throw new Exception("Field Brace Mismatch");
                                }
                                else
                                {
                                    endFound = true;
                                }
                            }
                            endPos++;
                        }

                        if (endPos > strlen)
                        {
                            throw new KxTemplateException(KxTemplateExceptionStatus.MissingEndBrace, "end } not found", null);
                        }

                        string firstHalf = template.Substring(0, t);
                        string secondHalf = template.Substring(endPos, template.Length - endPos);

                        string name = template.Substring(t + 2, (endPos - t - 3));
                        string originalName = template.Substring(t, (endPos - t));
                        //check fields that have been added externally
                        string fieldValue = null;
                        if (fields.Count > 0)
                        {
                            fieldValue = (string)fields[name.ToLower()];
                        }

                        if (fieldValue != null)
                        {
                            template = firstHalf + fieldValue + secondHalf;
                        }
                        else
                        {
                            template = firstHalf + originalName + secondHalf;
                            t++;
                        }
                        t--;
                    }
                }
            }
            return template;
        }*/
    }
}
