using System;
using System.Collections.Generic;
using System.Text;
using Kx.Core.Data;
using Kx.Core.Exceptions;

namespace Kx.Core.Base
{
    [Serializable]
    public class KxBaseClassList : IDisposable
    {
        [NonSerialized]
        public KxBaseClass parent;
        [NonSerialized]
        protected List<KxBaseClass> children = new List<KxBaseClass>();
   
        public List<KxBaseClass> Children
        {
            get
            { return children;  }
            set
            { children = value; }
        }

        public KxBaseClass Parent
        {
            get
            { return parent; }
            set
            { parent = value; }
        }
        
        public KxBaseClassList(KxBaseClass _parent)
        {
            parent = _parent;
        }

        public int IndexOf(KxBaseClass item)
        {
            return children.IndexOf(item);
        }

        public void Clear()
        {
            children.Clear();
        }

        public void Add(KxBaseClass item)
        {
            children.Add(item);
        }

        public void Remove(KxBaseClass item)
        {
            children.Remove(item);
        }

        public bool Delete(KxData data)
        {
            bool result = true;

            foreach (KxBaseClass baseclass in children)
            {
                result = baseclass.Delete(data);
                if (!result)
                {
                    break;
                }
            }

            return result;
        }

        public void ResetAudit()
        {
            foreach (KxBaseClass baseclass in Children)
            {
                try
                {
                    baseclass.ResetAudit();
                }
                catch { }
            }
        }

        public bool Save(KxData data)
        {
            bool result = true;

            new KxData().LogDebug("SaveChildren", "<Parent>" + this.Parent.ToString() + "</Parent>"); // pha
            foreach (KxBaseClass baseclass in children)
            {
                try
                {
                    if (baseclass != null)
                    {
                        result = baseclass.Save(data);

                        if (!result)
                        {
                            if (parent != null)
                            {
                                parent.LastError = baseclass.LastError;
                                break;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    KxLogger.LogActivity(this, "Save2", ex.Message, "");
                }
            }

            return result;
        }

        public void Dispose()
        {
            foreach (KxBaseClass baseclass in children)
            {
                baseclass.Dispose();
            }
        }
    }
}
