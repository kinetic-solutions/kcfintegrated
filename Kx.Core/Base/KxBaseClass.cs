using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Data;
using System.Reflection;
using Kx.Core.Exceptions;
using Kx.Core.Reflection;

namespace Kx.Core.Base
{
    [Serializable]
    public class KxBaseClass : IDisposable
    {
        #region Attributes & Properties
        private bool temporary = false;
        protected int dBIdentifier = 0;
        protected bool isKxClass = true;
        protected Guid guidIdentifier = Guid.Empty;
        protected bool changed = false;
        protected bool childChanged = false;
        protected string systemUser = "";
        protected string lastError = "";
        protected string childError = "";
        protected bool _writeAudit = true;
        [NonSerialized]
        protected KxBaseClass parent = null;
        [NonSerialized]
        protected KxBaseClassList children;
        protected StringBuilder auditDetails = new StringBuilder();


        public string LastError
        {
            get
            { return lastError; }
            set
            { 
                lastError = value;
                if (parent != null)
                {
                    ((KxBaseClass)parent).ChildError = lastError;
                }
            }
        }
        public string ChildError
        {
            get
            { return childError; }
            set
            {
                childError = value;
                if (parent != null)
                {
                    ((KxBaseClass)parent).ChildError = childError;
                }
            }
        }

        protected bool IsKxClass
        {
            get
            { return isKxClass; }
            set
            { isKxClass = value; }
        }

        public string SystemUser
        {
            get
            { return systemUser; }
            set
            { systemUser = value; }
        }

        public bool Changed
        {
            get
            { return changed && !temporary; }
            set
            {
                if (value && !temporary)
                {
                    changed = true;
                    if (parent != null)
                    {
                        ((KxBaseClass)parent).ChildChanged = true;
                    }
                }
                else
                {
                    changed = false;
                }
            }
        }

        public string AuditDetails
        {
            get
            {
                return auditDetails.ToString();
            }
        }

        public bool ChildChanged
        {
            get
            { return childChanged; }
            set
            {
                if (value && !temporary)
                {
                    childChanged = true;
                    if (parent != null)
                    {
                        ((KxBaseClass)parent).ChildChanged = true;
                    }
                }
            }
        }

        public KxBaseClassList Children
        {
            get
            { return children; }
        }

        public virtual Guid GuidIdentifier
        {
            get
            {
                return guidIdentifier;
            }
        }

        public KxBaseClass Parent
        {
            get
            { return parent; }
            set
            {
                if (value != null)
                {
                    value.Children.Add(this);
                }

                parent = value; 
            }
        }

        public bool Temporary
        {
            get
            {
                return temporary;
            }
            set
            {
                if (temporary == value)
                    return;

                if (value)
                {
                    dBIdentifier = new Random(DateTime.Now.Hour * DateTime.Now.Millisecond).Next(99999999,Int32.MaxValue);
                    guidIdentifier = Guid.NewGuid();
                    changed = false;
                }
                else
                {
                    changed = true;
                    dBIdentifier = 0;
                    guidIdentifier = Guid.Empty;
                }
                temporary = value;
            }
        }
        public bool IsValid
        {
            get
            {
                return dBIdentifier != 0 || guidIdentifier != Guid.Empty;
            }
        }

        #endregion

        public KxBaseClass(): this(null)
        {
        }

        public KxBaseClass(KxBaseClass parent) 
        {
            children = new KxBaseClassList(this);
            this.parent = parent;
            if (parent != null)
            {
                if (parent.Children.IndexOf(this) < 0)
                {
                    parent.Children.Add(this);
                }
            }
        }

        public KxBaseClass(KxBaseClass parent,bool _isKxClass):this(parent)
        {

            IsKxClass = _isKxClass;
        }

        public string GetId()
        {
            if (dBIdentifier != 0)
            {
                return dBIdentifier.ToString();
            }
            else
            {
                return guidIdentifier.ToString();
            }
        }

        public void ResetAudit()
        {
            Children.ResetAudit();

            auditDetails = new StringBuilder();
            changed = false;
            temporary = false;
            childChanged = false;
        }

        public void RemoveClass()
        {
            if (parent != null)
            {
                parent.Children.Remove(this);
            }
        }

  
        protected bool AddToAudit(string fieldName, object from, object to)
        {
            bool result = false;
            string fromStr = "null";
            string toStr = "null";

            if (from != null)
            {
                fromStr = from.ToString();
            }

            if (to != null)
            {
                toStr = to.ToString();
            }

            if (fromStr != toStr)
            {
                string AuditStr = fieldName + " changed from " + fromStr + " to " + toStr + ". ";
                auditDetails.AppendLine(AuditStr);
                Changed = true;
                result = true;
            }
            return result;
        }
  
        public virtual bool LoadFromXml(string xml,string xpath)
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xml);

                XmlNode node = doc.SelectSingleNode(@"//results/"+xpath+"/datarow");

                if (node != null)
                {
                    LoadFromXml(xml,node);
                }

                return true;
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "LoadXml", ex.Message, xpath);
                LastError = ex.Message;
                return false;
            }
        }

        public virtual void LoadFromXml(string xml,XmlNode node)
        {
        }
                
        public virtual void NotifyDelete(KxBaseClass child)
        {
           
        }
        
        public virtual bool Load()
        {
            bool result = true;

            KxData data = ConfigureData(ActionType.Select);

            if (data != null)
            {
                result = data.Execute();
                if (data._norecords && data.AutoKeyField != null)
                {
                    new KxData().LogDebug("LoadFail", this.GetType().Name + ". " + data.AutoKeyField.Name + ": " + data.AutoKeyField.Value); // pha
                }
                if (!result)
                {
                    LastError = data.LastError;
                }
                else
                {
                    try
                    {
                        LoadFromXml("<results>" + data.XmlResults + "</results>", data.RootName);
                        result = true;
                        changed = false;
                        temporary = false;
                        childChanged = false;
                    }
                    catch
                    {
                        KxLogger.LogActivity(this, "Load()", data.LastError, "");
                        LastError = data.LastError;
                        result = false;
                    }
                }
                    
            }
            return result;
        }

        public void SaveFromWorkflow()
        {
            if (!Save())
            {
                throw new Exception("Error during Save: " + LastError);
            }
        }
   

        public virtual bool Save()
        {
            bool result = true;
            string log = "";
            try
            {
                if (!Temporary)
                {
                    KxData data = ConfigureData(ActionType.Update);

                    if (data != null)
                    {
                        data.Initialise();
                        try
                        {
                            if (changed)
                            {
                                result = data.Execute();

                                if (result)
                                {
                                    if (dBIdentifier == 0)
                                    {
                                        dBIdentifier = data.Identifier;
                                    }
                                    if(_writeAudit)
                                    {
                                        data.ExecuteAuditLog(this);
                                    }
                                }
                                else
                                {
                                    try { KxLogger.LogActivity(this, "Save", data.LastError, ""); }
                                    catch { }
                                    // Insert failed, so reset Id's
                                    if (data.Action == ActionType.Insert)
                                    {
                                        guidIdentifier = Guid.Empty;
                                    }
                                }
                            }

                            if (!result)
                            {
                                LastError = data.LastError;
                            }
                            else
                            {
                                if (childChanged)
                                {
                                    result = Children.Save(data);    
                                }
                            }
                        }
                        finally
                        {
                            if (result)
                            {
                                data.Finalise();
                                ResetAudit();
                            }
                            else
                            {
                                data.Cancel();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                try { KxLogger.LogActivity(this, "Save2", ex.Message, ""); }
                catch { }
                LastError = ex.Message;
                result = false;
            }
           
            return result;
        }

        public virtual bool Save(KxData parentdata)
        {
            bool result = true;

            if (!Temporary)
            {
                KxData data = ConfigureData(ActionType.Update);
               
                if (changed && data != null)
                {
                    result = data.Execute(parentdata);

                    if (result)
                    {
                        if (dBIdentifier == 0)
                        {
                            dBIdentifier = data.Identifier;
                        }

                        //pha
                        //if(_writeAudit)
                        //{
                        //    data.ExecuteAuditLog(this);
                        //}
                    }
                    else
                    {
                        try { KxLogger.LogActivity(this, "Sub-Save", data.LastError, ""); }
                        catch { }
                        // Insert failed, so reset Id's
                        if (data.Action == ActionType.Insert)
                        {
                            guidIdentifier = Guid.Empty;
                        }
                    }
                }

                if (!result)
                {
                    LastError = data.LastError;
                }
                else
                {
                    result = Children.Save(parentdata);
                }
            }
            return result;
        }

        public virtual bool Delete()
        {
            bool result = true;

            KxData data = ConfigureData(ActionType.Delete);
            try
            {
                if (data != null)
                {
                    data.Initialise();
                }
                try
                {
                    result = Children.Delete(data);
                   
                    if (result && data != null && !IsKxClass)
                    {
                        result = data.Execute();

                        if (!result)
                        {
                            LastError = data.LastError;
                        }
                    }
                }
                finally
                {
                    if (result && data != null)
                    {
                        data.Finalise();
                    }
                    else
                    {
                        data.Cancel();
                    }
                }
                if (result && parent != null && !IsKxClass)
                {
                    parent.children.Remove(this);
                    parent = null;
                }

                if (Parent != null)
                {
                    Parent.NotifyDelete(this);
                }
            }
            finally
            {
                data.CloseConnection();
            }

            return result;
        }

        public virtual bool Delete(KxData parentdata)
        {
            bool result = true;

            KxData data = ConfigureData(ActionType.Delete);

            result = Children.Delete(parentdata);

            if (result && data != null && !IsKxClass)
            {
                result = data.Execute(parentdata);

                if (!result)
                {
                    LastError = data.LastError;
                }
            }
        
            if (result && parent != null && !IsKxClass)
            {
                parent.children.Remove(this);
                parent = null;
            }
            return result;
        }

        public virtual KxData ConfigureData(ActionType actiontype)
        {
            Changed = false;
            return null;
        }

        public object GetProperty(string PropertyName)
        {
            return KxReflection.GetPropertyObject(this, PropertyName);
        }

        public virtual void Dispose()
        {
            Children.Dispose();
        }
	}
 
}
