﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Security.Cryptography;

namespace Kx.Core.Encryption
{
    public class KxMD5
    {
        /// <summary>
        /// Generates a MD5 Hash of the Password
        /// </summary>
        /// <param name="password">The password..</param>
        /// <param name="salt">The salt..</param>
        /// <returns></returns>
        public static string Hash(string password, string salt)
        {
            // Returns a MD5 Hash of the Password
            return Hash(password + salt);
        }

        /// <summary>
        /// Generates a MD5 Hash of the Password
        /// </summary>
        /// <param name="password">The password..</param>
        /// <returns></returns>
        public static string Hash(string password)
        {
            string hash = string.Empty;
            if (password != string.Empty)
            {
                ASCIIEncoding encodeing = new ASCIIEncoding();
                byte[] bPassword = encodeing.GetBytes(password);

                MD5 md5 = new MD5CryptoServiceProvider();
                byte[] hashBytes = md5.ComputeHash(bPassword);

                hash = BitConverter.ToString(hashBytes).Replace("-", "");
            }

            return hash;
        }
    }
}
