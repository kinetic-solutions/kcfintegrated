﻿using System;
using System.Text;
using System.Security.Cryptography;

namespace Kx.Core.Encryption
{
    /// <summary>
    /// SHA1 Hash Provider
    /// </summary>
    public class KxSHA256
    {
        /// <summary>
        /// Generates a SHA1 Hash of the Password
        /// </summary>
        /// <param name="password">The password..</param>
        /// <param name="salt">The salt..</param>
        /// <returns></returns>
        public static string Hash(string password, string salt)
        {
            // Returns a SHA1 Hash of the Password
            return Hash(password + salt);
        }

        /// <summary>
        /// Generates a SHA1 Hash of the Password
        /// </summary>
        /// <param name="password"></param>
        /// <returns></returns>
        public static string Hash(string password)
        {
            string hash = string.Empty;
            if (password != string.Empty)
            {

                ASCIIEncoding encoding = new ASCIIEncoding();
                byte[] bPassword = encoding.GetBytes(password);

                SHA256 SHA256 = SHA256Managed.Create();
                byte[] hashBytes = SHA256.ComputeHash(bPassword);

                hash = BitConverter.ToString(hashBytes).Replace("-", "");
            }

            return hash;
        }
    }
}



