using System;
using System.Text;
using System.Security.Cryptography;

namespace Kx.Core.Encryption
{
	/// <summary>
	/// SHA1 Hash Provider
	/// </summary>
	public class KxSHA1
	{
		/// <summary>
		/// Generates a SHA1 Hash of the Password
		/// </summary>
		/// <param name="password">The password..</param>
        /// <param name="salt">The salt..</param>
		/// <returns></returns>
		public static string Hash(string password, string salt)
		{
			// Returns a SHA1 Hash of the Password
			return Hash(password + salt);
		}

		/// <summary>
		/// Generates a SHA1 Hash of the Password
		/// </summary>
		/// <param name="password">The password..</param>
		/// <returns></returns>
		public static string Hash(string password)
		{
            string hash = string.Empty;
            if (password != string.Empty)
            {
                ASCIIEncoding encodeing = new ASCIIEncoding();
                byte[] bPassword = encodeing.GetBytes(password);

                SHA1 sha1 = new SHA1CryptoServiceProvider();
                byte[] hashBytes = sha1.ComputeHash(bPassword);

                hash = BitConverter.ToString(hashBytes).Replace("-", "");
            }

            return hash;
        }
	}
}



