using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;

namespace Kx.Objects.Common
{
    [Serializable]
    public class KxEventCompany : KxBaseClass
    {
        #region Attributes & Properties

        protected int      eventId;
        protected int      companyId;
        protected int      addressId;
        protected int      companyRoleId;
        protected int      agentPersonId;
        protected float	   agentCommissionRate;
        protected string   agentReferenceNo;
        protected bool     primaryCompany;
        protected int      eventCompanyId;

        public int      EventId
        {
            get
            { return eventId;}
            set
            { 
                AddToAudit("EventId", eventId, value);
                eventId = value;
            }
        }

        public int      CompanyId
        {
            get
            { return companyId;}
            set
            { 
                AddToAudit("CompanyId", companyId, value);
                companyId = value;
            }
        }

        public int      AddressId
        {
            get
            { return addressId;}
            set
            { 
                AddToAudit("AddressId", addressId, value);
                addressId = value;
            }
        }

        public int      CompanyRoleId
        {
            get
            { return companyRoleId;}
            set
            { 
                AddToAudit("CompanyRoleId", companyRoleId, value);
                companyRoleId = value;
            }
        }

        public int      AgentPersonId
        {
            get
            { return agentPersonId;}
            set
            { 
                AddToAudit("AgentPersonId", agentPersonId, value);
                agentPersonId = value;
            }
        }

        public float AgentCommissionRate
        {
            get
            { return agentCommissionRate;}
            set
            { 
                AddToAudit("AgentCommissionRate", agentCommissionRate, value);
                agentCommissionRate = value;
            }
        }

        public string   AgentReferenceNo
        {
            get
            { return agentReferenceNo;}
            set
            { 
                AddToAudit("AgentReferenceNo", agentReferenceNo, value);
                agentReferenceNo = value;
            }
        }

        public bool     PrimaryCompany
        {
            get
            { return primaryCompany;}
            set
            { 
                AddToAudit("PrimaryCompany", primaryCompany, value);
                primaryCompany = value;
            }
        }

        public int      EventCompanyId
        {
			get
			{ return base.dBIdentifier; }
		}

        #endregion 

        public KxEventCompany(KxBaseClass parent):base(parent)
        {}


        public KxEventCompany(KxBaseClass parent, XmlNode node):base(parent)
        {
             LoadFromXml("", node);
        }

		public KxEventCompany(KxBaseClass parent, int eventcompanyid)
			: base(parent)
        {
			base.dBIdentifier = eventcompanyid;
             Load();
        }


        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("eventcompanies", "EventCompanies", TargetType.Table);

            result.AutoKeyField = new KxDataValue("eventCompanyId",eventCompanyId,KxValueType.Int);

             if ((actiontype == ActionType.Update) && (eventCompanyId == 0))
             {
                 result.Action = ActionType.Insert;
             }
             else
             {
                 result.Action = actiontype;
             }

            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {
                   result.AddParameter("companyId", companyId, KxValueType.Int, false);
                   result.AddParameter("addressId", addressId, KxValueType.Int, false);
                   result.AddParameter("companyRoleId", companyRoleId, KxValueType.Int, false);
                   result.AddParameter("agentPersonId", agentPersonId, KxValueType.Int, true);
                   result.AddParameter("agentCommissionRate", agentCommissionRate, KxValueType.Float, true);
                   result.AddParameter("agentReferenceNo", agentReferenceNo, KxValueType.String, 40, true);
                   result.AddParameter("primaryCompany", primaryCompany, KxValueType.Boolean, true);
            }
            return result;
        }


        public override  void LoadFromXml(string xml,XmlNode node)
        {
			dBIdentifier = KxXmlUtils.GetNodeValue(node, "eventcompanyid", 0);
            eventId  = KxXmlUtils.GetNodeValue(node,"eventid",0);
            companyId  = KxXmlUtils.GetNodeValue(node,"companyid",0);
            addressId  = KxXmlUtils.GetNodeValue(node,"addressid",0);
            companyRoleId  = KxXmlUtils.GetNodeValue(node,"companyroleid",0);
            agentPersonId  = KxXmlUtils.GetNodeValue(node,"agentpersonid",0);
            agentCommissionRate  = KxXmlUtils.GetNodeValue(node,"agentcommissionrate",0);
            agentReferenceNo  = KxXmlUtils.GetNodeValue(node,"agentreferenceno","");
            primaryCompany  = KxXmlUtils.GetNodeValue(node,"primarycompany",false);
        }
    }
}

