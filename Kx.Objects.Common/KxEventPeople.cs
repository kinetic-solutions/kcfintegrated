using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;


namespace Kx.Objects.Common
{
    [Serializable]
    public class KxEventPeople : KxBaseClass
    {
        #region Attributes & Properties

        protected List<KxEventPerson> items = new List<KxEventPerson>();
        protected int eventId;

        public List<KxEventPerson> Items
        {
            get
            { return items; }
        }

        public KxEventPerson this[int index]
        {
            get
            { return items[index]; }
            set
            { items[index] = value; }
        }

		public IEnumerator<KxEventPerson> GetEnumerator()
		{
			return items.GetEnumerator();
		}

		#endregion

        public KxEventPeople(KxBaseClass parent, int EventId)
            : base(parent)
        {
            eventId = EventId;
            Load();
        }

        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = null;

            if (actiontype == ActionType.Select)
            {
                result = new KxData("eventpeople", "EventPeople", TargetType.Table);
                result.KeyFields.Add(new KxDataValue("EventID", eventId, KxValueType.String));
            }
            return result;
        }

        public override bool LoadFromXml(string xml, string xpath)
        {
            items.Clear();
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xml);

                XmlNodeList nodes = doc.SelectNodes(@"//results/" + xpath + "/datarow");

                foreach (XmlNode node in nodes)
                {
                    if (node != null)
                    {
						KxEventPerson eventPerson = new KxEventPerson(this, node);
                        items.Add(eventPerson);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "LoadFromXml", ex.Message, xpath);
                LastError = ex.Message;
                return false;
            }
        }

		public KxEventPerson GetByPersonRoleId(int personRoleId)
		{
			KxEventPerson result = null;

			foreach (KxEventPerson item in items)
			{
				if (item.PersonRoleId == personRoleId)
				{
					result = item;
					break;
				}
			}
			return result;
		}

        
    }
}
