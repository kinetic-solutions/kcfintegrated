using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;

namespace Kx.Objects.Common
{
    [Serializable]
    public class KxEventPerson : KxBaseClass
    {
        #region Attributes & Properties

        protected int      eventId;
        protected int      personId;
        protected int      personRoleId;
        protected int      eventPersonId;
        protected int      eventCompanyId;

        public int      EventId
        {
            get
            { return eventId;}
            set
            { 
                AddToAudit("EventId", eventId, value);
                eventId = value;
            }
        }

        public int      PersonId
        {
            get
            { return personId;}
            set
            { 
                AddToAudit("PersonId", personId, value);
                personId = value;
            }
        }

        public int      PersonRoleId
        {
            get
            { return personRoleId;}
            set
            { 
                AddToAudit("PersonRoleId", personRoleId, value);
                personRoleId = value;
            }
        }

        public int      EventPersonId
        {
            get
            { return eventPersonId;}
            set
            { 
                AddToAudit("EventPersonId", eventPersonId, value);
                eventPersonId = value;
            }
        }

        public int      EventCompanyId
        {
            get
            { return eventCompanyId;}
            set
            { 
                AddToAudit("EventCompanyId", eventCompanyId, value);
                eventCompanyId = value;
            }
        }


        #endregion 

        public KxEventPerson(KxBaseClass parent)
            : base(parent)
        {}

		public KxEventPerson(KxBaseClass parent, XmlNode node)
            : base(parent)
        {
             LoadFromXml("", node);
        }

        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("eventpeople", "EventPeople", TargetType.Table);

            result.AddKeyField("EventId", eventId, KxValueType.Int);

            result.Action = actiontype;

            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {
                   result.AddParameter("eventId", eventId, KxValueType.Int, false);
                   result.AddParameter("personId", personId, KxValueType.Int, false);
                   result.AddParameter("personRoleId", personRoleId, KxValueType.Int, true);
                   result.AddParameter("eventPersonId", eventPersonId, KxValueType.Int, false);
                   result.AddParameter("eventCompanyId", eventCompanyId, KxValueType.Int, false);
            }
            return result;
        }


        public override  void LoadFromXml(string xml,XmlNode node)
        {
            eventId  = KxXmlUtils.GetNodeValue(node,"eventid",0);
            personId  = KxXmlUtils.GetNodeValue(node,"personid",0);
            personRoleId  = KxXmlUtils.GetNodeValue(node,"personroleid",0);
            eventPersonId  = KxXmlUtils.GetNodeValue(node,"eventpersonid",0);
            eventCompanyId  = KxXmlUtils.GetNodeValue(node,"eventcompanyid",0);
        }
    }
}


