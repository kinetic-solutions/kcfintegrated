using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Exceptions;
using Kx.Core.Utility;

namespace Kx.Objects.Common
{
    public class KxTaxModels : KxBaseClass
    {
        #region Fields and Attributes
        private int _findTaxModelID;
        private List<KxTaxModel> items = new List<KxTaxModel>();

        public KxTaxModel this[int index]
        {
            get
            { return items[index]; }
            set
            { items[index] = value; }
        }

        public List<KxTaxModel> Items
        {
            get
            { return items; }
        }
        #endregion


        #region Methods

        public KxTaxModel ContainsTaxModelByID(int taxModelID)
        {
            _findTaxModelID = taxModelID;
            return Items.Find(FindTaxModelById);
        }

        private bool FindTaxModelById(KxTaxModel p)
        {
            return p.TaxModelId == _findTaxModelID;
        }

        public override bool LoadFromXml(string xml, string xpath)
        {
            items.Clear();
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xml);

                XmlNodeList nodes = doc.SelectNodes(@"//results/" + xpath + "/datarow");

                foreach (XmlNode node in nodes)
                {
                    if (node != null)
                    {
                        KxTaxModel cf = new KxTaxModel(this, node);
                        items.Add(cf);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "LoadFromXml", ex.Message, xpath);
                LastError = ex.Message;
                return false;
            }
        }

        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = null;

            if (actiontype == ActionType.Select)
            {
                result = new KxData("taxmodels", "TaxModels", TargetType.Table);
            }
            return result;
        }
        #endregion

        #region Constructors
        public KxTaxModels(KxBaseClass parent)
            : base(parent)
        {
            Load();
        }
        #endregion
    }
}
