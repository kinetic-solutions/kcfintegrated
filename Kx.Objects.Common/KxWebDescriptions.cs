﻿using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;


namespace Kx.Objects.Common
{
    [Serializable]
    public class KxWebDescriptions : KxBaseClass
    {
        #region Attributes & Properties

        protected List<KxWebDescription> items = new List<KxWebDescription>();
        protected string name;

        #endregion

        public KxWebDescriptions(KxBaseClass parent, string namevalue)
            : base(parent)
        {
            name = namevalue;
            Load();
        }

        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = null;

            if (actiontype == ActionType.Select)
            {
                result = new KxData("webdescriptions", "WebDescriptions", TargetType.Table);
                result.KeyFields.Add(new KxDataValue("Name", name, KxValueType.String));
            }
            return result;
        }

        public override bool LoadFromXml(string xml, string xpath)
        {
            items.Clear();
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xml);

                XmlNodeList nodes = doc.SelectNodes(@"//results/" + xpath + "/datarow");

                foreach (XmlNode node in nodes)
                {
                    if (node != null)
                    {
                        KxWebDescription webDescription = new KxWebDescription(this, node);
                        items.Add(webDescription);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "LoadFromXml", ex.Message, xpath);
                LastError = ex.Message;
                return false;
            }
        }

        public KxWebDescription GetWebDescriptionById(int Id)
        {
            KxWebDescription result = null;

            foreach (KxWebDescription item in items)
            {
                if (item.WebDescriptionId == Id)
                {
                    result = item;
                    break;
                }
            }
            return result;
        }

        public KxWebDescription GetWebDescriptionByID1(string id1)
        {
            KxWebDescription result = null;

            foreach (KxWebDescription item in items)
            {
                if (item.Id1 == id1)
                {
                    result = item;
                    break;
                }
            }
            return result;
        }

        
    }
}


