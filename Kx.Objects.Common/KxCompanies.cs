﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Exceptions;
using Kx.Core.Utility;

namespace Kx.Objects.Common
{
    public class KxCompanies : KxBaseClass
    {
        private List<KxCompany> items = new List<KxCompany>();

        public List<KxCompany> Items
        {
            get
            { return items; }
        }

        public KxCompany this[int index]
        {
            get
            { return items[index]; }
            set
            { items[index] = value; }
        }

        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = null;

            if (actiontype == ActionType.Select)
            {
                result = new KxData("companies", "Companies", TargetType.Table, ActionType.Select);

                result.OrderFields.Add(new KxDataOrder("CompanyId"));

            }
            return result;
        }


        public KxCompanies(KxBaseClass parent)
            : base(parent)
        {

        }


        public override bool LoadFromXml(string xml, string xpath)
        {
            items.Clear();
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xml);

                XmlNodeList nodes = doc.SelectNodes(@"//results/" + xpath + "/datarow");

                foreach (XmlNode node in nodes)
                {
                    if (node != null)
                    {
                        KxCompany multiitem = new KxCompany(this, node);
                        Items.Add(multiitem);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "LoadFromXml", ex.Message, xpath);
                LastError = ex.Message;
                return false;
            }
        }

        public KxCompany GetCompanyById(int id)
        {
            KxCompany result = null;

            foreach (KxCompany item in Items)
            {
                if (item.CompanyId == id)
                {
                    result = item;
                    break;
                }
            }

            return result;
        }
    }
}
