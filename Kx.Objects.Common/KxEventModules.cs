using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;

namespace Kx.Objects.Common
{
    [Serializable]
    public class KxEventModules : KxBaseClass
    {
        #region Attributes & Properties

        protected int      eventId;
        protected string   description;
        protected DateTime ?arrivalDate;
        protected DateTime ?startDate;
        protected DateTime ?endDate;
        protected int      statusId;
        protected short    dayPeople;
        protected string   allAdded;
        protected short    nightPeople;
        protected int      minCapacity;
        protected int      maxCapacity;
        protected int      siteId;
        protected int      companyId;
        protected string   notes;
        protected DateTime ?creationDate;
        protected int      creationUserId;
        protected DateTime ?modifiedDate;
        protected int      modifiedUserId;
        protected string   purchaseOrderNumber;
        protected bool     vatexempt;
        protected int      accountManagerPersonId;
        protected int      coordinatorPersonId;
        protected bool     exportToAccounts;
        protected int      salesTypeId;
        protected int      billingStatusId;
        protected string   debitCode;
        protected int      exportEventPersonId;
        protected string   eventAims;
        protected string   eventEthos;
        protected int      matrixId;
        protected decimal  amount;
        protected int      hassleFactor;
        protected int      resConfirmationTypeId;
        protected DateTime ?defaultArrivalDate;
        protected DateTime ?defaultDepartureDate;
        protected string   externalId;
        protected int      areaId;
        protected int      enquirySourceId;
        protected int      paymentTermsId;
        protected string   exemptTaxes;
        protected int      copiedFromEventModuleId;
        protected string   defaultConfRoomPhoneAccessCode;
        protected DateTime ?globalAllocationLocked;
        protected int      globalAllocationLockedUserId;
        protected bool eventPublished;

        public int      EventModuleId
        {
            get
            { return base.dBIdentifier;}
        }

        public int      EventId
        {
            get
            { return eventId;}
            set
            { 
                AddToAudit("EventId", eventId, value);
                eventId = value;
            }
        }

        public string   Description
        {
            get
            { return description;}
            set
            { 
                AddToAudit("Description", description, value);
                description = value;
            }
        }

        public DateTime ?ArrivalDate
        {
            get
            { return arrivalDate;}
            set
            { 
                AddToAudit("ArrivalDate", arrivalDate, value);
                arrivalDate = value;
            }
        }

        public DateTime ?StartDate
        {
            get
            { return startDate;}
            set
            { 
                AddToAudit("StartDate", startDate, value);
                startDate = value;
            }
        }

        public DateTime ?EndDate
        {
            get
            { return endDate;}
            set
            { 
                AddToAudit("EndDate", endDate, value);
                endDate = value;
            }
        }

        public int      StatusId
        {
            get
            { return statusId;}
            set
            { 
                AddToAudit("StatusId", statusId, value);
                statusId = value;
            }
        }

        public short    DayPeople
        {
            get
            { return dayPeople;}
            set
            { 
                AddToAudit("DayPeople", dayPeople, value);
                dayPeople = value;
            }
        }

        public string   AllAdded
        {
            get
            { return allAdded;}
            set
            { 
                AddToAudit("AllAdded", allAdded, value);
                allAdded = value;
            }
        }

        public short    NightPeople
        {
            get
            { return nightPeople;}
            set
            { 
                AddToAudit("NightPeople", nightPeople, value);
                nightPeople = value;
            }
        }

        public int      MinCapacity
        {
            get
            { return minCapacity;}
            set
            { 
                AddToAudit("MinCapacity", minCapacity, value);
                minCapacity = value;
            }
        }

        public int      MaxCapacity
        {
            get
            { return maxCapacity;}
            set
            { 
                AddToAudit("MaxCapacity", maxCapacity, value);
                maxCapacity = value;
            }
        }

        public int      SiteId
        {
            get
            { return siteId;}
            set
            { 
                AddToAudit("SiteId", siteId, value);
                siteId = value;
            }
        }

        public int      CompanyId
        {
            get
            { return companyId;}
            set
            { 
                AddToAudit("CompanyId", companyId, value);
                companyId = value;
            }
        }

        public string   Notes
        {
            get
            { return notes;}
            set
            { 
                AddToAudit("Notes", notes, value);
                notes = value;
            }
        }

        public DateTime ?CreationDate
        {
            get
            { return creationDate;}
            set
            { 
                AddToAudit("CreationDate", creationDate, value);
                creationDate = value;
            }
        }

        public int      CreationUserId
        {
            get
            { return creationUserId;}
            set
            { 
                AddToAudit("CreationUserId", creationUserId, value);
                creationUserId = value;
            }
        }

        public DateTime ?ModifiedDate
        {
            get
            { return modifiedDate;}
            set
            { 
                AddToAudit("ModifiedDate", modifiedDate, value);
                modifiedDate = value;
            }
        }

        public int      ModifiedUserId
        {
            get
            { return modifiedUserId;}
            set
            { 
                AddToAudit("ModifiedUserId", modifiedUserId, value);
                modifiedUserId = value;
            }
        }

        public string   PurchaseOrderNumber
        {
            get
            { return purchaseOrderNumber;}
            set
            { 
                AddToAudit("PurchaseOrderNumber", purchaseOrderNumber, value);
                purchaseOrderNumber = value;
            }
        }

        public bool     Vatexempt
        {
            get
            { return vatexempt;}
            set
            { 
                AddToAudit("Vatexempt", vatexempt, value);
                vatexempt = value;
            }
        }

        public int      AccountManagerPersonId
        {
            get
            { return accountManagerPersonId;}
            set
            { 
                AddToAudit("AccountManagerPersonId", accountManagerPersonId, value);
                accountManagerPersonId = value;
            }
        }

        public int      CoordinatorPersonId
        {
            get
            { return coordinatorPersonId;}
            set
            { 
                AddToAudit("CoordinatorPersonId", coordinatorPersonId, value);
                coordinatorPersonId = value;
            }
        }

        public bool     ExportToAccounts
        {
            get
            { return exportToAccounts;}
            set
            { 
                AddToAudit("ExportToAccounts", exportToAccounts, value);
                exportToAccounts = value;
            }
        }

        public int      SalesTypeId
        {
            get
            { return salesTypeId;}
            set
            { 
                AddToAudit("SalesTypeId", salesTypeId, value);
                salesTypeId = value;
            }
        }

        public int      BillingStatusId
        {
            get
            { return billingStatusId;}
            set
            { 
                AddToAudit("BillingStatusId", billingStatusId, value);
                billingStatusId = value;
            }
        }

        public string   DebitCode
        {
            get
            { return debitCode;}
            set
            { 
                AddToAudit("DebitCode", debitCode, value);
                debitCode = value;
            }
        }

        public int      ExportEventPersonId
        {
            get
            { return exportEventPersonId;}
            set
            { 
                AddToAudit("ExportEventPersonId", exportEventPersonId, value);
                exportEventPersonId = value;
            }
        }

        public string   EventAims
        {
            get
            { return eventAims;}
            set
            { 
                AddToAudit("EventAims", eventAims, value);
                eventAims = value;
            }
        }

        public string   EventEthos
        {
            get
            { return eventEthos;}
            set
            { 
                AddToAudit("EventEthos", eventEthos, value);
                eventEthos = value;
            }
        }

        public int      MatrixId
        {
            get
            { return matrixId;}
            set
            { 
                AddToAudit("MatrixId", matrixId, value);
                matrixId = value;
            }
        }

        public decimal  Amount
        {
            get
            { return amount;}
            set
            { 
                AddToAudit("Amount", amount, value);
                amount = value;
            }
        }

        public int      HassleFactor
        {
            get
            { return hassleFactor;}
            set
            { 
                AddToAudit("HassleFactor", hassleFactor, value);
                hassleFactor = value;
            }
        }

        public int      ResConfirmationTypeId
        {
            get
            { return resConfirmationTypeId;}
            set
            { 
                AddToAudit("ResConfirmationTypeId", resConfirmationTypeId, value);
                resConfirmationTypeId = value;
            }
        }

        public DateTime ?DefaultArrivalDate
        {
            get
            { return defaultArrivalDate;}
            set
            { 
                AddToAudit("DefaultArrivalDate", defaultArrivalDate, value);
                defaultArrivalDate = value;
            }
        }

        public DateTime ?DefaultDepartureDate
        {
            get
            { return defaultDepartureDate;}
            set
            { 
                AddToAudit("DefaultDepartureDate", defaultDepartureDate, value);
                defaultDepartureDate = value;
            }
        }

        public string   ExternalId
        {
            get
            { return externalId;}
            set
            { 
                AddToAudit("ExternalId", externalId, value);
                externalId = value;
            }
        }

        public int      AreaId
        {
            get
            { return areaId;}
            set
            { 
                AddToAudit("AreaId", areaId, value);
                areaId = value;
            }
        }

        public int      EnquirySourceId
        {
            get
            { return enquirySourceId;}
            set
            { 
                AddToAudit("EnquirySourceId", enquirySourceId, value);
                enquirySourceId = value;
            }
        }

        public int      PaymentTermsId
        {
            get
            { return paymentTermsId;}
            set
            { 
                AddToAudit("PaymentTermsId", paymentTermsId, value);
                paymentTermsId = value;
            }
        }

        public string   ExemptTaxes
        {
            get
            { return exemptTaxes;}
            set
            { 
                AddToAudit("ExemptTaxes", exemptTaxes, value);
                exemptTaxes = value;
            }
        }

        public int      CopiedFromEventModuleId
        {
            get
            { return copiedFromEventModuleId;}
            set
            { 
                AddToAudit("CopiedFromEventModuleId", copiedFromEventModuleId, value);
                copiedFromEventModuleId = value;
            }
        }

        public string   DefaultConfRoomPhoneAccessCode
        {
            get
            { return defaultConfRoomPhoneAccessCode;}
            set
            { 
                AddToAudit("DefaultConfRoomPhoneAccessCode", defaultConfRoomPhoneAccessCode, value);
                defaultConfRoomPhoneAccessCode = value;
            }
        }

        public DateTime ?GlobalAllocationLocked
        {
            get
            { return globalAllocationLocked;}
            set
            { 
                AddToAudit("GlobalAllocationLocked", globalAllocationLocked, value);
                globalAllocationLocked = value;
            }
        }

        public int      GlobalAllocationLockedUserId
        {
            get
            { return globalAllocationLockedUserId;}
            set
            { 
                AddToAudit("GlobalAllocationLockedUserId", globalAllocationLockedUserId, value);
                globalAllocationLockedUserId = value;
            }
        }

        public bool EventPublished
        {

            get
            {
                return eventPublished;
            }
            set
            {
                eventPublished = value;
            }
        }


        #endregion 


        public KxEventModules(KxBaseClass parent):base(parent)
        {}


        public KxEventModules(KxBaseClass parent, XmlNode node):base(parent)
        {
             LoadFromXml("", node);
        }

        public KxEventModules(KxBaseClass parent,int      eventmoduleid):base(parent)
        {
             base.dBIdentifier = eventmoduleid;
             Load();
        }


        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("eventmodules", "EventModules", TargetType.Table);

            result.AutoKeyField = new KxDataValue("EventModuleId",EventModuleId,KxValueType.Int);


            if ((actiontype == ActionType.Update) && (EventModuleId == 0))
            {
                result.Action = ActionType.Insert;
            }
            else
            {
                result.Action = actiontype;
            }

            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {
                   result.AddParameter("description", description, KxValueType.String, 100, false);
                   result.AddParameter("arrivalDate", arrivalDate, KxValueType.DateTime, true);
                   result.AddParameter("startDate", startDate, KxValueType.DateTime, true);
                   result.AddParameter("endDate", endDate, KxValueType.DateTime, true);
                   result.AddParameter("statusId", statusId, KxValueType.Int, false);
                   result.AddParameter("dayPeople", dayPeople, KxValueType.Short, false);
                   result.AddParameter("allAdded", allAdded, KxValueType.String, 1, false);
                   result.AddParameter("nightPeople", nightPeople, KxValueType.Short, false);
                   result.AddParameter("minCapacity", minCapacity, KxValueType.Int, true);
                   result.AddParameter("maxCapacity", maxCapacity, KxValueType.Int, true);
                   result.AddParameter("siteId", siteId, KxValueType.Int, false);
                   result.AddParameter("companyId", companyId, KxValueType.Int, true);
                   result.AddParameter("notes", notes, KxValueType.String, 2147483647, true);
                   result.AddParameter("creationDate", creationDate, KxValueType.DateTime, true);
                   result.AddParameter("creationUserId", creationUserId, KxValueType.Int, false);
                   result.AddParameter("modifiedDate", modifiedDate, KxValueType.DateTime, true);
                   result.AddParameter("modifiedUserId", modifiedUserId, KxValueType.Int, false);
                   result.AddParameter("purchaseOrderNumber", purchaseOrderNumber, KxValueType.String, 40, true);
                   result.AddParameter("vatexempt", vatexempt, KxValueType.Boolean, true);
                   result.AddParameter("accountManagerPersonId", accountManagerPersonId, KxValueType.Int, true);
                   result.AddParameter("coordinatorPersonId", coordinatorPersonId, KxValueType.Int, true);
                   result.AddParameter("exportToAccounts", exportToAccounts, KxValueType.Boolean, false);
                   result.AddParameter("salesTypeId", salesTypeId, KxValueType.Int, true);
                   result.AddParameter("billingStatusId", billingStatusId, KxValueType.Int, true);
                   result.AddParameter("debitCode", debitCode, KxValueType.String, 50, true);
                   result.AddParameter("exportEventPersonId", exportEventPersonId, KxValueType.Int, true);
                   result.AddParameter("eventAims", eventAims, KxValueType.String, 1000, true);
                   result.AddParameter("eventEthos", eventEthos, KxValueType.String, 1000, true);
                   result.AddParameter("matrixId", matrixId, KxValueType.Int, true);
                   result.AddParameter("amount", amount, KxValueType.Decimal, true);
                   result.AddParameter("hassleFactor", hassleFactor, KxValueType.Int, false);
                   result.AddParameter("resConfirmationTypeId", resConfirmationTypeId, KxValueType.Int, false);
                   result.AddParameter("defaultArrivalDate", defaultArrivalDate, KxValueType.DateTime, true);
                   result.AddParameter("defaultDepartureDate", defaultDepartureDate, KxValueType.DateTime, true);
                   result.AddParameter("externalId", externalId, KxValueType.String, 255, true);
                   result.AddParameter("areaId", areaId, KxValueType.Int, true);
                   result.AddParameter("enquirySourceId", enquirySourceId, KxValueType.Int, true);
                   result.AddParameter("paymentTermsId", paymentTermsId, KxValueType.Int, true);
                   result.AddParameter("exemptTaxes", exemptTaxes, KxValueType.String, 255, true);
                   result.AddParameter("copiedFromEventModuleId", copiedFromEventModuleId, KxValueType.Int, true);
                   result.AddParameter("defaultConfRoomPhoneAccessCode", defaultConfRoomPhoneAccessCode, KxValueType.String, 2, true);
                   result.AddParameter("globalAllocationLocked", globalAllocationLocked, KxValueType.DateTime, true);
                   result.AddParameter("globalAllocationLockedUserId", globalAllocationLockedUserId, KxValueType.Int, true);
                   result.AddParameter("eventPublished", eventPublished, KxValueType.Boolean, false);
            }
            return result;
        }


        public override  void LoadFromXml(string xml,XmlNode node)
        {
            dBIdentifier  = KxXmlUtils.GetNodeValue(node,"eventmoduleid",0);
            eventId  = KxXmlUtils.GetNodeValue(node,"eventid",0);
            description  = KxXmlUtils.GetNodeValue(node,"description","");
            arrivalDate  = KxXmlUtils.GetNodeValueDT(node,"arrivaldate",null);
            startDate  = KxXmlUtils.GetNodeValueDT(node,"startdate",null);
            endDate  = KxXmlUtils.GetNodeValueDT(node,"enddate",null);
            statusId  = KxXmlUtils.GetNodeValue(node,"statusid",0);
            dayPeople  = KxXmlUtils.GetNodeValue(node,"daypeople",(short)0);
            allAdded  = KxXmlUtils.GetNodeValue(node,"alladded","");
            nightPeople  = KxXmlUtils.GetNodeValue(node,"nightpeople",(short)0);
            minCapacity  = KxXmlUtils.GetNodeValue(node,"mincapacity",0);
            maxCapacity  = KxXmlUtils.GetNodeValue(node,"maxcapacity",0);
            siteId  = KxXmlUtils.GetNodeValue(node,"siteid",0);
            companyId  = KxXmlUtils.GetNodeValue(node,"companyid",0);
            notes  = KxXmlUtils.GetNodeValue(node,"notes","");
            creationDate  = KxXmlUtils.GetNodeValueDT(node,"creationdate",null);
            creationUserId  = KxXmlUtils.GetNodeValue(node,"creationuserid",0);
            modifiedDate  = KxXmlUtils.GetNodeValueDT(node,"modifieddate",null);
            modifiedUserId  = KxXmlUtils.GetNodeValue(node,"modifieduserid",0);
            purchaseOrderNumber  = KxXmlUtils.GetNodeValue(node,"purchaseordernumber","");
            vatexempt  = KxXmlUtils.GetNodeValue(node,"vatexempt",false);
            accountManagerPersonId  = KxXmlUtils.GetNodeValue(node,"accountmanagerpersonid",0);
            coordinatorPersonId  = KxXmlUtils.GetNodeValue(node,"coordinatorpersonid",0);
            exportToAccounts  = KxXmlUtils.GetNodeValue(node,"exporttoaccounts",false);
            salesTypeId  = KxXmlUtils.GetNodeValue(node,"salestypeid",0);
            billingStatusId  = KxXmlUtils.GetNodeValue(node,"billingstatusid",0);
            debitCode  = KxXmlUtils.GetNodeValue(node,"debitcode","");
            exportEventPersonId  = KxXmlUtils.GetNodeValue(node,"exporteventpersonid",0);
            eventAims  = KxXmlUtils.GetNodeValue(node,"eventaims","");
            eventEthos  = KxXmlUtils.GetNodeValue(node,"eventethos","");
            matrixId  = KxXmlUtils.GetNodeValue(node,"matrixid",0);
            amount  = KxXmlUtils.GetNodeValue(node,"amount",(decimal)0.00M);
            hassleFactor  = KxXmlUtils.GetNodeValue(node,"hasslefactor",0);
            resConfirmationTypeId  = KxXmlUtils.GetNodeValue(node,"resconfirmationtypeid",0);
            defaultArrivalDate  = KxXmlUtils.GetNodeValueDT(node,"defaultarrivaldate",null);
            defaultDepartureDate  = KxXmlUtils.GetNodeValueDT(node,"defaultdeparturedate",null);
            externalId  = KxXmlUtils.GetNodeValue(node,"externalid","");
            areaId  = KxXmlUtils.GetNodeValue(node,"areaid",0);
            enquirySourceId  = KxXmlUtils.GetNodeValue(node,"enquirysourceid",0);
            paymentTermsId  = KxXmlUtils.GetNodeValue(node,"paymenttermsid",0);
            exemptTaxes  = KxXmlUtils.GetNodeValue(node,"exempttaxes","");
            copiedFromEventModuleId  = KxXmlUtils.GetNodeValue(node,"copiedfromeventmoduleid",0);
            defaultConfRoomPhoneAccessCode  = KxXmlUtils.GetNodeValue(node,"defaultconfroomphoneaccesscode","");
            globalAllocationLocked  = KxXmlUtils.GetNodeValueDT(node,"globalallocationlocked",null);
            globalAllocationLockedUserId  = KxXmlUtils.GetNodeValue(node,"globalallocationlockeduserid",0);
            eventPublished = KxXmlUtils.GetNodeValue(node, "EventPublished", false); //2009R1
        }
    }
}
