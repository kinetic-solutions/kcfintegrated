using System;
using System.Collections.Generic;
using System.Text;
using Kx.Core.Base;
using Kx.Core.Data;
using System.Xml;
using Kx.Core.Utility;

namespace Kx.Objects.Common
{
    [Serializable]
    public class KxLookup : KxBaseClass
    {
        #region Attributes & Properties

        protected string lookupType;
        protected string lookupValue;
        protected string extraData;
        protected bool _inactive;
        protected int _listOrder;

        public Guid LookupId
        {
            get
            { return base.guidIdentifier; }
        }

        public string LookupType
        {
            get
            { return lookupType; }
            set
            {
                AddToAudit("LookupType", lookupType, value);
                lookupType = value;
            }
        }

        public LookupItemType ItemType
        {
            get
            {
                return (LookupItemType) Enum.Parse(typeof(LookupItemType), lookupType);
            }
            set
            {
                LookupType = value.ToString();
            }
        }

        public string LookupValue
        {
            get
            { return lookupValue; }
            set
            {
                AddToAudit("LookupValue", lookupValue, value);
                lookupValue = value;
            }
        }

        public string ExtraData
        {
            get
            { return extraData; }
            set
            {
                AddToAudit("ExtraData", extraData, value);
                extraData = value;
            }
        }

        public bool Inactive
        {
            get
            { return _inactive; }
            set
            {
                AddToAudit("Inactive", _inactive, value);
                _inactive = value;
            }
        }

        public int ListOrder
        {
            get { return _listOrder; }
            set
            {
              AddToAudit("ListOrder", _listOrder, value);
              _listOrder = value;
            }
        }


        #endregion

        public KxLookup(KxBaseClass parent, LookupItemType itemType) : base(parent)
        {
            ItemType = itemType;
        }

        public KxLookup(KxBaseClass parent, XmlNode node)
            : base(parent)
        {
            LoadFromXml("", node);
        }

        public KxLookup(KxBaseClass parent, Guid lookupid)
            : base(parent)
        {
            base.guidIdentifier = lookupid;
            Load();
        }

        public override string ToString()
        {
            return lookupValue;
        }

        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("kx2lookups", "Kx2Lookups", TargetType.Table);


            if ((actiontype == ActionType.Update) && (guidIdentifier == Guid.Empty))
            {
                if (changed)
                {
                    guidIdentifier = Guid.NewGuid();
                }
                result.Action = ActionType.Insert;
            }
            else
            {
                result.Action = actiontype;
            }

            result.AddKeyField("lookupId", guidIdentifier, KxValueType.Guid);

            //if 


            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {
                result.AddParameter("lookupType", lookupType, KxValueType.String, 255, false);
                result.AddParameter("lookupValue", lookupValue, KxValueType.String, 255, false);
                result.AddParameter("extraData", extraData, KxValueType.String, 2147483647, true);
                result.AddParameter("inactive", _inactive, KxValueType.Boolean, false);
                result.AddParameter("ListOrder", _listOrder, KxValueType.Int, true);
            }
            return result;
        }


        public override void LoadFromXml(string xml, XmlNode node)
        {
            guidIdentifier = KxXmlUtils.GetNodeValue(node, "lookupid", Guid.Empty);
            lookupType = KxXmlUtils.GetNodeValue(node, "lookuptype", "");
            lookupValue = KxXmlUtils.GetNodeValue(node, "lookupvalue", "");
            extraData = KxXmlUtils.GetNodeValue(node, "extradata", "");
            _inactive = KxXmlUtils.GetNodeValue(node, "inactive", false);
            _listOrder = KxXmlUtils.GetNodeValue(node, "listorder", 1);
        }
    }

    public enum LookupItemType
    {
        Unknown,
        AccommodationType,
        PropertyType,
        PropertyRoomType,
        PropertyStatus,
        Declaration,
        LettingType,
        LocalArea,
        LandlordType,
        InspectionState,
        AddressType
    }
}
