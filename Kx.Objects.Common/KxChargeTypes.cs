using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Exceptions;
using Kx.Core.Utility;

namespace Kx.Objects.Common
{
    public class KxChargeTypes : KxBaseClass
    {
        private List<KxChargeType> items = new List<KxChargeType>();

        public List<KxChargeType> Items
        {
            get
            { return items; }
        }

        public KxChargeType this[int index]
        {
            get
            { return items[index]; }
            set
            { items[index] = value; }
        }

        private string _chargeTypeLevel = string.Empty;

        public KxChargeTypes (KxBaseClass parent, string chargeTypeLevel)
            : base (parent)
        {
            _chargeTypeLevel = chargeTypeLevel;
            Load();
        }

        public KxChargeTypes (KxBaseClass parent)
            : base (parent)
        {
            Load();
        }

        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = null;

            if (actiontype == ActionType.Select)
            {
                result = new KxData("chargetypes", "ChargeTypes", TargetType.Table, ActionType.Select);

                if (_chargeTypeLevel != string.Empty)
                {
                    result.Parameters.Add(new KxDataValue("chargetypelevel", _chargeTypeLevel, KxValueType.String, 1));
                }
            }
            return result;
        }

        public override bool LoadFromXml(string xml, string xpath)
        {
            items.Clear();
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xml);

                XmlNodeList nodes = doc.SelectNodes(@"//results/" + xpath + "/datarow");

                foreach (XmlNode node in nodes)
                {
                    if (node != null)
                    {
                        KxChargeType item = new KxChargeType(this, node);
                        items.Add(item);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "LoadFromXml", ex.Message, xpath);
                LastError = ex.Message;
                return false;
            }
        }

        public KxChargeType GetChargeType(int id)
        {
            KxChargeType result = null;

            foreach (KxChargeType i in Items)
            {
                if (i.ChargeTypeId == id)
                {
                    result = i;
                    break;
                }
            }
            return result;
        }

    }
}

