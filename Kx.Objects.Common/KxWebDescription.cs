﻿using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;


namespace Kx.Objects.Common
{
    [Serializable]
    public class KxWebDescription : KxBaseClass
    {
        #region Attributes & Properties

        protected string   name;
        protected string   id1;
        protected string   id2;
        protected string   description;
        protected int      displayOrder;

        public int      WebDescriptionId
        {
            get
            { return base.dBIdentifier;}
        }

        public string   Name
        {
            get
            { return name;}
            set
            { 
                AddToAudit("Name", name, value);
                name = value;
            }
        }

        public string   Id1
        {
            get
            { return id1;}
            set
            { 
                AddToAudit("Id1", id1, value);
                id1 = value;
            }
        }

        public string   Id2
        {
            get
            { return id2;}
            set
            { 
                AddToAudit("Id2", id2, value);
                id2 = value;
            }
        }

        public string   Description
        {
            get
            { return description;}
            set
            { 
                AddToAudit("Description", description, value);
                description = value;
            }
        }

        public int      DisplayOrder
        {
            get
            { return displayOrder;}
            set
            { 
                AddToAudit("DisplayOrder", displayOrder, value);
                displayOrder = value;
            }
        }


        #endregion 


        public KxWebDescription(KxBaseClass parent):base(parent)
        {}


        public KxWebDescription(KxBaseClass parent, XmlNode node):base(parent)
        {
             LoadFromXml("", node);
        }

        public KxWebDescription(KxBaseClass parent, int webdescriptionid)
            : base(parent)
        {
             base.dBIdentifier = webdescriptionid;
             Load();
        }

        public KxWebDescription(KxBaseClass parent, string name, string id1)
            : base(parent)
        {
            Name = name;
            Id1 = id1;
            Load();
        }


        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("webdescriptions", "WebDescriptions", TargetType.Table);

            if (actiontype == ActionType.Select)
            {
                result.AddKeyField("Name", name, KxValueType.String);
                result.AddKeyField("id1", id1, KxValueType.String);
            }
            else
            {
                result.AutoKeyField = new KxDataValue("WebDescriptionId", WebDescriptionId, KxValueType.Int);
            }

            if ((actiontype == ActionType.Update) && (WebDescriptionId == 0))
            {
                result.Action = ActionType.Insert;
            }
            else
            {
                result.Action = actiontype;
            }

            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {
                   result.AddParameter("id1", id1, KxValueType.String, 50, true);
                   result.AddParameter("id2", id2, KxValueType.String, 50, true);
                   result.AddParameter("Name", name, KxValueType.String, 255, true);
                   result.AddParameter("description", description, KxValueType.String, 2147483647, false);
                   result.AddParameter("displayOrder", displayOrder, KxValueType.Int, true);
            }
            return result;
        }


        public override  void LoadFromXml(string xml,XmlNode node)
        {
            dBIdentifier  = KxXmlUtils.GetNodeValue(node,"webdescriptionid",0);
            name  = KxXmlUtils.GetNodeValue(node,"name","");
            id1  = KxXmlUtils.GetNodeValue(node,"id1","");
            id2  = KxXmlUtils.GetNodeValue(node,"id2","");
            description  = KxXmlUtils.GetNodeValue(node,"description","");
            displayOrder  = KxXmlUtils.GetNodeValue(node,"displayorder",0);
        }
    }
}
