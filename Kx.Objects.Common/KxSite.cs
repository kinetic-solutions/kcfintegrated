using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;

namespace Kx.Objects.Common
{
    [Serializable]
    public class KxSite : KxBaseClass
    {
        #region Attributes & Properties

        protected string   name;
        protected string   siteCode;
        protected bool     offSite;
        protected string   salesOrgCode;
        protected string   salesAreaCode;
        protected int      cateringCapacity;
        protected int      delegateCapacity;
        protected string   externalReference1;
        protected int      cateringNominalId;
        protected int      cateringCostTypeId;
        protected int      residentialCostTypeId;
        protected int      creditNominalId;
        protected int      invoiceNominalId;
        protected bool     pmsenabled;
        protected int      defCatConfRoomId;
        protected string   cateringDeliveryZones;
        protected int      invoiceReportId;
        protected bool     wakeupCalls;
        protected bool     eftprocessing;
        protected string   availabilityScreen;
        protected string   externalCode1;
        protected string   externalCode2;
        protected string   externalCode3;
        protected int      bookingContractId;
        protected int      distribAdHocUserId;
        protected int      userGroupId;
        protected int      depositChargeTypeId;

        public int      SiteId
        {
            get
            { return base.dBIdentifier;}
        }

        public string   Name
        {
            get
            { return name;}
            set
            { 
                AddToAudit("Name", name, value);
                name = value;
            }
        }

        public string   SiteCode
        {
            get
            { return siteCode;}
            set
            { 
                AddToAudit("SiteCode", siteCode, value);
                siteCode = value;
            }
        }

        public bool     OffSite
        {
            get
            { return offSite;}
            set
            { 
                AddToAudit("OffSite", offSite, value);
                offSite = value;
            }
        }

        public string   SalesOrgCode
        {
            get
            { return salesOrgCode;}
            set
            { 
                AddToAudit("SalesOrgCode", salesOrgCode, value);
                salesOrgCode = value;
            }
        }

        public string   SalesAreaCode
        {
            get
            { return salesAreaCode;}
            set
            { 
                AddToAudit("SalesAreaCode", salesAreaCode, value);
                salesAreaCode = value;
            }
        }

        public int      CateringCapacity
        {
            get
            { return cateringCapacity;}
            set
            { 
                AddToAudit("CateringCapacity", cateringCapacity, value);
                cateringCapacity = value;
            }
        }

        public int      DelegateCapacity
        {
            get
            { return delegateCapacity;}
            set
            { 
                AddToAudit("DelegateCapacity", delegateCapacity, value);
                delegateCapacity = value;
            }
        }

        public string   ExternalReference1
        {
            get
            { return externalReference1;}
            set
            { 
                AddToAudit("ExternalReference1", externalReference1, value);
                externalReference1 = value;
            }
        }

        public int      CateringNominalId
        {
            get
            { return cateringNominalId;}
            set
            { 
                AddToAudit("CateringNominalId", cateringNominalId, value);
                cateringNominalId = value;
            }
        }

        public int      CateringCostTypeId
        {
            get
            { return cateringCostTypeId;}
            set
            { 
                AddToAudit("CateringCostTypeId", cateringCostTypeId, value);
                cateringCostTypeId = value;
            }
        }

        public int      ResidentialCostTypeId
        {
            get
            { return residentialCostTypeId;}
            set
            { 
                AddToAudit("ResidentialCostTypeId", residentialCostTypeId, value);
                residentialCostTypeId = value;
            }
        }

        public int      CreditNominalId
        {
            get
            { return creditNominalId;}
            set
            { 
                AddToAudit("CreditNominalId", creditNominalId, value);
                creditNominalId = value;
            }
        }

        public int      InvoiceNominalId
        {
            get
            { return invoiceNominalId;}
            set
            { 
                AddToAudit("InvoiceNominalId", invoiceNominalId, value);
                invoiceNominalId = value;
            }
        }

        public bool     Pmsenabled
        {
            get
            { return pmsenabled;}
            set
            { 
                AddToAudit("Pmsenabled", pmsenabled, value);
                pmsenabled = value;
            }
        }

        public int      DefCatConfRoomId
        {
            get
            { return defCatConfRoomId;}
            set
            { 
                AddToAudit("DefCatConfRoomId", defCatConfRoomId, value);
                defCatConfRoomId = value;
            }
        }

        public string   CateringDeliveryZones
        {
            get
            { return cateringDeliveryZones;}
            set
            { 
                AddToAudit("CateringDeliveryZones", cateringDeliveryZones, value);
                cateringDeliveryZones = value;
            }
        }

        public int      InvoiceReportId
        {
            get
            { return invoiceReportId;}
            set
            { 
                AddToAudit("InvoiceReportId", invoiceReportId, value);
                invoiceReportId = value;
            }
        }

        public bool     WakeupCalls
        {
            get
            { return wakeupCalls;}
            set
            { 
                AddToAudit("WakeupCalls", wakeupCalls, value);
                wakeupCalls = value;
            }
        }

        public bool     Eftprocessing
        {
            get
            { return eftprocessing;}
            set
            { 
                AddToAudit("Eftprocessing", eftprocessing, value);
                eftprocessing = value;
            }
        }

        public string   AvailabilityScreen
        {
            get
            { return availabilityScreen;}
            set
            { 
                AddToAudit("AvailabilityScreen", availabilityScreen, value);
                availabilityScreen = value;
            }
        }

        public string   ExternalCode1
        {
            get
            { return externalCode1;}
            set
            { 
                AddToAudit("ExternalCode1", externalCode1, value);
                externalCode1 = value;
            }
        }

        public string   ExternalCode2
        {
            get
            { return externalCode2;}
            set
            { 
                AddToAudit("ExternalCode2", externalCode2, value);
                externalCode2 = value;
            }
        }

        public string   ExternalCode3
        {
            get
            { return externalCode3;}
            set
            { 
                AddToAudit("ExternalCode3", externalCode3, value);
                externalCode3 = value;
            }
        }

        public int      BookingContractId
        {
            get
            { return bookingContractId;}
            set
            { 
                AddToAudit("BookingContractId", bookingContractId, value);
                bookingContractId = value;
            }
        }

        public int      DistribAdHocUserId
        {
            get
            { return distribAdHocUserId;}
            set
            { 
                AddToAudit("DistribAdHocUserId", distribAdHocUserId, value);
                distribAdHocUserId = value;
            }
        }

        public int      UserGroupId
        {
            get
            { return userGroupId;}
            set
            { 
                AddToAudit("UserGroupId", userGroupId, value);
                userGroupId = value;
            }
        }

        public int      DepositChargeTypeId
        {
            get
            { return depositChargeTypeId;}
            set
            { 
                AddToAudit("DepositChargeTypeId", depositChargeTypeId, value);
                depositChargeTypeId = value;
            }
        }

        public string Address
        {
            get
            {
               return cateringDeliveryZones;
            }
        }


        #endregion 

        public KxSite(KxBaseClass parent,int      siteid):base(parent)
        {
             base.dBIdentifier = siteid;
             Load();
        }

        public KxSite(KxBaseClass parent, XmlNode node) : base(parent)
        {
            LoadFromXml("",node);
        }

        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("sites", "Sites", TargetType.Table);

            result.AutoKeyField = new KxDataValue("SiteId",SiteId,KxValueType.Int);


            if ((actiontype == ActionType.Update) && (SiteId == 0))
            {
                result.Action = ActionType.Insert;
            }
            else
            {
                result.Action = actiontype;
            }

            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {
                   result.AddParameter("Name", name, KxValueType.String);
                   result.AddParameter("siteCode", siteCode, KxValueType.String);
                   result.AddParameter("offSite", offSite, KxValueType.Boolean);
                   result.AddParameter("salesOrgCode", salesOrgCode, KxValueType.String);
                   result.AddParameter("salesAreaCode", salesAreaCode, KxValueType.String);
                   result.AddParameter("cateringCapacity", cateringCapacity, KxValueType.Int);
                   result.AddParameter("delegateCapacity", delegateCapacity, KxValueType.Int);
                   result.AddParameter("externalReference1", externalReference1, KxValueType.String);
                   result.AddParameter("cateringNominalId", cateringNominalId, KxValueType.Int);
                   result.AddParameter("cateringCostTypeId", cateringCostTypeId, KxValueType.Int);
                   result.AddParameter("residentialCostTypeId", residentialCostTypeId, KxValueType.Int);
                   result.AddParameter("creditNominalId", creditNominalId, KxValueType.Int);
                   result.AddParameter("invoiceNominalId", invoiceNominalId, KxValueType.Int);
                   result.AddParameter("pmsenabled", pmsenabled, KxValueType.Boolean);
                   result.AddParameter("defCatConfRoomId", defCatConfRoomId, KxValueType.Int);
                   result.AddParameter("cateringDeliveryZones", cateringDeliveryZones, KxValueType.String);
                   result.AddParameter("invoiceReportId", invoiceReportId, KxValueType.Int);
                   result.AddParameter("wakeupCalls", wakeupCalls, KxValueType.Boolean);
                   result.AddParameter("eftprocessing", eftprocessing, KxValueType.Boolean);
                   result.AddParameter("availabilityScreen", availabilityScreen, KxValueType.String);
                   result.AddParameter("externalCode1", externalCode1, KxValueType.String);
                   result.AddParameter("externalCode2", externalCode2, KxValueType.String);
                   result.AddParameter("externalCode3", externalCode3, KxValueType.String);
                   result.AddParameter("bookingContractId", bookingContractId, KxValueType.Int);
                   result.AddParameter("distribAdHocUserId", distribAdHocUserId, KxValueType.Int);
                   result.AddParameter("userGroupId", userGroupId, KxValueType.Int);
                   result.AddParameter("depositChargeTypeId", depositChargeTypeId, KxValueType.Int);
            }
            return result;
        }


        public override  void LoadFromXml(string xml,XmlNode node)
        {
            dBIdentifier  = KxXmlUtils.GetNodeValue(node,"siteid",0);
            name  = KxXmlUtils.GetNodeValue(node,"name","");
            siteCode  = KxXmlUtils.GetNodeValue(node,"sitecode","");
            offSite  = KxXmlUtils.GetNodeValue(node,"offsite",false);
            salesOrgCode  = KxXmlUtils.GetNodeValue(node,"salesorgcode","");
            salesAreaCode  = KxXmlUtils.GetNodeValue(node,"salesareacode","");
            cateringCapacity  = KxXmlUtils.GetNodeValue(node,"cateringcapacity",0);
            delegateCapacity  = KxXmlUtils.GetNodeValue(node,"delegatecapacity",0);
            externalReference1  = KxXmlUtils.GetNodeValue(node,"externalreference1","");
            cateringNominalId  = KxXmlUtils.GetNodeValue(node,"cateringnominalid",0);
            cateringCostTypeId  = KxXmlUtils.GetNodeValue(node,"cateringcosttypeid",0);
            residentialCostTypeId  = KxXmlUtils.GetNodeValue(node,"residentialcosttypeid",0);
            creditNominalId  = KxXmlUtils.GetNodeValue(node,"creditnominalid",0);
            invoiceNominalId  = KxXmlUtils.GetNodeValue(node,"invoicenominalid",0);
            pmsenabled  = KxXmlUtils.GetNodeValue(node,"pmsenabled",false);
            defCatConfRoomId  = KxXmlUtils.GetNodeValue(node,"defcatconfroomid",0);
            cateringDeliveryZones  = KxXmlUtils.GetNodeValue(node,"cateringdeliveryzones","");
            invoiceReportId  = KxXmlUtils.GetNodeValue(node,"invoicereportid",0);
            wakeupCalls  = KxXmlUtils.GetNodeValue(node,"wakeupcalls",false);
            eftprocessing  = KxXmlUtils.GetNodeValue(node,"eftprocessing",false);
            availabilityScreen  = KxXmlUtils.GetNodeValue(node,"availabilityscreen","");
            externalCode1  = KxXmlUtils.GetNodeValue(node,"externalcode1","");
            externalCode2  = KxXmlUtils.GetNodeValue(node,"externalcode2","");
            externalCode3  = KxXmlUtils.GetNodeValue(node,"externalcode3","");
            bookingContractId  = KxXmlUtils.GetNodeValue(node,"bookingcontractid",0);
            distribAdHocUserId  = KxXmlUtils.GetNodeValue(node,"distribadhocuserid",0);
            userGroupId  = KxXmlUtils.GetNodeValue(node,"usergroupid",0);
            depositChargeTypeId  = KxXmlUtils.GetNodeValue(node,"depositchargetypeid",0);
        }
    }
}

