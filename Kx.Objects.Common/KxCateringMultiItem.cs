﻿using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;

namespace Kx.Objects.Common
{
    [Serializable]
    public class KxCateringMultiItem : KxBaseClass
    {
        #region Attributes & Properties

        protected string   description;
        protected int      siteId;
        protected int      sortOrder;
        protected bool     inactive;

        public int      MultiItemId
        {
            get
            { return base.dBIdentifier;}
        }

        public string   Description
        {
            get
            { return description;}
            set
            { 
                AddToAudit("Description", description, value);
                description = value;
            }
        }

        public int      SiteId
        {
            get
            { return siteId;}
            set
            { 
                AddToAudit("SiteId", siteId, value);
                siteId = value;
            }
        }

        public int      SortOrder
        {
            get
            { return sortOrder;}
            set
            { 
                AddToAudit("SortOrder", sortOrder, value);
                sortOrder = value;
            }
        }

        public bool     Inactive
        {
            get
            { return inactive;}
            set
            { 
                AddToAudit("Inactive", inactive, value);
                inactive = value;
            }
        }


        #endregion 


        public KxCateringMultiItem(KxBaseClass parent):base(parent)
        {}


        public KxCateringMultiItem(KxBaseClass parent, XmlNode node):base(parent)
        {
             LoadFromXml("", node);
        }

        public KxCateringMultiItem(KxBaseClass parent,int      multiitemid):base(parent)
        {
             base.dBIdentifier = multiitemid;
             Load();
        }


        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("cateringmultiitems", "CateringMultiItems", TargetType.Table);

            result.AutoKeyField = new KxDataValue("MultiItemId",MultiItemId,KxValueType.Int);


            if ((actiontype == ActionType.Update) && (MultiItemId == 0))
            {
                result.Action = ActionType.Insert;
            }
            else
            {
                result.Action = actiontype;
            }

            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {
                   result.AddParameter("siteId", siteId, KxValueType.Int, true);
                   result.AddParameter("sortOrder", sortOrder, KxValueType.Int, true);
                   result.AddParameter("inactive", inactive, KxValueType.Boolean, false);
            }
            return result;
        }


        public override  void LoadFromXml(string xml,XmlNode node)
        {
            dBIdentifier  = KxXmlUtils.GetNodeValue(node,"multiitemid",0);
            description  = KxXmlUtils.GetNodeValue(node,"description","");
            siteId  = KxXmlUtils.GetNodeValue(node,"siteid",0);
            sortOrder  = KxXmlUtils.GetNodeValue(node,"sortorder",0);
            inactive  = KxXmlUtils.GetNodeValue(node,"inactive",false);
        }
    }
}

