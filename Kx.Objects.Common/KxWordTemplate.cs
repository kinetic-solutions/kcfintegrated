using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;

namespace Kx.Objects.Common
{
    [Serializable]
    public class KxWordTemplate : KxBaseClass
    {
        #region Attributes & Properties

        protected string   description;
        protected string    template;
        protected string   comments;
        protected int      licenceTypeId;
        protected int      eventBusinessTypeId;
        protected string   subject;
        protected string   body;
        protected int      studentNightCount;
        protected bool     inactive;
        protected int      siteId;
        protected byte[] templateArray;

        public int      WordTemplateId
        {
            get
            { return base.dBIdentifier;}
        }

        public string   Description
        {
            get
            { return description;}
            set
            { 
                AddToAudit("Description", description, value);
                description = value;
            }
        }

        public string    Template
        {
            get
            { return template;}
            set
            { 
                AddToAudit("Template", template, value);
                template = value;
            }
        }

        public byte[] TemplateArray
        {
            get
            { return templateArray; }
            set
            {
                templateArray = value;
            }
        }

        public string   Comments
        {
            get
            { return comments;}
            set
            { 
                AddToAudit("Comments", comments, value);
                comments = value;
            }
        }

        public int      LicenceTypeId
        {
            get
            { return licenceTypeId;}
            set
            { 
                AddToAudit("LicenceTypeId", licenceTypeId, value);
                licenceTypeId = value;
            }
        }

        public int      EventBusinessTypeId
        {
            get
            { return eventBusinessTypeId;}
            set
            { 
                AddToAudit("EventBusinessTypeId", eventBusinessTypeId, value);
                eventBusinessTypeId = value;
            }
        }

        public string   Subject
        {
            get
            { return subject;}
            set
            { 
                AddToAudit("Subject", subject, value);
                subject = value;
            }
        }

        public string   Body
        {
            get
            { return body;}
            set
            { 
                AddToAudit("Body", body, value);
                body = value;
            }
        }

        public int      StudentNightCount
        {
            get
            { return studentNightCount;}
            set
            { 
                AddToAudit("StudentNightCount", studentNightCount, value);
                studentNightCount = value;
            }
        }

        public bool     Inactive
        {
            get
            { return inactive;}
            set
            { 
                AddToAudit("Inactive", inactive, value);
                inactive = value;
            }
        }

        public int      SiteId
        {
            get
            { return siteId;}
            set
            { 
                AddToAudit("SiteId", siteId, value);
                siteId = value;
            }
        }


        #endregion 

        public KxWordTemplate()
            : base()
        {
            Load();
        }
        
        public KxWordTemplate(int wordtemplateid)
            : base()
        {
             base.dBIdentifier = wordtemplateid;
             Load();
        }

        public KxWordTemplate(KxBaseClass parent, XmlNode node)
            : base(parent)
        {
             LoadFromXml("", node);
        }

        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("wordtemplates", "WordTemplates", TargetType.Table);

            result.AutoKeyField = new KxDataValue("WordTemplateId",WordTemplateId,KxValueType.Int);


            if ((actiontype == ActionType.Update) && (WordTemplateId == 0))
            {
                result.Action = ActionType.Insert;
            }
            else
            {
                result.Action = actiontype;
            }

            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {
                ASCIIEncoding encoding = new ASCIIEncoding();
                byte[] bArray = encoding.GetBytes(template);

                result.AddParameter("description", description, KxValueType.String);
                result.AddParameter("template", bArray, KxValueType.Image);
                result.AddParameter("comments", comments, KxValueType.String);
                result.AddParameter("licenceTypeId", licenceTypeId, KxValueType.Int);
                result.AddParameter("eventBusinessTypeId", eventBusinessTypeId, KxValueType.Int);
                result.AddParameter("subject", subject, KxValueType.String);
                result.AddParameter("body", body, KxValueType.String);
                result.AddParameter("studentNightCount", studentNightCount, KxValueType.Int);
                result.AddParameter("inactive", inactive, KxValueType.Boolean);
                result.AddParameter("siteId", siteId, KxValueType.Int);
            }
            return result;
        }


        public override void LoadFromXml(string xml,XmlNode node)
        {
            dBIdentifier = KxXmlUtils.GetNodeValue(node, "wordtemplateid", 0);
            description  = KxXmlUtils.GetNodeValue(node,"description","");
            templateArray = KxXmlUtils.GetNodeValueBA(node, "template", null);
            template  = KxXmlUtils.GetNodeValue(node,"template","");
            comments  = KxXmlUtils.GetNodeValue(node,"comments","");
            licenceTypeId  = KxXmlUtils.GetNodeValue(node,"licencetypeid",0);
            eventBusinessTypeId  = KxXmlUtils.GetNodeValue(node,"eventbusinesstypeid",0);
            subject  = KxXmlUtils.GetNodeValue(node,"subject","");
            body  = KxXmlUtils.GetNodeValue(node,"body","");
            studentNightCount  = KxXmlUtils.GetNodeValue(node,"studentnightcount",0);
            inactive  = KxXmlUtils.GetNodeValue(node,"inactive",false);
            siteId  = KxXmlUtils.GetNodeValue(node,"siteid",0);
        }
    }
}

