using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;

namespace Kx.Objects.Common
{
    [Serializable]
    public class KxEthnicity : KxBaseClass
    {
        #region Attributes & Properties

        protected string   description;

        public int      EthnicityId
        {
            get
            { return base.dBIdentifier;}
        }

        public string   Description
        {
            get
            { return description;}
            set
            { 
                AddToAudit("Description", description, value);
                description = value;
            }
        }


        #endregion 
        
        public KxEthnicity(KxBaseClass parent):base(parent)
        {}


        public KxEthnicity(KxBaseClass parent, XmlNode node):base(parent)
        {
             LoadFromXml("", node);
        }

        public KxEthnicity(KxBaseClass parent, int ethnicityid)
            : base(parent)
        {
             base.dBIdentifier = ethnicityid;
             Load();
        }


        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("ethnicities", "Ethnicities", TargetType.Table);

            result.AutoKeyField = new KxDataValue("EthnicityId",EthnicityId,KxValueType.Int);


            if ((actiontype == ActionType.Update) && (EthnicityId == 0))
            {
                result.Action = ActionType.Insert;
            }
            else
            {
                result.Action = actiontype;
            }

            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {
            }
            return result;
        }


        public override  void LoadFromXml(string xml,XmlNode node)
        {
            dBIdentifier  = KxXmlUtils.GetNodeValue(node,"ethnicityid",0);
            description  = KxXmlUtils.GetNodeValue(node,"description","");
        }
    }
}

