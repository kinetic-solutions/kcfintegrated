﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Kx.Objects.Common
{
    public class KxWordTemplateInfo
    {
        private KxWordTemplate _wordTemplate;

        public int _id;
        public string _description;

        public KxWordTemplateInfo(int TemplateID, string Description)
        {
            _id = TemplateID;
            _description = Description;
        }

        /// <summary>
        /// 
        /// </summary>
        public KxWordTemplate WordTemplate
        {
            get
            {
                KxWordTemplate result = null;

                if (_wordTemplate == null)
                    _wordTemplate = new KxWordTemplate(_id);
                
                result = _wordTemplate;

                return result;
            }
            set { _wordTemplate = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public int ID
        {
            get { return _id; }
            set { _id = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Description
        {
            get { return _description; }
            set { _description = value ; }
        }

        /// <summary>
        /// 
        /// </summary>
        public override string ToString()
        {
            return _description;
        }
    }
}
