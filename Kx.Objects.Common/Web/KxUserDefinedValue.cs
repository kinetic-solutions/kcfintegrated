using System;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;

namespace Kx.Objects.Common.Web
{
	[Serializable]
	public class KxUserDefinedValue : KxBaseClass
	{
		#region Attributes & Properties
		protected Guid applicationGuid;
		protected int customFieldId;
		protected string entityType = "";
		protected string fieldValue = "";
		protected KxUserDefinedField uDF;

	    private static int _fieldValueMaxLength = 0;
	    private int FieldValueMaxLength
	    {
	        get
	        {
	            if (_fieldValueMaxLength == 0)
	            {
	                GetFieldValueMaxLength();
	            }

	            return _fieldValueMaxLength;
	        }
	    }

		public string FieldType
		{
			get
			{ try { return UDF.FieldType; } catch { return ""; }}
			
		}
		public string FieldName
		{
			get
			{ try {return UDF.FieldName; } catch { return ""; } }

		}
		public bool IsCompanies
		{
			get
			{ try {return UDF.IsCompanies; } catch { return false; } }
		}
		public bool IsPeople
		{
			get
			{ try { return UDF.IsPeople; } catch { return false; } }
		}
		public bool IsStudents
		{
			get
			{ try {return UDF.IsStudents; } catch { return false; } }
		}
		public bool IsEvents
		{
			get
			{ try { return UDF.IsEvents; } catch { return false; } }
		}
		public string Picklist
		{
			get
			{ try {return UDF.Picklist; } catch { return ""; } }
		}
		public string DefaultValue
		{
			get
			{ try {return UDF.DefaultValue; } catch { return ""; } }
		}
		public int ValidationRequired
		{
			get
			{ try {return UDF.ValidationRequired; } catch { return 0; } }
		}
		public string ValidationScript
		{
			get
			{ try { return UDF.ValidationScript; } catch { return ""; } }
		}
		public bool IsStudentApplications
		{
			get
			{ try { return UDF.IsStudentApplications; } catch { return false; } }
		}
		public bool IsStudentCourses
		{
			get
			{ try { return UDF.IsStudentCourses; } catch { return false; } }
		}
		public string[] SelectionList
		{
			get
			{
				try { return UDF.SelectionList; } catch { return null; }
			}
		}
		public int CustomDataId
		{
			get
			{ return dBIdentifier;}
		}
		public Guid	 ApplicationGuid
		{
			get
			{ return applicationGuid;}
			set
			{ 
				AddToAudit("ApplicationGuid", applicationGuid, value);
				applicationGuid = value;
			}
		}
		public int CustomFieldId
		{
			get
			{ return customFieldId;}
			set
			{ 
				AddToAudit("CustomFieldId", customFieldId, value);
				customFieldId = value;
			}
		}
		public string EntityType
		{
			get
			{ return entityType;}
			set
			{ 
				AddToAudit("EntityType", entityType, value);
				entityType = value;
			}
		}
		public KxUserDefinedField UDF
		{
			get
			{
				if (uDF == null)
				{
					if (customFieldId > 0)
					{
						uDF = KxUDFProvider.GetUdfById(customFieldId);
					}
				}
				return uDF; 
			}
			set
			{ uDF = value; }
		}
		public string FieldValue
		{
			get
			{ return fieldValue;}
			set
			{ 
				AddToAudit("FieldValue", fieldValue, value);
				fieldValue = value;
			}
		}

		#endregion 

		public KxUserDefinedValue(KxBaseClass parent): base(parent)
		{}

		public KxUserDefinedValue(KxBaseClass parent, int customdataid) : base(parent)
		{
			 dBIdentifier = customdataid;
			 Load();
		}

		public KxUserDefinedValue(KxBaseClass parent, XmlNode node) : base(parent)
		{
			LoadFromXml("",node);
		}

		public override KxData ConfigureData(ActionType actiontype)
		{
			KxData result = new KxData("wsaudf", "WebStudentApplicationUDFValues", TargetType.Table);

			result.AutoKeyField = new KxDataValue("CustomDataId",CustomDataId,KxValueType.Int);
			
			if ((actiontype == ActionType.Update) && (CustomDataId == 0))
			{
				result.Action = ActionType.Insert;
			}
			else
			{
				result.Action = actiontype;
			}

			if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
			{
				ConfigureEntityTypes();

				if (applicationGuid == Guid.Empty && parent != null)
				{
					applicationGuid = parent.GuidIdentifier;
				}
				result.AddParameter("applicationGuid", applicationGuid, KxValueType.Guid, true);
				result.AddParameter("customFieldId", customFieldId, KxValueType.Int, true);
				result.AddParameter("entityType", entityType, KxValueType.String, 50, true);
				result.AddParameter("fieldValue", fieldValue, KxValueType.String, FieldValueMaxLength, true);
			}

			if (applicationGuid == Guid.Empty)
			{
				result = null;
			}

			return result;
		}

		private void ConfigureEntityTypes()
		{
			if (UDF != null)
			{
				if (IsCompanies)
				{
					entityType = "Companies";
				}
				else if (IsPeople)
				{
					entityType = "People";
				}
				else if (IsStudents)
				{
					entityType = "ST2Student";
				}
				else if (IsEvents)
				{
					entityType = "EventModules";
				}
				else if (IsStudentApplications)
				{
					entityType = "ST2StudentApplications";
				}
				else if (IsStudentCourses)
				{
					entityType = "ST2StudentCourses";
				}
			}
		}

		public override void LoadFromXml(string xml,XmlNode node)
		{
			uDF = null;

			dBIdentifier = KxXmlUtils.GetNodeValue(node, "customdataid", 0);
			applicationGuid = KxXmlUtils.GetNodeValue(node, "applicationguid", Guid.Empty);
			customFieldId = KxXmlUtils.GetNodeValue(node, "customfieldid", 0);
			entityType = KxXmlUtils.GetNodeValue(node, "entitytype", "");
			fieldValue = KxXmlUtils.GetNodeValue(node, "fieldvalue", "");
		}

	    private void GetFieldValueMaxLength()
	    {
	        int result = 255;
	        KxData data = new KxData("fieldvalue", "SELECT max_length FROM sys.columns WHERE object_id = OBJECT_ID(N'dbo.WebStudentApplicationUDFValues') AND name = 'FieldValue'", TargetType.Static);
	        try
	        {
	            result = (int)data.ExecuteScalar();
	        }
	        catch
	        {
	            result = 255;
	        }

	        _fieldValueMaxLength = result > 0 ? result : 1000;
	    }

    }
}

