﻿using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;

namespace Kx.Objects.Common.Web
{
    [Serializable]
    public class KxUdfMapping : KxBaseClass
    {
        #region Attributes & Properties

        protected int      webUdf;
        protected int      germUdf;

        public int      WebUdf
        {
            get
            { return webUdf;}
            set
            { 
                AddToAudit("WebUdf", webUdf, value);
                webUdf = value;
            }
        }

        public int      GermUdf
        {
            get
            { return germUdf;}
            set
            { 
                AddToAudit("GermUdf", germUdf, value);
                germUdf = value;
            }
        }


        #endregion 

        public KxUdfMapping(KxBaseClass parent)
            : base(parent)
        {
        }

        public KxUdfMapping(KxBaseClass parent, XmlNode node)
            : this(parent)
        {
            LoadFromXml("", node);
        }


        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("udfmapping", "UDFMapping", TargetType.Table);

            result.AddKeyField("webudf", webUdf,KxValueType.Int);


          
            return result;
        }


        public override  void LoadFromXml(string xml,XmlNode node)
        {
            webUdf  = KxXmlUtils.GetNodeValue(node,"webudf",0);
            germUdf  = KxXmlUtils.GetNodeValue(node,"germudf",0);
        }
    }
}

