using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using System.Threading;
using System.Globalization;

namespace Kx.Objects.Common.Web
{
    public enum LoadType {Email,Guid,User,UserID,Student,NewUser};

	/// <remarks>
	/// <para>
	/// Revision History
	/// Author              Date        Changes
	/// Adam Stewart        06/02/2008  Removed ExtraInfo as its not used
        /// Paul McMonnies      15/03/2012  Added dynamicValues 4-8 and E-H
	/// </para>
	/// </remarks>
    [Serializable]
    public class KxWebUser : KxBaseClass
    {
        #region Attributes & Properties
        protected bool newRow = false;
        protected bool loaded = false;

	    protected bool autoCreatePerson = false;

        protected LoadType loadType = LoadType.Guid;

        protected string userName = "";
        protected string password = "";
        protected string foreName = "";
        protected string surName = "";
        protected string salt = "";
        protected DateTime ?creationDate = DateTime.Now;
        protected int      active = 0;
        protected string emailAddress = "";
        protected DateTime ?emailConfirmed;
        protected int      kxStudentId;
        protected int      userId;
        protected int personId = 0;
        protected DateTime ?lockedAccountTime;
        protected int retryCount;
        protected bool loggedIn = false;
        protected string lastPage = "";
        protected string extraText1 = "";
        protected string extraText2 = "";
        protected string extraText3 = "";
        protected int extraInt1 = 0;
        protected int extraInt2 = 0;
        protected int extraInt3 = 0;
        protected string secretQuestion = "";
        protected string secretAnswer = "";

        public bool NewRow
        {
            get
            {
                return newRow;
            }
            set
            {
                newRow = value;
            }
        }

        /// <summary>
        /// Checks whether a WebUser is actually a (long term) WebUser saved in the WebUsers table
        /// </summary>
        public bool IsInWebUsers
        {
            get
            {
                bool inWebUsers = false;
                KxWebUser checkDb = new KxWebUser();
                if (UserGuid != Guid.Empty)
                {
                    checkDb.LoadGuid(UserGuid);
                    inWebUsers = (checkDb.UserGuid == UserGuid);
                }
                return inWebUsers;
            }
        }

        public bool Loaded
        {
            get
            {
                return loaded;
            }
            set
            {
                loaded = value;
            }
        }

        protected KxPerson person = null;

	    public string DynamicValue1 { get; set; }
	    public string DynamicValue2 { get; set; }
	    public string DynamicValue3 { get; set; }
	    public string DynamicValue4 { get; set; }
	    public string DynamicValue5 { get; set; }
	    public string DynamicValue6 { get; set; }
	    public string DynamicValue7 { get; set; }
	    public string DynamicValue8 { get; set; }
	    public string DynamicValue9 { get; set; }
	    public string DynamicValue0 { get; set; }
	    public string DynamicValueA { get; set; }
	    public string DynamicValueB { get; set; }
	    public string DynamicValueC { get; set; }
	    public string DynamicValueD { get; set; }
	    public string DynamicValueE { get; set; }
	    public string DynamicValueF { get; set; }
	    public string DynamicValueG { get; set; }
	    public string DynamicValueH { get; set; }
	    public string DynamicValueI { get; set; }
	    public string DynamicValueJ { get; set; }
	    public string DynamicValueK { get; set; }
	    public string DynamicValueL { get; set; }
	    public string DynamicValueM { get; set; }
	    public string DynamicValueN { get; set; }

	    public bool AutoCreatePerson
        {
            get
            {
                return autoCreatePerson;
            }
            set
            {
                autoCreatePerson = value;
            }
        }

        public string LastPage
        {
            get
            { return lastPage; }
            set
            { lastPage = value; }
        }

        public Guid UserGuid
        {
            get
            { return guidIdentifier; }
            set
            {
                if (guidIdentifier != value)
                {
                    Changed = true;
                    newRow = true;
                }
                guidIdentifier = value;
            }       
        }
        public string   UserName
        {
            get
            { return userName;}
            set
            { 
                AddToAudit("UserName", userName, value);
                userName = value;
            }
        }
        public string   Password
        {
            get
            { return password;}
            set
            {
                Changed = true;
                password = value;
            }
        }
        public string ForeName
        {
            get
            {
                return foreName;
            }
            set
            {
                AddToAudit("ForeName", foreName, value);
                foreName = value;
            }
        }
        public string SurName
        {
            get
            {
                return surName;
            }
            set
            {
                AddToAudit("SurName", surName, value);
                surName = value;
            }
        }
        public string   Salt
        {
            get
            { return salt;}
            set
            { 
                AddToAudit("Salt", salt, value);
                salt = value;
            }
        }
        public DateTime ?CreationDate
        {
            get
            { return creationDate;}
            set
            { 
                AddToAudit("CreationDate", creationDate, value);
                creationDate = value;

            }
        }
        public int      Active
        {
            get
            { return active;}
            set
            { 
                AddToAudit("Active", active, value);

                if (value == 1 && active == 0)
                {
                    active = value; 
                    AccountActivated();
                }
            }
        }

        public bool HasEmailAddress
        {
            get
            { return !String.IsNullOrEmpty(emailAddress); }
        }
        public string   EmailAddress
        {
            get
            { return emailAddress;}
            set
            { 
                AddToAudit("EmailAddress", emailAddress, value);
                emailAddress = value;
            }
        }
        public DateTime ?EmailConfirmed
        {
            get
            { return emailConfirmed;}
            set
            { 
                AddToAudit("EmailConfirmed", emailConfirmed, value);
                emailConfirmed = value;
            }
        }
        public int KxStudentId
        {
            get
            { return kxStudentId;}
            set
            { 
                AddToAudit("KxStudentId", kxStudentId, value);
                kxStudentId = value;

                KxStudentIdChanged();
            }
        }
        public int      UserId
        {
            get
            { return userId;}
            set
            { 
                AddToAudit("UserId", userId, value);
                userId = value;
            }
        }
        public int      PersonId
        {
            get
            { return personId;}
            set
            { 
                AddToAudit("PersonId", personId, value);
                personId = value;
            }
        }
        public DateTime ?LockedAccountTime
        {
            get
            { return lockedAccountTime;}
            set
            { 
                AddToAudit("LockedAccountTime", lockedAccountTime, value);
                lockedAccountTime = value;
            }
        }
        public DateTime CurrentTime
        {
            get
            {
                return DateTime.Now;
            }
        }
        public int RetryCount
        {
            get
            { return retryCount;}
            set
            { 
                AddToAudit("RetryCount", retryCount, value);
                retryCount = value;
            }
        }
        public string UniqueCode
        {
            get
            {
                return GetUniqueCode();
            }
        }
        public string EmailConfirmationStatus
        {
            get
            {
                string result = "Unconfirmed";

                if (emailConfirmed != null)
                {
                    if (emailConfirmed <= DateTime.Now)
                    {
                        result = "Confirmed";
                    }
                }
                return result;
            }
        }
        public bool LoggedIn
        {
            get
            { return loggedIn; }
            set
            {
                if (value != loggedIn && value)
                {
                    UserLoggedIn();
                }
                loggedIn = value; 
            }
        }

        public KxPerson Person
        {
            get
            {
                if (person == null)
                {
                    if (personId > 0)
                    {
                        person = new KxPerson(this, personId);
                    }
                    else if (autoCreatePerson)
                    {
                        person = new KxPerson(this);
                    }
                }
                return person;
            }
            set
            {
                person = value;
            }
        }


        public string Language
        {
            get
            {
                return Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName;
            }
        }

        public string ExtraText1
        {
            get
            { return extraText1; }
            set
            {
                AddToAudit("ExtraText1", extraText1, value);
                extraText1 = value;
            }
        }

        public string ExtraText2
        {
            get
            { return extraText2; }
            set
            {
                AddToAudit("ExtraText2", extraText2, value);
                extraText2 = value;
            }
        }

        public string ExtraText3
        {
            get
            { return extraText3; }
            set
            {
                AddToAudit("ExtraText3", extraText3, value);
                extraText3 = value;
            }
        }


        public int ExtraInt1
        {
            get
            { return extraInt1; }
            set
            {
                AddToAudit("ExtraInt1", extraInt1, value);
                extraInt1 = value;
            }
        }

        public int ExtraInt2
        {
            get
            { return extraInt2; }
            set
            {
                AddToAudit("ExtraInt2", extraInt2, value);
                extraInt2 = value;
            }
        }

        public int ExtraInt3
        {
            get
            { return extraInt3; }
            set
            {
                AddToAudit("ExtraInt3", extraInt3, value);
                extraInt3 = value;
            }
        }

        public string SecretQuestion
        {
            get
            { return secretQuestion; }
            set
            {
                AddToAudit("SecretQuestion", secretQuestion, value);
                secretQuestion = value;
            }
        }

        public string SecretAnswer
        {
            get
            { return secretAnswer; }
            set
            {
                AddToAudit("SecretAnswer", secretAnswer, value);
                secretAnswer = value;
            }
        }

        #endregion 

        public KxWebUser() : base(null)
        {
            DynamicValueN = "";
            DynamicValueM = "";
            DynamicValueL = "";
            DynamicValueK = "";
            DynamicValueJ = "";
            DynamicValueI = "";
            DynamicValueH = "";
            DynamicValueG = "";
            DynamicValueF = "";
            DynamicValueE = "";
            DynamicValueD = "";
            DynamicValueC = "";
            DynamicValueB = "";
            DynamicValueA = "";
            DynamicValue8 = "";
            DynamicValue7 = "";
            DynamicValue6 = "";
            DynamicValue5 = "";
            DynamicValue4 = "";
            DynamicValue3 = "";
            DynamicValue2 = "";
            DynamicValue1 = "";
            if (guidIdentifier == Guid.Empty)
            {
                UserGuid = Guid.NewGuid();
            }
        }

	    public KxWebUser(KxBaseClass parent) : base(parent)
	    {
	        DynamicValueN = "";
	        DynamicValueM = "";
	        DynamicValueL = "";
	        DynamicValueK = "";
	        DynamicValueJ = "";
	        DynamicValueI = "";
	        DynamicValueH = "";
	        DynamicValueG = "";
	        DynamicValueF = "";
	        DynamicValueE = "";
	        DynamicValueD = "";
	        DynamicValueC = "";
	        DynamicValueB = "";
	        DynamicValueA = "";
	        DynamicValue8 = "";
	        DynamicValue7 = "";
	        DynamicValue6 = "";
	        DynamicValue5 = "";
	        DynamicValue4 = "";
	        DynamicValue3 = "";
	        DynamicValue2 = "";
	        DynamicValue1 = "";
	        if (guidIdentifier == Guid.Empty)
            {
                UserGuid = Guid.NewGuid();
                changed = false;    // pha stop ghost record
            }
	    }

	    public KxWebUser(KxBaseClass parent, Guid userGuid) : base(parent)
        {
	        DynamicValueN = "";
	        DynamicValueM = "";
	        DynamicValueL = "";
	        DynamicValueK = "";
	        DynamicValueJ = "";
	        DynamicValueI = "";
	        DynamicValueH = "";
	        DynamicValueG = "";
	        DynamicValueF = "";
	        DynamicValueE = "";
	        DynamicValueD = "";
	        DynamicValueC = "";
	        DynamicValueB = "";
	        DynamicValueA = "";
	        DynamicValue8 = "";
	        DynamicValue7 = "";
	        DynamicValue6 = "";
	        DynamicValue5 = "";
	        DynamicValue4 = "";
	        DynamicValue3 = "";
	        DynamicValue2 = "";
	        DynamicValue1 = "";
	        this.guidIdentifier = userGuid;
            loadType = LoadType.Guid;
            BeforeLoad();
            Load();
        }

        public void LoadGuid(Guid userguid)
        {
            guidIdentifier = userguid;
            loadType = LoadType.Guid;
            BeforeLoad();
            Load();
        }

        public void LoadUserName(string userName)
        {
            if (!String.IsNullOrEmpty(userName))
            {
                this.userName = userName;
                loadType = LoadType.User;
                BeforeLoad();
                Load();
            }
        }

        public void LoadUserNameNew(string userName)
        {
            if (!String.IsNullOrEmpty(userName))
            {
                this.userName = userName;
                loadType = LoadType.NewUser;
                BeforeLoad();
                Load();
            }
        }

        public void LoadEmailAddress(string emailAddress)
        {
            if (!String.IsNullOrEmpty(userName))
            {
                this.EmailAddress = emailAddress;
                loadType = LoadType.Email;
                BeforeLoad();
                Load();
            }
        }

        public void LoadStudent(int kxstudentId)
        {
            this.kxStudentId = kxstudentId;

            loadType = LoadType.Student;
            BeforeLoad();
             Load();
        }
        
        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("webuser", "WebUser", TargetType.Table,actiontype);

            if ((actiontype == ActionType.Update) && (newRow))
            {
                Changed = true;
                result.Action = ActionType.Insert;
                newRow = false;
            }

            if (loadType == LoadType.NewUser && actiontype == ActionType.Select)
            {
                result.KeyFields.Add(new KxDataValue("userName", userName, KxValueType.String));
                result.KeyFields.Add(new KxDataValue("active", 0, KxValueType.Int));
            }
            else if (loadType == LoadType.User && actiontype == ActionType.Select)
            {
                result.KeyFields.Add(new KxDataValue("userName", userName, KxValueType.String));
                result.KeyFields.Add(new KxDataValue("active", 1, KxValueType.Int));
            }
            else if (loadType == LoadType.UserID && actiontype == ActionType.Select)
            {
                result.KeyFields.Add(new KxDataValue("userId", userId, KxValueType.Int));
                result.KeyFields.Add(new KxDataValue("active", 1, KxValueType.Int));
            }
            else if (loadType == LoadType.Student && actiontype == ActionType.Select)
            {
                result.KeyFields.Add(new KxDataValue("kxstudentid", kxStudentId, KxValueType.Int));
                result.KeyFields.Add(new KxDataValue("active", 1, KxValueType.Int));
            }
            else if (loadType == LoadType.Email && actiontype == ActionType.Select)
            {
                result.KeyFields.Add(new KxDataValue("emailaddress", EmailAddress, KxValueType.String));
                result.KeyFields.Add(new KxDataValue("active", 1, KxValueType.Int));
            }
            else
            {
                result.KeyFields.Add(new KxDataValue("userGuid", guidIdentifier, KxValueType.Guid));
            }

            if (actiontype == ActionType.Select)
            {
                guidIdentifier = Guid.Empty;
                newRow = false;
                BeforeLoad();
            }

            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {
                result.AddParameter("userName", userName, KxValueType.String, 255, true);
                result.AddParameter("password", password, KxValueType.String, 255, true);
                result.AddParameter("foreName", foreName, KxValueType.String, 50, true);
                result.AddParameter("surName", surName, KxValueType.String, 50, true);
                result.AddParameter("salt", salt, KxValueType.String, 255, true);
                result.AddParameter("creationDate", creationDate, KxValueType.DateTime, true);
                result.AddParameter("active", active, KxValueType.Int, true);
                result.AddParameter("emailAddress", emailAddress, KxValueType.String, 255, true);
                result.AddParameter("emailConfirmed", emailConfirmed, KxValueType.DateTime, true);
                result.AddParameter("kxStudentId", kxStudentId, KxValueType.Int, true);
                result.AddParameter("userId", userId, KxValueType.Int, true);
                result.AddParameter("personId", personId, KxValueType.Int, true);
                result.AddParameter("lockedAccountTime", lockedAccountTime, KxValueType.DateTime, true);
                result.AddParameter("retryCount", retryCount, KxValueType.Int, true);
                result.AddParameter("extraText1", extraText1, KxValueType.String, true);
                result.AddParameter("extraText2", extraText2, KxValueType.String, true);
                result.AddParameter("extraText3", extraText3, KxValueType.String, true);
                result.AddParameter("extraInt1", extraInt1, KxValueType.Int, true);
                result.AddParameter("extraInt2", extraInt2, KxValueType.Int, true);
                result.AddParameter("extraInt3", extraInt3, KxValueType.Int, true);
                result.AddParameter("secretquestion", secretQuestion, KxValueType.String, true);
                result.AddParameter("secretanswer", secretAnswer, KxValueType.String, true);
            }
            return result;
        }

        public override void LoadFromXml(string xml,XmlNode node)
        {
            guidIdentifier = KxXmlUtils.GetNodeValue(node, "userguid", Guid.Empty);

            if (guidIdentifier != Guid.Empty)
            {
                loaded = true;
            }

            userName = KxXmlUtils.GetNodeValue(node, "username", "");
            password = KxXmlUtils.GetNodeValue(node, "password", "");
            foreName = KxXmlUtils.GetNodeValue(node, "forename", "");
            surName = KxXmlUtils.GetNodeValue(node, "surname", "");
            salt = KxXmlUtils.GetNodeValue(node, "salt", "");
            creationDate = KxXmlUtils.GetNodeValueDT(node, "creationdate", null);
            active = KxXmlUtils.GetNodeValue(node, "active", 0);
            emailAddress = KxXmlUtils.GetNodeValue(node, "emailaddress", "");
            emailConfirmed = KxXmlUtils.GetNodeValueDT(node, "emailconfirmed", null);
            userId = KxXmlUtils.GetNodeValue(node, "userid", 0);
            personId = KxXmlUtils.GetNodeValue(node, "personid", 0);
            lockedAccountTime = KxXmlUtils.GetNodeValueDT(node, "lockedaccounttime", null);
            retryCount = KxXmlUtils.GetNodeValue(node, "retrycount", 0);
            extraText1 = KxXmlUtils.GetNodeValue(node, "extratext1", "");
            extraText2 = KxXmlUtils.GetNodeValue(node, "extratext2", "");
            extraText3 = KxXmlUtils.GetNodeValue(node, "extratext3", "");
            extraInt1 = KxXmlUtils.GetNodeValue(node, "extraint1", 0);
            extraInt2 = KxXmlUtils.GetNodeValue(node, "extraint2", 0);
            extraInt3 = KxXmlUtils.GetNodeValue(node, "extraint3", 0);
            secretQuestion = KxXmlUtils.GetNodeValue(node, "secretquestion", "");
            secretAnswer = KxXmlUtils.GetNodeValue(node, "secretanswer", "");
            KxStudentId = KxXmlUtils.GetNodeValue(node, "kxstudentid", 0);
        }


        public string GetUniqueCode()
        {
           return KxBase64.Base64Encode(UserGuid+"|"+EmailAddress);
        }

        public virtual void BeforeLoad()
        {
           
        }

        public virtual void KxStudentIdChanged()
        {

        }

        public virtual void UserLoggedIn()
        {

        }

        public virtual void AccountActivated()
        {

        }

        public void LoadUserID(int userId)
        {
            if (userId != null && userId != 0)
            {
                this.UserId = userId;
                loadType = LoadType.UserID;
                BeforeLoad();
                Load();
            }
        }

	    public virtual void ClearLoggedInUser()
	    {
	        this.loggedIn = false;
	        this.password = string.Empty;
	        this.salt = string.Empty;
	        this.userName = string.Empty;
	        this.UserGuid = Guid.Empty;
	        this.Active = 0;
	        this.emailAddress = string.Empty;
	        this.creationDate = null;
	        this.foreName = string.Empty;
	        this.surName = string.Empty;
	        this.kxStudentId = 0;
	        this.Loaded = false;
	    }
    }
}

