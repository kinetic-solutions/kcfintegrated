﻿using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;
using Kx.Objects.Common;

namespace Kx.Objects.Common.Web
{
    public class KxUdfMappings : KxBaseClass
    {
  
        private List<KxUdfMapping> items = new List<KxUdfMapping>();

        public List<KxUdfMapping> Items
        {
            get
            { return items; }
        }

        public KxUdfMapping this[int index]
        {
            get
            { return items[index]; }
            set
            { items[index] = value; }
        }

 


        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("udfmapping", "UDFMapping", TargetType.Table);

            result.AddOrderField("webudf",KxDataOrderType.Ascending);

            return result;
        }


        public KxUdfMappings(KxBaseClass parent) : base(parent)
        {
            Load();
        }

        public override bool LoadFromXml(string xml, string xpath)
        {
            items.Clear();
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xml);

                XmlNodeList nodes = doc.SelectNodes(@"//results/" + xpath + "/datarow");

                foreach (XmlNode node in nodes)
                {
                    if (node != null)
                    {
                        KxUdfMapping um = new KxUdfMapping(this, node);
                        Items.Add(um);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "LoadFromXml", ex.Message, xpath);
                LastError = ex.Message;
                return false;
            }
        }

        public int GetGermUdf(int webudf)
        {
            int result = 0;

            foreach (KxUdfMapping u in Items)
            {
                if (u.WebUdf == webudf)
                {
                    result = u.GermUdf;
                    break;
                }
            }

            return result;
        }

        public int GetWebUdf(int germudf)
        {
            int result = 0;

            foreach (KxUdfMapping u in Items)
            {
                if (u.GermUdf == germudf)
                {
                    result = u.WebUdf;
                    break;
                }
            }

            return result;
        }

   
    }
}
