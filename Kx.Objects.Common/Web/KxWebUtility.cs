using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI.WebControls;

namespace Kx.Objects.Common.Web
{
    public class KxWebUtility
    {
        public static TableCell AddCell(string id, string cssClass, string Text)
        {
            return AddCell(id, cssClass, Text, 0, HorizontalAlign.NotSet);
        }

        public static TableCell AddCell(string id, string cssClass, string Text, int ColumnSpan)
        {
            return AddCell(id, cssClass, Text, ColumnSpan, HorizontalAlign.NotSet);
        }

        public static TableCell AddCell(string id, string cssClass, string Text, HorizontalAlign align)
        {
            return AddCell(id, cssClass, Text, 0, align);
        }

        public static TableCell AddCell(string id, string cssClass, string Text, int columnSpan, HorizontalAlign align)
        {
            TableCell result = new TableCell();
            if (id != "")
            {
                result.ID = id;
            }
            result.HorizontalAlign = align;
            if (columnSpan > 1)
            {
                result.ColumnSpan = columnSpan;
            }
            result.Text = Text;
            result.CssClass = cssClass;

            return result;
        }
    }
}
