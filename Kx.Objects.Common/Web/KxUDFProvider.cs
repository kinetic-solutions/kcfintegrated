using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Data;
using Kx.Core.Utility;

namespace Kx.Objects.Common.Web
{
    public class KxUDFProvider
    {
        private static bool Loaded = false;
        private static Hashtable Mapping = new Hashtable();

        private static KxUserDefinedFields udfFields = null;

        public static KxUserDefinedFields UdfFields
        {
            get
            {
                if (udfFields == null)
                {
                    udfFields = new KxUserDefinedFields(null,true);
                }
                return udfFields;
            }
        }
        
        public static KxUserDefinedField GetUdfById(int Id)
        {
            if (!Loaded)
            {
                LoadMappings();
                Loaded = true;
            }

            if (Mapping[Id] != null)
            {
                return UdfFields.GetFieldById(Convert.ToInt32(Mapping[Id]));
            }
            else
            {
                return UdfFields.GetFieldById(Id);
            }
        }

        public static KxUserDefinedField GetUnmappedUdfById(int Id)
        {
            return UdfFields.GetFieldById(Id);
        }


        private static void LoadMappings()
        {
            Mapping.Clear();

            KxData data = new KxData("list", "UDFMapping", TargetType.Table, ActionType.Select);

            if (data.Execute())
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(data.XmlResults);

                XmlNodeList nodes = doc.SelectNodes("//list/datarow");

                foreach (XmlNode node in nodes)
                {
                    int id = KxXmlUtils.GetNodeValue(node, "webudf", 0);
                    string value = KxXmlUtils.GetNodeValue(node, "germudf", id.ToString());

                    if (id != 0 && !Mapping.ContainsKey(id))
                    {
                        Mapping.Add(id, value);
                    }
                }
            }
        }
    }
}
