using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;

namespace Kx.Objects.Common.Web
{
    [Serializable]
    public class KxWebUserPageLog : KxBaseClass
    {
        #region Attributes & Properties

        protected Guid     userGuid;
        protected string   pageName;
        protected int      viewDuration;
        protected DateTime ?lastView;

        public int      WebUserPageLogId
        {
            get
            { return base.dBIdentifier;}
        }

        public Guid     UserGuid
        {
            get
            { return userGuid;}
            set
            { 
                AddToAudit("UserGuid", userGuid, value);
                userGuid = value;
            }
        }

        public string   PageName
        {
            get
            { return pageName;}
            set
            { 
                AddToAudit("PageName", pageName, value);
                pageName = value;
            }
        }

        public int      ViewDuration
        {
            get
            { return viewDuration;}
            set
            { 
                AddToAudit("ViewDuration", viewDuration, value);
                viewDuration = value;
            }
        }

        public DateTime ?LastView
        {
            get
            { return lastView;}
            set
            { 
                AddToAudit("LastView", lastView, value);
                lastView = value;
            }
        }

        #endregion 

        public KxWebUserPageLog(KxBaseClass parent):base(parent)
        {}

        public KxWebUserPageLog(KxBaseClass parent, XmlNode node):base(parent)
        {
             LoadFromXml("", node);
        }

        public KxWebUserPageLog(KxBaseClass parent,int      webuserpagelogid):base(parent)
        {
             base.dBIdentifier = webuserpagelogid;
             Load();
        }


        public KxWebUserPageLog(KxBaseClass parent, Guid userguid, string pagename)
            : base(parent)
        {
            UserGuid = userguid;
            PageName = pagename;
            Load();
        }


        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("webuserpagelog", "WebUserPageLog", TargetType.Table);


            if ((actiontype == ActionType.Update) && (dBIdentifier == 0))
            {
 
                result.Action = ActionType.Insert;
            }
            else
            {
                result.Action = actiontype;
            }

            if (dBIdentifier < 1 && actiontype == ActionType.Select)
            {
                result.AddKeyField("userguid", UserGuid, KxValueType.Guid);
                result.AddKeyField("pageName", pageName, KxValueType.String);
            }
            else
            {
                result.AutoKeyField = new KxDataValue("webuserpagelogid", dBIdentifier, KxValueType.Int);
            }


            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {
                result.AddParameter("userguid", UserGuid, KxValueType.Guid);
                result.AddParameter("pageName", pageName, KxValueType.String, 50, false);
                result.AddParameter("viewDuration", viewDuration, KxValueType.Int, true);
                result.AddParameter("lastView", lastView, KxValueType.DateTime, true);
            }
            return result;
        }


        public override  void LoadFromXml(string xml,XmlNode node)
        {
            dBIdentifier  = KxXmlUtils.GetNodeValue(node,"webuserpagelogid",0);
            userGuid  = KxXmlUtils.GetNodeValue(node,"userguid",Guid.Empty);
            pageName  = KxXmlUtils.GetNodeValue(node,"pagename","");
            viewDuration  = KxXmlUtils.GetNodeValue(node,"viewduration",0);
            lastView  = KxXmlUtils.GetNodeValueDT(node,"lastview",null);
        }
    }
}

