using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Configuration;
using Kx.Core.Data;
using Kx.Core.Exceptions;
using Kx.Core.Utility;

namespace Kx.Objects.Common.Web
{
    public class KxUserDefinedValues : KxBaseClass
    {
        private List<KxUserDefinedValue> items = new List<KxUserDefinedValue>();

        public List<KxUserDefinedValue> Items
        {
            get
            { return items; }
        }

        public KxUserDefinedValue this[int index]
        {
            get
            { return items[index]; }
            set
            { items[index] = value; }
        }


        public override Guid GuidIdentifier
        {
            get
            {
                if (guidIdentifier != Guid.Empty)
                {
                    return guidIdentifier;
                }
                else if (Parent != null)
                {
                    return Parent.GuidIdentifier;
                }
                else
                {
                    return Guid.Empty;
                }
            }
        }


        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = null;

            if (actiontype == ActionType.Select)
            {
                result = new KxData("customdatamgr", "WebStudentApplicationUDFValues", TargetType.Table);

                result.KeyFields.Add(new KxDataValue("applicationGuid", GuidIdentifier, KxValueType.Guid));
            }
            return result;
        }

        public KxUserDefinedValues(KxBaseClass parent) : base(parent)
        {
        }

        public KxUserDefinedValues(KxBaseClass parent, Guid applicationGuid)
            : base(parent)
        {
            this.guidIdentifier = applicationGuid;
            Load();
        }
        
        public override bool LoadFromXml(string xml, string xpath)
        {
            items.Clear();
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xml);

                XmlNodeList nodes = doc.SelectNodes(@"//results/" + xpath + "/datarow");

                foreach (XmlNode node in nodes)
                {
                    if (node != null)
                    {
                        KxUserDefinedValue udf = new KxUserDefinedValue(this, node);
                        items.Add(udf);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "LoadFromXml", ex.Message, xpath);
                LastError = ex.Message;
                return false;
            }
        }

        public KxUserDefinedValue GetValueById(int Id)
        {
            KxUserDefinedValue result = null;

            foreach (KxUserDefinedValue udf in items)
            {
                if (udf.CustomFieldId == Id)
                {
                    result = udf;
                    break;
                }
            }

            if (result == null)
            {
                result = new KxUserDefinedValue(this);
                result.CustomFieldId = Id;
                result.ApplicationGuid = GuidIdentifier;
                items.Add(result);
            }
            return result;
        }
        public void SetUDF(int idx, string value)
        {
            KxUserDefinedValue udf = GetValueById(idx);

            udf.FieldValue = value;
            udf.Save();
        }

        public void SetUDFAndTransferToKx(int idx, string value)
        {
            SetUDF(idx, value);
            KxData data = new KxData("data", KxDbConfiguration.GetConfiguration("UDFProcessToKxProcedure", "kcf.ProcessUDFToKx"), TargetType.StoredProc, ActionType.ExecuteProcedure);
            data.AddParameter("applicationguid", GuidIdentifier.ToString(), KxValueType.String, 50);
            data.AddParameter("webudfid", idx, KxValueType.Int);
            data.AddParameter("value", value, KxValueType.String, 255);
            if (!data.Execute())
            {
                data.LogException("KxWebStudentApplication", "kcf.ProcessUDFToKx", data.LastError, "");
            }
        }

        public void TransferUDFToKx(int idx)
        {
            KxData data = new KxData("data", KxDbConfiguration.GetConfiguration("UDFProcessToKxProcedure", "kcf.ProcessUDFToKx"), TargetType.StoredProc, ActionType.ExecuteProcedure);
            data.AddParameter("applicationguid", GuidIdentifier.ToString(), KxValueType.String, 50);
            data.AddParameter("webudfid", idx, KxValueType.Int);
            if (!data.Execute())
            {
                data.LogException("KxWebStudentApplication", "kcf.ProcessUDFToKx", data.LastError, "");
            }
        }

        public string GetUDF(int idx)
        {
            KxUserDefinedValue udf = GetValueById(idx);

            if (udf != null)
            {
                return udf.FieldValue;
            }
            else
            {
                return "";
            }
        }
    }
}
