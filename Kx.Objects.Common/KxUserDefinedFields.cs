using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Exceptions;
using Kx.Core.Utility;

namespace Kx.Objects.Common
{
    public class KxUserDefinedFields : KxBaseClass
    {
        private SortedList<int,KxUserDefinedField> items = new SortedList<int,KxUserDefinedField>();

        public SortedList<int, KxUserDefinedField> Items
        {
            get
            { return items; }
        }

        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = null;

            if (actiontype == ActionType.Select)
            {
                result = new KxData("customfields", "customfields2", TargetType.Table);
            }
            return result;
        }

        public KxUserDefinedFields(KxBaseClass parent) : base(parent)
        {
           
        }

        public KxUserDefinedFields(KxBaseClass parent,bool doLoad) : base(parent)
        {
            if (doLoad)
            {
                Load();
            }
        }
        
        public override bool LoadFromXml(string xml, string xpath)
        {
            items.Clear();
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xml);

                XmlNodeList nodes = doc.SelectNodes(@"//results/" + xpath + "/datarow");

                foreach (XmlNode node in nodes)
                {
                    if (node != null)
                    {
                        KxUserDefinedField cf = new KxUserDefinedField(this, node);
                        items.Add(cf.CustomFieldId, cf);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "LoadFromXml", ex.Message, xpath);
                LastError = ex.Message;
                return false;
            }
        }

        public KxUserDefinedField GetFieldById(int Id)
        {
            KxUserDefinedField result = null;

            if (Items.ContainsKey(Id))
            {
                result = Items[Id];
            }
            return result;
        }

     
    }
}
