using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;

namespace Kx.Objects.Common
{
    [Serializable]
    public class KxTaxModel : KxBaseClass
    {
        #region Attributes & Properties

        protected string   description;
        protected DateTime ?creationDate;
        protected int      creationUserId;
        protected string   taxViewOutput;
        protected bool     inactive;

        public int      TaxModelId
        {
            get
            { return base.dBIdentifier;}
        }

        public string   Description
        {
            get
            { return description;}
            set
            { 
                AddToAudit("Description", description, value);
                description = value;
            }
        }

        public DateTime ?CreationDate
        {
            get
            { return creationDate;}
            set
            { 
                AddToAudit("CreationDate", creationDate, value);
                creationDate = value;
            }
        }

        public int      CreationUserId
        {
            get
            { return creationUserId;}
            set
            { 
                AddToAudit("CreationUserId", creationUserId, value);
                creationUserId = value;
            }
        }

        public string   TaxViewOutput
        {
            get
            { return taxViewOutput;}
            set
            { 
                AddToAudit("TaxViewOutput", taxViewOutput, value);
                taxViewOutput = value;
            }
        }

        public bool     Inactive
        {
            get
            { return inactive;}
            set
            { 
                AddToAudit("Inactive", inactive, value);
                inactive = value;
            }
        }

        protected KxVATs _taxElements;
        public KxVATs TaxElements
        {
            get
            {
                if (_taxElements == null)
                {
                    _taxElements = new KxVATs(null, this.TaxModelId);
                }
                return _taxElements;
            }
        }

        #endregion 


        public KxTaxModel(KxBaseClass parent):base(parent)
        {}


        public KxTaxModel(KxBaseClass parent, XmlNode node):base(parent)
        {
             LoadFromXml("", node);
        }

        public KxTaxModel(KxBaseClass parent,int      taxmodelid):base(parent)
        {
             base.dBIdentifier = taxmodelid;
             Load();
        }


        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("taxmodels", "TaxModels", TargetType.Table);

            result.AutoKeyField = new KxDataValue("TaxModelId",TaxModelId,KxValueType.Int);


            if ((actiontype == ActionType.Update) && (TaxModelId == 0))
            {
                result.Action = ActionType.Insert;
            }
            else
            {
                result.Action = actiontype;
            }

            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {
                   result.AddParameter("creationDate", creationDate, KxValueType.DateTime, false);
                   result.AddParameter("creationUserId", creationUserId, KxValueType.Int, false);
                   result.AddParameter("taxViewOutput", taxViewOutput, KxValueType.String, 10, true);
                   result.AddParameter("inactive", inactive, KxValueType.Boolean, false);
            }
            return result;
        }

        public decimal CalcNetFromGross (decimal gross)
        {
            if (gross == 0)
            {
                return 0M;
            }
            bool nve = false;
            if (gross < 0)
            {
                nve = true;
                gross = -gross;
            }

            if (this.TaxElements.Items.Count > 0)
            {
                decimal taxRate = new decimal(this.TaxElements.Items[0].Rate);

                taxRate = 1 + (taxRate / 100);

                decimal taxAmount = gross - (gross / taxRate);
                taxAmount = Decimal.Floor(taxAmount * 100) / 100;

                if (taxRate != 1M)
                {
                    if (taxAmount < 0.01M)
                    {
                        taxAmount += 0.01M;
                    }
                }

                decimal result = gross - taxAmount;
                if (nve)
                {
                    result = -result;
                }

                return result;
            }

            return 0M;
        }


        public override  void LoadFromXml(string xml,XmlNode node)
        {
            dBIdentifier  = KxXmlUtils.GetNodeValue(node,"taxmodelid",0);
            description  = KxXmlUtils.GetNodeValue(node,"description","");
            creationDate  = KxXmlUtils.GetNodeValueDT(node,"creationdate",null);
            creationUserId  = KxXmlUtils.GetNodeValue(node,"creationuserid",0);
            taxViewOutput  = KxXmlUtils.GetNodeValue(node,"taxviewoutput","");
            inactive  = KxXmlUtils.GetNodeValue(node,"inactive",false);
        }

        public override string ToString()
        {
            return description;
        }
    }
}

