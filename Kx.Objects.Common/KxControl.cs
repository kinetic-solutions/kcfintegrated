using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;

namespace Kx.Objects.Common
{
    [Serializable]
    public class KxControl : KxBaseClass
    {
        #region Attributes & Properties
        /*
        protected int      agencyCompanyRoleId;
         * */
        protected int      enquiryStatusId;
        /*
        protected string   newEventModuleMask;
        protected string   singleDayModuleArrivalDateMask;
        protected string   singleDayModuleStartDateMask;
        protected string   singleDayModuleEndDateMask;
        protected string   multiDayModuleArrivalDateMask;
        protected string   multiDayModuleStartDateMask;
        protected string   multiDayModuleEndDateMask;
        protected DateTime ?startWeek;
        protected byte     startDay;
        protected byte     startMonth;
        protected bool     rollForward;
        protected string   maxUsers;
        protected string   expiryDate;
        protected byte     notificationPeriod;
		*/
        protected int      defaultCompanyRoleId;
		/*
        protected int      defaultContactRoleId;
        protected Image    defaultCompanyRoleIcon;
        protected Image    defaultContactRoleIcon;
        protected Image    defaultStatusIcon;
        protected string   dimensionUnits;
        protected int      defaultBusinessTypeId;
        protected int      courseBusinessTypeId;
        protected string   selectedEventColour;
        protected string   confRoomPlanAMColour;
        protected string   confRoomPlanPMColour;
        protected string   confRoomPlanEveColour;
        protected int      defaultActionTypeId;
        protected int      defaultNewActionDays;
        protected int      customEventTypeId;
        protected int      maxCapacity;
        protected int      delegateCancelChargeTypeId;
        protected int      delegateTransferChargeTypeId;
        protected int      defaultPersonStatusId;
        protected int      confirmedPersonStatusId;
        protected int      exemptVATId;
         */
        protected int      defaultCompanyStatusId;
/*
        protected string   trainerNotesPassword;
        protected int      defaultTrainerStatus;
        protected int      workPersonAddressTypeId;
        protected int      invoiceBookingContactTypeId;
        protected int      defaultBookingContactTypeId;
         * */
        protected int      defaultEnquirySourceId;
        /*
        protected string   vatNumber;
        protected int      telephoneChargeTypeId;
        protected int      callTimer;
        protected int      invoicePrintCopies;
        */
        protected int      deadCompanyStatusId;
        /*
        protected DateTime ?lastLoggerTime;
        protected int      lastLoggerRecord;
        protected int      delegateDepositChargeTypeId;
        protected int      delegateStandardChargeTypeId;
        protected int      wordTypeFollowUpMailingsId;
        protected int      wordTypeActionsId;
        protected int      wordTypeDelegatesId;
        protected int      wordTypeEnquiriesId;
		 */ 
        protected int      defaultPersonRoleId;
		/*
        protected int      showroundActionTypeId;
        protected int      enhancedCapacity;
       */
        protected int      defaultEventStatusId;
        protected int      defaultEventTypeId;
        protected int      confirmedRoomBookingStatusId;
        protected int      provisionalRoomBookingStatusId;
        /*
        protected string   labelFieldDelimiter;
        protected string   labelRecordDelimiter;
        protected string   labelDataSourceFile;
        protected string   labelTemplateFile;
        protected int      arrivalCoffeeCateringTypeId;
        protected int      morningCoffeeCateringTypeId;
        protected int      lunchCateringTypeId;
        protected int      teaCateringTypeId;
         * */
        protected DateTime ?defaultBookingStartTime;
        protected DateTime ?defaultBookingEndTime;
        protected DateTime ?defaultBookingArrivalTime;
        protected DateTime ?defaultBookingDepartureTime;
        protected int      turnDownStatusId;
		/*
		protected int      requestStatusId;
		protected int      requestDeclinedStatusId;
		protected bool     consolidatedInvoices;
		protected int      invoiceAgeingDays;
		protected int      agentPersonRoleId;
		 * */
        protected int      defaultEventSalesTypeId;
        protected int      cancellationStatusId;
		/*
        protected string   providerAddress;
        protected int      providerPort;
        protected string   providerDLLName;
        protected string   postcodeDLLSource;
        protected string   postcodeINISource;
        protected int      wordTypeBookingsId;
        protected string   cateringcoloursInternal;
        protected string   cateringcoloursExternal;
        protected string   cateringcoloursExtVIP;
        protected string   cateringcoloursIntVIP;
        protected string   cateringcoloursCharged;
        protected bool     duplexEnabled;
        protected int      depositNominalId;
        protected int      debtorControlNominalId;
        protected int      provisionalWarningDays;
        protected int      delegatePeopleTypeId;
        protected string   outofServiceColour;
        protected string   meetingColour;
        protected int      invoiceCompanyRoleId;
        protected string   providerMechanism;
        protected int      requestCancelId;
        protected int      callLoggerHaltTime;
        protected string   tourColour;
        protected double   minConferenceRoomPeriod;
        protected int      timetableRptRoomTimes;
        protected int      defaultChargeToId;
        protected int      defaultAssessmentTypeId;
        protected int      walkInEventTypeId;
        protected int      creditNoteMinimumChar;
        protected int      cateringDeliveryFacilityId;
        protected int      customerDialogueActionTypeId;
        protected string   tickerText;
        protected string   rateCalculationType;
        protected decimal  cateringMinimumOrder;
        protected string   depositMethod;
        protected int      rateCalcNumberVariance;
        protected string   userName;
        protected string   userAddress;
        protected string   userBankAccount;
        protected string   userBankSortCode;
        protected string   userTelephone;
        protected string   userInvoiceMessage;
        protected string   userTermsText;
        protected bool     autoScripts;
        protected string   unavailableColour;
        protected string   nominalCodeSeparator;
        protected bool     dropVATPartPenny;
        protected bool     vatlineBased;
        protected string   availabilityScreen;
        protected int      cancellationTermsNotesId;
        protected string   fileLocation;
        protected int      defaultActionPriorityId;
        protected bool     checkAvailabilityFirst;
        protected string   defaultWalkInTitle;
        protected int      tableBookingTypeId;
        protected bool     newspapers;
        protected int      simpleMAPI;
        protected int      studentEventBusinessTypeId;
        protected int      defaultEventTemplateId;
        protected bool     surNameFirst;
        protected bool     postAfterInvoice;
        protected int      invoiceSortOrder;
        protected int      personConfirmedStatusLevel;
		 */ 
        protected int      eventConfirmedStatusLevel;
		/*
        protected int      defaultTableBookingEventTypeId;
        protected string   liveDatabaseName;
        protected string   eft_Guid;
        protected string   eft_AccountNumber;
        protected int      tableBookingBusinessTypeId;
        protected string   eft_PrimaryIP;
        protected string   eft_SecondaryIP;
        protected bool     captureCardDetailsOnPayment;
        protected bool     multiInternalAccountCodes;
        protected bool     alwaysCreditPostingReversals;
        protected bool     internalCateringPostingNumbers;
        protected bool     forceReservationMatch;
        protected bool     useEmailTemplates;
        protected int      breakfastBookingType;
        protected int      dinnerBookingType;
        protected bool     eventSearchUseUserSiteId;
        protected int      checkinCutoffDays;
        protected int      checkInCutoffDaysFromDeparture;
        protected int      checkoutDaysPrior;
        protected string   cateringColoursWalkIn;
        protected string   floorDimensionUnits;
         * */

        /*
        public int      AgencyCompanyRoleId
        {
            get
            { return agencyCompanyRoleId;}
            set
            { 
                AddToAudit("AgencyCompanyRoleId", agencyCompanyRoleId, value);
                agencyCompanyRoleId = value;
            }
        }

         */
        public int      EnquiryStatusId
        {
            get
            { return enquiryStatusId;}
            set
            { 
                AddToAudit("EnquiryStatusId", enquiryStatusId, value);
                enquiryStatusId = value;
            }
        }
        /*

        public string   NewEventModuleMask
        {
            get
            { return newEventModuleMask;}
            set
            { 
                AddToAudit("NewEventModuleMask", newEventModuleMask, value);
                newEventModuleMask = value;
            }
        }

        public string   SingleDayModuleArrivalDateMask
        {
            get
            { return singleDayModuleArrivalDateMask;}
            set
            { 
                AddToAudit("SingleDayModuleArrivalDateMask", singleDayModuleArrivalDateMask, value);
                singleDayModuleArrivalDateMask = value;
            }
        }

        public string   SingleDayModuleStartDateMask
        {
            get
            { return singleDayModuleStartDateMask;}
            set
            { 
                AddToAudit("SingleDayModuleStartDateMask", singleDayModuleStartDateMask, value);
                singleDayModuleStartDateMask = value;
            }
        }

        public string   SingleDayModuleEndDateMask
        {
            get
            { return singleDayModuleEndDateMask;}
            set
            { 
                AddToAudit("SingleDayModuleEndDateMask", singleDayModuleEndDateMask, value);
                singleDayModuleEndDateMask = value;
            }
        }

        public string   MultiDayModuleArrivalDateMask
        {
            get
            { return multiDayModuleArrivalDateMask;}
            set
            { 
                AddToAudit("MultiDayModuleArrivalDateMask", multiDayModuleArrivalDateMask, value);
                multiDayModuleArrivalDateMask = value;
            }
        }

        public string   MultiDayModuleStartDateMask
        {
            get
            { return multiDayModuleStartDateMask;}
            set
            { 
                AddToAudit("MultiDayModuleStartDateMask", multiDayModuleStartDateMask, value);
                multiDayModuleStartDateMask = value;
            }
        }

        public string   MultiDayModuleEndDateMask
        {
            get
            { return multiDayModuleEndDateMask;}
            set
            { 
                AddToAudit("MultiDayModuleEndDateMask", multiDayModuleEndDateMask, value);
                multiDayModuleEndDateMask = value;
            }
        }

        public DateTime ?StartWeek
        {
            get
            { return startWeek;}
            set
            { 
                AddToAudit("StartWeek", startWeek, value);
                startWeek = value;
            }
        }

        public byte     StartDay
        {
            get
            { return startDay;}
            set
            { 
                AddToAudit("StartDay", startDay, value);
                startDay = value;
            }
        }

        public byte     StartMonth
        {
            get
            { return startMonth;}
            set
            { 
                AddToAudit("StartMonth", startMonth, value);
                startMonth = value;
            }
        }

        public bool     RollForward
        {
            get
            { return rollForward;}
            set
            { 
                AddToAudit("RollForward", rollForward, value);
                rollForward = value;
            }
        }

        public string   MaxUsers
        {
            get
            { return maxUsers;}
            set
            { 
                AddToAudit("MaxUsers", maxUsers, value);
                maxUsers = value;
            }
        }

        public string   ExpiryDate
        {
            get
            { return expiryDate;}
            set
            { 
                AddToAudit("ExpiryDate", expiryDate, value);
                expiryDate = value;
            }
        }

        public byte     NotificationPeriod
        {
            get
            { return notificationPeriod;}
            set
            { 
                AddToAudit("NotificationPeriod", notificationPeriod, value);
                notificationPeriod = value;
            }
        }
		*/
        public int      DefaultCompanyRoleId
        {
            get
            { return defaultCompanyRoleId;}
            set
            { 
                AddToAudit("DefaultCompanyRoleId", defaultCompanyRoleId, value);
                defaultCompanyRoleId = value;
            }
        }
		/*
        public int      DefaultContactRoleId
        {
            get
            { return defaultContactRoleId;}
            set
            { 
                AddToAudit("DefaultContactRoleId", defaultContactRoleId, value);
                defaultContactRoleId = value;
            }
        }

        public Image    DefaultCompanyRoleIcon
        {
            get
            { return defaultCompanyRoleIcon;}
            set
            { 
                AddToAudit("DefaultCompanyRoleIcon", defaultCompanyRoleIcon, value);
                defaultCompanyRoleIcon = value;
            }
        }

        public Image    DefaultContactRoleIcon
        {
            get
            { return defaultContactRoleIcon;}
            set
            { 
                AddToAudit("DefaultContactRoleIcon", defaultContactRoleIcon, value);
                defaultContactRoleIcon = value;
            }
        }

        public Image    DefaultStatusIcon
        {
            get
            { return defaultStatusIcon;}
            set
            { 
                AddToAudit("DefaultStatusIcon", defaultStatusIcon, value);
                defaultStatusIcon = value;
            }
        }

        public string   DimensionUnits
        {
            get
            { return dimensionUnits;}
            set
            { 
                AddToAudit("DimensionUnits", dimensionUnits, value);
                dimensionUnits = value;
            }
        }

        public int      DefaultBusinessTypeId
        {
            get
            { return defaultBusinessTypeId;}
            set
            { 
                AddToAudit("DefaultBusinessTypeId", defaultBusinessTypeId, value);
                defaultBusinessTypeId = value;
            }
        }

        public int      CourseBusinessTypeId
        {
            get
            { return courseBusinessTypeId;}
            set
            { 
                AddToAudit("CourseBusinessTypeId", courseBusinessTypeId, value);
                courseBusinessTypeId = value;
            }
        }

        public string   SelectedEventColour
        {
            get
            { return selectedEventColour;}
            set
            { 
                AddToAudit("SelectedEventColour", selectedEventColour, value);
                selectedEventColour = value;
            }
        }

        public string   ConfRoomPlanAMColour
        {
            get
            { return confRoomPlanAMColour;}
            set
            { 
                AddToAudit("ConfRoomPlanAMColour", confRoomPlanAMColour, value);
                confRoomPlanAMColour = value;
            }
        }

        public string   ConfRoomPlanPMColour
        {
            get
            { return confRoomPlanPMColour;}
            set
            { 
                AddToAudit("ConfRoomPlanPMColour", confRoomPlanPMColour, value);
                confRoomPlanPMColour = value;
            }
        }

        public string   ConfRoomPlanEveColour
        {
            get
            { return confRoomPlanEveColour;}
            set
            { 
                AddToAudit("ConfRoomPlanEveColour", confRoomPlanEveColour, value);
                confRoomPlanEveColour = value;
            }
        }

        public int      DefaultActionTypeId
        {
            get
            { return defaultActionTypeId;}
            set
            { 
                AddToAudit("DefaultActionTypeId", defaultActionTypeId, value);
                defaultActionTypeId = value;
            }
        }

        public int      DefaultNewActionDays
        {
            get
            { return defaultNewActionDays;}
            set
            { 
                AddToAudit("DefaultNewActionDays", defaultNewActionDays, value);
                defaultNewActionDays = value;
            }
        }

        public int      CustomEventTypeId
        {
            get
            { return customEventTypeId;}
            set
            { 
                AddToAudit("CustomEventTypeId", customEventTypeId, value);
                customEventTypeId = value;
            }
        }

        public int      MaxCapacity
        {
            get
            { return maxCapacity;}
            set
            { 
                AddToAudit("MaxCapacity", maxCapacity, value);
                maxCapacity = value;
            }
        }

        public int      DelegateCancelChargeTypeId
        {
            get
            { return delegateCancelChargeTypeId;}
            set
            { 
                AddToAudit("DelegateCancelChargeTypeId", delegateCancelChargeTypeId, value);
                delegateCancelChargeTypeId = value;
            }
        }

        public int      DelegateTransferChargeTypeId
        {
            get
            { return delegateTransferChargeTypeId;}
            set
            { 
                AddToAudit("DelegateTransferChargeTypeId", delegateTransferChargeTypeId, value);
                delegateTransferChargeTypeId = value;
            }
        }

        public int      DefaultPersonStatusId
        {
            get
            { return defaultPersonStatusId;}
            set
            { 
                AddToAudit("DefaultPersonStatusId", defaultPersonStatusId, value);
                defaultPersonStatusId = value;
            }
        }

        public int      ConfirmedPersonStatusId
        {
            get
            { return confirmedPersonStatusId;}
            set
            { 
                AddToAudit("ConfirmedPersonStatusId", confirmedPersonStatusId, value);
                confirmedPersonStatusId = value;
            }
        }

        public int      ExemptVATId
        {
            get
            { return exemptVATId;}
            set
            { 
                AddToAudit("ExemptVATId", exemptVATId, value);
                exemptVATId = value;
            }
        }
*/
        public int      DefaultCompanyStatusId
        {
            get
            { return defaultCompanyStatusId;}
            set
            { 
                AddToAudit("DefaultCompanyStatusId", defaultCompanyStatusId, value);
                defaultCompanyStatusId = value;
            }
        }
/*
        public string   TrainerNotesPassword
        {
            get
            { return trainerNotesPassword;}
            set
            { 
                AddToAudit("TrainerNotesPassword", trainerNotesPassword, value);
                trainerNotesPassword = value;
            }
        }

        public int      DefaultTrainerStatus
        {
            get
            { return defaultTrainerStatus;}
            set
            { 
                AddToAudit("DefaultTrainerStatus", defaultTrainerStatus, value);
                defaultTrainerStatus = value;
            }
        }

        public int      WorkPersonAddressTypeId
        {
            get
            { return workPersonAddressTypeId;}
            set
            { 
                AddToAudit("WorkPersonAddressTypeId", workPersonAddressTypeId, value);
                workPersonAddressTypeId = value;
            }
        }

        public int      InvoiceBookingContactTypeId
        {
            get
            { return invoiceBookingContactTypeId;}
            set
            { 
                AddToAudit("InvoiceBookingContactTypeId", invoiceBookingContactTypeId, value);
                invoiceBookingContactTypeId = value;
            }
        }

        public int      DefaultBookingContactTypeId
        {
            get
            { return defaultBookingContactTypeId;}
            set
            { 
                AddToAudit("DefaultBookingContactTypeId", defaultBookingContactTypeId, value);
                defaultBookingContactTypeId = value;
            }
        }
        */
        public int      DefaultEnquirySourceId
        {
            get
            { return defaultEnquirySourceId;}
            set
            { 
                AddToAudit("DefaultEnquirySourceId", defaultEnquirySourceId, value);
                defaultEnquirySourceId = value;
            }
        }
        /*
        public string   VatNumber
        {
            get
            { return vatNumber;}
            set
            { 
                AddToAudit("VatNumber", vatNumber, value);
                vatNumber = value;
            }
        }

        public int      TelephoneChargeTypeId
        {
            get
            { return telephoneChargeTypeId;}
            set
            { 
                AddToAudit("TelephoneChargeTypeId", telephoneChargeTypeId, value);
                telephoneChargeTypeId = value;
            }
        }

        public int      CallTimer
        {
            get
            { return callTimer;}
            set
            { 
                AddToAudit("CallTimer", callTimer, value);
                callTimer = value;
            }
        }

        public int      InvoicePrintCopies
        {
            get
            { return invoicePrintCopies;}
            set
            { 
                AddToAudit("InvoicePrintCopies", invoicePrintCopies, value);
                invoicePrintCopies = value;
            }
        }
*/
        public int      DeadCompanyStatusId
        {
            get
            { return deadCompanyStatusId;}
            set
            { 
                AddToAudit("DeadCompanyStatusId", deadCompanyStatusId, value);
                deadCompanyStatusId = value;
            }
        }
/*
        public DateTime ?LastLoggerTime
        {
            get
            { return lastLoggerTime;}
            set
            { 
                AddToAudit("LastLoggerTime", lastLoggerTime, value);
                lastLoggerTime = value;
            }
        }

        public int      LastLoggerRecord
        {
            get
            { return lastLoggerRecord;}
            set
            { 
                AddToAudit("LastLoggerRecord", lastLoggerRecord, value);
                lastLoggerRecord = value;
            }
        }

        public int      DelegateDepositChargeTypeId
        {
            get
            { return delegateDepositChargeTypeId;}
            set
            { 
                AddToAudit("DelegateDepositChargeTypeId", delegateDepositChargeTypeId, value);
                delegateDepositChargeTypeId = value;
            }
        }

        public int      DelegateStandardChargeTypeId
        {
            get
            { return delegateStandardChargeTypeId;}
            set
            { 
                AddToAudit("DelegateStandardChargeTypeId", delegateStandardChargeTypeId, value);
                delegateStandardChargeTypeId = value;
            }
        }

        public int      WordTypeFollowUpMailingsId
        {
            get
            { return wordTypeFollowUpMailingsId;}
            set
            { 
                AddToAudit("WordTypeFollowUpMailingsId", wordTypeFollowUpMailingsId, value);
                wordTypeFollowUpMailingsId = value;
            }
        }

        public int      WordTypeActionsId
        {
            get
            { return wordTypeActionsId;}
            set
            { 
                AddToAudit("WordTypeActionsId", wordTypeActionsId, value);
                wordTypeActionsId = value;
            }
        }

        public int      WordTypeDelegatesId
        {
            get
            { return wordTypeDelegatesId;}
            set
            { 
                AddToAudit("WordTypeDelegatesId", wordTypeDelegatesId, value);
                wordTypeDelegatesId = value;
            }
        }

        public int      WordTypeEnquiriesId
        {
            get
            { return wordTypeEnquiriesId;}
            set
            { 
                AddToAudit("WordTypeEnquiriesId", wordTypeEnquiriesId, value);
                wordTypeEnquiriesId = value;
            }
        }
*/
        public int      DefaultPersonRoleId
        {
            get
            { return defaultPersonRoleId;}
            set
            { 
                AddToAudit("DefaultPersonRoleId", defaultPersonRoleId, value);
                defaultPersonRoleId = value;
            }
        }
/*
        public int      ShowroundActionTypeId
        {
            get
            { return showroundActionTypeId;}
            set
            { 
                AddToAudit("ShowroundActionTypeId", showroundActionTypeId, value);
                showroundActionTypeId = value;
            }
        }

        public int      EnhancedCapacity
        {
            get
            { return enhancedCapacity;}
            set
            { 
                AddToAudit("EnhancedCapacity", enhancedCapacity, value);
                enhancedCapacity = value;
            }
        }
        */
        public int      DefaultEventStatusId
        {
            get
            { return defaultEventStatusId;}
            set
            { 
                AddToAudit("DefaultEventStatusId", defaultEventStatusId, value);
                defaultEventStatusId = value;
            }
        }

        public int      DefaultEventTypeId
        {
            get
            { return defaultEventTypeId;}
            set
            { 
                AddToAudit("DefaultEventTypeId", defaultEventTypeId, value);
                defaultEventTypeId = value;
            }
        }

        public int      ConfirmedRoomBookingStatusId
        {
            get
            { return confirmedRoomBookingStatusId;}
            set
            { 
                AddToAudit("ConfirmedRoomBookingStatusId", confirmedRoomBookingStatusId, value);
                confirmedRoomBookingStatusId = value;
            }
        }

        public int      ProvisionalRoomBookingStatusId
        {
            get
            { return provisionalRoomBookingStatusId;}
            set
            { 
                AddToAudit("ProvisionalRoomBookingStatusId", provisionalRoomBookingStatusId, value);
                provisionalRoomBookingStatusId = value;
            }
        }
        /*
        public string   LabelFieldDelimiter
        {
            get
            { return labelFieldDelimiter;}
            set
            { 
                AddToAudit("LabelFieldDelimiter", labelFieldDelimiter, value);
                labelFieldDelimiter = value;
            }
        }

        public string   LabelRecordDelimiter
        {
            get
            { return labelRecordDelimiter;}
            set
            { 
                AddToAudit("LabelRecordDelimiter", labelRecordDelimiter, value);
                labelRecordDelimiter = value;
            }
        }

        public string   LabelDataSourceFile
        {
            get
            { return labelDataSourceFile;}
            set
            { 
                AddToAudit("LabelDataSourceFile", labelDataSourceFile, value);
                labelDataSourceFile = value;
            }
        }

        public string   LabelTemplateFile
        {
            get
            { return labelTemplateFile;}
            set
            { 
                AddToAudit("LabelTemplateFile", labelTemplateFile, value);
                labelTemplateFile = value;
            }
        }

        public int      ArrivalCoffeeCateringTypeId
        {
            get
            { return arrivalCoffeeCateringTypeId;}
            set
            { 
                AddToAudit("ArrivalCoffeeCateringTypeId", arrivalCoffeeCateringTypeId, value);
                arrivalCoffeeCateringTypeId = value;
            }
        }

        public int      MorningCoffeeCateringTypeId
        {
            get
            { return morningCoffeeCateringTypeId;}
            set
            { 
                AddToAudit("MorningCoffeeCateringTypeId", morningCoffeeCateringTypeId, value);
                morningCoffeeCateringTypeId = value;
            }
        }

        public int      LunchCateringTypeId
        {
            get
            { return lunchCateringTypeId;}
            set
            { 
                AddToAudit("LunchCateringTypeId", lunchCateringTypeId, value);
                lunchCateringTypeId = value;
            }
        }

        public int      TeaCateringTypeId
        {
            get
            { return teaCateringTypeId;}
            set
            { 
                AddToAudit("TeaCateringTypeId", teaCateringTypeId, value);
                teaCateringTypeId = value;
            }
        }
         * */

        public DateTime ?DefaultBookingStartTime
        {
            get
            { return defaultBookingStartTime;}
            set
            { 
                AddToAudit("DefaultBookingStartTime", defaultBookingStartTime, value);
                defaultBookingStartTime = value;
            }
        }

        public int DefaultBookingStartTimeMinutes
        {
            get
            { return GetTimeAsMinutes(defaultBookingStartTime); }
           
        }

        public DateTime ?DefaultBookingEndTime
        {
            get
            { return defaultBookingEndTime;}
            set
            { 
                AddToAudit("DefaultBookingEndTime", defaultBookingEndTime, value);
                defaultBookingEndTime = value;
            }
        }

        public int DefaultBookingEndTimeMinutes
        {
            get
            { return GetTimeAsMinutes(defaultBookingEndTime); }

        }

        public DateTime ?DefaultBookingArrivalTime
        {
            get
            { return defaultBookingArrivalTime;}
            set
            { 
                AddToAudit("DefaultBookingArrivalTime", defaultBookingArrivalTime, value);
                defaultBookingArrivalTime = value;
            }
        }

        public DateTime ?DefaultBookingDepartureTime
        {
            get
            { return defaultBookingDepartureTime;}
            set
            { 
                AddToAudit("DefaultBookingDepartureTime", defaultBookingDepartureTime, value);
                defaultBookingDepartureTime = value;
            }
        }

        public int      TurnDownStatusId
        {
            get
            { return turnDownStatusId;}
            set
            { 
                AddToAudit("TurnDownStatusId", turnDownStatusId, value);
                turnDownStatusId = value;
            }
        }
		/*
		public int      RequestStatusId
		{
			get
			{ return requestStatusId;}
			set
			{ 
				AddToAudit("RequestStatusId", requestStatusId, value);
				requestStatusId = value;
			}
		}

		public int      RequestDeclinedStatusId
		{
			get
			{ return requestDeclinedStatusId;}
			set
			{ 
				AddToAudit("RequestDeclinedStatusId", requestDeclinedStatusId, value);
				requestDeclinedStatusId = value;
			}
		}

		public bool     ConsolidatedInvoices
		{
			get
			{ return consolidatedInvoices;}
			set
			{ 
				AddToAudit("ConsolidatedInvoices", consolidatedInvoices, value);
				consolidatedInvoices = value;
			}
		}

		public int      InvoiceAgeingDays
		{
			get
			{ return invoiceAgeingDays;}
			set
			{ 
				AddToAudit("InvoiceAgeingDays", invoiceAgeingDays, value);
				invoiceAgeingDays = value;
			}
		}

		public int      AgentPersonRoleId
		{
			get
			{ return agentPersonRoleId;}
			set
			{ 
				AddToAudit("AgentPersonRoleId", agentPersonRoleId, value);
				agentPersonRoleId = value;
			}
		}
		*/
        public int      DefaultEventSalesTypeId
        {
            get
            { return defaultEventSalesTypeId;}
            set
            { 
                AddToAudit("DefaultEventSalesTypeId", defaultEventSalesTypeId, value);
                defaultEventSalesTypeId = value;
            }
        }
        
        public int      CancellationStatusId
        {
            get
            { return cancellationStatusId;}
            set
            { 
                AddToAudit("CancellationStatusId", cancellationStatusId, value);
                cancellationStatusId = value;
            }
        }
		/*
        public string   ProviderAddress
        {
            get
            { return providerAddress;}
            set
            { 
                AddToAudit("ProviderAddress", providerAddress, value);
                providerAddress = value;
            }
        }

        public int      ProviderPort
        {
            get
            { return providerPort;}
            set
            { 
                AddToAudit("ProviderPort", providerPort, value);
                providerPort = value;
            }
        }

        public string   ProviderDLLName
        {
            get
            { return providerDLLName;}
            set
            { 
                AddToAudit("ProviderDLLName", providerDLLName, value);
                providerDLLName = value;
            }
        }

        public string   PostcodeDLLSource
        {
            get
            { return postcodeDLLSource;}
            set
            { 
                AddToAudit("PostcodeDLLSource", postcodeDLLSource, value);
                postcodeDLLSource = value;
            }
        }

        public string   PostcodeINISource
        {
            get
            { return postcodeINISource;}
            set
            { 
                AddToAudit("PostcodeINISource", postcodeINISource, value);
                postcodeINISource = value;
            }
        }

        public int      WordTypeBookingsId
        {
            get
            { return wordTypeBookingsId;}
            set
            { 
                AddToAudit("WordTypeBookingsId", wordTypeBookingsId, value);
                wordTypeBookingsId = value;
            }
        }

        public string   CateringcoloursInternal
        {
            get
            { return cateringcoloursInternal;}
            set
            { 
                AddToAudit("CateringcoloursInternal", cateringcoloursInternal, value);
                cateringcoloursInternal = value;
            }
        }

        public string   CateringcoloursExternal
        {
            get
            { return cateringcoloursExternal;}
            set
            { 
                AddToAudit("CateringcoloursExternal", cateringcoloursExternal, value);
                cateringcoloursExternal = value;
            }
        }

        public string   CateringcoloursExtVIP
        {
            get
            { return cateringcoloursExtVIP;}
            set
            { 
                AddToAudit("CateringcoloursExtVIP", cateringcoloursExtVIP, value);
                cateringcoloursExtVIP = value;
            }
        }

        public string   CateringcoloursIntVIP
        {
            get
            { return cateringcoloursIntVIP;}
            set
            { 
                AddToAudit("CateringcoloursIntVIP", cateringcoloursIntVIP, value);
                cateringcoloursIntVIP = value;
            }
        }

        public string   CateringcoloursCharged
        {
            get
            { return cateringcoloursCharged;}
            set
            { 
                AddToAudit("CateringcoloursCharged", cateringcoloursCharged, value);
                cateringcoloursCharged = value;
            }
        }

        public bool     DuplexEnabled
        {
            get
            { return duplexEnabled;}
            set
            { 
                AddToAudit("DuplexEnabled", duplexEnabled, value);
                duplexEnabled = value;
            }
        }

        public int      DepositNominalId
        {
            get
            { return depositNominalId;}
            set
            { 
                AddToAudit("DepositNominalId", depositNominalId, value);
                depositNominalId = value;
            }
        }

        public int      DebtorControlNominalId
        {
            get
            { return debtorControlNominalId;}
            set
            { 
                AddToAudit("DebtorControlNominalId", debtorControlNominalId, value);
                debtorControlNominalId = value;
            }
        }

        public int      ProvisionalWarningDays
        {
            get
            { return provisionalWarningDays;}
            set
            { 
                AddToAudit("ProvisionalWarningDays", provisionalWarningDays, value);
                provisionalWarningDays = value;
            }
        }

        public int      DelegatePeopleTypeId
        {
            get
            { return delegatePeopleTypeId;}
            set
            { 
                AddToAudit("DelegatePeopleTypeId", delegatePeopleTypeId, value);
                delegatePeopleTypeId = value;
            }
        }

        public string   OutofServiceColour
        {
            get
            { return outofServiceColour;}
            set
            { 
                AddToAudit("OutofServiceColour", outofServiceColour, value);
                outofServiceColour = value;
            }
        }

        public string   MeetingColour
        {
            get
            { return meetingColour;}
            set
            { 
                AddToAudit("MeetingColour", meetingColour, value);
                meetingColour = value;
            }
        }

        public int      InvoiceCompanyRoleId
        {
            get
            { return invoiceCompanyRoleId;}
            set
            { 
                AddToAudit("InvoiceCompanyRoleId", invoiceCompanyRoleId, value);
                invoiceCompanyRoleId = value;
            }
        }

        public string   ProviderMechanism
        {
            get
            { return providerMechanism;}
            set
            { 
                AddToAudit("ProviderMechanism", providerMechanism, value);
                providerMechanism = value;
            }
        }

        public int      RequestCancelId
        {
            get
            { return requestCancelId;}
            set
            { 
                AddToAudit("RequestCancelId", requestCancelId, value);
                requestCancelId = value;
            }
        }

        public int      CallLoggerHaltTime
        {
            get
            { return callLoggerHaltTime;}
            set
            { 
                AddToAudit("CallLoggerHaltTime", callLoggerHaltTime, value);
                callLoggerHaltTime = value;
            }
        }

        public string   TourColour
        {
            get
            { return tourColour;}
            set
            { 
                AddToAudit("TourColour", tourColour, value);
                tourColour = value;
            }
        }

        public double   MinConferenceRoomPeriod
        {
            get
            { return minConferenceRoomPeriod;}
            set
            { 
                AddToAudit("MinConferenceRoomPeriod", minConferenceRoomPeriod, value);
                minConferenceRoomPeriod = value;
            }
        }

        public int      TimetableRptRoomTimes
        {
            get
            { return timetableRptRoomTimes;}
            set
            { 
                AddToAudit("TimetableRptRoomTimes", timetableRptRoomTimes, value);
                timetableRptRoomTimes = value;
            }
        }

        public int      DefaultChargeToId
        {
            get
            { return defaultChargeToId;}
            set
            { 
                AddToAudit("DefaultChargeToId", defaultChargeToId, value);
                defaultChargeToId = value;
            }
        }

        public int      DefaultAssessmentTypeId
        {
            get
            { return defaultAssessmentTypeId;}
            set
            { 
                AddToAudit("DefaultAssessmentTypeId", defaultAssessmentTypeId, value);
                defaultAssessmentTypeId = value;
            }
        }

        public int      WalkInEventTypeId
        {
            get
            { return walkInEventTypeId;}
            set
            { 
                AddToAudit("WalkInEventTypeId", walkInEventTypeId, value);
                walkInEventTypeId = value;
            }
        }

        public int      CreditNoteMinimumChar
        {
            get
            { return creditNoteMinimumChar;}
            set
            { 
                AddToAudit("CreditNoteMinimumChar", creditNoteMinimumChar, value);
                creditNoteMinimumChar = value;
            }
        }

        public int      CateringDeliveryFacilityId
        {
            get
            { return cateringDeliveryFacilityId;}
            set
            { 
                AddToAudit("CateringDeliveryFacilityId", cateringDeliveryFacilityId, value);
                cateringDeliveryFacilityId = value;
            }
        }

        public int      CustomerDialogueActionTypeId
        {
            get
            { return customerDialogueActionTypeId;}
            set
            { 
                AddToAudit("CustomerDialogueActionTypeId", customerDialogueActionTypeId, value);
                customerDialogueActionTypeId = value;
            }
        }

        public string   TickerText
        {
            get
            { return tickerText;}
            set
            { 
                AddToAudit("TickerText", tickerText, value);
                tickerText = value;
            }
        }

        public string   RateCalculationType
        {
            get
            { return rateCalculationType;}
            set
            { 
                AddToAudit("RateCalculationType", rateCalculationType, value);
                rateCalculationType = value;
            }
        }

        public decimal  CateringMinimumOrder
        {
            get
            { return cateringMinimumOrder;}
            set
            { 
                AddToAudit("CateringMinimumOrder", cateringMinimumOrder, value);
                cateringMinimumOrder = value;
            }
        }

        public string   DepositMethod
        {
            get
            { return depositMethod;}
            set
            { 
                AddToAudit("DepositMethod", depositMethod, value);
                depositMethod = value;
            }
        }

        public int      RateCalcNumberVariance
        {
            get
            { return rateCalcNumberVariance;}
            set
            { 
                AddToAudit("RateCalcNumberVariance", rateCalcNumberVariance, value);
                rateCalcNumberVariance = value;
            }
        }

        public string   UserName
        {
            get
            { return userName;}
            set
            { 
                AddToAudit("UserName", userName, value);
                userName = value;
            }
        }

        public string   UserAddress
        {
            get
            { return userAddress;}
            set
            { 
                AddToAudit("UserAddress", userAddress, value);
                userAddress = value;
            }
        }

        public string   UserBankAccount
        {
            get
            { return userBankAccount;}
            set
            { 
                AddToAudit("UserBankAccount", userBankAccount, value);
                userBankAccount = value;
            }
        }

        public string   UserBankSortCode
        {
            get
            { return userBankSortCode;}
            set
            { 
                AddToAudit("UserBankSortCode", userBankSortCode, value);
                userBankSortCode = value;
            }
        }

        public string   UserTelephone
        {
            get
            { return userTelephone;}
            set
            { 
                AddToAudit("UserTelephone", userTelephone, value);
                userTelephone = value;
            }
        }

        public string   UserInvoiceMessage
        {
            get
            { return userInvoiceMessage;}
            set
            { 
                AddToAudit("UserInvoiceMessage", userInvoiceMessage, value);
                userInvoiceMessage = value;
            }
        }

        public string   UserTermsText
        {
            get
            { return userTermsText;}
            set
            { 
                AddToAudit("UserTermsText", userTermsText, value);
                userTermsText = value;
            }
        }

        public bool     AutoScripts
        {
            get
            { return autoScripts;}
            set
            { 
                AddToAudit("AutoScripts", autoScripts, value);
                autoScripts = value;
            }
        }

        public string   UnavailableColour
        {
            get
            { return unavailableColour;}
            set
            { 
                AddToAudit("UnavailableColour", unavailableColour, value);
                unavailableColour = value;
            }
        }

        public string   NominalCodeSeparator
        {
            get
            { return nominalCodeSeparator;}
            set
            { 
                AddToAudit("NominalCodeSeparator", nominalCodeSeparator, value);
                nominalCodeSeparator = value;
            }
        }

        public bool     DropVATPartPenny
        {
            get
            { return dropVATPartPenny;}
            set
            { 
                AddToAudit("DropVATPartPenny", dropVATPartPenny, value);
                dropVATPartPenny = value;
            }
        }

        public bool     VatlineBased
        {
            get
            { return vatlineBased;}
            set
            { 
                AddToAudit("VatlineBased", vatlineBased, value);
                vatlineBased = value;
            }
        }

        public string   AvailabilityScreen
        {
            get
            { return availabilityScreen;}
            set
            { 
                AddToAudit("AvailabilityScreen", availabilityScreen, value);
                availabilityScreen = value;
            }
        }

        public int      CancellationTermsNotesId
        {
            get
            { return cancellationTermsNotesId;}
            set
            { 
                AddToAudit("CancellationTermsNotesId", cancellationTermsNotesId, value);
                cancellationTermsNotesId = value;
            }
        }

        public string   FileLocation
        {
            get
            { return fileLocation;}
            set
            { 
                AddToAudit("FileLocation", fileLocation, value);
                fileLocation = value;
            }
        }

        public int      DefaultActionPriorityId
        {
            get
            { return defaultActionPriorityId;}
            set
            { 
                AddToAudit("DefaultActionPriorityId", defaultActionPriorityId, value);
                defaultActionPriorityId = value;
            }
        }

        public bool     CheckAvailabilityFirst
        {
            get
            { return checkAvailabilityFirst;}
            set
            { 
                AddToAudit("CheckAvailabilityFirst", checkAvailabilityFirst, value);
                checkAvailabilityFirst = value;
            }
        }

        public string   DefaultWalkInTitle
        {
            get
            { return defaultWalkInTitle;}
            set
            { 
                AddToAudit("DefaultWalkInTitle", defaultWalkInTitle, value);
                defaultWalkInTitle = value;
            }
        }

        public int      TableBookingTypeId
        {
            get
            { return tableBookingTypeId;}
            set
            { 
                AddToAudit("TableBookingTypeId", tableBookingTypeId, value);
                tableBookingTypeId = value;
            }
        }

        public bool     Newspapers
        {
            get
            { return newspapers;}
            set
            { 
                AddToAudit("Newspapers", newspapers, value);
                newspapers = value;
            }
        }

        public int      SimpleMAPI
        {
            get
            { return simpleMAPI;}
            set
            { 
                AddToAudit("SimpleMAPI", simpleMAPI, value);
                simpleMAPI = value;
            }
        }

        public int      StudentEventBusinessTypeId
        {
            get
            { return studentEventBusinessTypeId;}
            set
            { 
                AddToAudit("StudentEventBusinessTypeId", studentEventBusinessTypeId, value);
                studentEventBusinessTypeId = value;
            }
        }

        public int      DefaultEventTemplateId
        {
            get
            { return defaultEventTemplateId;}
            set
            { 
                AddToAudit("DefaultEventTemplateId", defaultEventTemplateId, value);
                defaultEventTemplateId = value;
            }
        }

        public bool     SurNameFirst
        {
            get
            { return surNameFirst;}
            set
            { 
                AddToAudit("SurNameFirst", surNameFirst, value);
                surNameFirst = value;
            }
        }

        public bool     PostAfterInvoice
        {
            get
            { return postAfterInvoice;}
            set
            { 
                AddToAudit("PostAfterInvoice", postAfterInvoice, value);
                postAfterInvoice = value;
            }
        }

        public int      InvoiceSortOrder
        {
            get
            { return invoiceSortOrder;}
            set
            { 
                AddToAudit("InvoiceSortOrder", invoiceSortOrder, value);
                invoiceSortOrder = value;
            }
        }

        public int      PersonConfirmedStatusLevel
        {
            get
            { return personConfirmedStatusLevel;}
            set
            { 
                AddToAudit("PersonConfirmedStatusLevel", personConfirmedStatusLevel, value);
                personConfirmedStatusLevel = value;
            }
        }
		*/
        public int      EventConfirmedStatusLevel
        {
            get
            { return eventConfirmedStatusLevel;}
            set
            { 
                AddToAudit("EventConfirmedStatusLevel", eventConfirmedStatusLevel, value);
                eventConfirmedStatusLevel = value;
            }
        }
		/*
        public int      DefaultTableBookingEventTypeId
        {
            get
            { return defaultTableBookingEventTypeId;}
            set
            { 
                AddToAudit("DefaultTableBookingEventTypeId", defaultTableBookingEventTypeId, value);
                defaultTableBookingEventTypeId = value;
            }
        }

        public string   LiveDatabaseName
        {
            get
            { return liveDatabaseName;}
            set
            { 
                AddToAudit("LiveDatabaseName", liveDatabaseName, value);
                liveDatabaseName = value;
            }
        }

        public string   Eft_GuId
        {
            get
            { return eft_Guid;}
            set
            { 
                AddToAudit("Eft_GuId", eft_Guid, value);
                eft_Guid = value;
            }
        }

        public string   Eft_AccountNumber
        {
            get
            { return eft_AccountNumber;}
            set
            { 
                AddToAudit("Eft_AccountNumber", eft_AccountNumber, value);
                eft_AccountNumber = value;
            }
        }

        public int      TableBookingBusinessTypeId
        {
            get
            { return tableBookingBusinessTypeId;}
            set
            { 
                AddToAudit("TableBookingBusinessTypeId", tableBookingBusinessTypeId, value);
                tableBookingBusinessTypeId = value;
            }
        }

        public string   Eft_PrimaryIP
        {
            get
            { return eft_PrimaryIP;}
            set
            { 
                AddToAudit("Eft_PrimaryIP", eft_PrimaryIP, value);
                eft_PrimaryIP = value;
            }
        }

        public string   Eft_SecondaryIP
        {
            get
            { return eft_SecondaryIP;}
            set
            { 
                AddToAudit("Eft_SecondaryIP", eft_SecondaryIP, value);
                eft_SecondaryIP = value;
            }
        }

        public bool     CaptureCardDetailsOnPayment
        {
            get
            { return captureCardDetailsOnPayment;}
            set
            { 
                AddToAudit("CaptureCardDetailsOnPayment", captureCardDetailsOnPayment, value);
                captureCardDetailsOnPayment = value;
            }
        }

        public bool     MultiInternalAccountCodes
        {
            get
            { return multiInternalAccountCodes;}
            set
            { 
                AddToAudit("MultiInternalAccountCodes", multiInternalAccountCodes, value);
                multiInternalAccountCodes = value;
            }
        }

        public bool     AlwaysCreditPostingReversals
        {
            get
            { return alwaysCreditPostingReversals;}
            set
            { 
                AddToAudit("AlwaysCreditPostingReversals", alwaysCreditPostingReversals, value);
                alwaysCreditPostingReversals = value;
            }
        }

        public bool     InternalCateringPostingNumbers
        {
            get
            { return internalCateringPostingNumbers;}
            set
            { 
                AddToAudit("InternalCateringPostingNumbers", internalCateringPostingNumbers, value);
                internalCateringPostingNumbers = value;
            }
        }

        public bool     ForceReservationMatch
        {
            get
            { return forceReservationMatch;}
            set
            { 
                AddToAudit("ForceReservationMatch", forceReservationMatch, value);
                forceReservationMatch = value;
            }
        }

        public bool     UseEmailTemplates
        {
            get
            { return useEmailTemplates;}
            set
            { 
                AddToAudit("UseEmailTemplates", useEmailTemplates, value);
                useEmailTemplates = value;
            }
        }

        public int      BreakfastBookingType
        {
            get
            { return breakfastBookingType;}
            set
            { 
                AddToAudit("BreakfastBookingType", breakfastBookingType, value);
                breakfastBookingType = value;
            }
        }

        public int      DinnerBookingType
        {
            get
            { return dinnerBookingType;}
            set
            { 
                AddToAudit("DinnerBookingType", dinnerBookingType, value);
                dinnerBookingType = value;
            }
        }

        public bool     EventSearchUseUserSiteId
        {
            get
            { return eventSearchUseUserSiteId;}
            set
            { 
                AddToAudit("EventSearchUseUserSiteId", eventSearchUseUserSiteId, value);
                eventSearchUseUserSiteId = value;
            }
        }

        public int      CheckinCutoffDays
        {
            get
            { return checkinCutoffDays;}
            set
            { 
                AddToAudit("CheckinCutoffDays", checkinCutoffDays, value);
                checkinCutoffDays = value;
            }
        }

        public int      CheckInCutoffDaysFromDeparture
        {
            get
            { return checkInCutoffDaysFromDeparture;}
            set
            { 
                AddToAudit("CheckInCutoffDaysFromDeparture", checkInCutoffDaysFromDeparture, value);
                checkInCutoffDaysFromDeparture = value;
            }
        }

        public int      CheckoutDaysPrior
        {
            get
            { return checkoutDaysPrior;}
            set
            { 
                AddToAudit("CheckoutDaysPrior", checkoutDaysPrior, value);
                checkoutDaysPrior = value;
            }
        }

        public string   CateringColoursWalkIn
        {
            get
            { return cateringColoursWalkIn;}
            set
            { 
                AddToAudit("CateringColoursWalkIn", cateringColoursWalkIn, value);
                cateringColoursWalkIn = value;
            }
        }

        public string   FloorDimensionUnits
        {
            get
            { return floorDimensionUnits;}
            set
            { 
                AddToAudit("FloorDimensionUnits", floorDimensionUnits, value);
                floorDimensionUnits = value;
            }
        }
        */

        #endregion 


        public KxControl(KxBaseClass parent)
            : base(parent)
        {
            Load();
        }

        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("control", "Control", TargetType.Table);

            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {
               
                   result.AddParameter("enquiryStatusId", enquiryStatusId, KxValueType.Int, true);
                   /*
                     result.AddParameter("newEventModuleMask", newEventModuleMask, KxValueType.String, 20, true);
                     result.AddParameter("singleDayModuleArrivalDateMask", singleDayModuleArrivalDateMask, KxValueType.String, 20, true);
                     result.AddParameter("singleDayModuleStartDateMask", singleDayModuleStartDateMask, KxValueType.String, 20, true);
                     result.AddParameter("singleDayModuleEndDateMask", singleDayModuleEndDateMask, KxValueType.String, 20, true);
                     result.AddParameter("multiDayModuleArrivalDateMask", multiDayModuleArrivalDateMask, KxValueType.String, 20, true);
                     result.AddParameter("multiDayModuleStartDateMask", multiDayModuleStartDateMask, KxValueType.String, 20, true);
                     result.AddParameter("multiDayModuleEndDateMask", multiDayModuleEndDateMask, KxValueType.String, 20, true);
                     result.AddParameter("startWeek", startWeek, KxValueType.DateTime, true);
                     result.AddParameter("startDay", startDay, KxValueType.Short, true);
                     result.AddParameter("startMonth", startMonth, KxValueType.Short, true);
                     result.AddParameter("rollForward", rollForward, KxValueType.Boolean, false);
                     result.AddParameter("maxUsers", maxUsers, KxValueType.String, 10, true);
                     result.AddParameter("expiryDate", expiryDate, KxValueType.String, 10, true);
                     result.AddParameter("notificationPeriod", notificationPeriod, KxValueType.Short, true);
				    */
                     result.AddParameter("defaultCompanyRoleId", defaultCompanyRoleId, KxValueType.Int, true);
					/*
                     result.AddParameter("defaultContactRoleId", defaultContactRoleId, KxValueType.Int, true);
                     result.AddParameter("defaultCompanyRoleIcon", defaultCompanyRoleIcon, KxValueType.Image, true);
                     result.AddParameter("defaultContactRoleIcon", defaultContactRoleIcon, KxValueType.Image, true);
                     result.AddParameter("defaultStatusIcon", defaultStatusIcon, KxValueType.Image, true);
                     result.AddParameter("dimensionUnits", dimensionUnits, KxValueType.String, 10, true);
                     result.AddParameter("defaultBusinessTypeId", defaultBusinessTypeId, KxValueType.Int, true);
                     result.AddParameter("courseBusinessTypeId", courseBusinessTypeId, KxValueType.Int, true);
                     result.AddParameter("selectedEventColour", selectedEventColour, KxValueType.String, 10, true);
                     result.AddParameter("confRoomPlanAMColour", confRoomPlanAMColour, KxValueType.String, 10, true);
                     result.AddParameter("confRoomPlanPMColour", confRoomPlanPMColour, KxValueType.String, 10, true);
                     result.AddParameter("confRoomPlanEveColour", confRoomPlanEveColour, KxValueType.String, 10, true);
                     result.AddParameter("defaultActionTypeId", defaultActionTypeId, KxValueType.Int, true);
                     result.AddParameter("defaultNewActionDays", defaultNewActionDays, KxValueType.Int, true);
                     result.AddParameter("customEventTypeId", customEventTypeId, KxValueType.Int, true);
                     result.AddParameter("maxCapacity", maxCapacity, KxValueType.Int, true);
                     result.AddParameter("delegateCancelChargeTypeId", delegateCancelChargeTypeId, KxValueType.Int, true);
                     result.AddParameter("delegateTransferChargeTypeId", delegateTransferChargeTypeId, KxValueType.Int, true);
                     result.AddParameter("defaultPersonStatusId", defaultPersonStatusId, KxValueType.Int, true);
                     result.AddParameter("confirmedPersonStatusId", confirmedPersonStatusId, KxValueType.Int, true);
                     result.AddParameter("exemptVATId", exemptVATId, KxValueType.Int, true);

                   */
                   result.AddParameter("defaultCompanyStatusId", defaultCompanyStatusId, KxValueType.Int, true);
                   /* 
                     result.AddParameter("trainerNotesPassword", trainerNotesPassword, KxValueType.String, 8, true);
                     result.AddParameter("defaultTrainerStatus", defaultTrainerStatus, KxValueType.Int, true);
                     result.AddParameter("workPersonAddressTypeId", workPersonAddressTypeId, KxValueType.Int, true);
                     result.AddParameter("invoiceBookingContactTypeId", invoiceBookingContactTypeId, KxValueType.Int, true);
                     result.AddParameter("defaultBookingContactTypeId", defaultBookingContactTypeId, KxValueType.Int, true);
                   */
                   result.AddParameter("defaultEnquirySourceId", defaultEnquirySourceId, KxValueType.Int, true);
                /*
                   result.AddParameter("vatNumber", vatNumber, KxValueType.String, 30, true);
                   result.AddParameter("telephoneChargeTypeId", telephoneChargeTypeId, KxValueType.Int, true);
                   result.AddParameter("callTimer", callTimer, KxValueType.Int, true);
                   result.AddParameter("invoicePrintCopies", invoicePrintCopies, KxValueType.Int, true);
                   */
                   result.AddParameter("deadCompanyStatusId", deadCompanyStatusId, KxValueType.Int, true);
                /*
                   result.AddParameter("lastLoggerTime", lastLoggerTime, KxValueType.DateTime, true);
                   result.AddParameter("lastLoggerRecord", lastLoggerRecord, KxValueType.Int, true);
                   result.AddParameter("delegateDepositChargeTypeId", delegateDepositChargeTypeId, KxValueType.Int, true);
                   result.AddParameter("delegateStandardChargeTypeId", delegateStandardChargeTypeId, KxValueType.Int, true);
                   result.AddParameter("wordTypeFollowUpMailingsId", wordTypeFollowUpMailingsId, KxValueType.Int, true);
                   result.AddParameter("wordTypeActionsId", wordTypeActionsId, KxValueType.Int, true);
                   result.AddParameter("wordTypeDelegatesId", wordTypeDelegatesId, KxValueType.Int, true);
                   result.AddParameter("wordTypeEnquiriesId", wordTypeEnquiriesId, KxValueType.Int, true);
				 */ 
                   result.AddParameter("defaultPersonRoleId", defaultPersonRoleId, KxValueType.Int, true);
				/*
                   result.AddParameter("showroundActionTypeId", showroundActionTypeId, KxValueType.Int, true);
                   result.AddParameter("enhancedCapacity", enhancedCapacity, KxValueType.Int, true);
                 * */
                   result.AddParameter("defaultEventStatusId", defaultEventStatusId, KxValueType.Int, true);
                   result.AddParameter("defaultEventTypeId", defaultEventTypeId, KxValueType.Int, true);
                   result.AddParameter("confirmedRoomBookingStatusId", confirmedRoomBookingStatusId, KxValueType.Int, true);
                   result.AddParameter("provisionalRoomBookingStatusId", provisionalRoomBookingStatusId, KxValueType.Int, true);
                /*
                   result.AddParameter("labelFieldDelimiter", labelFieldDelimiter, KxValueType.String, 10, true);
                   result.AddParameter("labelRecordDelimiter", labelRecordDelimiter, KxValueType.String, 10, true);
                   result.AddParameter("labelDataSourceFile", labelDataSourceFile, KxValueType.String, 255, true);
                   result.AddParameter("labelTemplateFile", labelTemplateFile, KxValueType.String, 255, true);
                   result.AddParameter("arrivalCoffeeCateringTypeId", arrivalCoffeeCateringTypeId, KxValueType.Int, true);
                   result.AddParameter("morningCoffeeCateringTypeId", morningCoffeeCateringTypeId, KxValueType.Int, true);
                   result.AddParameter("lunchCateringTypeId", lunchCateringTypeId, KxValueType.Int, true);
                   result.AddParameter("teaCateringTypeId", teaCateringTypeId, KxValueType.Int, true);
                */
                   result.AddParameter("defaultBookingStartTime", defaultBookingStartTime, KxValueType.DateTime, true);
                   result.AddParameter("defaultBookingEndTime", defaultBookingEndTime, KxValueType.DateTime, true);
                   result.AddParameter("defaultBookingArrivalTime", defaultBookingArrivalTime, KxValueType.DateTime, true);
                   result.AddParameter("defaultBookingDepartureTime", defaultBookingDepartureTime, KxValueType.DateTime, true);
                   result.AddParameter("turnDownStatusId", turnDownStatusId, KxValueType.Int, true);
				 /*
					result.AddParameter("requestStatusId", requestStatusId, KxValueType.Int, true);
					result.AddParameter("requestDeclinedStatusId", requestDeclinedStatusId, KxValueType.Int, true);
					result.AddParameter("consolidatedInvoices", consolidatedInvoices, KxValueType.Boolean, false);
					result.AddParameter("invoiceAgeingDays", invoiceAgeingDays, KxValueType.Int, false);
					result.AddParameter("agentPersonRoleId", agentPersonRoleId, KxValueType.Int, true);
				*/
                   result.AddParameter("defaultEventSalesTypeId", defaultEventSalesTypeId, KxValueType.Int, true);
                   result.AddParameter("cancellationStatusId", cancellationStatusId, KxValueType.Int, true);
                /*
				   result.AddParameter("providerAddress", providerAddress, KxValueType.String, 50, true);
                   result.AddParameter("providerPort", providerPort, KxValueType.Int, true);
                   result.AddParameter("providerDLLName", providerDLLName, KxValueType.String, 50, true);
                   result.AddParameter("postcodeDLLSource", postcodeDLLSource, KxValueType.String, 500, true);
                   result.AddParameter("postcodeINISource", postcodeINISource, KxValueType.String, 500, true);
                   result.AddParameter("wordTypeBookingsId", wordTypeBookingsId, KxValueType.Int, true);
                   result.AddParameter("cateringcoloursInternal", cateringcoloursInternal, KxValueType.String, 10, true);
                   result.AddParameter("cateringcoloursExternal", cateringcoloursExternal, KxValueType.String, 10, true);
                   result.AddParameter("cateringcoloursExtVIP", cateringcoloursExtVIP, KxValueType.String, 10, true);
                   result.AddParameter("cateringcoloursIntVIP", cateringcoloursIntVIP, KxValueType.String, 10, true);
                   result.AddParameter("cateringcoloursCharged", cateringcoloursCharged, KxValueType.String, 10, true);
                   result.AddParameter("duplexEnabled", duplexEnabled, KxValueType.Boolean, false);
                   result.AddParameter("depositNominalId", depositNominalId, KxValueType.Int, true);
                   result.AddParameter("debtorControlNominalId", debtorControlNominalId, KxValueType.Int, true);
                   result.AddParameter("provisionalWarningDays", provisionalWarningDays, KxValueType.Int, true);
                   result.AddParameter("delegatePeopleTypeId", delegatePeopleTypeId, KxValueType.Int, true);
                   result.AddParameter("outofServiceColour", outofServiceColour, KxValueType.String, 10, true);
                   result.AddParameter("meetingColour", meetingColour, KxValueType.String, 10, true);
                   result.AddParameter("invoiceCompanyRoleId", invoiceCompanyRoleId, KxValueType.Int, true);
                   result.AddParameter("providerMechanism", providerMechanism, KxValueType.String, 1, true);
                   result.AddParameter("requestCancelId", requestCancelId, KxValueType.Int, true);
                   result.AddParameter("callLoggerHaltTime", callLoggerHaltTime, KxValueType.Int, true);
                   result.AddParameter("tourColour", tourColour, KxValueType.String, 10, true);
                   result.AddParameter("minConferenceRoomPeriod", minConferenceRoomPeriod, KxValueType.Float, true);
                   result.AddParameter("timetableRptRoomTimes", timetableRptRoomTimes, KxValueType.Int, false);
                   result.AddParameter("defaultChargeToId", defaultChargeToId, KxValueType.Int, true);
                   result.AddParameter("defaultAssessmentTypeId", defaultAssessmentTypeId, KxValueType.Int, true);
                   result.AddParameter("walkInEventTypeId", walkInEventTypeId, KxValueType.Int, true);
                   result.AddParameter("creditNoteMinimumChar", creditNoteMinimumChar, KxValueType.Int, true);
                   result.AddParameter("cateringDeliveryFacilityId", cateringDeliveryFacilityId, KxValueType.Int, true);
                   result.AddParameter("customerDialogueActionTypeId", customerDialogueActionTypeId, KxValueType.Int, true);
                   result.AddParameter("tickerText", tickerText, KxValueType.String, 1000, true);
                   result.AddParameter("rateCalculationType", rateCalculationType, KxValueType.String, 1, true);
                   result.AddParameter("cateringMinimumOrder", cateringMinimumOrder, KxValueType.Decimal, true);
                   result.AddParameter("depositMethod", depositMethod, KxValueType.String, 1, true);
                   result.AddParameter("rateCalcNumberVariance", rateCalcNumberVariance, KxValueType.Int, true);
                   result.AddParameter("userName", userName, KxValueType.String, 50, true);
                   result.AddParameter("userAddress", userAddress, KxValueType.String, 2147483647, true);
                   result.AddParameter("userBankAccount", userBankAccount, KxValueType.String, 50, true);
                   result.AddParameter("userBankSortCode", userBankSortCode, KxValueType.String, 10, true);
                   result.AddParameter("userTelephone", userTelephone, KxValueType.String, 50, true);
                   result.AddParameter("userInvoiceMessage", userInvoiceMessage, KxValueType.String, 2147483647, true);
                   result.AddParameter("userTermsText", userTermsText, KxValueType.String, 50, true);
                   result.AddParameter("autoScripts", autoScripts, KxValueType.Boolean, true);
                   result.AddParameter("unavailableColour", unavailableColour, KxValueType.String, 10, true);
                   result.AddParameter("nominalCodeSeparator", nominalCodeSeparator, KxValueType.String, 1, true);
                   result.AddParameter("dropVATPartPenny", dropVATPartPenny, KxValueType.Boolean, true);
                   result.AddParameter("vatlineBased", vatlineBased, KxValueType.Boolean, true);
                   result.AddParameter("availabilityScreen", availabilityScreen, KxValueType.String, 1, true);
                   result.AddParameter("cancellationTermsNotesId", cancellationTermsNotesId, KxValueType.Int, true);
                   result.AddParameter("fileLocation", fileLocation, KxValueType.String, 255, true);
                   result.AddParameter("defaultActionPriorityId", defaultActionPriorityId, KxValueType.Int, true);
                   result.AddParameter("checkAvailabilityFirst", checkAvailabilityFirst, KxValueType.Boolean, false);
                   result.AddParameter("defaultWalkInTitle", defaultWalkInTitle, KxValueType.String, 255, true);
                   result.AddParameter("tableBookingTypeId", tableBookingTypeId, KxValueType.Int, true);
                   result.AddParameter("newspapers", newspapers, KxValueType.Boolean, true);
                   result.AddParameter("simpleMAPI", simpleMAPI, KxValueType.Int, true);
                   result.AddParameter("studentEventBusinessTypeId", studentEventBusinessTypeId, KxValueType.Int, true);
                   result.AddParameter("defaultEventTemplateId", defaultEventTemplateId, KxValueType.Int, true);
                   result.AddParameter("surNameFirst", surNameFirst, KxValueType.Boolean, false);
                   result.AddParameter("postAfterInvoice", postAfterInvoice, KxValueType.Boolean, false);
                   result.AddParameter("invoiceSortOrder", invoiceSortOrder, KxValueType.Int, false);
                   result.AddParameter("personConfirmedStatusLevel", personConfirmedStatusLevel, KxValueType.Int, true);
				 */
                   result.AddParameter("eventConfirmedStatusLevel", eventConfirmedStatusLevel, KxValueType.Int, true);
/*
                   result.AddParameter("defaultTableBookingEventTypeId", defaultTableBookingEventTypeId, KxValueType.Int, true);
                   result.AddParameter("liveDatabaseName", liveDatabaseName, KxValueType.String, 50, true);
                   result.AddParameter("eft_Guid", eft_Guid, KxValueType.String, 255, true);
                   result.AddParameter("eft_AccountNumber", eft_AccountNumber, KxValueType.String, 50, true);
                   result.AddParameter("tableBookingBusinessTypeId", tableBookingBusinessTypeId, KxValueType.Int, true);
                   result.AddParameter("eft_PrimaryIP", eft_PrimaryIP, KxValueType.String, 255, true);
                   result.AddParameter("eft_SecondaryIP", eft_SecondaryIP, KxValueType.String, 255, true);
                   result.AddParameter("captureCardDetailsOnPayment", captureCardDetailsOnPayment, KxValueType.Boolean, true);
                   result.AddParameter("multiInternalAccountCodes", multiInternalAccountCodes, KxValueType.Boolean, true);
                   result.AddParameter("alwaysCreditPostingReversals", alwaysCreditPostingReversals, KxValueType.Boolean, true);
                   result.AddParameter("internalCateringPostingNumbers", internalCateringPostingNumbers, KxValueType.Boolean, true);
                   result.AddParameter("forceReservationMatch", forceReservationMatch, KxValueType.Boolean, true);
                   result.AddParameter("useEmailTemplates", useEmailTemplates, KxValueType.Boolean, true);
                   result.AddParameter("breakfastBookingType", breakfastBookingType, KxValueType.Int, true);
                   result.AddParameter("dinnerBookingType", dinnerBookingType, KxValueType.Int, true);
                   result.AddParameter("eventSearchUseUserSiteId", eventSearchUseUserSiteId, KxValueType.Boolean, true);
                   result.AddParameter("checkinCutoffDays", checkinCutoffDays, KxValueType.Int, true);
                   result.AddParameter("checkInCutoffDaysFromDeparture", checkInCutoffDaysFromDeparture, KxValueType.Int, true);
                   result.AddParameter("checkoutDaysPrior", checkoutDaysPrior, KxValueType.Int, true);
                   result.AddParameter("cateringColoursWalkIn", cateringColoursWalkIn, KxValueType.String, 10, true);
                   result.AddParameter("floorDimensionUnits", floorDimensionUnits, KxValueType.String, 10, true);
                 * */
            }
            return result;
        }


        public override  void LoadFromXml(string xml,XmlNode node)
        {
            /*
            agencyCompanyRoleId  = KxXmlUtils.GetNodeValue(node,"agencycompanyroleid",0);
             */
            enquiryStatusId  = KxXmlUtils.GetNodeValue(node,"enquirystatusid",0);
            /*
            newEventModuleMask  = KxXmlUtils.GetNodeValue(node,"neweventmodulemask","");
            singleDayModuleArrivalDateMask  = KxXmlUtils.GetNodeValue(node,"singledaymodulearrivaldatemask","");
            singleDayModuleStartDateMask  = KxXmlUtils.GetNodeValue(node,"singledaymodulestartdatemask","");
            singleDayModuleEndDateMask  = KxXmlUtils.GetNodeValue(node,"singledaymoduleenddatemask","");
            multiDayModuleArrivalDateMask  = KxXmlUtils.GetNodeValue(node,"multidaymodulearrivaldatemask","");
            multiDayModuleStartDateMask  = KxXmlUtils.GetNodeValue(node,"multidaymodulestartdatemask","");
            multiDayModuleEndDateMask  = KxXmlUtils.GetNodeValue(node,"multidaymoduleenddatemask","");
            startWeek  = KxXmlUtils.GetNodeValueDT(node,"startweek",null);
            startDay  = KxXmlUtils.GetNodeValue(node,"startday",0);
            startMonth  = KxXmlUtils.GetNodeValue(node,"startmonth",0);
            rollForward  = KxXmlUtils.GetNodeValue(node,"rollforward",false);
            maxUsers  = KxXmlUtils.GetNodeValue(node,"maxusers","");
            expiryDate  = KxXmlUtils.GetNodeValue(node,"expirydate","");
            notificationPeriod  = KxXmlUtils.GetNodeValue(node,"notificationperiod",0);
			*/ 
            defaultCompanyRoleId  = KxXmlUtils.GetNodeValue(node,"defaultcompanyroleid",0);
			/*
            defaultContactRoleId  = KxXmlUtils.GetNodeValue(node,"defaultcontactroleid",0);
            defaultCompanyRoleIcon  = KxXmlUtils.GetNodeValue(node,"defaultcompanyroleicon",(Image)null);
            defaultContactRoleIcon  = KxXmlUtils.GetNodeValue(node,"defaultcontactroleicon",(Image)null);
            defaultStatusIcon  = KxXmlUtils.GetNodeValue(node,"defaultstatusicon",(Image)null);
            dimensionUnits  = KxXmlUtils.GetNodeValue(node,"dimensionunits","");
            defaultBusinessTypeId  = KxXmlUtils.GetNodeValue(node,"defaultbusinesstypeid",0);
            courseBusinessTypeId  = KxXmlUtils.GetNodeValue(node,"coursebusinesstypeid",0);
            selectedEventColour  = KxXmlUtils.GetNodeValue(node,"selectedeventcolour","");
            confRoomPlanAMColour  = KxXmlUtils.GetNodeValue(node,"confroomplanamcolour","");
            confRoomPlanPMColour  = KxXmlUtils.GetNodeValue(node,"confroomplanpmcolour","");
            confRoomPlanEveColour  = KxXmlUtils.GetNodeValue(node,"confroomplanevecolour","");
            defaultActionTypeId  = KxXmlUtils.GetNodeValue(node,"defaultactiontypeid",0);
            defaultNewActionDays  = KxXmlUtils.GetNodeValue(node,"defaultnewactiondays",0);
            customEventTypeId  = KxXmlUtils.GetNodeValue(node,"customeventtypeid",0);
            maxCapacity  = KxXmlUtils.GetNodeValue(node,"maxcapacity",0);
            delegateCancelChargeTypeId  = KxXmlUtils.GetNodeValue(node,"delegatecancelchargetypeid",0);
            delegateTransferChargeTypeId  = KxXmlUtils.GetNodeValue(node,"delegatetransferchargetypeid",0);
            defaultPersonStatusId  = KxXmlUtils.GetNodeValue(node,"defaultpersonstatusid",0);
            confirmedPersonStatusId  = KxXmlUtils.GetNodeValue(node,"confirmedpersonstatusid",0);
            exemptVATId  = KxXmlUtils.GetNodeValue(node,"exemptvatid",0);
            */ 
            defaultCompanyStatusId  = KxXmlUtils.GetNodeValue(node,"defaultcompanystatusid",0);
            /*
            trainerNotesPassword  = KxXmlUtils.GetNodeValue(node,"trainernotespassword","");
            defaultTrainerStatus  = KxXmlUtils.GetNodeValue(node,"defaulttrainerstatus",0);
            workPersonAddressTypeId  = KxXmlUtils.GetNodeValue(node,"workpersonaddresstypeid",0);
            invoiceBookingContactTypeId  = KxXmlUtils.GetNodeValue(node,"invoicebookingcontacttypeid",0);
            defaultBookingContactTypeId  = KxXmlUtils.GetNodeValue(node,"defaultbookingcontacttypeid",0);
             */
            defaultEnquirySourceId  = KxXmlUtils.GetNodeValue(node,"defaultenquirysourceid",0);
            /*
            vatNumber  = KxXmlUtils.GetNodeValue(node,"vatnumber","");
            telephoneChargeTypeId  = KxXmlUtils.GetNodeValue(node,"telephonechargetypeid",0);
            callTimer  = KxXmlUtils.GetNodeValue(node,"calltimer",0);
            invoicePrintCopies  = KxXmlUtils.GetNodeValue(node,"invoiceprintcopies",0);
            */
            deadCompanyStatusId  = KxXmlUtils.GetNodeValue(node,"deadcompanystatusid",0);
            /*
            lastLoggerTime  = KxXmlUtils.GetNodeValueDT(node,"lastloggertime",null);
            lastLoggerRecord  = KxXmlUtils.GetNodeValue(node,"lastloggerrecord",0);
            delegateDepositChargeTypeId  = KxXmlUtils.GetNodeValue(node,"delegatedepositchargetypeid",0);
            delegateStandardChargeTypeId  = KxXmlUtils.GetNodeValue(node,"delegatestandardchargetypeid",0);
            wordTypeFollowUpMailingsId  = KxXmlUtils.GetNodeValue(node,"wordtypefollowupmailingsid",0);
            wordTypeActionsId  = KxXmlUtils.GetNodeValue(node,"wordtypeactionsid",0);
            wordTypeDelegatesId  = KxXmlUtils.GetNodeValue(node,"wordtypedelegatesid",0);
            wordTypeEnquiriesId  = KxXmlUtils.GetNodeValue(node,"wordtypeenquiriesid",0);
			 */
            defaultPersonRoleId  = KxXmlUtils.GetNodeValue(node,"defaultpersonroleid",0);
			/*
            showroundActionTypeId  = KxXmlUtils.GetNodeValue(node,"showroundactiontypeid",0);
            enhancedCapacity  = KxXmlUtils.GetNodeValue(node,"enhancedcapacity",0);
            */
            defaultEventStatusId  = KxXmlUtils.GetNodeValue(node,"defaulteventstatusid",0);
            defaultEventTypeId  = KxXmlUtils.GetNodeValue(node,"defaulteventtypeid",0);
            confirmedRoomBookingStatusId  = KxXmlUtils.GetNodeValue(node,"confirmedroombookingstatusid",0);
            provisionalRoomBookingStatusId  = KxXmlUtils.GetNodeValue(node,"provisionalroombookingstatusid",0);
            /*
            labelFieldDelimiter  = KxXmlUtils.GetNodeValue(node,"labelfielddelimiter","");
            labelRecordDelimiter  = KxXmlUtils.GetNodeValue(node,"labelrecorddelimiter","");
            labelDataSourceFile  = KxXmlUtils.GetNodeValue(node,"labeldatasourcefile","");
            labelTemplateFile  = KxXmlUtils.GetNodeValue(node,"labeltemplatefile","");
            arrivalCoffeeCateringTypeId  = KxXmlUtils.GetNodeValue(node,"arrivalcoffeecateringtypeid",0);
            morningCoffeeCateringTypeId  = KxXmlUtils.GetNodeValue(node,"morningcoffeecateringtypeid",0);
            lunchCateringTypeId  = KxXmlUtils.GetNodeValue(node,"lunchcateringtypeid",0);
            teaCateringTypeId  = KxXmlUtils.GetNodeValue(node,"teacateringtypeid",0);
             */
            defaultBookingStartTime  = KxXmlUtils.GetNodeValueDT(node,"defaultbookingstarttime",null);
            defaultBookingEndTime  = KxXmlUtils.GetNodeValueDT(node,"defaultbookingendtime",null);
            defaultBookingArrivalTime  = KxXmlUtils.GetNodeValueDT(node,"defaultbookingarrivaltime",null);
            defaultBookingDepartureTime  = KxXmlUtils.GetNodeValueDT(node,"defaultbookingdeparturetime",null);
            turnDownStatusId  = KxXmlUtils.GetNodeValue(node,"turndownstatusid",0);
			/*
			requestStatusId  = KxXmlUtils.GetNodeValue(node,"requeststatusid",0);
			requestDeclinedStatusId  = KxXmlUtils.GetNodeValue(node,"requestdeclinedstatusid",0);
			consolidatedInvoices  = KxXmlUtils.GetNodeValue(node,"consolidatedinvoices",false);
			invoiceAgeingDays  = KxXmlUtils.GetNodeValue(node,"invoiceageingdays",0);
			agentPersonRoleId  = KxXmlUtils.GetNodeValue(node,"agentpersonroleid",0);
			 * */
            defaultEventSalesTypeId  = KxXmlUtils.GetNodeValue(node,"defaulteventsalestypeid",0);
            cancellationStatusId  = KxXmlUtils.GetNodeValue(node,"cancellationstatusid",0);
			/*
			providerAddress  = KxXmlUtils.GetNodeValue(node,"provideraddress","");
			providerPort  = KxXmlUtils.GetNodeValue(node,"providerport",0);
			providerDLLName  = KxXmlUtils.GetNodeValue(node,"providerdllname","");
			postcodeDLLSource  = KxXmlUtils.GetNodeValue(node,"postcodedllsource","");
			postcodeINISource  = KxXmlUtils.GetNodeValue(node,"postcodeinisource","");
			wordTypeBookingsId  = KxXmlUtils.GetNodeValue(node,"wordtypebookingsid",0);
			cateringcoloursInternal  = KxXmlUtils.GetNodeValue(node,"cateringcoloursinternal","");
			cateringcoloursExternal  = KxXmlUtils.GetNodeValue(node,"cateringcoloursexternal","");
			cateringcoloursExtVIP  = KxXmlUtils.GetNodeValue(node,"cateringcoloursextvip","");
			cateringcoloursIntVIP  = KxXmlUtils.GetNodeValue(node,"cateringcoloursintvip","");
			cateringcoloursCharged  = KxXmlUtils.GetNodeValue(node,"cateringcolourscharged","");
			duplexEnabled  = KxXmlUtils.GetNodeValue(node,"duplexenabled",false);
			depositNominalId  = KxXmlUtils.GetNodeValue(node,"depositnominalid",0);
			debtorControlNominalId  = KxXmlUtils.GetNodeValue(node,"debtorcontrolnominalid",0);
			provisionalWarningDays  = KxXmlUtils.GetNodeValue(node,"provisionalwarningdays",0);
			delegatePeopleTypeId  = KxXmlUtils.GetNodeValue(node,"delegatepeopletypeid",0);
			outofServiceColour  = KxXmlUtils.GetNodeValue(node,"outofservicecolour","");
			meetingColour  = KxXmlUtils.GetNodeValue(node,"meetingcolour","");
			invoiceCompanyRoleId  = KxXmlUtils.GetNodeValue(node,"invoicecompanyroleid",0);
			providerMechanism  = KxXmlUtils.GetNodeValue(node,"providermechanism","");
			requestCancelId  = KxXmlUtils.GetNodeValue(node,"requestcancelid",0);
			callLoggerHaltTime  = KxXmlUtils.GetNodeValue(node,"callloggerhalttime",0);
			tourColour  = KxXmlUtils.GetNodeValue(node,"tourcolour","");
			minConferenceRoomPeriod  = KxXmlUtils.GetNodeValue(node,"minconferenceroomperiod",0.0f);
			timetableRptRoomTimes  = KxXmlUtils.GetNodeValue(node,"timetablerptroomtimes",0);
			defaultChargeToId  = KxXmlUtils.GetNodeValue(node,"defaultchargetoid",0);
			defaultAssessmentTypeId  = KxXmlUtils.GetNodeValue(node,"defaultassessmenttypeid",0);
			walkInEventTypeId  = KxXmlUtils.GetNodeValue(node,"walkineventtypeid",0);
			creditNoteMinimumChar  = KxXmlUtils.GetNodeValue(node,"creditnoteminimumchar",0);
			cateringDeliveryFacilityId  = KxXmlUtils.GetNodeValue(node,"cateringdeliveryfacilityid",0);
			customerDialogueActionTypeId  = KxXmlUtils.GetNodeValue(node,"customerdialogueactiontypeid",0);
			tickerText  = KxXmlUtils.GetNodeValue(node,"tickertext","");
			rateCalculationType  = KxXmlUtils.GetNodeValue(node,"ratecalculationtype","");
			cateringMinimumOrder  = KxXmlUtils.GetNodeValue(node,"cateringminimumorder",(decimal)0.00M);
			depositMethod  = KxXmlUtils.GetNodeValue(node,"depositmethod","");
			rateCalcNumberVariance  = KxXmlUtils.GetNodeValue(node,"ratecalcnumbervariance",0);
			userName  = KxXmlUtils.GetNodeValue(node,"username","");
			userAddress  = KxXmlUtils.GetNodeValue(node,"useraddress","");
			userBankAccount  = KxXmlUtils.GetNodeValue(node,"userbankaccount","");
			userBankSortCode  = KxXmlUtils.GetNodeValue(node,"userbanksortcode","");
			userTelephone  = KxXmlUtils.GetNodeValue(node,"usertelephone","");
			userInvoiceMessage  = KxXmlUtils.GetNodeValue(node,"userinvoicemessage","");
			userTermsText  = KxXmlUtils.GetNodeValue(node,"usertermstext","");
			autoScripts  = KxXmlUtils.GetNodeValue(node,"autoscripts",false);
			unavailableColour  = KxXmlUtils.GetNodeValue(node,"unavailablecolour","");
			nominalCodeSeparator  = KxXmlUtils.GetNodeValue(node,"nominalcodeseparator","");
			dropVATPartPenny  = KxXmlUtils.GetNodeValue(node,"dropvatpartpenny",false);
			vatlineBased  = KxXmlUtils.GetNodeValue(node,"vatlinebased",false);
			availabilityScreen  = KxXmlUtils.GetNodeValue(node,"availabilityscreen","");
			cancellationTermsNotesId  = KxXmlUtils.GetNodeValue(node,"cancellationtermsnotesid",0);
			fileLocation  = KxXmlUtils.GetNodeValue(node,"filelocation","");
			defaultActionPriorityId  = KxXmlUtils.GetNodeValue(node,"defaultactionpriorityid",0);
			checkAvailabilityFirst  = KxXmlUtils.GetNodeValue(node,"checkavailabilityfirst",false);
			defaultWalkInTitle  = KxXmlUtils.GetNodeValue(node,"defaultwalkintitle","");
			tableBookingTypeId  = KxXmlUtils.GetNodeValue(node,"tablebookingtypeid",0);
			newspapers  = KxXmlUtils.GetNodeValue(node,"newspapers",false);
			simpleMAPI  = KxXmlUtils.GetNodeValue(node,"simplemapi",0);
			studentEventBusinessTypeId  = KxXmlUtils.GetNodeValue(node,"studenteventbusinesstypeid",0);
			defaultEventTemplateId  = KxXmlUtils.GetNodeValue(node,"defaulteventtemplateid",0);
			surNameFirst  = KxXmlUtils.GetNodeValue(node,"surnamefirst",false);
			postAfterInvoice  = KxXmlUtils.GetNodeValue(node,"postafterinvoice",false);
			invoiceSortOrder  = KxXmlUtils.GetNodeValue(node,"invoicesortorder",0);
			personConfirmedStatusLevel  = KxXmlUtils.GetNodeValue(node,"personconfirmedstatuslevel",0);
			 */ 
			eventConfirmedStatusLevel  = KxXmlUtils.GetNodeValue(node,"eventconfirmedstatuslevel",0);
			/*
			defaultTableBookingEventTypeId  = KxXmlUtils.GetNodeValue(node,"defaulttablebookingeventtypeid",0);
			liveDatabaseName  = KxXmlUtils.GetNodeValue(node,"livedatabasename","");
			eft_Guid  = KxXmlUtils.GetNodeValue(node,"eft_guid","");
			eft_AccountNumber  = KxXmlUtils.GetNodeValue(node,"eft_accountnumber","");
			tableBookingBusinessTypeId  = KxXmlUtils.GetNodeValue(node,"tablebookingbusinesstypeid",0);
			eft_PrimaryIP  = KxXmlUtils.GetNodeValue(node,"eft_primaryip","");
			eft_SecondaryIP  = KxXmlUtils.GetNodeValue(node,"eft_secondaryip","");
			captureCardDetailsOnPayment  = KxXmlUtils.GetNodeValue(node,"capturecarddetailsonpayment",false);
			multiInternalAccountCodes  = KxXmlUtils.GetNodeValue(node,"multiinternalaccountcodes",false);
			alwaysCreditPostingReversals  = KxXmlUtils.GetNodeValue(node,"alwayscreditpostingreversals",false);
			internalCateringPostingNumbers  = KxXmlUtils.GetNodeValue(node,"internalcateringpostingnumbers",false);
			forceReservationMatch  = KxXmlUtils.GetNodeValue(node,"forcereservationmatch",false);
			useEmailTemplates  = KxXmlUtils.GetNodeValue(node,"useemailtemplates",false);
			breakfastBookingType  = KxXmlUtils.GetNodeValue(node,"breakfastbookingtype",0);
			dinnerBookingType  = KxXmlUtils.GetNodeValue(node,"dinnerbookingtype",0);
			eventSearchUseUserSiteId  = KxXmlUtils.GetNodeValue(node,"eventsearchuseusersiteid",false);
			checkinCutoffDays  = KxXmlUtils.GetNodeValue(node,"checkincutoffdays",0);
			checkInCutoffDaysFromDeparture  = KxXmlUtils.GetNodeValue(node,"checkincutoffdaysfromdeparture",0);
			checkoutDaysPrior  = KxXmlUtils.GetNodeValue(node,"checkoutdaysprior",0);
			cateringColoursWalkIn  = KxXmlUtils.GetNodeValue(node,"cateringcolourswalkin","");
			floorDimensionUnits  = KxXmlUtils.GetNodeValue(node,"floordimensionunits","");
			 * */
        }

        private int GetTimeAsMinutes(DateTime? dte)
        {
            int result = 0;

            try
            {
                result = (dte.Value.Hour * 60) + dte.Value.Minute;

            }
            catch { }


            return result;
        }
    }
}

