using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;

namespace Kx.Objects.Common
{
    [Serializable]
    public class KxEvents : KxBaseClass
    {
        #region Attributes & Properties

        protected int      eventTypeId;
        protected int      enquiryId;
        protected int      siteId;
        protected DateTime ?bookingDate;
        protected DateTime ?confirmedDate;
        protected int      statusId;
        protected string   eventTitle;
        protected string   shortEventTitle;
        protected DateTime ?arrivalTime;
        protected DateTime ?eventStart;
        protected DateTime ?eventEnd;
        protected DateTime ?creationDate;
        protected int      creationUserId;
        protected int      maxCapacity;
        protected bool     modular;
        protected bool     chargeToCompany;
        protected int      reservationCodeId;
        protected int      eventTemplateId;
        protected int      matrixId;
        protected decimal  amount;
        protected bool     chargePerModule;
        protected bool     chargePerDelegate;
        protected int      defaultDelegateStatusId;
        protected int      companyGroupId;
        protected int      companyId;
        protected int      companyAddressId;
        protected int      contactPersonId;
        protected bool     addDelegatesToFutureModules;

        public int      EventId
        {
            get
            { return base.dBIdentifier;}
        }

        public int      EventTypeId
        {
            get
            { return eventTypeId;}
            set
            { 
                AddToAudit("EventTypeId", eventTypeId, value);
                eventTypeId = value;
            }
        }

        public int      EnquiryId
        {
            get
            { return enquiryId;}
            set
            { 
                AddToAudit("EnquiryId", enquiryId, value);
                enquiryId = value;
            }
        }

        public int      SiteId
        {
            get
            { return siteId;}
            set
            { 
                AddToAudit("SiteId", siteId, value);
                siteId = value;
            }
        }

        public DateTime ?BookingDate
        {
            get
            { return bookingDate;}
            set
            { 
                AddToAudit("BookingDate", bookingDate, value);
                bookingDate = value;
            }
        }

        public DateTime ?ConfirmedDate
        {
            get
            { return confirmedDate;}
            set
            { 
                AddToAudit("ConfirmedDate", confirmedDate, value);
                confirmedDate = value;
            }
        }

        public int      StatusId
        {
            get
            { return statusId;}
            set
            { 
                AddToAudit("StatusId", statusId, value);
                statusId = value;
            }
        }

        public string   EventTitle
        {
            get
            { return eventTitle;}
            set
            { 
                AddToAudit("EventTitle", eventTitle, value);
                eventTitle = value;
            }
        }

        public string   ShortEventTitle
        {
            get
            { return shortEventTitle;}
            set
            { 
                AddToAudit("ShortEventTitle", shortEventTitle, value);
                shortEventTitle = value;
            }
        }

        public DateTime ?ArrivalTime
        {
            get
            { return arrivalTime;}
            set
            { 
                AddToAudit("ArrivalTime", arrivalTime, value);
                arrivalTime = value;
            }
        }

        public DateTime ?EventStart
        {
            get
            { return eventStart;}
            set
            { 
                AddToAudit("EventStart", eventStart, value);
                eventStart = value;
            }
        }

        public DateTime ?EventEnd
        {
            get
            { return eventEnd;}
            set
            { 
                AddToAudit("EventEnd", eventEnd, value);
                eventEnd = value;
            }
        }

        public DateTime ?CreationDate
        {
            get
            { return creationDate;}
            set
            { 
                AddToAudit("CreationDate", creationDate, value);
                creationDate = value;
            }
        }

        public int      CreationUserId
        {
            get
            { return creationUserId;}
            set
            { 
                AddToAudit("CreationUserId", creationUserId, value);
                creationUserId = value;
            }
        }

        public int      MaxCapacity
        {
            get
            { return maxCapacity;}
            set
            { 
                AddToAudit("MaxCapacity", maxCapacity, value);
                maxCapacity = value;
            }
        }

        public bool     Modular
        {
            get
            { return modular;}
            set
            { 
                AddToAudit("Modular", modular, value);
                modular = value;
            }
        }

        public bool     ChargeToCompany
        {
            get
            { return chargeToCompany;}
            set
            { 
                AddToAudit("ChargeToCompany", chargeToCompany, value);
                chargeToCompany = value;
            }
        }

        public int      ReservationCodeId
        {
            get
            { return reservationCodeId;}
            set
            { 
                AddToAudit("ReservationCodeId", reservationCodeId, value);
                reservationCodeId = value;
            }
        }

        public int      EventTemplateId
        {
            get
            { return eventTemplateId;}
            set
            { 
                AddToAudit("EventTemplateId", eventTemplateId, value);
                eventTemplateId = value;
            }
        }

        public int      MatrixId
        {
            get
            { return matrixId;}
            set
            { 
                AddToAudit("MatrixId", matrixId, value);
                matrixId = value;
            }
        }

        public decimal  Amount
        {
            get
            { return amount;}
            set
            { 
                AddToAudit("Amount", amount, value);
                amount = value;
            }
        }

        public bool     ChargePerModule
        {
            get
            { return chargePerModule;}
            set
            { 
                AddToAudit("ChargePerModule", chargePerModule, value);
                chargePerModule = value;
            }
        }

        public bool     ChargePerDelegate
        {
            get
            { return chargePerDelegate;}
            set
            { 
                AddToAudit("ChargePerDelegate", chargePerDelegate, value);
                chargePerDelegate = value;
            }
        }

        public int      DefaultDelegateStatusId
        {
            get
            { return defaultDelegateStatusId;}
            set
            { 
                AddToAudit("DefaultDelegateStatusId", defaultDelegateStatusId, value);
                defaultDelegateStatusId = value;
            }
        }

        public int      CompanyGroupId
        {
            get
            { return companyGroupId;}
            set
            { 
                AddToAudit("CompanyGroupId", companyGroupId, value);
                companyGroupId = value;
            }
        }

        public int      CompanyId
        {
            get
            { return companyId;}
            set
            { 
                AddToAudit("CompanyId", companyId, value);
                companyId = value;
            }
        }

        public int      CompanyAddressId
        {
            get
            { return companyAddressId;}
            set
            { 
                AddToAudit("CompanyAddressId", companyAddressId, value);
                companyAddressId = value;
            }
        }

        public int      ContactPersonId
        {
            get
            { return contactPersonId;}
            set
            { 
                AddToAudit("ContactPersonId", contactPersonId, value);
                contactPersonId = value;
            }
        }

        public bool     AddDelegatesToFutureModules
        {
            get
            { return addDelegatesToFutureModules;}
            set
            { 
                AddToAudit("AddDelegatesToFutureModules", addDelegatesToFutureModules, value);
                addDelegatesToFutureModules = value;
            }
        }


        #endregion 


        public KxEvents(KxBaseClass parent):base(parent)
        {}


        public KxEvents(KxBaseClass parent, XmlNode node):base(parent)
        {
             LoadFromXml("", node);
        }

        public KxEvents(KxBaseClass parent,int      eventid):base(parent)
        {
             base.dBIdentifier = eventid;
             Load();
        }


        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("events", "Events", TargetType.Table);

            result.AutoKeyField = new KxDataValue("EventId",EventId,KxValueType.Int);


            if ((actiontype == ActionType.Update) && (EventId == 0))
            {
                result.Action = ActionType.Insert;
            }
            else
            {
                result.Action = actiontype;
            }

            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {
                   result.AddParameter("enquiryId", enquiryId, KxValueType.Int, true);
                   result.AddParameter("siteId", siteId, KxValueType.Int, true);
                   result.AddParameter("bookingDate", bookingDate, KxValueType.DateTime, true);
                   result.AddParameter("confirmedDate", confirmedDate, KxValueType.DateTime, true);
                   result.AddParameter("statusId", statusId, KxValueType.Int, true);
                   result.AddParameter("eventTitle", eventTitle, KxValueType.String, 100, true);
                   result.AddParameter("shortEventTitle", shortEventTitle, KxValueType.String, 5, true);
                   result.AddParameter("arrivalTime", arrivalTime, KxValueType.DateTime, true);
                   result.AddParameter("eventStart", eventStart, KxValueType.DateTime, true);
                   result.AddParameter("eventEnd", eventEnd, KxValueType.DateTime, true);
                   result.AddParameter("creationDate", creationDate, KxValueType.DateTime, false);
                   result.AddParameter("creationUserId", creationUserId, KxValueType.Int, false);
                   result.AddParameter("maxCapacity", maxCapacity, KxValueType.Int, true);
                   result.AddParameter("modular", modular, KxValueType.Boolean, true);
                   result.AddParameter("chargeToCompany", chargeToCompany, KxValueType.Boolean, true);
                   result.AddParameter("reservationCodeId", reservationCodeId, KxValueType.Int, true);
                   result.AddParameter("eventTemplateId", eventTemplateId, KxValueType.Int, true);
                   result.AddParameter("matrixId", matrixId, KxValueType.Int, true);
                   result.AddParameter("amount", amount, KxValueType.Decimal, true);
                   result.AddParameter("chargePerModule", chargePerModule, KxValueType.Boolean, true);
                   result.AddParameter("chargePerDelegate", chargePerDelegate, KxValueType.Boolean, true);
                   result.AddParameter("defaultDelegateStatusId", defaultDelegateStatusId, KxValueType.Int, true);
                   result.AddParameter("companyGroupId", companyGroupId, KxValueType.Int, true);
                   result.AddParameter("companyId", companyId, KxValueType.Int, true);
                   result.AddParameter("companyAddressId", companyAddressId, KxValueType.Int, true);
                   result.AddParameter("contactPersonId", contactPersonId, KxValueType.Int, true);
                   result.AddParameter("addDelegatesToFutureModules", addDelegatesToFutureModules, KxValueType.Boolean, true);
            }
            return result;
        }


        public override  void LoadFromXml(string xml,XmlNode node)
        {
            dBIdentifier  = KxXmlUtils.GetNodeValue(node,"eventid",0);
            eventTypeId  = KxXmlUtils.GetNodeValue(node,"eventtypeid",0);
            enquiryId  = KxXmlUtils.GetNodeValue(node,"enquiryid",0);
            siteId  = KxXmlUtils.GetNodeValue(node,"siteid",0);
            bookingDate  = KxXmlUtils.GetNodeValueDT(node,"bookingdate",null);
            confirmedDate  = KxXmlUtils.GetNodeValueDT(node,"confirmeddate",null);
            statusId  = KxXmlUtils.GetNodeValue(node,"statusid",0);
            eventTitle  = KxXmlUtils.GetNodeValue(node,"eventtitle","");
            shortEventTitle  = KxXmlUtils.GetNodeValue(node,"shorteventtitle","");
            arrivalTime  = KxXmlUtils.GetNodeValueDT(node,"arrivaltime",null);
            eventStart  = KxXmlUtils.GetNodeValueDT(node,"eventstart",null);
            eventEnd  = KxXmlUtils.GetNodeValueDT(node,"eventend",null);
            creationDate  = KxXmlUtils.GetNodeValueDT(node,"creationdate",null);
            creationUserId  = KxXmlUtils.GetNodeValue(node,"creationuserid",0);
            maxCapacity  = KxXmlUtils.GetNodeValue(node,"maxcapacity",0);
            modular  = KxXmlUtils.GetNodeValue(node,"modular",false);
            chargeToCompany  = KxXmlUtils.GetNodeValue(node,"chargetocompany",false);
            reservationCodeId  = KxXmlUtils.GetNodeValue(node,"reservationcodeid",0);
            eventTemplateId  = KxXmlUtils.GetNodeValue(node,"eventtemplateid",0);
            matrixId  = KxXmlUtils.GetNodeValue(node,"matrixid",0);
            amount  = KxXmlUtils.GetNodeValue(node,"amount",(decimal)0.00M);
            chargePerModule  = KxXmlUtils.GetNodeValue(node,"chargepermodule",false);
            chargePerDelegate  = KxXmlUtils.GetNodeValue(node,"chargeperdelegate",false);
            defaultDelegateStatusId  = KxXmlUtils.GetNodeValue(node,"defaultdelegatestatusid",0);
            companyGroupId  = KxXmlUtils.GetNodeValue(node,"companygroupid",0);
            companyId  = KxXmlUtils.GetNodeValue(node,"companyid",0);
            companyAddressId  = KxXmlUtils.GetNodeValue(node,"companyaddressid",0);
            contactPersonId  = KxXmlUtils.GetNodeValue(node,"contactpersonid",0);
            addDelegatesToFutureModules  = KxXmlUtils.GetNodeValue(node,"adddelegatestofuturemodules",false);
        }
    }
}
