using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Exceptions;
using Kx.Core.Utility;

namespace Kx.Objects.Common
{
    public class KxVATs : KxBaseClass
    {
        #region Fields and Attributes
        int _taxModelID = 0;
        private List<KxVAT> items = new List<KxVAT>();

        public KxVAT this[int index]
        {
            get
            { return items[index]; }
            set
            { items[index] = value; }
        }

        public List<KxVAT> Items
        {
            get
            { return items; }
        }
        #endregion

        #region Methods

        public override bool LoadFromXml(string xml, string xpath)
        {
            items.Clear();
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xml);

                XmlNodeList nodes = doc.SelectNodes(@"//results/" + xpath + "/datarow");

                foreach (XmlNode node in nodes)
                {
                    if (node != null)
                    {
                        KxVAT cf = new KxVAT(this, node);
                        items.Add(cf);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "LoadFromXml", ex.Message, xpath);
                LastError = ex.Message;
                return false;
            }
        }

        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = null;

            if (actiontype == ActionType.Select)
            {
                result = new KxData("vat", "VAT", TargetType.Table);

                if (_taxModelID != 0)
                {
                    result.AddKeyField("taxmodelid", _taxModelID, KxValueType.Int);
                }
            }

            return result;
        }
        #endregion

        #region Constructors
        public KxVATs(KxBaseClass parent)
            : base(parent)
        {
            Load();
        }

        public KxVATs(KxBaseClass parent, int taxModelID)
            : this (parent)
        {
            _taxModelID = taxModelID;
            Load();
        }
        #endregion
    }
}
