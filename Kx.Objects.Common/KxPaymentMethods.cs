using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Exceptions;
using Kx.Core.Utility;

namespace Kx.Objects.Common
{
    public class KxPaymentMethods : KxBaseClass
    {
        #region Fields and Attributes
        private List<KxPaymentMethod> items = new List<KxPaymentMethod>();
        private int _findPaymentMethodId;

        public KxPaymentMethod this[int index]
        {
            get
            { return items[index]; }
            set
            { items[index] = value; }
        }

        public List<KxPaymentMethod> Items
        {
            get
            { return items; }
        }
        #endregion


        #region Methods

        public KxPaymentMethod ContainsPaymentMethodByID(int paymentMethodId)
        {
            _findPaymentMethodId = paymentMethodId;
            return Items.Find(FindById);
        }

        private bool FindById(KxPaymentMethod p)
        {
            return p.PaymentMethodId == _findPaymentMethodId;
        }

        public override bool LoadFromXml(string xml, string xpath)
        {
            items.Clear();
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xml);

                XmlNodeList nodes = doc.SelectNodes(@"//results/" + xpath + "/datarow");

                foreach (XmlNode node in nodes)
                {
                    if (node != null)
                    {
                        KxPaymentMethod cf = new KxPaymentMethod(this, node);
                        items.Add(cf);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "LoadFromXml", ex.Message, xpath);
                LastError = ex.Message;
                return false;
            }
        }

        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = null;

            if (actiontype == ActionType.Select)
            {
                result = new KxData("paymentmethods", "PaymentMethods", TargetType.Table);
            }
            return result;
        }
        #endregion

        #region Constructors
        public KxPaymentMethods(KxBaseClass parent)
            : base(parent)
        {
            Load();
        }
        #endregion
    }
}
