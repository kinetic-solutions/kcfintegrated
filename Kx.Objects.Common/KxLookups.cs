using System;
using System.Collections.Generic;
using System.Text;
using Kx.Core.Data;
using System.Xml;
using Kx.Core.Exceptions;
using Kx.Core.Base;

namespace Kx.Objects.Common
{
    public class KxLookups : KxBaseClass
    {
        private LookupItemType _itemType = LookupItemType.Unknown;
        private List<KxLookup> _items = new List<KxLookup>();
        private Guid _findById;
        private bool _returnInactive = false;

        public List<KxLookup> Items
        {
            get { return _items; }
        }

        public KxLookup this[int index]
        {
            get { return _items[index]; }
            set
            {
             if (value == null)
             		return;
              _items[index] = value; 
            }
        }

        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = null;

            if (actiontype == ActionType.Select)
            {
                result = new KxData("kx2lookups", "Kx2Lookups", TargetType.Table);
                result.AddKeyField("LookupType", _itemType.ToString(), KxValueType.String);

                if (!_returnInactive)
                  result.AddKeyField("Inactive", false, KxValueType.Boolean);

                result.OrderFields.Add(new KxDataOrder("ListOrder"));
                result.OrderFields.Add(new KxDataOrder("LookupValue"));
            }

            return result;
        }

        public KxLookups(KxBaseClass parent, LookupItemType itemType)
            : base(parent)
        {
            //Call the other constructor setting the flag not to return the inactive Lookups
//          _returnInactive = false;
          _itemType = itemType;
          Load();
        }

        public KxLookups(KxBaseClass parent)
            : base(parent)
        {
            Load();
        }

        public KxLookups(KxBaseClass parent, LookupItemType itemType, bool ReturnInactive)
          : base(parent)
        {
            _returnInactive = ReturnInactive;

            _itemType = itemType;
            Load();
        }

        public KxLookup ContainsLookup (Guid identifier)
        {
            _findById = identifier;
            return Items.Find(MatchById);
        }

        private bool MatchById(KxLookup lu)
        {
            return lu.LookupId.ToString() == _findById.ToString();
        }

        public override bool LoadFromXml(string xml, string xpath)
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xml);

                XmlNodeList nodes = doc.SelectNodes(@"//results/" + xpath + "/datarow");

                foreach (XmlNode node in nodes)
                {
                    if (node != null)
                    {
                        KxLookup item;
                        //if (_itemType == LookupItemType.PropertyType)
                        //{
                        //    item = new KxLookupPropertyType(null, node);
                        //}
                        //else if (_itemType == LookupItemType.PropertyRoomType)
                        //{
                        //    item = new KxLookupPropertyRoomType(null, node);
                        //}
                        //else
                        //{
                            item = new KxLookup(null, node);
                        //}
                        _items.Add(item);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "LoadFromXml", ex.Message, xpath);
                LastError = ex.Message;
                return false;
            }
        }
    }
}
