using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Exceptions;
using Kx.Core.Utility;

namespace Kx.Objects.Common
{

    public class KxSiteBlocks : KxBaseClass
    {
        private List<KxSiteBlock> items = new List<KxSiteBlock>();

        public List<KxSiteBlock> Items
        {
            get
            { return items; }
        }

        public KxSiteBlock this[int index]
        {
            get
            { return items[index]; }
            set
            { items[index] = value; }
        }

        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = null;

            if (actiontype == ActionType.Select)
            {
                result = new KxData("siteblocks", "siteblocks", TargetType.Table, ActionType.Select);

                result.OrderFields.Add(new KxDataOrder("blockid"));
                
            }
            return result;
        }

        public int[] BlockIdArray
        {
            get
            { return GetBlockIdArray(); }
        }



        public KxSiteBlocks(KxBaseClass parent) : base(parent)
        {

        }


        public override bool LoadFromXml(string xml, string xpath)
        {
            items.Clear();
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xml);

                XmlNodeList nodes = doc.SelectNodes(@"//results/" + xpath + "/datarow");

                foreach (XmlNode node in nodes)
                {
                    if (node != null)
                    {
                        KxSiteBlock sb = new KxSiteBlock(this, node);
                        Items.Add(sb);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "LoadFromXml", ex.Message, xpath);
                LastError = ex.Message;
                return false;
            }
        }

        protected int[] GetBlockIdArray()
        {
            int loop = 0;

            int[] result = new int[Items.Count];

            foreach (KxSiteBlock sb in Items)
            {
                result[loop] = sb.BlockId;
                loop++;
            }
            return result;
        }

        public KxSiteBlock GetSiteBlockById(int id)
        {
            KxSiteBlock result = null;

            foreach (KxSiteBlock sb in Items)
            {
                if (sb.BlockId == id)
                {
                    result = sb;
                    break;
                }
            }

            if (result == null)
            {
                KxSiteBlock dummy = new KxSiteBlock(this, id);
                if (dummy.BlockId == id)
                {
                    Items.Add(dummy);
                    result = dummy;
                }
            }
            
            return result;
        }

		public KxSiteBlock GetSiteBlockByExternalCode(string externalcode)
		{
			KxSiteBlock result = null;

			foreach (KxSiteBlock sb in Items)
			{
				if (sb.ExternalCode == externalcode)
				{
					result = sb;
					break;
				}
			}

			return result;
		}
	}
}
