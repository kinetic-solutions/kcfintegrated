using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;

namespace Kx.Objects.Common
{
    [Serializable]
    public class KxCompanyAddresses : KxBaseClass
    {
        #region Attributes & Properties

        protected int companyId;
        protected int addressId;
        protected string department;
        protected string telephone;
        protected string fax;
        protected string email;
        protected string www;
        protected bool invoiceAddress;
        protected string externalAccountCode;
        protected bool active;

        public int CompanyAddressId
        {
            get
            { return base.dBIdentifier; }
        }

        public int CompanyId
        {
            get
            { return companyId; }
            set
            {
                AddToAudit("CompanyId", companyId, value);
                companyId = value;
            }
        }

        public int AddressId
        {
            get
            { return addressId; }
            set
            {
                AddToAudit("AddressId", addressId, value);
                addressId = value;
            }
        }

        public string Department
        {
            get
            { return department; }
            set
            {
                AddToAudit("Department", department, value);
                department = value;
            }
        }

        public string Telephone
        {
            get
            { return telephone; }
            set
            {
                AddToAudit("Telephone", telephone, value);
                telephone = value;
            }
        }

        public string Fax
        {
            get
            { return fax; }
            set
            {
                AddToAudit("Fax", fax, value);
                fax = value;
            }
        }

        public string Email
        {
            get
            { return email; }
            set
            {
                AddToAudit("Email", email, value);
                email = value;
            }
        }

        public string WWW
        {
            get
            { return www; }
            set
            {
                AddToAudit("Www", www, value);
                www = value;
            }
        }

        public bool InvoiceAddress
        {
            get
            { return invoiceAddress; }
            set
            {
                AddToAudit("InvoiceAddress", invoiceAddress, value);
                invoiceAddress = value;
            }
        }

        public string ExternalAccountCode
        {
            get
            { return externalAccountCode; }
            set
            {
                AddToAudit("ExternalAccountCode", externalAccountCode, value);
                externalAccountCode = value;
            }
        }

        public bool Active
        {
            get
            { return active; }
            set
            {
                AddToAudit("Active", active, value);
                active = value;
            }
        }

        #endregion

        public KxCompanyAddresses(KxBaseClass parent)
            : base(parent)
        {
        }

        public KxCompanyAddresses(KxBaseClass parent,XmlNode node)
            : base(parent)
        {
            LoadFromXml("", node);
        }

        //Stu - Added new constructor to fix auto-gen'd Code.
        public KxCompanyAddresses(KxBaseClass parent, int companyid, int addressid, string externalAccountCode)
            : base(parent)
        {
            CompanyId = companyid;
            AddressId = addressid;
            Load();
        }
        
        public KxCompanyAddresses(KxBaseClass parent, int companyaddressid)
            : base(parent)
        {
            base.dBIdentifier = companyaddressid;
            Load();
        }

        public KxCompanyAddresses(KxBaseClass parent, int loadcompanyid, int loadaddressid)
            : base(parent)
        {
            int compAddrId = GetByCompanyAndAddress(loadcompanyid, loadaddressid);
            if (compAddrId > 0)
            {
                base.dBIdentifier = compAddrId;
                Load();
            }
            Load();
        }

        public KxCompanyAddresses(KxBaseClass parent, int companyId, string externalAccountCode)
            : base(parent)
        {
            int compAddrId = GetByExternalAccountCode(companyId, externalAccountCode);
            if (compAddrId > 0)
            {
                base.dBIdentifier = compAddrId;
                Load();
            }
        }

        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("companyaddresses", "CompanyAddresses", TargetType.Table);

            //result.AutoKeyField = new KxDataValue("CompanyAddressId",CompanyAddressId,KxValueType.Int);

			if ((actiontype != ActionType.Insert) && (actiontype != ActionType.Update))
			{
				result.AddKeyField("CompanyAddressId", CompanyAddressId, KxValueType.Int);
			}

            if ((actiontype == ActionType.Update) && (CompanyAddressId == 0))
            {
                result.Action = ActionType.Insert;
            }
            else
            {
                result.Action = actiontype;
            }

            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {
                result.AddParameter("addressId", addressId, KxValueType.Int, false);
                result.AddParameter("companyId", companyId, KxValueType.Int, false);
                result.AddParameter("department", department, KxValueType.String, 255, true);
                result.AddParameter("telephone", telephone, KxValueType.String, 255, true);
                result.AddParameter("fax", fax, KxValueType.String, 255, true);
                result.AddParameter("email", email, KxValueType.String, 255, true);
                result.AddParameter("www", www, KxValueType.String, 255, true);
                result.AddParameter("invoiceAddress", invoiceAddress, KxValueType.Boolean, false);
                result.AddParameter("externalAccountCode", externalAccountCode, KxValueType.String, 255, true);
                result.AddParameter("active", active, KxValueType.Boolean, false);
            }
            return result;
        }

        public override void LoadFromXml(string xml, XmlNode node)
        {
            dBIdentifier = KxXmlUtils.GetNodeValue(node, "companyaddressid", 0);
            companyId = KxXmlUtils.GetNodeValue(node, "companyid", 0);
            addressId = KxXmlUtils.GetNodeValue(node, "addressid", 0);
            department = KxXmlUtils.GetNodeValue(node, "department", "");
            telephone = KxXmlUtils.GetNodeValue(node, "telephone", "");
            fax = KxXmlUtils.GetNodeValue(node, "fax", "");
            email = KxXmlUtils.GetNodeValue(node, "email", "");
            www = KxXmlUtils.GetNodeValue(node, "www", "");
            invoiceAddress = KxXmlUtils.GetNodeValue(node, "invoiceaddress", false);
            externalAccountCode = KxXmlUtils.GetNodeValue(node, "externalaccountcode", "");
            active = KxXmlUtils.GetNodeValue(node, "active", false);
        }

        public int GetByCompanyAndAddress(int getcompanyid, int getaddressid)
        {

            int result = 0;

            try
            {
				string sql = "SELECT CA.CompanyAddressID FROM CompanyAddresses CA WHERE CA.CompanyID = <CompanyID> AND CA.AddressID = <AddressID> AND CA.Active = 1";

				if (getaddressid == 0)
				{
					sql = "SELECT TOP 1 CompanyAddressID FROM CompanyAddresses WHERE CompanyID = <CompanyID> AND Active = 1 ORDER BY InvoiceAddress DESC, AddressID";
				}
				else
				{
					sql = sql.Replace("<AddressID>", getaddressid.ToString());
				}

                sql = sql.Replace("<CompanyID>", getcompanyid.ToString());
                
                KxData data = new KxData("companyaddresses", sql, TargetType.Static);

                object dbresult = data.ExecuteScalar();

                if (dbresult != null)
                {
                    result = Convert.ToInt32(dbresult);
                }
            }
            catch
            { }

            return result;

        }

        public int GetByExternalAccountCode(int companyid, string externalAccountCode)
        {

           int result = 0;

           try
           {
                string sql = "SELECT CA.CompanyAddressID FROM CompanyAddresses CA WHERE CA.CompanyID = <CompanyID> AND CA.ExternalAccountCode = '<ExternalAccountCode>' AND CA.Active = 1";

				sql = sql.Replace("<CompanyID>", companyid.ToString());
				sql = sql.Replace("<ExternalAccountCode>", externalAccountCode);

                KxData data = new KxData("companyaddresses", sql, TargetType.Static);

                object dbresult = data.ExecuteScalar();

                if (dbresult != null)
                {
                        result = Convert.ToInt32(dbresult);
                }
           }
           catch
           { }
           return result;
        }
    }
}
