using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;


namespace Kx.Objects.Common
{
    [Serializable]
    public class KxEventModuleDelegates : KxBaseClass
    {
        #region Attributes & Properties

        protected List<KxEventModuleDelegate> items = new List<KxEventModuleDelegate>();
        protected int eventModuleId;

        public List<KxEventModuleDelegate> Items
        {
            get
            { return items; }
        }

        public KxEventModuleDelegate this[int index]
        {
            get
            { return items[index]; }
            set
            { items[index] = value; }
        }

		public IEnumerator<KxEventModuleDelegate> GetEnumerator()
		{
			return items.GetEnumerator();
		}

		#endregion

        public KxEventModuleDelegates(KxBaseClass parent, int EventModuleId)
            : base(parent)
        {
            eventModuleId = EventModuleId;
            Load();
        }

        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = null;

            if (actiontype == ActionType.Select)
            {
                result = new KxData("eventmoduledelegates", "EventModuleDelegates", TargetType.Table);
                result.KeyFields.Add(new KxDataValue("EventModuleID", eventModuleId, KxValueType.String));
            }
            return result;
        }

        public override bool LoadFromXml(string xml, string xpath)
        {
            items.Clear();
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xml);

                XmlNodeList nodes = doc.SelectNodes(@"//results/" + xpath + "/datarow");

                foreach (XmlNode node in nodes)
                {
                    if (node != null)
                    {
                        KxEventModuleDelegate eventModuleDelegate = new KxEventModuleDelegate(this, node);
                        items.Add(eventModuleDelegate);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "LoadFromXml", ex.Message, xpath);
                LastError = ex.Message;
                return false;
            }
        }
        
    }
}
