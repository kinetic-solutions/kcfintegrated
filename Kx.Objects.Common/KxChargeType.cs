using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;

namespace Kx.Objects.Common
{
    [Serializable]
    public class KxChargeType : KxBaseClass
    {
        #region Attributes & Properties

        protected string   description;
        protected string   invoiceText1;
        protected string   invoiceText2;
        protected int      departmentId;
        protected int      chargeToId;
        protected decimal  feePerPerson;
        protected decimal  feeAmount;
        protected int      vatid;
        protected int      nominalId;
        protected string   amountType;
        protected string   chargeTypeLevel;
        protected bool     forceVAT;
        protected int      rechargeNominalId;
        protected int      rechargeVATId;
        protected int      analysisCodeId;
        protected bool     displayInRegister;
        protected string   displayAs;
        protected bool     display;
        protected string   defaultExternalReference;
        protected int      defaultSequenceNumber;
        protected int      siteId;
        protected bool     _internal;
        protected bool     inactive;
        protected string   distChargeType;
        protected int      cateringTypeId;
        protected bool     quoteCalculator;
        protected int      chargeTypeGroupId;
        protected int      taxModelId;
        protected bool     excludeFromRevenueReports;

        public int      ChargeTypeId
        {
            get
            { return base.dBIdentifier;}
        }

        public string   Description
        {
            get
            { return description;}
            set
            { 
                AddToAudit("Description", description, value);
                description = value;
            }
        }

        public string   InvoiceText1
        {
            get
            { return invoiceText1;}
            set
            { 
                AddToAudit("InvoiceText1", invoiceText1, value);
                invoiceText1 = value;
            }
        }

        public string   InvoiceText2
        {
            get
            { return invoiceText2;}
            set
            { 
                AddToAudit("InvoiceText2", invoiceText2, value);
                invoiceText2 = value;
            }
        }

        public int      DepartmentId
        {
            get
            { return departmentId;}
            set
            { 
                AddToAudit("DepartmentId", departmentId, value);
                departmentId = value;
            }
        }

        public int      ChargeToId
        {
            get
            { return chargeToId;}
            set
            { 
                AddToAudit("ChargeToId", chargeToId, value);
                chargeToId = value;
            }
        }

        public decimal  FeePerPerson
        {
            get
            { return feePerPerson;}
            set
            { 
                AddToAudit("FeePerPerson", feePerPerson, value);
                feePerPerson = value;
            }
        }

        public decimal  FeeAmount
        {
            get
            { return feeAmount;}
            set
            { 
                AddToAudit("FeeAmount", feeAmount, value);
                feeAmount = value;
            }
        }

        public int      VatId
        {
            get
            { return vatid;}
            set
            { 
                AddToAudit("VatId", vatid, value);
                vatid = value;
            }
        }

        public int      NominalId
        {
            get
            { return nominalId;}
            set
            { 
                AddToAudit("NominalId", nominalId, value);
                nominalId = value;
            }
        }

        public string   AmountType
        {
            get
            { return amountType;}
            set
            { 
                AddToAudit("AmountType", amountType, value);
                amountType = value;
            }
        }

        public string   ChargeTypeLevel
        {
            get
            { return chargeTypeLevel;}
            set
            { 
                AddToAudit("ChargeTypeLevel", chargeTypeLevel, value);
                chargeTypeLevel = value;
            }
        }

        public bool     ForceVAT
        {
            get
            { return forceVAT;}
            set
            { 
                AddToAudit("ForceVAT", forceVAT, value);
                forceVAT = value;
            }
        }

        public int      RechargeNominalId
        {
            get
            { return rechargeNominalId;}
            set
            { 
                AddToAudit("RechargeNominalId", rechargeNominalId, value);
                rechargeNominalId = value;
            }
        }

        public int      RechargeVATId
        {
            get
            { return rechargeVATId;}
            set
            { 
                AddToAudit("RechargeVATId", rechargeVATId, value);
                rechargeVATId = value;
            }
        }

        public int      AnalysisCodeId
        {
            get
            { return analysisCodeId;}
            set
            { 
                AddToAudit("AnalysisCodeId", analysisCodeId, value);
                analysisCodeId = value;
            }
        }

        public bool     DisplayInRegister
        {
            get
            { return displayInRegister;}
            set
            { 
                AddToAudit("DisplayInRegister", displayInRegister, value);
                displayInRegister = value;
            }
        }

        public string   DisplayAs
        {
            get
            { return displayAs;}
            set
            { 
                AddToAudit("DisplayAs", displayAs, value);
                displayAs = value;
            }
        }

        public bool     Display
        {
            get
            { return display;}
            set
            { 
                AddToAudit("Display", display, value);
                display = value;
            }
        }

        public string   DefaultExternalReference
        {
            get
            { return defaultExternalReference;}
            set
            { 
                AddToAudit("DefaultExternalReference", defaultExternalReference, value);
                defaultExternalReference = value;
            }
        }

        public int      DefaultSequenceNumber
        {
            get
            { return defaultSequenceNumber;}
            set
            { 
                AddToAudit("DefaultSequenceNumber", defaultSequenceNumber, value);
                defaultSequenceNumber = value;
            }
        }

        public int      SiteId
        {
            get
            { return siteId;}
            set
            { 
                AddToAudit("SiteId", siteId, value);
                siteId = value;
            }
        }

        public bool     Internal
        {
            get
            { return _internal;}
            set
            { 
                AddToAudit("Internal", _internal, value);
                _internal = value;
            }
        }

        public bool     Inactive
        {
            get
            { return inactive;}
            set
            { 
                AddToAudit("Inactive", inactive, value);
                inactive = value;
            }
        }

        public string   DistChargeType
        {
            get
            { return distChargeType;}
            set
            { 
                AddToAudit("DistChargeType", distChargeType, value);
                distChargeType = value;
            }
        }

        public int      CateringTypeId
        {
            get
            { return cateringTypeId;}
            set
            { 
                AddToAudit("CateringTypeId", cateringTypeId, value);
                cateringTypeId = value;
            }
        }

        public bool     QuoteCalculator
        {
            get
            { return quoteCalculator;}
            set
            { 
                AddToAudit("QuoteCalculator", quoteCalculator, value);
                quoteCalculator = value;
            }
        }

        public int      ChargeTypeGroupId
        {
            get
            { return chargeTypeGroupId;}
            set
            { 
                AddToAudit("ChargeTypeGroupId", chargeTypeGroupId, value);
                chargeTypeGroupId = value;
            }
        }

        public int      TaxModelId
        {
            get
            { return taxModelId;}
            set
            { 
                AddToAudit("TaxModelId", taxModelId, value);
                taxModelId = value;
            }
        }

        public bool     ExcludeFromRevenueReports
        {
            get
            { return excludeFromRevenueReports;}
            set
            { 
                AddToAudit("ExcludeFromRevenueReports", excludeFromRevenueReports, value);
                excludeFromRevenueReports = value;
            }
        }

        private KxChargeTypeRateMatrixs _prices;
        public KxChargeTypeRateMatrixs Prices
        {
            get
            {
                if (_prices == null)
                {
                    _prices = new KxChargeTypeRateMatrixs(this, 0, base.dBIdentifier);
                }
                return _prices;
            }
        }


        #endregion 


        public KxChargeType(KxBaseClass parent):base(parent)
        {}


        public KxChargeType(KxBaseClass parent, XmlNode node):base(parent)
        {
             LoadFromXml("", node);
        }

        public KxChargeType(KxBaseClass parent,int      chargetypeid):base(parent)
        {
             base.dBIdentifier = chargetypeid;
             Load();
        }


        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("chargetypes", "ChargeTypes", TargetType.Table);

            result.AutoKeyField = new KxDataValue("ChargeTypeId",ChargeTypeId,KxValueType.Int);


            if ((actiontype == ActionType.Update) && (ChargeTypeId == 0))
            {
                result.Action = ActionType.Insert;
            }
            else
            {
                result.Action = actiontype;
            }

            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {
                   result.AddParameter("description", description, KxValueType.String, 50, false);
                   result.AddParameter("invoiceText1", invoiceText1, KxValueType.String, 255, true);
                   result.AddParameter("invoiceText2", invoiceText2, KxValueType.String, 255, true);
                   result.AddParameter("departmentId", departmentId, KxValueType.Int, true);
                   result.AddParameter("chargeToId", chargeToId, KxValueType.Int, false);
                   result.AddParameter("feePerPerson", feePerPerson, KxValueType.Decimal, false);
                   result.AddParameter("feeAmount", feeAmount, KxValueType.Decimal, false);
                   result.AddParameter("vatid", vatid, KxValueType.Int, false);
                   result.AddParameter("nominalId", nominalId, KxValueType.Int, false);
                   result.AddParameter("amountType", amountType, KxValueType.String, 1, false);
                   result.AddParameter("chargeTypeLevel", chargeTypeLevel, KxValueType.String, 1, false);
                   result.AddParameter("forceVAT", forceVAT, KxValueType.Boolean, false);
                   result.AddParameter("rechargeNominalId", rechargeNominalId, KxValueType.Int, false);
                   result.AddParameter("rechargeVATId", rechargeVATId, KxValueType.Int, false);
                   result.AddParameter("analysisCodeId", analysisCodeId, KxValueType.Int, true);
                   result.AddParameter("displayInRegister", displayInRegister, KxValueType.Boolean, false);
                   result.AddParameter("displayAs", displayAs, KxValueType.String, 20, true);
                   result.AddParameter("display", display, KxValueType.Boolean, false);
                   result.AddParameter("defaultExternalReference", defaultExternalReference, KxValueType.String, 50, true);
                   result.AddParameter("defaultSequenceNumber", defaultSequenceNumber, KxValueType.Int, false);
                   result.AddParameter("siteId", siteId, KxValueType.Int, true);
                   result.AddParameter("internal", _internal, KxValueType.Boolean, false);
                   result.AddParameter("inactive", inactive, KxValueType.Boolean, false);
                   result.AddParameter("distChargeType", distChargeType, KxValueType.String, 1, true);
                   result.AddParameter("cateringTypeId", cateringTypeId, KxValueType.Int, true);
                   result.AddParameter("quoteCalculator", quoteCalculator, KxValueType.Boolean, false);
                   result.AddParameter("chargeTypeGroupId", chargeTypeGroupId, KxValueType.Int, true);
                   result.AddParameter("taxModelId", taxModelId, KxValueType.Int, true);
                   result.AddParameter("excludeFromRevenueReports", excludeFromRevenueReports, KxValueType.Boolean, true);
            }
            return result;
        }


        public override  void LoadFromXml(string xml,XmlNode node)
        {
            dBIdentifier  = KxXmlUtils.GetNodeValue(node,"chargetypeid",0);
            description  = KxXmlUtils.GetNodeValue(node,"description","");
            invoiceText1  = KxXmlUtils.GetNodeValue(node,"invoicetext1","");
            invoiceText2  = KxXmlUtils.GetNodeValue(node,"invoicetext2","");
            departmentId  = KxXmlUtils.GetNodeValue(node,"departmentid",0);
            chargeToId  = KxXmlUtils.GetNodeValue(node,"chargetoid",0);
            feePerPerson  = KxXmlUtils.GetNodeValue(node,"feeperperson",(decimal)0.00M);
            feeAmount  = KxXmlUtils.GetNodeValue(node,"feeamount",(decimal)0.00M);
            vatid  = KxXmlUtils.GetNodeValue(node,"vatid",0);
            nominalId  = KxXmlUtils.GetNodeValue(node,"nominalid",0);
            amountType  = KxXmlUtils.GetNodeValue(node,"amounttype","");
            chargeTypeLevel  = KxXmlUtils.GetNodeValue(node,"chargetypelevel","");
            forceVAT  = KxXmlUtils.GetNodeValue(node,"forcevat",false);
            rechargeNominalId  = KxXmlUtils.GetNodeValue(node,"rechargenominalid",0);
            rechargeVATId  = KxXmlUtils.GetNodeValue(node,"rechargevatid",0);
            analysisCodeId  = KxXmlUtils.GetNodeValue(node,"analysiscodeid",0);
            displayInRegister  = KxXmlUtils.GetNodeValue(node,"displayinregister",false);
            displayAs  = KxXmlUtils.GetNodeValue(node,"displayas","");
            display  = KxXmlUtils.GetNodeValue(node,"display",false);
            defaultExternalReference  = KxXmlUtils.GetNodeValue(node,"defaultexternalreference","");
            defaultSequenceNumber  = KxXmlUtils.GetNodeValue(node,"defaultsequencenumber",0);
            siteId  = KxXmlUtils.GetNodeValue(node,"siteid",0);
            _internal  = KxXmlUtils.GetNodeValue(node,"internal",false);
            inactive  = KxXmlUtils.GetNodeValue(node,"inactive",false);
            distChargeType  = KxXmlUtils.GetNodeValue(node,"distchargetype","");
            cateringTypeId  = KxXmlUtils.GetNodeValue(node,"cateringtypeid",0);
            quoteCalculator  = KxXmlUtils.GetNodeValue(node,"quotecalculator",false);
            chargeTypeGroupId  = KxXmlUtils.GetNodeValue(node,"chargetypegroupid",0);
            taxModelId  = KxXmlUtils.GetNodeValue(node,"taxmodelid",0);
            excludeFromRevenueReports  = KxXmlUtils.GetNodeValue(node,"excludefromrevenuereports",false);
        }
    }
}

