using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;
using Kx.Objects.Common.Classes;

namespace Kx.Objects.Common
{
    [Serializable]
    public class KxChargeTypeRateMatrix : KxBaseClass
    {
        #region Attributes & Properties

        protected int      siteId;
        protected int      chargeTypeId;
        protected int      parentMatrixId;
        protected int      salesTypeId;
        protected int      companyId;
        protected int      bandId;
        protected string   externalMaterialType;
        protected string   externalDescription;
        protected decimal  rate;
        protected DateTime ?startDate;
        protected DateTime ?endDate;
        protected int      rateTypeId;
        protected int      facilityId;
        protected int      conferenceRoomId;
        protected bool     useDetailPrices;
        protected int      blockId;
        protected int      bedroomTypeId;
        protected bool     absorbsDiscount;
        protected double   splitPercent;
        protected int      daysApplied;
        protected int      eventTypeId;
        protected int      nominalId;
        protected string   externalCode;
        protected int      maxRoomOccupancy;
        protected int      companyGroupId;
        protected bool     absorbsVATRounding;

        public int      MatrixId
        {
            get
            { return base.dBIdentifier;}
        }

        public int      SiteId
        {
            get
            { return siteId;}
            set
            { 
                AddToAudit("SiteId", siteId, value);
                siteId = value;
            }
        }

        private KxSite _site;
        public KxSite Site
        {
            get
            {
                if (_site == null)
                {
                    _site = new KxSite(null, siteId);
                }
                return _site;
            }
            set
            {
                _site = value;
                if (_site != null)
                {
                    SiteId = _site.SiteId;
                }
                else
                {
                    SiteId = 0; // all sites
                }
            }
        }

        public int      ChargeTypeId
        {
            get
            { return chargeTypeId;}
            set
            { 
                AddToAudit("ChargeTypeId", chargeTypeId, value);
                chargeTypeId = value;
            }
        }

        private KxChargeType _chargeType;
        public KxChargeType ChargeType
        {
            get
            {
                if (_chargeType == null)
                {
                    _chargeType = new KxChargeType(null, chargeTypeId);
                }
                return _chargeType;
            }
            set
            {
                _chargeType = value;
                ChargeTypeId = _chargeType.ChargeTypeId;
            }
        }

        public int      ParentMatrixId
        {
            get
            { return parentMatrixId;}
            set
            { 
                AddToAudit("ParentMatrixId", parentMatrixId, value);
                parentMatrixId = value;
            }
        }

        public int      SalesTypeId
        {
            get
            { return salesTypeId;}
            set
            { 
                AddToAudit("SalesTypeId", salesTypeId, value);
                salesTypeId = value;
            }
        }

        private KxSalesType _salesType;
        public KxSalesType SalesType
        {
            get
            {
                if (_salesType == null)
                {
                    _salesType = new KxSalesType(null, salesTypeId);
                }
                return _salesType;
            }
            set
            {
                _salesType = value;
                if (_salesType != null)
                {
                    SalesTypeId = _salesType.SalesTypeId;
                }
                else
                {
                    SalesTypeId = 0; // All Sales Types
                }
            }
        }

        public int      CompanyId
        {
            get
            { return companyId;}
            set
            { 
                AddToAudit("CompanyId", companyId, value);
                companyId = value;
            }
        }

        public int      BandId
        {
            get
            { return bandId;}
            set
            { 
                AddToAudit("BandId", bandId, value);
                bandId = value;
            }
        }

        public string   ExternalMaterialType
        {
            get
            { return externalMaterialType;}
            set
            { 
                AddToAudit("ExternalMaterialType", externalMaterialType, value);
                externalMaterialType = value;
            }
        }

        public string   ExternalDescription
        {
            get
            { return externalDescription;}
            set
            { 
                AddToAudit("ExternalDescription", externalDescription, value);
                externalDescription = value;
            }
        }

        public decimal  Rate
        {
            get
            { return rate;}
            set
            { 
                AddToAudit("Rate", rate, value);
                rate = value;
            }
        }

        public DateTime ?StartDate
        {
            get
            { return startDate;}
            set
            { 
                AddToAudit("StartDate", startDate, value);
                startDate = value;
            }
        }

        public DateTime ?EndDate
        {
            get
            { return endDate;}
            set
            { 
                AddToAudit("EndDate", endDate, value);
                endDate = value;
            }
        }

        public int      RateTypeId
        {
            get
            { return rateTypeId;}
            set
            { 
                AddToAudit("RateTypeId", rateTypeId, value);
                rateTypeId = value;
            }
        }

        public int      FacilityId
        {
            get
            { return facilityId;}
            set
            { 
                AddToAudit("FacilityId", facilityId, value);
                facilityId = value;
            }
        }

        public int      ConferenceRoomId
        {
            get
            { return conferenceRoomId;}
            set
            { 
                AddToAudit("ConferenceRoomId", conferenceRoomId, value);
                conferenceRoomId = value;
            }
        }

        public bool     UseDetailPrices
        {
            get
            { return useDetailPrices;}
            set
            { 
                AddToAudit("UseDetailPrices", useDetailPrices, value);
                useDetailPrices = value;
            }
        }

        public int      BlockId
        {
            get
            { return blockId;}
            set
            { 
                AddToAudit("BlockId", blockId, value);
                blockId = value;
            }
        }

        private KxSiteBlock _siteBlock;
        public KxSiteBlock SiteBlock
        {
            get
            {
                if (_siteBlock == null)
                {
                    _siteBlock = new KxSiteBlock(null, blockId);
                }
                return _siteBlock;
            }
        }

        public int      BedroomTypeId
        {
            get
            { return bedroomTypeId;}
            set
            { 
                AddToAudit("BedroomTypeId", bedroomTypeId, value);
                bedroomTypeId = value;
            }
        }

        private KxBedroomType _bedroomType;
        public KxBedroomType BedroomType
        {
            get
            {
                if (_bedroomType == null)
                {
                    _bedroomType = new KxBedroomType(null, bedroomTypeId);
                }
                return _bedroomType;
            }
        }

        public bool     AbsorbsDiscount
        {
            get
            { return absorbsDiscount;}
            set
            { 
                AddToAudit("AbsorbsDiscount", absorbsDiscount, value);
                absorbsDiscount = value;
            }
        }

        public double   SplitPercent
        {
            get
            { return splitPercent;}
            set
            { 
                AddToAudit("SplitPercent", splitPercent, value);
                splitPercent = value;
            }
        }

        public int      DaysApplied
        {
            get
            { return daysApplied;}
            set
            { 
                AddToAudit("DaysApplied", daysApplied, value);
                daysApplied = value;
            }
        }

        public string DaysAppliedString
        {
            get
            {
                using (KxEncodedDaysOfWeek dow = new KxEncodedDaysOfWeek(DaysApplied))
                {
                    return dow.DaysString;
                }
            }
        }

        public int      EventTypeId
        {
            get
            { return eventTypeId;}
            set
            { 
                AddToAudit("EventTypeId", eventTypeId, value);
                eventTypeId = value;
            }
        }

        public int      NominalId
        {
            get
            { return nominalId;}
            set
            { 
                AddToAudit("NominalId", nominalId, value);
                nominalId = value;
            }
        }

        public string   ExternalCode
        {
            get
            { return externalCode;}
            set
            { 
                AddToAudit("ExternalCode", externalCode, value);
                externalCode = value;
            }
        }

        public int      MaxRoomOccupancy
        {
            get
            { return maxRoomOccupancy;}
            set
            { 
                AddToAudit("MaxRoomOccupancy", maxRoomOccupancy, value);
                maxRoomOccupancy = value;
            }
        }

        public int      CompanyGroupId
        {
            get
            { return companyGroupId;}
            set
            { 
                AddToAudit("CompanyGroupId", companyGroupId, value);
                companyGroupId = value;
            }
        }

        public bool     AbsorbsVATRounding
        {
            get
            { return absorbsVATRounding;}
            set
            { 
                AddToAudit("AbsorbsVATRounding", absorbsVATRounding, value);
                absorbsVATRounding = value;
            }
        }


        #endregion 


        public KxChargeTypeRateMatrix(KxBaseClass parent):base(parent)
        {}


        public KxChargeTypeRateMatrix(KxBaseClass parent, XmlNode node):base(parent)
        {
             LoadFromXml("", node);
        }

        public KxChargeTypeRateMatrix(KxBaseClass parent,int      matrixid):base(parent)
        {
             base.dBIdentifier = matrixid;
             Load();
        }


        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("chargetyperatematrix", "ChargeTypeRateMatrix", TargetType.Table);

            result.AutoKeyField = new KxDataValue("MatrixId",MatrixId,KxValueType.Int);


            if ((actiontype == ActionType.Update) && (MatrixId == 0))
            {
                result.Action = ActionType.Insert;
            }
            else
            {
                result.Action = actiontype;
            }

            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {
                   result.AddParameter("chargeTypeId", chargeTypeId, KxValueType.Int, false);
                   result.AddParameter("parentMatrixId", parentMatrixId, KxValueType.Int, true);
                   result.AddParameter("salesTypeId", salesTypeId, KxValueType.Int, true);
                   result.AddParameter("companyId", companyId, KxValueType.Int, true);
                   result.AddParameter("bandId", bandId, KxValueType.Int, true);
                   result.AddParameter("externalMaterialType", externalMaterialType, KxValueType.String, 50, true);
                   result.AddParameter("externalDescription", externalDescription, KxValueType.String, 50, true);
                   result.AddParameter("rate", rate, KxValueType.Decimal, false);
                   result.AddParameter("startDate", startDate, KxValueType.DateTime, true);
                   result.AddParameter("endDate", endDate, KxValueType.DateTime, true);
                   result.AddParameter("rateTypeId", rateTypeId, KxValueType.Int, true);
                   result.AddParameter("facilityId", facilityId, KxValueType.Int, true);
                   result.AddParameter("conferenceRoomId", conferenceRoomId, KxValueType.Int, true);
                   result.AddParameter("useDetailPrices", useDetailPrices, KxValueType.Boolean, false);
                   result.AddParameter("blockId", blockId, KxValueType.Int, true);
                   result.AddParameter("bedroomTypeId", bedroomTypeId, KxValueType.Int, true);
                   result.AddParameter("absorbsDiscount", absorbsDiscount, KxValueType.Boolean, false);
                   result.AddParameter("splitPercent", splitPercent, KxValueType.Float, false);
                   result.AddParameter("daysApplied", daysApplied, KxValueType.Int, false);
                   result.AddParameter("eventTypeId", eventTypeId, KxValueType.Int, false);
                   result.AddParameter("nominalId", nominalId, KxValueType.Int, true);
                   result.AddParameter("externalCode", externalCode, KxValueType.String, 255, true);
                   result.AddParameter("maxRoomOccupancy", maxRoomOccupancy, KxValueType.Int, true);
                   result.AddParameter("companyGroupId", companyGroupId, KxValueType.Int, true);
                   result.AddParameter("absorbsVATRounding", absorbsVATRounding, KxValueType.Boolean, true);
            }
            return result;
        }


        public override  void LoadFromXml(string xml,XmlNode node)
        {
            dBIdentifier  = KxXmlUtils.GetNodeValue(node,"matrixid",0);
            siteId  = KxXmlUtils.GetNodeValue(node,"siteid",0);
            chargeTypeId  = KxXmlUtils.GetNodeValue(node,"chargetypeid",0);
            parentMatrixId  = KxXmlUtils.GetNodeValue(node,"parentmatrixid",0);
            salesTypeId  = KxXmlUtils.GetNodeValue(node,"salestypeid",0);
            companyId  = KxXmlUtils.GetNodeValue(node,"companyid",0);
            bandId  = KxXmlUtils.GetNodeValue(node,"bandid",0);
            externalMaterialType  = KxXmlUtils.GetNodeValue(node,"externalmaterialtype","");
            externalDescription  = KxXmlUtils.GetNodeValue(node,"externaldescription","");
            rate  = KxXmlUtils.GetNodeValue(node,"rate",(decimal)0.00M);
            startDate  = KxXmlUtils.GetNodeValueDT(node,"startdate",null);
            endDate  = KxXmlUtils.GetNodeValueDT(node,"enddate",null);
            rateTypeId  = KxXmlUtils.GetNodeValue(node,"ratetypeid",0);
            facilityId  = KxXmlUtils.GetNodeValue(node,"facilityid",0);
            conferenceRoomId  = KxXmlUtils.GetNodeValue(node,"conferenceroomid",0);
            useDetailPrices  = KxXmlUtils.GetNodeValue(node,"usedetailprices",false);
            blockId  = KxXmlUtils.GetNodeValue(node,"blockid",0);
            bedroomTypeId  = KxXmlUtils.GetNodeValue(node,"bedroomtypeid",0);
            absorbsDiscount  = KxXmlUtils.GetNodeValue(node,"absorbsdiscount",false);
            splitPercent  = KxXmlUtils.GetNodeValue(node,"splitpercent",0.0f);
            daysApplied  = KxXmlUtils.GetNodeValue(node,"daysapplied",0);
            eventTypeId  = KxXmlUtils.GetNodeValue(node,"eventtypeid",0);
            nominalId  = KxXmlUtils.GetNodeValue(node,"nominalid",0);
            externalCode  = KxXmlUtils.GetNodeValue(node,"externalcode","");
            maxRoomOccupancy  = KxXmlUtils.GetNodeValue(node,"maxroomoccupancy",0);
            companyGroupId  = KxXmlUtils.GetNodeValue(node,"companygroupid",0);
            absorbsVATRounding  = KxXmlUtils.GetNodeValue(node,"absorbsvatrounding",false);
        }
    }
}

