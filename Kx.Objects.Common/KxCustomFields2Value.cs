using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;
using Kx.Objects.Common.Web;

namespace Kx.Objects.Common
{
    [Serializable]
    public class KxCustomFields2Value : KxBaseClass
    {
        #region Attributes & Properties

        protected int webUdf = 0;
        protected int      customFieldId;
        protected string   entityType;
        protected int      entityId;
        protected string   fieldValue = "";

        protected KxUserDefinedField uDF = null;

        public int      CustomFieldValueId
        {
            get
            { return base.dBIdentifier;}
        }

        public int WebUdf
        {
            get
            { return webUdf; }
            set
            {
                AddToAudit("webUdf", webUdf, value);
                webUdf = value;
            }
        }
        
        public int      CustomFieldId
        {
            get
            { return customFieldId;}
            set
            { 
                AddToAudit("CustomFieldId", customFieldId, value);
                customFieldId = value;
            }
        }

        public string   EntityType
        {
            get
            { return entityType;}
            set
            { 
                AddToAudit("EntityType", entityType, value);
                entityType = value;
            }
        }

        public int      EntityId
        {
            get
            { return entityId;}
            set
            { 
                AddToAudit("EntityId", entityId, value);
                entityId = value;
            }
        }

        public string   FieldValue
        {
            get
            { return fieldValue;}
            set
            { 
                AddToAudit("FieldValue", fieldValue, value);
                fieldValue = value;
            }
        }

        public string FieldType
        {
            get
            { 
                try
                {
                    return UDF.FieldType;
                }
                catch
                {
                    return "";
                }

            }

        }
        public string FieldName
        {
            get
            {
                try
                {
                    return UDF.FieldName;
                }
                catch
                {
                    return "";
                }
            }

        }
        public bool IsCompanies
        {
            get
            { return UDF.IsCompanies; }
        }
        public bool IsPeople
        {
            get
            { return UDF.IsPeople; }
        }
        public bool IsStudents
        {
            get
            { return UDF.IsStudents; }
        }
        public bool IsEvents
        {
            get
            { return UDF.IsEvents; }
        }
        public string Picklist
        {
            get
            { return UDF.Picklist; }
        }
        public string DefaultValue
        {
            get
            { return UDF.DefaultValue; }
        }
        public int ValidationRequired
        {
            get
            { return UDF.ValidationRequired; }
        }
        public string ValidationScript
        {
            get
            { return UDF.ValidationScript; }
        }
        public bool IsStudentApplications
        {
            get
            { return UDF.IsStudentApplications; }
        }
        public bool IsStudentCourses
        {
            get
            { return UDF.IsStudentCourses; }
        }
        public string[] SelectionList
        {
            get
            {
                return UDF.SelectionList;
            }
        }

        public KxUserDefinedField UDF
        {
            get
            {
                if (uDF == null)
                {
                    if (customFieldId > 0)
                    {
                        uDF = KxUDFProvider.GetUnmappedUdfById(customFieldId);
                    }
                }
                return uDF;
            }
            set
            { uDF = value; }
        }
        #endregion 

        public KxCustomFields2Value(KxBaseClass parent, int customfieldvalueid)
            : base(parent)
        {
             base.dBIdentifier = customfieldvalueid;
             Load();
        }

        public KxCustomFields2Value(KxBaseClass parent, XmlNode node) : base(parent)
        {
            LoadFromXml("",node);
        }


        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("customfields2values", "CustomFields2Values", TargetType.Table);

            result.AutoKeyField = new KxDataValue("CustomFieldValueId",CustomFieldValueId,KxValueType.Int);


            if ((actiontype == ActionType.Update) && (CustomFieldValueId == 0))
            {
                result.Action = ActionType.Insert;
            }
            else
            {
                result.Action = actiontype;
            }

            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {
                   result.AddParameter("customFieldId", customFieldId, KxValueType.Int);
                   result.AddParameter("entityType", entityType, KxValueType.String);
                   result.AddParameter("entityId", entityId, KxValueType.Int);
                   result.AddParameter("fieldValue", fieldValue, KxValueType.String);
            }
            return result;
        }


        public override void LoadFromXml(string xml,XmlNode node)
        {
            dBIdentifier  = KxXmlUtils.GetNodeValue(node,"customfieldvalueid",0);
            webUdf = KxXmlUtils.GetNodeValue(node, "webudf", 0);
            customFieldId  = KxXmlUtils.GetNodeValue(node,"customfieldid",0);
            entityType  = KxXmlUtils.GetNodeValue(node,"entitytype","");
            entityId  = KxXmlUtils.GetNodeValue(node,"entityid",0);
            fieldValue  = KxXmlUtils.GetNodeValue(node,"fieldvalue","");
        }
    }
}

