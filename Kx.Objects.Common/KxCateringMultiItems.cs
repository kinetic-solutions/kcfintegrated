﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Exceptions;
using Kx.Core.Utility;

namespace Kx.Objects.Common
{
    public class KxCateringMultiItems : KxBaseClass
    {
        private List<KxCateringMultiItem> items = new List<KxCateringMultiItem>();

        public List<KxCateringMultiItem> Items
        {
            get
            { return items; }
        }

        public KxCateringMultiItem this[int index]
        {
            get
            { return items[index]; }
            set
            { items[index] = value; }
        }

        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = null;

            if (actiontype == ActionType.Select)
            {
                result = new KxData("cateringmultiitems", "cateringmultiitems", TargetType.Table, ActionType.Select);

                result.OrderFields.Add(new KxDataOrder("multiitemid"));
                
            }
            return result;
        }


        public KxCateringMultiItems(KxBaseClass parent)
            : base(parent)
        {

        }


        public override bool LoadFromXml(string xml, string xpath)
        {
            items.Clear();
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xml);

                XmlNodeList nodes = doc.SelectNodes(@"//results/" + xpath + "/datarow");

                foreach (XmlNode node in nodes)
                {
                    if (node != null)
                    {
                        KxCateringMultiItem multiitem = new KxCateringMultiItem(this, node);
                        Items.Add(multiitem);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "LoadFromXml", ex.Message, xpath);
                LastError = ex.Message;
                return false;
            }
        }

        protected int[] GetMultiItemIDArray()
        {
            int loop = 0;

            int[] result = new int[Items.Count];

            foreach (KxCateringMultiItem multiitem in Items)
            {
                result[loop] = multiitem.MultiItemId;
                loop++;
            }
            return result;
        }

        public KxCateringMultiItem GetMultiItemById(int id)
        {
            KxCateringMultiItem result = null;

            foreach (KxCateringMultiItem multiitem in Items)
            {
                if (multiitem.MultiItemId == id)
                {
                    result = multiitem;
                    break;
                }
            }

            return result;
        }
    }
}
