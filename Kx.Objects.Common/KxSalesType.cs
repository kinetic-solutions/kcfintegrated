﻿using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;

namespace Kx.Objects.Common
{
    [Serializable]
    public class KxSalesType : KxBaseClass
    {
        #region Attributes & Properties

        protected string   description;
        protected string   externalCode;
        protected bool     internalValue;

        public int      SalesTypeId
        {
            get
            { return base.dBIdentifier;}
        }

        public string   Description
        {
            get
            { return description;}
            set
            { 
                AddToAudit("Description", description, value);
                description = value;
            }
        }

        public string   ExternalCode
        {
            get
            { return externalCode;}
            set
            { 
                AddToAudit("ExternalCode", externalCode, value);
                externalCode = value;
            }
        }

        public bool     Internal
        {
            get
            { return internalValue;}
            set
            { 
                AddToAudit("Internal", internalValue, value);
                internalValue = value;
            }
        }


        #endregion 


        public KxSalesType(KxBaseClass parent):base(parent)
        {}


        public KxSalesType(KxBaseClass parent, XmlNode node):base(parent)
        {
             LoadFromXml("", node);
        }

        public KxSalesType(KxBaseClass parent,int      salestypeid):base(parent)
        {
             base.dBIdentifier = salestypeid;
             Load();
        }


        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("salestype", "SalesType", TargetType.Table);

            result.AutoKeyField = new KxDataValue("SalesTypeId",SalesTypeId,KxValueType.Int);


            if ((actiontype == ActionType.Update) && (SalesTypeId == 0))
            {
                result.Action = ActionType.Insert;
            }
            else
            {
                result.Action = actiontype;
            }

            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {
                   result.AddParameter("externalCode", externalCode, KxValueType.String, 20, false);
                   result.AddParameter("internal", internalValue, KxValueType.Boolean, false);
            }
            return result;
        }


        public override  void LoadFromXml(string xml,XmlNode node)
        {
            dBIdentifier  = KxXmlUtils.GetNodeValue(node,"salestypeid",0);
            description  = KxXmlUtils.GetNodeValue(node,"description","");
            externalCode  = KxXmlUtils.GetNodeValue(node,"externalcode","");
            internalValue = KxXmlUtils.GetNodeValue(node, "internal", false);
        }
    }
}
