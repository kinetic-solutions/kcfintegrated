using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;

namespace Kx.Objects.Common
{
    [Serializable]
    public class KxSiteBlock : KxBaseClass
    {
        #region Attributes & Properties

        protected int      siteId;
        protected string   name;
        protected string   blockType;
        protected string blockUrl = "";
        protected int      masterBlockId;
        protected int      sequenceNo;
        protected int      defCatConfRoomId;
        protected int      areaId;
        protected bool     allocatable;
        protected int      addressId;
        protected string   externalCode;
        protected int      distribAdHocUserId;
        protected int      userGroupId;
        protected int      extraChargeId;
        protected KxAddress address = null;
        protected KxSite site = null;
        protected KxArea area = null;

        public int      BlockId
        {
            get
            { return base.dBIdentifier;}
        }

        public int      SiteId
        {
            get
            { return siteId;}
            set
            { 
                AddToAudit("SiteId", siteId, value);
                siteId = value;
            }
        }

        public string   Name
        {
            get
            { return name;}
            set
            { 
                AddToAudit("Name", name, value);
                name = value;
            }
        }

        public string AreaName
        {
            get
            {
                if (Area != null)
                {
                    return Area.Description;
                }
                else
                {
                    return "";
                }
            }
        }

        public string   BlockType
        {
            get
            { return blockType;}
            set
            { 
                AddToAudit("BlockType", blockType, value);
                blockType = value;
            }
        }

        public string BlockUrl
        {
            get
            {
                if (blockUrl == "")
                {
                    blockUrl = GetBlockUrl();
                }
                return blockUrl;
            }
            set
            {
                blockUrl = value;
            }
        }

        private string _blockUrlString1;
        public string BlockUrlString1
        {
            get
            {
                if (string.IsNullOrEmpty(_blockUrlString1))
                {
                    _blockUrlString1 = GetBlockUrlString1();
                }
                return _blockUrlString1;
            }
            set
            {
                _blockUrlString1 = value;
            }
        }

        private string _blockUrl2String1;
        public string BlockUrl2String1
        {
            get
            {
                if (string.IsNullOrEmpty(_blockUrl2String1))
                {
                    _blockUrl2String1 = GetBlockUrl2String1();
                }
                return _blockUrl2String1;
            }
        }

        public int MasterBlockId
        {
            get
            { return masterBlockId;}
            set
            { 
                AddToAudit("MasterBlockId", masterBlockId, value);
                masterBlockId = value;
            }
        }

        public int      SequenceNo
        {
            get
            { return sequenceNo;}
            set
            { 
                AddToAudit("SequenceNo", sequenceNo, value);
                sequenceNo = value;
            }
        }

        public int      DefCatConfRoomId
        {
            get
            { return defCatConfRoomId;}
            set
            { 
                AddToAudit("DefCatConfRoomId", defCatConfRoomId, value);
                defCatConfRoomId = value;
            }
        }

        public int      AreaId
        {
            get
            { return areaId;}
            set
            { 
                AddToAudit("AreaId", areaId, value);
                areaId = value;
            }
        }

        public bool     Allocatable
        {
            get
            { return allocatable;}
            set
            { 
                AddToAudit("Allocatable", allocatable, value);
                allocatable = value;
            }
        }

        public int      AddressId
        {
            get
            { return addressId;}
            set
            { 
                AddToAudit("AddressId", addressId, value);
                addressId = value;
            }
        }

        public string   ExternalCode
        {
            get
            { return externalCode;}
            set
            { 
                AddToAudit("ExternalCode", externalCode, value);
                externalCode = value;
            }
        }

        public int      DistribAdHocUserId
        {
            get
            { return distribAdHocUserId;}
            set
            { 
                AddToAudit("DistribAdHocUserId", distribAdHocUserId, value);
                distribAdHocUserId = value;
            }
        }

        public int      UserGroupId
        {
            get
            { return userGroupId;}
            set
            { 
                AddToAudit("UserGroupId", userGroupId, value);
                userGroupId = value;
            }
        }

        public int      ExtraChargeId
        {
            get
            { return extraChargeId;}
            set
            { 
                AddToAudit("ExtraChargeId", extraChargeId, value);
                extraChargeId = value;
            }
        }

        public KxAddress Address
        {
            get
            {
                if (address == null && addressId > 0)
                {
                    address = new KxAddress(this, addressId);
                }
                return address;
            }
            set
            {
                if (address == value)
                    return;
                address = value;
            }
        }

        public KxArea Area
        {
            get
            {
                if (area == null && areaId != 0)
                {
                    area = new KxArea(this,areaId);
                }
                return area;
            }
        }

        public KxSite Site
        {
            get
            {
                if (site == null && siteId > 0)
                {
                    site = new KxSite(this, siteId);
                }
                return site;
            }
        }
        
        #endregion 

        public KxSiteBlock(KxBaseClass parent,int      blockid):base(parent)
        {
             base.dBIdentifier = blockid;
             Load();
        }

        public KxSiteBlock(KxBaseClass parent, XmlNode node)
            : base(parent)
        {
            LoadFromXml("",node);
        }

        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("siteblocks", "SiteBlocks", TargetType.Table);

            result.AutoKeyField = new KxDataValue("BlockId",BlockId,KxValueType.Int);


            if ((actiontype == ActionType.Update) && (BlockId == 0))
            {
                result.Action = ActionType.Insert;
            }
            else
            {
                result.Action = actiontype;
            }

            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {
                   result.AddParameter("siteId", siteId, KxValueType.Int);
                   result.AddParameter("Name", Name, KxValueType.String);
                   result.AddParameter("blockType", blockType, KxValueType.String);
                   result.AddParameter("masterBlockId", masterBlockId, KxValueType.Int);
                   result.AddParameter("sequenceNo", sequenceNo, KxValueType.Int);
                   result.AddParameter("defCatConfRoomId", defCatConfRoomId, KxValueType.Int);
                   result.AddParameter("areaId", areaId, KxValueType.Int);
                   result.AddParameter("allocatable", allocatable, KxValueType.Boolean);
                   result.AddParameter("addressId", addressId, KxValueType.Int);
                   result.AddParameter("externalCode", externalCode, KxValueType.String);
                   result.AddParameter("distribAdHocUserId", distribAdHocUserId, KxValueType.Int);
                   result.AddParameter("userGroupId", userGroupId, KxValueType.Int);
                   result.AddParameter("extraChargeId", extraChargeId, KxValueType.Int);
            }
            return result;
        }


        public override  void LoadFromXml(string xml,XmlNode node)
        {
            dBIdentifier  = KxXmlUtils.GetNodeValue(node,"blockid",0);
            siteId  = KxXmlUtils.GetNodeValue(node,"siteid",0);
            Name  = KxXmlUtils.GetNodeValue(node,"name","").TrimEnd();
            blockType  = KxXmlUtils.GetNodeValue(node,"blocktype","");
            masterBlockId  = KxXmlUtils.GetNodeValue(node,"masterblockid",0);
            sequenceNo  = KxXmlUtils.GetNodeValue(node,"sequenceno",0);
            defCatConfRoomId  = KxXmlUtils.GetNodeValue(node,"defcatconfroomid",0);
            areaId  = KxXmlUtils.GetNodeValue(node,"areaid",0);
            allocatable  = KxXmlUtils.GetNodeValue(node,"allocatable",false);
            addressId  = KxXmlUtils.GetNodeValue(node,"addressid",0);
            externalCode  = KxXmlUtils.GetNodeValue(node,"externalcode","");
            distribAdHocUserId  = KxXmlUtils.GetNodeValue(node,"distribadhocuserid",0);
            userGroupId  = KxXmlUtils.GetNodeValue(node,"usergroupid",0);
            extraChargeId  = KxXmlUtils.GetNodeValue(node,"extrachargeid",0);
        }

        protected string GetBlockUrl()
        {
            string result = "";

            if (dBIdentifier != 0)
            {
                try
                {
                    KxCustomSetting setting = new KxCustomSetting(null,"Kx.Web.ArrivalInst.URL", dBIdentifier);
                    result =  setting.AllString;
                }
                catch { }
            }
            return result;
        }

        protected string GetBlockUrlString1()
        {
            string result = "";

            if (dBIdentifier != 0)
            {
                try
                {
                    KxCustomSetting setting = new KxCustomSetting(null, "Kx.Web.ArrivalInst.URL", dBIdentifier);
                    result = setting.String1;
                }
                catch { }
            }
            return result;
        }

        protected string GetBlockUrl2String1()
        {
            string result = "";

            if (dBIdentifier != 0)
            {
                try
                {
                    KxCustomSetting setting = new KxCustomSetting(null, "Kx.Web.Block.URL", dBIdentifier);
                    result = setting.String1;
                }
                catch { }
            }
            return result;
        }
    }
}

