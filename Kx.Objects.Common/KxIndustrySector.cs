using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;

namespace Kx.Objects.Common
{
    [Serializable]
    public class KxIndustrySector : KxBaseClass
    {
        #region Attributes & Properties

        protected string   description;
        protected string   notes;
        protected bool     inactive;
        protected string   externalCode;

        public int      IndustrySectorId
        {
            get
            { return base.dBIdentifier;}
        }

        public string   Description
        {
            get
            { return description;}
            set
            { 
                AddToAudit("Description", description, value);
                description = value;
            }
        }

        public string   Notes
        {
            get
            { return notes;}
            set
            { 
                AddToAudit("Notes", notes, value);
                notes = value;
            }
        }

        public bool     Inactive
        {
            get
            { return inactive;}
            set
            { 
                AddToAudit("Inactive", inactive, value);
                inactive = value;
            }
        }

        public string   ExternalCode
        {
            get
            { return externalCode;}
            set
            { 
                AddToAudit("ExternalCode", externalCode, value);
                externalCode = value;
            }
        }


        #endregion 


        public KxIndustrySector(KxBaseClass parent):base(parent)
        {}


        public KxIndustrySector(KxBaseClass parent, XmlNode node):base(parent)
        {
             LoadFromXml("", node);
        }

        public KxIndustrySector(KxBaseClass parent,int      industrysectorid):base(parent)
        {
             base.dBIdentifier = industrysectorid;
             Load();
        }


        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("industrysectors", "IndustrySectors", TargetType.Table);

            result.AutoKeyField = new KxDataValue("IndustrySectorId",IndustrySectorId,KxValueType.Int);


            if ((actiontype == ActionType.Update) && (IndustrySectorId == 0))
            {
                result.Action = ActionType.Insert;
            }
            else
            {
                result.Action = actiontype;
            }

            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {
                   result.AddParameter("notes", notes, KxValueType.String, 2147483647, true);
                   result.AddParameter("inactive", inactive, KxValueType.Boolean, true);
                   result.AddParameter("externalCode", externalCode, KxValueType.String, 255, true);
            }
            return result;
        }


        public override  void LoadFromXml(string xml,XmlNode node)
        {
            dBIdentifier  = KxXmlUtils.GetNodeValue(node,"industrysectorid",0);
            description  = KxXmlUtils.GetNodeValue(node,"description","");
            notes  = KxXmlUtils.GetNodeValue(node,"notes","");
            inactive  = KxXmlUtils.GetNodeValue(node,"inactive",false);
            externalCode  = KxXmlUtils.GetNodeValue(node,"externalcode","");
        }
    }
}

