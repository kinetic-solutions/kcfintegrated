﻿using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;

namespace Kx.Objects.Common
{
    [Serializable]
    public class KxUserGroup : KxBaseClass
    {
        #region Attributes & Properties

        protected string   description;
        protected bool     inactive;
        protected bool     notifyMove;
        protected bool     notifySwap;
        protected bool     notifyCheckIn;
        protected bool     notifyCheckOut;
        protected bool     notifyRoomWaiting;
        protected bool     notifyDamage;
        protected bool     notifyStudentAccepted;
        protected bool     notifyStudentRejected;
        protected bool     notifyStudentRemovedRoom;

        public int      UserGroupId
        {
            get
            { return base.dBIdentifier;}
        }

        public string   Description
        {
            get
            { return description;}
            set
            { 
                AddToAudit("Description", description, value);
                description = value;
            }
        }

        public bool     Inactive
        {
            get
            { return inactive;}
            set
            { 
                AddToAudit("Inactive", inactive, value);
                inactive = value;
            }
        }

        public bool     NotifyMove
        {
            get
            { return notifyMove;}
            set
            { 
                AddToAudit("NotifyMove", notifyMove, value);
                notifyMove = value;
            }
        }

        public bool     NotifySwap
        {
            get
            { return notifySwap;}
            set
            { 
                AddToAudit("NotifySwap", notifySwap, value);
                notifySwap = value;
            }
        }

        public bool     NotifyCheckIn
        {
            get
            { return notifyCheckIn;}
            set
            { 
                AddToAudit("NotifyCheckIn", notifyCheckIn, value);
                notifyCheckIn = value;
            }
        }

        public bool     NotifyCheckOut
        {
            get
            { return notifyCheckOut;}
            set
            { 
                AddToAudit("NotifyCheckOut", notifyCheckOut, value);
                notifyCheckOut = value;
            }
        }

        public bool     NotifyRoomWaiting
        {
            get
            { return notifyRoomWaiting;}
            set
            { 
                AddToAudit("NotifyRoomWaiting", notifyRoomWaiting, value);
                notifyRoomWaiting = value;
            }
        }

        public bool     NotifyDamage
        {
            get
            { return notifyDamage;}
            set
            { 
                AddToAudit("NotifyDamage", notifyDamage, value);
                notifyDamage = value;
            }
        }

        public bool     NotifyStudentAccepted
        {
            get
            { return notifyStudentAccepted;}
            set
            { 
                AddToAudit("NotifyStudentAccepted", notifyStudentAccepted, value);
                notifyStudentAccepted = value;
            }
        }

        public bool     NotifyStudentRejected
        {
            get
            { return notifyStudentRejected;}
            set
            { 
                AddToAudit("NotifyStudentRejected", notifyStudentRejected, value);
                notifyStudentRejected = value;
            }
        }

        public bool     NotifyStudentRemovedRoom
        {
            get
            { return notifyStudentRemovedRoom;}
            set
            { 
                AddToAudit("NotifyStudentRemovedRoom", notifyStudentRemovedRoom, value);
                notifyStudentRemovedRoom = value;
            }
        }


        #endregion 


        public KxUserGroup(KxBaseClass parent):base(parent)
        {}


        public KxUserGroup(KxBaseClass parent, XmlNode node):base(parent)
        {
             LoadFromXml("", node);
        }

        public KxUserGroup(KxBaseClass parent, int usergroupid)
            : base(parent)
        {
             base.dBIdentifier = usergroupid;
             Load();
        }


        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("usergroups", "UserGroups", TargetType.Table);

            result.AutoKeyField = new KxDataValue("UserGroupId",UserGroupId,KxValueType.Int);


            if ((actiontype == ActionType.Update) && (UserGroupId == 0))
            {
                result.Action = ActionType.Insert;
            }
            else
            {
                result.Action = actiontype;
            }

            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {
                   result.AddParameter("inactive", inactive, KxValueType.Boolean, false);
                   result.AddParameter("notifyMove", notifyMove, KxValueType.Boolean, true);
                   result.AddParameter("notifySwap", notifySwap, KxValueType.Boolean, true);
                   result.AddParameter("notifyCheckIn", notifyCheckIn, KxValueType.Boolean, true);
                   result.AddParameter("notifyCheckOut", notifyCheckOut, KxValueType.Boolean, true);
                   result.AddParameter("notifyRoomWaiting", notifyRoomWaiting, KxValueType.Boolean, true);
                   result.AddParameter("notifyDamage", notifyDamage, KxValueType.Boolean, true);
                   result.AddParameter("notifyStudentAccepted", notifyStudentAccepted, KxValueType.Boolean, true);
                   result.AddParameter("notifyStudentRejected", notifyStudentRejected, KxValueType.Boolean, true);
                   result.AddParameter("notifyStudentRemovedRoom", notifyStudentRemovedRoom, KxValueType.Boolean, true);
            }
            return result;
        }


        public override  void LoadFromXml(string xml,XmlNode node)
        {
            dBIdentifier  = KxXmlUtils.GetNodeValue(node,"usergroupid",0);
            description  = KxXmlUtils.GetNodeValue(node,"description","");
            inactive  = KxXmlUtils.GetNodeValue(node,"inactive",false);
            notifyMove  = KxXmlUtils.GetNodeValue(node,"notifymove",false);
            notifySwap  = KxXmlUtils.GetNodeValue(node,"notifyswap",false);
            notifyCheckIn  = KxXmlUtils.GetNodeValue(node,"notifycheckin",false);
            notifyCheckOut  = KxXmlUtils.GetNodeValue(node,"notifycheckout",false);
            notifyRoomWaiting  = KxXmlUtils.GetNodeValue(node,"notifyroomwaiting",false);
            notifyDamage  = KxXmlUtils.GetNodeValue(node,"notifydamage",false);
            notifyStudentAccepted  = KxXmlUtils.GetNodeValue(node,"notifystudentaccepted",false);
            notifyStudentRejected  = KxXmlUtils.GetNodeValue(node,"notifystudentrejected",false);
            notifyStudentRemovedRoom  = KxXmlUtils.GetNodeValue(node,"notifystudentremovedroom",false);
        }
    }
}
