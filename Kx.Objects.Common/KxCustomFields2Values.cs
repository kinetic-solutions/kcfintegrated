using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;

namespace Kx.Objects.Common
{
    [Serializable]
    public class KxCustomFields2Values : KxBaseClass
    {
        #region Attributes & Properties

        protected List<KxCustomFields2Value> items = new List<KxCustomFields2Value>();
        protected string entityType;
        protected int entityId;

        public List<KxCustomFields2Value> Items
        {
            get
            {
                return items;
            }
            set
            {
                items = value;
            }
        }
        #endregion

        public KxCustomFields2Values(KxBaseClass parent, string entitytype, int entityid)
            : base(parent)
        {
            entityType = entitytype;
            entityId = entityid;
            Load();
        }

        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = null;

            if (actiontype == ActionType.Select)
            {
                result = new KxData("udfs", "CustomFields2Values", TargetType.StoredProc);
                result.KeyFields.Add(new KxDataValue("entityType", entityType, KxValueType.String));
                result.KeyFields.Add(new KxDataValue("entityId", entityId, KxValueType.Int));
            }
            return result;
        }

        public override bool LoadFromXml(string xml, string xpath)
        {
            items.Clear();
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xml);

                XmlNodeList nodes = doc.SelectNodes(@"//results/" + xpath + "/datarow");

                foreach (XmlNode node in nodes)
                {
                    if (node != null)
                    {
                        KxCustomFields2Value udf = new KxCustomFields2Value(this, node);
                        items.Add(udf);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "LoadFromXml", ex.Message, xpath);
                LastError = ex.Message;
                return false;
            }
        }

        public KxCustomFields2Value GetUDFById(int Id)
        {
            KxCustomFields2Value result = null;

            foreach (KxCustomFields2Value udf in items)
            {
                if (udf.WebUdf == Id || (udf.WebUdf == 0 && udf.CustomFieldId == Id))
                {
                    result = udf;
                    break;
                }
            }
            return result;
        }

        public KxCustomFields2Value GetUDFByName(string fieldName)
        {
            KxCustomFields2Value result = new KxCustomFields2Value(null, 0);
			String sFieldname = fieldName.ToUpper().Trim();

            foreach (KxCustomFields2Value udf in items)
            {
				if (udf.FieldName.ToUpper().Trim() == sFieldname)
                {
					result = udf;
                    break;
                }
            }
			return result;
        }

        public string GetFieldValueById(int Id)
        {
            string result = "";

            KxCustomFields2Value val = GetUDFById(Id);

            if (val != null)
            {
                return val.FieldValue;
            }
            return result;
        }

        public string GetFieldValueByName(string fieldName)
        {
            string result = "";

            KxCustomFields2Value val = GetUDFByName(fieldName);

            if (val != null)
            {
                return val.FieldValue;
            }
            return result;
        }
    }
}

