﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Exceptions;
using Kx.Core.Reflection;
using Kx.Core.Utility;
using System.Xml;



namespace Kx.Objects.Common
{
    public class KxWordTemplates : KxBaseClass, IEnumerable<KxWordTemplateInfo>
    {


        private List<KxWordTemplateInfo> items = new List<KxWordTemplateInfo>();
		//private List<KxWordTemplate> items = new List<KxWordTemplate>();

	
		/// <summary>
		/// Returns a KxWordTemplate from the items list at the specified position
		/// </summary>
		public KxWordTemplateInfo this[int index]
		{
			get { return items[index]; }
            //set { items[index].WordTemplate = value; }
		}

		/// <value>
		/// Contians a collection of Kx2Request
		/// </value>
        public List<KxWordTemplateInfo> Items
        {
            get { return items; }
        }
		
		/// <summary>
		/// Creates an instance of this class
		/// </summary>
		public KxWordTemplates(KxBaseClass parent)
			: this(parent, string.Empty)
		{
		}

		/// <summary>
		/// Creates an instance of this class
		/// </summary>
		public KxWordTemplates(KxBaseClass parent, string reference)
			: base(parent)
		{
			//_reference = reference;
			
			Load();
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="actiontype">action type to configure for</param>
		/// <returns>KxData</returns>
        //public override KxData ConfigureData(ActionType actiontype)
        //{			
        //    KxData result = null;

        //    if (actiontype == ActionType.Select)
        //    {
        //        result = new KxData("wordtemplates", "WordTemplates", TargetType.Table);
        //    }
        //    return result;
        //}



        /// <summary>
        /// 
        /// </summary>
        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = null;

            if (actiontype == ActionType.Select)
            {
                result = new KxData();

                result.Target = TargetType.Static;
                result.RootName = "wordtemplates";
                result.Action = ActionType.Select;

                StringBuilder sql = new StringBuilder();
                StringBuilder sqlWhere = new StringBuilder();

                sql.AppendLine("SELECT WordTemplateID, Description FROM WordTemplates");

                //_reference = "0";
                //if (_reference != Kx2RequestStatus.Undefined)
                //{
                    sqlWhere.AppendFormat("WHERE Inactive LIKE '%{0}%'", 0);
                //}
                                

                //if (!String.IsNullOrEmpty(_reference))
                //{
                //    if (sqlWhere.Length > 0)
                //        sqlWhere.Append(" AND");
                //    else
                //        sqlWhere.Append(" WHERE");

                //    sqlWhere.AppendFormat(" Inactive LIKE '%{0}%'", _reference);
                //}


                sql.Append(sqlWhere.ToString());

                result.TargetName = sql.ToString();
            }

            return result;
        }





        /// <summary>
        /// 
        /// </summary>
        public override bool LoadFromXml(string xml, string xpath)
		{
            items.Clear();
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xml);

                XmlNodeList nodes = doc.SelectNodes(@"//results/" + xpath + "/datarow");

                foreach (XmlNode node in nodes)
                {
                    if (node != null)
                    {
                        int TemplateID = KxXmlUtils.GetNodeValue(node, "WordTemplateID", 0);
                        string Description = KxXmlUtils.GetNodeValue(node, "Description", "");

                        KxWordTemplateInfo template = new KxWordTemplateInfo(TemplateID, Description);

                       items.Add(template);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "LoadFromXml", ex.Message, xpath);
                LastError = ex.Message;
                return false;
            }
		}

        #region IEnumerable<KxWordTemplate> Members

        public IEnumerator<KxWordTemplateInfo> GetEnumerator()
		{
            return items.GetEnumerator();
        }

		#endregion

		#region IEnumerable Members

		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}

		#endregion
	}
}





