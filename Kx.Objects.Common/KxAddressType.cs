using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;

namespace Kx.Objects.Common
{
    [Serializable]
    public class KxAddressType : KxBaseClass
    {
        #region Attributes & Properties

        protected string   description;

        public int      AddressTypeId
        {
            get
            { return base.dBIdentifier;}
        }

        public string   Description
        {
            get
            { return description;}
            set
            { 
                AddToAudit("Description", description, value);
                description = value;
            }
        }
        
        #endregion 

        public KxAddressType(KxBaseClass parent)  : base(parent)
        {
        }

        public KxAddressType(KxBaseClass parent,int      addresstypeid) : this(parent)
        {
             base.dBIdentifier = addresstypeid;
             Load();
        }


        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("st2addresstypes", "ST2AddressTypes", TargetType.Table);

            result.AutoKeyField = new KxDataValue("AddressTypeId",AddressTypeId,KxValueType.Int);


            if ((actiontype == ActionType.Update) && (AddressTypeId == 0))
            {
                result.Action = ActionType.Insert;
            }
            else
            {
                result.Action = actiontype;
            }

            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {
                   result.AddParameter("description", description, KxValueType.String);
            }
            return result;
        }

        public override void LoadFromXml(string xml,XmlNode node)
        {
            description = KxXmlUtils.GetNodeValue(node, "description", "");
        }
    }
}

