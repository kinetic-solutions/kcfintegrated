using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Exceptions;
using Kx.Core.Utility;

namespace Kx.Objects.Common
{
    public class KxIndustrySectors : KxBaseClass
    {
        private List<KxIndustrySector> items = new List<KxIndustrySector>();

        public List<KxIndustrySector> Items
        {
            get
            { return items; }
        }

        public KxIndustrySector this[int index]
        {
            get
            { return items[index]; }
            set
            { items[index] = value; }
        }

        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = null;

            if (actiontype == ActionType.Select)
            {
                result = new KxData("industrysectors", "IndustrySectors", TargetType.Table, ActionType.Select);

                //result.Parameters.Add(new KxDataValue("keyfield", keyfield, KxValueType.Int));
            }
            return result;
        }

        public override bool LoadFromXml(string xml, string xpath)
        {
            items.Clear();
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xml);

                XmlNodeList nodes = doc.SelectNodes(@"//results/" + xpath + "/datarow");

                foreach (XmlNode node in nodes)
                {
                    if (node != null)
                    {
                        KxIndustrySector item = new KxIndustrySector(this, node);
                        items.Add(item);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "LoadFromXml", ex.Message, xpath);
                LastError = ex.Message;
                return false;
            }
        }

        public KxIndustrySector GetIndustrySector(int id)
        {
            KxIndustrySector result = null;

            foreach (KxIndustrySector i in Items)
            {
                if (i.IndustrySectorId == id)
                {
                    result = i;
                    break;
                }
            }
            return result;
        }

    }
}

