using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Exceptions;
using Kx.Core.Utility;

namespace Kx.Objects.Common
{
    public class KxRoomConfiguration : KxBaseClass
    {
        private int residentialRoomId;
        private List<KxRoomConfig> items = new List<KxRoomConfig>();

        public int ResidentialRoomId
        {
            get
            { return residentialRoomId; }
            set
            { residentialRoomId  = value; }
        }

        public List<KxRoomConfig> Items
        {
            get
            { return items; }
        }

        public KxRoomConfig this[int index]
        {
            get
            { return Items[index]; }
            set
            { Items[index] = value; }
        }

        public override KxData ConfigureData(ActionType actiontype)
        {
           KxData result = null;

            if (actiontype == ActionType.Select)
            {
                result = new KxData("st2roomconfig", "ST2RoomConfig", TargetType.Table);

                result.KeyFields.Add(new KxDataValue("residentialroomid", ResidentialRoomId, KxValueType.Int));
            }
            return result;
        }

        public KxRoomConfiguration(KxBaseClass parent,int residentialRoomId) : base(parent)
        {
            this.ResidentialRoomId = residentialRoomId;

            Load();
        }

        public override bool LoadFromXml(string xml, string xpath)
        {
            items.Clear();
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xml);

                XmlNodeList nodes = doc.SelectNodes(@"//results/" + xpath + "/datarow");

                foreach (XmlNode node in nodes)
                {
                    if (node != null)
                    {
                        KxRoomConfig roomcfg = new KxRoomConfig(this, node);
                        Items.Add(roomcfg);
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "LoadFromXml", ex.Message, xpath);
                LastError = ex.Message;
                return false;
            }
        }

        public KxRoomConfig GetRoomConfig(int academicYearId)
        {
            KxRoomConfig result = null;

            foreach (KxRoomConfig cfg in Items)
            {
                if (cfg.AcademicYearId == academicYearId)
                {
                    result = cfg;
                    break;
                }
            }
            return result;
        }

        public static bool CustomValueExists(int id, string value)
        {
            bool result = false;

            try
            {
                KxData data = new KxData("count", "ST2ROOMCONFIG", TargetType.Table);

                data.AddKeyField("customvalue"+id.ToString(), value, KxValueType.String);

                int currentCount = data.ExecuteScalarCount();

                result = currentCount > 0;
            }
            catch { }

            return result;
        }
    }
}
