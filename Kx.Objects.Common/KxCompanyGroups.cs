﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Exceptions;
using Kx.Core.Utility;

namespace Kx.Objects.Common
{
    public class KxCompanyGroups : KxBaseClass
    {
        private List<KxCompanyGroup> items = new List<KxCompanyGroup>();

        public List<KxCompanyGroup> Items
        {
            get
            { return items; }
        }

        public KxCompanyGroup this[int index]
        {
            get
            { return items[index]; }
            set
            { items[index] = value; }
        }

        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = null;

            if (actiontype == ActionType.Select)
            {
                result = new KxData("companygroups", "CompanyGroups", TargetType.Table, ActionType.Select);

                result.OrderFields.Add(new KxDataOrder("companygroupid"));

            }
            return result;
        }


        public KxCompanyGroups(KxBaseClass parent)
            : base(parent)
        {

        }


        public override bool LoadFromXml(string xml, string xpath)
        {
            items.Clear();
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xml);

                XmlNodeList nodes = doc.SelectNodes(@"//results/" + xpath + "/datarow");

                foreach (XmlNode node in nodes)
                {
                    if (node != null)
                    {
                        KxCompanyGroup item = new KxCompanyGroup(this, node);
                        Items.Add(item);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "LoadFromXml", ex.Message, xpath);
                LastError = ex.Message;
                return false;
            }
        }

        public KxCompanyGroup GetMultiItemById(int id)
        {
            KxCompanyGroup result = null;

            foreach (KxCompanyGroup item in Items)
            {
                if (item.CompanyGroupId == id)
                {
                    result = item;
                    break;
                }
            }

            return result;
        }
    }
}
