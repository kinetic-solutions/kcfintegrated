using System;
using System.Collections.Generic;
using System.Text;
using Kx.Core.Data;
using System.Xml;
using Kx.Core.Utility;
using System.Globalization;

namespace Kx.Objects.Common.Classes
{
    public class LastNumbers
    {
        public int GetLastNumber (string lastNumberType)
        {
            int result = -1;
            KxData getLastNumbers = new KxData("lastnumbers", "GetLastNumbers", TargetType.StoredProc);
            getLastNumbers.Action = ActionType.ExecuteProcedure;
            getLastNumbers.AddParameter("LastNumberType", lastNumberType, KxValueType.String);
            getLastNumbers.AddParameter("GetNext", "Y", KxValueType.String);
            getLastNumbers.AddParameter("NewNumber", 0, KxValueType.Int);
            getLastNumbers.AddParameter("Suppress", false, KxValueType.Boolean);

            if (getLastNumbers.Execute())
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(string.Format(@"<results>{0}</results>", getLastNumbers.XmlResults));

                XmlNodeList nodes = doc.SelectNodes(@"//results/lastnumbers/datarow");

                foreach (XmlNode node in nodes)
                {
                    result = KxXmlUtils.GetNodeValue(node, "lastnumber", 0);
                }
            }

            return result;
        }

        public string GetLastNumber (string lastNumberType, int numChars)
        {
            int number = GetLastNumber(lastNumberType);

            string format = "";

            for (int i = 0; i < numChars; i++)
            {
                format += "0";
            }

            return number.ToString(format);
        }
    }
}
