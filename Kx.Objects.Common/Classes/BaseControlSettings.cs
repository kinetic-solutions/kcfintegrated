using System;
using System.Collections.Generic;
using System.Text;
using Kx.Core.Encryption;
using Kx.Core.Utility;

namespace Kx.Objects.Common.Classes
{
    public class BaseControlSettings
    {
        #region Consts
        protected const string CRM_EMAIL = "CRM.Email";
        protected const string CRM_EMAIL_SERVER = "Server";
        protected const string CRM_EMAIL_PORT = "Port";
        protected const string CRM_EMAIL_REPLYTO = "ReplyTo";
        protected const string CRM_EMAIL_SENDFROM = "SendFrom";
        protected const string CRM_EMAIL_AUTHENTICATE = "Authenticate";
        protected const string CRM_EMAIL_USER = "UserName";
        protected const string CRM_EMAIL_PASSWORD = "Password";
        protected const string CRM_EMAIL_USEQUEUE = "UseQueue";
        #endregion

        #region Fields and Attributes
        #region Email Settings
        public string EmailServer
        {
            get
            {
                return InternalGetCustomSetting(CRM_EMAIL, CRM_EMAIL_SERVER, "");
            }
            set
            {
            	InternalSetCustomSetting(CRM_EMAIL, CRM_EMAIL_SERVER, value);
            }
        }

        public int EmailPort
        {
            get
            {
                return InternalGetCustomSetting(CRM_EMAIL, CRM_EMAIL_PORT, 25);
            }
            set
            {
            	InternalSetCustomSetting(CRM_EMAIL, CRM_EMAIL_PORT, value);
            }
        }

        public string EmailReplyTo
        {
            get
            {
                return InternalGetCustomSetting(CRM_EMAIL, CRM_EMAIL_REPLYTO, "");
            }
            set
            {
            	InternalSetCustomSetting(CRM_EMAIL, CRM_EMAIL_REPLYTO, value);
            }
        }

        public string EmailSendFrom
        {
            get
            {
                return InternalGetCustomSetting(CRM_EMAIL, CRM_EMAIL_SENDFROM, "");
            }
            set
            {
            	InternalSetCustomSetting(CRM_EMAIL, CRM_EMAIL_SENDFROM, value);
            }
        }

        public bool EmailAuthenticate
        {
            get
            {
                return InternalGetCustomSetting(CRM_EMAIL, CRM_EMAIL_AUTHENTICATE, false);
            }
            set
            {
            	InternalSetCustomSetting(CRM_EMAIL, CRM_EMAIL_AUTHENTICATE, value);
            }
        }

        public string EmailUserName
        {
            get
            {
                return InternalGetCustomSetting(CRM_EMAIL, CRM_EMAIL_USER, "");
            }
            set
            {
                InternalSetCustomSetting(CRM_EMAIL, CRM_EMAIL_USER, value);
            }
        }

        public string EmailPassword
        {
            get
            {
                string password = InternalGetCustomSetting(CRM_EMAIL, CRM_EMAIL_PASSWORD, "");
                if (string.IsNullOrEmpty(password))
                {
                    return string.Empty;
                }

                return Rijndael.DecryptString(KxBase64.Base64Decode(password), "email");
            }
            set
            {
            	string password = value;

                if (string.IsNullOrEmpty(password))
                {
                    InternalSetCustomSetting(CRM_EMAIL, CRM_EMAIL_PASSWORD, string.Empty);
                }
                else
                {
                    InternalSetCustomSetting(CRM_EMAIL, CRM_EMAIL_PASSWORD, KxBase64.Base64Encode(Rijndael.EncryptString(password, "email")));
                }
            }
        }

        public bool EmailUseQueue
        {
            get
            {
                return InternalGetCustomSetting(CRM_EMAIL, CRM_EMAIL_USEQUEUE, false);
            }
            set
            {
                InternalSetCustomSetting(CRM_EMAIL, CRM_EMAIL_USEQUEUE, value);
            }
        }
        #endregion
        #endregion

        #region Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="setting"></param>
        /// <param name="value"></param>
        protected void InternalSetCustomSettingNotes(string setting, string value)
        {
            int notesId = InternalGetCustomSetting(setting, "NotesId", 0);
            KxNote notes;

            if (notesId == 0)
            {
                notes = new KxNote(null);
                notes.UserId = 1;
                notes.DateTime = DateTime.Now;
            }
            else
            {
                notes = new KxNote(null, notesId);
            }

            notes.Notes = value;
            if (!notes.Save())
            {
                throw new Exception(notes.LastError);
            }
            notesId = notes.NotesId;
            InternalSetCustomSetting(setting, "NotesId", notesId.ToString());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="setting"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        protected string InternalGetCustomSettingNotes(string setting, string defaultValue)
        {
            int notesId = InternalGetCustomSetting(setting, "NotesId", 0);
            if (notesId == 0)
            {
                return defaultValue;
            }

            return InternalGetNotesValue(notesId);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="notesId"></param>
        /// <returns></returns>
        protected string InternalGetNotesValue(int notesId)
        {
            KxNote note = new KxNote(null, notesId);
            return note.Notes;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="customSetting"></param>
        /// <param name="customSettingValue"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        protected bool InternalGetCustomSetting(string customSetting, string customSettingValue, bool defaultValue)
        {
            return Boolean.Parse(InternalGetCustomSetting(customSetting, customSettingValue, defaultValue.ToString()));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="customSetting"></param>
        /// <param name="customSettingValue"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        protected string InternalGetCustomSetting(string customSetting, string customSettingValue, string defaultValue)
        {
            try
            {
                KxCustomSetting setting = new KxCustomSetting(null, customSetting, customSettingValue);
                if (!string.IsNullOrEmpty(setting.String1))
                {
                    return setting.String1;
                }
            }
            catch
            {
            }
            return defaultValue;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="customSetting"></param>
        /// <param name="customSettingValue"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        protected int InternalGetCustomSetting(string customSetting, string customSettingValue, int defaultValue)
        {
            try
            {
                string tmp = InternalGetCustomSetting(customSetting, customSettingValue, defaultValue.ToString());
                return int.Parse(tmp);
            }
            catch
            {
            }
            return defaultValue;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="customSetting"></param>
        /// <param name="customSettingValue"></param>
        /// <param name="string1Value"></param>
        protected void InternalSetCustomSetting(string customSetting, string customSettingValue, string string1Value)
        {
            KxCustomSetting setting = new KxCustomSetting(null, customSetting, customSettingValue);
            setting.String1 = string1Value;
            if (!setting.Save())
            {
                throw new Exception(setting.LastError);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="customSetting"></param>
        /// <param name="customSettingValue"></param>
        /// <param name="string1Value"></param>
        protected void InternalSetCustomSetting(string customSetting, string customSettingValue, string string1Value, string string2Value, string string3Value)
        {
            KxCustomSetting setting = new KxCustomSetting(null, customSetting, customSettingValue);
            setting.String1 = string1Value;
            setting.String2 = string2Value;
            setting.String3 = string3Value;
            if (!setting.Save())
            {
                throw new Exception(setting.LastError);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="customSetting"></param>
        /// <param name="customSettingValue"></param>
        /// <param name="value"></param>
        protected void InternalSetCustomSetting(string customSetting, string customSettingValue, bool value)
        {
            InternalSetCustomSetting(customSetting, customSettingValue, value.ToString());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="customSetting"></param>
        /// <param name="customSettingValue"></param>
        /// <param name="value"></param>
        protected void InternalSetCustomSetting(string customSetting, string customSettingValue, int value)
        {
            InternalSetCustomSetting(customSetting, customSettingValue, value.ToString());
        }

        #endregion
    }
}
