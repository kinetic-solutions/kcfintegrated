﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Kx.Objects.Common.Classes
{
    public class KxEncodedDaysOfWeek : IDisposable
    {
        public int DaysOfWeek { get; set; }
        public string DayDelimiter { get; set; }

        public KxEncodedDaysOfWeek (int daysOfWeek)
        {
            DaysOfWeek = daysOfWeek;
            DayDelimiter = ",";
        }

        public bool DateIsAvailableDay (DateTime date)
        {
            return
                date.DayOfWeek == DayOfWeek.Sunday && this.Sunday
                || date.DayOfWeek == DayOfWeek.Monday && this.Monday
                || date.DayOfWeek == DayOfWeek.Tuesday && this.Tuesday
                || date.DayOfWeek == DayOfWeek.Wednesday && this.Wednesday
                || date.DayOfWeek == DayOfWeek.Thursday && this.Thursday
                || date.DayOfWeek == DayOfWeek.Friday && this.Friday
                || date.DayOfWeek == DayOfWeek.Saturday && this.Saturday;
        }

        public bool Sunday
        {
            get { return (DaysOfWeek & 1) == 1 || DaysOfWeek == 0; }
        }

        public bool Monday
        {
            get { return (DaysOfWeek & 2) == 2 || DaysOfWeek == 0; }
        }

        public bool Tuesday
        {
            get { return (DaysOfWeek & 4) == 4 || DaysOfWeek == 0; }
        }

        public bool Wednesday
        {
            get { return (DaysOfWeek & 8) == 8 || DaysOfWeek == 0; }
        }

        public bool Thursday
        {
            get { return (DaysOfWeek & 16) == 16 || DaysOfWeek == 0; }
        }

        public bool Friday
        {
            get { return (DaysOfWeek & 32) == 32 || DaysOfWeek == 0; }
        }

        public bool Saturday
        {
            get { return (DaysOfWeek & 64) == 64 || DaysOfWeek == 0; }
        }

        public string DaysString
        {
            get
            {
                if (DaysOfWeek == 0)
                {
                    return "All";
                }
                else
                {
                    string result = string.Empty;

                    if (Monday) { result += string.Format("{0}{1}", (result != string.Empty ? DayDelimiter: ""),"M"); }
                    if (Tuesday) { result += string.Format("{0}{1}", (result != string.Empty ? DayDelimiter : ""), "Tu"); }
                    if (Wednesday) { result += string.Format("{0}{1}", (result != string.Empty ? DayDelimiter : ""), "W"); }
                    if (Thursday) { result += string.Format("{0}{1}", (result != string.Empty ? DayDelimiter : ""), "Th"); }
                    if (Friday) { result += string.Format("{0}{1}", (result != string.Empty ? DayDelimiter : ""), "F"); }
                    if (Saturday) { result += string.Format("{0}{1}", (result != string.Empty ? DayDelimiter : ""), "Sa"); }
                    if (Sunday) { result += string.Format("{0}{1}", (result != string.Empty ? DayDelimiter : ""), "Su"); }

                    return result;
                }
            }
        }

        #region IDisposable Members

        public void Dispose()
        {
        }

        #endregion
    }
}
