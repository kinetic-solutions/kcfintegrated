﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Exceptions;
using Kx.Core.Utility;

namespace Kx.Objects.Common
{
    public class KxSalesTypes : KxBaseClass
    {
        private List<KxSalesType> items = new List<KxSalesType>();

        public List<KxSalesType> Items
        {
            get
            { return items; }
        }

        public KxSalesType this[int index]
        {
            get
            { return items[index]; }
            set
            { items[index] = value; }
        }

        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = null;

            if (actiontype == ActionType.Select)
            {
                result = new KxData("salestype", "salestype", TargetType.Table, ActionType.Select);

                result.OrderFields.Add(new KxDataOrder("salestypeid"));
                
            }
            return result;
        }


        public KxSalesTypes(KxBaseClass parent) : base(parent)
        {

        }


        public override bool LoadFromXml(string xml, string xpath)
        {
            items.Clear();
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xml);

                XmlNodeList nodes = doc.SelectNodes(@"//results/" + xpath + "/datarow");

                foreach (XmlNode node in nodes)
                {
                    if (node != null)
                    {
                        KxSalesType salestype = new KxSalesType(this, node);
                        Items.Add(salestype);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "LoadFromXml", ex.Message, xpath);
                LastError = ex.Message;
                return false;
            }
        }

        protected int[] GetSalesTypeIDArray()
        {
            int loop = 0;

            int[] result = new int[Items.Count];

            foreach (KxSalesType salestype in Items)
            {
                result[loop] = salestype.SalesTypeId;
                loop++;
            }
            return result;
        }

        public KxSalesType GetSalesTypeById(int id)
        {
            KxSalesType result = null;

            foreach (KxSalesType salestype in Items)
            {
                if (salestype.SalesTypeId == id)
                {
                    result = salestype;
                    break;
                }
            }

            return result;
        }
    }
}
