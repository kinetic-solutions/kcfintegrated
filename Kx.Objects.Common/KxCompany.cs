using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;

namespace Kx.Objects.Common
{
    [Serializable]
    public class KxCompany : KxBaseClass
    {
        #region Attributes & Properties

        protected int      parentCompanyId;
        protected int      companyGroupId;
        protected string   name;
        protected int      companyTypeId;
        protected int      invoiceAddressId;
        protected int      warmthId;
        protected bool     award;
        protected double awardPercent;
        protected string   awardText;
        protected double yearStartMonth;
        protected int      employees;
        protected int      companyStatusId;
        protected string   companyStatusReason;
        protected DateTime ?creationDate;
        protected int      creationUserId;
        protected DateTime ?modifiedDate;
        protected int      modifiedUserId;
        protected string   vatregNumber;
        protected bool     internalRecharge;
        protected string   department;
        protected int      accountManagerPersonId;
        protected int      accountNo;
        protected bool     associate;
        protected DateTime ?associateExpireDate;
        protected int      associatePersonId;
        protected string   externalAccountCode;
        protected string   www;
        protected bool     prestigious;
        protected int      preferredAgencyCompanyId;
        protected decimal  turnOver;
        protected decimal  meetingValue;
        protected string   purchaseOrder;
        protected int      salesManagerUserId;
        protected bool     vatexempt;
        protected string   accountNumber;
        protected string   exemptTaxes;

        protected KxCompanyAddresseses _companyAddresses;

        
        public int      CompanyId
        {
            get
            { return base.dBIdentifier;}
        }

        public int      ParentCompanyId
        {
            get
            { return parentCompanyId;}
            set
            { 
                AddToAudit("ParentCompanyId", parentCompanyId, value);
                parentCompanyId = value;
            }
        }

        public int      CompanyGroupId
        {
            get
            { return companyGroupId;}
            set
            { 
                AddToAudit("CompanyGroupId", companyGroupId, value);
                companyGroupId = value;
            }
        }

        public string   Name
        {
            get
            { return name;}
            set
            { 
                AddToAudit("Name", name, value);
                name = value;
            }
        }

        public int      CompanyTypeId
        {
            get
            { return companyTypeId;}
            set
            { 
                AddToAudit("CompanyTypeId", companyTypeId, value);
                companyTypeId = value;
            }
        }

        public int      InvoiceAddressId
        {
            get
            { return invoiceAddressId;}
            set
            { 
                AddToAudit("InvoiceAddressId", invoiceAddressId, value);
                invoiceAddressId = value;
            }
        }

        public int      WarmthId
        {
            get
            { return warmthId;}
            set
            { 
                AddToAudit("WarmthId", warmthId, value);
                warmthId = value;
            }
        }

        public bool     Award
        {
            get
            { return award;}
            set
            { 
                AddToAudit("Award", award, value);
                award = value;
            }
        }

        public double AwardPercent
        {
            get
            { return awardPercent;}
            set
            { 
                AddToAudit("AwardPercent", awardPercent, value);
                awardPercent = value;
            }
        }

        public string   AwardText
        {
            get
            { return awardText;}
            set
            { 
                AddToAudit("AwardText", awardText, value);
                awardText = value;
            }
        }

        public double YearStartMonth
        {
            get
            { return yearStartMonth;}
            set
            { 
                AddToAudit("YearStartMonth", yearStartMonth, value);
                yearStartMonth = value;
            }
        }

        public int      Employees
        {
            get
            { return employees;}
            set
            { 
                AddToAudit("Employees", employees, value);
                employees = value;
            }
        }

        public int      CompanyStatusId
        {
            get
            { return companyStatusId;}
            set
            { 
                AddToAudit("CompanyStatusId", companyStatusId, value);
                companyStatusId = value;
            }
        }

        public string   CompanyStatusReason
        {
            get
            { return companyStatusReason;}
            set
            { 
                AddToAudit("CompanyStatusReason", companyStatusReason, value);
                companyStatusReason = value;
            }
        }

        public DateTime ?CreationDate
        {
            get
            { return creationDate;}
            set
            { 
                AddToAudit("CreationDate", creationDate, value);
                creationDate = value;
            }
        }

        public int      CreationUserId
        {
            get
            { return creationUserId;}
            set
            { 
                AddToAudit("CreationUserId", creationUserId, value);
                creationUserId = value;
            }
        }

        public DateTime ?ModifiedDate
        {
            get
            { return modifiedDate;}
            set
            { 
                AddToAudit("ModifiedDate", modifiedDate, value);
                modifiedDate = value;
            }
        }

        public int      ModifiedUserId
        {
            get
            { return modifiedUserId;}
            set
            { 
                AddToAudit("ModifiedUserId", modifiedUserId, value);
                modifiedUserId = value;
            }
        }

        public string   VatregNumber
        {
            get
            { return vatregNumber;}
            set
            { 
                AddToAudit("VatregNumber", vatregNumber, value);
                vatregNumber = value;
            }
        }

        public bool     InternalRecharge
        {
            get
            { return internalRecharge;}
            set
            { 
                AddToAudit("InternalRecharge", internalRecharge, value);
                internalRecharge = value;
            }
        }

        public string   Department
        {
            get
            { return department;}
            set
            { 
                AddToAudit("Department", department, value);
                department = value;
            }
        }

        public int      AccountManagerPersonId
        {
            get
            { return accountManagerPersonId;}
            set
            { 
                AddToAudit("AccountManagerPersonId", accountManagerPersonId, value);
                accountManagerPersonId = value;
            }
        }

        public int      AccountNo
        {
            get
            { return accountNo;}
            set
            { 
                AddToAudit("AccountNo", accountNo, value);
                accountNo = value;
            }
        }

        public bool     Associate
        {
            get
            { return associate;}
            set
            { 
                AddToAudit("Associate", associate, value);
                associate = value;
            }
        }

        public DateTime ?AssociateExpireDate
        {
            get
            { return associateExpireDate;}
            set
            { 
                AddToAudit("AssociateExpireDate", associateExpireDate, value);
                associateExpireDate = value;
            }
        }

        public int      AssociatePersonId
        {
            get
            { return associatePersonId;}
            set
            { 
                AddToAudit("AssociatePersonId", associatePersonId, value);
                associatePersonId = value;
            }
        }

        public string   ExternalAccountCode
        {
            get
            { return externalAccountCode;}
            set
            { 
                AddToAudit("ExternalAccountCode", externalAccountCode, value);
                externalAccountCode = value;
            }
        }

        public string   Www
        {
            get
            { return www;}
            set
            { 
                AddToAudit("Www", www, value);
                www = value;
            }
        }

        public bool     Prestigious
        {
            get
            { return prestigious;}
            set
            { 
                AddToAudit("Prestigious", prestigious, value);
                prestigious = value;
            }
        }

        public int      PreferredAgencyCompanyId
        {
            get
            { return preferredAgencyCompanyId;}
            set
            { 
                AddToAudit("PreferredAgencyCompanyId", preferredAgencyCompanyId, value);
                preferredAgencyCompanyId = value;
            }
        }

        public decimal  TurnOver
        {
            get
            { return turnOver;}
            set
            { 
                AddToAudit("TurnOver", turnOver, value);
                turnOver = value;
            }
        }

        public decimal  MeetingValue
        {
            get
            { return meetingValue;}
            set
            { 
                AddToAudit("MeetingValue", meetingValue, value);
                meetingValue = value;
            }
        }

        public string   PurchaseOrder
        {
            get
            { return purchaseOrder;}
            set
            { 
                AddToAudit("PurchaseOrder", purchaseOrder, value);
                purchaseOrder = value;
            }
        }

        public int      SalesManagerUserId
        {
            get
            { return salesManagerUserId;}
            set
            { 
                AddToAudit("SalesManagerUserId", salesManagerUserId, value);
                salesManagerUserId = value;
            }
        }

        public bool     Vatexempt
        {
            get
            { return vatexempt;}
            set
            { 
                AddToAudit("Vatexempt", vatexempt, value);
                vatexempt = value;
            }
        }

        public string ExemptTaxes
        {
            get
            { return exemptTaxes; }
            set
            {
                AddToAudit("ExemptTaxes", exemptTaxes, value);
                exemptTaxes = value;
            }
        }
        public string   AccountNumber
        {
            get
            { return accountNumber;}
            set
            { 
                AddToAudit("AccountNumber", accountNumber, value);
                accountNumber = value;
            }
        }

        public KxCompanyAddresseses CompanyAddresses
        {
            get
            {
                if (_companyAddresses == null)
                {
                    _companyAddresses = new KxCompanyAddresseses(CompanyId);
                }
                return _companyAddresses;
            }
        }



        #endregion 


        public KxCompany(KxBaseClass parent, XmlNode node) : base(parent)
        {
             LoadFromXml("", node);
        }

        public KxCompany(KxBaseClass parent, int companyid) : base(parent)
        {
             base.dBIdentifier = companyid;
             Load();
        }
        
        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("companies", "Companies", TargetType.Table);

            result.AutoKeyField = new KxDataValue("CompanyId",CompanyId,KxValueType.Int);


            if ((actiontype == ActionType.Update) && (CompanyId == 0))
            {
                result.Action = ActionType.Insert;
            }
            else
            {
                result.Action = actiontype;
            }

            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {
                   result.AddParameter("companyGroupId", companyGroupId, KxValueType.Int, true);
                   result.AddParameter("Name", name, KxValueType.String, 100, false);
                   result.AddParameter("companyTypeId", companyTypeId, KxValueType.Int, true);
                   result.AddParameter("invoiceAddressId", invoiceAddressId, KxValueType.Int, true);
                   result.AddParameter("warmthId", warmthId, KxValueType.Int, true);
                   result.AddParameter("award", award, KxValueType.Boolean, false);
                   result.AddParameter("awardPercent", awardPercent, KxValueType.Float, true);
                   result.AddParameter("awardText", awardText, KxValueType.String, 2147483647, true);
                   result.AddParameter("yearStartMonth", yearStartMonth, KxValueType.Float, true);
                   result.AddParameter("employees", employees, KxValueType.Int, true);
                   result.AddParameter("companyStatusId", companyStatusId, KxValueType.Int, true);
                   result.AddParameter("companyStatusReason", companyStatusReason, KxValueType.String, 50, true);
                   result.AddParameter("creationDate", creationDate, KxValueType.DateTime, true);
                   result.AddParameter("creationUserId", creationUserId, KxValueType.Int, false);
                   result.AddParameter("modifiedDate", modifiedDate, KxValueType.DateTime, true);
                   result.AddParameter("modifiedUserId", modifiedUserId, KxValueType.Int, false);
                   result.AddParameter("vatregNumber", vatregNumber, KxValueType.String, 30, true);
                   result.AddParameter("internalRecharge", internalRecharge, KxValueType.Boolean, false);
                   result.AddParameter("department", department, KxValueType.String, 10, true);
                   result.AddParameter("accountManagerPersonId", accountManagerPersonId, KxValueType.Int, true);
                   result.AddParameter("accountNo", accountNo, KxValueType.Int, true);
                   result.AddParameter("associate", associate, KxValueType.Boolean, true);
                   result.AddParameter("associateExpireDate", associateExpireDate, KxValueType.DateTime, true);
                   result.AddParameter("associatePersonId", associatePersonId, KxValueType.Int, true);
                   result.AddParameter("externalAccountCode", externalAccountCode, KxValueType.String, 50, true);
                   result.AddParameter("www", www, KxValueType.String, 255, true);
                   result.AddParameter("prestigious", prestigious, KxValueType.Boolean, true);
                   result.AddParameter("preferredAgencyCompanyId", preferredAgencyCompanyId, KxValueType.Int, true);
                   result.AddParameter("turnOver", turnOver, KxValueType.Decimal, true);
                   result.AddParameter("meetingValue", meetingValue, KxValueType.Decimal, true);
                   result.AddParameter("purchaseOrder", purchaseOrder, KxValueType.String, 50, true);
                   result.AddParameter("salesManagerUserId", salesManagerUserId, KxValueType.Int, true);
                   result.AddParameter("vatexempt", vatexempt, KxValueType.Boolean, true);
                   result.AddParameter("accountNumber", accountNumber, KxValueType.String, 30, true);
                   result.AddParameter("exemptTaxes", exemptTaxes, KxValueType.String, 30, true);
            }
            return result;
        }


        public override  void LoadFromXml(string xml,XmlNode node)
        {
            dBIdentifier  = KxXmlUtils.GetNodeValue(node,"companyid",0);
            parentCompanyId  = KxXmlUtils.GetNodeValue(node,"parentcompanyid",0);
            companyGroupId  = KxXmlUtils.GetNodeValue(node,"companygroupid",0);
            name  = KxXmlUtils.GetNodeValue(node,"name","");
            companyTypeId  = KxXmlUtils.GetNodeValue(node,"companytypeid",0);
            invoiceAddressId  = KxXmlUtils.GetNodeValue(node,"invoiceaddressid",0);
            warmthId  = KxXmlUtils.GetNodeValue(node,"warmthid",0);
            award  = KxXmlUtils.GetNodeValue(node,"award",false);
            awardPercent  = KxXmlUtils.GetNodeValue(node,"awardpercent",0f);
            awardText  = KxXmlUtils.GetNodeValue(node,"awardtext","");
            yearStartMonth  = KxXmlUtils.GetNodeValue(node,"yearstartmonth",0f);
            employees  = KxXmlUtils.GetNodeValue(node,"employees",0);
            companyStatusId  = KxXmlUtils.GetNodeValue(node,"companystatusid",0);
            companyStatusReason  = KxXmlUtils.GetNodeValue(node,"companystatusreason","");
            creationDate  = KxXmlUtils.GetNodeValueDT(node,"creationdate",null);
            creationUserId  = KxXmlUtils.GetNodeValue(node,"creationuserid",0);
            modifiedDate  = KxXmlUtils.GetNodeValueDT(node,"modifieddate",null);
            modifiedUserId  = KxXmlUtils.GetNodeValue(node,"modifieduserid",0);
            vatregNumber  = KxXmlUtils.GetNodeValue(node,"vatregnumber","");
            internalRecharge  = KxXmlUtils.GetNodeValue(node,"internalrecharge",false);
            department  = KxXmlUtils.GetNodeValue(node,"department","");
            accountManagerPersonId  = KxXmlUtils.GetNodeValue(node,"accountmanagerpersonid",0);
            accountNo  = KxXmlUtils.GetNodeValue(node,"accountno",0);
            associate  = KxXmlUtils.GetNodeValue(node,"associate",false);
            associateExpireDate  = KxXmlUtils.GetNodeValueDT(node,"associateexpiredate",null);
            associatePersonId  = KxXmlUtils.GetNodeValue(node,"associatepersonid",0);
            externalAccountCode  = KxXmlUtils.GetNodeValue(node,"externalaccountcode","");
            www  = KxXmlUtils.GetNodeValue(node,"www","");
            prestigious  = KxXmlUtils.GetNodeValue(node,"prestigious",false);
            preferredAgencyCompanyId  = KxXmlUtils.GetNodeValue(node,"preferredagencycompanyid",0);
            turnOver  = KxXmlUtils.GetNodeValue(node,"turnover",(decimal)0.00M);
            meetingValue  = KxXmlUtils.GetNodeValue(node,"meetingvalue",(decimal)0.00M);
            purchaseOrder  = KxXmlUtils.GetNodeValue(node,"purchaseorder","");
            salesManagerUserId  = KxXmlUtils.GetNodeValue(node,"salesmanageruserid",0);
            vatexempt  = KxXmlUtils.GetNodeValue(node,"vatexempt",false);
            accountNumber  = KxXmlUtils.GetNodeValue(node,"accountnumber","");
            exemptTaxes = KxXmlUtils.GetNodeValue(node, "exempttaxes", "");
            
        }
    }
}

