﻿using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;

namespace Kx.Objects.Common
{
    [Serializable]
    public class KxCompanyGroup : KxBaseClass
    {
        #region Attributes & Properties

        protected string name;
        protected string type;

        public int CompanyGroupId
        {
            get
            { return base.dBIdentifier; }
        }

        public string Name
        {
            get
            { return name; }
            set
            {
                AddToAudit("Name", name, value);
                name = value;
            }
        }

        public string Type
        {
            get
            { return type; }
            set
            {
                AddToAudit("Type", type, value);
                type = value;
            }
        }


        #endregion


        public KxCompanyGroup(KxBaseClass parent)
            : base(parent)
        { }


        public KxCompanyGroup(KxBaseClass parent, XmlNode node)
            : base(parent)
        {
            LoadFromXml("", node);
        }

        public KxCompanyGroup(KxBaseClass parent, int companygroupid)
            : base(parent)
        {
            base.dBIdentifier = companygroupid;
            Load();
        }


        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("companygroups", "CompanyGroups", TargetType.Table);

            result.AutoKeyField = new KxDataValue("CompanyGroupId", CompanyGroupId, KxValueType.Int);


            if ((actiontype == ActionType.Update) && (CompanyGroupId == 0))
            {
                result.Action = ActionType.Insert;
            }
            else
            {
                result.Action = actiontype;
            }

            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {
                result.AddParameter("type", type, KxValueType.String, 1, false);
            }
            return result;
        }


        public override void LoadFromXml(string xml, XmlNode node)
        {
            dBIdentifier = KxXmlUtils.GetNodeValue(node, "companygroupid", 0);
            name = KxXmlUtils.GetNodeValue(node, "name", "");
            type = KxXmlUtils.GetNodeValue(node, "type", "");
        }
    }
}
