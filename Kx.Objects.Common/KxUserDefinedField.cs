using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;

namespace Kx.Objects.Common
{
    [Serializable]
    public class KxUserDefinedField : KxBaseClass
    {
        #region Attributes & Properties

        protected string   fieldName;
        protected string   fieldType;
        protected bool     isCompanies;
        protected bool     isPeople;
        protected bool     isStudents;
        protected bool     isEvents;
        protected string   picklist;
        protected string   defaultValue;
        protected int      validationRequired;
        protected string   validationScript;
        protected bool     isStudentApplications;
        protected bool     isStudentCourses;
        protected string[] selectionList;

        public int      CustomFieldId
        {
            get
            { return base.dBIdentifier;}
        }
        public string   FieldName
        {
            get
            { return fieldName;}
            set
            { 
                AddToAudit("FieldName", fieldName, value);
                fieldName = value;
            }
        }
        public string   FieldType
        {
            get
            { return fieldType;}
            set
            { 
                AddToAudit("FieldType", fieldType, value);
                fieldType = value;
            }
        }
        public bool IsCompanies
        {
            get
            { return isCompanies;}
            set
            {
                AddToAudit("isCompanies", isCompanies, value);
                isCompanies = value;
            }
        }
        public bool IsPeople
        {
            get
            { return isPeople; }
            set
            {
                AddToAudit("isPeople", isPeople, value);
                isPeople = value;
            }
        }
        public bool IsStudents
        {
            get
            { return isStudents; }
            set
            {
                AddToAudit("isStudents", isStudents, value);
                isStudents = value;
            }
        }
        public bool IsEvents
        {
            get
            { return isEvents; }
            set
            {
                AddToAudit("isEvents", isEvents, value);
                isEvents = value;
            }
        }
        public string   Picklist
        {
            get
            { return picklist;}
            set
            { 
                AddToAudit("Picklist", picklist, value);
                picklist = value;
            }
        }
        public string   DefaultValue
        {
            get
            { return defaultValue;}
            set
            { 
                AddToAudit("DefaultValue", defaultValue, value);
                defaultValue = value;
            }
        }
        public int      ValidationRequired
        {
            get
            { return validationRequired;}
            set
            { 
                AddToAudit("ValidationRequired", validationRequired, value);
                validationRequired = value;
            }
        }
        public string   ValidationScript
        {
            get
            { return validationScript;}
            set
            { 
                AddToAudit("ValidationScript", validationScript, value);
                validationScript = value;
            }
        }
        public bool      IsStudentApplications
        {
            get
            { return isStudentApplications; }
            set
            {
                AddToAudit("isStudentApplications", isStudentApplications, value);
                isStudentApplications = value;
            }
        }
        public bool      IsStudentCourses
        {
            get
            { return isStudentCourses; }
            set
            {
                AddToAudit("isStudentCourses", isStudentCourses, value);
                isStudentCourses = value;
            }
        }
        public string[] SelectionList
        {
            get
            {
                if (selectionList == null || selectionList.Length == 0)
                {
                    GetSelectionList();
                }
                return selectionList;
            }
        }

        public bool IsPickList
        {
            get
            {
                return fieldType.ToUpper() == "PICK LIST";
            }
        }

        #endregion 

        public KxUserDefinedField(KxBaseClass parent) : base(parent)
        {
        }

        public KxUserDefinedField(KxBaseClass parent,int      customfieldid): base(parent)
        {
             base.dBIdentifier = customfieldid;
             Load();
        }

        public KxUserDefinedField(KxBaseClass parent, XmlNode node)
            : base(parent)
        {
            LoadFromXml("",node);
        }

            public KxUserDefinedField(KxBaseClass parent,KxUserDefinedField customField) : base(parent)
        {
            Clone(customField);
        }

        
        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("customfields2", "CustomFields2", TargetType.Table);

            result.AutoKeyField = new KxDataValue("CustomFieldId",CustomFieldId,KxValueType.Int);


            if ((actiontype == ActionType.Update) && (CustomFieldId == 0))
            {
                result.Action = ActionType.Insert;
            }
            else
            {
                result.Action = actiontype;
            }

            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {
                   result.AddParameter("fieldName", fieldName, KxValueType.String);
                   result.AddParameter("fieldType", fieldType, KxValueType.String);
                   result.AddParameter("companies", isCompanies, KxValueType.Int);
                   result.AddParameter("people", isPeople, KxValueType.Int);
                   result.AddParameter("students", isStudents, KxValueType.Int);
                   result.AddParameter("events", isEvents, KxValueType.Int);
                   result.AddParameter("picklist", picklist, KxValueType.String);
                   result.AddParameter("defaultValue", defaultValue, KxValueType.String);
                   result.AddParameter("validationRequired", validationRequired, KxValueType.Int);
                   result.AddParameter("validationScript", validationScript, KxValueType.String);
                   result.AddParameter("studentApplications", isStudentApplications, KxValueType.Int);
                   result.AddParameter("studentCourses", isStudentCourses, KxValueType.Int);
            }
            return result;
        }


        public override void LoadFromXml(string xml,XmlNode node)
        {
            dBIdentifier = KxXmlUtils.GetNodeValue(node, "customfieldid", 0);
            fieldName  = KxXmlUtils.GetNodeValue(node,"fieldname","");
            fieldType  = KxXmlUtils.GetNodeValue(node,"fieldtype","");
            isCompanies = KxXmlUtils.GetNodeValue(node, "companies", false);
            isPeople = KxXmlUtils.GetNodeValue(node, "people", false);
            isStudents = KxXmlUtils.GetNodeValue(node, "students", false);
            isEvents = KxXmlUtils.GetNodeValue(node, "events", false);
            picklist  = KxXmlUtils.GetNodeValue(node,"picklist","");
            defaultValue  = KxXmlUtils.GetNodeValue(node,"defaultvalue","");
            validationRequired  = KxXmlUtils.GetNodeValue(node,"validationrequired",0);
            validationScript  = KxXmlUtils.GetNodeValue(node,"validationscript","");
            isStudentApplications = KxXmlUtils.GetNodeValue(node, "studentapplications", false);
            isStudentCourses  = KxXmlUtils.GetNodeValue(node,"studentcourses",false);
        }

         public void Clone(KxUserDefinedField customfield)
         {
             this.fieldName = customfield.FieldName;
             this.fieldType = customfield.FieldType;
             this.isCompanies = customfield.IsCompanies;
             this.isPeople = customfield.IsPeople;
             this.isStudents = customfield.IsStudents;
             this.isEvents = customfield.IsEvents;
             this.picklist = customfield.Picklist;
             this.defaultValue = customfield.DefaultValue;
             this.validationRequired = customfield.ValidationRequired;
             this.validationScript = customfield.ValidationScript;
             this.isStudentApplications = customfield.IsStudentApplications;
             this.isStudentCourses = customfield.IsStudentCourses;
         }

        private void GetSelectionList()
        {
            if (fieldType.ToUpper() == "PICK LIST")
            {
                selectionList = picklist.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
            }
            if (fieldType.ToUpper() == "PICK SQL")
            {
                GetSelectionsFromSql();
            }
        }

        public bool Validate(string value)
        {
            return ValidateVerbose(value) == "";
        }

        public string ValidateVerbose(string value)
        {
            string result = "";

            if (fieldType.ToUpper() == "DATE")
            {
                try
                {
                    DateTime temp = Convert.ToDateTime(value);
                }
                catch 
                {
                    result += "Value must be a valid date";
                }

            }
            else if (fieldType == "INTEGER")
            {
                try
                {
                    int temp = Convert.ToInt32(value);
                }
                catch
                {
                    result += "Value must be a valid integer";
                }
            }
            else if (fieldType == "CURRENCY")
            {
                try
                {
                    double temp = Convert.ToDouble(value);
                }
                catch
                {
                    result += "Value must be a valid currency value";
                }
            }
            else if (fieldType == "PICK LIST")
            {
                if (!ListContainsValue(value))
                {
                    result += "Value must be an entry from the selection";
                }
            }
            else if (fieldType == "PICK SQL")
            {
                if (!ListContainsValue(value))
                {
                    result += "Value must be an entry from the selection";
                }
            }
            return result;
        }

        public bool ListContainsValue(string value)
        {
            bool result = false;

            foreach (string val in selectionList)
            {
                if (val == value)
                {
                    result = true;
                    break;
                }
            }
            return result;
        }

        private void GetSelectionsFromSql()
        {
            try
            {
                KxData dummy = new KxData("listdata", picklist, TargetType.Static, ActionType.Select);

                if (dummy.Execute())
                {
                    XmlDocument doc = new XmlDocument();
                    doc.LoadXml(dummy.XmlResults);

                    XmlNodeList nodes = doc.SelectNodes("//listdata/datarow");

                    selectionList = new string[nodes.Count];
                    int idx = 0;

                    foreach (XmlNode node in nodes)
                    {
                        if (node.FirstChild != null)
                        {
                            selectionList[idx] = node.FirstChild.Value;
                            idx++;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "GetSelectionsFromSql", ex.Message, "");
            }
        }
    }
}

