﻿using System;
using Kx.Core.Base;
using System.Xml;

namespace Kx.Objects.Common
{
    public class KxUnknownWebPublishedZone : KxWebPublishedZone
    {
        public KxUnknownWebPublishedZone (KxBaseClass parent)
            : base(parent)
        {
            this.ZoneType = KxWebPublishedZoneTypes.Unknown;
        }

        public KxUnknownWebPublishedZone(KxBaseClass parent, XmlNode node)
            : base (parent, node)
        {
        }

        public KxUnknownWebPublishedZone (KxBaseClass parent, int webPublishedZoneID)
            : base (parent, webPublishedZoneID)
        {
        }
    }
}
