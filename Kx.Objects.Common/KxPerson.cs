using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;

namespace Kx.Objects.Common
{
    [Serializable]
    public class KxPerson : KxBaseClass
    {
        #region Attributes & Properties

        protected int      addressId;
        protected string   surName;
        protected string   foreName;
        protected string   initials;
        protected string   title;
        protected string   sex;
        protected DateTime ?dob;
        protected int      companyId;
        protected int      contactRoleId;
        protected string   companyExt;
        protected string   salutation;
        protected string   telephone;
        protected string   telephone2;
        protected string   fax;
        protected string   mobile;
        protected string   addressHeader;
        protected string   email;
        protected bool     award;
        protected int      awardPercent;
        protected string   awardText;
        protected bool     refined;
        protected bool     resource;
        protected int      specialDietId;
        protected int      dietId;
        protected string   dietCode;
        protected string   subDiet;
        protected string   dietNotes;
        protected DateTime ?creationDate;
        protected int      creationUserId;
        protected DateTime ?modifiedDate;
        protected int      modifiedUserId;
        protected int      housekeepingReqNoteId;
        protected string   jobTitle;
        protected string   companyName;
        protected string   qualifications;
        protected string   notes;
        protected string   gp;
        protected string   medicalNotes;
        protected string   exerciseCat;
        protected int      enquirySourceId;
        protected bool     contact;
        protected bool     nominator;
        protected bool     leftCompany;
        protected bool     potentialTutor;
        protected bool     onMailList;
        protected bool     smoker;
        protected bool     disabled;
        protected bool     vip;
        protected int      companyAddressId;
        protected int      contactAddressId;
        protected string   externalId;
        protected string   webUserId;
        protected string   webPassword;
        protected string   salt;
        protected DateTime ?lockedAccountTime;
        protected bool     webAccessAllowed;
        protected int      retryCount;
        protected bool     emailVerified;
        protected bool     telephoneMarketing;
        protected bool     faxMarketing;
        protected bool     letterMarketing;
        protected string   marketingPreference;
        protected bool     inactive;
        protected string _invoiceLanguage;

        protected KxAddress address;
        protected KxCompany company = null;

        public int      PersonId
        {
            get
            { return base.dBIdentifier;}
        }

        public int      AddressId
        {
            get
            { return addressId;}
            set
            { 
                AddToAudit("AddressId", addressId, value);
                addressId = value;
            }
        }

        public string   SurName
        {
            get
            { return surName;}
            set
            { 
                AddToAudit("SurName", surName, value);
                surName = value;
            }
        }

        public string   ForeName
        {
            get
            { return foreName;}
            set
            { 
                AddToAudit("ForeName", foreName, value);
                foreName = value;
            }
        }

        public string   Initials
        {
            get
            { return initials;}
            set
            { 
                AddToAudit("Initials", initials, value);
                initials = value;
            }
        }

        public string   Title
        {
            get
            { return title;}
            set
            { 
                AddToAudit("Title", title, value);
                title = value;
            }
        }

        public string   Sex
        {
            get
            { return sex;}
            set
            { 
                AddToAudit("Sex", sex, value);
                sex = value;
            }
        }

        public DateTime ?Dob
        {
            get
            { return dob;}
            set
            { 
                AddToAudit("Dob", dob, value);
                dob = value;
            }
        }

        public int      CompanyId
        {
            get
            { return companyId;}
            set
            { 
                AddToAudit("CompanyId", companyId, value);
                companyId = value;
            }
        }

        public int      ContactRoleId
        {
            get
            { return contactRoleId;}
            set
            { 
                AddToAudit("ContactRoleId", contactRoleId, value);
                contactRoleId = value;
            }
        }

        public string   CompanyExt
        {
            get
            { return companyExt;}
            set
            { 
                AddToAudit("CompanyExt", companyExt, value);
                companyExt = value;
            }
        }

        public string   Salutation
        {
            get
            { return salutation;}
            set
            { 
                AddToAudit("Salutation", salutation, value);
                salutation = value;
            }
        }

        public string   Telephone
        {
            get
            { return telephone;}
            set
            { 
                AddToAudit("Telephone", telephone, value);
                telephone = value;
            }
        }

        public string   Telephone2
        {
            get
            { return telephone2;}
            set
            { 
                AddToAudit("Telephone2", telephone2, value);
                telephone2 = value;
            }
        }

        public string   Fax
        {
            get
            { return fax;}
            set
            { 
                AddToAudit("Fax", fax, value);
                fax = value;
            }
        }

        public string   Mobile
        {
            get
            { return mobile;}
            set
            { 
                AddToAudit("Mobile", mobile, value);
                mobile = value;
            }
        }

        public string   AddressHeader
        {
            get
            { return addressHeader;}
            set
            { 
                AddToAudit("AddressHeader", addressHeader, value);
                addressHeader = value;
            }
        }

        public string   Email
        {
            get
            { return email;}
            set
            { 
                AddToAudit("Email", email, value);
                email = value;
            }
        }

        public bool     Award
        {
            get
            { return award;}
            set
            { 
                AddToAudit("Award", award, value);
                award = value;
            }
        }

        public int      AwardPercent
        {
            get
            { return awardPercent;}
            set
            { 
                AddToAudit("AwardPercent", awardPercent, value);
                awardPercent = value;
            }
        }

        public string   AwardText
        {
            get
            { return awardText;}
            set
            { 
                AddToAudit("AwardText", awardText, value);
                awardText = value;
            }
        }

        public bool     Refined
        {
            get
            { return refined;}
            set
            { 
                AddToAudit("Refined", refined, value);
                refined = value;
            }
        }

        public bool     Resource
        {
            get
            { return resource;}
            set
            { 
                AddToAudit("Resource", resource, value);
                resource = value;
            }
        }

        public int      SpecialDietId
        {
            get
            { return specialDietId;}
            set
            { 
                AddToAudit("SpecialDietId", specialDietId, value);
                specialDietId = value;
            }
        }

        public int      DietId
        {
            get
            { return dietId;}
            set
            { 
                AddToAudit("DietId", dietId, value);
                dietId = value;
            }
        }

        public string   DietCode
        {
            get
            { return dietCode;}
            set
            { 
                AddToAudit("DietCode", dietCode, value);
                dietCode = value;
            }
        }

        public string   SubDiet
        {
            get
            { return subDiet;}
            set
            { 
                AddToAudit("SubDiet", subDiet, value);
                subDiet = value;
            }
        }

        public string   DietNotes
        {
            get
            { return dietNotes;}
            set
            { 
                AddToAudit("DietNotes", dietNotes, value);
                dietNotes = value;
            }
        }

        public DateTime ?CreationDate
        {
            get
            { return creationDate;}
            set
            { 
                AddToAudit("CreationDate", creationDate, value);
                creationDate = value;
            }
        }

        public int      CreationUserId
        {
            get
            { return creationUserId;}
            set
            { 
                AddToAudit("CreationUserId", creationUserId, value);
                creationUserId = value;
            }
        }

        public DateTime ?ModifiedDate
        {
            get
            { return modifiedDate;}
            set
            { 
                AddToAudit("ModifiedDate", modifiedDate, value);
                modifiedDate = value;
            }
        }

        public int      ModifiedUserId
        {
            get
            { return modifiedUserId;}
            set
            { 
                AddToAudit("ModifiedUserId", modifiedUserId, value);
                modifiedUserId = value;
            }
        }

        public int      HousekeepingReqNoteId
        {
            get
            { return housekeepingReqNoteId;}
            set
            { 
                AddToAudit("HousekeepingReqNoteId", housekeepingReqNoteId, value);
                housekeepingReqNoteId = value;
            }
        }

        public string   JobTitle
        {
            get
            { return jobTitle;}
            set
            { 
                AddToAudit("JobTitle", jobTitle, value);
                jobTitle = value;
            }
        }

        public string   CompanyName
        {
            get
            { return companyName;}
            set
            { 
                AddToAudit("CompanyName", companyName, value);
                companyName = value;
            }
        }

        public string   Qualifications
        {
            get
            { return qualifications;}
            set
            { 
                AddToAudit("Qualifications", qualifications, value);
                qualifications = value;
            }
        }

        public string   Notes
        {
            get
            { return notes;}
            set
            { 
                AddToAudit("Notes", notes, value);
                notes = value;
            }
        }

        public string   Gp
        {
            get
            { return gp;}
            set
            { 
                AddToAudit("Gp", gp, value);
                gp = value;
            }
        }

        public string   MedicalNotes
        {
            get
            { return medicalNotes;}
            set
            { 
                AddToAudit("MedicalNotes", medicalNotes, value);
                medicalNotes = value;
            }
        }

        public string   ExerciseCat
        {
            get
            { return exerciseCat;}
            set
            { 
                AddToAudit("ExerciseCat", exerciseCat, value);
                exerciseCat = value;
            }
        }

        public int      EnquirySourceId
        {
            get
            { return enquirySourceId;}
            set
            { 
                AddToAudit("EnquirySourceId", enquirySourceId, value);
                enquirySourceId = value;
            }
        }

        public bool     Contact
        {
            get
            { return contact;}
            set
            { 
                AddToAudit("Contact", contact, value);
                contact = value;
            }
        }

        public bool     Nominator
        {
            get
            { return nominator;}
            set
            { 
                AddToAudit("Nominator", nominator, value);
                nominator = value;
            }
        }

        public bool     LeftCompany
        {
            get
            { return leftCompany;}
            set
            { 
                AddToAudit("LeftCompany", leftCompany, value);
                leftCompany = value;
            }
        }

        public bool     PotentialTutor
        {
            get
            { return potentialTutor;}
            set
            { 
                AddToAudit("PotentialTutor", potentialTutor, value);
                potentialTutor = value;
            }
        }

        public bool     OnMailList
        {
            get
            { return onMailList;}
            set
            { 
                AddToAudit("OnMailList", onMailList, value);
                onMailList = value;
            }
        }

        public bool     Smoker
        {
            get
            { return smoker;}
            set
            { 
                AddToAudit("Smoker", smoker, value);
                smoker = value;
            }
        }

        public bool     Disabled
        {
            get
            { return disabled;}
            set
            { 
                AddToAudit("Disabled", disabled, value);
                disabled = value;
            }
        }

        public bool     Vip
        {
            get
            { return vip;}
            set
            { 
                AddToAudit("Vip", vip, value);
                vip = value;
            }
        }

        public int      CompanyAddressId
        {
            get
            { return companyAddressId;}
            set
            { 
                AddToAudit("CompanyAddressId", companyAddressId, value);
                companyAddressId = value;
            }
        }

        public int      ContactAddressId
        {
            get
            { return contactAddressId;}
            set
            { 
                AddToAudit("ContactAddressId", contactAddressId, value);
                contactAddressId = value;
            }
        }

        public string   ExternalId
        {
            get
            { return externalId;}
            set
            { 
                AddToAudit("ExternalId", externalId, value);
                externalId = value;
            }
        }

        public string   WebUserId
        {
            get
            { return webUserId;}
            set
            { 
                AddToAudit("WebUserId", webUserId, value);
                webUserId = value;
            }
        }

        public string   WebPassword
        {
            get
            { return webPassword;}
            set
            { 
                AddToAudit("WebPassword", webPassword, value);
                webPassword = value;
            }
        }

        public string   Salt
        {
            get
            { return salt;}
            set
            { 
                AddToAudit("Salt", salt, value);
                salt = value;
            }
        }

        public DateTime ?LockedAccountTime
        {
            get
            { return lockedAccountTime;}
            set
            { 
                AddToAudit("LockedAccountTime", lockedAccountTime, value);
                lockedAccountTime = value;
            }
        }

        public bool     WebAccessAllowed
        {
            get
            { return webAccessAllowed;}
            set
            { 
                AddToAudit("WebAccessAllowed", webAccessAllowed, value);
                webAccessAllowed = value;
            }
        }

        public int      RetryCount
        {
            get
            { return retryCount;}
            set
            { 
                AddToAudit("RetryCount", retryCount, value);
                retryCount = value;
            }
        }

        public bool     EmailVerified
        {
            get
            { return emailVerified;}
            set
            { 
                AddToAudit("EmailVerified", emailVerified, value);
                emailVerified = value;
            }
        }

        public bool     TelephoneMarketing
        {
            get
            { return telephoneMarketing;}
            set
            { 
                AddToAudit("TelephoneMarketing", telephoneMarketing, value);
                telephoneMarketing = value;
            }
        }

        public bool     FaxMarketing
        {
            get
            { return faxMarketing;}
            set
            { 
                AddToAudit("FaxMarketing", faxMarketing, value);
                faxMarketing = value;
            }
        }

        public bool     LetterMarketing
        {
            get
            { return letterMarketing;}
            set
            { 
                AddToAudit("LetterMarketing", letterMarketing, value);
                letterMarketing = value;
            }
        }

        public string   MarketingPreference
        {
            get
            { return marketingPreference;}
            set
            { 
                AddToAudit("MarketingPreference", marketingPreference, value);
                marketingPreference = value;
            }
        }

        public bool     Inactive
        {
            get
            { return inactive;}
            set
            { 
                AddToAudit("Inactive", inactive, value);
                inactive = value;
            }
        }

        public KxAddress Address
        {
            get
            {
                if (address == null && addressId > 0)
                {
                    address = new KxAddress(this, AddressId);
                }
                return address;
            }
           
        }

        public KxCompany Company
        {
            get
            {
                if (company == null && companyId > 0)
                {
                    company = new KxCompany(this, companyId);
                }
                return company;
            }
            set
            {
                company = value;
            }
        }

        public string InvoiceLanguage
        {
            get { return _invoiceLanguage; }
        }


        #endregion 

        public KxPerson(KxBaseClass parent) : base(parent)
        {
        }

        public KxPerson(KxBaseClass parent, int personid)
            : this(parent)
        {
             base.dBIdentifier = personid;
             Load();
        }

        public KxPerson(KxBaseClass parent, XmlNode node)
            : base (parent)
        {
            LoadFromXml("", node);
        }

        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("people", "People", TargetType.Table);

            result.AutoKeyField = new KxDataValue("PersonId",PersonId,KxValueType.Int);


            if ((actiontype == ActionType.Update) && (PersonId == 0))
            {
                result.Action = ActionType.Insert;
            }
            else
            {
                result.Action = actiontype;
            }

            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {
                   result.AddParameter("addressId", addressId, KxValueType.Int);
                   result.AddParameter("surName", surName, KxValueType.String);
                   result.AddParameter("foreName", foreName, KxValueType.String);
                   result.AddParameter("initials", initials, KxValueType.String);
                   result.AddParameter("title", title, KxValueType.String);
                   result.AddParameter("sex", sex, KxValueType.String);
                   result.AddParameter("dob", dob, KxValueType.DateTime);
                   result.AddParameter("companyId", companyId, KxValueType.Int);
                   result.AddParameter("contactRoleId", contactRoleId, KxValueType.Int);
                   result.AddParameter("companyExt", companyExt, KxValueType.String);
                   result.AddParameter("salutation", salutation, KxValueType.String);
                   result.AddParameter("telephone", telephone, KxValueType.String);
                   result.AddParameter("telephone2", telephone2, KxValueType.String);
                   result.AddParameter("fax", fax, KxValueType.String);
                   result.AddParameter("mobile", mobile, KxValueType.String);
                   result.AddParameter("addressHeader", addressHeader, KxValueType.String);
                   result.AddParameter("email", email, KxValueType.String);
                   result.AddParameter("award", award, KxValueType.Boolean);
                   result.AddParameter("awardPercent", awardPercent, KxValueType.Int);
                   result.AddParameter("awardText", awardText, KxValueType.String);
                   result.AddParameter("refined", refined, KxValueType.Boolean);
                   result.AddParameter("resource", resource, KxValueType.Boolean);
                   result.AddParameter("specialDietId", specialDietId, KxValueType.Int);
                   result.AddParameter("dietId", dietId, KxValueType.Int);
                   result.AddParameter("dietCode", dietCode, KxValueType.String);
                   result.AddParameter("subDiet", subDiet, KxValueType.String);
                   result.AddParameter("dietNotes", dietNotes, KxValueType.String);
                   result.AddParameter("creationDate", creationDate, KxValueType.DateTime);
                   result.AddParameter("creationUserId", creationUserId, KxValueType.Int);
                   result.AddParameter("modifiedDate", modifiedDate, KxValueType.DateTime);
                   result.AddParameter("modifiedUserId", modifiedUserId, KxValueType.Int);
                   result.AddParameter("housekeepingReqNoteId", housekeepingReqNoteId, KxValueType.Int);
                   result.AddParameter("jobTitle", jobTitle, KxValueType.String);
                   result.AddParameter("companyName", companyName, KxValueType.String);
                   result.AddParameter("qualifications", qualifications, KxValueType.String);
                   result.AddParameter("notes", notes, KxValueType.String);
                   result.AddParameter("gp", gp, KxValueType.String);
                   result.AddParameter("medicalNotes", medicalNotes, KxValueType.String);
                   result.AddParameter("exerciseCat", exerciseCat, KxValueType.String);
                   result.AddParameter("enquirySourceId", enquirySourceId, KxValueType.Int);
                   result.AddParameter("contact", contact, KxValueType.Boolean);
                   result.AddParameter("nominator", nominator, KxValueType.Boolean);
                   result.AddParameter("leftCompany", leftCompany, KxValueType.Boolean);
                   result.AddParameter("potentialTutor", potentialTutor, KxValueType.Boolean);
                   result.AddParameter("onMailList", onMailList, KxValueType.Boolean);
                   result.AddParameter("smoker", smoker, KxValueType.Boolean);
                   result.AddParameter("disabled", disabled, KxValueType.Boolean);
                   result.AddParameter("vip", vip, KxValueType.Boolean);
                   result.AddParameter("companyAddressId", companyAddressId, KxValueType.Int);
                   result.AddParameter("contactAddressId", contactAddressId, KxValueType.Int);
                   result.AddParameter("externalId", externalId, KxValueType.String);
                   result.AddParameter("webUserId", webUserId, KxValueType.String);
                   result.AddParameter("webPassword", webPassword, KxValueType.String);
                   result.AddParameter("salt", salt, KxValueType.String);
                   result.AddParameter("lockedAccountTime", lockedAccountTime, KxValueType.DateTime);
                   result.AddParameter("webAccessAllowed", webAccessAllowed, KxValueType.Boolean);
                   result.AddParameter("retryCount", retryCount, KxValueType.Int);
                   result.AddParameter("emailVerified", emailVerified, KxValueType.Boolean);
                   result.AddParameter("telephoneMarketing", telephoneMarketing, KxValueType.Boolean);
                   result.AddParameter("faxMarketing", faxMarketing, KxValueType.Boolean);
                   result.AddParameter("letterMarketing", letterMarketing, KxValueType.Boolean);
                   result.AddParameter("marketingPreference", marketingPreference, KxValueType.String);
                   result.AddParameter("inactive", inactive, KxValueType.Boolean);
            }
            return result;
        }

        public override void LoadFromXml(string xml, XmlNode node)
        {
            dBIdentifier = KxXmlUtils.GetNodeValue(node, "personId", 0);
            addressId = KxXmlUtils.GetNodeValue(node, "addressid", 0);
            surName = KxXmlUtils.GetNodeValue(node, "surname", "");
            foreName = KxXmlUtils.GetNodeValue(node, "forename", "");
            initials = KxXmlUtils.GetNodeValue(node, "initials", "");
            title = KxXmlUtils.GetNodeValue(node, "title", "");
            sex = KxXmlUtils.GetNodeValue(node, "sex", "");
            dob = KxXmlUtils.GetNodeValueDT(node, "dob", null);
            companyId = KxXmlUtils.GetNodeValue(node, "companyid", 0);
            contactRoleId = KxXmlUtils.GetNodeValue(node, "contactroleid", 0);
            companyExt = KxXmlUtils.GetNodeValue(node, "companyext", "");
            salutation = KxXmlUtils.GetNodeValue(node, "salutation", "");
            telephone = KxXmlUtils.GetNodeValue(node, "telephone", "");
            telephone2 = KxXmlUtils.GetNodeValue(node, "telephone2", "");
            fax = KxXmlUtils.GetNodeValue(node, "fax", "");
            mobile = KxXmlUtils.GetNodeValue(node, "mobile", "");
            addressHeader = KxXmlUtils.GetNodeValue(node, "addressheader", "");
            email = KxXmlUtils.GetNodeValue(node, "email", "");
            award = KxXmlUtils.GetNodeValue(node, "award", false);
            awardPercent = KxXmlUtils.GetNodeValue(node, "awardpercent", 0);
            awardText = KxXmlUtils.GetNodeValue(node, "awardtext", "");
            refined = KxXmlUtils.GetNodeValue(node, "refined", false);
            resource = KxXmlUtils.GetNodeValue(node, "resource", false);
            specialDietId = KxXmlUtils.GetNodeValue(node, "specialdietid", 0);
            dietId = KxXmlUtils.GetNodeValue(node, "dietid", 0);
            dietCode = KxXmlUtils.GetNodeValue(node, "dietcode", "");
            subDiet = KxXmlUtils.GetNodeValue(node, "subdiet", "");
            dietNotes = KxXmlUtils.GetNodeValue(node, "dietnotes", "");
            creationDate = KxXmlUtils.GetNodeValueDT(node, "creationdate", null);
            creationUserId = KxXmlUtils.GetNodeValue(node, "creationuserid", 0);
            modifiedDate = KxXmlUtils.GetNodeValueDT(node, "modifieddate", null);
            modifiedUserId = KxXmlUtils.GetNodeValue(node, "modifieduserid", 0);
            housekeepingReqNoteId = KxXmlUtils.GetNodeValue(node, "housekeepingreqnoteid", 0);
            jobTitle = KxXmlUtils.GetNodeValue(node, "jobtitle", "");
            companyName = KxXmlUtils.GetNodeValue(node, "companyname", "");
            qualifications = KxXmlUtils.GetNodeValue(node, "qualifications", "");
            notes = KxXmlUtils.GetNodeValue(node, "notes", "");
            gp = KxXmlUtils.GetNodeValue(node, "gp", "");
            medicalNotes = KxXmlUtils.GetNodeValue(node, "medicalnotes", "");
            exerciseCat = KxXmlUtils.GetNodeValue(node, "exercisecat", "");
            enquirySourceId = KxXmlUtils.GetNodeValue(node, "enquirysourceid", 0);
            contact = KxXmlUtils.GetNodeValue(node, "contact", false);
            nominator = KxXmlUtils.GetNodeValue(node, "nominator", false);
            leftCompany = KxXmlUtils.GetNodeValue(node, "leftcompany", false);
            potentialTutor = KxXmlUtils.GetNodeValue(node, "potentialtutor", false);
            onMailList = KxXmlUtils.GetNodeValue(node, "onmaillist", false);
            smoker = KxXmlUtils.GetNodeValue(node, "smoker", false);
            disabled = KxXmlUtils.GetNodeValue(node, "disabled", false);
            vip = KxXmlUtils.GetNodeValue(node, "vip", false);
            companyAddressId = KxXmlUtils.GetNodeValue(node, "companyaddressid", 0);
            contactAddressId = KxXmlUtils.GetNodeValue(node, "contactaddressid", 0);
            externalId = KxXmlUtils.GetNodeValue(node, "externalid", "");
            webUserId = KxXmlUtils.GetNodeValue(node, "webuserid", "");
            webPassword = KxXmlUtils.GetNodeValue(node, "webpassword", "");
            salt = KxXmlUtils.GetNodeValue(node, "salt", "");
            lockedAccountTime = KxXmlUtils.GetNodeValueDT(node, "lockedaccounttime", null);
            webAccessAllowed = KxXmlUtils.GetNodeValue(node, "webaccessallowed", false);
            retryCount = KxXmlUtils.GetNodeValue(node, "retrycount", 0);
            emailVerified = KxXmlUtils.GetNodeValue(node, "emailverified", false);
            telephoneMarketing = KxXmlUtils.GetNodeValue(node, "telephonemarketing", false);
            faxMarketing = KxXmlUtils.GetNodeValue(node, "faxmarketing", false);
            letterMarketing = KxXmlUtils.GetNodeValue(node, "lettermarketing", false);
            marketingPreference = KxXmlUtils.GetNodeValue(node, "marketingpreference", "");
            inactive = KxXmlUtils.GetNodeValue(node, "inactive", false);
            _invoiceLanguage = KxXmlUtils.GetNodeValue(node, "invoicelanguage", "en-gb");
        }
    }
}

