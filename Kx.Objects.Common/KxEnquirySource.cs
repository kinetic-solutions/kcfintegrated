using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;

namespace Kx.Objects.Common
{
    [Serializable]
    public class KxEnquirySource : KxBaseClass
    {
        #region Attributes & Properties

        protected string   description;
        protected bool     inactive;
        protected bool     enquiries;
        protected bool     propsects;
        protected bool     rfps;
        protected string   externalCode;

        public int      EnquirySourceId
        {
            get
            { return base.dBIdentifier;}
        }

        public string   Description
        {
            get
            { return description;}
            set
            { 
                AddToAudit("Description", description, value);
                description = value;
            }
        }

        public bool     Inactive
        {
            get
            { return inactive;}
            set
            { 
                AddToAudit("Inactive", inactive, value);
                inactive = value;
            }
        }

        public bool     Enquiries
        {
            get
            { return enquiries;}
            set
            { 
                AddToAudit("Enquiries", enquiries, value);
                enquiries = value;
            }
        }

        public bool     Propsects
        {
            get
            { return propsects;}
            set
            { 
                AddToAudit("Propsects", propsects, value);
                propsects = value;
            }
        }

        public bool     Rfps
        {
            get
            { return rfps;}
            set
            { 
                AddToAudit("Rfps", rfps, value);
                rfps = value;
            }
        }

        public string   ExternalCode
        {
            get
            { return externalCode;}
            set
            { 
                AddToAudit("ExternalCode", externalCode, value);
                externalCode = value;
            }
        }


        #endregion 


        public KxEnquirySource(KxBaseClass parent):base(parent)
        {}


        public KxEnquirySource(KxBaseClass parent, XmlNode node):base(parent)
        {
             LoadFromXml("", node);
        }

        public KxEnquirySource(KxBaseClass parent,int      enquirysourceid):base(parent)
        {
             base.dBIdentifier = enquirysourceid;
             Load();
        }


        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("enquirysources", "EnquirySources", TargetType.Table);

            result.AutoKeyField = new KxDataValue("EnquirySourceId",EnquirySourceId,KxValueType.Int);

			if ((actiontype == ActionType.Update) && (EnquirySourceId == 0))
            {
                result.Action = ActionType.Insert;
            }
            else
            {
                result.Action = actiontype;
            }

            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {
                   result.AddParameter("inactive", inactive, KxValueType.Boolean, false);
                   result.AddParameter("enquiries", enquiries, KxValueType.Boolean, true);
                   result.AddParameter("propsects", propsects, KxValueType.Boolean, true);
                   result.AddParameter("rfps", rfps, KxValueType.Boolean, true);
                   result.AddParameter("externalCode", externalCode, KxValueType.String, 255, true);
            }
            return result;
        }

        public override  void LoadFromXml(string xml,XmlNode node)
        {
            dBIdentifier  = KxXmlUtils.GetNodeValue(node,"enquirysourceid",0);
            description  = KxXmlUtils.GetNodeValue(node,"description","");
            inactive  = KxXmlUtils.GetNodeValue(node,"inactive",false);
            enquiries  = KxXmlUtils.GetNodeValue(node,"enquiries",false);
            propsects  = KxXmlUtils.GetNodeValue(node,"propsects",false);
            rfps  = KxXmlUtils.GetNodeValue(node,"rfps",false);
            externalCode  = KxXmlUtils.GetNodeValue(node,"externalcode","");
        }
    }
}

