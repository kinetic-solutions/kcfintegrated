using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;

namespace Kx.Objects.Common
{
	/// <remarks>
	/// <para>
	/// Revision History
	/// Author              Date        Changes
	/// Adam Stewart        06/02/2008  Added constructor that can be called by derived class
	/// </para>
	/// </remarks>
    [Serializable]
    public class KxCustomSetting : KxBaseClass
    {
        #region Attributes & Properties

        protected string   customSetting;
        protected string   value;
        protected string   string1 = "";
        protected string   string2 = "";
        protected string   string3 = "";
        protected string   string4 = "";

        /// <summary>
        /// 
        /// </summary>
        public int CustomSettingId
        {
            get { return base.dBIdentifier; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string CustomSetting
        {
            get { return customSetting; }
            set
            { 
                AddToAudit("CustomSetting", customSetting, value);
                customSetting = value;
            }
        }   
        /// <summary>
        /// 
        /// </summary>
        public string Value
        {
            get { return value; }
            set
            { 
                AddToAudit("Value", value, value);
                this.value = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public string AllString
        {
            get 
            {
                return string1 + string2 + string3 + string4;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public string   String1
        {
            get { return string1; }
            set
            { 
                AddToAudit("String1", string1, value);
                string1 = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public string String2
        {
            get { return string2; }
            set
            { 
                AddToAudit("String2", string2, value);
                string2 = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public string String3
        {
            get { return string3; }
            set
            { 
                AddToAudit("String3", string3, value);
                string3 = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public string String4
        {
            get { return string4; }
            set
            { 
                AddToAudit("String4", string4, value);
                string4 = value;
            }
        }
        
        #endregion 

		/// <summary>
		/// Calls the base constructor passing the parent.
		/// </summary>
		/// <param name="parent">Parent for the instance being created</param>
		protected KxCustomSetting(KxBaseClass parent)
			: base(parent)
		{

		}
        /// <summary>
        /// 
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="customsetting"></param>
        /// <param name="value"></param>
        public KxCustomSetting(KxBaseClass parent, string customsetting, string value)
            : base(parent)
        {
            CustomSetting = customsetting;
            Value = value;
            Load();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="customsetting"></param>
        /// <param name="value"></param>
        public KxCustomSetting(KxBaseClass parent, string customsetting, int value)
            : base(parent)
        {
            CustomSetting = customsetting;
            Value = value.ToString();
            Load();
        }

	    public KxCustomSetting(KxBaseClass parent, XmlNode node)
            : base (parent)
	    {
	        LoadFromXml("", node);
	    }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="actiontype"></param>
        /// <returns></returns>
        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("customsettings", "CustomSettings", TargetType.Table);

            result.AddKeyField("CustomSetting", CustomSetting, KxValueType.String);
            result.AddKeyField("Value", Value, KxValueType.String);
            
            if ((actiontype == ActionType.Update) && (CustomSettingId == 0))
            {
                result.Action = ActionType.Insert;
            }
            else
            {
                result.Action = actiontype;
            }

            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {
                   result.AddParameter("string1", string1, KxValueType.String);
                   result.AddParameter("string2", string2, KxValueType.String);
                   result.AddParameter("string3", string3, KxValueType.String);
                   result.AddParameter("string4", string4, KxValueType.String);
            }
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="xml"></param>
        /// <param name="node"></param>
        public override  void LoadFromXml(string xml,XmlNode node)
        {
            dBIdentifier    = KxXmlUtils.GetNodeValue(node,"customsettingid",0);
            customSetting   = KxXmlUtils.GetNodeValue(node,"customsetting","");
            value           = KxXmlUtils.GetNodeValue(node,"value","");
            string1         = KxXmlUtils.GetNodeValue(node,"string1","");
            string2         = KxXmlUtils.GetNodeValue(node,"string2","");
            string3         = KxXmlUtils.GetNodeValue(node,"string3","");
            string4         = KxXmlUtils.GetNodeValue(node,"string4","");
        }

	    public static IEnumerable<KxCustomSetting> GetCustomSettingsByCustomSetting(string customSetting)
	    {
	        KxData data = new KxData("customdata", "CustomSetting", TargetType.Table);
	        data.AddKeyField("customsetting", customSetting, KxValueType.String);

	        if (data.Execute())
	        {
	            XmlDocument doc  = new XmlDocument();
                doc.LoadXml(data.XmlResults);
	            XmlNodeList nodes = doc.SelectNodes(@"//customdata/datarow");

	            foreach (XmlNode node in nodes)
	            {
	                yield return new KxCustomSetting(null, node);
	            }
	        }

            throw new KxDataException(data.LastError);
	    }
        
        public static KxCustomSetting GetCustomSettingByCustomSetting(string customSetting)
	    {
	        KxData data = new KxData("customdata", "CustomSettings", TargetType.Table);
	        data.AddKeyField("customsetting", customSetting, KxValueType.String);

	        if (data.Execute())
	        {
	            XmlDocument doc  = new XmlDocument();
                doc.LoadXml(data.XmlResults);
	            XmlNodeList nodes = doc.SelectNodes(@"//customdata/datarow");

	            foreach (XmlNode node in nodes)
	            {
	                return new KxCustomSetting(null, node);
	            }
	        }
            else
	        {
	            throw new KxDataException(data.LastError);
	        }

            return new KxCustomSetting(null) {customSetting = customSetting, Value = string.Empty};
	    }
    }
}

