using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;

namespace Kx.Objects.Common
{
    [Serializable]
    public class KxStatus : KxBaseClass
    {
        #region Attributes & Properties

        protected string   statusType;
        protected string   description;
        protected string   backgroundColour;
        protected string   foregroundColour;
        protected Image    icon;
        protected bool     effectCapacity;
        protected bool     lockEvent;
        protected string   infoTitle;
        protected int      statusLevel;
        protected bool     forceQuote;
        protected bool     inactive;
        protected string   externalCode;

        public int      StatusId
        {
            get
            { return base.dBIdentifier;}
        }

        public string   StatusType
        {
            get
            { return statusType;}
            set
            { 
                AddToAudit("StatusType", statusType, value);
                statusType = value;
            }
        }

        public string   Description
        {
            get
            { return description;}
            set
            { 
                AddToAudit("Description", description, value);
                description = value;
            }
        }

        public string   BackgroundColour
        {
            get
            { return backgroundColour;}
            set
            { 
                AddToAudit("BackgroundColour", backgroundColour, value);
                backgroundColour = value;
            }
        }

        public string   ForegroundColour
        {
            get
            { return foregroundColour;}
            set
            { 
                AddToAudit("ForegroundColour", foregroundColour, value);
                foregroundColour = value;
            }
        }

        public Image    Icon
        {
            get
            { return icon;}
            set
            { 
                AddToAudit("Icon", icon, value);
                icon = value;
            }
        }

        public bool     EffectCapacity
        {
            get
            { return effectCapacity;}
            set
            { 
                AddToAudit("EffectCapacity", effectCapacity, value);
                effectCapacity = value;
            }
        }

        public bool     LockEvent
        {
            get
            { return lockEvent;}
            set
            { 
                AddToAudit("LockEvent", lockEvent, value);
                lockEvent = value;
            }
        }

        public string   InfoTitle
        {
            get
            { return infoTitle;}
            set
            { 
                AddToAudit("InfoTitle", infoTitle, value);
                infoTitle = value;
            }
        }

        public int      StatusLevel
        {
            get
            { return statusLevel;}
            set
            { 
                AddToAudit("StatusLevel", statusLevel, value);
                statusLevel = value;
            }
        }

        public bool     ForceQuote
        {
            get
            { return forceQuote;}
            set
            { 
                AddToAudit("ForceQuote", forceQuote, value);
                forceQuote = value;
            }
        }

        public bool     Inactive
        {
            get
            { return inactive;}
            set
            { 
                AddToAudit("Inactive", inactive, value);
                inactive = value;
            }
        }

        public string   ExternalCode
        {
            get
            { return externalCode;}
            set
            { 
                AddToAudit("ExternalCode", externalCode, value);
                externalCode = value;
            }
        }


        #endregion 


        public KxStatus(KxBaseClass parent):base(parent)
        {}


        public KxStatus(KxBaseClass parent, XmlNode node):base(parent)
        {
             LoadFromXml("", node);
        }

        public KxStatus(KxBaseClass parent,int  statusid):base(parent)
        {
             base.dBIdentifier = statusid;
             Load();
        }


        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("status", "Status", TargetType.Table);

            result.AutoKeyField = new KxDataValue("StatusId",StatusId,KxValueType.Int);


            if ((actiontype == ActionType.Update) && (StatusId == 0))
            {
                result.Action = ActionType.Insert;
            }
            else
            {
                result.Action = actiontype;
            }

            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {
                   result.AddParameter("description", description, KxValueType.String, 40, false);
                   result.AddParameter("backgroundColour", backgroundColour, KxValueType.String, 10, false);
                   result.AddParameter("foregroundColour", foregroundColour, KxValueType.String, 10, false);
                   result.AddParameter("icon", icon, KxValueType.Image, true);
                   result.AddParameter("effectCapacity", effectCapacity, KxValueType.Boolean, false);
                   result.AddParameter("lockEvent", lockEvent, KxValueType.Boolean, false);
                   result.AddParameter("infoTitle", infoTitle, KxValueType.String, 100, true);
                   result.AddParameter("statusLevel", statusLevel, KxValueType.Int, false);
                   result.AddParameter("forceQuote", forceQuote, KxValueType.Boolean, false);
                   result.AddParameter("inactive", inactive, KxValueType.Boolean, false);
                   result.AddParameter("externalCode", externalCode, KxValueType.String, 255, true);
            }
            return result;
        }


        public override  void LoadFromXml(string xml,XmlNode node)
        {
            dBIdentifier  = KxXmlUtils.GetNodeValue(node,"statusid",0);
            statusType  = KxXmlUtils.GetNodeValue(node,"statustype","");
            description  = KxXmlUtils.GetNodeValue(node,"description","");
            backgroundColour  = KxXmlUtils.GetNodeValue(node,"backgroundcolour","");
            foregroundColour  = KxXmlUtils.GetNodeValue(node,"foregroundcolour","");
            icon  = KxXmlUtils.GetNodeValue(node,"icon",(Image)null);
            effectCapacity  = KxXmlUtils.GetNodeValue(node,"effectcapacity",false);
            lockEvent  = KxXmlUtils.GetNodeValue(node,"lockevent",false);
            infoTitle  = KxXmlUtils.GetNodeValue(node,"infotitle","");
            statusLevel  = KxXmlUtils.GetNodeValue(node,"statuslevel",0);
            forceQuote  = KxXmlUtils.GetNodeValue(node,"forcequote",false);
            inactive  = KxXmlUtils.GetNodeValue(node,"inactive",false);
            externalCode  = KxXmlUtils.GetNodeValue(node,"externalcode","");
        }
    }
}

