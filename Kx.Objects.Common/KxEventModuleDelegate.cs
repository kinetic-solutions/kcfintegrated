using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;

namespace Kx.Objects.Common
{
    [Serializable]
    public class KxEventModuleDelegate : KxBaseClass
    {
        #region Attributes & Properties

        protected int      eventModuleId;
        protected int      personId;
        protected int      personSequenceId;
        protected int      delegateGroupId;
        protected DateTime ?arrivalDate;
        protected DateTime ?departureDate;
        protected DateTime ?checkedInDate;
        protected DateTime ?checkedOutDate;
        protected int      statusId;
        protected bool     attended;
        protected string   bedroom;
        protected string   displayCompany;
        protected bool     receivedMedical;
        protected bool     medicalOnSite;
        protected DateTime ?confirmationDate;
        protected int      modifiedUserId;
        protected int      accountManagerPersonId;
        protected string   type;
        protected DateTime ?creationDate;
        protected int      creationUserId;
        protected string   notes;
        protected bool     singleOccupancy;
        protected bool     resident;
        protected bool     potentialTutor;
        protected string   catering;
        protected bool     selected;
        protected string   checkInNotes;
        protected int      residentialRoomId;
        protected int      checkedInByUserId;
        protected int      checkedOutByUserId;
        protected int      blockId;
        protected int      bedroomTypeId;
        protected int      contactPersonId;
        protected int      numOccupants;
        protected int      matrixId;
        protected string   purchaseOrder;
        protected int      delegateTypeId;
        protected int      paymentTypeId;
        protected DateTime ?studentApplicationDate;
        protected int      studentApplicationTypeId;
        protected int      studentOfferStatusId;
        protected int      studentCurrentLicenceId;
        protected bool     allowBarPosting;
        protected DateTime ?studentAllocatedDate;
        protected int      studentAllocatedUserId;
        protected int      studentLicenceTypeId;
        protected int      studentCurrentOfferId;
        protected int      studentPeriodNumber;
        protected string   carRegNumber;
        protected int      routeChargesToPersonId;
        protected int      invoicePersonId;
        protected int      webBookingId;
        protected string   housekeepingNotes;
        protected string   phoneAccessCode;
        protected bool     defaultRegCardNote;
        protected bool     defaultCheckInNote;
        protected bool     defaultHKNotes;

        public int      EventModuleId
        {
            get
            { return eventModuleId;}
            set
            { 
                AddToAudit("EventModuleId", eventModuleId, value);
                eventModuleId = value;
            }
        }

        public int      PersonId
        {
            get
            { return personId;}
            set
            { 
                AddToAudit("PersonId", personId, value);
                personId = value;
            }
        }

        public int      PersonSequenceId
        {
            get
            { return personSequenceId;}
            set
            { 
                AddToAudit("PersonSequenceId", personSequenceId, value);
                personSequenceId = value;
            }
        }

        public int      DelegateGroupId
        {
            get
            { return delegateGroupId;}
            set
            { 
                AddToAudit("DelegateGroupId", delegateGroupId, value);
                delegateGroupId = value;
            }
        }

        public DateTime ?ArrivalDate
        {
            get
            { return arrivalDate;}
            set
            { 
                AddToAudit("ArrivalDate", arrivalDate, value);
                arrivalDate = value;
            }
        }

        public DateTime ?DepartureDate
        {
            get
            { return departureDate;}
            set
            { 
                AddToAudit("DepartureDate", departureDate, value);
                departureDate = value;
            }
        }

        public DateTime ?CheckedInDate
        {
            get
            { return checkedInDate;}
            set
            { 
                AddToAudit("CheckedInDate", checkedInDate, value);
                checkedInDate = value;
            }
        }

        public DateTime ?CheckedOutDate
        {
            get
            { return checkedOutDate;}
            set
            { 
                AddToAudit("CheckedOutDate", checkedOutDate, value);
                checkedOutDate = value;
            }
        }

        public int      StatusId
        {
            get
            { return statusId;}
            set
            { 
                AddToAudit("StatusId", statusId, value);
                statusId = value;
            }
        }

        public bool     Attended
        {
            get
            { return attended;}
            set
            { 
                AddToAudit("Attended", attended, value);
                attended = value;
            }
        }

        public string   Bedroom
        {
            get
            { return bedroom;}
            set
            { 
                AddToAudit("Bedroom", bedroom, value);
                bedroom = value;
            }
        }

        public string   DisplayCompany
        {
            get
            { return displayCompany;}
            set
            { 
                AddToAudit("DisplayCompany", displayCompany, value);
                displayCompany = value;
            }
        }

        public bool     ReceivedMedical
        {
            get
            { return receivedMedical;}
            set
            { 
                AddToAudit("ReceivedMedical", receivedMedical, value);
                receivedMedical = value;
            }
        }

        public bool     MedicalOnSite
        {
            get
            { return medicalOnSite;}
            set
            { 
                AddToAudit("MedicalOnSite", medicalOnSite, value);
                medicalOnSite = value;
            }
        }

        public DateTime ?ConfirmationDate
        {
            get
            { return confirmationDate;}
            set
            { 
                AddToAudit("ConfirmationDate", confirmationDate, value);
                confirmationDate = value;
            }
        }

        public int      ModifiedUserId
        {
            get
            { return modifiedUserId;}
            set
            { 
                AddToAudit("ModifiedUserId", modifiedUserId, value);
                modifiedUserId = value;
            }
        }

        public int      AccountManagerPersonId
        {
            get
            { return accountManagerPersonId;}
            set
            { 
                AddToAudit("AccountManagerPersonId", accountManagerPersonId, value);
                accountManagerPersonId = value;
            }
        }

        public string   Type
        {
            get
            { return type;}
            set
            { 
                AddToAudit("Type", type, value);
                type = value;
            }
        }

        public DateTime ?CreationDate
        {
            get
            { return creationDate;}
            set
            { 
                AddToAudit("CreationDate", creationDate, value);
                creationDate = value;
            }
        }

        public int      CreationUserId
        {
            get
            { return creationUserId;}
            set
            { 
                AddToAudit("CreationUserId", creationUserId, value);
                creationUserId = value;
            }
        }

        public string   Notes
        {
            get
            { return notes;}
            set
            { 
                AddToAudit("Notes", notes, value);
                notes = value;
            }
        }

        public bool     SingleOccupancy
        {
            get
            { return singleOccupancy;}
            set
            { 
                AddToAudit("SingleOccupancy", singleOccupancy, value);
                singleOccupancy = value;
            }
        }

        public bool     Resident
        {
            get
            { return resident;}
            set
            { 
                AddToAudit("Resident", resident, value);
                resident = value;
            }
        }

        public bool     PotentialTutor
        {
            get
            { return potentialTutor;}
            set
            { 
                AddToAudit("PotentialTutor", potentialTutor, value);
                potentialTutor = value;
            }
        }

        public string   Catering
        {
            get
            { return catering;}
            set
            { 
                AddToAudit("Catering", catering, value);
                catering = value;
            }
        }

        public bool     Selected
        {
            get
            { return selected;}
            set
            { 
                AddToAudit("Selected", selected, value);
                selected = value;
            }
        }

        public string   CheckInNotes
        {
            get
            { return checkInNotes;}
            set
            { 
                AddToAudit("CheckInNotes", checkInNotes, value);
                checkInNotes = value;
            }
        }

        public int      ResidentialRoomId
        {
            get
            { return residentialRoomId;}
            set
            { 
                AddToAudit("ResidentialRoomId", residentialRoomId, value);
                residentialRoomId = value;
            }
        }

        public int      CheckedInByUserId
        {
            get
            { return checkedInByUserId;}
            set
            { 
                AddToAudit("CheckedInByUserId", checkedInByUserId, value);
                checkedInByUserId = value;
            }
        }

        public int      CheckedOutByUserId
        {
            get
            { return checkedOutByUserId;}
            set
            { 
                AddToAudit("CheckedOutByUserId", checkedOutByUserId, value);
                checkedOutByUserId = value;
            }
        }

        public int      BlockId
        {
            get
            { return blockId;}
            set
            { 
                AddToAudit("BlockId", blockId, value);
                blockId = value;
            }
        }

        public int      BedroomTypeId
        {
            get
            { return bedroomTypeId;}
            set
            { 
                AddToAudit("BedroomTypeId", bedroomTypeId, value);
                bedroomTypeId = value;
            }
        }

        public int      ContactPersonId
        {
            get
            { return contactPersonId;}
            set
            { 
                AddToAudit("ContactPersonId", contactPersonId, value);
                contactPersonId = value;
            }
        }

        public int      NumOccupants
        {
            get
            { return numOccupants;}
            set
            { 
                AddToAudit("NumOccupants", numOccupants, value);
                numOccupants = value;
            }
        }

        public int      MatrixId
        {
            get
            { return matrixId;}
            set
            { 
                AddToAudit("MatrixId", matrixId, value);
                matrixId = value;
            }
        }

        public string   PurchaseOrder
        {
            get
            { return purchaseOrder;}
            set
            { 
                AddToAudit("PurchaseOrder", purchaseOrder, value);
                purchaseOrder = value;
            }
        }

        public int      DelegateTypeId
        {
            get
            { return delegateTypeId;}
            set
            { 
                AddToAudit("DelegateTypeId", delegateTypeId, value);
                delegateTypeId = value;
            }
        }

        public int      PaymentTypeId
        {
            get
            { return paymentTypeId;}
            set
            { 
                AddToAudit("PaymentTypeId", paymentTypeId, value);
                paymentTypeId = value;
            }
        }

        public DateTime ?StudentApplicationDate
        {
            get
            { return studentApplicationDate;}
            set
            { 
                AddToAudit("StudentApplicationDate", studentApplicationDate, value);
                studentApplicationDate = value;
            }
        }

        public int      StudentApplicationTypeId
        {
            get
            { return studentApplicationTypeId;}
            set
            { 
                AddToAudit("StudentApplicationTypeId", studentApplicationTypeId, value);
                studentApplicationTypeId = value;
            }
        }

        public int      StudentOfferStatusId
        {
            get
            { return studentOfferStatusId;}
            set
            { 
                AddToAudit("StudentOfferStatusId", studentOfferStatusId, value);
                studentOfferStatusId = value;
            }
        }

        public int      StudentCurrentLicenceId
        {
            get
            { return studentCurrentLicenceId;}
            set
            { 
                AddToAudit("StudentCurrentLicenceId", studentCurrentLicenceId, value);
                studentCurrentLicenceId = value;
            }
        }

        public bool     AllowBarPosting
        {
            get
            { return allowBarPosting;}
            set
            { 
                AddToAudit("AllowBarPosting", allowBarPosting, value);
                allowBarPosting = value;
            }
        }

        public DateTime ?StudentAllocatedDate
        {
            get
            { return studentAllocatedDate;}
            set
            { 
                AddToAudit("StudentAllocatedDate", studentAllocatedDate, value);
                studentAllocatedDate = value;
            }
        }

        public int      StudentAllocatedUserId
        {
            get
            { return studentAllocatedUserId;}
            set
            { 
                AddToAudit("StudentAllocatedUserId", studentAllocatedUserId, value);
                studentAllocatedUserId = value;
            }
        }

        public int      StudentLicenceTypeId
        {
            get
            { return studentLicenceTypeId;}
            set
            { 
                AddToAudit("StudentLicenceTypeId", studentLicenceTypeId, value);
                studentLicenceTypeId = value;
            }
        }

        public int      StudentCurrentOfferId
        {
            get
            { return studentCurrentOfferId;}
            set
            { 
                AddToAudit("StudentCurrentOfferId", studentCurrentOfferId, value);
                studentCurrentOfferId = value;
            }
        }

        public int      StudentPeriodNumber
        {
            get
            { return studentPeriodNumber;}
            set
            { 
                AddToAudit("StudentPeriodNumber", studentPeriodNumber, value);
                studentPeriodNumber = value;
            }
        }

        public string   CarRegNumber
        {
            get
            { return carRegNumber;}
            set
            { 
                AddToAudit("CarRegNumber", carRegNumber, value);
                carRegNumber = value;
            }
        }

        public int      RouteChargesToPersonId
        {
            get
            { return routeChargesToPersonId;}
            set
            { 
                AddToAudit("RouteChargesToPersonId", routeChargesToPersonId, value);
                routeChargesToPersonId = value;
            }
        }

        public int      InvoicePersonId
        {
            get
            { return invoicePersonId;}
            set
            { 
                AddToAudit("InvoicePersonId", invoicePersonId, value);
                invoicePersonId = value;
            }
        }

        public int      WebBookingId
        {
            get
            { return webBookingId;}
            set
            { 
                AddToAudit("WebBookingId", webBookingId, value);
                webBookingId = value;
            }
        }

        public string   HousekeepingNotes
        {
            get
            { return housekeepingNotes;}
            set
            { 
                AddToAudit("HousekeepingNotes", housekeepingNotes, value);
                housekeepingNotes = value;
            }
        }

        public string   PhoneAccessCode
        {
            get
            { return phoneAccessCode;}
            set
            { 
                AddToAudit("PhoneAccessCode", phoneAccessCode, value);
                phoneAccessCode = value;
            }
        }

        public bool     DefaultRegCardNote
        {
            get
            { return defaultRegCardNote;}
            set
            { 
                AddToAudit("DefaultRegCardNote", defaultRegCardNote, value);
                defaultRegCardNote = value;
            }
        }

        public bool     DefaultCheckInNote
        {
            get
            { return defaultCheckInNote;}
            set
            { 
                AddToAudit("DefaultCheckInNote", defaultCheckInNote, value);
                defaultCheckInNote = value;
            }
        }

        public bool     DefaultHKNotes
        {
            get
            { return defaultHKNotes;}
            set
            { 
                AddToAudit("DefaultHKNotes", defaultHKNotes, value);
                defaultHKNotes = value;
            }
        }


        #endregion 

        public KxEventModuleDelegate(KxBaseClass parent)
            : base(parent)
        {}

        public KxEventModuleDelegate(KxBaseClass parent, XmlNode node)
            : base(parent)
        {
             LoadFromXml("", node);
        }

        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("eventmoduledelegates", "EventModuleDelegates", TargetType.Table);
            
            result.AddKeyField("EventModuleID", eventModuleId, KxValueType.Int);
            result.AddKeyField("PersonID", personId, KxValueType.Int);
            result.AddKeyField("PersonSequenceID", personSequenceId, KxValueType.Int);

            result.Action = actiontype;

            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {
                   result.AddParameter("personId", personId, KxValueType.Int, false);
                   result.AddParameter("personSequenceId", personSequenceId, KxValueType.Int, false);
                   result.AddParameter("delegateGroupId", delegateGroupId, KxValueType.Int, true);
                   result.AddParameter("arrivalDate", arrivalDate, KxValueType.DateTime, true);
                   result.AddParameter("departureDate", departureDate, KxValueType.DateTime, true);
                   result.AddParameter("checkedInDate", checkedInDate, KxValueType.DateTime, true);
                   result.AddParameter("checkedOutDate", checkedOutDate, KxValueType.DateTime, true);
                   result.AddParameter("statusId", statusId, KxValueType.Int, false);
                   result.AddParameter("attended", attended, KxValueType.Boolean, true);
                   result.AddParameter("bedroom", bedroom, KxValueType.String, 50, true);
                   result.AddParameter("displayCompany", displayCompany, KxValueType.String, 50, true);
                   result.AddParameter("receivedMedical", receivedMedical, KxValueType.Boolean, true);
                   result.AddParameter("medicalOnSite", medicalOnSite, KxValueType.Boolean, true);
                   result.AddParameter("confirmationDate", confirmationDate, KxValueType.DateTime, true);
                   result.AddParameter("modifiedUserId", modifiedUserId, KxValueType.Int, false);
                   result.AddParameter("accountManagerPersonId", accountManagerPersonId, KxValueType.Int, true);
                   result.AddParameter("type", type, KxValueType.String, 1, true);
                   result.AddParameter("creationDate", creationDate, KxValueType.DateTime, true);
                   result.AddParameter("creationUserId", creationUserId, KxValueType.Int, false);
                   result.AddParameter("notes", notes, KxValueType.String, 2147483647, true);
                   result.AddParameter("singleOccupancy", singleOccupancy, KxValueType.Boolean, true);
                   result.AddParameter("resident", resident, KxValueType.Boolean, true);
                   result.AddParameter("potentialTutor", potentialTutor, KxValueType.Boolean, true);
                   result.AddParameter("catering", catering, KxValueType.String, 1, true);
                   result.AddParameter("selected", selected, KxValueType.Boolean, true);
                   result.AddParameter("checkInNotes", checkInNotes, KxValueType.String, 2147483647, true);
                   result.AddParameter("residentialRoomId", residentialRoomId, KxValueType.Int, true);
                   result.AddParameter("checkedInByUserId", checkedInByUserId, KxValueType.Int, true);
                   result.AddParameter("checkedOutByUserId", checkedOutByUserId, KxValueType.Int, true);
                   result.AddParameter("blockId", blockId, KxValueType.Int, true);
                   result.AddParameter("bedroomTypeId", bedroomTypeId, KxValueType.Int, true);
                   result.AddParameter("contactPersonId", contactPersonId, KxValueType.Int, true);
                   result.AddParameter("numOccupants", numOccupants, KxValueType.Int, false);
                   result.AddParameter("matrixId", matrixId, KxValueType.Int, true);
                   result.AddParameter("purchaseOrder", purchaseOrder, KxValueType.String, 50, true);
                   result.AddParameter("delegateTypeId", delegateTypeId, KxValueType.Int, true);
                   result.AddParameter("paymentTypeId", paymentTypeId, KxValueType.Int, true);
                   result.AddParameter("studentApplicationDate", studentApplicationDate, KxValueType.DateTime, true);
                   result.AddParameter("studentApplicationTypeId", studentApplicationTypeId, KxValueType.Int, true);
                   result.AddParameter("studentOfferStatusId", studentOfferStatusId, KxValueType.Int, true);
                   result.AddParameter("studentCurrentLicenceId", studentCurrentLicenceId, KxValueType.Int, true);
                   result.AddParameter("allowBarPosting", allowBarPosting, KxValueType.Boolean, false);
                   result.AddParameter("studentAllocatedDate", studentAllocatedDate, KxValueType.DateTime, true);
                   result.AddParameter("studentAllocatedUserId", studentAllocatedUserId, KxValueType.Int, true);
                   result.AddParameter("studentLicenceTypeId", studentLicenceTypeId, KxValueType.Int, true);
                   result.AddParameter("studentCurrentOfferId", studentCurrentOfferId, KxValueType.Int, true);
                   result.AddParameter("studentPeriodNumber", studentPeriodNumber, KxValueType.Int, true);
                   result.AddParameter("carRegNumber", carRegNumber, KxValueType.String, 20, true);
                   result.AddParameter("routeChargesToPersonId", routeChargesToPersonId, KxValueType.Int, true);
                   result.AddParameter("invoicePersonId", invoicePersonId, KxValueType.Int, true);
                   result.AddParameter("webBookingId", webBookingId, KxValueType.Int, false);
                   result.AddParameter("housekeepingNotes", housekeepingNotes, KxValueType.String, 2147483647, true);
                   result.AddParameter("phoneAccessCode", phoneAccessCode, KxValueType.String, 2, true);
                   result.AddParameter("defaultRegCardNote", defaultRegCardNote, KxValueType.Boolean, true);
                   result.AddParameter("defaultCheckInNote", defaultCheckInNote, KxValueType.Boolean, true);
                   result.AddParameter("defaultHKNotes", defaultHKNotes, KxValueType.Boolean, true);
            }
            return result;
        }


        public override  void LoadFromXml(string xml,XmlNode node)
        {
            eventModuleId  = KxXmlUtils.GetNodeValue(node,"eventmoduleid",0);
            personId  = KxXmlUtils.GetNodeValue(node,"personid",0);
            personSequenceId  = KxXmlUtils.GetNodeValue(node,"personsequenceid",0);
            delegateGroupId  = KxXmlUtils.GetNodeValue(node,"delegategroupid",0);
            arrivalDate  = KxXmlUtils.GetNodeValueDT(node,"arrivaldate",null);
            departureDate  = KxXmlUtils.GetNodeValueDT(node,"departuredate",null);
            checkedInDate  = KxXmlUtils.GetNodeValueDT(node,"checkedindate",null);
            checkedOutDate  = KxXmlUtils.GetNodeValueDT(node,"checkedoutdate",null);
            statusId  = KxXmlUtils.GetNodeValue(node,"statusid",0);
            attended  = KxXmlUtils.GetNodeValue(node,"attended",false);
            bedroom  = KxXmlUtils.GetNodeValue(node,"bedroom","");
            displayCompany  = KxXmlUtils.GetNodeValue(node,"displaycompany","");
            receivedMedical  = KxXmlUtils.GetNodeValue(node,"receivedmedical",false);
            medicalOnSite  = KxXmlUtils.GetNodeValue(node,"medicalonsite",false);
            confirmationDate  = KxXmlUtils.GetNodeValueDT(node,"confirmationdate",null);
            modifiedUserId  = KxXmlUtils.GetNodeValue(node,"modifieduserid",0);
            accountManagerPersonId  = KxXmlUtils.GetNodeValue(node,"accountmanagerpersonid",0);
            type  = KxXmlUtils.GetNodeValue(node,"type","");
            creationDate  = KxXmlUtils.GetNodeValueDT(node,"creationdate",null);
            creationUserId  = KxXmlUtils.GetNodeValue(node,"creationuserid",0);
            notes  = KxXmlUtils.GetNodeValue(node,"notes","");
            singleOccupancy  = KxXmlUtils.GetNodeValue(node,"singleoccupancy",false);
            resident  = KxXmlUtils.GetNodeValue(node,"resident",false);
            potentialTutor  = KxXmlUtils.GetNodeValue(node,"potentialtutor",false);
            catering  = KxXmlUtils.GetNodeValue(node,"catering","");
            selected  = KxXmlUtils.GetNodeValue(node,"selected",false);
            checkInNotes  = KxXmlUtils.GetNodeValue(node,"checkinnotes","");
            residentialRoomId  = KxXmlUtils.GetNodeValue(node,"residentialroomid",0);
            checkedInByUserId  = KxXmlUtils.GetNodeValue(node,"checkedinbyuserid",0);
            checkedOutByUserId  = KxXmlUtils.GetNodeValue(node,"checkedoutbyuserid",0);
            blockId  = KxXmlUtils.GetNodeValue(node,"blockid",0);
            bedroomTypeId  = KxXmlUtils.GetNodeValue(node,"bedroomtypeid",0);
            contactPersonId  = KxXmlUtils.GetNodeValue(node,"contactpersonid",0);
            numOccupants  = KxXmlUtils.GetNodeValue(node,"numoccupants",0);
            matrixId  = KxXmlUtils.GetNodeValue(node,"matrixid",0);
            purchaseOrder  = KxXmlUtils.GetNodeValue(node,"purchaseorder","");
            delegateTypeId  = KxXmlUtils.GetNodeValue(node,"delegatetypeid",0);
            paymentTypeId  = KxXmlUtils.GetNodeValue(node,"paymenttypeid",0);
            studentApplicationDate  = KxXmlUtils.GetNodeValueDT(node,"studentapplicationdate",null);
            studentApplicationTypeId  = KxXmlUtils.GetNodeValue(node,"studentapplicationtypeid",0);
            studentOfferStatusId  = KxXmlUtils.GetNodeValue(node,"studentofferstatusid",0);
            studentCurrentLicenceId  = KxXmlUtils.GetNodeValue(node,"studentcurrentlicenceid",0);
            allowBarPosting  = KxXmlUtils.GetNodeValue(node,"allowbarposting",false);
            studentAllocatedDate  = KxXmlUtils.GetNodeValueDT(node,"studentallocateddate",null);
            studentAllocatedUserId  = KxXmlUtils.GetNodeValue(node,"studentallocateduserid",0);
            studentLicenceTypeId  = KxXmlUtils.GetNodeValue(node,"studentlicencetypeid",0);
            studentCurrentOfferId  = KxXmlUtils.GetNodeValue(node,"studentcurrentofferid",0);
            studentPeriodNumber  = KxXmlUtils.GetNodeValue(node,"studentperiodnumber",0);
            carRegNumber  = KxXmlUtils.GetNodeValue(node,"carregnumber","");
            routeChargesToPersonId  = KxXmlUtils.GetNodeValue(node,"routechargestopersonid",0);
            invoicePersonId  = KxXmlUtils.GetNodeValue(node,"invoicepersonid",0);
            webBookingId  = KxXmlUtils.GetNodeValue(node,"webbookingid",0);
            housekeepingNotes  = KxXmlUtils.GetNodeValue(node,"housekeepingnotes","");
            phoneAccessCode  = KxXmlUtils.GetNodeValue(node,"phoneaccesscode","");
            defaultRegCardNote  = KxXmlUtils.GetNodeValue(node,"defaultregcardnote",false);
            defaultCheckInNote  = KxXmlUtils.GetNodeValue(node,"defaultcheckinnote",false);
            defaultHKNotes  = KxXmlUtils.GetNodeValue(node,"defaulthknotes",false);
        }
    }
}
