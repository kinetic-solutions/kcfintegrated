using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;

namespace Kx.Objects.Common
{
    [Serializable]
    public class KxRoomConfig : KxBaseClass
    {
        #region Attributes & Properties

        protected int      residentialRoomId;
        protected int      academicYearId;
        protected bool     inactive;
        protected int      courseTypeId;
        protected int      studentYearId;
        protected int      gender;
        protected int      singleSex;
        protected bool     smoking;
        protected bool     disabled;
        protected int      bedroomTypeId;
        protected bool     customField1;
        protected bool     customField2;
        protected string   customValue1;
        protected string   customValue2;
        protected string   customValue3;
        protected string   customValue4;
        protected string   customValue5;
        protected string   customValue6;
        protected string   customValue7;
        protected string   customValue8;
        protected int      capacity;
        protected int      chargeBand;
        protected bool     webPublished;
        protected int      webAdditionalTextNotesId;
        protected int      webLargeProperty;
        protected int      webPublishVacancy;
        protected int      allocationModelId;
        protected int      defaultPricingModelId;
        protected DateTime ?defaultArrivalDate;
        protected DateTime ?defaultDepartureDate;
        protected int      defaultWordTemplateId;
        protected int      defaultOfferTemplateId;
        protected bool     lockAllocationModel;
        protected bool     lockPricingModel;
        protected bool     lockOfferTemplate;
        protected bool     lockLicenceTemplate;

        protected KxBedroomType bedroomType = null;
        protected KxResidentialRoom residentialRoom = null;

        public int      RoomConfigId
        {
            get
            { return base.dBIdentifier;}
        }

        public int      ResidentialRoomId
        {
            get
            { return residentialRoomId;}
            set
            { 
                AddToAudit("ResidentialRoomId", residentialRoomId, value);
                residentialRoomId = value;
            }
        }

        public int      AcademicYearId
        {
            get
            { return academicYearId;}
            set
            { 
                AddToAudit("AcademicYearId", academicYearId, value);
                academicYearId = value;
            }
        }

        public bool     Inactive
        {
            get
            { return inactive;}
            set
            { 
                AddToAudit("Inactive", inactive, value);
                inactive = value;
            }
        }

        public int      CourseTypeId
        {
            get
            { return courseTypeId;}
            set
            { 
                AddToAudit("CourseTypeId", courseTypeId, value);
                courseTypeId = value;
            }
        }

        public int      StudentYearId
        {
            get
            { return studentYearId;}
            set
            { 
                AddToAudit("StudentYearId", studentYearId, value);
                studentYearId = value;
            }
        }

        public int      Gender
        {
            get
            { return gender;}
            set
            { 
                AddToAudit("Gender", gender, value);
                gender = value;
            }
        }


        public string GenderString
        {
            get
            { return GetGenderString(gender); }
            
        }

        public int      SingleSex
        {
            get
            { return singleSex;}
            set
            { 
                AddToAudit("SingleSex", singleSex, value);
                singleSex = value;
            }
        }

        public bool     Smoking
        {
            get
            { return smoking;}
            set
            { 
                AddToAudit("Smoking", smoking, value);
                smoking = value;
            }
        }

        public bool     Disabled
        {
            get
            { return disabled;}
            set
            { 
                AddToAudit("Disabled", disabled, value);
                disabled = value;
            }
        }

        public int      BedroomTypeId
        {
            get
            { return bedroomTypeId;}
            set
            { 
                AddToAudit("BedroomTypeId", bedroomTypeId, value);
                bedroomTypeId = value;
            }
        }

        public bool     CustomField1
        {
            get
            { return customField1;}
            set
            { 
                AddToAudit("CustomField1", customField1, value);
                customField1 = value;
            }
        }

        public bool     CustomField2
        {
            get
            { return customField2;}
            set
            { 
                AddToAudit("CustomField2", customField2, value);
                customField2 = value;
            }
        }

        public string CustomField1String
        {
            get
            {
                if (CustomField1)
                {
                    return "Yes";
                }

                else
                {
                    return "No";
                }

            }
        }

        public string CustomField2String
        {
            get
            {
                if (CustomField2)
                {
                    return "Yes";
                }   

                else
                {
                    return "No";
                }
            
            }
            
        }

        public string   CustomValue1
        {
            get
            { return customValue1;}
            set
            { 
                AddToAudit("CustomValue1", customValue1, value);
                customValue1 = value;
            }
        }

        public string   CustomValue2
        {
            get
            { return customValue2;}
            set
            { 
                AddToAudit("CustomValue2", customValue2, value);
                customValue2 = value;
            }
        }

        public string   CustomValue3
        {
            get
            { return customValue3;}
            set
            { 
                AddToAudit("CustomValue3", customValue3, value);
                customValue3 = value;
            }
        }

        public string   CustomValue4
        {
            get
            { return customValue4;}
            set
            { 
                AddToAudit("CustomValue4", customValue4, value);
                customValue4 = value;
            }
        }

        
        public string CustomValue5
        {
            get
            { return customValue5; }
            set
            {
                AddToAudit("CustomValue5", customValue5, value);
                customValue5 = value;
            }
        }

        public string CustomValue6
        {
            get
            { return customValue6; }
            set
            {
                AddToAudit("CustomValue6", customValue6, value);
                customValue6 = value;
            }
        }

        public string CustomValue7
        {
            get
            { return customValue7; }
            set
            {
                AddToAudit("CustomValue7", customValue7, value);
                customValue7 = value;
            }
        }
        public string CustomValue8
        {
            get
            { return customValue8; }
            set
            {
                AddToAudit("CustomValue8", customValue8, value);
                customValue8 = value;
            }
        }

        public int      Capacity
        {
            get
            { return capacity;}
            set
            { 
                AddToAudit("Capacity", capacity, value);
                capacity = value;
            }
        }

        public int      ChargeBand
        {
            get
            { return chargeBand;}
            set
            { 
                AddToAudit("ChargeBand", chargeBand, value);
                chargeBand = value;
            }
        }

        public bool     WebPublished
        {
            get
            { return webPublished;}
            set
            { 
                AddToAudit("WebPublished", webPublished, value);
                webPublished = value;
            }
        }

        public int      WebAdditionalTextNotesId
        {
            get
            { return webAdditionalTextNotesId;}
            set
            { 
                AddToAudit("WebAdditionalTextNotesId", webAdditionalTextNotesId, value);
                webAdditionalTextNotesId = value;
            }
        }

        public int      WebLargeProperty
        {
            get
            { return webLargeProperty;}
            set
            { 
                AddToAudit("WebLargeProperty", webLargeProperty, value);
                webLargeProperty = value;
            }
        }

        public int      WebPublishVacancy
        {
            get
            { return webPublishVacancy;}
            set
            { 
                AddToAudit("WebPublishVacancy", webPublishVacancy, value);
                webPublishVacancy = value;
            }
        }

        public int      AllocationModelId
        {
            get
            { return allocationModelId;}
            set
            { 
                AddToAudit("AllocationModelId", allocationModelId, value);
                allocationModelId = value;
            }
        }

        public int      DefaultPricingModelId
        {
            get
            { return defaultPricingModelId;}
            set
            { 
                AddToAudit("DefaultPricingModelId", defaultPricingModelId, value);
                defaultPricingModelId = value;
            }
        }

        public DateTime ?DefaultArrivalDate
        {
            get
            { return defaultArrivalDate;}
            set
            { 
                AddToAudit("DefaultArrivalDate", defaultArrivalDate, value);
                defaultArrivalDate = value;
            }
        }

        public DateTime ?DefaultDepartureDate
        {
            get
            { return defaultDepartureDate;}
            set
            { 
                AddToAudit("DefaultDepartureDate", defaultDepartureDate, value);
                defaultDepartureDate = value;
            }
        }

        public int      DefaultWordTemplateId
        {
            get
            { return defaultWordTemplateId;}
            set
            { 
                AddToAudit("DefaultWordTemplateId", defaultWordTemplateId, value);
                defaultWordTemplateId = value;
            }
        }

        public int      DefaultOfferTemplateId
        {
            get
            { return defaultOfferTemplateId;}
            set
            { 
                AddToAudit("DefaultOfferTemplateId", defaultOfferTemplateId, value);
                defaultOfferTemplateId = value;
            }
        }

        public bool     LockAllocationModel
        {
            get
            { return lockAllocationModel;}
            set
            { 
                AddToAudit("LockAllocationModel", lockAllocationModel, value);
                lockAllocationModel = value;
            }
        }

        public bool     LockPricingModel
        {
            get
            { return lockPricingModel;}
            set
            { 
                AddToAudit("LockPricingModel", lockPricingModel, value);
                lockPricingModel = value;
            }
        }

        public bool     LockOfferTemplate
        {
            get
            { return lockOfferTemplate;}
            set
            { 
                AddToAudit("LockOfferTemplate", lockOfferTemplate, value);
                lockOfferTemplate = value;
            }
        }

        public bool     LockLicenceTemplate
        {
            get
            { return lockLicenceTemplate;}
            set
            { 
                AddToAudit("LockLicenceTemplate", lockLicenceTemplate, value);
                lockLicenceTemplate = value;
            }
        }

        public string BedroomTypeName
        {
            get
            {
                if (BedroomType != null)
                {
                    return BedroomType.Description;
                }
                else
                {
                    return "";
                }
            }
        }

        public KxBedroomType BedroomType
        {
            get
            {
                if (bedroomType == null && bedroomTypeId > 0)
                {
                    bedroomType = new KxBedroomType(this, bedroomTypeId);
                }
                return bedroomType;
            }
            set
            {
                if (bedroomType == value)
                    return;
                bedroomType = value;
            }
        }

        public string SingleSexString
        {
            get
            {
                return GetSingleSexString();
            }
        }


        public KxResidentialRoom ResidentialRoom
        {
            get
            {
                if (residentialRoom == null && ResidentialRoomId > 0)
                {
                    residentialRoom = new KxResidentialRoom(this, ResidentialRoomId);
                }
                return residentialRoom;
            }
            set
            {
                if (residentialRoom == value)
                    return;
                residentialRoom = value;
            }
        }
        
        #endregion 

        public KxRoomConfig(int      roomconfigid)
        {
             base.dBIdentifier = roomconfigid;
             Load();
        }

        public KxRoomConfig(KxBaseClass parent, XmlNode node) : base(parent)
        {
            LoadFromXml("",node);
        }

        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("st2roomconfig", "ST2RoomConfig", TargetType.StoredProc);

            result.AutoKeyField = new KxDataValue("RoomConfigId",RoomConfigId,KxValueType.Int);


            if ((actiontype == ActionType.Update) && (RoomConfigId == 0))
            {
                result.Action = ActionType.Insert;
            }
            else
            {
                result.Action = actiontype;
            }

            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {
                   result.AddParameter("residentialRoomId", residentialRoomId, KxValueType.Int);
                   result.AddParameter("academicYearId", academicYearId, KxValueType.Int);
                   result.AddParameter("inactive", inactive, KxValueType.Boolean);
                   result.AddParameter("courseTypeId", courseTypeId, KxValueType.Int);
                   result.AddParameter("studentYearId", studentYearId, KxValueType.Int);
                   result.AddParameter("gender", gender, KxValueType.Int);
                   result.AddParameter("singleSex", singleSex, KxValueType.Int);
                   result.AddParameter("smoking", smoking, KxValueType.Boolean);
                   result.AddParameter("disabled", disabled, KxValueType.Boolean);
                   result.AddParameter("bedroomTypeId", bedroomTypeId, KxValueType.Int);
                   result.AddParameter("customField1", customField1, KxValueType.Boolean);
                   result.AddParameter("customField2", customField2, KxValueType.Boolean);
                   result.AddParameter("customValue1", customValue1, KxValueType.String);
                   result.AddParameter("customValue2", customValue2, KxValueType.String);
                   result.AddParameter("customValue3", customValue3, KxValueType.String);
                   result.AddParameter("customValue4", customValue4, KxValueType.String);
                   result.AddParameter("customValue5", customValue5, KxValueType.String);
                   result.AddParameter("customValue6", customValue6, KxValueType.String);
                   result.AddParameter("customValue7", customValue7, KxValueType.String);
                   result.AddParameter("customValue8", customValue8, KxValueType.String);
                   result.AddParameter("capacity", capacity, KxValueType.Int);
                   result.AddParameter("chargeBand", chargeBand, KxValueType.Int);
                   result.AddParameter("webPublished", webPublished, KxValueType.Boolean);
                   result.AddParameter("webAdditionalTextNotesId", webAdditionalTextNotesId, KxValueType.Int);
                   result.AddParameter("webLargeProperty", webLargeProperty, KxValueType.Int);
                   result.AddParameter("webPublishVacancy", webPublishVacancy, KxValueType.Int);
                   result.AddParameter("allocationModelId", allocationModelId, KxValueType.Int);
                   result.AddParameter("defaultPricingModelId", defaultPricingModelId, KxValueType.Int);
                   result.AddParameter("defaultArrivalDate", defaultArrivalDate, KxValueType.DateTime);
                   result.AddParameter("defaultDepartureDate", defaultDepartureDate, KxValueType.DateTime);
                   result.AddParameter("defaultWordTemplateId", defaultWordTemplateId, KxValueType.Int);
                   result.AddParameter("defaultOfferTemplateId", defaultOfferTemplateId, KxValueType.Int);
                   result.AddParameter("lockAllocationModel", lockAllocationModel, KxValueType.Boolean);
                   result.AddParameter("lockPricingModel", lockPricingModel, KxValueType.Boolean);
                   result.AddParameter("lockOfferTemplate", lockOfferTemplate, KxValueType.Boolean);
                   result.AddParameter("lockLicenceTemplate", lockLicenceTemplate, KxValueType.Boolean);
            }
            return result;
        }


        public override void LoadFromXml(string xml, XmlNode node)
        {
            dBIdentifier = KxXmlUtils.GetNodeValue(node, "roomconfigid", 0);
            residentialRoomId  = KxXmlUtils.GetNodeValue(node,"residentialroomid",0);
            academicYearId  = KxXmlUtils.GetNodeValue(node,"academicyearid",0);
            inactive  = KxXmlUtils.GetNodeValue(node,"inactive",false);
            courseTypeId  = KxXmlUtils.GetNodeValue(node,"coursetypeid",0);
            studentYearId  = KxXmlUtils.GetNodeValue(node,"studentyearid",0);
            gender  = KxXmlUtils.GetNodeValue(node,"gender",0);
            singleSex  = KxXmlUtils.GetNodeValue(node,"singlesex",0);
            smoking  = KxXmlUtils.GetNodeValue(node,"smoking",false);
            disabled  = KxXmlUtils.GetNodeValue(node,"disabled",false);
            bedroomTypeId  = KxXmlUtils.GetNodeValue(node,"bedroomtypeid",0);
            customField1  = KxXmlUtils.GetNodeValue(node,"customfield1",false);
            customField2  = KxXmlUtils.GetNodeValue(node,"customfield2",false);
            customValue1  = KxXmlUtils.GetNodeValue(node,"customvalue1","");
            customValue2  = KxXmlUtils.GetNodeValue(node,"customvalue2","");
            customValue3  = KxXmlUtils.GetNodeValue(node,"customvalue3","");
            customValue4  = KxXmlUtils.GetNodeValue(node,"customvalue4","");
            customValue5 = KxXmlUtils.GetNodeValue(node, "customvalue5", "");
            customValue6 = KxXmlUtils.GetNodeValue(node, "customvalue6", "");
            customValue7 = KxXmlUtils.GetNodeValue(node, "customvalue7", "");
            customValue8 = KxXmlUtils.GetNodeValue(node, "customvalue8", "");
            capacity  = KxXmlUtils.GetNodeValue(node,"capacity",0);
            chargeBand  = KxXmlUtils.GetNodeValue(node,"chargeband",0);
            webPublished  = KxXmlUtils.GetNodeValue(node,"webpublished",false);
            webAdditionalTextNotesId  = KxXmlUtils.GetNodeValue(node,"webadditionaltextnotesid",0);
            webLargeProperty  = KxXmlUtils.GetNodeValue(node,"weblargeproperty",0);
            webPublishVacancy  = KxXmlUtils.GetNodeValue(node,"webpublishvacancy",0);
            allocationModelId  = KxXmlUtils.GetNodeValue(node,"allocationmodelid",0);
            defaultPricingModelId  = KxXmlUtils.GetNodeValue(node,"defaultpricingmodelid",0);
            defaultArrivalDate  = KxXmlUtils.GetNodeValueDT(node,"defaultarrivaldate",null);
            defaultDepartureDate  = KxXmlUtils.GetNodeValueDT(node,"defaultdeparturedate",null);
            defaultWordTemplateId  = KxXmlUtils.GetNodeValue(node,"defaultwordtemplateid",0);
            defaultOfferTemplateId  = KxXmlUtils.GetNodeValue(node,"defaultoffertemplateid",0);
            lockAllocationModel  = KxXmlUtils.GetNodeValue(node,"lockallocationmodel",false);
            lockPricingModel  = KxXmlUtils.GetNodeValue(node,"lockpricingmodel",false);
            lockOfferTemplate  = KxXmlUtils.GetNodeValue(node,"lockoffertemplate",false);
            lockLicenceTemplate  = KxXmlUtils.GetNodeValue(node,"locklicencetemplate",false);
        }

        private string GetGenderString(int value)
        {
            switch (value)
            {
                case 1: return "Male";
                case 2: return "Female";
                case 3: return "First Gender";
                default: return "Any gender";
            }
        }

        private string GetSingleSexString()
        {
            switch (singleSex)
            {
                case 1: return "Single Sex";
                default: return "Mixed";
            }
        }
    }
}

