using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Exceptions;
using Kx.Core.Utility;

namespace Kx.Objects.Common
{
    public class KxChargeTypeRateMatrixs : KxBaseClass
    {
        private List<KxChargeTypeRateMatrix> items = new List<KxChargeTypeRateMatrix>();

        public List<KxChargeTypeRateMatrix> Items
        {
            get
            { return items; }
        }

        public KxChargeTypeRateMatrix this[int index]
        {
            get
            { return items[index]; }
            set
            { items[index] = value; }
        }

        private int _chargeTypeID;
        private int _parentMatrixID;

        /// <summary>
        /// List all prices
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="parentMatrixID">0 for top level prices, and the matrix id of the parent if part of a package</param>
        /// <param name="chargeTypeID">Optional, list only prices for a specific charge type.</param>
        public KxChargeTypeRateMatrixs(KxBaseClass parent, int parentMatrixID, int chargeTypeID)
            : base(parent)
        {
            _chargeTypeID = chargeTypeID;
            _parentMatrixID = parentMatrixID;
            Load();
        }

        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = null;

            if (actiontype == ActionType.Select)
            {
                result = new KxData("chargetyperatematrix", "ChargeTypeRateMatrix", TargetType.Table, ActionType.Select);

                if (_chargeTypeID > 0)
                {
                    result.Parameters.Add(new KxDataValue("chargetypeid", _chargeTypeID, KxValueType.Int));
                }

                result.Parameters.Add(new KxDataValue("parentmatrixid", _parentMatrixID, KxValueType.Int));
            }
            return result;
        }

        public override bool LoadFromXml(string xml, string xpath)
        {
            items.Clear();
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xml);

                XmlNodeList nodes = doc.SelectNodes(@"//results/" + xpath + "/datarow");

                foreach (XmlNode node in nodes)
                {
                    if (node != null)
                    {
                        KxChargeTypeRateMatrix item = new KxChargeTypeRateMatrix(this, node);
                        items.Add(item);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "LoadFromXml", ex.Message, xpath);
                LastError = ex.Message;
                return false;
            }
        }

        public KxChargeTypeRateMatrix GetChargeTypeRateMatrix(int id)
        {
            KxChargeTypeRateMatrix result = null;

            foreach (KxChargeTypeRateMatrix i in Items)
            {
                if (i.MatrixId == id)
                {
                    result = i;
                    break;
                }
            }
            return result;
        }

    }
}

