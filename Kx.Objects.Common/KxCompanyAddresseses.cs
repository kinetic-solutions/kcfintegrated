﻿using System;
using System.Collections.Generic;
using System.Text;
using Kx.Core.Data;
using Kx.Core.Base;
using System.Xml;
using Kx.Core.Exceptions;



namespace Kx.Objects.Common
{
    /// <summary>
    /// This has to be called KxCompanyAddresseses because someone already named the singular as a plural...
    /// </summary>
    public class KxCompanyAddresseses : KxBaseClass
    {
        private List<KxCompanyAddresses> items = new List<KxCompanyAddresses>();
        private int _companyId;

        public List<KxCompanyAddresses> Items
        {
            get
            { return items; }
        }

        public KxCompanyAddresses this[int index]
        {
            get
            { return items[index]; }
            set
            { items[index] = value; }
        }

        public KxCompanyAddresseses(int CompanyId)
        {
            _companyId = CompanyId;
            Load();
        }

        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = null;

            if (actiontype == ActionType.Select)
            {
                result = new KxData("companyaddresses", "CompanyAddresses", TargetType.Table, ActionType.Select);

                result.AddKeyField("CompanyId", _companyId, KxValueType.Int);

                //result.Parameters.Add(new KxDataValue("keyfield", keyfield, KxValueType.Int));
            }
            return result;
        }

        public override bool LoadFromXml(string xml, string xpath)
        {
            items.Clear();
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xml);

                XmlNodeList nodes = doc.SelectNodes(@"//results/" + xpath + "/datarow");

                foreach (XmlNode node in nodes)
                {
                    if (node != null)
                    {
                        KxCompanyAddresses item = new KxCompanyAddresses(this, node);
                        items.Add(item);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "LoadFromXml", ex.Message, xpath);
                LastError = ex.Message;
                return false;
            }
        }

        //public KxCompanyAddresses GetCompanyAddress(int id)
        //{
        //    KxCompanyAddresses result = null;

        //    foreach (KxCompanyAddress i in Items)
        //    {
        //        if (i.XXXX == id)
        //        {
        //            result = i;
        //            break;
        //        }
        //    }
        //    return result;
        //}

    }
}
