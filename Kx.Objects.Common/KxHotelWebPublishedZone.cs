﻿using System;
using Kx.Objects.Common;
using Kx.Core.Base;
using System.Xml;

namespace Kx.Objects.Common
{
    public class KxHotelWebPublishedZone : KxWebPublishedZone
    {
        #region Constructors
        public KxHotelWebPublishedZone (KxBaseClass parent)
            : base (parent)
        {
            this.ZoneType = KxWebPublishedZoneTypes.KxHotel;
        }

        public KxHotelWebPublishedZone (KxBaseClass parent, XmlNode node)
            : base (parent, node)
        {
        }

        public KxHotelWebPublishedZone(KxBaseClass parent, int webPublishedZoneID)
            : base(parent, webPublishedZoneID)
        {
            if (this.ZoneType != KxWebPublishedZoneTypes.KxHotel)
                throw new ArgumentException("zoneType", "This zone is not a KxHotel zone type");
            
        }
        #endregion
    }
}
