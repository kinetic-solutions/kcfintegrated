﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Exceptions;
using Kx.Core.Utility;

namespace Kx.Objects.Common
{
    public class KxStatuses : KxBaseClass
    {
        private List<KxStatus> items = new List<KxStatus>();

        public List<KxStatus> Items
        {
            get
            { return items; }
        }

        public KxStatus this[int index]
        {
            get
            { return items[index]; }
            set
            { items[index] = value; }
        }

        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = null;

            if (actiontype == ActionType.Select)
            {
                result = new KxData("status", "status", TargetType.Table, ActionType.Select);

                result.OrderFields.Add(new KxDataOrder("statusid"));

            }
            return result;
        }


        public KxStatuses(KxBaseClass parent)
            : base(parent)
        {

        }


        public override bool LoadFromXml(string xml, string xpath)
        {
            items.Clear();
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xml);

                XmlNodeList nodes = doc.SelectNodes(@"//results/" + xpath + "/datarow");

                foreach (XmlNode node in nodes)
                {
                    if (node != null)
                    {
                        KxStatus s = new KxStatus(this, node);
                        Items.Add(s);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "LoadFromXml", ex.Message, xpath);
                LastError = ex.Message;
                return false;
            }
        }



        public KxStatus GetStatusById(int id)
        {
            KxStatus result = null;

            foreach (KxStatus s in Items)
            {
                if (int.Parse(s.GetId()) == id)
                {
                    result = s;
                    break;
                }
            }

            if (result == null)
            {
                KxStatus dummy = new KxStatus(this, id);
                if (int.Parse(dummy.GetId()) == id)
                {
                    Items.Add(dummy);
                    result = dummy;
                }
            }
            
            return result;
        }
    }
}
