using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;

namespace Kx.Objects.Common
{
    [Serializable]
    public class KxBedroomType : KxBaseClass
    {
        #region Attributes & Properties

        protected string   description;
        protected int capacity;
        protected bool     bookByRoom;
        protected string   shortName;
        protected bool     notAllocatable;
        protected string   externalCode;
        protected string   webDescription;
        protected string   customData1;
        protected string   customData2;

        public int      BedroomTypeId
        {
            get
            { return base.dBIdentifier;}
        }

        public string   Description
        {
            get
            { return description;}
            set
            { 
                AddToAudit("Description", description, value);
                description = value;
            }
        }

        public int Capacity
        {
            get
            { return capacity;}
            set
            { 
                AddToAudit("Capacity", capacity, value);
                capacity = value;
            }
        }

        public bool     BookByRoom
        {
            get
            { return bookByRoom;}
            set
            { 
                AddToAudit("BookByRoom", bookByRoom, value);
                bookByRoom = value;
            }
        }

        public string   ShortName
        {
            get
            { return shortName;}
            set
            { 
                AddToAudit("ShortName", shortName, value);
                shortName = value;
            }
        }

        public bool     NotAllocatable
        {
            get
            { return notAllocatable;}
            set
            { 
                AddToAudit("NotAllocatable", notAllocatable, value);
                notAllocatable = value;
            }
        }

        public string   ExternalCode
        {
            get
            { return externalCode;}
            set
            { 
                AddToAudit("ExternalCode", externalCode, value);
                externalCode = value;
            }
        }

        public string   WebDescription
        {
            get
            { return webDescription;}
            set
            { 
                AddToAudit("WebDescription", webDescription, value);
                webDescription = value;
            }
        }

        public string CustomData1
        {
            get
            {
                return customData1;
            }
            set
            {
                AddToAudit("CustomData1", customData1, value);
                customData1 = value;
            }
        }
        public string CustomData2
        {
            get
            {
                return customData2;
            }
            set
            {
                AddToAudit("CustomData2", customData2, value);
                customData2 = value;
            }
        }


        #endregion 

        public KxBedroomType(KxBaseClass parent, XmlNode node) : base(parent)
        {
            LoadFromXml("",node);
        }

        public KxBedroomType(KxBaseClass parent,int      bedroomtypeid):base(parent)
        {
             base.dBIdentifier = bedroomtypeid;
             Load();
        }

        public KxBedroomType(KxBaseClass parent, string webDesc)
            : base(parent)
        {
            webDescription = webDesc;
            Load();
        }


        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("bedroomtypes", "BedroomTypes", TargetType.Table);

            if (!String.IsNullOrEmpty(webDescription))
            {
                result.AddKeyField("WebDescription", webDescription, KxValueType.String);
            }
            else
            {
                result.AutoKeyField = new KxDataValue("BedroomTypeId", BedroomTypeId, KxValueType.Int);
            }

            if ((actiontype == ActionType.Update) && (BedroomTypeId == 0))
            {
                result.Action = ActionType.Insert;
            }
            else
            {
                result.Action = actiontype;
            }

            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {
                   result.AddParameter("description", description, KxValueType.String);
                   result.AddParameter("capacity", capacity, KxValueType.Int);
                   result.AddParameter("bookByRoom", bookByRoom, KxValueType.Boolean);
                   result.AddParameter("shortName", shortName, KxValueType.String);
                   result.AddParameter("notAllocatable", notAllocatable, KxValueType.Boolean);
                   result.AddParameter("externalCode", externalCode, KxValueType.String);
                   result.AddParameter("webDescription", webDescription, KxValueType.String);
                   //result.AddParameter("customdata1", customData1, KxValueType.String);
                   //result.AddParameter("customdata2", customData2, KxValueType.String);
            }
            return result;
        }


        public override  void LoadFromXml(string xml,XmlNode node)
        {
            dBIdentifier  = KxXmlUtils.GetNodeValue(node,"bedroomtypeid",0);
            description  = KxXmlUtils.GetNodeValue(node,"description","");
            capacity  = KxXmlUtils.GetNodeValue(node,"capacity",0);
            bookByRoom  = KxXmlUtils.GetNodeValue(node,"bookbyroom",false);
            shortName  = KxXmlUtils.GetNodeValue(node,"shortname","");
            notAllocatable  = KxXmlUtils.GetNodeValue(node,"notallocatable",false);
            externalCode  = KxXmlUtils.GetNodeValue(node,"externalcode","");
            webDescription  = KxXmlUtils.GetNodeValue(node,"webdescription","");
            customData1 = KxXmlUtils.GetNodeValue(node, "customdata1", "");
            customData2 = KxXmlUtils.GetNodeValue(node, "customdata2", "");
        }
    }
}

