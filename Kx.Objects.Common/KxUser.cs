using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;

namespace Kx.Objects.Common
{
    public enum LoadType { Id, User};

    [Serializable]
    public class KxUser : KxBaseClass
    {
        #region Attributes & Properties

        protected int      siteId;
        protected int      userGroupId;
        protected string   userName;
        protected string   title;
        protected string   foreName;
        protected string   surName;
        protected int      defaultBusinessTypeId;
        protected int      personId;
        protected string   releaseNoteVersion;
        protected string   email;
        protected bool     adminRights;
        protected int      startupSystemObjectId;
        protected bool     showToolbarImages;
        protected bool     showPersonalToolbarImages;
        protected bool     showMainToolbar;
        protected bool     directBooking;
        protected string   password;
        protected int      eventAccess;
        protected int      residentAccess;
        protected int      marketingAccess;
        protected int      maintAccess;
        protected int      cateringAccess;
        protected bool     inactive;
        protected int      residentStartupObjectId;
        protected int      marketStartupObjectId;
        protected int      cateringStartupObjectId;
        protected int      areaId;
        protected string   jobTitle;
        protected string   dir;
        protected string   telephone;
        protected int      studentAccess;
        protected int      courseAccess;
        protected string   emailSummaryText;
        protected int      emailReceiptRequest;
        protected bool     noMultiSiteMaint;
        protected string   guid;
        protected int      financeAccess;
        protected bool     propertyManager;
        protected string   externalId;
        protected string   overbookingPassword;
        protected bool     creditRights;
        protected int      actionReadAccess;
        protected int      actionEditAccess;
        protected int      simpleMAPI;
        protected int authenticationType;
        protected int authAttempts;
        protected bool changePasswordOnLogin;
        protected DateTime? lastPasswordChange;
        protected DateTime? accountLockedUntil;

        LoadType loadType = LoadType.Id;

        public int      UserId
        {
            get
            { return base.dBIdentifier;}
        }

        public int      SiteId
        {
            get
            { return siteId;}
            set
            { 
                AddToAudit("SiteId", siteId, value);
                siteId = value;
            }
        }

        public int      UserGroupId
        {
            get
            { return userGroupId;}
            set
            { 
                AddToAudit("UserGroupId", userGroupId, value);
                userGroupId = value;
            }
        }

        public string   UserName
        {
            get
            { return userName;}
            set
            { 
                AddToAudit("UserName", userName, value);
                userName = value;
            }
        }

        public string   Title
        {
            get
            { return title;}
            set
            { 
                AddToAudit("Title", title, value);
                title = value;
            }
        }

        public string   ForeName
        {
            get
            { return foreName;}
            set
            { 
                AddToAudit("ForeName", foreName, value);
                foreName = value;
            }
        }

        public string   SurName
        {
            get
            { return surName;}
            set
            { 
                AddToAudit("SurName", surName, value);
                surName = value;
            }
        }

        public int      DefaultBusinessTypeId
        {
            get
            { return defaultBusinessTypeId;}
            set
            { 
                AddToAudit("DefaultBusinessTypeId", defaultBusinessTypeId, value);
                defaultBusinessTypeId = value;
            }
        }

        public int      PersonId
        {
            get
            { return personId;}
            set
            { 
                AddToAudit("PersonId", personId, value);
                personId = value;
            }
        }

        public string   ReleaseNoteVersion
        {
            get
            { return releaseNoteVersion;}
            set
            { 
                AddToAudit("ReleaseNoteVersion", releaseNoteVersion, value);
                releaseNoteVersion = value;
            }
        }

        public string   Email
        {
            get
            { return email;}
            set
            { 
                AddToAudit("Email", email, value);
                email = value;
            }
        }

        public bool     AdminRights
        {
            get
            { return adminRights;}
            set
            { 
                AddToAudit("AdminRights", adminRights, value);
                adminRights = value;
            }
        }

        public int      StartupSystemObjectId
        {
            get
            { return startupSystemObjectId;}
            set
            { 
                AddToAudit("StartupSystemObjectId", startupSystemObjectId, value);
                startupSystemObjectId = value;
            }
        }

        public bool     ShowToolbarImages
        {
            get
            { return showToolbarImages;}
            set
            { 
                AddToAudit("ShowToolbarImages", showToolbarImages, value);
                showToolbarImages = value;
            }
        }

        public bool     ShowPersonalToolbarImages
        {
            get
            { return showPersonalToolbarImages;}
            set
            { 
                AddToAudit("ShowPersonalToolbarImages", showPersonalToolbarImages, value);
                showPersonalToolbarImages = value;
            }
        }

        public bool     ShowMainToolbar
        {
            get
            { return showMainToolbar;}
            set
            { 
                AddToAudit("ShowMainToolbar", showMainToolbar, value);
                showMainToolbar = value;
            }
        }

        public bool     DirectBooking
        {
            get
            { return directBooking;}
            set
            { 
                AddToAudit("DirectBooking", directBooking, value);
                directBooking = value;
            }
        }

        public string   Password
        {
            get
            { return password;}
            set
            { 
                AddToAudit("Password", password, value);
                password = value;
            }
        }

        public int      EventAccess
        {
            get
            { return eventAccess;}
            set
            { 
                AddToAudit("EventAccess", eventAccess, value);
                eventAccess = value;
            }
        }

        public int      ResidentAccess
        {
            get
            { return residentAccess;}
            set
            { 
                AddToAudit("ResidentAccess", residentAccess, value);
                residentAccess = value;
            }
        }

        public int      MarketingAccess
        {
            get
            { return marketingAccess;}
            set
            { 
                AddToAudit("MarketingAccess", marketingAccess, value);
                marketingAccess = value;
            }
        }

        public int      MaintAccess
        {
            get
            { return maintAccess;}
            set
            { 
                AddToAudit("MaintAccess", maintAccess, value);
                maintAccess = value;
            }
        }

        public int      CateringAccess
        {
            get
            { return cateringAccess;}
            set
            { 
                AddToAudit("CateringAccess", cateringAccess, value);
                cateringAccess = value;
            }
        }

        public bool     Inactive
        {
            get
            { return inactive;}
            set
            { 
                AddToAudit("Inactive", inactive, value);
                inactive = value;
            }
        }

        public int      ResidentStartupObjectId
        {
            get
            { return residentStartupObjectId;}
            set
            { 
                AddToAudit("ResidentStartupObjectId", residentStartupObjectId, value);
                residentStartupObjectId = value;
            }
        }

        public int      MarketStartupObjectId
        {
            get
            { return marketStartupObjectId;}
            set
            { 
                AddToAudit("MarketStartupObjectId", marketStartupObjectId, value);
                marketStartupObjectId = value;
            }
        }

        public int      CateringStartupObjectId
        {
            get
            { return cateringStartupObjectId;}
            set
            { 
                AddToAudit("CateringStartupObjectId", cateringStartupObjectId, value);
                cateringStartupObjectId = value;
            }
        }

        public int      AreaId
        {
            get
            { return areaId;}
            set
            { 
                AddToAudit("AreaId", areaId, value);
                areaId = value;
            }
        }

        public string   JobTitle
        {
            get
            { return jobTitle;}
            set
            { 
                AddToAudit("JobTitle", jobTitle, value);
                jobTitle = value;
            }
        }

        public string   Dir
        {
            get
            { return dir;}
            set
            { 
                AddToAudit("Dir", dir, value);
                dir = value;
            }
        }

        public string   Telephone
        {
            get
            { return telephone;}
            set
            { 
                AddToAudit("Telephone", telephone, value);
                telephone = value;
            }
        }

        public int      StudentAccess
        {
            get
            { return studentAccess;}
            set
            { 
                AddToAudit("StudentAccess", studentAccess, value);
                studentAccess = value;
            }
        }

        public int      CourseAccess
        {
            get
            { return courseAccess;}
            set
            { 
                AddToAudit("CourseAccess", courseAccess, value);
                courseAccess = value;
            }
        }

        public string   EmailSummaryText
        {
            get
            { return emailSummaryText;}
            set
            { 
                AddToAudit("EmailSummaryText", emailSummaryText, value);
                emailSummaryText = value;
            }
        }

        public int      EmailReceiptRequest
        {
            get
            { return emailReceiptRequest;}
            set
            { 
                AddToAudit("EmailReceiptRequest", emailReceiptRequest, value);
                emailReceiptRequest = value;
            }
        }

        public bool     NoMultiSiteMaint
        {
            get
            { return noMultiSiteMaint;}
            set
            { 
                AddToAudit("NoMultiSiteMaint", noMultiSiteMaint, value);
                noMultiSiteMaint = value;
            }
        }

        public string   GuId
        {
            get
            { return guid;}
            set
            { 
                AddToAudit("GuId", guid, value);
                guid = value;
            }
        }

        public int      FinanceAccess
        {
            get
            { return financeAccess;}
            set
            { 
                AddToAudit("FinanceAccess", financeAccess, value);
                financeAccess = value;
            }
        }

        public bool     PropertyManager
        {
            get
            { return propertyManager;}
            set
            { 
                AddToAudit("PropertyManager", propertyManager, value);
                propertyManager = value;
            }
        }

        public string   ExternalId
        {
            get
            { return externalId;}
            set
            { 
                AddToAudit("ExternalId", externalId, value);
                externalId = value;
            }
        }

        public string   OverbookingPassword
        {
            get
            { return overbookingPassword;}
            set
            { 
                AddToAudit("OverbookingPassword", overbookingPassword, value);
                overbookingPassword = value;
            }
        }

        public bool     CreditRights
        {
            get
            { return creditRights;}
            set
            { 
                AddToAudit("CreditRights", creditRights, value);
                creditRights = value;
            }
        }

        public int      ActionReadAccess
        {
            get
            { return actionReadAccess;}
            set
            { 
                AddToAudit("ActionReadAccess", actionReadAccess, value);
                actionReadAccess = value;
            }
        }

        public int      ActionEditAccess
        {
            get
            { return actionEditAccess;}
            set
            { 
                AddToAudit("ActionEditAccess", actionEditAccess, value);
                actionEditAccess = value;
            }
        }

        public int      SimpleMAPI
        {
            get
            { return simpleMAPI;}
            set
            { 
                AddToAudit("SimpleMAPI", simpleMAPI, value);
                simpleMAPI = value;
            }
        }

        public int AuthenticationType
        {
            get
            {
                return authenticationType;
            }
            set
            {
                AddToAudit("AuthenticationType", authenticationType, value);
            	authenticationType = value;
            }
        }

        public int AuthAttempts
        {
            get
            {
                return authAttempts;
            }
            set
            {
                AddToAudit("AuthAttempts", authAttempts, value);
            	authAttempts = value;
            }
        }

        public bool ChangePasswordOnLogin
        {
            get
            {
                return changePasswordOnLogin;
            }
            set
            {
                AddToAudit("ChangePasswordOnLogin", changePasswordOnLogin, value);
            	changePasswordOnLogin = value;
            }
        }

        public DateTime? LastPasswordChange
        {
            get
            {
                return lastPasswordChange;
            }
            set
            {
                AddToAudit("LastPasswordChange", lastPasswordChange, value);
            	lastPasswordChange = value;
            }
        }

        public DateTime? AccountLockedUntil
        {
            get
            {
                return accountLockedUntil;
            }
            set
            {
                AddToAudit("AccountLockedUntil", accountLockedUntil, value);
            	accountLockedUntil = value;
            }
        }
        #endregion 


        public KxUser(KxBaseClass parent):base(parent)
        {}


        public KxUser(KxBaseClass parent, XmlNode node):base(parent)
        {
             LoadFromXml("", node);
        }

        public KxUser(KxBaseClass parent,int      userid):base(parent)
        {
             base.dBIdentifier = userid;
             loadType = LoadType.Id;
             Load();
        }

        public KxUser(KxBaseClass parent, string username)
            : base(parent)
        {
            userName = username;
            loadType = LoadType.User;
            Load();
        }


        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("users", "Users", TargetType.Table);

            if (loadType == LoadType.Id)
            {
                result.AutoKeyField = new KxDataValue("UserId", UserId, KxValueType.Int);
            }
            else
            {
                result.AddKeyField("username",userName,KxValueType.String);
            }
            
            if ((actiontype == ActionType.Update) && (UserId == 0))
            {
                result.Action = ActionType.Insert;
            }
            else
            {
                result.Action = actiontype;
            }

            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {
               result.AddParameter("userGroupId", userGroupId, KxValueType.Int, true);
               result.AddParameter("userName", userName, KxValueType.String, 50, true);
               result.AddParameter("title", title, KxValueType.String, 50, true);
               result.AddParameter("foreName", foreName, KxValueType.String, 50, true);
               result.AddParameter("surName", surName, KxValueType.String, 50, true);
               result.AddParameter("defaultBusinessTypeId", defaultBusinessTypeId, KxValueType.Int, true);
               result.AddParameter("personId", personId, KxValueType.Int, true);
               result.AddParameter("releaseNoteVersion", releaseNoteVersion, KxValueType.String, 10, true);
               result.AddParameter("email", email, KxValueType.String, 255, true);
               result.AddParameter("adminRights", adminRights, KxValueType.Boolean, false);
               result.AddParameter("startupSystemObjectId", startupSystemObjectId, KxValueType.Int, true);
               result.AddParameter("showToolbarImages", showToolbarImages, KxValueType.Boolean, false);
               result.AddParameter("showPersonalToolbarImages", showPersonalToolbarImages, KxValueType.Boolean, false);
               result.AddParameter("showMainToolbar", showMainToolbar, KxValueType.Boolean, false);
               result.AddParameter("directBooking", directBooking, KxValueType.Boolean, false);
               result.AddParameter("password", password, KxValueType.String, 50, true);
               result.AddParameter("eventAccess", eventAccess, KxValueType.Int, true);
               result.AddParameter("residentAccess", residentAccess, KxValueType.Int, true);
               result.AddParameter("marketingAccess", marketingAccess, KxValueType.Int, true);
               result.AddParameter("maintAccess", maintAccess, KxValueType.Int, true);
               result.AddParameter("cateringAccess", cateringAccess, KxValueType.Int, true);
               result.AddParameter("inactive", inactive, KxValueType.Boolean, false);
               result.AddParameter("residentStartupObjectId", residentStartupObjectId, KxValueType.Int, true);
               result.AddParameter("marketStartupObjectId", marketStartupObjectId, KxValueType.Int, true);
               result.AddParameter("cateringStartupObjectId", cateringStartupObjectId, KxValueType.Int, true);
               result.AddParameter("areaId", areaId, KxValueType.Int, true);
               result.AddParameter("jobTitle", jobTitle, KxValueType.String, 50, true);
               result.AddParameter("dir", dir, KxValueType.String, 255, true);
               result.AddParameter("telephone", telephone, KxValueType.String, 50, true);
               result.AddParameter("studentAccess", studentAccess, KxValueType.Int, false);
               result.AddParameter("courseAccess", courseAccess, KxValueType.Int, false);
               result.AddParameter("emailSummaryText", emailSummaryText, KxValueType.String, 2147483647, true);
               result.AddParameter("emailReceiptRequest", emailReceiptRequest, KxValueType.Int, true);
               result.AddParameter("noMultiSiteMaint", noMultiSiteMaint, KxValueType.Boolean, true);
               result.AddParameter("guid", guid, KxValueType.String, 50, true);
               result.AddParameter("financeAccess", financeAccess, KxValueType.Int, true);
               result.AddParameter("propertyManager", propertyManager, KxValueType.Boolean, true);
               result.AddParameter("externalId", externalId, KxValueType.String, 255, true);
               result.AddParameter("overbookingPassword", overbookingPassword, KxValueType.String, 255, true);
               result.AddParameter("creditRights", creditRights, KxValueType.Boolean, true);
               result.AddParameter("actionReadAccess", actionReadAccess, KxValueType.Int, true);
               result.AddParameter("actionEditAccess", actionEditAccess, KxValueType.Int, true);
               result.AddParameter("simpleMAPI", simpleMAPI, KxValueType.Int, true);
               result.AddParameter("authenticationtype", authenticationType, KxValueType.Int, false);
               result.AddParameter("authattempts", authAttempts, KxValueType.Int, false);
               result.AddParameter("changepasswordonlogin", changePasswordOnLogin, KxValueType.Boolean, false);
               result.AddParameter("lastpasswordchange", lastPasswordChange, KxValueType.DateTime, false);
               result.AddParameter("accountlockeduntil", accountLockedUntil, KxValueType.DateTime, false);
            }
            return result;
        }


        public override  void LoadFromXml(string xml,XmlNode node)
        {
            dBIdentifier  = KxXmlUtils.GetNodeValue(node,"userid",0);
            siteId  = KxXmlUtils.GetNodeValue(node,"siteid",0);
            userGroupId  = KxXmlUtils.GetNodeValue(node,"usergroupid",0);
            userName  = KxXmlUtils.GetNodeValue(node,"username","");
            title  = KxXmlUtils.GetNodeValue(node,"title","");
            foreName  = KxXmlUtils.GetNodeValue(node,"forename","");
            surName  = KxXmlUtils.GetNodeValue(node,"surname","");
            defaultBusinessTypeId  = KxXmlUtils.GetNodeValue(node,"defaultbusinesstypeid",0);
            personId  = KxXmlUtils.GetNodeValue(node,"personid",0);
            releaseNoteVersion  = KxXmlUtils.GetNodeValue(node,"releasenoteversion","");
            email  = KxXmlUtils.GetNodeValue(node,"email","");
            adminRights  = KxXmlUtils.GetNodeValue(node,"adminrights",false);
            startupSystemObjectId  = KxXmlUtils.GetNodeValue(node,"startupsystemobjectid",0);
            showToolbarImages  = KxXmlUtils.GetNodeValue(node,"showtoolbarimages",false);
            showPersonalToolbarImages  = KxXmlUtils.GetNodeValue(node,"showpersonaltoolbarimages",false);
            showMainToolbar  = KxXmlUtils.GetNodeValue(node,"showmaintoolbar",false);
            directBooking  = KxXmlUtils.GetNodeValue(node,"directbooking",false);
            password  = KxXmlUtils.GetNodeValue(node,"password","");
            eventAccess  = KxXmlUtils.GetNodeValue(node,"eventaccess",0);
            residentAccess  = KxXmlUtils.GetNodeValue(node,"residentaccess",0);
            marketingAccess  = KxXmlUtils.GetNodeValue(node,"marketingaccess",0);
            maintAccess  = KxXmlUtils.GetNodeValue(node,"maintaccess",0);
            cateringAccess  = KxXmlUtils.GetNodeValue(node,"cateringaccess",0);
            inactive  = KxXmlUtils.GetNodeValue(node,"inactive",false);
            residentStartupObjectId  = KxXmlUtils.GetNodeValue(node,"residentstartupobjectid",0);
            marketStartupObjectId  = KxXmlUtils.GetNodeValue(node,"marketstartupobjectid",0);
            cateringStartupObjectId  = KxXmlUtils.GetNodeValue(node,"cateringstartupobjectid",0);
            areaId  = KxXmlUtils.GetNodeValue(node,"areaid",0);
            jobTitle  = KxXmlUtils.GetNodeValue(node,"jobtitle","");
            dir  = KxXmlUtils.GetNodeValue(node,"dir","");
            telephone  = KxXmlUtils.GetNodeValue(node,"telephone","");
            studentAccess  = KxXmlUtils.GetNodeValue(node,"studentaccess",0);
            courseAccess  = KxXmlUtils.GetNodeValue(node,"courseaccess",0);
            emailSummaryText  = KxXmlUtils.GetNodeValue(node,"emailsummarytext","");
            emailReceiptRequest  = KxXmlUtils.GetNodeValue(node,"emailreceiptrequest",0);
            noMultiSiteMaint  = KxXmlUtils.GetNodeValue(node,"nomultisitemaint",false);
            guid  = KxXmlUtils.GetNodeValue(node,"guid","");
            financeAccess  = KxXmlUtils.GetNodeValue(node,"financeaccess",0);
            propertyManager  = KxXmlUtils.GetNodeValue(node,"propertymanager",false);
            externalId  = KxXmlUtils.GetNodeValue(node,"externalid","");
            overbookingPassword  = KxXmlUtils.GetNodeValue(node,"overbookingpassword","");
            creditRights  = KxXmlUtils.GetNodeValue(node,"creditrights",false);
            actionReadAccess  = KxXmlUtils.GetNodeValue(node,"actionreadaccess",0);
            actionEditAccess  = KxXmlUtils.GetNodeValue(node,"actioneditaccess",0);
            simpleMAPI  = KxXmlUtils.GetNodeValue(node,"simplemapi",0);
            authenticationType = KxXmlUtils.GetNodeValue(node, "authenticationtype", 0);
            authAttempts = KxXmlUtils.GetNodeValue(node, "authattempts", 0);
            changePasswordOnLogin = KxXmlUtils.GetNodeValue(node, "changepasswordonlogin", false);
            lastPasswordChange = KxXmlUtils.GetNodeValueDT(node, "lastpasswordchange", DateTime.Now.AddDays(-1));
            accountLockedUntil = KxXmlUtils.GetNodeValueDT(node, "accountlockeduntil", DateTime.Now.AddDays(-1));
        }
    }
}

