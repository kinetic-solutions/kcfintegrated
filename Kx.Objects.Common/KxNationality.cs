using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;

namespace Kx.Objects.Student
{
    [Serializable]
    public class KxNationality : KxBaseClass
    {
        #region Attributes & Properties

        protected string   description;

        public int      NationalityId
        {
            get
            { return base.dBIdentifier;}
        }

        public string   Description
        {
            get
            { return description;}
            set
            { 
                AddToAudit("Description", description, value);
                description = value;
            }
        }


        #endregion 


        public KxNationality(KxBaseClass parent):base(parent)
        {}


        public KxNationality(KxBaseClass parent, XmlNode node):base(parent)
        {
             LoadFromXml("", node);
        }

        public KxNationality(KxBaseClass parent, int nationalityid)
            : base(parent)
        {
             base.dBIdentifier = nationalityid;
             Load();
        }


        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("nationalities", "Nationalities", TargetType.Table);

            result.AutoKeyField = new KxDataValue("NationalityId",NationalityId,KxValueType.Int);


            if ((actiontype == ActionType.Update) && (NationalityId == 0))
            {
                result.Action = ActionType.Insert;
            }
            else
            {
                result.Action = actiontype;
            }

            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {
            }
            return result;
        }


        public override  void LoadFromXml(string xml,XmlNode node)
        {
            dBIdentifier  = KxXmlUtils.GetNodeValue(node,"nationalityid",0);
            description  = KxXmlUtils.GetNodeValue(node,"description","");
        }
    }
}

