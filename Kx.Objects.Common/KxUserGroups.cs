﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Exceptions;
using Kx.Core.Utility;

namespace Kx.Objects.Common
{
    public class KxUserGroups : KxBaseClass
    {
        private List<KxUserGroup> items = new List<KxUserGroup>();

        public List<KxUserGroup> Items
        {
            get
            { return items; }
        }

        public KxUserGroup this[int index]
        {
            get
            { return items[index]; }
            set
            { items[index] = value; }
        }


        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = null;

            if (actiontype == ActionType.Select)
            {
                result = new KxData("usergroups", "UserGroups", TargetType.Table);
            }
            return result;
        }

        public KxUserGroups(KxBaseClass parent)
            : base(parent)
        {

        }

        public KxUserGroups(KxBaseClass parent, bool doLoad)
            : base(parent)
        {
            if (doLoad)
            {
                Load();
            }
        }

        public override bool LoadFromXml(string xml, string xpath)
        {
            items.Clear();
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xml);

                XmlNodeList nodes = doc.SelectNodes(@"//results/" + xpath + "/datarow");

                foreach (XmlNode node in nodes)
                {
                    if (node != null)
                    {
                        KxUserGroup item = new KxUserGroup(this, node);
                        items.Add(item);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "LoadFromXml", ex.Message, xpath);
                LastError = ex.Message;
                return false;
            }
        }

        public KxUserGroup GetFieldById(int Id)
        {
            KxUserGroup result = null;

            foreach (KxUserGroup item in items)
            {
                if (item.UserGroupId == Id)
                {
                    result = item;
                    break;
                }
            }
            return result;
        }


    }
}
