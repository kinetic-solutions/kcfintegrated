﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Web.Configuration;
using Kx.Core.Exceptions;
using Kx.Core.Security;
using Kx.Core.ServiceLocator;
using Kx.Core.Database;
using System.Configuration;

namespace Kx.Objects.Common.Authentication
{
    public class KxAuthenticationConfiguration
    {
        #region Constants

        private const string STR_KxAuthenticationSource = "Kx.Authentication.Source";
        private const string STR_KxAuthenticationMessageUserInactive = "Kx.Authentication.Message.UserInactive";

        private const string STR_KxAuthenticationMessagePasswordIncorrect =
            "Kx.Authentication.Message.PasswordIncorrect";

        private const string STR_KxAuthenticationAccountLockoutMins = "Kx.Authentication.Account.LockoutMins";
        private const string STR_KxAuthenticationMessageAccountLocked = "Kx.Authentication.Message.AccountLocked";
        private const string STR_KxAuthenticationPasswordMaxAttempts = "Kx.Authentication.Password.MaxAttempts";

        private const string STR_KxLDAPDirectoryType = "Kx.LDAP.DirectoryType";
        private const string STR_KxLDAPBaseDN = "Kx.LDAP.BaseDN";
        private const string STR_KxLDAPUserNameAttribute = "Kx.LDAP.UserNameAttribute";
        private const string STR_KxLDAPServer = "Kx.LDAP.Server";
        private const string STR_KxLDAPPort = "Kx.LDAP.Port";
        private const string STR_KxLDAPBindUser = "Kx.LDAP.BindUser";
        private const string STR_KxLDAPBindPassword = "Kx.LDAP.BindPassword";
        private const string STR_KxLDAPSearchAttribute = "Kx.LDAP.SearchAttribute";
        private const string STR_KxLDAPSSL = "Kx.LDAP.SSL";
        private const string STR_ADDomain = "ADDomain";
        private const string STR_ADUseSearchPath = "ADUseSearchPath";

        #endregion

        #region Properties

        private string _authenticationInactiveUserMessage;

        public string AuthenticationInactiveUserMessage
        {
            get
            {
                return _authenticationInactiveUserMessage ??
                       GetCustomSettingString(STR_KxAuthenticationMessageUserInactive, "Account not valid",
                           ref _authenticationInactiveUserMessage);
            }
        }

        private string _incorrectPasswordMessage;

        public string IncorrectPasswordMessage
        {
            get
            {
                return _incorrectPasswordMessage ??
                       GetCustomSettingString(STR_KxAuthenticationMessagePasswordIncorrect,
                           "Username / password incorrect", ref _incorrectPasswordMessage);
            }
        }

        private string _lockedAccountMessage;

        public string LockedAccountMessage
        {
            get
            {
                return _lockedAccountMessage ??
                       GetCustomSettingString(STR_KxAuthenticationMessageAccountLocked, "Account not valid",
                           ref _lockedAccountMessage);
            }
        }

        private int _authenticationAttempts = -1;

        public int AuthenticationAttempts
        {
            get
            {
                return _authenticationAttempts < 0
                    ? GetCustomSettingInt(STR_KxAuthenticationPasswordMaxAttempts, 3, ref _authenticationAttempts)
                    : _authenticationAttempts;
            }
        }

        private int _authenticationLockOutMinutes = -1;

        public int AuthenticationLockOutMinutes
        {
            get
            {
                return _authenticationLockOutMinutes < 0
                    ? GetCustomSettingInt(STR_KxAuthenticationAccountLockoutMins, 5, ref _authenticationLockOutMinutes)
                    : _authenticationLockOutMinutes;
            }
        }

        private string _lDAPServer;

        public string LDAPServer
        {
            get { return _lDAPServer ?? GetCustomSettingString(STR_KxLDAPServer, string.Empty, ref _lDAPServer); }
        }

        private int _lDAPPort = -1;

        public int LDAPPort
        {
            get { return _lDAPPort < 0 ? GetCustomSettingInt(STR_KxLDAPPort, 389, ref _lDAPPort) : _lDAPPort; }
        }

        private string _lDAPBaseDN;

        public string LDAPBaseDN
        {
            get { return _lDAPBaseDN ?? GetCustomSettingString(STR_KxLDAPBaseDN, string.Empty, ref _lDAPBaseDN); }
        }

        private string _lDAPBindUser;

        public string LDAPBindUser
        {
            get { return _lDAPBindUser ?? ConfigurationManager.AppSettings["Kx.LDAP.BindUser"] ?? GetCustomSettingString(STR_KxLDAPBindUser, string.Empty, ref _lDAPBindUser); }
        }

        private string _lDAPBindPassword;

        public string LDAPBindPassword
        {
            get { return GetLDAPBindPassword(); }
        }

        private string GetLDAPBindPassword()
        {
            try
            {
                _lDAPBindPassword = _lDAPBindPassword ?? ConfigurationManager.AppSettings["Kx.LDAP.BindPassword"] ??
                                    GetCustomSettingString(STR_KxLDAPBindPassword, string.Empty, ref _lDAPBindPassword);

                if (!string.IsNullOrWhiteSpace(_lDAPBindPassword))
                {
                    IDelphiDecrypter decrypter = ServiceLocator.ResolveService<IDelphiDecrypter>();
                    if (decrypter != null)
                    {
                        _lDAPBindPassword = decrypter.DecryptString(_lDAPBindPassword);
                    }
                }
            }
            catch (Exception e)
            {
                KxLogger.LogActivity(this, "GetLDAPBindPassword", e.Message, "");
            }
            return _lDAPBindPassword;
        }

        private string _lDAPSearchAttribute;

        public string LDAPSearchAttribute
        {
            get
            {
                return _lDAPSearchAttribute ??
                       GetCustomSettingString(STR_KxLDAPSearchAttribute, string.Empty, ref _lDAPSearchAttribute);
            }
        }

        private string _lDAPUsernameAttribute;

        public string LDAPUsernameAttribute
        {
            get
            {
                return _lDAPUsernameAttribute ??
                       GetCustomSettingString(STR_KxLDAPUserNameAttribute, string.Empty, ref _lDAPUsernameAttribute);
            }
        }

        private string _aDUseSearchPath;

        public bool ADUseSearchPath
        {
            get
            {
                return
                    bool.Parse(_aDUseSearchPath ??
                               GetCustomSettingString(STR_ADUseSearchPath, bool.FalseString, ref _aDUseSearchPath));
            }
        }

        private string _aDDomain;

        public string ADDomain
        {
            get { return _aDDomain ?? GetCustomSettingString(STR_ADDomain, string.Empty, ref _aDDomain); }
        }

        private int _ldapType = -1;

        private string _ssl;

        public bool SSL
        {
            get { return (_ssl ?? GetCustomSettingString(STR_KxLDAPSSL, bool.FalseString, ref _ssl)) == "1"; }
        }


        public AuthenticationLDAPType LDAPType
        {
            get
            {
                if (_ldapType < 0)
                {
                    _ldapType = GetCustomSettingInt(STR_KxLDAPDirectoryType, 0, ref _ldapType);
                }
                return (AuthenticationLDAPType) _ldapType;
            }
        }

        protected int _authenticationSource = -1;

        public AuthenticationSource AuthenticationSource
        {
            get
            {
                if (_authenticationSource < 0)
                {
                    _authenticationSource = GetCustomSettingInt(STR_KxAuthenticationSource, 0, ref _authenticationSource);
                }

                return (AuthenticationSource) _authenticationSource;
            }
        }

        #endregion

        #region Private Methods

        protected virtual string GetCustomSettingString(string customSetting, string defaultValue, ref string storage)
        {
            KxCustomSetting cs = KxCustomSetting.GetCustomSettingByCustomSetting(customSetting);

            if (cs != null && cs.CustomSettingId > 0)
            {
                storage = cs.Value;
                return cs.Value;
            }

            storage = cs.Value;
            return defaultValue;
        }

        protected virtual int GetCustomSettingInt(string customSetting, int defaultValue, ref int storage)
        {
            string tmpDefault = storage.ToString();
            string tmpVal = GetCustomSettingString(customSetting, defaultValue.ToString(), ref tmpDefault);
            int result = defaultValue;

            int.TryParse(tmpVal, out result);

            storage = result;

            return result;
        }

        #endregion
    }
}