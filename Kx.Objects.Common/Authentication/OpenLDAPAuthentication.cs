﻿using System;
using System.Collections.Generic;
using System.DirectoryServices.Protocols;
using System.Text;
using Kx.Core.Exceptions;
using Kx.Core.Configuration;

namespace Kx.Objects.Common.Authentication
{
    class OpenLDAPAuthentication : LDAPAuthentication
    {
        #region Constructors
        public OpenLDAPAuthentication(KxAuthenticationConfiguration configuration)
            : base(configuration)
        {
        }
        #endregion

        #region LDAPAuthentication Members
        public override bool Authenticate(string username, string password)
        {
            bool result = false;

            // Lookup the user in the directory
            SearchResultEntry user = FindEntry(username, password);

            string expandedUserName = ExpandUserName(user, username);

            // Attempt to bind as the new user.
            LdapConnection conn = GetLDAPConnection(expandedUserName, password);

            if (conn != null)
            {
                result = true;
            }

            return result;
        }
        #endregion

        #region Private Methods
        private string ExpandUserName(SearchResultEntry search, string username)
        {
            string result = username;

            if (search != null)
            {
                if (!string.IsNullOrWhiteSpace(search.DistinguishedName))
                {
                    result = search.DistinguishedName;
                }
                else
                {
                    result = string.Format("{0}={1},{2}", UserNameAttribute, username, GetExtension(search.DistinguishedName));
                    
                }
            }
            else if (!string.IsNullOrEmpty(BaseDN))
            {
                result = string.Format("{0}={1},{2}", UserNameAttribute, username, BaseDN);
            }

            return result;
        }


        private SearchResultEntry FindEntry(string username, string password)
        {
            string bindUser = GetBindUser(username);
            string bindPassword = GetBindPassword(password);

            LdapConnection conn = GetLDAPConnection(bindUser, bindPassword);

            if (conn != null)
            {
                using (conn)
                {
                    try
                    {
                        SearchRequest request = new SearchRequest();
                        request.DistinguishedName = BaseDN;
                        request.Filter = string.Format(KxDbConfiguration.GetConfiguration("LDAPSearchFilter", "(&(objectclass=*)({0}={1}))"), SearchAttribute, username);
                        request.Scope = System.DirectoryServices.Protocols.SearchScope.Subtree;

                        SearchResponse response = (SearchResponse)conn.SendRequest(request);

                        if (response.Entries != null && response.Entries.Count > 0)
                        {
                            return response.Entries[0];
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new KxAuthenticationException(string.Format("Error looking up LDAP user: {0}", ex.Message));
                    }
                }
            }

            return null;
        }

        private LdapConnection GetLDAPConnection(string username, string password)
        {
            LdapConnection result = null;

            try
            {
                LdapDirectoryIdentifier dir = null;

                if (Port != 0)
                {
                    dir = new LdapDirectoryIdentifier(Server, Port);
                }
                else
                {
                    dir = new LdapDirectoryIdentifier(Server);
                }

                result = new LdapConnection(dir);

                result.AuthType = AuthType.Basic;

                System.Net.NetworkCredential credentials = new System.Net.NetworkCredential(username, password);
                result.Bind(credentials);
            }
            catch (Exception ex)
            {
                throw new KxAuthenticationException(string.Format("Error getting LDAP connection: {0}", ex.Message));
            }

            return result;
        }
        #endregion
    }
}
