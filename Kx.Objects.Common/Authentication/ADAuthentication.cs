﻿using System;
using System.Collections.Generic;
using System.DirectoryServices;
using System.Runtime.InteropServices;
using System.Text;
using Kx.Core.Configuration;
using Kx.Core.Exceptions;

namespace Kx.Objects.Common.Authentication
{
    public class ADAuthentication : LDAPAuthentication
    {
        string searchPath = string.Empty;

        #region Constructors
        public ADAuthentication(KxAuthenticationConfiguration configuration)
            : base(configuration)
        {

        }
        #endregion
        #region LDAPAuthentication Members

        public override bool Authenticate(string username, string password)
        {
            bool result = false;
            DirectoryEntry user = null;
            try
            {
                user = FindEntry(username, password, SearchScope.OneLevel);
            }
            catch (Exception ex)
            {
                throw new KxAuthenticationException(string.Format("Error binding user: {0}", ex.Message));
            }

            if (user != null)
            {
                DirectoryEntry de = GetServerConnection(username, password);

                if (de != null)
                {
                    try
                    {
                        try
                        {
                            using (DirectoryEntry entry = new DirectoryEntry(de.Path, ExtendUserName(username), password))
                            {
                                object nativeObject = entry.NativeObject;
                            }
                            result = true;
                        }
                        catch (COMException)
                        {
                            // Not authenticated.
                            result = false;
                        }
                        catch (Exception ex)
                        {
                            throw new KxAuthenticationException(string.Format("Error authenticating user: {0}", ex.Message));
                        }
                    }
                    finally
                    {
                        de.Close();
                    }
                }
            }

            return result;
        }
        #endregion

        #region Private Methods
        protected string ExtendUserName(string userName)
        {
            string result = userName;


            if (!String.IsNullOrEmpty(searchPath))
            {
                result = String.Format("{0}={1},{2}", UserNameAttribute, userName, BaseDN);
            }
            else if (!string.IsNullOrEmpty(ADDomain))
            {
                result = String.Format("{0}={1},{2}", UserNameAttribute, userName, ADDomain);
            }

            return result;
        }


        private DirectoryEntry FindEntry(string username, string password, SearchScope scope)
        {
            DirectoryEntry result = null;

            DirectoryEntry de = GetServerConnection(GetBindUser(username), GetBindPassword(password));

            if (de != null)
            {
                try
                {
                    try
                    {
                        using (DirectorySearcher ds = new DirectorySearcher())
                        {
                            ds.SearchRoot = de;
                            ds.Filter = String.Format(KxDbConfiguration.GetConfiguration("LDAPSearchFilter", "(&(objectclass=*)({0}={1}))"), UserNameAttribute, username);
                            ds.SearchScope = scope;
                            ds.CacheResults = true;
                            ds.ReferralChasing = ReferralChasingOption.None;
                            SearchResult src = ds.FindOne();
                            if (src != null)
                            {
                                if (UseSearchPath)
                                {
                                    searchPath = GetExtension(src.Path);
                                }
                                result = src.GetDirectoryEntry();
                            }
                            else if (src == null && scope == SearchScope.OneLevel)
                            {
                                result = FindEntry(username, password, SearchScope.Subtree);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new KxAuthenticationException(string.Format("Error finding entry: {0}", ex.Message));
                    }
                }
                finally
                {
                    de.Close();
                }
            }
            else
            {
                throw new KxAuthenticationException(string.Format("Finding AD Entry, no connection"));
            }
            return result;
        }

        private DirectoryEntry GetServerConnection(string username, string password)
        {
            DirectoryEntry result = null;
            try
            {
                string serverPath = Server;

                if (Port > 0)
                {
                    serverPath += String.Format(":{0}", Port);
                }

                if (username != "")
                {
                    result = new DirectoryEntry(String.Format("LDAP://{0}", serverPath));
                    result.AuthenticationType = AuthenticationTypes.Secure;
                    result.Username = username;
                    result.Password = password;
                }
                else
                {
                    result = new DirectoryEntry(String.Format("LDAP://{0}", serverPath));
                }

                result.RefreshCache();
            }
            catch (Exception ex)
            {
                throw new KxAuthenticationException(string.Format("Error connecting to AD Server: {0}", ex.Message, ex));
            }
            return result;
        }
        #endregion

    }
}
