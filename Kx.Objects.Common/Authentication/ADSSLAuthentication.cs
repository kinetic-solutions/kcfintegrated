﻿#region copyright

// =======================================================
// Product:   KCF.Student
// Project:   Kx.Objects.Common
// File:      ADSSLAuthentication.cs
// Created:   28/10/2016
// By:        Steve Ashley
// Modified:  28/10/2016
// 
// Copyright © Kinetic Solutions Ltd 2016
// All rights reserved.
// 
// No portion of this file may be re-created, copied or otherwise
// used without the express permission of Kinetic Solutions Ltd.
// =======================================================

#endregion

#region usings

using System;
using System.DirectoryServices.Protocols;
using System.Net;
using Kx.Core.Exceptions;

#endregion

namespace Kx.Objects.Common.Authentication
{
    public class ADSSLAuthentication : LDAPAuthentication
    {
        #region Constructors

        public ADSSLAuthentication(KxAuthenticationConfiguration configuration)
            : base(configuration)
        {
        }

        #endregion

        #region Public Methods

        public override bool Authenticate(string username, string password)
        {
            string bindUsername = GetBindUser(username);
            string bindPassword = GetBindPassword(password);

            try
            {
                using (var ldapConnection = GetConnection(bindUsername, bindPassword))
                {
                    if (!BindUserIsFinalUser)
                    {
                        SearchRequest request = new SearchRequest(BaseDN, string.Format("({0}={1})", SearchAttribute, username), SearchScope.Subtree);
                        SearchResponse response = (SearchResponse) ldapConnection.SendRequest(request);

                        if (response.Entries.Count == 1)
                        {
                            SearchResultEntry entry = response.Entries[0];
                            string userName = (string) entry.Attributes[UserNameAttribute].GetValues(typeof(string))[0];
                            using (var newConnection = GetConnection(userName, password))
                            {
                            }
                            return true;
                        }
                    }
                    else
                    {
                        return true;
                    }
                }
            }
            catch (LdapException)
            { }
            catch (Exception ex)
            {
                throw new KxAuthenticationException(string.Format("Error authenticating user: {0}", ex.Message));
            }

            return false;
        }

        #endregion

        #region Private Methods

        private LdapConnection GetConnection(string username, string password)
        {
            LdapConnection ldapConnection = new LdapConnection(string.Format("{0}:{1}", Server, Port));
            var networkCredential = new NetworkCredential(username, password);

            if (username.Contains("\\"))
            {
                var splitUsername = username.Split(new char[] {'\\'}, StringSplitOptions.RemoveEmptyEntries);
                networkCredential = new NetworkCredential(splitUsername[1], password, splitUsername[0]);
            }

            ldapConnection.SessionOptions.SecureSocketLayer = true;
            ldapConnection.SessionOptions.VerifyServerCertificate += delegate { return true; };
            ldapConnection.AuthType = AuthType.Ntlm;
            ldapConnection.Bind(networkCredential);
            return ldapConnection;
        }

        #endregion
    }
}