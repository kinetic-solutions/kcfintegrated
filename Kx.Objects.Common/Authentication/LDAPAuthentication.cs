﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Kx.Objects.Common.Authentication
{
    public abstract class LDAPAuthentication
    {
        #region Properties and Attributes
        protected string Server { get; set; }
        protected int Port { get; set; }
        protected string BaseDN { get; set; }
        protected string BindUser { get; set; }
        protected string BindPassword { get; set; }
        protected string SearchAttribute { get; set; }
        protected string UserNameAttribute { get; set; }
        protected bool UseSearchPath { get; set; }
        protected string ADDomain { get; set; }
        protected bool SSL { get; set; }

        protected bool BindUserIsFinalUser
        {
            get { return BindUser.Contains("%CURRENTUSERNAME%"); }
        }
        #endregion

        #region Constructors
        public LDAPAuthentication(KxAuthenticationConfiguration configuration)
        {
            Server = configuration.LDAPServer;
            Port = configuration.LDAPPort;
            BaseDN = configuration.LDAPBaseDN;
            BindUser = configuration.LDAPBindUser;
            BindPassword = configuration.LDAPBindPassword;
            SearchAttribute = configuration.LDAPSearchAttribute;
            UserNameAttribute = configuration.LDAPUsernameAttribute;
            UseSearchPath = configuration.ADUseSearchPath;
            ADDomain = configuration.ADDomain;
            SSL = configuration.SSL;
        }
        #endregion

        #region Public Methods
        public abstract bool Authenticate(string username, string password);
        #endregion

        #region Protected Methods
        protected string GetBindPassword(string password)
        {
            return BindPassword.Replace("%CURRENTPASSWORD%", password);
        }

        protected string GetBindUser(string username)
        {
            return BindUser.Replace("%CURRENTUSERNAME%", username);
        }

        protected string GetExtension(string path)
        {
            string result = "";

            int idx = path.ToUpper().IndexOf("OU");

            if (idx > -1)
            {
                result = path.Remove(0, idx);
            }

            return result;
        }
        #endregion
    }
}
