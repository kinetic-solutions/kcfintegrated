﻿using Kx.Core.Exceptions;
using Kx.Core.Security;
using System;
using System.Collections.Generic;
using System.Text;

namespace Kx.Objects.Common.Authentication
{
    public class KxAuthenticationStore : AuthenticationStore
    {
        #region Constructors
        /// <summary>
        /// Base object to entire system, containing the factory and auditor used throughout.
        /// </summary>
        public KxAuthenticationStore()
        {

        }
        #endregion

        #region Public Methods
        public override AuthenticateResponse Authenticate(string username, string password)
        {
            AuthenticateResponse result = new AuthenticateResponse();
            try
            {
                KxUser user = new KxUser(null, username);
                if (user.UserId != 0)
                {
                    KxAuthenticationConfiguration cf = new KxAuthenticationConfiguration();

                    if (user.Inactive)
                    {
                        result.Authenticated = false;
                        result.AuthenticationMessage = cf.AuthenticationInactiveUserMessage;
                        return result;
                    }

                    if (cf.AuthenticationSource == AuthenticationSource.KxInternal
                        ||
                        cf.AuthenticationSource == AuthenticationSource.MixedMode &&
                        user.AuthenticationType == (int) AuthenticationSource.KxInternal)
                    {
                        if (user.ChangePasswordOnLogin)
                        {
                            result.Authenticated = false;
                            result.AuthenticationMessage =
                                "You are required to change you password on login, please use Kx to change your password before logging in here";
                        }
                        else
                        {
                            if (AuthenticateInternal(password, user, cf))
                            {
                                result.Authenticated = true;
                                result.EmailAddress = user.Email;
                                result.UserIdentifier = user.UserId.ToString();
                                result.UserName = user.UserName;
                            }
                            else
                            {
                                result.Authenticated = false;
                                result.AuthenticationMessage = cf.IncorrectPasswordMessage;
                            }
                        }
                    }
                    else if (cf.AuthenticationSource == AuthenticationSource.LDAP
                             ||
                             cf.AuthenticationSource == AuthenticationSource.MixedMode &&
                             user.AuthenticationType == (int) AuthenticationSource.LDAP)
                    {
                        if (AuthenticateLDAP(username, password, cf))
                        {
                            result.Authenticated = true;
                            result.EmailAddress = user.Email;
                            result.UserIdentifier = user.UserId.ToString();
                            result.UserName = user.UserName;
                        }
                        else
                        {
                            result.Authenticated = false;
                            result.AuthenticationMessage = cf.IncorrectPasswordMessage;
                        }
                    }
                }
                else
                {
                    result.AuthenticationMessage = "User not found or password is incorrect";
                }
            }
            catch (KxAuthenticationException ex)
            {
                result.Authenticated = false;
                result.AuthenticationMessage = ex.Message;
            }
            catch (Exception ex)
            {
                result.Authenticated = false;
                result.AuthenticationMessage = ex.Message;
            }

            return result;
        }

        public override bool UsernameExists(string username)
        {
            return new KxUser(null, username).UserId != 0;
        }
        #endregion

        #region Private Methods
        private bool AuthenticateInternal(string password, KxUser user, KxAuthenticationConfiguration myStayConfiguration)
        {
            bool result = new PasswordManager(password, user.GuId, user.Password).IsValid;
            // Account is locked, so fail authentication
            if (user.AccountLockedUntil.HasValue && user.AccountLockedUntil > DateTime.Now)
                throw new KxAuthenticationException(myStayConfiguration.LockedAccountMessage);

            if (!result)
            {
                user.AuthAttempts += 1;
                user.Save();

                if (user.AuthAttempts >= myStayConfiguration.AuthenticationAttempts)
                {
                    user.AccountLockedUntil = DateTime.Now.AddMinutes(myStayConfiguration.AuthenticationLockOutMinutes);
                    user.AuthAttempts = 0;
                    user.Save();
                }
            }
            else
            {
                if (user.AuthAttempts > 0)
                {
                    user.AuthAttempts = 0;
                    user.Save();
                }
            }

            return result;
        }

        private bool AuthenticateLDAP(string username, string password, KxAuthenticationConfiguration cf)
        {
            LDAPAuthentication ldap = null;

            if (cf.LDAPType == AuthenticationLDAPType.OpenLDAP)
            {
                ldap = new OpenLDAPAuthentication(cf);
            }
            else
            {
                if (cf.SSL)
                {
                    ldap = new ADSSLAuthentication(cf);
                }
                else
                {
                    ldap = new ADAuthentication(cf);
                }
            }

            return ldap.Authenticate(username, password);
        }
        #endregion
    }

}
