using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;

namespace Kx.Objects.Common
{
    [Serializable]
    public class KxResidentialRoom : KxBaseClass
    {
        #region Attributes & Properties

        protected int      blockId;
        protected int      bedroomTypeId;
        protected string   name;
        protected int      capacity;
        protected int      floorNumber;
        protected int      cleaningRotaId;
        protected int      extNo;
        protected int      beds;
        protected int      viewId;
        protected string   info;
        protected string   maidStatus;
        protected int      subBlockId;
        protected bool     smoking;
        protected bool     disabled;
        protected bool     lastLet;
        protected bool     vip;
        protected int      typicalCapacity;
        protected DateTime ?creationDate;
        protected int      studentYearId;
        protected int      gender;
        protected int      headerResidentialRoomId;
        protected int      displayOrder;
        protected string   maidStatusNotes;
        protected bool     customField1;
        protected bool     customField2;
        protected int      studentBedroomTypeId;
        protected bool     inactive;
        protected int      singleSex;
        protected int      chargeBand;
        protected int      nominalId;
        protected string   externalCode;
        protected string   externalId;
        protected string addressLine;
        protected int acadedmicYearID;

        protected string extensionPrefix = "";

        protected KxBedroomType bedroomType = null;
        protected KxRoomConfiguration roomConfiguration = null;
        protected KxCustomSetting customAlias = null;
        protected KxCustomSetting customAddress = null;

        protected KxSiteBlock siteBlock = null;
        protected KxSiteBlock subBlock = null;

        public int      ResidentialRoomId
        {
            get
            { return base.dBIdentifier;}
        }

        public int      BlockId
        {
            get
            { return blockId;}
            set
            { 
                AddToAudit("BlockId", blockId, value);
                blockId = value;
            }
        }

        public int      BedroomTypeId
        {
            get
            { return bedroomTypeId;}
            set
            { 
                AddToAudit("BedroomTypeId", bedroomTypeId, value);
                bedroomTypeId = value;
            }
        }

        public string   Name
        {
            get
            { return name;}
            set
            { 
                AddToAudit("Name", name, value);
                name = value;
            }
        }

        public int      Capacity
        {
            get
            { return capacity;}
            set
            { 
                AddToAudit("Capacity", capacity, value);
                capacity = value;
            }
        }

        public int      FloorNumber
        {
            get
            { return floorNumber;}
            set
            { 
                AddToAudit("FloorNumber", floorNumber, value);
                floorNumber = value;
            }
        }

        public int      CleaningRotaId
        {
            get
            { return cleaningRotaId;}
            set
            { 
                AddToAudit("CleaningRotaId", cleaningRotaId, value);
                cleaningRotaId = value;
            }
        }

        public int      ExtNo
        {
            get
            { return extNo;}
            set
            { 
                AddToAudit("ExtNo", extNo, value);
                extNo = value;
            }
        }

        public int ExtensionDigits
        {
            get
            {
                return extNo.ToString().Length;
            }
        }

        public int FullExtensionDigits
        {
            get
            {
                return FullExtension.Length;
            }
        }

        public string ExtensionPrefix
        {
            get
            { return extensionPrefix; }
            set
            { extensionPrefix = value; }
        }

        public string FullExtension
        {
            get
            { return extensionPrefix + extNo.ToString(); }
        }

        public int      Beds
        {
            get
            { return beds;}
            set
            { 
                AddToAudit("Beds", beds, value);
                beds = value;
            }
        }

        public int      ViewId
        {
            get
            { return viewId;}
            set
            { 
                AddToAudit("ViewId", viewId, value);
                viewId = value;
            }
        }

        public string   Info
        {
            get
            { return info;}
            set
            { 
                AddToAudit("Info", info, value);
                info = value;
            }
        }

        public string   MaidStatus
        {
            get
            { return maidStatus;}
            set
            { 
                AddToAudit("MaidStatus", maidStatus, value);
                maidStatus = value;
            }
        }

        public int      SubBlockId
        {
            get
            { return subBlockId;}
            set
            { 
                AddToAudit("SubBlockId", subBlockId, value);
                subBlockId = value;
            }
        }

        public bool     Smoking
        {
            get
            { return smoking;}
            set
            { 
                AddToAudit("Smoking", smoking, value);
                smoking = value;
            }
        }

        public bool     Disabled
        {
            get
            { return disabled;}
            set
            { 
                AddToAudit("Disabled", disabled, value);
                disabled = value;
            }
        }

        public bool     LastLet
        {
            get
            { return lastLet;}
            set
            { 
                AddToAudit("LastLet", lastLet, value);
                lastLet = value;
            }
        }

        public bool     Vip
        {
            get
            { return vip;}
            set
            { 
                AddToAudit("Vip", vip, value);
                vip = value;
            }
        }

        public int      TypicalCapacity
        {
            get
            { return typicalCapacity;}
            set
            { 
                AddToAudit("TypicalCapacity", typicalCapacity, value);
                typicalCapacity = value;
            }
        }

        public DateTime ?CreationDate
        {
            get
            { return creationDate;}
            set
            { 
                AddToAudit("CreationDate", creationDate, value);
                creationDate = value;
            }
        }

        public int      StudentYearId
        {
            get
            { return studentYearId;}
            set
            { 
                AddToAudit("StudentYearId", studentYearId, value);
                studentYearId = value;
            }
        }

        public int      Gender
        {
            get
            { return gender;}
            set
            { 
                AddToAudit("Gender", gender, value);
                gender = value;
            }
        }

        public int      HeaderResidentialRoomId
        {
            get
            { return headerResidentialRoomId;}
            set
            { 
                AddToAudit("HeaderResidentialRoomId", headerResidentialRoomId, value);
                headerResidentialRoomId = value;
            }
        }

        public int      DisplayOrder
        {
            get
            { return displayOrder;}
            set
            { 
                AddToAudit("DisplayOrder", displayOrder, value);
                displayOrder = value;
            }
        }

        public string   MaidStatusNotes
        {
            get
            { return maidStatusNotes;}
            set
            { 
                AddToAudit("MaidStatusNotes", maidStatusNotes, value);
                maidStatusNotes = value;
            }
        }

        public bool     CustomField1
        {
            get
            { return customField1;}
            set
            { 
                AddToAudit("CustomField1", customField1, value);
                customField1 = value;
            }
        }

        public string CustomField1String
        {
            get
            {
                return customField1 ? "Yes":"No";
            }
        }

        public bool     CustomField2
        {
            get
            { return customField2;}
            set
            { 
                AddToAudit("CustomField2", customField2, value);
                customField2 = value;
            }
        }

        public string CustomField2String
        {
            get
            {
                return customField2 ? "Yes":"No";
            }
        }

        public int      StudentBedroomTypeId
        {
            get
            { return studentBedroomTypeId;}
            set
            { 
                AddToAudit("StudentBedroomTypeId", studentBedroomTypeId, value);
                studentBedroomTypeId = value;
            }
        }

        public bool     Inactive
        {
            get
            { return inactive;}
            set
            { 
                AddToAudit("Inactive", inactive, value);
                inactive = value;
            }
        }

        public int      SingleSex
        {
            get
            { return singleSex;}
            set
            { 
                AddToAudit("SingleSex", singleSex, value);
                singleSex = value;
            }
        }

        public int      ChargeBand
        {
            get
            { return chargeBand;}
            set
            { 
                AddToAudit("ChargeBand", chargeBand, value);
                chargeBand = value;
            }
        }

        public int      NominalId
        {
            get
            { return nominalId;}
            set
            { 
                AddToAudit("NominalId", nominalId, value);
                nominalId = value;
            }
        }

        public string   ExternalCode
        {
            get
            { return externalCode;}
            set
            { 
                AddToAudit("ExternalCode", externalCode, value);
                externalCode = value;
            }
        }

        public string   ExternalId
        {
            get
            { return externalId;}
            set
            { 
                AddToAudit("ExternalId", externalId, value);
                externalId = value;
            }
        }

        public string AddressLine
        {
            get
            { return addressLine; }
            set
            {
                AddToAudit("AddressLine", addressLine, value);
                addressLine = value;
            }
        }

        public string BedroomTypeName
        {
            get
            {
                if (BedroomType != null)
                {
                    return BedroomType.Description;
                }
                else
                {
                    return "";
                }
            }
        }

        public KxBedroomType BedroomType
        {
            get
            {
                if (bedroomType == null && bedroomTypeId > 0)
                {
                    bedroomType = new KxBedroomType(this, bedroomTypeId);
                }
                return bedroomType;
            }
            set
            {
                if (bedroomType == value)
                    return;
                bedroomType = value;
            }
        }
        
        public KxRoomConfiguration RoomConfiguration
        {
            get
            {
                if (ResidentialRoomId > 0 && roomConfiguration == null)
                {
                    roomConfiguration = new KxRoomConfiguration(this, ResidentialRoomId);
                }
                return roomConfiguration;
            }
        }

        public KxCustomSetting CustomAlias
        {
            get
            {
                if (ResidentialRoomId > 0 && customAlias == null)
                {
                    customAlias = new KxCustomSetting(this, "Kx.Student.Flats.Alias", ResidentialRoomId);
                }
                return customAlias;
            }
        }

        public string CustomAliasString1
        {
            get
            {
                string result = "";
                if (!String.IsNullOrEmpty(CustomAlias.String1))
                {
                    result = CustomAlias.String1;
                }
                return result.TrimEnd();
            }
        }

        public KxCustomSetting CustomAddress
        {
            get
            {
                if (ResidentialRoomId > 0 && customAddress == null)
                {
                    customAddress = new KxCustomSetting(this, "Kx.Student.Flats.Addresses", ResidentialRoomId);
                }
                return customAddress;
            }
        }

        public string CustomAddressString1
        {
            get
            {
                string result = "";
                if (!String.IsNullOrEmpty(CustomAddress.String1))
                {
                    result = CustomAddress.String1;
                }
                return result;
            }
        }

        public KxSiteBlock Block
        {
            get
            {
                if (blockId > 0 && siteBlock == null)
                {
                    siteBlock = new KxSiteBlock(this, blockId);
                }
                return siteBlock;
            }
        }

        public KxSiteBlock SubBlock
        {
            get
            {
                if (subBlockId > 0 && subBlock == null)
                {
                    subBlock = new KxSiteBlock(this, subBlockId);
                }
                return subBlock;
            }
        }

        public string BlockUrl
        {
            get
            {
                if (Block != null)
                {
                    return Block.BlockUrl;
                }
                else
                {
                    return "";
                }
            }
        }

        public string RoomAddress
        {
            get
            {
                return GetCustomDefaultAddress();
            }
        }

        public string RoomAlias
        {
            get
            {
                return GetCustomDefaultAlias();
            }
        }
        
        #endregion 

        public KxResidentialRoom(KxBaseClass parent, int residentialroomid) : base(parent)
		{
			base.dBIdentifier = residentialroomid;
			acadedmicYearID = 0;
			Load();
        }

        public KxResidentialRoom(KxBaseClass parent, int residentialroomid, int academicyearid) : base(parent)
        {
            base.dBIdentifier = residentialroomid;
			acadedmicYearID = academicyearid;
            Load();
        }

        public override KxData ConfigureData(ActionType actiontype)
        {
#if IMPERIAL_CHANGES_NOT_TESTED
			if (actiontype == ActionType.Select)
			{
				KxData result = new KxData("residentialrooms", "qryWebResidentialRoom", TargetType.StoredProc);
				result.AddParameter("ResidentialRoomID", ResidentialRoomId, KxValueType.Int);
				result.AddParameter("AcademicYearID", acadedmicYearID, KxValueType.Int);
				return result;
			}
			else
			{
#endif
				KxData result = new KxData("residentialrooms", "ResidentialRooms", TargetType.Table);
				result.AutoKeyField = new KxDataValue("ResidentialRoomId", ResidentialRoomId, KxValueType.Int);

				if ((actiontype == ActionType.Update) && (ResidentialRoomId == 0))
				{
					result.Action = ActionType.Insert;
				}
				else
				{
					result.Action = actiontype;
				}

				if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
				{
					result.AddParameter("blockId", blockId, KxValueType.Int);
					result.AddParameter("bedroomTypeId", bedroomTypeId, KxValueType.Int);
					result.AddParameter("Name", Name, KxValueType.String);
					result.AddParameter("capacity", capacity, KxValueType.Int);
					result.AddParameter("floorNumber", floorNumber, KxValueType.Int);
					result.AddParameter("cleaningRotaId", cleaningRotaId, KxValueType.Int);
					result.AddParameter("extNo", extNo, KxValueType.Int);
					result.AddParameter("beds", beds, KxValueType.Int);
					result.AddParameter("viewId", viewId, KxValueType.Int);
					result.AddParameter("info", info, KxValueType.String);
					result.AddParameter("maidStatus", maidStatus, KxValueType.String);
					result.AddParameter("subBlockId", subBlockId, KxValueType.Int);
					result.AddParameter("smoking", smoking, KxValueType.Boolean);
					result.AddParameter("disabled", disabled, KxValueType.Boolean);
					result.AddParameter("lastLet", lastLet, KxValueType.Boolean);
					result.AddParameter("vip", vip, KxValueType.Boolean);
					result.AddParameter("typicalCapacity", typicalCapacity, KxValueType.Int);
					result.AddParameter("creationDate", creationDate, KxValueType.DateTime);
					result.AddParameter("studentYearId", studentYearId, KxValueType.Int);
					result.AddParameter("gender", gender, KxValueType.Int);
					result.AddParameter("headerResidentialRoomId", headerResidentialRoomId, KxValueType.Int);
					result.AddParameter("displayOrder", displayOrder, KxValueType.Int);
					result.AddParameter("maidStatusNotes", maidStatusNotes, KxValueType.String);
					result.AddParameter("customField1", customField1, KxValueType.Boolean);
					result.AddParameter("customField2", customField2, KxValueType.Boolean);
					result.AddParameter("studentBedroomTypeId", studentBedroomTypeId, KxValueType.Int);
					result.AddParameter("inactive", inactive, KxValueType.Boolean);
					result.AddParameter("singleSex", singleSex, KxValueType.Int);
					result.AddParameter("chargeBand", chargeBand, KxValueType.Int);
					result.AddParameter("nominalId", nominalId, KxValueType.Int);
					result.AddParameter("externalCode", externalCode, KxValueType.String);
					result.AddParameter("externalId", externalId, KxValueType.String);
				}
				return result;
#if IMPERIAL_CHANGES_NOT_TESTED
			}
			return null;
#endif
        }


        public override void LoadFromXml(string xml,XmlNode node)
        {
            dBIdentifier = KxXmlUtils.GetNodeValue(node, "residentialroomid", 0);
            blockId = KxXmlUtils.GetNodeValue(node, "blockid", 0);
            bedroomTypeId = KxXmlUtils.GetNodeValue(node, "bedroomtypeid", 0);
            Name = KxXmlUtils.GetNodeValue(node, "name", "");
            capacity = KxXmlUtils.GetNodeValue(node, "capacity", 0);
            floorNumber = KxXmlUtils.GetNodeValue(node, "floornumber", 0);
            cleaningRotaId = KxXmlUtils.GetNodeValue(node, "cleaningrotaid", 0);
            extNo = KxXmlUtils.GetNodeValue(node, "extno", 0);
            beds = KxXmlUtils.GetNodeValue(node, "beds", 0);
            viewId = KxXmlUtils.GetNodeValue(node, "viewid", 0);
            info = KxXmlUtils.GetNodeValue(node, "info", "");
            maidStatus = KxXmlUtils.GetNodeValue(node, "maidstatus", "");
            subBlockId = KxXmlUtils.GetNodeValue(node, "subblockid", 0);
            smoking = KxXmlUtils.GetNodeValue(node, "smoking", false);
            disabled = KxXmlUtils.GetNodeValue(node, "disabled", false);
            lastLet = KxXmlUtils.GetNodeValue(node, "lastlet", false);
            vip = KxXmlUtils.GetNodeValue(node, "vip", false);
            typicalCapacity = KxXmlUtils.GetNodeValue(node, "typicalcapacity", 0);
            creationDate = KxXmlUtils.GetNodeValueDT(node, "creationdate", null);
            studentYearId = KxXmlUtils.GetNodeValue(node, "studentyearid", 0);
            gender = KxXmlUtils.GetNodeValue(node, "gender", 0);
            headerResidentialRoomId = KxXmlUtils.GetNodeValue(node, "headerresidentialroomid", 0);
            displayOrder = KxXmlUtils.GetNodeValue(node, "displayorder", 0);
            maidStatusNotes = KxXmlUtils.GetNodeValue(node, "maidstatusnotes", "");
            customField1 = KxXmlUtils.GetNodeValue(node, "customfield1", false);
            customField2 = KxXmlUtils.GetNodeValue(node, "customfield2", false);
            studentBedroomTypeId = KxXmlUtils.GetNodeValue(node, "studentbedroomtypeid", 0);
            inactive = KxXmlUtils.GetNodeValue(node, "inactive", false);
            singleSex = KxXmlUtils.GetNodeValue(node, "singlesex", 0);
            chargeBand = KxXmlUtils.GetNodeValue(node, "chargeband", 0);
            nominalId = KxXmlUtils.GetNodeValue(node, "nominalid", 0);
            externalCode = KxXmlUtils.GetNodeValue(node, "externalcode", "");
            externalId = KxXmlUtils.GetNodeValue(node, "externalid", "");
            addressLine = KxXmlUtils.GetNodeValue(node, "addressline", "");
        }

        private string GetCustomDefaultAlias()
        {
            string result = "";

            if (!String.IsNullOrEmpty(CustomAliasString1))
            {
                result += CustomAliasString1 + " ";
            }
            else
            {
                if (Block != null && CustomAddressString1 == "")
                {
                    result += Name + " " + Block.Name;
                }
                else
                {
                    result += Name + " ";
                }
            }
            return result.TrimEnd();
        }

        private string GetCustomDefaultAddress()
        {
            string result = "";

            if (!String.IsNullOrEmpty(CustomAddressString1))
            {
                result += CustomAddressString1 + " ";
            }
            else
            {
                try
                {
                    result += Block.Address.Address + " "+Block.Address.Town+" "+Block.Address.Postcode;
                }
                catch
                { }
            }
            return result;
        }

     
    }
}

