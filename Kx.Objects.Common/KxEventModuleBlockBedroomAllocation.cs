using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;

namespace Kx.Objects.Common
{
    [Serializable]
    public class KxEventModuleBlockBedroomAllocation : KxBaseClass
    {
        #region Attributes & Properties

        protected int      eventModuleId;
        protected int      blockId;
        protected DateTime ?date;
        protected int      allocation;
        protected int      bedroomTypeId;
        protected string   status;
        protected int      bookedAllocation;
        protected int      notesId;
        protected int      residentialRoomId;
        protected int      residentialRoomAttributeId;

        public int      EventModuleId
        {
            get
            { return eventModuleId;}
            set
            { 
                AddToAudit("EventModuleId", eventModuleId, value);
                eventModuleId = value;
            }
        }

        public int      BlockId
        {
            get
            { return blockId;}
            set
            { 
                AddToAudit("BlockId", blockId, value);
                blockId = value;
            }
        }

        public DateTime ?Date
        {
            get
            { return date;}
            set
            { 
                AddToAudit("Date", date, value);
                date = value;
            }
        }

        public int      Allocation
        {
            get
            { return allocation;}
            set
            { 
                AddToAudit("Allocation", allocation, value);
                allocation = value;
            }
        }

        public int      BedroomTypeId
        {
            get
            { return bedroomTypeId;}
            set
            { 
                AddToAudit("BedroomTypeId", bedroomTypeId, value);
                bedroomTypeId = value;
            }
        }

        public string   Status
        {
            get
            { return status;}
            set
            { 
                AddToAudit("Status", status, value);
                status = value;
            }
        }

        public int      BookedAllocation
        {
            get
            { return bookedAllocation;}
            set
            { 
                AddToAudit("BookedAllocation", bookedAllocation, value);
                bookedAllocation = value;
            }
        }

        public int      NotesId
        {
            get
            { return notesId;}
            set
            { 
                AddToAudit("NotesId", notesId, value);
                notesId = value;
            }
        }

        public int      ResidentialRoomId
        {
            get
            { return residentialRoomId;}
            set
            { 
                AddToAudit("ResidentialRoomId", residentialRoomId, value);
                residentialRoomId = value;
            }
        }

        public int      ResidentialRoomAttributeId
        {
            get
            { return residentialRoomAttributeId;}
            set
            { 
                AddToAudit("ResidentialRoomAttributeId", residentialRoomAttributeId, value);
                residentialRoomAttributeId = value;
            }
        }

        public int      EmbbaId
        {
            get
			{ return base.dBIdentifier; }
        }


        #endregion 


        public KxEventModuleBlockBedroomAllocation(KxBaseClass parent)
            : base(parent)
        {}

        public KxEventModuleBlockBedroomAllocation(KxBaseClass parent, XmlNode node)
            : base(parent)
        {
             LoadFromXml("", node);
        }

        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("eventmoduleblockbedroomallocation", "EventModuleBlockBedroomAllocation", TargetType.Table);

             result.AutoKeyField = new KxDataValue("embbaid",EmbbaId, KxValueType.Int);

             if ((actiontype == ActionType.Update) && (EmbbaId == 0))
             {
                 result.Action = ActionType.Insert;
             }
             else
             {
                 result.Action = actiontype;
             }

            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {
					result.AddParameter("eventModuleId", eventModuleId, KxValueType.Int, false);
					result.AddParameter("blockId", blockId, KxValueType.Int, false);
					result.AddParameter("date", date, KxValueType.DateTime, false);
					result.AddParameter("allocation", allocation, KxValueType.Int, true);
					result.AddParameter("bedroomTypeId", bedroomTypeId, KxValueType.Int, false);
					result.AddParameter("status", status, KxValueType.String, 1, false);
					result.AddParameter("bookedAllocation", bookedAllocation, KxValueType.Int, true);
					result.AddParameter("notesId", notesId, KxValueType.Int, true);
					result.AddParameter("residentialRoomId", residentialRoomId, KxValueType.Int, false);
					result.AddParameter("residentialRoomAttributeId", residentialRoomAttributeId, KxValueType.Int, true);
            }
            return result;
        }


        public override  void LoadFromXml(string xml,XmlNode node)
        {
			dBIdentifier = KxXmlUtils.GetNodeValue(node, "embbaid", 0);
			eventModuleId = KxXmlUtils.GetNodeValue(node, "eventmoduleid", 0);
            blockId  = KxXmlUtils.GetNodeValue(node,"blockid",0);
            date  = KxXmlUtils.GetNodeValueDT(node,"date",null);
            allocation  = KxXmlUtils.GetNodeValue(node,"allocation",0);
            bedroomTypeId  = KxXmlUtils.GetNodeValue(node,"bedroomtypeid",0);
            status  = KxXmlUtils.GetNodeValue(node,"status","");
            bookedAllocation  = KxXmlUtils.GetNodeValue(node,"bookedallocation",0);
            notesId  = KxXmlUtils.GetNodeValue(node,"notesid",0);
            residentialRoomId  = KxXmlUtils.GetNodeValue(node,"residentialroomid",0);
            residentialRoomAttributeId  = KxXmlUtils.GetNodeValue(node,"residentialroomattributeid",0);
        }
    }
}
