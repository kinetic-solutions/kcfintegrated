using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;

namespace Kx.Objects.Common
{
    [Serializable]
    public class KxNote : KxBaseClass
    {
        #region Attributes & Properties

        protected DateTime ?dateTime;
        protected int      userId;
        protected string   notes;
        protected DateTime ?chaseDate;
        protected int      chaseUserId;
        protected DateTime ?chaseCompletedDate;
        protected int      chaseCompletedUserId;

        public int      NotesId
        {
            get
            { return base.dBIdentifier;}
        }

        public DateTime ?DateTime
        {
            get
            { return dateTime;}
            set
            { 
                AddToAudit("DateTime", dateTime, value);
                dateTime = value;
            }
        }

        public int      UserId
        {
            get
            { return userId;}
            set
            { 
                AddToAudit("UserId", userId, value);
                userId = value;
            }
        }

        public string   Notes
        {
            get
            { return notes;}
            set
            { 
                AddToAudit("Notes", notes, value);
                notes = value;
            }
        }

        public DateTime ?ChaseDate
        {
            get
            { return chaseDate;}
            set
            { 
                AddToAudit("ChaseDate", chaseDate, value);
                chaseDate = value;
            }
        }

        public int      ChaseUserId
        {
            get
            { return chaseUserId;}
            set
            { 
                AddToAudit("ChaseUserId", chaseUserId, value);
                chaseUserId = value;
            }
        }

        public DateTime ?ChaseCompletedDate
        {
            get
            { return chaseCompletedDate;}
            set
            { 
                AddToAudit("ChaseCompletedDate", chaseCompletedDate, value);
                chaseCompletedDate = value;
            }
        }

        public int      ChaseCompletedUserId
        {
            get
            { return chaseCompletedUserId;}
            set
            { 
                AddToAudit("ChaseCompletedUserId", chaseCompletedUserId, value);
                chaseCompletedUserId = value;
            }
        }


        #endregion 


        public KxNote(KxBaseClass parent):base(parent)
        {}


        public KxNote(KxBaseClass parent, XmlNode node):base(parent)
        {
             LoadFromXml("", node);
        }

        public KxNote(KxBaseClass parent,int      notesid):base(parent)
        {
             base.dBIdentifier = notesid;
             Load();
        }


        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("notes", "Notes", TargetType.Table);

            result.AutoKeyField = new KxDataValue("NotesId",NotesId,KxValueType.Int);


            if ((actiontype == ActionType.Update) && (NotesId == 0))
            {
                result.Action = ActionType.Insert;
            }
            else
            {
                result.Action = actiontype;
            }

            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {
                   result.AddParameter("userId", userId, KxValueType.Int, false);
                   result.AddParameter("notes", notes, KxValueType.String, 2147483647, false);
                   result.AddParameter("chaseDate", chaseDate, KxValueType.DateTime, true);
                   result.AddParameter("chaseUserId", chaseUserId, KxValueType.Int, true);
                   result.AddParameter("chaseCompletedDate", chaseCompletedDate, KxValueType.DateTime, true);
                   result.AddParameter("chaseCompletedUserId", chaseCompletedUserId, KxValueType.Int, true);
                   result.AddParameter("datetime", dateTime, KxValueType.DateTime);
            }
            return result;
        }


        public override  void LoadFromXml(string xml,XmlNode node)
        {
            dBIdentifier  = KxXmlUtils.GetNodeValue(node,"notesid",0);
            dateTime  = KxXmlUtils.GetNodeValueDT(node,"datetime",null);
            userId  = KxXmlUtils.GetNodeValue(node,"userid",0);
            notes  = KxXmlUtils.GetNodeValue(node,"notes","");
            chaseDate  = KxXmlUtils.GetNodeValueDT(node,"chasedate",null);
            chaseUserId  = KxXmlUtils.GetNodeValue(node,"chaseuserid",0);
            chaseCompletedDate  = KxXmlUtils.GetNodeValueDT(node,"chasecompleteddate",null);
            chaseCompletedUserId  = KxXmlUtils.GetNodeValue(node,"chasecompleteduserid",0);
        }
    }
}

