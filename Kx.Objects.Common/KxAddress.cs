using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;

namespace Kx.Objects.Common
{
    [Serializable]
    public class KxAddress : KxBaseClass
    {
        #region Attributes & Properties

        protected string   address;
        protected string   town;
        protected string   county;
        protected string   postcode;
        protected string   country;

        public int      AddressId
        {
            get
            { return base.dBIdentifier;}
        }

        public string   Address
        {
            get
            { return address;}
            set
            { 
                AddToAudit("Address", address, value);
                address = value;
            }
        }

        public string   Town
        {
            get
            { return town;}
            set
            { 
                AddToAudit("Town", town, value);
                town = value;
            }
        }

        public string   County
        {
            get
            { return county;}
            set
            { 
                AddToAudit("County", county, value);
                county = value;
            }
        }

        public string   Postcode
        {
            get
            { return postcode;}
            set
            { 
                AddToAudit("Postcode", postcode, value);
                postcode = value;
            }
        }

        public string   Country
        {
            get
            { return country;}
            set
            { 
                AddToAudit("Country", country, value);
                country = value;
            }
        }

        public string FormattedAddressSL
        {
            get
            { return GetFormattedAddressSL(); }
        }



        #endregion 

        public KxAddress(KxBaseClass parent,int      addressid):base(parent)
        {
             base.dBIdentifier = addressid;
             Load();
        }


        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("addresses", "Addresses", TargetType.Table);

            result.AutoKeyField = new KxDataValue("AddressId",AddressId,KxValueType.Int);


            if ((actiontype == ActionType.Update) && (AddressId == 0))
            {
                result.Action = ActionType.Insert;
            }
            else
            {
                result.Action = actiontype;
            }

            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {
                   result.AddParameter("address", address, KxValueType.String);
                   result.AddParameter("town", town, KxValueType.String);
                   result.AddParameter("county", county, KxValueType.String);
                   result.AddParameter("postcode", postcode, KxValueType.String);
                   result.AddParameter("country", country, KxValueType.String);
            }
            return result;
        }


        public override  void LoadFromXml(string xml,XmlNode node)
        {
            dBIdentifier  = KxXmlUtils.GetNodeValue(node,"addressid",0);
            address  = KxXmlUtils.GetNodeValue(node,"address","");
            town  = KxXmlUtils.GetNodeValue(node,"town","");
            county  = KxXmlUtils.GetNodeValue(node,"county","");
            postcode  = KxXmlUtils.GetNodeValue(node,"postcode","");
            country  = KxXmlUtils.GetNodeValue(node,"country","");
        }

        public string GetFormattedAddressSL()
        {
            string result = address + ", " + town + ", " + county + ", " + postcode;
            result = result.Replace(", , , , ", ", ");
            result = result.Replace(", , , ", ", ");
            result = result.Replace(", , ", ", ");
            result = result.TrimEnd(' ', ',');
            return result;
        }
    }
}

