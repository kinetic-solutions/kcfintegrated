using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;

namespace Kx.Objects.Common
{
    [Serializable]
    public class KxPaymentMethod : KxBaseClass
    {
        #region Attributes & Properties

        protected string   description;
        protected decimal  openingBalance;
        protected bool     allowMiscTransfers;
        protected int      nominalId;
        protected int      listOrder;
        protected bool     creditCard;
        protected bool     cheque;
        protected decimal  houseLimit;
        protected int      industryTypeDigit;
        protected int      cardNumberLength;
        protected string   shortCut;
        protected int      paymentType;
        protected int      creditCardGroupId;
        protected bool     allowPayments;
        protected bool     allowRefunds;
        protected string   webPaymentProviderDescription;

        public int      PaymentMethodId
        {
            get
            { return base.dBIdentifier;}
        }

        public string   Description
        {
            get
            { return description;}
            set
            { 
                AddToAudit("Description", description, value);
                description = value;
            }
        }

        public decimal  OpeningBalance
        {
            get
            { return openingBalance;}
            set
            { 
                AddToAudit("OpeningBalance", openingBalance, value);
                openingBalance = value;
            }
        }

        public bool     AllowMiscTransfers
        {
            get
            { return allowMiscTransfers;}
            set
            { 
                AddToAudit("AllowMiscTransfers", allowMiscTransfers, value);
                allowMiscTransfers = value;
            }
        }

        public int      NominalId
        {
            get
            { return nominalId;}
            set
            { 
                AddToAudit("NominalId", nominalId, value);
                nominalId = value;
            }
        }

        public int      ListOrder
        {
            get
            { return listOrder;}
            set
            { 
                AddToAudit("ListOrder", listOrder, value);
                listOrder = value;
            }
        }

        public bool     CreditCard
        {
            get
            { return creditCard;}
            set
            { 
                AddToAudit("CreditCard", creditCard, value);
                creditCard = value;
            }
        }

        public bool     Cheque
        {
            get
            { return cheque;}
            set
            { 
                AddToAudit("Cheque", cheque, value);
                cheque = value;
            }
        }

        public decimal  HouseLimit
        {
            get
            { return houseLimit;}
            set
            { 
                AddToAudit("HouseLimit", houseLimit, value);
                houseLimit = value;
            }
        }

        public int      IndustryTypeDigit
        {
            get
            { return industryTypeDigit;}
            set
            { 
                AddToAudit("IndustryTypeDigit", industryTypeDigit, value);
                industryTypeDigit = value;
            }
        }

        public int      CardNumberLength
        {
            get
            { return cardNumberLength;}
            set
            { 
                AddToAudit("CardNumberLength", cardNumberLength, value);
                cardNumberLength = value;
            }
        }

        public string   ShortCut
        {
            get
            { return shortCut;}
            set
            { 
                AddToAudit("ShortCut", shortCut, value);
                shortCut = value;
            }
        }

        public int      PaymentType
        {
            get
            { return paymentType;}
            set
            { 
                AddToAudit("PaymentType", paymentType, value);
                paymentType = value;
            }
        }

        public int      CreditCardGroupId
        {
            get
            { return creditCardGroupId;}
            set
            { 
                AddToAudit("CreditCardGroupId", creditCardGroupId, value);
                creditCardGroupId = value;
            }
        }

        public bool     AllowPayments
        {
            get
            { return allowPayments;}
            set
            { 
                AddToAudit("AllowPayments", allowPayments, value);
                allowPayments = value;
            }
        }

        public bool     AllowRefunds
        {
            get
            { return allowRefunds;}
            set
            { 
                AddToAudit("AllowRefunds", allowRefunds, value);
                allowRefunds = value;
            }
        }

        public string   WebPaymentProviderDescription
        {
            get
            { return webPaymentProviderDescription;}
            set
            { 
                AddToAudit("WebPaymentProviderDescription", webPaymentProviderDescription, value);
                webPaymentProviderDescription = value;
            }
        }


        #endregion 


        public KxPaymentMethod(KxBaseClass parent):base(parent)
        {}


        public KxPaymentMethod(KxBaseClass parent, XmlNode node):base(parent)
        {
             LoadFromXml("", node);
        }

        public KxPaymentMethod(KxBaseClass parent,int      paymentmethodid):base(parent)
        {
             base.dBIdentifier = paymentmethodid;
             Load();
        }


        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("paymentmethods", "PaymentMethods", TargetType.Table);

            result.AutoKeyField = new KxDataValue("PaymentMethodId",PaymentMethodId,KxValueType.Int);


            if ((actiontype == ActionType.Update) && (PaymentMethodId == 0))
            {
                result.Action = ActionType.Insert;
            }
            else
            {
                result.Action = actiontype;
            }

            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {
                result.AddParameter("description", description, KxValueType.String, false);
                   result.AddParameter("openingBalance", openingBalance, KxValueType.Decimal, false);
                   result.AddParameter("allowMiscTransfers", allowMiscTransfers, KxValueType.Boolean, false);
                   result.AddParameter("nominalId", nominalId, KxValueType.Int, false);
                   result.AddParameter("listOrder", listOrder, KxValueType.Int, true);
                   result.AddParameter("creditCard", creditCard, KxValueType.Boolean, false);
                   result.AddParameter("cheque", cheque, KxValueType.Boolean, false);
                   result.AddParameter("houseLimit", houseLimit, KxValueType.Decimal, false);
                   result.AddParameter("industryTypeDigit", industryTypeDigit, KxValueType.Int, true);
                   result.AddParameter("cardNumberLength", cardNumberLength, KxValueType.Int, true);
                   result.AddParameter("shortCut", shortCut, KxValueType.String, 1, true);
                   result.AddParameter("paymentType", paymentType, KxValueType.Int, true);
                   result.AddParameter("creditCardGroupId", creditCardGroupId, KxValueType.Int, true);
                   result.AddParameter("allowPayments", allowPayments, KxValueType.Boolean, false);
                   result.AddParameter("allowRefunds", allowRefunds, KxValueType.Boolean, false);
                   result.AddParameter("webPaymentProviderDescription", webPaymentProviderDescription, KxValueType.String, 255, true);
            }
            return result;
        }


        public override  void LoadFromXml(string xml,XmlNode node)
        {
            dBIdentifier  = KxXmlUtils.GetNodeValue(node,"paymentmethodid",0);
            description  = KxXmlUtils.GetNodeValue(node,"description","");
            openingBalance  = KxXmlUtils.GetNodeValue(node,"openingbalance",(decimal)0.00M);
            allowMiscTransfers  = KxXmlUtils.GetNodeValue(node,"allowmisctransfers",false);
            nominalId  = KxXmlUtils.GetNodeValue(node,"nominalid",0);
            listOrder  = KxXmlUtils.GetNodeValue(node,"listorder",0);
            creditCard  = KxXmlUtils.GetNodeValue(node,"creditcard",false);
            cheque  = KxXmlUtils.GetNodeValue(node,"cheque",false);
            houseLimit  = KxXmlUtils.GetNodeValue(node,"houselimit",(decimal)0.00M);
            industryTypeDigit  = KxXmlUtils.GetNodeValue(node,"industrytypedigit",0);
            cardNumberLength  = KxXmlUtils.GetNodeValue(node,"cardnumberlength",0);
            shortCut  = KxXmlUtils.GetNodeValue(node,"shortcut","");
            paymentType  = KxXmlUtils.GetNodeValue(node,"paymenttype",0);
            creditCardGroupId  = KxXmlUtils.GetNodeValue(node,"creditcardgroupid",0);
            allowPayments  = KxXmlUtils.GetNodeValue(node,"allowpayments",false);
            allowRefunds  = KxXmlUtils.GetNodeValue(node,"allowrefunds",false);
            webPaymentProviderDescription  = KxXmlUtils.GetNodeValue(node,"webpaymentproviderdescription","");
        }

        public override string ToString()
        {
            return description;
        }
    }
}

