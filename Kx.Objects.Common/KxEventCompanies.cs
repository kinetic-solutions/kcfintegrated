using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Exceptions;
using Kx.Core.Utility;

namespace Kx.Objects.Common
{
    public class KxEventCompanies : KxBaseClass
    {
        private List<KxEventCompany> items = new List<KxEventCompany>();

        public List<KxEventCompany> Items
        {
            get
            { return items; }
        }

        public KxEventCompany this[int index]
        {
            get
            { return items[index]; }
            set
            { items[index] = value; }
        }

        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = null;

            if (actiontype == ActionType.Select)
            {
				result = new KxData("eventcompanies", "EventCompanies", TargetType.Table, ActionType.Select);

                //result.Parameters.Add(new KxDataValue("keyfield", keyfield, KxValueType.Int));
            }
            return result;
        }

        public override bool LoadFromXml(string xml, string xpath)
        {
            items.Clear();
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xml);

                XmlNodeList nodes = doc.SelectNodes(@"//results/" + xpath + "/datarow");

                foreach (XmlNode node in nodes)
                {
                    if (node != null)
                    {
                        KxEventCompany item = new KxEventCompany(this, node);
                        items.Add(item);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "LoadFromXml", ex.Message, xpath);
                LastError = ex.Message;
                return false;
            }
        }

        public KxEventCompany GetEventCompanies(int id)
        {
            KxEventCompany result = null;

            foreach (KxEventCompany i in Items)
            {
                if (i.EventCompanyId == id)
                {
                    result = i;
                    break;
                }
            }
            return result;
        }

    }
}

