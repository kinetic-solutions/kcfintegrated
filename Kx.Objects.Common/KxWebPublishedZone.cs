using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;

namespace Kx.Objects.Common
{
    [Serializable]
    public abstract class KxWebPublishedZone : KxBaseClass
    {
        public enum KxWebPublishedZoneTypes
        {
            Unknown,
            KxHotel
        }

        #region Attributes & Properties

        protected string   _description;
        protected KxWebPublishedZoneTypes   _zoneType;
        protected string   _zoneConfiguration;

        public int      WebPublishedZoneId
        {
            get
            { return base.dBIdentifier;}
        }

        public string   Description
        {
            get
            { return _description;}
            set
            { 
                AddToAudit("Description", _description, value);
                _description = value;
            }
        }

        /// <summary>
        /// Deprecated...
        /// </summary>
        ///
        public int PublishedLevel { get; protected set; }

        public string ZoneConfiguration
        {
            get
            {
                return _zoneConfiguration;
            }
            set
            {
                AddToAudit("ZoneConfiguration", _zoneConfiguration, value);
            	_zoneConfiguration = value;
            }
        }

        public  KxWebPublishedZoneTypes  ZoneType
        {
            get
            { return _zoneType;}
            set
            { 
                AddToAudit("ZoneType", _zoneType.ToString (), value.ToString());
                _zoneType = value;
            }
        }

        #endregion 

        #region Constructors

        protected KxWebPublishedZone(KxBaseClass parent,int      webpublishedzoneid):base(parent)
        {
             base.dBIdentifier = webpublishedzoneid;
             Load();
        }


        protected KxWebPublishedZone(KxBaseClass parent, XmlNode node):base(parent)
        {
             LoadFromXml("", node);
        }

        protected KxWebPublishedZone(KxBaseClass parent):base(parent)
        {}
        #endregion

        #region Static Methods
        public static List<KxWebPublishedZone> GetWebPublishedZones (KxWebPublishedZoneTypes? zoneType)
        {
            List<KxWebPublishedZone> result = new List<KxWebPublishedZone>();

            const string xpath = "webpublishedzone";

            KxData data = new KxData(xpath, "WebPublishedZones", TargetType.Table, ActionType.Select);

            if (data.Execute ())
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(string.Format("<results>{0}</results>", data.XmlResults));

                XmlNodeList nodes = doc.SelectNodes(String.Format(@"//results/{0}/datarow", xpath));
                foreach (XmlNode node in nodes)
                {
                    if (node != null)
                    {
                        KxWebPublishedZoneTypes zt = KxWebPublishedZoneTypes.Unknown;
                        try
                        {
                            zt = (KxWebPublishedZoneTypes)Enum.Parse(typeof(KxWebPublishedZoneTypes), KxXmlUtils.GetNodeValue(node, "zonetype", "Unknown"));
                        }
                        catch (Exception)
                        {
                            zt = KxWebPublishedZoneTypes.Unknown;
                        }

                        KxWebPublishedZone item = null;

                        switch (zt)
                        {
                            case KxWebPublishedZoneTypes.KxHotel:
                                item = new KxHotelWebPublishedZone(null, node);
                                break;
                            default:
                                item = new KxUnknownWebPublishedZone(null, node);
                                break;
                        }

                        if (!zoneType.HasValue || item.ZoneType == zoneType)
                        {
                            result.Add(item);
                        }
                    }
                }
            }
            return result;
        }
        #endregion

        #region Public Methods
        public override  void LoadFromXml(string xml,XmlNode node)
        {
            dBIdentifier  = KxXmlUtils.GetNodeValue(node,"webpublishedzoneid",0);
            _description  = KxXmlUtils.GetNodeValue(node,"description","");
            PublishedLevel  = KxXmlUtils.GetNodeValue(node,"publishedlevel",0);

            // decode the string into the enum...
            try
            {
                _zoneType = (KxWebPublishedZoneTypes)Enum.Parse(typeof(KxWebPublishedZoneTypes), KxXmlUtils.GetNodeValue(node, "zonetype", "Unknown"));
            }
            catch
            {
                _zoneType = KxWebPublishedZoneTypes.Unknown;
            }
            _zoneConfiguration  = KxXmlUtils.GetNodeValue(node,"zoneconfiguration","");
        }

        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("webpublishedzones", "WebPublishedZones", TargetType.Table);

            result.AutoKeyField = new KxDataValue("WebPublishedZoneId",WebPublishedZoneId,KxValueType.Int);


            if ((actiontype == ActionType.Update) && (WebPublishedZoneId == 0))
            {
                result.Action = ActionType.Insert;
            }
            else
            {
                result.Action = actiontype;
            }

            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {
                result.AddParameter("description", _description, KxValueType.String, 50, false);
                   result.AddParameter("publishedLevel", PublishedLevel, KxValueType.Int, false);
                   result.AddParameter("zoneType", _zoneType.ToString(), KxValueType.String, 50, true);
                   result.AddParameter("zoneConfiguration", _zoneConfiguration, KxValueType.String);
            }
            return result;
        }

        public override string ToString()
        {
            return _description;
        }
        #endregion

    }
}

