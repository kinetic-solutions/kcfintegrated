using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Exceptions;
using Kx.Core.Utility;

namespace Kx.Objects.Common
{
    public class KxBedroomTypes : KxBaseClass
    {
        private List<KxBedroomType> items = new List<KxBedroomType>();
        private bool UseStoredProc = false;

        public List<KxBedroomType> Items
        {
            get
            { return items; }
        }

        public KxBedroomType this[int index]
        {
            get
            { return items[index]; }
            set
            { items[index] = value; }
        }

        public int[] BedroomTypeArray
        {
            get
            { return GetBedroomTypeArray(); }
        }

        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = null;

            if (actiontype == ActionType.Select)
            {
                if (!UseStoredProc)
                {
                    result = new KxData("bedroomtypes", "bedroomtypes", TargetType.Table, ActionType.Select);
                    result.AddOrderField("description", KxDataOrderType.Ascending);
                }
                else
                {
                    result = new KxData("bedroomtypes", "bedroomtypes", TargetType.StoredProc, ActionType.Select);
                }
            }
            return result;
        }

        public KxBedroomTypes(KxBaseClass parent)
            : base(parent)
        {
            Load();
        }

        public KxBedroomTypes(KxBaseClass parent, bool useStoredProc)
            : base(parent)
        {
            UseStoredProc = useStoredProc;
            Load();
        }

        public override bool LoadFromXml(string xml, string xpath)
        {
            items.Clear();
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xml);

                XmlNodeList nodes = doc.SelectNodes(@"//results/" + xpath + "/datarow");

                foreach (XmlNode node in nodes)
                {
                    if (node != null)
                    {
                        KxBedroomType bedtype = new KxBedroomType(this, node);
                        Items.Add(bedtype);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "LoadFromXml", ex.Message, xpath);
                LastError = ex.Message;
                return false;
            }
        }

        protected int[] GetBedroomTypeArray()
        {
            int loop = 0;

            int[] result = new int[Items.Count];

            foreach (KxBedroomType bedtype in Items)
            {
                result[loop] = bedtype.BedroomTypeId;
                loop++;
            }
            return result;
        }

        public KxBedroomType GetBedroomTypeById(int id)
        {
            KxBedroomType result = null;

            foreach (KxBedroomType bedtype in Items)
            {
                if (bedtype.BedroomTypeId == id)
                {
                    result = bedtype;
                    break;
                }
            }

            return result;
        }

		public KxBedroomType GetBedroomTypeByExternalCode(string externalcode)
		{
			KxBedroomType result = null;

			foreach (KxBedroomType bedtype in Items)
			{
				if (bedtype.ExternalCode == externalcode)
				{
					result = bedtype;
					break;
				}
			}

			return result;
		}

        public List<string> GetWebDescriptions()
        {
            List<string> result = new List<string>();

            foreach (KxBedroomType type in Items)
            {
                if (!String.IsNullOrEmpty(type.WebDescription) && result.IndexOf(type.WebDescription) < 0)
                {
                    result.Add(type.WebDescription);
                }
            }

            return result;
        }

        public string[] GetWebDescriptionsArray()
        {
            return GetWebDescriptions().ToArray();
        }

        public List<int> GetWebIds()
        {
            List<int> result = new List<int>();

            foreach (string str in GetWebDescriptions())
            {
                KxBedroomType type = GetBedroomTypeByWebDescription(str);
                if (type != null)
                {
                    result.Add(type.BedroomTypeId);
                }
            }
            return result;
        }

        public KxBedroomType GetBedroomTypeByWebDescription(string Description)
        {
            KxBedroomType result = null;

            foreach (KxBedroomType type in Items)
            {
                if (type.WebDescription == Description)
                {
                    result = type;
                    break;
                }
            }

            return result;
        }


    }
}
