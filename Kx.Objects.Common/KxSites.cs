using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Exceptions;
using Kx.Core.Utility;

namespace Kx.Objects.Common
{

    public class KxSites : KxBaseClass
    {
        private List<KxSite> items = new List<KxSite>();

        public List<KxSite> Items
        {
            get
            { return items; }
        }

        public KxSite this[int index]
        {
            get
            { return items[index]; }
            set
            { items[index] = value; }
        }

        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = null;

            if (actiontype == ActionType.Select)
            {
                result = new KxData("sites", "sites", TargetType.Table, ActionType.Select);

                result.OrderFields.Add(new KxDataOrder("name"));
                
            }
            return result;
        }

        public int[] SiteIdArray
        {
            get
            { return GetSiteIdArray(); }
        }



        public KxSites(KxBaseClass parent) : base(parent)
        {

        }


        public override bool LoadFromXml(string xml, string xpath)
        {
            items.Clear();
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xml);

                XmlNodeList nodes = doc.SelectNodes(@"//results/" + xpath + "/datarow");

                foreach (XmlNode node in nodes)
                {
                    if (node != null)
                    {
                        KxSite site = new KxSite(this, node);
                        Items.Add(site);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "LoadFromXml", ex.Message, xpath);
                LastError = ex.Message;
                return false;
            }
        }

        protected int[] GetSiteIdArray()
        {
            int loop = 0;

            int[] result = new int[Items.Count];

            foreach (KxSite site in Items)
            {
                result[loop] = site.SiteId;
                loop++;
            }
            return result;
        }

        public KxSite GetSiteById(int id)
        {
            KxSite result = null;

            foreach (KxSite site in Items)
            {
                if (site.SiteId == id)
                {
                    result = site;
                    break;
                }
            }

            return result;
        }
    }
}
