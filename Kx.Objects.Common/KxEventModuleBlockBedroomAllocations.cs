using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;


namespace Kx.Objects.Common
{
    [Serializable]
    public class KxEventModuleBlockBedroomAllocations : KxBaseClass
    {
        #region Attributes & Properties

        protected List<KxEventModuleBlockBedroomAllocation> items = new List<KxEventModuleBlockBedroomAllocation>();
        protected int eventModuleId;

        public List<KxEventModuleBlockBedroomAllocation> Items
        {
            get
            { return items; }
        }

        public KxEventModuleBlockBedroomAllocation this[int index]
        {
            get
            { return items[index]; }
            set
            { items[index] = value; }
        }

		public IEnumerator<KxEventModuleBlockBedroomAllocation> GetEnumerator()
		{
			return items.GetEnumerator();
		}
		
        #endregion

        public KxEventModuleBlockBedroomAllocations(KxBaseClass parent, int EventModuleId)
            : base(parent)
        {
            eventModuleId = EventModuleId;
            Load();
        }

        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = null;

            if (actiontype == ActionType.Select)
            {
                result = new KxData("eventmoduleblockbedroomallocation", "EventModuleBlockBedroomAllocation", TargetType.Table);
                result.KeyFields.Add(new KxDataValue("EventModuleID", eventModuleId, KxValueType.String));
            }
            return result;
        }

        public override bool LoadFromXml(string xml, string xpath)
        {
            items.Clear();
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xml);

                XmlNodeList nodes = doc.SelectNodes(@"//results/" + xpath + "/datarow");

                foreach (XmlNode node in nodes)
                {
                    if (node != null)
                    {
                        KxEventModuleBlockBedroomAllocation eventModuleBlockBedroomAllocation = new KxEventModuleBlockBedroomAllocation(this, node);
                        items.Add(eventModuleBlockBedroomAllocation);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "LoadFromXml", ex.Message, xpath);
                LastError = ex.Message;
                return false;
            }
        }
        
        public KxEventModuleBlockBedroomAllocation GetEventModuleBlockBedroomAllocationforDate(DateTime eventdate)
        {
            KxEventModuleBlockBedroomAllocation result = null;

            foreach (KxEventModuleBlockBedroomAllocation item in items)
            {
                if (item.Date == eventdate)
                {
                    result = item;
                    break;
                }
            }
            return result;
        }

		public KxEventModuleBlockBedroomAllocation GetEventModuleBlockBedroomAllocationBlockandType(DateTime eventdate, int blockId, int bedroomTypeId)
		{
			KxEventModuleBlockBedroomAllocation result = null;

			foreach (KxEventModuleBlockBedroomAllocation item in items)
			{
				if ((item.Date == eventdate) & (item.BlockId == blockId) & (item.BedroomTypeId == bedroomTypeId))
				{
					result = item;
					break;
				}
			}
			return result;
		}

        
    }
}
