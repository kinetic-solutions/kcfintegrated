using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Exceptions;
using Kx.Core.Utility;

namespace Kx.Objects.Common
{

    public class KxAreas : KxBaseClass
    {

        int _siteId;

        private List<KxArea> items = new List<KxArea>();

        public List<KxArea> Items
        {
            get
            { return items; }
        }

        public KxArea this[int index]
        {
            get
            { return items[index]; }
            set
            { items[index] = value; }
        }

        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = null;

            if (actiontype == ActionType.Select)
            {
                result = new KxData("areas", "areas", TargetType.Table, ActionType.Select);

                if (_siteId > 0)
                {
                    result.AddKeyField("siteId", _siteId, KxValueType.Int);
                }

                result.OrderFields.Add(new KxDataOrder("Description"));
                
            }
            return result;
        }

        public KxAreas(KxBaseClass parent) : base(parent)
        {
        }

        public KxAreas(KxBaseClass parent, int SiteId)
            : base(parent)
        {
            _siteId = SiteId;
            Load();
        }

        public override bool LoadFromXml(string xml, string xpath)
        {
            items.Clear();
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xml);

                XmlNodeList nodes = doc.SelectNodes(@"//results/" + xpath + "/datarow");

                foreach (XmlNode node in nodes)
                {
                    if (node != null)
                    {
                        KxArea area = new KxArea(this, node);
                        Items.Add(area);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "LoadFromXml", ex.Message, xpath);
                LastError = ex.Message;
                return false;
            }
        }


        public KxArea GetAreaById(int id)
        {
            KxArea result = null;

            foreach (KxArea area in Items)
            {
                if (area.AreaId == id)
                {
                    result = area;
                    break;
                }
            }

            if (result == null)
            {
                KxArea dummy = new KxArea(this, id);
                if (dummy.AreaId == id)
                {
                    items.Add(dummy);
                    result = dummy;
                }
            }

            return result;
        }
    }
}
