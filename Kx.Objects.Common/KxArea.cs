using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;

namespace Kx.Objects.Common
{
    [Serializable]
    public class KxArea : KxBaseClass
    {
        #region Attributes & Properties

        protected string   description;
        protected int      siteId;
        protected string   externalCode;
        protected int      displayOrder;
        protected int      distribAdHocUserId;
        protected int      userGroupId;
        protected string areaUrl = "";

        public int      AreaId
        {
            get
            { return base.dBIdentifier;}
        }

        public string   Description
        {
            get
            { return description;}
            set
            { 
                AddToAudit("Description", description, value);
                description = value;
            }
        }

        public int      SiteId
        {
            get
            { return siteId;}
            set
            { 
                AddToAudit("SiteId", siteId, value);
                siteId = value;
            }
        }

        public string   ExternalCode
        {
            get
            { return externalCode;}
            set
            { 
                AddToAudit("ExternalCode", externalCode, value);
                externalCode = value;
            }
        }

        public int      DisplayOrder
        {
            get
            { return displayOrder;}
            set
            { 
                AddToAudit("DisplayOrder", displayOrder, value);
                displayOrder = value;
            }
        }

        public int      DistribAdHocUserId
        {
            get
            { return distribAdHocUserId;}
            set
            { 
                AddToAudit("DistribAdHocUserId", distribAdHocUserId, value);
                distribAdHocUserId = value;
            }
        }

        public int      UserGroupId
        {
            get
            { return userGroupId;}
            set
            { 
                AddToAudit("UserGroupId", userGroupId, value);
                userGroupId = value;
            }
        }

        public string AreaUrl
        {
            get
            {
                if (areaUrl == "")
                {
                    areaUrl = GetAreaUrl();
                }
                return areaUrl;
            }
            set
            {
                areaUrl = value;
            }
        }

        #endregion 

        public KxArea(KxBaseClass parent,int      areaid):base(parent)
        {
             base.dBIdentifier = areaid;
             Load();
        }

            public KxArea(KxBaseClass parent, XmlNode node) : base(parent)
        {
            LoadFromXml("",node);
        }


        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("areas", "Areas", TargetType.Table);

            result.AutoKeyField = new KxDataValue("AreaId",AreaId,KxValueType.Int);


            if ((actiontype == ActionType.Update) && (AreaId == 0))
            {
                result.Action = ActionType.Insert;
            }
            else
            {
                result.Action = actiontype;
            }

            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {
                   result.AddParameter("description", description, KxValueType.String);
                   result.AddParameter("siteId", siteId, KxValueType.Int);
                   result.AddParameter("externalCode", externalCode, KxValueType.String);
                   result.AddParameter("displayOrder", displayOrder, KxValueType.Int);
                   result.AddParameter("distribAdHocUserId", distribAdHocUserId, KxValueType.Int);
                   result.AddParameter("userGroupId", userGroupId, KxValueType.Int);
            }
            return result;
        }


        public override  void LoadFromXml(string xml,XmlNode node)
        {
            dBIdentifier  = KxXmlUtils.GetNodeValue(node,"areaid",0);
            description  = KxXmlUtils.GetNodeValue(node,"description","");
            siteId  = KxXmlUtils.GetNodeValue(node,"siteid",0);
            externalCode  = KxXmlUtils.GetNodeValue(node,"externalcode","");
            displayOrder  = KxXmlUtils.GetNodeValue(node,"displayorder",0);
            distribAdHocUserId  = KxXmlUtils.GetNodeValue(node,"distribadhocuserid",0);
            userGroupId  = KxXmlUtils.GetNodeValue(node,"usergroupid",0);
        }


        protected string GetAreaUrl()
        {
            string result = "";

            if (dBIdentifier != 0)
            {
                try
                {
                    KxData data = new KxData("", "Select string1 from customsettings where CustomSetting = 'Kx.Web.Area.URL' and Value = '" + dBIdentifier.ToString() + "'", TargetType.Static);
                    object obj = data.ExecuteScalar();

                    if (obj != null)
                    {
                        try
                        {
                            result = obj.ToString();
                        }
                        catch
                        { }
                    }
                }
                catch { }
            }
            return result;
        }
    }
}

