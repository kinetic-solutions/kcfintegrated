/* BEGIN C:\TFS\KCF-STUDENT\Branches\Trunk\KCF.Student\Database\1. Schema.sql */

/* BEGIN C:\TFS\KCF-STUDENT\Branches\Trunk\KCF.Student\Database\Schemas\KCF.sql */

IF NOT EXISTS (SELECT null FROM sys.schemas WHERE name = 'kcf')
BEGIN
	DECLARE @SchemaSQL nvarchar(500)

	SET @SchemaSQL = 'CREATE SCHEMA kcf AUTHORIZATION [dbo]'
	EXEC sp_sqlexec @SchemaSQL
END
ELSE
BEGIN
	PRINT 'Schema KCF already exists'
END;

/* END C:\TFS\KCF-STUDENT\Branches\Trunk\KCF.Student\Database\Schemas\KCF.sql */
/* BEGIN C:\TFS\KCF-STUDENT\Branches\Trunk\KCF.Student\Database\Schemas\KCFSTATS.sql */

IF NOT EXISTS (SELECT null FROM sys.schemas WHERE name = 'kcfstats')
BEGIN
	DECLARE @SchemaSQL1 nvarchar(500)

	SET @SchemaSQL1 = 'CREATE SCHEMA kcfstats'
	EXEC sp_sqlexec @SchemaSQL1
END
ELSE
BEGIN
	PRINT 'Schema KCFSTATS already exists'
END;

/* END C:\TFS\KCF-STUDENT\Branches\Trunk\KCF.Student\Database\Schemas\KCFSTATS.sql */

/* END C:\TFS\KCF-STUDENT\Branches\Trunk\KCF.Student\Database\1. Schema.sql */
/* BEGIN C:\TFS\KCF-STUDENT\Branches\Trunk\KCF.Student\Database\2. Tables.sql */

/* BEGIN C:\TFS\KCF-STUDENT\Branches\Trunk\KCF.Student\Database\Tables\kcf.ApplicationWindow.sql */

IF NOT EXISTS (SELECT NULL FROM sys.tables WHERE object_id = OBJECT_ID(N'kcf.ApplicationWindow'))
BEGIN
	CREATE TABLE kcf.ApplicationWindow
	(
		ApplicationWindowID	int NOT NULL IDENTITY(1, 1) PRIMARY KEY,
		AcademicYearID		int NOT NULL,
		ApplicationTypeID	int NOT NULL,
		ApplicationsOpen	datetime NOT NULL,
		ApplicationsClose	datetime NOT NULL,
		ClosedMessage		varchar(max) NULL
	)
END
ELSE
BEGIN
	PRINT 'Table kcf.ApplicationWindow already exists'
END
GO

IF NOT EXISTS (SELECT NULL FROM sys.indexes WHERE object_id = OBJECT_ID(N'kcf.ApplicationWindow') AND name = 'U_AcademicYearID_ApplicationTypeID')
BEGIN
	CREATE UNIQUE INDEX U_AcademicYearID_ApplicationTypeID ON kcf.ApplicationWindow
	(
		AcademicYearID ASC,
		ApplicationTypeID	ASC
	)
END
ELSE
BEGIN
	PRINT 'Index kcf.ApplicationWindow.U_AcademicYearID_ApplicationTypeID already exists'
END
GO

/* END C:\TFS\KCF-STUDENT\Branches\Trunk\KCF.Student\Database\Tables\kcf.ApplicationWindow.sql */
/* BEGIN C:\TFS\KCF-STUDENT\Branches\Trunk\KCF.Student\Database\Tables\kcf.MergeTagSnapshot.sql */

IF NOT EXISTS (SELECT NULL FROM sys.tables WHERE object_id = OBJECT_ID(N'kcf.MergeTagSnapshot'))
BEGIN
	CREATE TABLE kcf.MergeTagSnapshot
	(
		SnapshotID			int	NOT NULL IDENTITY(1, 1) PRIMARY KEY,
		MergeTagSet			varchar(20) NOT NULL,
		AllocationID		int NOT NULL DEFAULT(0),
		KxStudentID			int NOT NULL DEFAULT(0),
		CreationDate		datetime NOT NULL DEFAULT(getdate()),
		MergeTag			varchar(255) NOT NULL,
		MergeData			nvarchar(max) NOT NULL
	)
END
ELSE
BEGIN
	PRINT 'Table kcf.MergeTagSnapshot already exists'
END;

IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'kcf.MergeTagSnapshot') AND name = 'IX_UQ_MergeTagSet_AllocationID_KxStudentID')
BEGIN
	DROP INDEX IX_UQ_MergeTagSet_AllocationID_KxStudentID ON kcf.MergeTagSnapshot
END;

IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'kcf.MergeTagSnapshot') AND name = 'IX_UQ_MergeTagSet_AllocationID_KxStudentID_MergeTag')
BEGIN
	CREATE UNIQUE INDEX IX_UQ_MergeTagSet_AllocationID_KxStudentID_MergeTag ON kcf.MergeTagSnapshot
	(
		MergeTagSet,
		AllocationID,
		KxStudentID,
		MergeTag
	)
END
ELSE
BEGIN
	PRINT 'Index IX_UQ_MergeTagSet_AllocationID_KxStudentID ON kcf.MergeTagSnapshot already exists'
END;

IF (SELECT max_length from sys.columns WHERE object_id = OBJECT_ID(N'kcf.MergeTagSnapshot') AND name = 'MergeTagSet') < 50
BEGIN
	ALTER TABLE kcf.MergeTagSnapshot
		ALTER COLUMN MergeTagSet varchar(50) NOT NULL
END
ELSE
BEGIN
	PRINT 'Column length for kcf.MergeTagSnapshot.MergeTagSet is already 50'
END;

/* END C:\TFS\KCF-STUDENT\Branches\Trunk\KCF.Student\Database\Tables\kcf.MergeTagSnapshot.sql */
/* BEGIN C:\TFS\KCF-STUDENT\Branches\Trunk\KCF.Student\Database\Tables\kcf.OneTimeUseCodes.sql */

IF NOT EXISTS (SELECT NULL FROM sys.tables WHERE object_id = OBJECT_ID(N'kcf.OneTimeUseCodes'))
BEGIN
	CREATE TABLE kcf.OneTimeUseCodes
	(
		OneTimeUseCode			varchar(20) NOT NULL PRIMARY KEY,
		CreationDate			datetime NOT NULL default(getdate()),
		CreatedBy				varchar(50) NOT NULL,
		UsedOn					datetime NULL,
		UsedByApplicationGUID	uniqueidentifier NULL,
		UsedByUserGUID			uniqueidentifier NULL,
		UsedByAdditional		varchar(max) NULL
	)
END
ELSE
BEGIN
	PRINT 'Table kcf.OneTimeUseCodes already exists'
END;

IF EXISTS (SELECT NULL FROM sys.columns WHERE object_id = OBJECT_ID(N'kcf.OneTimeUseCodes') AND name = 'Issued')
BEGIN
	ALTER TABLE kcf.OneTimeUseCodes
	DROP COLUMN Issued
END
ELSE
BEGIN
	PRINT 'Column kcf.OneTimeUseCodes.Issued removed'
END;

IF NOT EXISTS (SELECT NULL FROM sys.columns WHERE object_id = OBJECT_ID(N'kcf.OneTimeUseCodes') AND name = 'IssuedTo')
BEGIN
	ALTER TABLE kcf.OneTimeUseCodes
	ADD IssuedTo varchar(50) NULL
END
ELSE
BEGIN
	PRINT 'Column kcf.OneTimeUseCodes.IssuedTo already exists'
END;

/* END C:\TFS\KCF-STUDENT\Branches\Trunk\KCF.Student\Database\Tables\kcf.OneTimeUseCodes.sql */
/* BEGIN C:\TFS\KCF-STUDENT\Branches\Trunk\KCF.Student\Database\Tables\kcf.PropertyDetails.sql */

IF NOT EXISTS (SELECT NULL FROM sys.tables WHERE object_id = OBJECT_ID(N'[kcf].[PropertyDetails]'))
BEGIN
	CREATE TABLE [kcf].[PropertyDetails]
	(
		PropertyDetailID int NOT NULL IDENTITY(1, 1) PRIMARY KEY,
		[SiteId] [int] NOT NULL,
		[AreaId] [int] NOT NULL,
		[BlockId] [int] NOT NULL,
		[Region] [varchar](50) NULL,
		[CityName] [varchar](50) NULL,
		[PropertyName] [varchar](50) NULL,
		[PropertyAddress] [varchar](1000) NULL,
		[PropertyTelephone] [varchar](50) NULL,
		[PropertyEmail] [varchar](255) NULL,
		[PropertyLandlord] [varchar](1000) NULL,
		[PropertyContractResource] varchar(255) NULL,
		[PropertyTCResource] varchar(255) NULL,
		[Additional1] varchar(255) NULL,
		[Additional2] varchar(255) NULL,
		[Additional3] varchar(255) NULL,
		[Additional4] varchar(255) NULL,
		[Additional5] varchar(255) NULL,
		[Additional6] varchar(255) NULL,
		[Additional7] varchar(255) NULL,
		[Additional8] varchar(255) NULL
	)
END
ELSE
BEGIN
	PRINT 'Table [kcf].[PropertyDetails] already exists'
END
GO

IF NOT EXISTS (SELECT NULL FROM sys.columns WHERE object_id = OBJECT_ID(N'[kcf].[PropertyDetails]') AND name = 'Additional6')
BEGIN
	ALTER TABLE [kcf].[PropertyDetails]
	ADD [Additional6] varchar(255) NULL
END
ELSE
BEGIN
	PRINT 'Column kcf.PropertyDetails.Additional6 already exists'
END
GO

IF NOT EXISTS (SELECT NULL FROM sys.columns WHERE object_id = OBJECT_ID(N'[kcf].[PropertyDetails]') AND name = 'Additional7')
BEGIN
	ALTER TABLE [kcf].[PropertyDetails]
	ADD [Additional7] varchar(255) NULL
END
ELSE
BEGIN
	PRINT 'Column kcf.PropertyDetails.Additional7 already exists'
END
GO

IF NOT EXISTS (SELECT NULL FROM sys.columns WHERE object_id = OBJECT_ID(N'[kcf].[PropertyDetails]') AND name = 'Additional8')
BEGIN
	ALTER TABLE [kcf].[PropertyDetails]
	ADD [Additional8] varchar(255) NULL
END
ELSE
BEGIN
	PRINT 'Column kcf.PropertyDetails.Additional8 already exists'
END
GO

IF NOT EXISTS (SELECT NULL FROM sys.indexes WHERE object_id = OBJECT_ID(N'[kcf].[PropertyDetails]') AND name = 'U_BySiteAreaBlock')
BEGIN
	CREATE UNIQUE INDEX U_BySiteAreaBlock ON [kcf].[PropertyDetails]
	(
		SiteID,
		AreaID,
		BlockID
	)
END
ELSE
BEGIN
	PRINT 'Index kcf.PropertyDetails.U_BySiteAreaBlock already exists'
END
GO

/* END C:\TFS\KCF-STUDENT\Branches\Trunk\KCF.Student\Database\Tables\kcf.PropertyDetails.sql */
/* BEGIN C:\TFS\KCF-STUDENT\Branches\Trunk\KCF.Student\Database\Tables\KCFWebStats.sql */

IF NOT EXISTS (SELECT * FROM sys.tables WHERE object_id = OBJECT_ID(N'[dbo].[KCFWebStats]', N'U'))
BEGIN
	CREATE TABLE [dbo].[KCFWebStats]
	(
		StatID		int IDENTITY(1, 1) PRIMARY KEY,
		Activity	varchar(255) NOT NULL,
		TrackingID	varchar(255) NULL,
		TrackingTimestamp datetime NOT NULL DEFAULT(getdate()),
		StatData	xml NULL
	)
END
ELSE
BEGIN
	PRINT 'Table dbo.KCFWebStats already exists'
END;

/* END C:\TFS\KCF-STUDENT\Branches\Trunk\KCF.Student\Database\Tables\KCFWebStats.sql */
/* BEGIN C:\TFS\KCF-STUDENT\Branches\Trunk\KCF.Student\Database\Tables\LocationContent.sql */


IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LocationContent]') AND type in (N'U'))
BEGIN
	CREATE TABLE dbo.LocationContent
	(
		LocationContentID int NOT NULL IDENTITY PRIMARY KEY,
		AreaID	int NULL,
		BedroomTypeID	int NULL,
		Features varchar(500),
		Images varchar(max),
		Facilities varchar(max),
		MoreInfo varchar(max)
	)
END
ELSE
BEGIN
	PRINT 'Table LocationContent already exists'
END
;

IF NOT EXISTS (SELECT * FROM sys.columns c WHERE c.object_id = OBJECT_ID(N'[dbo].[LocationContent]') AND c.name = 'BlockID')
BEGIN
	ALTER TABLE dbo.LocationContent
	ADD
		BlockID	int	NULL
END
ELSE
BEGIN
	PRINT 'Column BlockID already exists on LocationContent'
END
;

IF EXISTS (SELECT * FROM sys.indexes i WHERE i.object_id = OBJECT_ID(N'[dbo].[LocationContent]') AND i.name = 'ByAreaIDBedroomTypeID')
BEGIN
	DROP INDEX ByAreaIDBedroomTypeID ON dbo.LocationContent
END
;



IF NOT EXISTS (SELECT * FROM sys.columns c WHERE c.object_id = OBJECT_ID(N'[dbo].[LocationContent]') AND c.name = 'SiteID')
BEGIN
	ALTER TABLE dbo.LocationContent
	ADD
		SiteID	int	NULL
END
ELSE
BEGIN
	PRINT 'Column SiteID already exists on LocationContent'
END
;

IF NOT EXISTS (SELECT * FROM sys.indexes i WHERE i.object_id = OBJECT_ID(N'[dbo].[LocationContent]') AND i.name = 'BySiteIDAreaIDBedroomTypeID')
BEGIN
	CREATE UNIQUE INDEX BySiteIDAreaIDBedroomTypeID ON dbo.LocationContent
	(
		SiteID ASC,
		AreaID ASC,
		BlockID ASC,
		BedroomTypeID ASC
	)
END
ELSE
BEGIN
	PRINT 'Index LocationContent.BySiteIDAreaIDBedroomTypeID already exists'
END
;


IF NOT EXISTS (SELECT * FROM sys.columns c WHERE c.object_id = OBJECT_ID(N'[dbo].[LocationContent]') AND c.name = 'Latitude')
BEGIN
	ALTER TABLE dbo.LocationContent
	ADD
		Latitude	int	NULL
END
ELSE
BEGIN
	PRINT 'Column Latitude already exists on LocationContent'
END
;


IF NOT EXISTS (SELECT * FROM sys.columns c WHERE c.object_id = OBJECT_ID(N'[dbo].[LocationContent]') AND c.name = 'Longitude')
BEGIN
	ALTER TABLE dbo.LocationContent
	ADD
		Longitude	varchar(50)	NULL
END
ELSE
BEGIN
	PRINT 'Column Longitude already exists on LocationContent'
END
;

IF EXISTS (select null from sys.columns c join sys.types t on t.system_type_id = c.system_type_id where c.object_id = OBJECT_ID(N'dbo.LocationContent') AND c.name = 'Latitude' AND t.name = 'int')
BEGIN
	ALTER TABLE dbo.LocationContent
	ALTER COLUMN Latitude varchar(50) null
END
;

IF EXISTS (select null from sys.columns c join sys.types t on t.system_type_id = c.system_type_id where c.object_id = OBJECT_ID(N'dbo.LocationContent') AND c.name = 'Longitude' AND t.name = 'int')
BEGIN
	ALTER TABLE dbo.LocationContent
	ALTER COLUMN Longitude varchar(50) null
END
;

/* END C:\TFS\KCF-STUDENT\Branches\Trunk\KCF.Student\Database\Tables\LocationContent.sql */
/* BEGIN C:\TFS\KCF-STUDENT\Branches\Trunk\KCF.Student\Database\Tables\WebFriendshipGroup.sql */

IF NOT EXISTS (SELECT NULL FROM sys.tables WHERE object_id = OBJECT_ID(N'dbo.WebFriendshipGroup'))
BEGIN
	CREATE TABLE [dbo].[WebFriendshipGroup](
		[FriendshipGroupID] [int] IDENTITY(1,1) NOT NULL PRIMARY KEY,
		[CreationDate] [datetime] NULL,
		[GroupName] [varchar](50) NULL,
		[AcademicYearID] [int] NOT NULL,
		[ApplicationGUID] [uniqueidentifier] NULL,
		[GroupLeaderUserGUID] [uniqueidentifier] NULL,
	)
END
ELSE
BEGIN
	PRINT 'Table dbo.WebFriendshipGroup already exists'
END
GO

IF NOT EXISTS (SELECT NULL FROM sys.indexes WHERE object_id = OBJECT_ID(N'dbo.WebFriendshipGroup') AND name = 'ByApplicationGUID')
BEGIN
	CREATE INDEX ByApplicationGUID ON dbo.WebFriendshipGroup
	(
		ApplicationGuid ASC
	)
END
ELSE
BEGIN
	PRINT 'Index WebFriendshipGroup.ByApplicationGUID already exists'
END
GO

IF NOT EXISTS (SELECT NULL FROM sys.columns WHERE object_id = OBJECT_ID(N'dbo.WebFriendshipGroup') AND name = 'KxFriendshipGroupID')
BEGIN
	ALTER TABLE dbo.WebFriendshipGroup
	ADD
		KxFriendshipGroupID int NULL
END
ELSE
BEGIN
	PRINT 'Column dbo.WebFriendshipGroup.KxFriendshipGroupID already exists'
END
GO

/* END C:\TFS\KCF-STUDENT\Branches\Trunk\KCF.Student\Database\Tables\WebFriendshipGroup.sql */
/* BEGIN C:\TFS\KCF-STUDENT\Branches\Trunk\KCF.Student\Database\Tables\WebFriendshipGroupMember.sql */

IF NOT EXISTS (SELECT NULL FROM sys.tables WHERE object_id = OBJECT_ID(N'[dbo].[WebFriendshipGroupMember]'))
BEGIN
	CREATE TABLE [dbo].[WebFriendshipGroupMember](
		[GroupMemberID] [int] IDENTITY(1,1) NOT NULL PRIMARY KEY,
		[FriendshipGroupID] [int] NOT NULL,
		[CreationDate] [datetime] NOT NULL,
		[AcceptedDate] [datetime] NULL,
		[RejectedDate] [datetime] NULL,
		[RejectedReason] [varchar](255) NULL,
		[MemberEmailAddress] [varchar](255) NULL,
		[MemberSurname] [varchar](50) NULL,
		[MemberStudentID] [varchar](50) NULL,
		[MemberDOB] [datetime] NULL,
	)
END
ELSE
BEGIN
	PRINT 'Table WebFriendshipGroupMember already exists'
END
GO

IF NOT EXISTS (SELECT NULL FROM sys.columns WHERE object_id = OBJECT_ID(N'[dbo].[WebFriendshipGroupMember]') AND name = 'MemberForename')
BEGIN
	ALTER TABLE dbo.WebFriendshipGroupMember
	ADD MemberForename nvarchar(50) NULL
END
ELSE
BEGIN
	PRINT 'Column WebFriendshipGroupMember.MemberForename already exists'
END
GO

IF NOT EXISTS (SELECT NULL FROM sys.columns WHERE object_id = OBJECT_ID(N'[dbo].[WebFriendshipGroupMember]') AND name = 'ApplicationGUID')
BEGIN
	ALTER TABLE dbo.WebFriendshipGroupMember
	ADD ApplicationGUID uniqueidentifier NULL
END
ELSE
BEGIN
	PRINT 'Column WebFriendshipGroupMember.ApplicationGUID already exists'
END
GO

IF(SELECT TOP 1 t.name FROM sys.columns c JOIN sys.types t ON t.user_type_id = c.user_type_id AND object_id = OBJECT_ID(N'[dbo].[WebFriendshipGroupMember]') AND c.name = 'MemberSurname') = 'varchar'
BEGIN
	ALTER TABLE dbo.WebFriendshipGroupMember
	ALTER COLUMN MemberSurname nvarchar(50) NULL
END
ELSE
BEGIN
	PRINT 'Column WebFriendshipGroupMember.MemberSurname is already nvarchar'
END
GO

/* END C:\TFS\KCF-STUDENT\Branches\Trunk\KCF.Student\Database\Tables\WebFriendshipGroupMember.sql */
/* BEGIN C:\TFS\KCF-STUDENT\Branches\Trunk\KCF.Student\Database\Tables\WebPaymentCache.sql */

IF NOT EXISTS (SELECT * FROM sysobjects WHERE name = 'WebPaymentCache' AND type = 'U')
BEGIN
		CREATE TABLE [dbo].[WebPaymentCache](
			[CacheId] [uniqueidentifier] NOT NULL PRIMARY KEY,
			[Provider] [varchar](200) NULL,
			[Reference] [varchar](100) NULL,
			[CustomerId] [varchar](100) NULL,
			[CustomerIdentifier] [int] NULL,
			[Name] [varchar](200) NULL,
			[TransactionId] [int] NULL,
			[TransactionGuid] [uniqueidentifier] NULL,
			[PaidAmount] [decimal](18, 2) NULL,
			[PaidAmount2] [decimal](18, 2) NULL,
			[Currency] [varchar](20) NULL,
			[AuthorisationCode] [varchar](50) NULL,
			[AuthorisationCode2] [varchar](50) NULL,
			[Authorised] [bit] NULL,
			[TimeStampOut] [datetime] NULL,
			[TimeStampIn] [datetime] NULL,
			[Processed] [bit] NULL
		) ON [PRIMARY]
END
ELSE
BEGIN
	PRINT 'Table WebPaymentCache already exists'
END;

/* END C:\TFS\KCF-STUDENT\Branches\Trunk\KCF.Student\Database\Tables\WebPaymentCache.sql */
/* BEGIN C:\TFS\KCF-STUDENT\Branches\Trunk\KCF.Student\Database\Tables\WebPaymentCacheCustomField.sql */

IF NOT EXISTS (SELECT * FROM sysobjects WHERE name = 'WebPaymentCacheCustomField' AND type ='U')
BEGIN
	CREATE TABLE [dbo].[WebPaymentCacheCustomField](
		[CustomFieldCacheId] [int] IDENTITY(1,1) NOT NULL PRIMARY KEY,
		[CacheId] [uniqueidentifier] NOT NULL,
		[Name] [varchar](1000) NULL,
		[Value] [varchar](1000) NULL
	) ON [PRIMARY]
END
ELSE
BEGIN
	PRINT 'Table WebPaymentCacheCustomField already exists'
END;

/* END C:\TFS\KCF-STUDENT\Branches\Trunk\KCF.Student\Database\Tables\WebPaymentCacheCustomField.sql */
/* BEGIN C:\TFS\KCF-STUDENT\Branches\Trunk\KCF.Student\Database\Tables\WebPaymentCachePayment.sql */

IF NOT EXISTS (SELECT * FROM sysobjects WHERE name = 'WebPaymentCachePayment' AND type = 'U')
BEGIN
	CREATE TABLE [dbo].[WebPaymentCachePayment](
		[PaymentCacheId] [int] IDENTITY(1,1) NOT NULL PRIMARY KEY,
		[CacheId] [uniqueidentifier] NOT NULL,
		[ChargeId] [int] NULL,
		[RequestedAmount] [decimal](18, 2) NULL,
		[Description] [varchar](500) NULL,
		[PaymentType] [varchar](50) NULL
	) ON [PRIMARY]
END
ELSE
BEGIN
	PRINT 'Table WebPaymentCachePayment already exists'
END;

/* END C:\TFS\KCF-STUDENT\Branches\Trunk\KCF.Student\Database\Tables\WebPaymentCachePayment.sql */
/* BEGIN C:\TFS\KCF-STUDENT\Branches\Trunk\KCF.Student\Database\Tables\WebReceipts.sql */

IF NOT EXISTS (SELECT NULL FROM sys.tables WHERE object_id = OBJECT_ID(N'[dbo].[WebReceipts]'))
BEGIN

	CREATE TABLE [dbo].[WebReceipts](
		[ReceiptId] [int] IDENTITY(1,1) NOT NULL PRIMARY KEY,
		[ApplicationGUID] [uniqueidentifier] NOT NULL
	) ON [PRIMARY]

END
ELSE
BEGIN
	PRINT 'Table [dbo].[WebReceipts] already exists'
END
GO



/* END C:\TFS\KCF-STUDENT\Branches\Trunk\KCF.Student\Database\Tables\WebReceipts.sql */
/* BEGIN C:\TFS\KCF-STUDENT\Branches\Trunk\KCF.Student\Database\Tables\WebStudentAddress.sql */

IF NOT EXISTS (SELECT NULL FROM sys.tables WHERE object_id = OBJECT_ID(N'dbo.WebStudentAddress'))
BEGIN

	CREATE TABLE [dbo].[WebStudentAddress](
		[AddressID] [int] IDENTITY(1,1) NOT NULL PRIMARY KEY,
		[ApplicationGUID] [uniqueidentifier] NOT NULL,
		[CreationDate] [datetime] NOT NULL,
		[AddressTypeID] [int] NOT NULL,
		[Address1] [varchar](255) NULL,
		[Address2] [varchar](255) NULL,
		[Address3] [varchar](255) NULL,
		[Address4] [varchar](255) NULL,
		[Postcode] [varchar](20) NULL,
		[ContactName] [varchar](255) NULL,
		[TelephoneNumber] [varchar](50) NULL,
		[MobileNumber] [varchar](50) NULL,
		[FaxNumber] [varchar](50) NULL,
		[EmailAddress] [varchar](255) NULL,
	)
END
ELSE
BEGIN
	PRINT 'Table dbo.WebStudentAddress already exists'
END;

IF NOT EXISTS (SELECT NULL FROM sys.columns c WHERE object_id = OBJECT_ID(N'dbo.WebStudentAddress', N'U') AND c.name = 'Title')
BEGIN
	ALTER TABLE dbo.WebStudentAddress
	ADD
		Title nvarchar(50) null
END
ELSE
BEGIN
	PRINT 'Column dbo.WebStudentAddress.Title already exists'
END;

IF NOT EXISTS (SELECT NULL FROM sys.columns c WHERE object_id = OBJECT_ID(N'dbo.WebStudentAddress', N'U') AND c.name = 'Forename')
BEGIN
	ALTER TABLE dbo.WebStudentAddress
	ADD
		Forename nvarchar(255) null
END
ELSE
BEGIN
	PRINT 'Column dbo.WebStudentAddress.Forename already exists'
END;

IF NOT EXISTS (SELECT NULL FROM sys.columns c WHERE object_id = OBJECT_ID(N'dbo.WebStudentAddress', N'U') AND c.name = 'Surname')
BEGIN
	ALTER TABLE dbo.WebStudentAddress
	ADD
		Surname nvarchar(255) null
END
ELSE
BEGIN
	PRINT 'Column dbo.WebStudentAddress.Surname already exists'
END;

IF NOT EXISTS (SELECT NULL FROM sys.columns c WHERE object_id = OBJECT_ID(N'dbo.WebStudentAddress', N'U') AND c.name = 'Relationship')
BEGIN
	ALTER TABLE dbo.WebStudentAddress
	ADD
		Relationship nvarchar(50) null
END
ELSE
BEGIN
	PRINT 'Column dbo.WebStudentAddress.Relationship already exists'
END;

/* END C:\TFS\KCF-STUDENT\Branches\Trunk\KCF.Student\Database\Tables\WebStudentAddress.sql */
/* BEGIN C:\TFS\KCF-STUDENT\Branches\Trunk\KCF.Student\Database\Tables\WebStudentApplication.sql */

IF NOT EXISTS (SELECT null FROM sys.tables WHERE object_id = OBJECT_ID(N'dbo.WebStudentApplication'))
BEGIN
	CREATE TABLE [dbo].[WebStudentApplication](
		[ApplicationGUID] [uniqueidentifier] NOT NULL PRIMARY KEY,
		[ProcessGUID] [uniqueidentifier] NULL,
		[KxStudentID] [int] NULL,
		[AcademicYearID] [int] NULL,
		[ApplicationID] [int] NULL,
		[ApplicationTypeID] [int] NULL,
		[CourseTypeID] [int] NULL,
		[StudentYearID] [int] NULL,
		[LicenceTypeID] [int] NULL,
		[UserGUID] [uniqueidentifier] NULL,
		[StudentIdentifier] [varchar](50) NULL,
		[Title] [varchar](50) NULL,
		[Forename] [varchar](50) NULL,
		[Midname] [varchar](50) NULL,
		[Surname] [varchar](50) NULL,
		[EmailAddress] [varchar](255) NULL,
		[DOB] [datetime] NULL,
		[NationalityID] [int] NULL,
		[EthnicityID] [int] NULL,
		[CreationDate] [datetime] NOT NULL,
		[ConfirmedDate] [datetime] NULL,
		[Gender] [varchar](10) NULL,
		[Expired] [tinyint] NULL DEFAULT (0),
		[PaymentScheduleTypeId] [int] NULL
	)
END
ELSE
BEGIN
	PRINT 'Table dbo.WebStudentApplication already exists'
END;

IF NOT EXISTS (SELECT null FROM sys.columns WHERE object_id = OBJECT_ID(N'dbo.WebStudentApplication') AND name = 'SessionID')
BEGIN
	ALTER TABLE dbo.WebStudentApplication
	ADD	SessionID	int NOT NULL DEFAULT(0)
END
ELSE
BEGIN
	PRINT 'Column dbo.WebStudentApplication.SessionID already exists'
END;

IF NOT EXISTS (SELECT null FROM sys.columns WHERE object_id = OBJECT_ID(N'dbo.WebStudentApplication') AND name = 'SessionStartDate')
BEGIN
	ALTER TABLE dbo.WebStudentApplication
	ADD	SessionStartDate	datetime NULL
END
ELSE
BEGIN
	PRINT 'Column dbo.WebStudentApplication.SessionStartDate already exists'
END;

IF NOT EXISTS (SELECT null FROM sys.columns WHERE object_id = OBJECT_ID(N'dbo.WebStudentApplication') AND name = 'SessionEndDate')
BEGIN
	ALTER TABLE dbo.WebStudentApplication
	ADD	SessionEndDate	datetime NULL
END
ELSE
BEGIN
	PRINT 'Column dbo.WebStudentApplication.SessionEndDate already exists'
END;

/* END C:\TFS\KCF-STUDENT\Branches\Trunk\KCF.Student\Database\Tables\WebStudentApplication.sql */
/* BEGIN C:\TFS\KCF-STUDENT\Branches\Trunk\KCF.Student\Database\Tables\WebStudentApplicationInduction.sql */

IF NOT EXISTS (SELECT null FROM sys.tables WHERE object_id = OBJECT_ID(N'dbo.WebStudentApplicationInduction'))
BEGIN
	CREATE TABLE [dbo].[WebStudentApplicationInduction](
		[InductionID] [int] NOT NULL PRIMARY KEY IDENTITY(1, 1),
		[ApplicationGUID] [uniqueidentifier] NOT NULL,
		[AllocationID] [int] NULL,
		[InductionStarted] [datetime] NOT NULL,
		[InductionCompleted] [datetime] NULL,
		[InductionAudit] varchar(max) NULL
	)
END
ELSE
BEGIN
	PRINT 'Table dbo.WebStudentApplicationInduction already exists'
END;

/* END C:\TFS\KCF-STUDENT\Branches\Trunk\KCF.Student\Database\Tables\WebStudentApplicationInduction.sql */
/* BEGIN C:\TFS\KCF-STUDENT\Branches\Trunk\KCF.Student\Database\Tables\WebStudentCorrespondance.sql */

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[WebStudentCorrespondance]') AND type in (N'U'))
BEGIN
	CREATE TABLE [dbo].[WebStudentCorrespondance]
	(
		[WebStudentCorrespondenceID] [int] IDENTITY(1,1) NOT NULL PRIMARY KEY,
		[KxStudentId] [int] NOT NULL,
		[BinaryDocument] [image] NOT NULL,
		[DocumentName] [varchar](100) NOT NULL,
		[ApplicationGuid] [uniqueidentifier] NOT NULL,
		[FileName] [varchar](255) NULL,
		[Processed] [int] NULL,
	)
END
ELSE
BEGIN
	PRINT 'WebStudentCorrespondance table already exists'
END
GO

IF EXISTS (SELECT NULL FROM sys.columns c JOIN sys.types t ON t.user_type_id = c.user_type_id WHERE object_id =  OBJECT_ID(N'[dbo].[WebStudentCorrespondance]') AND c.name = 'Processed' AND t.name = 'bit')
BEGIN
	ALTER TABLE [dbo].[WebStudentCorrespondance]
	ALTER COLUMN [Processed] [int] NULL
END
ELSE
BEGIN
	PRINT 'Processed column is already an int'
END
GO

/* END C:\TFS\KCF-STUDENT\Branches\Trunk\KCF.Student\Database\Tables\WebStudentCorrespondance.sql */
/* BEGIN C:\TFS\KCF-STUDENT\Branches\Trunk\KCF.Student\Database\Tables\WebStudentPayment.sql */

IF NOT EXISTS (SELECT NULL FROM sys.tables WHERE object_id = OBJECT_ID(N'[dbo].[WebStudentPayment]'))
BEGIN
CREATE TABLE [dbo].[WebStudentPayment](
	[WebPaymentID] [int] IDENTITY(1,1) NOT NULL PRIMARY KEY,
	[ApplicationGUID] [uniqueidentifier] NOT NULL,
	[PaymentDate] [datetime] NULL,
	[TransactionReference] [varchar](255) NULL,
	[TransactionAmount] [decimal](18, 2) NULL,
	[GatewayName] [varchar](50) NULL,
	[PaymentType] [varchar](50) NULL,
)
END
ELSE
BEGIN
	PRINT 'Table [dbo].[WebStudentPayment] already exists'
END
GO

IF NOT EXISTS (SELECT NULL FROM sys.columns WHERE object_id = OBJECT_ID(N'[dbo].[WebStudentPayment]') AND name = 'StudentPaymentID')
BEGIN
	ALTER TABLE dbo.WebStudentPayment
	ADD StudentPaymentID int NULL

	EXEC sp_sqlexec 'UPDATE WebStudentPayment SET StudentPaymentID = -1'
END
ELSE
BEGIN
	PRINT 'Column dbo.WebStudentPayment.StudentPaymentID already exists'
END
GO


/* END C:\TFS\KCF-STUDENT\Branches\Trunk\KCF.Student\Database\Tables\WebStudentPayment.sql */
/* BEGIN C:\TFS\KCF-STUDENT\Branches\Trunk\KCF.Student\Database\Tables\WebStudentPaymentAudit.sql */

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[WebStudentPaymentAudit]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[WebStudentPaymentAudit]
(
	[AuditId] [int] IDENTITY(1,1) NOT NULL PRIMARY KEY,
	[Direction] [varchar](50) NULL,
	[Provider] [varchar](200) NULL,
	[PaymentType] [varchar](50) NULL,
	[Reference] [varchar](100) NULL,
	[StudentId] [int] NULL,
	[Name] [varchar](200) NULL,
	[ApplicationId] [int] NULL,
	[ApplicationGuid] [uniqueidentifier] NULL,
	[ChargeIds] [varchar](50) NULL,
	[RequestedAmount] [decimal](18, 2) NULL,
	[PaidAmount] [decimal](18, 2) NULL,
	[Currency] [varchar](20) NULL,
	[AuthorisationCode] [varchar](50) NULL,
	[Authorised] [bit] NULL,
	[TimeStamp] [datetime] NULL,
)
END
ELSE
BEGIN
	PRINT 'Table WebStudentPaymentAudit already exists'
END
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = OBJECT_ID(N'[dbo].[WebStudentPaymentAudit]') AND name = 'Raw')
BEGIN
	ALTER TABLE [dbo].[WebStudentPaymentAudit]
	ADD [Raw] varchar(max) NULL
END
ELSE
BEGIN
	PRINT 'Column WebStudentPaymentAudit.Raw already exists'
END
GO

/* END C:\TFS\KCF-STUDENT\Branches\Trunk\KCF.Student\Database\Tables\WebStudentPaymentAudit.sql */
/* BEGIN C:\TFS\KCF-STUDENT\Branches\Trunk\KCF.Student\Database\Tables\WebStudentPaymentBatch.sql */

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[WebStudentPaymentBatch]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[WebStudentPaymentBatch]
(
	[UniqueId] [uniqueidentifier] NOT NULL PRIMARY KEY,
	[StudentChargeId] [int] NOT NULL,
	[StatementText] [varchar](200) NULL,
	[OutstandingBalance] [decimal](18, 2) NULL,
	[WebPaymentId] [int] NULL
)
END
ELSE
BEGIN
	PRINT 'Table [WebStudentPaymentBatch] already exists'
END
GO

/* END C:\TFS\KCF-STUDENT\Branches\Trunk\KCF.Student\Database\Tables\WebStudentPaymentBatch.sql */
/* BEGIN C:\TFS\KCF-STUDENT\Branches\Trunk\KCF.Student\Database\Tables\WebSubstitution.sql */

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[WebSubstitution]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[WebSubstitution](
	[KeyValue] [varchar](900) NOT NULL PRIMARY KEY,
	[en-GB] [varchar](max) NULL,
	[fr] [varchar](max) NULL,
	[de] [varchar](max) NULL,
	[nl] [varchar](max) NULL
)
END
ELSE
BEGIN
	PRINT 'WebSubstitution already exists'
END;


IF NOT EXISTS (SELECT * FROM sys.columns c WHERE c.object_id = OBJECT_ID(N'[dbo].[WebSubstitution]') AND c.name = 'en')
BEGIN
	ALTER TABLE dbo.WebSubstitution
		ADD [en] [varchar](max) NULL
END
ELSE
BEGIN
	PRINT 'WebSubstitution.en already exists'
END;


UPDATE dbo.WebSubstitution SET [en] = [en-GB] WHERE [en] IS NULL and [en-GB] IS NOT NULL;


IF NOT EXISTS (SELECT * FROM sys.columns c WHERE c.object_id = OBJECT_ID(N'[dbo].[WebSubstitution]') AND c.name = 'cy')
BEGIN
	ALTER TABLE dbo.WebSubstitution
		ADD [cy] [varchar](max) NULL
END
ELSE
BEGIN
	PRINT 'WebSubstitution.cy already exists'
END;

/* END C:\TFS\KCF-STUDENT\Branches\Trunk\KCF.Student\Database\Tables\WebSubstitution.sql */
/* BEGIN C:\TFS\KCF-STUDENT\Branches\Trunk\KCF.Student\Database\Tables\WebUser.sql */

IF NOT EXISTS (SELECT NULL FROM sys.tables WHERE object_id = OBJECT_ID(N'dbo.WebUser'))
BEGIN
CREATE TABLE [dbo].[WebUser](
	[UserGUID] [uniqueidentifier] NOT NULL PRIMARY KEY,
	[UserName] [varchar](255) NULL,
	[Password] [varchar](255) NULL,
	[ForeName] [varchar](50) NULL,
	[SurName] [varchar](50) NULL,
	[Salt] [varchar](255) NULL,
	[CreationDate] [datetime] NULL,
	[Active] [int] NULL,
	[EmailAddress] [varchar](255) NULL,
	[EmailConfirmed] [datetime] NULL,
	[KxStudentID] [int] NULL,
	[UserID] [int] NULL,
	[PersonID] [int] NULL,
	[LockedAccountTime] [datetime] NULL,
	[RetryCount] [int] NULL
)
END
ELSE
BEGIN
	PRINT 'Table dbo.WebUser already exists'
END;

IF NOT EXISTS (SELECT null FROM sys.columns WHERE object_id = OBJECT_ID(N'dbo.WebUser') AND name = 'ExtraText1')
BEGIN
	ALTER TABLE dbo.WebUser
	ADD	ExtraText1    VARCHAR(MAX)  NULL  
END
ELSE
BEGIN
	PRINT 'Column dbo.WebUser.ExtraText1 already exists'
END;

IF NOT EXISTS (SELECT null FROM sys.columns WHERE object_id = OBJECT_ID(N'dbo.WebUser') AND name = 'ExtraText2')
BEGIN
	ALTER TABLE dbo.WebUser
	ADD	ExtraText2    VARCHAR(MAX)  NULL  
END
ELSE
BEGIN
	PRINT 'Column dbo.WebUser.ExtraText2 already exists'
END;

IF NOT EXISTS (SELECT null FROM sys.columns WHERE object_id = OBJECT_ID(N'dbo.WebUser') AND name = 'ExtraText3')
BEGIN
	ALTER TABLE dbo.WebUser
	ADD	ExtraText3    VARCHAR(MAX)  NULL  
END
ELSE
BEGIN
	PRINT 'Column dbo.WebUser.ExtraText3 already exists'
END;

IF NOT EXISTS (SELECT null FROM sys.columns WHERE object_id = OBJECT_ID(N'dbo.WebUser') AND name = 'ExtraInt1')
BEGIN
	ALTER TABLE dbo.WebUser
	ADD	ExtraInt1    INT  NULL  
END
ELSE
BEGIN
	PRINT 'Column dbo.WebUser.ExtraInt1 already exists'
END;

IF NOT EXISTS (SELECT null FROM sys.columns WHERE object_id = OBJECT_ID(N'dbo.WebUser') AND name = 'ExtraInt2')
BEGIN
	ALTER TABLE dbo.WebUser
	ADD	ExtraInt2    INT  NULL  
END
ELSE
BEGIN
	PRINT 'Column dbo.WebUser.ExtraInt2 already exists'
END;

IF NOT EXISTS (SELECT null FROM sys.columns WHERE object_id = OBJECT_ID(N'dbo.WebUser') AND name = 'ExtraInt3')
BEGIN
	ALTER TABLE dbo.WebUser
	ADD	ExtraInt3    INT  NULL  
END
ELSE
BEGIN
	PRINT 'Column dbo.WebUser.ExtraInt3 already exists'
END;

IF NOT EXISTS (SELECT null FROM sys.columns WHERE object_id = OBJECT_ID(N'dbo.WebUser') AND name = 'SecretQuestion')
BEGIN
	ALTER TABLE dbo.WebUser
	ADD	SecretQuestion    VARCHAR(MAX)  NULL  
END
ELSE
BEGIN
	PRINT 'Column dbo.WebUser.SecretQuestion already exists'
END;

IF NOT EXISTS (SELECT null FROM sys.columns WHERE object_id = OBJECT_ID(N'dbo.WebUser') AND name = 'SecretAnswer')
BEGIN
	ALTER TABLE dbo.WebUser
	ADD	SecretAnswer    VARCHAR(MAX)  NULL  
END
ELSE
BEGIN
	PRINT 'Column dbo.WebUser.SecretAnswer already exists'
END;

/* END C:\TFS\KCF-STUDENT\Branches\Trunk\KCF.Student\Database\Tables\WebUser.sql */

/* END C:\TFS\KCF-STUDENT\Branches\Trunk\KCF.Student\Database\2. Tables.sql */
/* BEGIN C:\TFS\KCF-STUDENT\Branches\Trunk\KCF.Student\Database\3. Views.sql */

/* BEGIN C:\TFS\KCF-STUDENT\Branches\Trunk\KCF.Student\Database\Views\ViewStudentStatement.sql */

IF EXISTS (SELECT * FROM sysobjects WHERE name = 'ViewStudentStatement' AND type = 'V')
BEGIN
	DROP VIEW dbo.ViewStudentStatement
END
GO

CREATE VIEW [dbo].[ViewStudentStatement]
AS
select 'C'+CAST(sc.StudentChargeId as varchar(50)) as uniqueId,
		sc.KxStudentId,
		sc.StudentChargeId,
		0 as StudentPaymentId,
		sa.AcademicYearId,sc.ApplicationId,sc.AllocationDateId,
		sc.DateFrom,
		sc.DateTo,
		COALESCE(sc.DueDate, SC.DateFrom) AS DueDate,
		DateDiff(DD,sc.DateFrom,sc.DateTo) as Nights,
		sc.StatementText,
		TotalAmount,
		ISNULL(s.Name,'') as SiteName,
		ISNULL(a.Description,'') as AreaName,
		ISNULL(rr.Name,'') as RoomName,
		sc.StudentChargeTypeId,
		sct.Description as ChargeDescription,
		InvoiceNumber as DocumentNumber,
	0 as PaymentReasonId,'' as PaymentReasonDescription,
	0 as PaymentMethodId, '' as PaymentMethodDescription,
	0 as DepositTypeId,
	ISNULL((select SUM(AmountAllocated) from ST2PaymentAllocations where FromStudentChargeId = sc.StudentChargeId),0.00) as FromCharge,
	ISNULL((select SUM(AmountAllocated) from ST2PaymentAllocations where ToStudentChargeId = sc.StudentChargeId),0.00) as ToCharge,
	0.00 as FromPayment,
	0.00 as ToPayment
from st2studentcharges sc 
join StudentChargeTypes sct on sct.StudentChargeTypeId = sc.StudentChargeTypeId
join ST2StudentApplications sa on sa.ApplicationID = sc.ApplicationId
left join Sites s on s.SiteId = sc.SiteId
left join Areas a on a.AreaId = sc.AreaId
left join ResidentialRooms rr on rr.ResidentialRoomId = sc.ResidentialRoomId

union ALL

select 'P'+CAST(sp.StudentPaymentId as varchar(50)),
		sp.KxStudentId,
		0,
		sp.StudentPaymentId,
		sa.AcademicYearId,
		sp.ApplicationId,
		sp.ApplicationId,
		sp.ReferenceDate,
		sp.ReferenceDate,
		0,
		0,
		sp.StatementText,-Amount,
	ISNULL(s.Name,'') as SiteName,
	ISNULL(a.Description,'') as AreaName,
	ISNULL(rr.Name,'') as RoomName,
	0,
	'',
	sp.ReceiptNumber,
	sp.PaymentReasonId,
	ISNULL(pr.Description,''),sp.PaymentMethodId,ISNULL(pm.Description,''),
	ISNULL((select distinct 1 from  studentdeposittypes where paymentreasonid = sp.PaymentReasonId),0),
	0.00,
	0.00,
	ISNULL((select SUM(AmountAllocated) from ST2PaymentAllocations where FromStudentPaymentId = sp.StudentPaymentId),0.00),
	ISNULL((select SUM(AmountAllocated) from ST2PaymentAllocations where ToStudentPaymentId = sp.StudentPaymentId),0.00)
	
from st2studentpayments sp 
join ST2StudentApplications sa on sa.ApplicationID = sp.ApplicationId
left join Sites s on s.SiteId = sp.SiteId
left join Areas a on a.AreaId = sp.AreaId
left join ResidentialRooms rr on rr.ResidentialRoomId = sp.ResidentialRoomId
left join StudentpaymentReasons pr on pr.PaymentReasonId = sp.PaymentReasonid
left join PaymentMethods pm on pm.PaymentMethodId = sp.PaymentMethodId




GO



/* END C:\TFS\KCF-STUDENT\Branches\Trunk\KCF.Student\Database\Views\ViewStudentStatement.sql */

/* END C:\TFS\KCF-STUDENT\Branches\Trunk\KCF.Student\Database\3. Views.sql */
/* BEGIN C:\TFS\KCF-STUDENT\Branches\Trunk\KCF.Student\Database\4. Functions.sql */

/* BEGIN C:\TFS\KCF-STUDENT\Branches\Trunk\KCF.Student\Database\Functions\dbo.fnPreferenceAvailabilityHasRooms.sql */

IF EXISTS (SELECT NULL FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fnPreferenceAvailabilityHasRooms]', N'fn'))
BEGIN
	DROP FUNCTION [dbo].[fnPreferenceAvailabilityHasRooms]
END
GO

/****** Object:  UserDefinedFunction [dbo].[fnPreferenceAvailabilityHasRooms]    Script Date: 19/03/2014 16:24:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[fnPreferenceAvailabilityHasRooms](@PreferenceAvailabilityID int) RETURNS int AS
BEGIN
	IF EXISTS (
SELECT DISTINCT
	RR.ResidentialRoomID,
	RR.SubBlockID,
	RR.BlockID,
	PA.AllocationModelID,
	PA.PricingModelID
FROM
	PreferenceAvailability PA
	JOIN ST2RoomConfig RC ON RC.AcademicYearID = PA.AcademicYearID AND ISNULL(RC.CustomValue3, '') = ISNULL(PA.CustomValue3, '') AND ISNULL(RC.CustomValue2, '') = ISNULL(PA.CustomValue2, '') AND RC.DefaultPricingModelID = PA.PricingModelID AND RC.AllocationModelID = PA.AllocationModelID AND RC.WebPublished = 1
	JOIN ResidentialRooms RR ON RR.ResidentialRoomID = RC.ResidentialRoomID
	JOIN SiteBlocks B ON B.BlockID = RR.BlockID AND B.SiteID = PA.SiteID AND B.AreaID = PA.AreaID AND B.BlockID = COALESCE(PA.BlockID, B.BlockID)
	JOIN ST2AllocationModelDates AMD ON AMD.AllocationModelID = PA.AllocationModelID AND AMD.AcademicYearID = PA.AcademicYearID
WHERE
	PreferenceAvailabilityID = @PreferenceAvailabilityID
AND NOT EXISTS (SELECT NULL FROM ResidentialRoomOccupants RRO WHERE RRO.ResidentialRoomID = RC.ResidentialRoomID AND RRO.Date BETWEEN AMD.StartDate AND AMD.EndDate)
UNION ALL
SELECT DISTINCT
	RR.ResidentialRoomID,
	RR.SubBlockID,
	RR.BlockID,
	PA.AllocationModelID,
	PA.PricingModelID
FROM
	PreferenceAvailability PA
	JOIN ST2RoomConfig RC ON RC.AcademicYearID = PA.AcademicYearID AND ISNULL(RC.CustomValue3, '') = ISNULL(PA.CustomValue3, '') AND ISNULL(RC.CustomValue2, '') = ISNULL(PA.CustomValue2, '') AND RC.WebPublished = 1
	JOIN ResidentialRooms RR ON RR.ResidentialRoomID = RC.ResidentialRoomID
	JOIN SiteBlocks B ON B.BlockID = RR.BlockID AND B.SiteID = PA.SiteID AND B.AreaID = PA.AreaID AND B.BlockID = COALESCE(PA.BlockID, B.BlockID)
	JOIN ST2RoomAllocationModels RAM ON RAM.RoomConfigID = RC.RoomConfigID AND RAM.AllocationModelID = PA.AllocationModelID
	JOIN ST2AllocationModelDates AMD ON AMD.AllocationModelID = PA.AllocationModelID AND AMD.AcademicYearID = PA.AcademicYearID
WHERE
	PreferenceAvailabilityID = @PreferenceAvailabilityID
AND EXISTS (SELECT NULL FROM ST2RoomPricingModels RPM WHERE RPM.RoomConfigID = RC.RoomConfigID AND RPM.PricingModelID = PA.PricingModelID)
AND NOT EXISTS (SELECT NULL FROM ResidentialRoomOccupants RRO WHERE RRO.ResidentialRoomID = RC.ResidentialRoomID AND RRO.Date BETWEEN AMD.StartDate AND AMD.EndDate)
	)
	BEGIN
		RETURN 1
	END
	
	RETURN 0

END

GO



/* END C:\TFS\KCF-STUDENT\Branches\Trunk\KCF.Student\Database\Functions\dbo.fnPreferenceAvailabilityHasRooms.sql */

/* END C:\TFS\KCF-STUDENT\Branches\Trunk\KCF.Student\Database\4. Functions.sql */
/* BEGIN C:\TFS\KCF-STUDENT\Branches\Trunk\KCF.Student\Database\5. Procedures.sql */

/* BEGIN C:\TFS\KCF-STUDENT\Branches\Trunk\KCF.Student\Database\Stored Procedures\kcf.Application_ProcessToKx.sql */

IF EXISTS (SELECT NULL FROM sys.procedures WHERE object_id = OBJECT_ID(N'[kcf].[Application_ProcessToKx]'))
BEGIN
	DROP PROCEDURE [kcf].[Application_ProcessToKx]
END
GO

CREATE PROCEDURE [kcf].[Application_ProcessToKx]
(
	@ApplicationGuid VARCHAR(100)
)
	
AS

DECLARE @UserGUID uniqueidentifier,
	@ApplicationId int,
	@ApplicationTypeId int,
	@kxStudentId int,
	@StudentYearId int,
	@BedroomCount varchar(5),
	@BedRoomTypeId int,
	@AreaId int

SELECT 
	@kxStudentId = KxStudentId,
	@UserGUID = UserGUID,
	@ApplicationTypeId = ApplicationTypeId,
	@StudentYearId = StudentYearid,
	@AreaId = ISNULL(wap.AreaId,0),
	@ApplicationID = ISNULL(wsa.ApplicationID, 0)
FROM
	WebStudentApplication wsa
LEFT JOIN WebStudentApplicationPreference wap ON wap.ApplicationGuid = wsa.ApplicationGuid AND wap.PreferenceNumber = 1
WHERE
	wsa.ApplicationGuid = @ApplicationGuid

BEGIN TRANSACTION
		
	DECLARE @Manual bit

	SET @Manual = 0

	IF @ApplicationID = 0
	BEGIN

   		INSERT INTO ST2StudentApplications 
		(
			KxStudentID,
			ApplicationDate,
			LicenceTypeID,
			InactiveReasonID,
			AcademicYearID,
			SessionID,
			StudentYearID,
			CourseTypeID,
			CreationDate,
			CreationUserID,
			ApplicationTypeID,
			ApplicationSourceID,
			InactiveUserID,
			InactiveDate,
			WaitingListReasonID,
			WaitingListUserID,
			WaitingListDate,
			InactiveNotes,
			WaitingListNotes,
			ManualAllocationOnly,
			SessionStartDate,
			SessionEndDate
		)
		SELECT
			wu.KxStudentID,
			GetDate(),
			LicenceTypeID,
			0 AS InactiveReasonID,
			AcademicYearID,
			SessionID,
			1 AS StudentYearID,
			CourseTypeID,
			GetDate(),
			1 AS CreationUserID,
			ApplicationTypeID,
			1 AS ApplicationSourceID,
			0 AS InactiveUserID,
			-2 AS InactiveDate,
			0 AS WaitingListReasonID,
			0 AS WaitingListUserID,
			-2 AS WaitingListDate,
			'' AS InactiveNotes,
			'' AS WaitingListNotes,	
			@Manual,
			wsa.SessionStartDate,
			wsa.SessionEndDate
		FROM	WebStudentApplication wsa
		JOIN	WebUser wu ON wu.UserGuid = wsa.userGUID WHERE applicationGUID = @applicationGuid


		IF @@ERROR <> 0 GOTO RollbackBlock
	
		SET @ApplicationId = SCOPE_IDENTITY()

		UPDATE WebStudentApplication SET ApplicationId = @ApplicationId WHERE applicationGuid = @applicationGuid

		IF @@ERROR <> 0 GOTO RollbackBlock
	END
	ELSE
	BEGIN
		DELETE
			P
		FROM
			ST2StudentPreferences P
		WHERE
			P.ApplicationID = @ApplicationID	

		UPDATE
			A
		SET
			A.CourseTypeID = WSA.CourseTypeID,
			A.LicenceTypeID = WSA.LicenceTypeID,
			A.ApplicationTypeID = WSA.ApplicationTypeID,
			A.SessionStartDate = WSA.SessionStartDate,
			A.SessionEndDate = WSA.SessionEndDate
		FROM
			WebStudentApplication wsa
			JOIN	WebUser wu ON wu.UserGuid = wsa.userGUID
			JOIN ST2StudentApplications A ON A.ApplicationID = WSA.ApplicationID
		 WHERE 
			WSA.applicationGUID = @applicationGuid
		AND WSA.ApplicationID = @ApplicationID

		IF @@ERROR <> 0 GOTO RollbackBlock
	END

	INSERT INTO StudentPeriods
	(
		PersonStudentID,
		PeriodNumber,
		EventModuleID,
		ApplicationID
	)
	(
		SELECT
			KxStudentId,
			1 AS PeriodNumber,
			0 AS EventModuleID,
			ApplicationId
		FROM
			ST2StudentApplications SA
		WHERE
			SA.ApplicationID = @ApplicationID
		AND	NOT EXISTS (SELECT NULL FROM StudentPeriods P WHERE P.PeriodNumber = 1 AND P.ApplicationID = SA.ApplicationID)
	)

	IF @@ERROR <> 0 GOTO RollbackBlock

	INSERT INTO ST2StudentPreferences
	(	ApplicationId,		
		PreferenceNumber,	
		SiteID,		
		AreaID,
		BlockID,		
		BedroomTypeID,		
		[Floor],	
		SingleSexArea,
		Smoking,		
		CustomField1,		
		CustomField2,	
		CustomValue1,
		CustomValue2,		
		CustomValue3,		
		CustomValue4,	
		CreationDate,
		CreationUserId,	
		Notes
	)
	SELECT 	
		@ApplicationId,		
		WSAP.PreferenceNumber,	
		WSAP.SiteID,		
		WSAP.AreaID,
		WSAP.BlockID,		
		WSAP.BedroomTypeID,	
		WSAP.Floor,	
		WSAP.SingleSexArea,
		WSAP.Smoking,		
		WSAP.CustomField1,		
		WSAP.CustomField2,	
		WSAP.CustomValue1,
		WSAP.CustomValue2,		
		WSAP.CustomValue3,
		WSAP.CustomValue4,	
		GetDate() AS CreationDate,
		1 AS CreationUserID,
		'' AS Notes
	FROM 
		WebStudentApplicationPreference WSAP
	WHERE 
		WSAP.ApplicationGUID = @applicationGuid  
	ORDER BY 
		WSAP.PreferenceNumber

	IF @@ERROR <> 0 GOTO RollbackBlock

	INSERT INTO CustomFields2Values
	(
		CustomFieldID,
		EntityType,
		EntityID,
		FieldValue
	)
	(
		SELECT
			um.GermUDF,
			'ST2StudentApplication',
			@ApplicationId,
			FieldValue
		FROM 
			WebStudentApplicationUDFValues udf
		JOIN udfmapping um ON um.Webudf = udf.customfieldid
		WHERE 
			ApplicationGuid = @ApplicationGUID 
		AND fieldvalue IS NOT NULL 
		AND fieldvalue <> ''
		AND NOT EXISTS (SELECT NULL FROM CustomFields2Values V WHERE V.CustomFieldID = um.GERMUDF AND V.EntityType = 'ST2StudentApplication' AND V.EntityID = @ApplicationID)
	)
	IF @@ERROR <> 0 GOTO RollbackBlock

	UPDATE
		CFV
	SET
		CFV.FieldValue = UDF.FieldValue
	FROM
			WebStudentApplicationUDFValues udf
		JOIN udfmapping um ON um.Webudf = udf.customfieldid
		JOIN CustomFields2Values CFV ON CFV.CustomFieldID = UM.GERMUDF AND CFV.EntityType = 'ST2StudentApplication' AND CFV.EntityID = @ApplicationID AND CFV.FieldValue <> UDF.FieldValue
		WHERE 
			ApplicationGuid = @ApplicationGUID 
		AND UDF.fieldvalue IS NOT NULL 
		AND UDF.fieldvalue <> ''
	IF @@ERROR <> 0 GOTO RollbackBlock

	DECLARE @EmailId INT 

	SELECT
		@EmailID = StudentAddressID
	FROM
		WebUser U
	JOIN ST2StudentAddresses A ON A.KxStudentID = U.KxStudentID AND A.AddressTypeID = 6 AND A.CreationUserID = 1
	WHERE
		U.UserGUID = @UserGUID

	SET @EmailID = ISNULL(@EmailID, 0)

	IF @EmailID = 0
	BEGIN	
		INSERT INTO ST2StudentAddresses
		(
			KxStudentID,
			AddressTypeID,
			[Address],
			CreationDate,
			CreationUserID,
			EmailAddress,
			Imported
		)
		(
			SELECT
				KxStudentID,	
				6 AS AddressTypeID,
				'' AS [Address],
				GetDate() AS CreationDate,
				1 AS CreationUserID,
				EmailAddress,
				0 AS Imported
			FROM
				WebUser U
			WHERE 
				userGUID = @UserGUID
		)
		IF @@ERROR <> 0 GOTO RollbackBlock		

		SET @EmailId = SCOPE_IDENTITY()
	END
	ELSE
	BEGIN
		UPDATE
			A
		SET
			A.EmailAddress = U.EmailAddress
		FROM
			WebUser U
			JOIN ST2StudentAddresses A ON A.KxStudentID = U.KxStudentID AND A.AddressTypeID = 6 AND A.CreationUserID = 1 AND A.StudentAddressID = @EmailID
		WHERE
			U.UserGUID = @UserGUID

		IF @@ERROR <> 0 GOTO RollbackBlock		
	END

	UPDATE St2Students SET PreferredEmailAddressID = @EmailId WHERE KxStudentId = @KxStudentId


	IF @@ERROR <> 0 GOTO RollbackBlock

	/* Documents */

	INSERT INTO ST2StudentCorrespondence
	(
		KxStudentID, --int (4)
		CreationDate, --datetime (8)
		CreationUserID, --int (4)
		DocumentType, --varchar (50)
		ResponseRequestedBy, --datetime (8)
		ResponseReceived, --datetime (8)
		ApplicationID, --int (4)
		AllocationID, --int (4)
		DocumentCreated, --datetime (8)
		[Sent], --datetime (8)

		[Description], --varchar (1000)
		DocumentName, --varchar (255)
		Document, --image (16)
		DocumentText, --text (16)
		TemplateId, --int (4)
		DocumentTypeId, --int (4)
		DocumentImageType, --int (4)
		ContactMethodId, --int (4)
		PrintToFileDestination, --varchar (4000)
		PostSourceAddressId, --int (4)
		MassMailingId, --int (4)
		InvoiceNumber, --int (4)
		SMSResponse, --varchar (30)
		SMSProviderRef --varchar (255)
	)
	(
		SELECT
			WSA.KxStudentID, --int (4)
			GETDATE() AS CreationDate, --datetime (8)
			1 AS CreationUserID, --int (4)
			'Web Upload' AS DocumentType, --varchar (50)
			-2 AS ResponseRequestedBy, --datetime (8)
			-2 AS ResponseReceived, --datetime (8)
			@ApplicationID, --int (4)
			0 AS AllocationID, --int (4)
			GETDATE() AS DocumentCreated, --datetime (8)
			-2 AS [Sent], --datetime (8)

			WSC.FileName AS [Description], --varchar (1000)
			WSC.DocumentName, --varchar (255)
			WSC.BinaryDocument AS Document, --image (16)
			'Uploaded document' AS DocumentText, --text (16)
			0 AS TemplateId, --int (4)
			0 AS DocumentTypeId, --int (4)
			0 AS DocumentImageType, --int (4)
			0 AS ContactMethodId, --int (4)
			'' AS PrintToFileDestination, --varchar (4000)
			0 AS PostSourceAddressId, --int (4)
			0 AS MassMailingId, --int (4)
			0 AS InvoiceNumber, --int (4)
			NULL AS SMSResponse, --varchar (30)
			NULL AS SMSProviderRef --varchar (255)
		FROM
			WebStudentCorrespondance WSC
			JOIN WebStudentApplication WSA ON WSA.ApplicationGUID = WSC.ApplicationGUID
		WHERE
			WSC.ApplicationGUID = @ApplicationGUID
		AND WSC.Processed = 0
	)
	IF @@ERROR <> 0 GOTO RollbackBlock

	UPDATE
		WSC
	SET
		WSC.Processed = 1
	FROM
			WebStudentCorrespondance WSC
			JOIN WebStudentApplication WSA ON WSA.ApplicationGUID = WSC.ApplicationGUID
		WHERE
			WSC.ApplicationGUID = @ApplicationGUID
		AND WSC.Processed = 0

	/* END Documents */

	DECLARE @StudentPaymentID	int,
			@RowCount			int

	INSERT INTO ST2StudentPayments
	(
		ApplicationID,
		KxStudentID,
		PaymentReasonID,
		StatementText,
		PaymentMethodID,
		SiteID,
		AreaID,
		ResidentialRoomID,
		CreationDate,
		CreationUserID,
		ReferenceNumber,
		ReferenceDate,
		ReceiptNumber,
		ExportBatchID,
		ReleaseExportBatchID,
		Amount,
		SuppressFromInterface
	)
	(
		SELECT 
			@ApplicationID, 
			@KxStudentId,
			1 AS PaymentReasonID ,
			'' AS StatementText,
			14 AS PaymentMethodID,
			0 AS SiteID,
			0 AS AreaID,
			0 AS ResidentialRoomID,
			CURRENT_TIMESTAMP AS CreationDate,
			1 AS CreationUserID,
			CAST(transactionreference AS varchar(50)) AS ReferenceNumber,
			PaymentDate AS ReferenceDate,
			-1,
			0 AS ExportBatchID,
			0 AS ReleaseExportBatchID,
			transactionamount AS Amount,
			0 AS SuppressFromInterface
		FROM 
			webstudentpayment P
		WHERE 
			ApplicationGuid = @applicationGuid
		AND NOT EXISTS (SELECT NULL FROM ST2StudentPayments SP WHERE SP.KxStudentID = @KxStudentID AND SP.ApplicationID = @ApplicationID AND SP.ReferenceNumber = CAST(transactionreference AS varchar(50)))
	)
	IF @@ERROR > 0 GOTO RollbackBlock
	SET @RowCount = @@ROWCOUNT
	SET @StudentPaymentID = SCOPE_IDENTITY()

	IF ISNULL(@StudentPaymentID, 0) > 0 AND @RowCount = 1
	BEGIN
		DECLARE @ReceiptNumber int
		EXEC DBNAME..GetLastNumbers 'STURECEIPT','Y', @ReceiptNumber OUTPUT, 1

		UPDATE
			ST2StudentPayments
		SET
			ReceiptNumber = @ReceiptNumber
		WHERE
			StudentPaymentID = @StudentPaymentID
	IF @@ERROR > 0 GOTO RollbackBlock
	END

COMMIT
GOTO ReturnBlock

RollbackBlock:
	ROLLBACK

ReturnBlock:
	RETURN 0
;

GO

/* END C:\TFS\KCF-STUDENT\Branches\Trunk\KCF.Student\Database\Stored Procedures\kcf.Application_ProcessToKx.sql */
/* BEGIN C:\TFS\KCF-STUDENT\Branches\Trunk\KCF.Student\Database\Stored Procedures\kcf.Application_ProcessToKxCreatingStudent.sql */

IF EXISTS (SELECT NULL FROM sys.procedures WHERE object_id = OBJECT_ID(N'[kcf].[Application_ProcessToKxCreatingStudent]'))
BEGIN
	DROP PROCEDURE [kcf].[Application_ProcessToKxCreatingStudent]
END
GO

CREATE PROCEDURE [kcf].[Application_ProcessToKxCreatingStudent]
(
	@ApplicationGuid VARCHAR(100)
)
	
AS

DECLARE @UserGUID uniqueidentifier,
	@ApplicationId int,
	@ApplicationTypeId int,
	@kxStudentId int,
	@StudentYearId int,
	@BedroomCount varchar(5),
	@BedRoomTypeId int,
	@AreaId int,
	@CreateResult	varchar(max)

SELECT 
	@kxStudentId = WU.KxStudentId,
	@UserGUID = WSA.UserGUID,
	@ApplicationTypeId = WSA.ApplicationTypeId,
	@StudentYearId = WSA.StudentYearid,
	@AreaId = ISNULL(wap.AreaId,0),
	@ApplicationID = ISNULL(wsa.ApplicationID, 0)
FROM
	WebStudentApplication wsa
	JOIN WebUSer WU ON WU.UserGUID = WSA.UserGUID
LEFT JOIN WebStudentApplicationPreference wap ON wap.ApplicationGuid = wsa.ApplicationGuid AND wap.PreferenceNumber = 1
WHERE
	wsa.ApplicationGuid = @ApplicationGuid

BEGIN TRANSACTION
		
	DECLARE @Manual bit

	SET @Manual = 0

	IF ISNULL(@KxStudentID, 0) = 0
	BEGIN
		EXEC kcf.[Internal_CreateKxStudentRecordAndBindToWebUser] @ApplicationGUID = @ApplicationGUID, @UserGUID = @UserGUID, @KxStudentID = @KxStudentID OUTPUT, @Result = @CreateResult OUTPUT

		IF ISNULL(@KxStudentID, 0) = 0
		BEGIN
			PRINT @CreateResult
			GOTO RollbackBlock
		END
	END

	IF @ApplicationID = 0
	BEGIN

   		INSERT INTO ST2StudentApplications 
		(
			KxStudentID,
			ApplicationDate,
			LicenceTypeID,
			InactiveReasonID,
			AcademicYearID,
			SessionID,
			StudentYearID,
			CourseTypeID,
			CreationDate,
			CreationUserID,
			ApplicationTypeID,
			ApplicationSourceID,
			InactiveUserID,
			InactiveDate,
			WaitingListReasonID,
			WaitingListUserID,
			WaitingListDate,
			InactiveNotes,
			WaitingListNotes,
			ManualAllocationOnly,
			SessionStartDate,
			SessionEndDate
		)
		SELECT
			wu.KxStudentID,
			GetDate(),
			LicenceTypeID,
			0 AS InactiveReasonID,
			AcademicYearID,
			SessionID,
			1 AS StudentYearID,
			CourseTypeID,
			GetDate(),
			1 AS CreationUserID,
			ApplicationTypeID,
			1 AS ApplicationSourceID,
			0 AS InactiveUserID,
			-2 AS InactiveDate,
			0 AS WaitingListReasonID,
			0 AS WaitingListUserID,
			-2 AS WaitingListDate,
			'' AS InactiveNotes,
			'' AS WaitingListNotes,	
			@Manual,
			wsa.SessionStartDate,
			wsa.SessionEndDate
		FROM	WebStudentApplication wsa
		JOIN	WebUser wu ON wu.UserGuid = wsa.userGUID WHERE applicationGUID = @applicationGuid


		IF @@ERROR <> 0 GOTO RollbackBlock
	
		SET @ApplicationId = SCOPE_IDENTITY()

		UPDATE WebStudentApplication SET ApplicationId = @ApplicationId WHERE applicationGuid = @applicationGuid

		IF @@ERROR <> 0 GOTO RollbackBlock
	END
	ELSE
	BEGIN
		DELETE
			P
		FROM
			ST2StudentPreferences P
		WHERE
			P.ApplicationID = @ApplicationID	

		UPDATE
			A
		SET
			A.CourseTypeID = WSA.CourseTypeID,
			A.LicenceTypeID = WSA.LicenceTypeID,
			A.ApplicationTypeID = WSA.ApplicationTypeID,
			A.SessionStartDate = WSA.SessionStartDate,
			A.SessionEndDate = WSA.SessionEndDate
		FROM
			WebStudentApplication wsa
			JOIN	WebUser wu ON wu.UserGuid = wsa.userGUID
			JOIN ST2StudentApplications A ON A.ApplicationID = WSA.ApplicationID
		 WHERE 
			WSA.applicationGUID = @applicationGuid
		AND WSA.ApplicationID = @ApplicationID

		IF @@ERROR <> 0 GOTO RollbackBlock
	END

	INSERT INTO StudentPeriods
	(
		PersonStudentID,
		PeriodNumber,
		EventModuleID,
		ApplicationID
	)
	(
		SELECT
			KxStudentId,
			1 AS PeriodNumber,
			0 AS EventModuleID,
			ApplicationId
		FROM
			ST2StudentApplications SA
		WHERE
			SA.ApplicationID = @ApplicationID
		AND	NOT EXISTS (SELECT NULL FROM StudentPeriods P WHERE P.PeriodNumber = 1 AND P.ApplicationID = SA.ApplicationID)
	)

	IF @@ERROR <> 0 GOTO RollbackBlock

	INSERT INTO ST2StudentPreferences
	(	ApplicationId,		
		PreferenceNumber,	
		SiteID,		
		AreaID,
		BlockID,		
		BedroomTypeID,		
		[Floor],	
		SingleSexArea,
		Smoking,		
		CustomField1,		
		CustomField2,	
		CustomValue1,
		CustomValue2,		
		CustomValue3,		
		CustomValue4,	
		CreationDate,
		CreationUserId,	
		Notes
	)
	SELECT 	
		@ApplicationId,		
		WSAP.PreferenceNumber,	
		WSAP.SiteID,		
		WSAP.AreaID,
		WSAP.BlockID,		
		WSAP.BedroomTypeID,	
		WSAP.Floor,	
		WSAP.SingleSexArea,
		WSAP.Smoking,		
		WSAP.CustomField1,		
		WSAP.CustomField2,	
		WSAP.CustomValue1,
		WSAP.CustomValue2,		
		WSAP.CustomValue3,
		WSAP.CustomValue4,	
		GetDate() AS CreationDate,
		1 AS CreationUserID,
		'' AS Notes
	FROM 
		WebStudentApplicationPreference WSAP
	WHERE 
		WSAP.ApplicationGUID = @applicationGuid  
	ORDER BY 
		WSAP.PreferenceNumber

	IF @@ERROR <> 0 GOTO RollbackBlock

	INSERT INTO CustomFields2Values
	(
		CustomFieldID,
		EntityType,
		EntityID,
		FieldValue
	)
	(
		SELECT
			um.GermUDF,
			'ST2StudentApplication',
			@ApplicationId,
			FieldValue
		FROM 
			WebStudentApplicationUDFValues udf
		JOIN udfmapping um ON um.Webudf = udf.customfieldid
		WHERE 
			ApplicationGuid = @ApplicationGUID 
		AND fieldvalue IS NOT NULL 
		AND fieldvalue <> ''
		AND NOT EXISTS (SELECT NULL FROM CustomFields2Values V WHERE V.CustomFieldID = um.GERMUDF AND V.EntityType = 'ST2StudentApplication' AND V.EntityID = @ApplicationID)
	)
	IF @@ERROR <> 0 GOTO RollbackBlock

	UPDATE
		CFV
	SET
		CFV.FieldValue = UDF.FieldValue
	FROM
			WebStudentApplicationUDFValues udf
		JOIN udfmapping um ON um.Webudf = udf.customfieldid
		JOIN CustomFields2Values CFV ON CFV.CustomFieldID = UM.GERMUDF AND CFV.EntityType = 'ST2StudentApplication' AND CFV.EntityID = @ApplicationID AND CFV.FieldValue <> UDF.FieldValue
		WHERE 
			ApplicationGuid = @ApplicationGUID 
		AND UDF.fieldvalue IS NOT NULL 
		AND UDF.fieldvalue <> ''
	IF @@ERROR <> 0 GOTO RollbackBlock

	DECLARE @EmailId INT 

	SELECT
		@EmailID = StudentAddressID
	FROM
		WebUser U
	JOIN ST2StudentAddresses A ON A.KxStudentID = U.KxStudentID AND A.AddressTypeID = 6 AND A.CreationUserID = 1
	WHERE
		U.UserGUID = @UserGUID

	SET @EmailID = ISNULL(@EmailID, 0)

	IF @EmailID = 0
	BEGIN	
		INSERT INTO ST2StudentAddresses
		(
			KxStudentID,
			AddressTypeID,
			[Address],
			CreationDate,
			CreationUserID,
			EmailAddress,
			Imported
		)
		(
			SELECT
				KxStudentID,	
				6 AS AddressTypeID,
				'' AS [Address],
				GetDate() AS CreationDate,
				1 AS CreationUserID,
				EmailAddress,
				0 AS Imported
			FROM
				WebUser U
			WHERE 
				userGUID = @UserGUID
		)
		IF @@ERROR <> 0 GOTO RollbackBlock		

		SET @EmailId = SCOPE_IDENTITY()
	END
	ELSE
	BEGIN
		UPDATE
			A
		SET
			A.EmailAddress = U.EmailAddress
		FROM
			WebUser U
			JOIN ST2StudentAddresses A ON A.KxStudentID = U.KxStudentID AND A.AddressTypeID = 6 AND A.CreationUserID = 1 AND A.StudentAddressID = @EmailID
		WHERE
			U.UserGUID = @UserGUID

		IF @@ERROR <> 0 GOTO RollbackBlock		
	END

	UPDATE St2Students SET PreferredEmailAddressID = @EmailId WHERE KxStudentId = @KxStudentId


	IF @@ERROR <> 0 GOTO RollbackBlock

	DECLARE @StudentPaymentID	int,
			@RowCount			int

	INSERT INTO ST2StudentPayments
	(
		ApplicationID,
		KxStudentID,
		PaymentReasonID,
		StatementText,
		PaymentMethodID,
		SiteID,
		AreaID,
		ResidentialRoomID,
		CreationDate,
		CreationUserID,
		ReferenceNumber,
		ReferenceDate,
		ReceiptNumber,
		ExportBatchID,
		ReleaseExportBatchID,
		Amount,
		SuppressFromInterface
	)
	(
		SELECT 
			@ApplicationID, 
			@KxStudentId,
			1 AS PaymentReasonID ,
			'' AS StatementText,
			14 AS PaymentMethodID,
			0 AS SiteID,
			0 AS AreaID,
			0 AS ResidentialRoomID,
			CURRENT_TIMESTAMP AS CreationDate,
			1 AS CreationUserID,
			CAST(transactionreference AS varchar(50)) AS ReferenceNumber,
			PaymentDate AS ReferenceDate,
			-1,
			0 AS ExportBatchID,
			0 AS ReleaseExportBatchID,
			transactionamount AS Amount,
			0 AS SuppressFromInterface
		FROM 
			webstudentpayment P
		WHERE 
			ApplicationGuid = @applicationGuid
		AND NOT EXISTS (SELECT NULL FROM ST2StudentPayments SP WHERE SP.KxStudentID = @KxStudentID AND SP.ApplicationID = @ApplicationID AND SP.ReferenceNumber = CAST(transactionreference AS varchar(50)))
	)
	IF @@ERROR > 0 GOTO RollbackBlock
	SET @RowCount = @@ROWCOUNT
	SET @StudentPaymentID = SCOPE_IDENTITY()

	IF ISNULL(@StudentPaymentID, 0) > 0 AND @RowCount = 1
	BEGIN
		DECLARE @ReceiptNumber int
		EXEC KCF_Kx..GetLastNumbers 'STURECEIPT','Y', @ReceiptNumber OUTPUT, 1

		UPDATE
			ST2StudentPayments
		SET
			ReceiptNumber = @ReceiptNumber
		WHERE
			StudentPaymentID = @StudentPaymentID
	IF @@ERROR > 0 GOTO RollbackBlock
	END

COMMIT
GOTO ReturnBlock

RollbackBlock:
	ROLLBACK

ReturnBlock:
	RETURN 0
;

GO

/* END C:\TFS\KCF-STUDENT\Branches\Trunk\KCF.Student\Database\Stored Procedures\kcf.Application_ProcessToKxCreatingStudent.sql */
/* BEGIN C:\TFS\KCF-STUDENT\Branches\Trunk\KCF.Student\Database\Stored Procedures\kcf.GenerateOneTimeUseCodes.sql */

IF EXISTS (SELECT NULL FROM sys.procedures WHERE object_id = OBJECT_ID(N'kcf.GenerateOneTimeUseCodes'))
BEGIN
	DROP PROCEDURE kcf.GenerateOneTimeUseCodes
END
GO

CREATE PROCEDURE kcf.GenerateOneTimeUseCodes
	@NumberOfCodes	int,
	@CodeLength		int,
	@CreatedBy		varchar(50),
	@IssuedTo		varchar(50)
AS
DECLARE @Counter	int,
		@Code	varchar(50),
		@CreationSet	datetime

SET @CreationSet = getdate()

SET @Counter = 0
WHILE @Counter < @NumberOfCodes
BEGIN
	SET @Code = left(replace(newid(),'-',''),@CodeLength)
	INSERT INTO kcf.OneTimeUseCodes (OneTimeUseCode, CreatedBy, CreationDate, IssuedTo)
	(
		SELECT @Code, @CreatedBy, @CreationSet, @IssuedTo WHERE NOT EXISTS (SELECT  NULL FROM kcf.OneTimeUseCodes WHERE OneTimeUseCode = @Code)
	)
	SET @Counter = @Counter + @@ROWCOUNT
END

SELECT @CreationSet

GO

/* END C:\TFS\KCF-STUDENT\Branches\Trunk\KCF.Student\Database\Stored Procedures\kcf.GenerateOneTimeUseCodes.sql */
/* BEGIN C:\TFS\KCF-STUDENT\Branches\Trunk\KCF.Student\Database\Stored Procedures\kcf.GenerateStudentID.sql */

IF EXISTS (SELECT NULL FROM sys.procedures WHERE object_id = OBJECT_ID(N'kcf.GenerateStudentID'))
BEGIN
	DROP PROCEDURE kcf.GenerateStudentID
END
GO

CREATE PROCEDURE kcf.GenerateStudentID
	@UserGUID	uniqueidentifier,
	@StudentID	varchar(255) OUTPUT
AS

DECLARE @LastNumberType varchar(20),
		@LastNumber int

SET @LastNumberType = 'StudentID'

IF NOT EXISTS (SELECT NULL FROM dbo.LastNumbers WHERE LastNumberType = @LastNumberType)
BEGIN
	INSERT INTO dbo.LastNumbers
	( LastNumberType, LastNumber )
	VALUES
	( @LastNumberType, 0 )
END

SELECT
	@LastNumber	= LastNumber
FROM
	LastNumbers
WHERE
	LastNumberType	= @LastNumberType

SET @LastNumber = @LastNumber + 1

UPDATE
	LastNumbers
SET
	LastNumber		= @LastNumber
WHERE
	LastNumberType	= @LastNumberType

WHILE EXISTS (SELECT NULL FROM ST2Students S WHERE S.StudentID = RIGHT('0000000' + CAST(@LastNumber AS varchar(7)), 7))
BEGIN
	SET @LastNumber = @LastNumber + 1

	UPDATE
		LastNumbers
	SET
		LastNumber		= @LastNumber
	WHERE
		LastNumberType	= @LastNumberType
END

SET @StudentID = RIGHT('0000000' + CAST(@LastNumber AS varchar(7)), 7)


GO

/* END C:\TFS\KCF-STUDENT\Branches\Trunk\KCF.Student\Database\Stored Procedures\kcf.GenerateStudentID.sql */
/* BEGIN C:\TFS\KCF-STUDENT\Branches\Trunk\KCF.Student\Database\Stored Procedures\kcf.Internal_CreateKxStudentRecordAndBindToWebUser.sql */

IF EXISTS (SELECT NULL FROM sys.procedures WHERE object_id = OBJECT_ID(N'[kcf].[Internal_CreateKxStudentRecordAndBindToWebUser]'))
BEGIN
	DROP PROCEDURE [kcf].[Internal_CreateKxStudentRecordAndBindToWebUser]
END
GO

CREATE PROCEDURE [kcf].[Internal_CreateKxStudentRecordAndBindToWebUser]
		@ApplicationGUID	uniqueidentifier,
		@UserGUID			uniqueidentifier,
		@KxStudentID	int OUTPUT,
		@Result			varchar(255) OUTPUT
	AS

	DECLARE 
		@Now datetime,
		@NullDate datetime

		SELECT @Now=GETDATE()
		SET @NullDate = '1899-12-30 00:00:00.000'

	DECLARE 
		@Forename varchar(50),
		@Surname varchar(50),
		@DOB datetime,
		@Gender varchar(10),
		@NationalityID int,
		@NewNumber int,
		@StudentId varchar(255),
		@PersonID int,
		@ContactName varchar(255),
		@EmergencyStudentAddressID int,
		@CorrespondenceStudentAddressID int,
		@HomeStudentAddressID int,
		@ApplicationSourceID int,
		@StudentYearDesc varchar(20),
		@StudentYearID int,
		@SmokerYN bit,
		@SitePrefix varchar(3),
		@Pref1SiteID int

--*************************************************************************************************
-- Initialise our variables 
--*************************************************************************************************

		SET @Forename  = ''
		SET @Surname  = ''
		SET @DOB = @NullDate
		SET @Gender  = ''
		SET @NationalityID = 0
		SET @NewNumber = 0
		SET @StudentId  = ''
		SET @PersonID = 0
		SET @ContactName = ''
		SET @EmergencyStudentAddressID = 0
		SET @CorrespondenceStudentAddressID = 0
		SET @HomeStudentAddressID = 0
		SET @ApplicationSourceID = 0
		SET @StudentYearDesc = ''
		SET @StudentYearID = 0
		SET @SmokerYN = 0
		SET @SitePrefix = ''

	SELECT	
		@SitePrefix = UPPER(S.SiteCode)
	FROM 
		WebStudentApplication WSA
		JOIN WebStudentApplicationPreference WSAP ON WSAP.ApplicationGUID = WSA.ApplicationGUID AND WSAP.PreferenceNumber = 1
		JOIN Sites S ON S.SiteID = WSAP.SiteID
	WHERE 
		WSA.ApplicationGUID = @ApplicationGUID


	IF @KxStudentId = 0 --Student Record doesn't exist yet
	BEGIN
		SELECT
			@Forename= Forename, 
			@Surname = Surname, 
			@DOB = DOB, 
			@Gender = Gender,
			@NationalityID = NationalityID
		FROM 
			WebStudentApplication 
		WHERE 
			ApplicationGuid = @ApplicationGUID

		SET @DOB = ISNULL(@DOB, @NullDate)
		
		EXECUTE DBNAME..GetLastNumbers 'STUDENTCODE','Y',@NewNumber OUTPUT,1
	
		SET @StudentId = ISNULL(@SitePrefix, '')+CAST(@NewNumber as varchar(255))
		
		EXECUTE DBNAME..NewPeople
		   @PersonID OUTPUT ,0 ,@Surname ,@Forename ,'' ,'' ,@Gender ,@Dob ,''  ,''  ,''  ,''  ,''  ,''  ,''  ,0  ,0  ,''  ,0  ,0
		  ,''  ,0  ,0  ,0  ,''  ,''  ,''  ,@Now  ,1  ,null  ,0  ,0  ,''  ,''  ,''  ,''  ,''  ,''  ,''  ,0  ,0  ,0  ,0  ,0
		  ,0  ,0  ,0  ,0  ,0  ,0  ,''  ,0  ,0  ,0  ,''  ,0
		IF @@ERROR > 0
		BEGIN
			SET @Result='Step 1 - Create Person'
			GOTO RollbackBlock
		END
		
		EXECUTE DBNAME..NewST2Students	@KxStudentID OUTPUT,@StudentId,@StudentId,'',@Forename,@Surname,'','',0,'',@SmokerYN,0,@NationalityID,@DOB,0,@Now,1,@PersonId,0,0,@Gender,0,'','',1,'',NULL,'',0,0,0,0,0
		IF @@ERROR > 0
		BEGIN
			SET @Result='Step 1 - Create Student'
			GOTO RollbackBlock
		END
			
		UPDATE WebUser SET KxStudentId = @KxStudentId WHERE UserGUID = @UserGUID

		--Jump out if we've errored
		IF @@ERROR > 0
		BEGIN
			SET @Result='Step 1a - UPDATE WebUser with KxStudentID'
			GOTO RollbackBlock
		END

		UPDATE WebStudentApplication SET KxStudentId = @KxStudentId WHERE UserGUID = @UserGUID AND ApplicationGUID = @ApplicationGUID

		--Jump out if we've errored
		IF @@ERROR > 0
		BEGIN
			SET @Result='Step 1b - UPDATE WebStudentApplication with KxStudentID'
			GOTO RollbackBlock
		END

		SET @Result = 'Success'

	END

	GOTO ReturnBlock

RollbackBlock:
	SET @KxStudentID = 0

ReturnBlock:
	RETURN 0
GO

/* END C:\TFS\KCF-STUDENT\Branches\Trunk\KCF.Student\Database\Stored Procedures\kcf.Internal_CreateKxStudentRecordAndBindToWebUser.sql */
/* BEGIN C:\TFS\KCF-STUDENT\Branches\Trunk\KCF.Student\Database\Stored Procedures\kcf.IsWebApplicationInAFriendshipGroup.sql */

IF EXISTS (SELECT NULL FROM sys.procedures WHERE object_id = OBJECT_ID(N'kcf.IsWebApplicationInAFriendshipGroup'))
BEGIN
	DROP PROCEDURE kcf.IsWebApplicationInAFriendshipGroup
END
GO

CREATE PROCEDURE kcf.IsWebApplicationInAFriendshipGroup
	@ApplicationGUID	uniqueidentifier
AS

SELECT
	COUNT(*) AS Result
FROM
	WebFriendshipGroup WG
	JOIN WebFriendshipGroupMember GM ON GM.FriendshipGroupID = WG.FriendshipGroupID
WHERE
	@ApplicationGUID IN (WG.ApplicationGUID, GM.ApplicationGUID)

GO

/* END C:\TFS\KCF-STUDENT\Branches\Trunk\KCF.Student\Database\Stored Procedures\kcf.IsWebApplicationInAFriendshipGroup.sql */
/* BEGIN C:\TFS\KCF-STUDENT\Branches\Trunk\KCF.Student\Database\Stored Procedures\kcf.ProcessDocumentsToKxForApplication.sql */

IF EXISTS (SELECT null FROM sys.procedures WHERE object_id = OBJECT_ID(N'[kcf].[ProcessDocumentsToKxForApplication]'))
BEGIN
	DROP PROCEDURE [kcf].[ProcessDocumentsToKxForApplication]
END
GO

CREATE PROCEDURE [kcf].[ProcessDocumentsToKxForApplication]
	-- Add the parameters for the stored procedure here
	@ApplicationGUID varchar(100)
AS


DECLARE @KxStudentID int,
@ApplicationID int,
@WebStudentCorrespondenceID int

BEGIN
	SELECT 
		@KxStudentID = KxStudentID,
		@ApplicationID = ApplicationID
	FROM
		WebStudentApplication
	WHERE
		ApplicationGuid = @ApplicationGUID

-- Copy across the student's correspondance.
	
	DECLARE curDocuments CURSOR FOR
	SELECT WebStudentCorrespondenceID
	FROM WebStudentCorrespondance
	WHERE ApplicationGUID = @ApplicationGUID
	AND	ISNull(Processed,0)  < 1

	OPEN curDocuments
	FETCH NEXT FROM curDocuments INTO @WebStudentCorrespondenceID
	
	WHILE @@FETCH_STATUS = 0
	BEGIN
		DECLARE @DocumentID int

		INSERT INTO ST2StudentCorrespondence
		(			
			KxStudentID,
			CreationDate,
			CreationUserID,
			DocumentType,
			Description,
			DocumentName,
			Document,
			DocumentText,
			ResponseRequestedBy,
			ResponseReceived,
			ApplicationID,
			AllocationID,
			TemplateId,
			DocumentTypeId,
			DocumentImageType,
			DocumentCreated,
			ContactMethodId,
			PrintToFileDestination,
			PostSourceAddressId,
			Sent,
			MassMailingId,
			InvoiceNumber,
			SMSResponse,
			SMSProviderRef
		)
		SELECT
			KxStudentID,
			GETDATE(),
			1,
			'Web Document Upload',
			CAST(DocumentName + ':' + FileName as varchar(50)) ,
			FileName,
			BinaryDocument,
			'',
			'1899-12-30 00:00:00.000',
			'1899-12-30 00:00:00.000',
			@ApplicationID,
			0,
			NULL,
			0,
			0,
			GETDATE(),
			0,
			NULL,
			NULL,
			GETDATE(),
			0,
			NULL,
			NULL,
			NULL
		FROM 	WebStudentCorrespondance
		WHERE  WebStudentCorrespondenceID = @WebStudentCorrespondenceID

		SET @DocumentID = SCOPE_IDENTITY()
		
		UPDATE WebStudentCorrespondance SET Processed = @DocumentID WHERE @WebStudentCorrespondenceID = WebStudentCorrespondenceID

		FETCH NEXT FROM curDocuments INTO @WebStudentCorrespondenceID
	END
	CLOSE curDocuments
	DEALLOCATE curDocuments
END

GO

/* END C:\TFS\KCF-STUDENT\Branches\Trunk\KCF.Student\Database\Stored Procedures\kcf.ProcessDocumentsToKxForApplication.sql */
/* BEGIN C:\TFS\KCF-STUDENT\Branches\Trunk\KCF.Student\Database\Stored Procedures\kcf.ProcessPaymentToKxForApplication.sql */

IF EXISTS (SELECT NULL FROM sys.procedures WHERE object_id = OBJECT_ID(N'kcf.ProcessPaymentToKxForApplication'))
BEGIN
	DROP PROCEDURE kcf.ProcessPaymentToKxForApplication
END
GO

CREATE PROCEDURE kcf.ProcessPaymentToKxForApplication
	@ApplicationGUID	uniqueidentifier,
	@DepositPaymentReasonID	    int,
	@StatementPaymentReasonID	int,
	@PayNowPaymentReasonID		int,
	@SurchargePaymentReasonID	int,
	@SurchargeChargeTypeID		int,
	@PaymentMethodID			int,
	@WebPaymentID				int
AS

DECLARE @Result	int

SET @Result = 0

BEGIN TRANSACTION

INSERT INTO	dbo.ST2StudentPayments
(
	/* NOT NULL */
	KxStudentID, --int (4)
	PaymentReasonID, --int (4)
	PaymentMethodID, --int (4)
	SiteID, --int (4)
	AreaID, --int (4)
	ResidentialRoomID, --int (4)
	CreationDate, --datetime (8)
	CreationUserID, --int (4)
	ReferenceNumber, --varchar (50)
	ReferenceDate, --datetime (8)
	ReceiptNumber, --int (4)
	Amount, --money (8)
	SuppressFromInterface, --int (4)
	/* NULLABLE */
	ApplicationID, --int (4)
	StatementText, --varchar (255)
	ExportBatchID, --int (4)
	ReleaseExportBatchID, --int (4)
	ChequeRequestBatchID, --int (4)
	ReconciledDate, --datetime (8)
	ReconciledUserID, --int (4)
	PrintOnStatement --bit (1)
)
(
	SELECT
	/* NOT NULL */
	S.KxStudentID, --int (4)
	CASE WSP.PaymentType WHEN 'DEPOSIT' THEN @DepositPaymentReasonID WHEN 'PAYNOW' THEN @PayNowPaymentReasonID WHEN 'STATEMENT' THEN @StatementPaymentReasonID WHEN 'SURCHARGE' THEN @SurchargePaymentReasonID ELSE @PayNowPaymentReasonID END AS PaymentReasonID, --int (4)
	@PaymentMethodID, --int (4)
	COALESCE((SELECT TOP 1 B.SiteID FROM ST2StudentAllocations AL JOIN ResidentialRooms RR ON RR.ResidentialRoomID = AL.ResidentialRoomID JOIN SiteBlocks B ON B.BlockID = RR.BlockID WHERE AL.ApplicationID = SA.ApplicationID AND EXISTS (SELECT NULL FROM ST2StudentAllocationDates SAD WHERE SAD.AllocationID = AL.AllocationID)), SA.SiteID, 0) AS SiteID, --int (4)
	COALESCE((SELECT TOP 1 B.AreaID FROM ST2StudentAllocations AL JOIN ResidentialRooms RR ON RR.ResidentialRoomID = AL.ResidentialRoomID JOIN SiteBlocks B ON B.BlockID = RR.BlockID WHERE AL.ApplicationID = SA.ApplicationID AND EXISTS (SELECT NULL FROM ST2StudentAllocationDates SAD WHERE SAD.AllocationID = AL.AllocationID)), SA.AreaID, 0) AS AreaID, --int (4)
	COALESCE((SELECT TOP 1 AL.ResidentialRoomID FROM ST2StudentAllocations AL WHERE AL.ApplicationID = SA.ApplicationID AND EXISTS (SELECT NULL FROM ST2StudentAllocationDates SAD WHERE SAD.AllocationID = AL.AllocationID)), SA.AreaID, 0) AS ResidentialRoomID, --int (4)
	getdate() AS CreationDate, --datetime (8)
	1 AS CreationUserID, --int (4)
	WSP.TransactionReference AS ReferenceNumber, --varchar (50)
	WSP.PaymentDate AS ReferenceDate, --datetime (8)
	-1 AS ReceiptNumber, --int (4)
	WSP.TransactionAmount AS  Amount, --money (8)
	0 AS SuppressFromInterface, --int (4)
	/* NULLABLE */
	SA.ApplicationID, --int (4)
	PR.Description AS StatementText, --varchar (255)
	0 AS ExportBatchID, --int (4)
	0 AS ReleaseExportBatchID, --int (4)
	0 AS ChequeRequestBatchID, --int (4)
	-2 AS ReconciledDate, --datetime (8)
	0 AS ReconciledUserID, --int (4)
	1 AS PrintOnStatement --bit (1)
	FROM
		WebStudentPayment WSP
		JOIN WebStudentApplication WSA ON WSA.ApplicationGUID = WSP.ApplicationGUID
		JOIN WebUser WU ON WU.UserGUID = WSA.UserGUID
		JOIN ST2Students S ON S.KxStudentID = WU.KxStudentID
		JOIN ST2StudentApplications SA ON SA.ApplicationID = WSA.ApplicationID
		JOIN StudentPaymentReasons PR ON PR.PaymentReasonID = CASE WSP.PaymentType WHEN 'DEPOSIT' THEN @DepositPaymentReasonID WHEN 'PAYNOW' THEN @PayNowPaymentReasonID WHEN 'STATEMENT' THEN @StatementPaymentReasonID WHEN 'SURCHARGE' THEN @SurchargePaymentReasonID ELSE @PayNowPaymentReasonID END
	WHERE
		WSP.ApplicationGUID = @ApplicationGUID
	AND WSP.StudentPaymentID IS NULL
	AND NOT EXISTS (SELECT NULL FROM ST2StudentPayments P WHERE P.ApplicationID = SA.ApplicationID AND P.ReferenceNumber = WSP.TransactionReference)
	AND WSP.TransactionAmount <> 0
)
IF @@ERROR > 0
GOTO RollbackBlock

UPDATE
	WSP
SET
	WSP.StudentPaymentID = SP.StudentPaymentID
FROM
	WebStudentPayment WSP
	JOIN WebStudentApplication WSA ON WSA.ApplicationGUID = WSP.ApplicationGUID
	JOIN WebUser WU ON WU.UserGUID = WSA.UserGUID
	JOIN ST2StudentPayments SP ON SP.ApplicationID = WSA.ApplicationID AND SP.ReferenceNumber = WSP.TransactionReference
WHERE
	WSP.ApplicationGUID = @ApplicationGUID
IF @@ERROR > 0
GOTO RollbackBlock

/* Statement Payment */
IF @WebPaymentID > 0 AND (SELECT PaymentType FROM WebStudentPayment WHERE WebPaymentID = @WebPaymentID) = 'STATEMENT'
BEGIN
	INSERT INTO ST2PaymentAllocations
	(
		FromStudentChargeID, 
		ToStudentPaymentID, 
		AmountAllocated, 
		CreationUserID, 
		CreationDate
	)
	SELECT 
		 wpb.StudentChargeId, 
		 WP.StudentPaymentID, 
		 wpb.OutstandingBalance, 
		 1, 
		 getdate()
	FROM 
		WebStudentPayment wp
		JOIN WebStudentPaymentBatch wpb on wpb.WebPaymentId = wp.WebPaymentid
	WHERE 
		wp.WebPaymentID = @WebPaymentId
END

IF ISNULL(@SurchargeChargeTypeID, 0) > 0
BEGIN
	INSERT INTO ST2StudentCharges
	(
		KxStudentID, --int (4)
		ApplicationID, --int (4)
		AllocationDateID, --int (4)
		StudentChargeTypeID, --int (4)
		SiteID, --int (4)
		AreaID, --int (4)
		CreationDate, --datetime (8)
		CreationUserID, --int (4)
		DateFrom, --datetime (8)
		DateTo, --datetime (8)
		StatementText, --varchar (255)
		Amount, --money (8)
		VATID, --int (4)
		TotalAmount, --money (8)
		PrintOnStatement, --bit (1)
		InvoiceNumber, --int (4)
		ExportBatchID, --int (4)
		CreditReasonID, --int (4)
		RoomDamageID, --int (4)
		ResidentialRoomID, --int (4)
		SuppressFromInterface, --int (4)
		LinkedInvoiceNumber, --int (4)
		Period, --int (4)
		LineType, --varchar (10)
		TaxFromAccountChargeID, --int (4)
		InstallmentNo, --int (4)
		ContraCharge, --int (4)
		AllocationCharge, --datetime (8)
		DueDate --datetime (8)
	)
	(
	SELECT
		SP.KxStudentID, --int (4)
		SP.ApplicationID, --int (4)
		0 AS AllocationDateID, --int (4)
		@SurchargeChargeTypeID AS StudentChargeTypeID, --int (4)
		SP.SiteID, --int (4)
		SP.AreaID, --int (4)
		SP.CreationDate, --datetime (8)
		SP.CreationUserID, --int (4)
		SP.ReferenceDate AS DateFrom, --datetime (8)
		SP.ReferenceDate AS DateTo, --datetime (8)
		SCT.StatementText, --varchar (255)
		SP.Amount, --money (8)
		SCT.VATID, --int (4)
		SP.Amount, --money (8)
		1 AS PrintOnStatement, --bit (1)
		0 AS InvoiceNumber, --int (4)
		0 AS ExportBatchID, --int (4)
		0 AS CreditReasonID, --int (4)
		0 AS RoomDamageID, --int (4)
		SP.ResidentialRoomID, --int (4)
		0 AS SuppressFromInterface, --int (4)
		0  AS LinkedInvoiceNumber, --int (4)
		WSP.WebPaymentID AS Period, --int (4)
		'' AS LineType, --varchar (10)
		0 AS TaxFromAccountChargeID, --int (4)
		0 AS InstallmentNo, --int (4)
		0 AS ContraCharge, --int (4)
		-2 AS AllocationCharge, --datetime (8)
		SP.ReferenceDate AS DueDate --datetime (8)
	FROM
		WebStudentPayment WSP
		JOIN ST2StudentPayments SP ON SP.StudentPaymentID = WSP.StudentPaymentID
		JOIN StudentChargeTypes SCT ON SCT.StudentChargeTypeID = @SurchargeChargeTypeID
	WHERE
		WSP.ApplicationGUID = @ApplicationGUID
	AND WSP.PaymentType = 'SURCHARGE'
	AND NOT EXISTS (SELECT NULL FROM ST2PaymentAllocations PA WHERE PA.ToStudentPaymentID = SP.StudentPaymentID)
	)
	IF @@ERROR > 0
	GOTO RollbackBlock

	INSERT INTO ST2PaymentAllocations
	(
		FromStudentChargeID, --int (4)
		FromStudentPaymentID, --int (4)
		ToStudentChargeID, --int (4)
		ToStudentPaymentID, --int (4)
		AmountAllocated, --money (8)
		CreationUserID, --int (4)
		CreationDate --datetime (8)
	)
	(
		SELECT
			SC.StudentChargeID AS FromStudentChargeID, --int (4)
			0 AS FromStudentPaymentID, --int (4)
			0 AS ToStudentChargeID, --int (4)
			SP.StudentPaymentID AS ToStudentPaymentID, --int (4)
			SP.Amount AS AmountAllocated, --money (8)
			1 AS CreationUserID, --int (4)
			getdate() AS CreationDate --datetime (8)
		FROM
			WebStudentPayment WSP
			JOIN ST2StudentPayments SP ON SP.StudentPaymentID = WSP.StudentPaymentID
			JOIN ST2StudentCharges SC ON SC.ApplicationID = SP.ApplicationID AND SC.Amount = SP.Amount AND SC.Period = WSP.WebPaymentID
		WHERE
			WSP.ApplicationGUID = @ApplicationGUID
		AND WSP.PaymentType = 'SURCHARGE'
		AND NOT EXISTS (SELECT NULL FROM ST2PaymentAllocations PA WHERE PA.ToStudentPaymentID = SP.StudentPaymentID)
	)
	IF @@ERROR > 0
	GOTO RollbackBlock

	UPDATE
		SC
	SET
		SC.Period = 0
	FROM
		WebStudentPayment WSP
		JOIN ST2StudentPayments SP ON SP.StudentPaymentID = WSP.StudentPaymentID
		JOIN ST2StudentCharges SC ON SC.ApplicationID = SP.ApplicationID AND SC.Amount = SP.Amount AND SC.Period = WSP.WebPaymentID
	WHERE
		WSP.ApplicationGUID = @ApplicationGUID
	AND WSP.PaymentType = 'SURCHARGE'
	IF @@ERROR > 0
	GOTO RollbackBlock
END

SET @Result = 1
COMMIT

/* Set Receipt numbers */

DECLARE @StudentPaymentID	int,
		@ReceiptNumber		int

DECLARE cr CURSOR FOR
SELECT
	SP.StudentPaymentID
FROM
	WebStudentPayment WSP
	JOIN ST2StudentPayments SP ON SP.StudentPaymentID = WSP.StudentPaymentID AND SP.ReceiptNumber = -1
WHERE
	WSP.ApplicationGUID = @ApplicationGUID
	
OPEN cr
FETCH cr INTO @StudentPaymentID
WHILE @@FETCH_STATUS = 0
BEGIN
	EXECUTE DBNAME..GetLastNumbers 'STURECEIPT','Y',@ReceiptNumber OUTPUT,1

	UPDATE
		ST2StudentPayments
	SET
		ReceiptNumber = @ReceiptNumber
	WHERE
		StudentPaymentID = @StudentPaymentID
	AND ReceiptNumber = -1

	FETCH cr INTO @StudentPaymentID
END
CLOSE cr
DEALLOCATE cr

/* END Receipt Numbers */

GOTO ReturnBlock

RollbackBlock:
ROLLBACK
GOTO ReturnBlock

ReturnBlock:
SELECT @Result AS Result
RETURN

GO

/* END C:\TFS\KCF-STUDENT\Branches\Trunk\KCF.Student\Database\Stored Procedures\kcf.ProcessPaymentToKxForApplication.sql */
/* BEGIN C:\TFS\KCF-STUDENT\Branches\Trunk\KCF.Student\Database\Stored Procedures\kcf.ProcessUDFToKx.sql */

IF EXISTS (SELECT NULL FROM sys.procedures WHERE object_id = OBJECT_ID(N'[kcf].[ProcessUDFToKx]'))
BEGIN
	DROP PROCEDURE [kcf].[ProcessUDFToKx]
END
GO

CREATE PROCEDURE [kcf].[ProcessUDFToKx]
(
	@ApplicationGUID UniqueIdentifier,	-- Application Guid
	@WebUDFID int,			-- Web UDF Id
	@Value varchar(255) = NULL			-- Web Value
)
AS 

DECLARE @ApplicationId INT
DECLARE @GermUDFId INT
DECLARE @WebValue VARCHAR(255)

SELECT TOP 1
	@GermUDFId = GermUdf
FROM
	UDFMapping
WHERE
	WebUdf = @WebUDFId
	
SELECT
	@ApplicationId = wsa.ApplicationId,
	@WebValue = wudf.FieldValue
FROM
	WebStudentApplication wsa
	JOIN WebStudentApplicationUDFValues wudf ON wsa.ApplicationGUID = wudf.ApplicationGuid
WHERE
	wsa.ApplicationGUID = @ApplicationGuid
	AND CustomFieldId = @WebUDFId
	AND EntityType = 'ST2StudentApplications' 

IF EXISTS(SELECT TOP 1 FieldValue FROM CustomFields2Values WHERE EntityType = 'ST2StudentApplication' AND EntityID = @ApplicationId AND CustomFieldID = @GermUDFId)
	BEGIN
		UPDATE
			CustomFields2Values
		SET
			FieldValue = @WebValue
		WHERE
			EntityType = 'ST2StudentApplication'
			AND EntityID = @ApplicationId
			AND CustomFieldID = @GermUDFId
	END

ELSE
	
	BEGIN
		INSERT INTO CustomFields2Values (
			CustomFieldID,
			EntityType,
			EntityID,
			FieldValue
			)
		VALUES (
			@GermUDFId,
			'ST2StudentApplication',
			@ApplicationId,
			@WebValue
			)	
	END







GO



/* END C:\TFS\KCF-STUDENT\Branches\Trunk\KCF.Student\Database\Stored Procedures\kcf.ProcessUDFToKx.sql */
/* BEGIN C:\TFS\KCF-STUDENT\Branches\Trunk\KCF.Student\Database\Stored Procedures\kcf.ValidateJoinFriendshipGroup.sql */

IF EXISTS (SELECT NULL FROM sys.procedures WHERE object_id = OBJECT_ID(N'kcf.ValidateJoinFriendshipGroup'))
BEGIN
	DROP PROCEDURE kcf.ValidateJoinFriendshipGroup
END
GO

CREATE PROCEDURE kcf.ValidateJoinFriendshipGroup
		@GroupCode	varchar(50),
		@ValidateEmailAddress	varchar(255),
		@ApplicationGuid		uniqueidentifier
	AS

SELECT
	WGM.*
FROM
	WebStudentApplication WSA
	JOIN WebFriendshipGroupMember WGM ON WGM.MemberEmailAddress = @ValidateEmailAddress
	JOIN WebFriendshipGroup WG ON WG.FriendshipGroupID = WGM.FriendshipGroupID
	JOIN WebStudentApplication LSA ON LSA.ApplicationGUID = WG.ApplicationGUID
WHERE
	WSA.ApplicationGUID = @ApplicationGUID
AND WSA.AcademicYearID = LSA.AcademicYearID
AND NOT EXISTS (SELECT NULL FROM WebFriendshipGroupMember OM WHERE OM.ApplicationGUID = @ApplicationGUID AND OM.FriendshipGroupID <> WG.FriendshipGroupID) /* NOT ALREADY A MEMBER */
AND NOT EXISTS (SELECT NULL FROM WebFriendshipGroup OM WHERE OM.ApplicationGUID = @ApplicationGUID AND OM.FriendshipGroupID <> WG.FriendshipGroupID) /* NOT ALREADY A LEADER */
AND @GroupCode = WG.GroupName + CAST(WG.FriendshipGroupID as varchar(20))

GO

/* END C:\TFS\KCF-STUDENT\Branches\Trunk\KCF.Student\Database\Stored Procedures\kcf.ValidateJoinFriendshipGroup.sql */
/* BEGIN C:\TFS\KCF-STUDENT\Branches\Trunk\KCF.Student\Database\Stored Procedures\KxStudentDepositBalances_LibertySelect.sql */

IF EXISTS (SELECT * FROM sysobjects WHERE name = 'KxStudentDepositBalances_LibertySelect' AND type = 'P')
BEGIN
	DROP PROCEDURE dbo.KxStudentDepositBalances_LibertySelect
END
GO

CREATE PROCEDURE  [dbo].KxStudentDepositBalances_LibertySelect
  @KxStudentId int 
AS

SELECT
	1 AS DepositTypeID,
	'Security Deposit' AS Description,
	sum(amount) AS DepositBalance
FROM
	st2studentpayments p
	JOIN studentpaymentreasons r on r.paymentreasonid = p.paymentreasonid and r.memopayment = 1
WHERE
	P.KxStudentID = @KxStudentID

GO



/* END C:\TFS\KCF-STUDENT\Branches\Trunk\KCF.Student\Database\Stored Procedures\KxStudentDepositBalances_LibertySelect.sql */
/* BEGIN C:\TFS\KCF-STUDENT\Branches\Trunk\KCF.Student\Database\Stored Procedures\KxStudentDepositBalancesSelect.sql */

IF EXISTS (SELECT * FROM sysobjects WHERE name = 'KxStudentDepositBalancesSelect' AND type = 'P')
BEGIN
	DROP PROCEDURE dbo.KxStudentDepositBalancesSelect
END
GO

CREATE PROCEDURE  [dbo].[KxStudentDepositBalancesSelect]
  @KxStudentId int 
AS


SELECT
      DepositTypeID,
      Description,
      SUM(DepositBalance) AS DepositBalance
FROM
(
	SELECT
          DT.DepositTypeID,
          DT.Description,
           ABS(P.Amount) - 
           ISNULL((SELECT SUM(AmountAllocated) FROM ST2PaymentAllocations PA WHERE P.StudentPaymentID IN (PA.FromStudentPaymentID, PA.ToStudentPaymentID)), 0) AS DepositBalance
     FROM
	    ST2StudentPayments P
     JOIN 
		StudentDepositTypes DT ON DT.PaymentReasonID = P.PaymentReasonID
     WHERE
		P.KxStudentID = @KxStudentID
) D
GROUP BY
      DepositTypeID,[Description]




GO



/* END C:\TFS\KCF-STUDENT\Branches\Trunk\KCF.Student\Database\Stored Procedures\KxStudentDepositBalancesSelect.sql */
/* BEGIN C:\TFS\KCF-STUDENT\Branches\Trunk\KCF.Student\Database\Stored Procedures\KxWebCreateBookingSelect.sql */

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KxWebCreateBookingSelect]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.KxWebCreateBookingSelect
END
GO


CREATE PROCEDURE dbo.KxWebCreateBookingSelect
			@Parm1 VARCHAR(100), -- ApplicationGUID
			@Parm2 VARCHAR(100), -- PreferenceAvailabilityID
			@Parm3 VARCHAR(100), -- ResidentialRoomID
			@Parm4 INT  -- Not used
		AS

DECLARE @KxStudentID	int,
		@ApplicationID	int,
		@AllocationID	int,
		@AllocationModelID	int,
		@PricingModelID		int,
		@ResidentialRoomID	int,
		@AcademicYearID		int,
		@Result				int,
		@OfferLength		int,
		@PreferenceAvailabilityID	int


SELECT
	@OfferLength = CAST(ControlValue AS int)
FROM
	ControlStudent
WHERE
	[Description] = 'OBSOfferAgeHours'


SET @OfferLength = ISNULL(@OfferLength, 48)

SELECT
	@KxStudentID = KxStudentID,
	@ApplicationID = ApplicationID
FROM
	WebStudentApplication
WHERE
	ApplicationGUID = @Parm1

SELECT
	@AllocationModelID = AllocationModelID,
	@PricingModelID = PricingModelID,
	@AcademicYearID = AcademicYearID,
	@PreferenceAvailabilityID = PreferenceAvailabilityID
FROM
	PreferenceAvailability
WHERE
	PreferenceAvailabilityID = CAST(@Parm2 AS int)

SET @ResidentialRoomID = CAST(@Parm3 AS int)

BEGIN TRANSACTION

	/* Has room been allocated? */
	IF EXISTS (	SELECT 
					NULL 
				FROM
					ST2AllocationModelDates AMD
					JOIN ResidentialRoomOccupants RRO ON RRO.ResidentialRoomID = @ResidentialRoomID AND RRO.Date BETWEEN AMD.StartDate AND AMD.EndDate -1 AND RRO.Date >= CAST(CURRENT_TIMESTAMP AS date)
				WHERE
					AllocationModelID = @AllocationModelID
				AND AcademicYearID = @AcademicYearID
				AND AllocateDuringPeriod = 1
				AND AMD.EndDate >= CAST(CURRENT_TIMESTAMP AS date)
			)
	BEGIN
		SET @Result = -1
		GOTO RollbackBlock
	END
	/* END Has room been allocated? */
	ELSE
	BEGIN
		INSERT INTO ST2StudentAllocations
		( 
			ApplicationID, 
			BlockID, 
			ResidentialRoomID, 
			MoveReasonID, 
			CreationUserID, 
			CreationDate, 
			OfferDate, 
			OfferUserID,  
			OfferCorrespondanceID, 
			AcceptedDate, 
			AcceptedUserID, 
			RejectedDate, 
			RejectedUserID, 
			RejectedReasonID, 
			LicenceCorrespondanceID, 
			RoomPricingID, 
			MovedToAllocationID, 
			WaitingListDate, 
			WaitingListReasonID, 
			WaitingListNotes, 
			ChargesCreated, 
			WaitingListUserId, 
			OfferNotes, 
			OfferExpireDate, 
			MassBatchID, 
			LicenceTemplateID, 
			OfferTemplateID, 
			PricingModelID, 
			AllocationModelID
		)
		(
			SELECT
				SA.ApplicationID,
				RR.BlockID,
				RR.ResidentialRoomID,
				0 AS MoveReasonID,
				1 AS CreationUserID,
				GETDATE() AS CreationDate,
				GETDATE() AS OfferDate,
				1 AS OfferUserID,
				0 AS OfferCorrespondanceID,
				-2 AS AcceptedDate,
				0 AS AcceptedUserID,
				-2 AS RejectedDate, 
				0 AS RejectedUserID, 
				0 AS RejectedReasonID, 
				0 AS LicenceCorrespondanceID, 
				0 AS RoomPricingID, 
				0 AS MovedToAllocationID, 
				-2 AS WaitingListDate, 
				0 AS WaitingListReasonID, 
				'' AS WaitingListNotes, 
				1 AS ChargesCreated, 
				0 AS WaitingListUserId, 
				NULL AS OfferNotes, 
				DATEADD(day, 1, DATEADD(hour, @OfferLength, CAST(FLOOR(CAST(GETDATE() AS money)) AS datetime))) AS OfferExpireDate, 
				0 AS MassBatchID, 
				RC.DefaultWordTemplateID, 
				RC.DefaultOfferTemplateID, 
				PA.PricingModelID, 
				PA.AllocationModelID
			FROM
				ST2StudentApplications SA
				JOIN PreferenceAvailability PA ON PA.PreferenceAvailabilityID = @PreferenceAvailabilityID
				JOIN ResidentialRooms RR ON RR.ResidentialRoomID = @ResidentialRoomID
				JOIN ST2RoomConfig RC ON RC.ResidentialRoomID = RR.ResidentialRoomID AND RC.AcademicYearID = SA.AcademicYearID
			WHERE
				SA.ApplicationID = @ApplicationID
		)

		SET @AllocationID = SCOPE_IDENTITY ()
		IF @@ERROR > 0 GOTO ErrorBlock

		DECLARE @StartDate	datetime,
				@EndDate	datetime,
				@AllocationModelDateID	int

		DECLARE sad CURSOR FOR
		SELECT
			CASE WHEN StartDate < CAST(getdate() AS date) THEN CAST(FLOOR(CAST(GETDATE() AS money)) AS datetime) ELSE StartDate END,
			EndDate,
			AMD.AllocationModelDateID
		FROM
			ST2AllocationModelDates AMD
		WHERE
			AMD.AllocationModelID = @AllocationModelID
		AND AMD.AcademicYearID = @AcademicYearID
		AND AMD.AllocateDuringPeriod = 1
		AND AMD.EndDate >= CAST(getdate() AS date)
		ORDER BY AMD.StartDate

		DECLARE @AllocationDateID	int,
				@InstalmentNo		int

		SET @InstalmentNo = 1

		OPEN sad
		FETCH sad INTO @StartDate, @EndDate, @AllocationModelDateID
		WHILE @@FETCH_STATUS = 0
		BEGIN
			EXEC DBNAME.dbo.NewST2StudentAllocationDates @AllocationDateID OUTPUT, @AllocationID, @StartDate, @EndDate, -2, -2, 0, 0, 0, -2

			IF @AllocationDateID < 1 GOTO ErrorBlock

			INSERT INTO ST2StudentCharges
			(
				KxStudentID, --int (4)
				ApplicationID, --int (4)
				StudentChargeTypeID, --int (4)
				SiteID, --int (4)
				AreaID, --int (4)
				CreationDate, --datetime (8)
				CreationUserID, --int (4)
				DateFrom, --datetime (8)
				DateTo, --datetime (8)
				StatementText, --varchar (255)
				Amount, --money (8)
				VATID, --int (4)
				TotalAmount, --money (8)
				PrintOnStatement, --bit (1)
				InvoiceNumber, --int (4)
				ResidentialRoomID, --int (4)
				SuppressFromInterface, --int (4)
				AllocationDateID, --int (4)
				ExportBatchID, --int (4)
				CreditReasonID, --int (4)
				RoomDamageID, --int (4)
				LinkedInvoiceNumber, --int (4)
				Period, --int (4)
				LineType, --varchar (10)
				TaxFromAccountChargeID, --int (4)
				InstallmentNo, --int (4)
				ContraCharge, --int (4)
				AllocationCharge, --datetime (8)
				DueDate --datetime (8)
			)
			(
				SELECT
						SA.KxStudentID, --int (4)
						SA.ApplicationID, --int (4)
						PMC.StudentChargeTypeID, --int (4)
						B.SiteID, --int (4)
						B.AreaID, --int (4)
						GETDATE() AS CreationDate, --datetime (8)
						1 AS CreationUserID, --int (4)
						SAD.ArrivalDate AS DateFrom, --datetime (8)
						SAD.DepartureDate AS DateTo, --datetime (8)
						SCT.StatementText, --varchar (255)
						ROUND(CASE D.ModelType
							WHEN 'Weekly' THEN (D.Weeks * D.WeeklyAmount) + (D.Extras * D.DailyAmount)
							WHEN 'Nightly' THEN (D.Nights * D.DailyAmount)
							WHEN 'Daily' THEN (D.Days * D.DailyAmount)
							END, 2) AS Amount, --money (8)
						SCT.VATID, --int (4)
						ROUND(ROUND(CASE D.ModelType
							WHEN 'Weekly' THEN (D.Weeks * D.WeeklyAmount) + (D.Extras * D.DailyAmount)
							WHEN 'Nightly' THEN (D.Nights * D.DailyAmount)
							WHEN 'Daily' THEN (D.Days * D.DailyAmount)
							END, 2) * (1 + V.Rate), 2) AS TotalAmount, --money (8)
						1 AS PrintOnStatement, --bit (1)
						0 AS InvoiceNumber, --int (4)
						RR.ResidentialRoomID, --int (4)
						0 AS SuppressFromInterface, --int (4)
						SAD.AllocationDateID, --int (4)
						0 AS ExportBatchID, --int (4)
						0 AS CreditReasonID, --int (4)
						0 AS RoomDamageID, --int (4)
						0 AS LinkedInvoiceNumber, --int (4)
						AMD.PeriodNumber AS Period, --int (4)
						'' AS LineType, --varchar (10)
						0 AS TaxFromAccountChargeID, --int (4)
						@InstalmentNo AS InstallmentNo, --int (4)
						0 AS ContraCharge, --int (4)
						GETDATE() AS AllocationCharge, --datetime (8)
						CASE WHEN AMD.DueDate <> AMD.StartDate THEN AMD.DueDate ELSE SAD.ArrivalDate END AS DueDate --datetime (8)
				FROM
					ST2AllocationModelDates AMD
					JOIN ST2StudentAllocationDates SAD ON SAD.AllocationDateID = @AllocationDateID
					JOIN ST2StudentAllocations SAl ON SAl.AllocationID = SAD.AllocationID
					JOIN ST2StudentApplications SA ON SA.ApplicationID = SAl.ApplicationID
					JOIN ResidentialRooms RR ON RR.ResidentialRoomID = SAl.ResidentialRoomID
					JOIN SiteBlocks B ON B.BlockID = RR.BlockID
					JOIN ST2PricingModelCharges PMC ON PMC.PricingModelID = SAl.PricingModelID AND PMC.AcademicYearID = SA.AcademicYearID AND PMC.StudentChargeTypeID IN (AMD.StudentChargeID1, AMD.StudentChargeID2, AMD.StudentChargeID3, AMD.StudentChargeID4)
					JOIN StudentChargeTypes SCT ON SCT.StudentChargeTypeID = PMC.StudentChargeTypeID
					JOIN VAT V ON V.VATID = SCT.VATID
					JOIN ST2PricingModels PM ON PM.PricingModelID = PMC.PricingModelID
					JOIN
					(
						SELECT
							AllocationModelDateID,
							PMC.PricingModelID,
							DATEDIFF(week, StartDate, EndDate) AS Weeks,
							DATEDIFF(day, StartDate, EndDate) - (DATEDIFF(week, StartDate, EndDate) * 7) AS Extras,
							DATEDIFF(DAY, StartDate, EndDate) AS Nights,
							DATEDIFF(DAY, StartDate, EndDate)+1 AS [Days],
							PMC.WeeklyAmount,
							PMC.DailyAmount,
							PM.ModelType
						FROM
							ST2AllocationModelDates AMD
							JOIN ST2StudentAllocations SAl ON SAl.AllocationID = @AllocationID AND AMD.AllocationModelID = SAl.AllocationModelID
							JOIN ST2PricingModelCharges PMC ON PMC.AcademicYearID = AMD.AcademicYearID AND PMC.StudentChargeTypeID = AMD.StudentChargeID1 AND PMC.PricingModelID = SAl.PricingModelID
							JOIN ST2PricingModels PM ON PM.PricingModelID = PMC.PricingModelID
						WHERE
							AMD.AllocateDuringPeriod = 1
					) D ON D.AllocationModelDateID = AMD.AllocationModelDateID AND D.PricingModelID = SAl.PricingModelID
				WHERE
					AMD.AllocationModelDateID = @AllocationModelDateID
			)
			IF @@ERROR > 0 GOTO ErrorBlock

			SET @InstalmentNo = @InstalmentNo + 1

			FETCH sad INTO @StartDate, @EndDate, @AllocationModelDateID
		END
		CLOSE sad
		DEALLOCATE sad
		IF @@ERROR > 0 GOTO ErrorBlock
	END

	SET @Result = 1

	COMMIT
	GOTO ReturnBlock

ErrorBlock:
	SET @Result = 0

RollbackBlock:
	ROLLBACK

ReturnBlock:
SELECT @Result AS Result
RETURN

GO

/* END C:\TFS\KCF-STUDENT\Branches\Trunk\KCF.Student\Database\Stored Procedures\KxWebCreateBookingSelect.sql */
/* BEGIN C:\TFS\KCF-STUDENT\Branches\Trunk\KCF.Student\Database\Stored Procedures\KxWebCreatePreferenceSelect.sql */

IF EXISTS (SELECT * FROM sys.procedures WHERE object_id = OBJECT_ID(N'dbo.KxWebCreatePreferenceSelect',N'P'))
BEGIN
	DROP PROCEDURE dbo.KxWebCreatePreferenceSelect
END
GO

CREATE PROCEDURE dbo.KxWebCreatePreferenceSelect
			@Parm1 VARCHAR(100), -- ApplicationGUID
			@Parm2 INT, -- PreferenceAvailabilityID
			@Parm3 INT, -- ResidentialRoomID
			@Parm4 INT  -- Not used
		AS


MERGE WebStudentApplicationPReference AS target
USING (SELECT 
		WSA.ApplicationGUID, 
		1 AS PreferenceNumber, 
		PA.SiteID, 
		PA.AreaID, 
		PA.BlockID, 
		PA.BedroomTypeID, 
		PA.SingleSexArea, 
		PA.Smoking, 
		PA.CustomField1, 
		PA.CustomField2, 
		PA.CustomValue1,
		PA.CustomValue2,
		PA.CustomValue3,
		PA.CustomValue4,
		getdate() AS CreationDate,
		0 AS OriginalDescription
	FROM
		WebStudentApplication WSA
		JOIN PreferenceAvailability PA ON PA.PreferenceAvailabilityID = @Parm2
	WHERE
		WSA.ApplicationGUID = @Parm1
	) AS source (ApplicationGUID, PreferenceNumber, SiteID, AreaID, BlockID, BedroomTypeID, SingleSexArea, Smoking, CustomField1, CustomField2, CustomValue1, CustomValue2, CustomValue3, CustomValue4, CreationDate, OriginalDescription)
ON target.ApplicationGUID = source.ApplicationGUID AND target.PreferenceNumber = source.PreferenceNumber
WHEN NOT MATCHED BY TARGET THEN
	INSERT (ApplicationGUID, PreferenceNumber, SiteID, AreaID, BlockID, BedroomTypeID, SingleSexArea, Smoking, CustomField1, CustomField2, CustomValue1, CustomValue2, CustomValue3, CustomValue4, CreationDate, OriginalDescription, [Floor])
	VALUES (ApplicationGUID, PreferenceNumber, SiteID, AreaID, BlockID, BedroomTypeID, SingleSexArea, Smoking, CustomField1, CustomField2, CustomValue1, CustomValue2, CustomValue3, CustomValue4, CreationDate, OriginalDescription, 0)
WHEN MATCHED THEN
	UPDATE SET
		target.SiteID = source.SiteID,
		target.AreaID = source.AreaID,
		target.BlockID = source.BlockID,
		target.BedroomTypeID = source.BedroomTypeID,
		target.SingleSexArea = source.SingleSexArea,
		target.CustomField1 = source.CustomField1,
		target.CustomField2 = source.CustomField2,
		target.CustomValue1 = source.CustomValue1,
		target.CustomValue2 = source.CustomValue2,
		target.CustomValue3 = source.CustomValue3,
		target.CustomValue4 = source.CustomValue4;

SELECT @@ROWCOUNT AS Result
GO

/* END C:\TFS\KCF-STUDENT\Branches\Trunk\KCF.Student\Database\Stored Procedures\KxWebCreatePreferenceSelect.sql */
/* BEGIN C:\TFS\KCF-STUDENT\Branches\Trunk\KCF.Student\Database\Stored Procedures\KxWebFriendshipGroupSelect.sql */

IF EXISTS (SELECT NULL FROM sys.procedures WHERE object_id = OBJECT_ID(N'[dbo].[KXWebFriendshipGroupSelect]'))
BEGIN
	DROP PROCEDURE [dbo].[KXWebFriendshipGroupSelect]
END
GO


CREATE PROCEDURE [dbo].[KXWebFriendshipGroupSelect]
	@ApplicationGuid varchar(50)
AS

SELECT
	WFG.*
FROM
	WebFriendshipGroup WFG
WHERE
	WFG.ApplicationGUID = @ApplicationGuid

GO



/* END C:\TFS\KCF-STUDENT\Branches\Trunk\KCF.Student\Database\Stored Procedures\KxWebFriendshipGroupSelect.sql */
/* BEGIN C:\TFS\KCF-STUDENT\Branches\Trunk\KCF.Student\Database\Stored Procedures\KxWebProcessOBSApplicationSelect.sql */

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KxWebProcessOBSApplicationSelect]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[KxWebProcessOBSApplicationSelect]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON 
GO

CREATE PROCEDURE [dbo].[KxWebProcessOBSApplicationSelect]
(
	@Parm1 VARCHAR(100), -- ApplicationGUID
	@Parm2 varchar(50), -- Application source (Online/OBS)
	@Parm3 INT, -- Not used
	@Parm4 INT  -- Not used
)
AS

--*************************************************************************************************
-- Declare and Initialise Error Logging variables
--*************************************************************************************************
	DECLARE
		@StoredProcName varchar(50),
		@ErrorCode int,
		@ErrorStep varchar (255),
		@ErrorLogInfo varchar(255),
		@SuccessLogInfo varchar(255),
		@Now datetime,
		@NullDate datetime

		SET @StoredProcName = 'KxWebProcessOBSApplicationSelect'
		SET @ErrorCode = 0
		SET @ErrorStep = ''
		SET @ErrorLogInfo = ''
		SET @SuccessLogInfo = ''
		SELECT @Now=GETDATE()
		SET @NullDate = '1899-12-30 00:00:00.000'

--*************************************************************************************************
-- Declare our SP variables
--*************************************************************************************************
	DECLARE 
		@ApplicationGUID varchar(100),
		@ApplicationID int,
		@WebApplicationTypeID int,
		@KxApplicationTypeID int,
		@ApplicationTypeDesc varchar(100),
		@KxStudentID int,
		@Manual bit,
		@WebAddressTypeId int,
		@KxAddressTypeId int,
		@UserGUID varchar(100),
		@Forename varchar(50),
		@Surname varchar(50),
		@DOB datetime,
		@Gender varchar(10),
		@NationalityID int,
		@NewNumber int,
		@StudentId varchar(255),
		@PersonID int,
		@ContactName varchar(255),
		@EmergencyStudentAddressID int,
		@CorrespondenceStudentAddressID int,
		@HomeStudentAddressID int,
		@ApplicationSourceID int,
		@StudentYearDesc varchar(20),
		@StudentYearID int,
		@SmokerYN bit,
		@SitePrefix varchar(3),
		@Pref1SiteID int

--*************************************************************************************************
-- Initialise our variables 
--*************************************************************************************************

		SET @ApplicationID = 0
		SET @WebApplicationTypeID = 0
		SET @KxApplicationTypeID = 0
		SET @ApplicationTypeDesc  = ''
		SET @KxStudentID = 0
		SET @WebAddressTypeId = 0
		SET @KxAddressTypeId  = ''
		SET @UserGUID  = ''
		SET @Forename  = ''
		SET @Surname  = ''
		SET @DOB = @NullDate
		SET @Gender  = ''
		SET @NationalityID = 0
		SET @NewNumber = 0
		SET @StudentId  = ''
		SET @PersonID = 0
		SET @ContactName = ''
		SET @EmergencyStudentAddressID = 0
		SET @CorrespondenceStudentAddressID = 0
		SET @HomeStudentAddressID = 0
		SET @ApplicationSourceID = 0
		SET @StudentYearDesc = ''
		SET @StudentYearID = 0
		SET @SmokerYN = 0
		SET @SitePrefix = ''

		--Rename our passed in parameter to something more meaningful
		SET @ApplicationGUID = @Parm1

		--Default to Manual Allocation 
		SET @Manual = 0

		--Set Application Source to be Online or OBS
		SELECT @ApplicationSourceID=ApplicationSourceID FROM ST2ApplicationSources WHERE Description = @Parm2


--*************************************************************************************************
-- Populate variables from the Web Application record using the ApplicationGUID passed in 
--*************************************************************************************************
	SELECT	
		@KxStudentID = WSA.KxStudentID,
		@UserGUID = WSA.UserGUID,
		@SitePrefix = UPPER(SUBSTRING(S.Name,1,3))
	FROM 
		WebStudentApplication WSA
		JOIN WebStudentApplicationPreference WSAP ON WSAP.ApplicationGUID = WSA.ApplicationGUID AND WSAP.PreferenceNumber = 1
		JOIN Sites S ON S.SiteID = WSAP.SiteID
	WHERE 
		WSA.ApplicationGUID = @ApplicationGUID


--*************************************************************************************************
-- Determine if they are a smoker or not
--*************************************************************************************************

	--Web UDF 12 determines if they are a smoker or not
	IF EXISTS
	(SELECT * FROM WebStudentApplicationUDFValues where ApplicationGUID = @ApplicationGUID AND CustomFieldID = 12 AND UPPER(FieldValue) ='YES')
	SET @SmokerYN = 1


--*************************************************************************************************
-- Begin transaction - we will rollback to here if we fail
--*************************************************************************************************

BEGIN TRANSACTION

--*************************************************************************************************
-- Create a new Student Record
--*************************************************************************************************

	IF ISNULL(@KxStudentID, 0) = 0
	BEGIN
		EXEC kcf.[Internal_CreateKxStudentRecordAndBindToWebUser] @ApplicationGUID = @ApplicationGUID, @UserGUID = @UserGUID, @KxStudentID = @KxStudentID OUTPUT, @Result = @ErrorLogInfo OUTPUT

		IF ISNULL(@KxStudentID, 0) = 0
		BEGIN
			GOTO RollbackBlock
		END
	END


--*************************************************************************************************
-- ADDRESSES - Create various Kx addresses from the web versions
--*************************************************************************************************
	/*
		Web
		AddressType 1 = Correspondence
		AddressType 2 = Home
		AddressType 4 = Emergency
		AddressType 9 = Guarantor

		Kx
		AddressType 1 = Correspondence
		AddressType 2 = Emergency
		AddressType 3 = Guarantor
		AddressType 4 = Home

		ST2Students
		currentstudentaddressid
		homestudentaddressid
		emergencystudentaddressid
		preferredemailaddressid
		preferredsmsaddressid
	*/

	--Do we have an Home Address to transfer? (Web Address Type = 2)

	SET @WebAddressTypeId = 2 --Home Address Type in Web

	IF EXISTS (SELECT * FROM WebStudentAddress WHERE ApplicationGUID = @ApplicationGUID AND AddressTypeID = @WebAddressTypeId AND Address1 IS NOT NULL)
	BEGIN
		SET @KxAddressTypeId = 4 --Home Address Type is 4 in Kx

		--Transfer the Home Address
		INSERT INTO ST2StudentAddresses
		(
			KxStudentID,		AddressTypeID,		Address,			Town,
			County,				Country,			Postcode,			CreationDate,
			CreationUserID,		EmailAddress,		Telephone,			Mobile,				
			Relationship,
			Forename,			
			Surname,			
			EmailID,			Imported,			ValidFrom,			ValidTo
		)
		SELECT
			@KxStudentID,		@KxAddressTypeId,	WSA.Address1,		WSA.Address2,
			WSA.Address3,		WSA.Address4,		WSA.Postcode,		@Now,
			1,					WU.EmailAddress,	WSA.TelephoneNumber,WSA.MobileNumber,
			WSA.FaxNumber AS Relationship,
			--SUBSTRING(ContactName,1,CHARINDEX(' ',ContactName)-1) as Forename, 
			--SUBSTRING(ContactName,CHARINDEX(' ',ContactName)+1,LEN(ContactName)+1-CHARINDEX(' ',ContactName)) as Surname,	
			WU.ForeName,
			WU.SurName,
			0,					0,					@Now,			NULL
		FROM 	WebStudentAddress WSA
		JOIN	WebStudentApplication WA on WA.ApplicationGuid = WSA.ApplicationGUID
		JOIN    WebUser WU on WU.UserGUID = WA.UserGUID
		WHERE	WA.ApplicationGUID = @ApplicationGUID 
		AND		WSA.AddressTypeId = @WebAddressTypeId --Home

		--Jump out if we've errored
		SET @ErrorCode = @@ERROR
		IF @ErrorCode > 0
		BEGIN
			SET @ErrorStep='Step2a - INSERT ST2StudentAddresses (Home)'
			GOTO RollbackBlock
		END

		SET @HomeStudentAddressID = SCOPE_IDENTITY()

		--Now update the student record to reflect this insert
		UPDATE ST2Students
		SET 
			CurrentStudentAddressID = @HomeStudentAddressID,
			HomeStudentAddressID = @HomeStudentAddressID
		WHERE 
			KxStudentID = @KxStudentID

		--Jump out if we've errored
		SET @ErrorCode = @@ERROR
		IF @ErrorCode > 0
		BEGIN
			SET @ErrorStep='Step2b - UPDATE ST2Students (@HomeStudentAddressID)'
			GOTO RollbackBlock
		END

	END

	--Do we have an Correspondence Address to transfer? (Web Address Type = 1)

	SET @WebAddressTypeId = 1 --Correspondence Address Type in Web

	IF EXISTS (SELECT * FROM WebStudentAddress WHERE ApplicationGUID = @ApplicationGUID AND AddressTypeID = @WebAddressTypeId AND Address1 IS NOT NULL)
	BEGIN
		SET @KxAddressTypeId = 1 --Correspondence Address Type is also 1 in Kx

		--Transfer the Correspondence Address
		INSERT INTO ST2StudentAddresses
		(
			KxStudentID,		AddressTypeID,		Address,			Town,
			County,				Country,			Postcode,			CreationDate,
			CreationUserID,		EmailAddress,		Telephone,			Mobile,				
			Relationship,
			Forename,			
			Surname,			
			EmailID,			Imported,			ValidFrom,			ValidTo
		)
		SELECT
			@KxStudentID,		@KxAddressTypeId,	WSA.Address1,		WSA.Address2,
			WSA.Address3,		WSA.Address4,		WSA.Postcode,		@Now,
			1,					WSA.EmailAddress,	WSA.TelephoneNumber,WSA.MobileNumber,
			WSA.FaxNumber AS Relationship,
			--SUBSTRING(ContactName,1,CHARINDEX(' ',ContactName)-1) as Forename, 
			--SUBSTRING(ContactName,CHARINDEX(' ',ContactName)+1,LEN(ContactName)+1-CHARINDEX(' ',ContactName)) as Surname,	
			WU.ForeName,
			WU.SurName,
			0,					0,					@Now,			NULL
		FROM 	WebStudentAddress WSA
		JOIN	WebStudentApplication WA on WA.ApplicationGuid = WSA.ApplicationGUID
		JOIN    WebUser WU on WU.UserGUID = WA.UserGUID
		WHERE	WA.ApplicationGUID = @ApplicationGUID 
		AND		WSA.AddressTypeId = @WebAddressTypeId --Correspondence

		--Jump out if we've errored
		SET @ErrorCode = @@ERROR
		IF @ErrorCode > 0
		BEGIN
			SET @ErrorStep='Step2c - INSERT ST2StudentAddresses (Correspondence)'
			GOTO RollbackBlock
		END

		SET @CorrespondenceStudentAddressID = SCOPE_IDENTITY()

		--Now update the student record to reflect this insert
		UPDATE ST2Students
		SET 
			CurrentStudentAddressID = @CorrespondenceStudentAddressID
		WHERE 
			KxStudentID = @KxStudentID

		--Jump out if we've errored
		SET @ErrorCode = @@ERROR
		IF @ErrorCode > 0
		BEGIN
			SET @ErrorStep='Step2d - UPDATE ST2Students (@CorrespondenceStudentAddressID)'
			GOTO RollbackBlock
		END
	END

	--Do we have an Emergency Address to transfer? (Web Address Type = 4)

	SET @WebAddressTypeId = 4 --Emergency Address Type in Web

	IF EXISTS (SELECT * FROM WebStudentAddress WHERE ApplicationGUID = @ApplicationGUID AND AddressTypeID = @WebAddressTypeId AND Address1 IS NOT NULL)
	BEGIN
		SET @KxAddressTypeId = 2 --Emergency Address Type is 2 in Kx

		--Sort out the Contact Name
		SELECT @ContactName=ISNULL(WSA.ContactName,'') 
		FROM 	WebStudentAddress WSA
		WHERE	WSA.ApplicationGUID = @ApplicationGUID 
		AND		WSA.AddressTypeId = @WebAddressTypeId --Emergency

		IF @ContactName = ''
		BEGIN
			SET @Forename = ''
			SET @Surname = ''
		END
		ELSE
		BEGIN
			IF CHARINDEX(' ',@ContactName) = 0 --No Spaces - put it all in Surname
			BEGIN
				SET @Forename = ''
				SET @Surname = @ContactName
			END
			ELSE --At least 1 Space - put everything before the first space in as Forename, the rest as surname
			BEGIN
				SELECT @Forename=SUBSTRING(@ContactName,1,CHARINDEX(' ',@ContactName)-1) 
				SELECT @Surname=SUBSTRING(@ContactName,CHARINDEX(' ',@ContactName)+1,LEN(@ContactName)+1-CHARINDEX(' ',@ContactName))
			END
		END

		--Transfer the Emergency Address
		INSERT INTO ST2StudentAddresses
		(
			KxStudentID,		AddressTypeID,		Address,			Town,
			County,				Country,			Postcode,			CreationDate,
			CreationUserID,		EmailAddress,		Telephone,			Mobile,				
			Relationship,
			Forename,			
			Surname,			
			EmailID,			Imported,			ValidFrom,			ValidTo
		)
		SELECT
			@KxStudentID,		@KxAddressTypeId,	WSA.Address1,		WSA.Address2,
			WSA.Address3,		WSA.Address4,		WSA.Postcode,		@Now,
			1,					WSA.EmailAddress,	WSA.TelephoneNumber,WSA.MobileNumber,
			WSA.FaxNumber AS Relationship,
			@Forename, 
			@Surname,	
			--WU.ForeName,
			--WU.SurName,
			0,					0,					@Now,			NULL
		FROM 	WebStudentAddress WSA
		JOIN	WebStudentApplication WA on WA.ApplicationGuid = WSA.ApplicationGUID
		JOIN    WebUser WU on WU.UserGUID = WA.UserGUID
		WHERE	WA.ApplicationGUID = @ApplicationGUID 
		AND		WSA.AddressTypeId = @WebAddressTypeId --Emergency

		--Jump out if we've errored
		SET @ErrorCode = @@ERROR
		IF @ErrorCode > 0
		BEGIN
			SET @ErrorStep='Step2e - INSERT ST2StudentAddresses (Emergency)'
			GOTO RollbackBlock
		END

		SET @EmergencyStudentAddressID = SCOPE_IDENTITY()

		--Now update the student record to reflect this insert
		UPDATE ST2Students
		SET EmergencyStudentAddressID = @EmergencyStudentAddressID WHERE KxStudentID = @KxStudentID

		--Jump out if we've errored
		SET @ErrorCode = @@ERROR
		IF @ErrorCode > 0
		BEGIN
			SET @ErrorStep='Step2f - UPDATE ST2Students (@EmergencyStudentAddressID)'
			GOTO RollbackBlock
		END
	END

	--Do we have an Guarantor Address to transfer? (Web Address Type = 9)
	SET @WebAddressTypeId = 9 --Sponsor/Guarantor Address Type in Web

	IF EXISTS (SELECT * FROM WebStudentAddress WHERE ApplicationGUID = @ApplicationGUID AND AddressTypeID = @WebAddressTypeId AND Address1 IS NOT NULL)
	BEGIN
		SET @KxAddressTypeId = 3 --Sponsor/Guarantor Address Type is 3 in Kx

		--Sort out the Contact Name
		SELECT @ContactName=ISNULL(WSA.ContactName,'') 
		FROM 	WebStudentAddress WSA
		WHERE	WSA.ApplicationGUID = @ApplicationGUID 
		AND		WSA.AddressTypeId = @WebAddressTypeId --Emergency

		IF @ContactName = ''
		BEGIN
			SET @Forename = ''
			SET @Surname = ''
		END
		ELSE
		BEGIN
			IF CHARINDEX(' ',@ContactName) = 0 --No Spaces - put it all in Surname
			BEGIN
				SET @Forename = ''
				SET @Surname = @ContactName
			END
			ELSE --At least 1 Space - put everything before the first space in as Forename, the rest as surname
			BEGIN
				SELECT @Forename=SUBSTRING(@ContactName,1,CHARINDEX(' ',@ContactName)-1) 
				SELECT @Surname=SUBSTRING(@ContactName,CHARINDEX(' ',@ContactName)+1,LEN(@ContactName)+1-CHARINDEX(' ',@ContactName))
			END
		END

		--Transfer the Home Address
		INSERT INTO ST2StudentAddresses
		(
			KxStudentID,		AddressTypeID,		Address,			Town,
			County,				Country,			Postcode,			CreationDate,
			CreationUserID,		EmailAddress,		Telephone,			Mobile,				
			Relationship,
			Forename,			
			Surname,			
			EmailID,			Imported,			ValidFrom,			ValidTo
		)
		SELECT
			@KxStudentID,		@KxAddressTypeId,	WSA.Address1,		WSA.Address2,
			WSA.Address3,		WSA.Address4,		WSA.Postcode,		@Now,
			1,					WSA.EmailAddress,	WSA.TelephoneNumber,WSA.MobileNumber,
			WSA.FaxNumber AS Relationship,
			@Forename, 
			@Surname,	
			--WU.ForeName,
			--WU.SurName,
			0,					0,					@Now,			NULL
		FROM 	WebStudentAddress WSA
		JOIN	WebStudentApplication WA on WA.ApplicationGuid = WSA.ApplicationGUID
		JOIN    WebUser WU on WU.UserGUID = WA.UserGUID
		WHERE	WA.ApplicationGUID = @ApplicationGUID 
		AND		WSA.AddressTypeId = @WebAddressTypeId --Guarantor

		--Jump out if we've errored
		SET @ErrorCode = @@ERROR
		IF @ErrorCode > 0
		BEGIN
			SET @ErrorStep='Step2g - INSERT ST2StudentAddresses (Guarantor)'
			GOTO RollbackBlock
		END
	END

	--Finally, update the student's preferred Email to be their current address
	UPDATE ST2Students SET PreferredEmailAddressID = CurrentStudentAddressID WHERE KxStudentID = @KxStudentID

	--Jump out if we've errored
	SET @ErrorCode = @@ERROR
	IF @ErrorCode > 0
	BEGIN
		SET @ErrorStep='Step2h - UPDATE ST2Students (PreferredEmailAddressID)'
		GOTO RollbackBlock
	END


--*************************************************************************************************
-- Determine the Student Year 
--*************************************************************************************************
	
	IF (SELECT StudentYearID FROM WebStudentApplication WHERE ApplicationGUID = @ApplicationGUID) = 0
	BEGIN
		--Web UDF 9 holds what the student entered
		SELECT @StudentYearDesc=FieldValue from WebStudentApplicationUDFValues where ApplicationGUID = @ApplicationGUID AND CustomFieldID = 9

		--The StudentYears table holds data slightly differently
		IF @StudentYearDesc = 'PostGrad' 
		BEGIN
			SELECT @StudentYearID=StudentYearID FROM StudentYears WHERE Description = 'PG'
		END
		ELSE
		BEGIN
			SELECT @StudentYearID=StudentYearID FROM StudentYears WHERE Description = @StudentYearDesc
		END
	END
	ELSE
	BEGIN
		SELECT @StudentYearID = StudentYearID FROM WebStudentApplication WHERE ApplicationGUID = @ApplicationGUID
	END


--*************************************************************************************************
--Insert our web application into Kx
--*************************************************************************************************

IF (SELECT ISNULL(ApplicationID, 0) FROM WebStudentApplication WHERE ApplicationGUID = @ApplicationGUID) = 0
BEGIN
	IF EXISTS (SELECT * FROM ST2ApplicationTypes WHERE Description = 'Online')
	BEGIN
		SELECT @KxApplicationTypeID=ApplicationTypeID FROM ST2ApplicationTypes WHERE Description = 'Online'
	END
	ELSE
	BEGIN
		--Create the Application Type Record
		INSERT INTO ST2ApplicationTypes
			([Description],[LicenceTypeID],[StudentYearID],[CourseTypeID],[Inactive],[WebPublished])
		VALUES ('Online',0,0,0,0,1)
		--Jump out if we've errored
		SET @ErrorCode = @@ERROR
		IF @ErrorCode > 0
		BEGIN
			SET @ErrorStep='Step3.0 - INSERT ST2ApplicationTypes'
			GOTO RollbackBlock
		END
		--Log the ID of the ApplicationType we just created in Kx
		SET @KxApplicationTypeID = SCOPE_IDENTITY()
	END

   	INSERT INTO ST2StudentApplications 
	(
		KxStudentID,		ApplicationDate,	LicenceTypeID,			InactiveReasonID,
		AcademicYearID,		StudentYearID,		CourseTypeID,			CreationDate,
		CreationUserID,		ApplicationTypeID,	ApplicationSourceID,	InactiveUserID,
		InactiveDate,		WaitingListReasonID,WaitingListUserID,		WaitingListDate,
		InactiveNotes,		WaitingListNotes,	ManualAllocationOnly,   PaymentScheduleTypeId, SessionID
	)
	SELECT
		WU.KxStudentID,		@Now,				LicenceTypeID,			0,
		AcademicYearID,		@StudentYearID,		CourseTypeID,			@Now,
		1,			        @KxApplicationTypeID,@ApplicationSourceID,	0,
		@NullDate,			0,					0,						@NullDate,
		'',					'',					@Manual,				PaymentScheduleTypeId, SessionID
	FROM	WebStudentApplication WSA
	JOIN	WebUser WU ON WU.UserGUID = WSA.UserGUID WHERE ApplicationGUID = @ApplicationGUID

	--Jump out if we've errored
	SET @ErrorCode = @@ERROR
	IF @ErrorCode > 0
	BEGIN
		SET @ErrorStep='Step3 - INSERT ST2StudentApplications'
		GOTO RollbackBlock
	END
	
	--Log the ID of the Application we just created in Kx
	SET @ApplicationId = SCOPE_IDENTITY()

--*************************************************************************************************
-- Add the Student Period
--*************************************************************************************************
	INSERT INTO StudentPeriods(PersonStudentID,PeriodNumber,EventModuleID,ApplicationID)
	VALUES (@KxStudentID,1,0,@ApplicationID)

	--Jump out if we've errored
	SET @ErrorCode = @@ERROR
	IF @ErrorCode > 0
	BEGIN
		SET @ErrorStep='Step4 - INSERT StudentPeriods'
		GOTO RollbackBlock
	END

--*************************************************************************************************
-- Update the WebStudentApplication to link it to the Application we just created in Kx
--*************************************************************************************************
	UPDATE WebStudentApplication SET ApplicationID = @ApplicationID, ConfirmedDate = getdate() WHERE ApplicationGUID = @ApplicationGUID

	--Jump out if we've errored
	SET @ErrorCode = @@ERROR
	IF @ErrorCode > 0
	BEGIN
		SET @ErrorStep='Step5 - UPDATE WebStudentApplication'
		GOTO RollbackBlock
	END

--*************************************************************************************************
-- PREFERENCES - Insert all the preferences for this Application into Kx
--*************************************************************************************************
	INSERT INTO ST2StudentPreferences
		(
		ApplicationID,	PreferenceNumber,	SiteID,			AreaID,
		BlockID,		BedroomTypeID,		[Floor],		SingleSexArea,
		Smoking,		CustomField1,		CustomField2,	CustomValue1,
		CustomValue2,	CustomValue3,		CustomValue4,	CreationDate,	CreationUserID
	)
	SELECT 	
		@ApplicationID,	PreferenceNumber,	SiteID,			AreaID,
		BlockID,		BedroomTypeID,		[Floor],		SingleSexArea,
		Smoking,		CustomField1,		CustomField2,	CustomValue1,
		CustomValue2,	CustomValue3, 		CustomValue4,	@Now,		1
	FROM WebStudentApplicationPreference
	WHERE ApplicationGUID = @ApplicationGUID
	ORDER BY PreferenceNumber

	--Jump out if we've errored
	SET @ErrorCode = @@ERROR
	IF @ErrorCode > 0
	BEGIN
		SET @ErrorStep='Step6 - INSERT ST2StudentPreferences'
		GOTO RollbackBlock
	END
END -- CrEATE APPLICATION IF 0
ELSE
BEGIN
	UPDATE
		S
	SET
		S.DOB = WA.DOB, 
		S.Gender = WA.Gender,
		S.NationalityID = WA.NationalityID
	FROM 
		WebStudentApplication WA
		JOIN ST2Students S ON S.KxStudentID = WA.KxStudentID
	WHERE 
		WA.ApplicationGuid = @ApplicationGUID		
END
--*************************************************************************************************
-- UDFs - Insert all the UDFs for this Application into Kx
--*************************************************************************************************
	INSERT INTO CustomFields2Values
	(
		CustomFieldID,
		EntityType,
		EntityID,
		FieldValue
	)
	SELECT
		UM.GermUDF,
		'ST2StudentApplication',
		WSA.ApplicationID,
		SUBSTRING(FieldValue,1,255)
	FROM  WebStudentApplicationUDFValues UDF
	JOIN WebStudentApplication WSA ON WSA.ApplicationGUID = UDF.ApplicationGUID
	JOIN UDFMapping UM ON UM.WebUdf = UDF.CustomFieldId
	WHERE UDF.ApplicationGUID = @ApplicationGUID 
	AND FieldValue IS NOT NULL  AND FieldValue <> '' --Don't bother to transfer blanks
	AND NOT EXISTS (SELECT NULL FROM CustomFields2Values CFV WHERE CFV.CustomFieldID = UM.GERMUDF AND CFV.EntityType = 'ST2StudentApplication' AND CFV.EntityID = WSA.ApplicationID)

	UPDATE
		CFV
	SET
		CFV.FieldValue = SUBSTRING(UDF.FieldValue, 1, 255)
	FROM  WebStudentApplicationUDFValues UDF
	JOIN WebStudentApplication WSA ON WSA.ApplicationGUID = UDF.ApplicationGUID
	JOIN UDFMapping UM ON UM.WebUdf = UDF.CustomFieldId
	JOIN CustomFields2Values CFV ON CFV.CustomFieldID = UM.GERMUDF AND CFV.EntityType = 'ST2StudentApplication' AND CFV.EntityID = WSA.ApplicationID
	WHERE UDF.ApplicationGUID = @ApplicationGUID 
	AND UDF.FieldValue IS NOT NULL  AND UDF.FieldValue <> '' --Don't bother to transfer blanks

	--Jump out if we've errored
	SET @ErrorCode = @@ERROR
	IF @ErrorCode > 0
	BEGIN
		SET @ErrorStep='Step7 - INSERT CustomFields2Values'
		GOTO RollbackBlock
	END

--*************************************************************************************************
-- Payments - Transfer any web payments for this Application into Kx
--*************************************************************************************************

	DECLARE @WebPaymentReasonDesc varchar(50)
	SET @WebPaymentReasonDesc='Deposit'

	DECLARE @OnlinePaymentMethod varchar(50)
	SET @OnlinePaymentMethod = 'Online'

	--Copy across any Payments for this Application that don't already exist into Kx
	IF EXISTS (
				--Check if there are records to transfer - if not, do nothing
				SELECT TOP 1 *
				FROM WebStudentPayment WSP
				JOIN WebStudentApplication WSAP ON WSAP.ApplicationGUID = WSP.ApplicationGUID
				LEFT JOIN ST2StudentAllocations SAL ON SAL.ApplicationID = WSAP.ApplicationID
				LEFT JOIN ResidentialRooms RR ON RR.ResidentialRoomID = SAL.ResidentialRoomID 
				LEFT JOIN SiteBlocks SB ON SB.BlockID = RR.BlockID
				WHERE
					WSP.ApplicationGUID = @ApplicationGUID
				AND 
					WSP.TransactionReference NOT IN (SELECT DISTINCT ReferenceNumber FROM ST2StudentPayments)
				AND 
					WSP.TransactionAmount > 0
				AND
					WSP.PaymentType = 'DEPOSIT'
				)
	BEGIN
		DECLARE @PaymentReasonID int
		DECLARE @PaymentMethodID int

		IF NOT EXISTS (SELECT PaymentReasonID from StudentPaymentReasons WHERE Description = @WebPaymentReasonDesc)
		BEGIN
			--Doesn't exist, create one
			INSERT INTO [StudentPaymentReasons] ([Description],[Inactive],[MemoPayment],[EnableQuestion],[Question],[QuestionHint],[Signed])
			VALUES (@WebPaymentReasonDesc,0,0,0,'','',0)
		END

		IF NOT EXISTS (SELECT PaymentMethodID from PaymentMethods WHERE Description = @OnlinePaymentMethod)
		BEGIN
			--Doesn't exist, create one
			INSERT INTO [PaymentMethods]([Description],[OpeningBalance],[AllowMiscTransfers],[NominalID],[ListOrder],[CreditCard],[Cheque],[HouseLimit],[IndustryTypeDigit],[CardNumberLength],[ShortCut],[PaymentType],[CreditCardGroupID],[AllowPayments],[AllowRefunds],[WebPaymentProviderDescription],[CurrencyID],[Authorise],[DaysLimit])
			VALUES (@OnlinePaymentMethod,0.00,0,1,NULL,0,0,0.00,NULL,NULL,NULL,NULL,NULL,1,1,NULL,NULL,NULL,NULL)
		END
				
		--These will now always exist
		SELECT @PaymentReasonID=PaymentReasonID from StudentPaymentReasons WHERE Description = @WebPaymentReasonDesc
		SELECT @PaymentMethodID=PaymentMethodID from PaymentMethods WHERE Description = @OnlinePaymentMethod

		INSERT INTO ST2StudentPayments
				(ApplicationID,						KxStudentID,			PaymentReasonID,		StatementText,									PaymentMethodID,	SiteID,					AreaID,			  
				ResidentialRoomID, 					CreationDate,			CreationUserID,			ReferenceNumber,								ReferenceDate,		ReceiptNumber,			ExportBatchID, 
				ReleaseExportBatchID,				Amount,					ChequeRequestBatchID,	ReconciledDate,									ReconciledUserID,	SuppressFROMInterface)
		SELECT	DISTINCT 
				WSAP.ApplicationID,					WSAP.KxStudentID,		@PaymentReasonID,		'Deposit',										@PaymentMethodID,	ISNULL(SB.SiteID,0),	ISNULL(SB.AreaID,0), 
				ISNULL(SAL.ResidentialRoomID,0),	@Now,					1,						CAST(WSP.TransactionReference AS varchar(50)),	WSP.PaymentDate,	WSP.WebPaymentId,		0,
				0,									WSP.TransactionAmount,	NULL,					NULL,											NULL,				0 
		FROM WebStudentPayment WSP
		JOIN WebStudentApplication WSAP ON WSAP.ApplicationGUID = WSP.ApplicationGUID
		LEFT JOIN ST2StudentAllocations SAL ON SAL.ApplicationID = WSAP.ApplicationID
		LEFT JOIN ResidentialRooms RR ON RR.ResidentialRoomID = SAL.ResidentialRoomID 
		LEFT JOIN SiteBlocks SB ON SB.BlockID = RR.BlockID
		WHERE 
			WSP.ApplicationGUID = @ApplicationGUID
		AND
			WSP.TransactionReference NOT IN (SELECT DISTINCT ReferenceNumber FROM ST2StudentPayments)
		AND 
			WSP.TransactionAmount > 0
		AND
			WSP.PaymentType = 'DEPOSIT'
	END
	--Jump out if we've errored
	SET @ErrorCode = @@ERROR
	IF @ErrorCode > 0
	BEGIN
		SET @ErrorStep='Step8 - INSERT ST2StudentPayments'
		GOTO RollbackBlock
	END

--*************************************************************************************************
-- Set the SiteID in the Application to be the same as the SiteID for the first preference (Job Ref : 149818) 
--*************************************************************************************************
	SELECT @Pref1SiteID=SiteID FROM ST2StudentPreferences WHERE ApplicationID=@ApplicationID AND PreferenceNumber = 1
	
	UPDATE ST2StudentApplications 
	SET SiteID = @Pref1SiteID 
	WHERE ApplicationID = @ApplicationID
	AND ISNULL(SiteID,0) = 0 --Should always be true but add this check just in case as they import applications

	--Jump out if we've errored
	SET @ErrorCode = @@ERROR
	IF @ErrorCode > 0
	BEGIN
		SET @ErrorStep='Step9 - UPDATE St2StudentApplication (SiteID)'
		GOTO RollbackBlock
	END

--*************************************************************************************************
-- ALL DONE
--*************************************************************************************************
	
SuccessBlock:
	--If we come into this section, everything has worked
	COMMIT TRANSACTION
	SELECT 1 AS Result --Optional - use it to report success/failure back to Workflow
	
	--Only log success if the Profiler flag is ON in WebConfig
	--IF EXISTS (SELECT * FROM WebResources WHERE ApplicationId = 'naff' AND Identifier = 'Profiler' AND ResourceType = 'CONFIG' AND CAST(Data as varchar(1000)) = 'on')
	BEGIN
		SET @SuccessLogInfo = 'Web ApplicationGUID '+CAST(@ApplicationGUID as varchar(100))
		SET @SuccessLogInfo = @SuccessLogInfo+' successfully transferred into Kx as ApplicationID '+CAST(@ApplicationID as varchar(100))
		INSERT INTO WebAudit(ClassName,Identifier,AuditText,TimeStamp)
		VALUES('StoredProc',@StoredProcName,@SuccessLogInfo,@Now)	
	END
	GOTO ReturnBlock

RollbackBlock:
	--If we come into this section, something has failed
	SET @ErrorLogInfo = 'Transfer failed for ApplicationGUID '+CAST(@ApplicationGUID as varchar(100)) 
	SET @ErrorLogInfo = @ErrorLogInfo+'. SQLErrorCode='+CAST(@ErrorCode as varchar(100))
	SET @ErrorLogInfo = @ErrorLogInfo+'. ErrorStep='+@ErrorStep
    ROLLBACK TRANSACTION
    --Now log the failure in Core Exceptions (always)
	INSERT INTO CoreExceptions(ClassName,Method,Error,ExtraInfo,[TimeStamp])
	VALUES('StoredProc',@StoredProcName,'Transaction Rolled Back',@ErrorLogInfo,@Now)	
	GOTO ErrorBlock

ErrorBlock:
	SELECT 0 AS Result --Optional - use it to report success/failure back to Workflow
	GOTO ReturnBlock

ReturnBlock:
	RETURN

GO

/* END C:\TFS\KCF-STUDENT\Branches\Trunk\KCF.Student\Database\Stored Procedures\KxWebProcessOBSApplicationSelect.sql */
/* BEGIN C:\TFS\KCF-STUDENT\Branches\Trunk\KCF.Student\Database\Stored Procedures\WebGetKxStudentIDByUDFValue.sql */

IF EXISTS (SELECT NULL FROM sys.procedures WHERE object_id = OBJECT_ID(N'dbo.WebGetKxStudentIDByUDFValue'))
BEGIN
	DROP PROCEDURE dbo.WebGetKxStudentIDByUDFValue
END
GO

CREATE PROCEDURE dbo.WebGetKxStudentIDByUDFValue
	@UDFFieldName	varchar(50),
	@UDFValue		varchar(50)
AS

SELECT
	CAST(CFV.EntityID as int) AS KxStudentId
FROM
	CustomFields2Values CFV
	JOIN CustomFields2 CF ON CF.CustomFieldID = CFV.CustomFieldID AND CF.FieldName = @UDFFieldName AND CF.Students = 1
WHERE
	CFV.FieldValue = @UDFValue
AND CFV.EntityType = 'ST2Student'

GO

/* END C:\TFS\KCF-STUDENT\Branches\Trunk\KCF.Student\Database\Stored Procedures\WebGetKxStudentIDByUDFValue.sql */
/* BEGIN C:\TFS\KCF-STUDENT\Branches\Trunk\KCF.Student\Database\Stored Procedures\WebGetPreferences.sql */

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[WebGetPreferences]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.WebGetPreferences
END
GO

CREATE  PROCEDURE [dbo].[WebGetPreferences]
	@ApplicationTypeID	int,
	@AcademicYearID		int,
	@Filter1			varchar(50), -- DynamicValue1
	@Filter2			varchar(50), -- DynamicValue2
	@Filter3			varchar(50), -- DynamicValue3
	@Filter4			varchar(50),
	@SessionID			int = 0
AS

SELECT
	PA.PreferenceAvailabilityID AS PreferenceID,
	PA.SiteID,
	COALESCE(PA.SiteName, S.Name, '') AS SiteName,
	PA.AreaID,
	COALESCE(PA.AreaName, A.Description, PA.BlockName, B.Name, '') AS AreaName,
	PA.BlockID,
	COALESCE(PA.BlockName, B.Name, '') AS BlockName,
	PA.BedroomTypeID,
	COALESCE(PA.BedroomTypeName, BT.Description, '') AS BedroomTypeName,
	COALESCE(PA.SingleSexArea, 2) AS SingleSexArea,
	COALESCE(PA.Smoking, 2) AS Smoking,
	COALESCE(PA.CustomField1, 2) AS CustomField1,
	COALESCE(PA.CustomField2, 2) AS CustomField2,
	COALESCE(PA.CustomValue1, '') AS CustomValue1,
	COALESCE(PA.CustomValue2, '') AS CustomValue2,
	COALESCE(PA.CustomValue3, '') AS CustomValue3,
	COALESCE(PA.CustomValue4, '') AS CustomValue4,
	COALESCE(PC.MoreInfo, '<p>No info</p>') AS TempString1,
	COALESCE(PC.Features, '') AS TempString2,
	COALESCE(PC.Images, '') AS TempString3,
	COALESCE(PC.Facilities, '') AS TempString4,
	COALESCE(PA.Thumbnail, '') AS TempString5,
	COALESCE(PA.Price, 999) AS TempDecimal1,
	COALESCE(PA.Price * CAST(PA.ExtraInt1 AS money), 9999) AS TempDecimal2,
	COALESCE(PA.ExtraInt1, 999) AS TempInt1,
	PA.MinPreferences,
	PA.MaxPreferences
FROM
	PreferenceAvailability PA
	LEFT JOIN Sites S ON S.SiteID = PA.SiteID
	LEFT JOIN Areas A ON A.AreaID = PA.AreaID
	LEFT JOIN SiteBlocks B ON B.BlockID = PA.BlockID
	LEFT JOIN BedroomTypes BT ON BT.BedroomTypeID = PA.BedroomTypeID
	LEFT JOIN LocationContent PC ON COALESCE(PC.AreaID, PA.AreaID, 0) = COALESCE(PA.AreaID, 0) AND COALESCE(PC.BlockID, PA.BlockID, 0) = COALESCE(PA.BlockID, 0) AND COALESCE(PC.BedroomTypeID, PA.BedroomTypeID, 0) = COALESCE(PA.BedroomTypeID, 0)
WHERE
	PA.AcademicYearID = @AcademicYearID
AND PA.SessionID = @SessionID
AND	PA.ApplicationTypeID IN (@ApplicationTypeID, 0)
AND @Filter1 = COALESCE(PA.Filter1, @Filter1)
AND @Filter2 = COALESCE(PA.Filter2, @Filter2)
AND @Filter3 = COALESCE(PA.Filter3, @Filter3)
AND @Filter4 = COALESCE(PA.Filter4, @Filter4)
ORDER BY TempDecimal1


GO

/* END C:\TFS\KCF-STUDENT\Branches\Trunk\KCF.Student\Database\Stored Procedures\WebGetPreferences.sql */

/* END C:\TFS\KCF-STUDENT\Branches\Trunk\KCF.Student\Database\5. Procedures.sql */
