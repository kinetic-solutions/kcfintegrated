﻿using Kx.PaymentsSdk.Infrastructure;
using Kx.PaymentsSdk.Mappers;
using Kx.PaymentsSdk.Models;
using System;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("Kx.PaymentsSdk.Tests")]
[assembly: InternalsVisibleTo("DynamicProxyGenAssembly2")]
namespace Kx.PaymentsSdk
{
    /// <summary>
    /// Interface which defines KxPaymentsRequestClient methods
    /// </summary>
    public interface IKxPaymentsRequestClient
    {
        /// <summary>
        /// Main method to create a payment request.
        /// </summary>
        /// <remarks>
        /// Throws ArgumentNullException for null paymentRequest.
        /// </remarks>
        /// <param name="paymentRequest"> payment request object that must be sent</param>
        /// <returns>a url that the browser can redirect the user to make payments</returns>
        Uri NewPaymentRequest(PaymentRequest paymentRequest);

        /// <summary>
        /// Main method to verify whether the given payment was successfully taken from a trusted source.
        /// </summary>
        /// <remarks>
        /// Throws KxPaymentsException, KxPaymentsSignatureException, KxPaymentsEncryptionException for invalid responses.
        /// </remarks>
        /// <param name="paymentResponse">response sent from KxPayments callback in after the payment is completed</param>
        /// <returns>PaymentResponse for a successful verification.        
        /// </returns>
        IPaymentResponse VerifyResponse(PaymentResponseModel paymentResponse);
    }
    /// <summary>
    /// Main class for creating a payment request.
    /// Contains all methods required to make and verify a successful payment
    /// </summary>
    public class KxPaymentsRequestClient : IKxPaymentsRequestClient
    {
        private readonly IPaymentRequestUrlBuilder _newPaymentRequestBuilder;
        private readonly IPaymentResponseVerifier _paymentResponseVerifier;

        /// <summary>
        /// Constructor that takes the PGPConfiguration class as a parameter
        /// </summary>
        /// <param name="pgpConfiguration">Class which holds public, private keys and passphrase</param>
        public KxPaymentsRequestClient(IPGPConfiguration pgpConfiguration)
            : this(
                  new PaymentRequestUrlBuilder(new PaymentRequestToNativePayment(), new Deflater(), new PGPEncrypter(pgpConfiguration)),
                  new PaymentResponseVerifier(new PGPDecrypter(pgpConfiguration)))
        {
        }

        internal KxPaymentsRequestClient(
            IPaymentRequestUrlBuilder paymentRequestUrlBuilder,
            IPaymentResponseVerifier paymentResponseVerifier)
        {
            _newPaymentRequestBuilder = paymentRequestUrlBuilder ?? throw new ArgumentNullException(nameof(paymentRequestUrlBuilder));
            _paymentResponseVerifier = paymentResponseVerifier ?? throw new ArgumentNullException(nameof(paymentResponseVerifier));
        }

        /// <summary>
        /// Main method to create a payment request.
        /// </summary>
        /// <remarks>
        /// Throws ArgumentNullException for null paymentRequest.
        /// </remarks>
        /// <param name="paymentRequest"> payment request object that must be sent</param>
        /// <returns>a url that the browser can redirect the user to make payments</returns>
        public Uri NewPaymentRequest(PaymentRequest paymentRequest) => _newPaymentRequestBuilder.CreateNewRequestUri(paymentRequest);

        /// <summary>
        /// Main method to verify whether the given payment was successfully taken from a trusted source.
        /// </summary>
        /// <remarks>
        /// Throws KxPaymentsException, KxPaymentsSignatureException, KxPaymentsEncryptionException for invalid responses.
        /// </remarks>
        /// <param name="paymentResponse">response sent from KxPayments callback in after the payment is completed</param>
        /// <returns>PaymentResponse for a successful verification.        
        /// </returns>
        public IPaymentResponse VerifyResponse(PaymentResponseModel paymentResponse) => _paymentResponseVerifier.Verify(paymentResponse);
    }    
}