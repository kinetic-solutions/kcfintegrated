﻿namespace Kx.PaymentsSdk.Models
{
    /// <summary>
    /// Model for incoming KxPayments callback response
    /// </summary>
    public class PaymentResponseModel
    {
        /// <summary>
        /// The Base64 encoded response data
        /// </summary>
        public string Data { get; set; }
    }
}
