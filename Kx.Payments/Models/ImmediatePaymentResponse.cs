﻿using System;

namespace Kx.PaymentsSdk.Models
{
    /// <summary>
    /// Class which represents taken immediate payments for this payment
    /// </summary>
    public class ImmediatePaymentResponse : ImmediatePayment
    {

        /// <summary>
        /// Constructor that takes a name and amount to set a new immediate payment
        /// </summary>
        /// <param name="paymentId">immediate KxPayments payment id received</param>
        /// <param name="name"> immediate payment name received</param>
        /// <param name="amount">immediate payment currency received </param>
        public ImmediatePaymentResponse(string paymentId, string name, int amount)
            : base(name, amount)
        {
            PaymentId = paymentId ?? throw new ArgumentNullException(nameof(paymentId));
        }

        /// <summary>
        /// Returns the received KxPayments payment id
        /// </summary>
        public string PaymentId { get; }
    }
}
