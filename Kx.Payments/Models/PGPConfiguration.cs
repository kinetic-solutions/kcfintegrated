﻿using System;
using System.IO;

namespace Kx.PaymentsSdk.Models
{
    /// <summary>
    /// Interface which defines PGP settings
    /// </summary>
    public interface IPGPConfiguration
    {
        /// <summary>
        /// Returns the provided private key for this client
        /// </summary>
        Stream ClientPrivateKey { get; }

        /// <summary>
        /// Returns the provided public key for KxPayments
        /// </summary>
        Stream KxPaymentsPublicKey { get; }

        /// <summary>
        /// Returns the provided private passphrase for this client
        /// </summary>
        string PrivateKeyPassphrase { get; }
    }

    /// <summary>
    /// Main class for configuring PGP keys and passphrase
    /// </summary>
    public class PGPConfiguration : IPGPConfiguration
    {
        /// <summary>
        /// Constructor that takes arguments of PGP keys as streams and the private key passphrase as a string.
        /// </summary>
        /// <remarks>
        ///  Throws ArgumentNullException if there's a null parameter.
        /// </remarks>
        /// <param name="clientPrivateKey">PGP private key as stream</param>
        /// <param name="privateKeyPassphrase">PGP private key passphrase as string</param>
        /// <param name="kxPaymentsPublicKey">PGP public key as stream</param>
        public PGPConfiguration(
            Stream clientPrivateKey,
            string privateKeyPassphrase,
            Stream kxPaymentsPublicKey)
        {
            ClientPrivateKey = clientPrivateKey ?? throw new ArgumentNullException(nameof(clientPrivateKey));
            PrivateKeyPassphrase = privateKeyPassphrase ?? throw new ArgumentNullException(nameof(privateKeyPassphrase));
            KxPaymentsPublicKey = kxPaymentsPublicKey ?? throw new ArgumentNullException(nameof(kxPaymentsPublicKey));
        }

        /// <summary>
        /// Returns the provided private key for this client
        /// </summary>
        public Stream ClientPrivateKey { get; }

        /// <summary>
        /// Returns the provided private passphrase for this client
        /// </summary>
        public string PrivateKeyPassphrase { get; }

        /// <summary>
        /// Returns the provided public key for KxPayments
        /// </summary>
        public Stream KxPaymentsPublicKey { get; }
    }
}
