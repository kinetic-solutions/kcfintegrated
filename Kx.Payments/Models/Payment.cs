﻿namespace Kx.PaymentsSdk.Models
{
    /// <summary>
    /// Class which represents a payment. This is an abstract class and cannot be instantiated
    /// </summary>
    public abstract class Payment
    {
        /// <summary>
        /// Constructor that takes a name and amount to set a new immediate payment
        /// </summary>
        /// <param name="name"> name of the immediate payment</param>
        /// <param name="amount">the smallest unit for the specified currency</param>
        protected Payment(string name, int amount)
        {
            Name = name;
            Amount = amount;
        }

        /// <summary>
        /// Returns the payment name for this immediate payment
        /// </summary>
        public string Name { get; }

        /// <summary>
        /// Returns the payment amount for this immediate payment
        /// </summary>
        public int Amount { get; }
    }
}
