﻿namespace Kx.PaymentsSdk.Models
{
    /// <summary>
    /// Class which represents immediate payments for this payment
    /// </summary>
    public class ImmediatePayment : Payment
    {
        /// <summary>
        /// Constructor that takes a name and amount to set a new immediate payment
        /// </summary>
        /// <param name="name"> name of the immediate payment</param>
        /// <param name="amount">the smallest unit for the specified currency</param>
        public ImmediatePayment(string name, int amount)
            : base(name, amount)
        {
        }
    }
}
