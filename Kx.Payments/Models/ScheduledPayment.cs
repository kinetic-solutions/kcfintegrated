﻿using System;

namespace Kx.PaymentsSdk.Models
{
    /// <summary>
    /// Class which represents a scheduled instalment in KxPayments
    /// </summary>
    public class ScheduledPayment : Payment
    {

        /// <summary>
        /// Returns the date when the instalment will be scheduled to be taken
        /// </summary>
        public DateTime DateToBeTaken { get; }

        /// <summary>
        /// Constructor that takes a payment name, amount and a date to be taken
        /// </summary>
        /// <param name="name"> name of the scheduled payment</param>
        /// <param name="amount">the smallest unit for the specified currency</param>
        /// <param name="dateToBeTaken">when the instalment will be scheduled to be taken</param>
        public ScheduledPayment(string name, int amount, DateTime dateToBeTaken)
            : base(name, amount)
        {
            DateToBeTaken = dateToBeTaken;
        }
    }
}
