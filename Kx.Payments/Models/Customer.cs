﻿using Kx.PaymentsSdk.Schemas.Addresses;
using Kx.PaymentsSdk.Schemas.Names;
using System;

namespace Kx.PaymentsSdk.Models
{
    /// <summary>
    /// Class which holds customer configuration for the payment
    /// </summary>
    public class Customer
    {
        /// <summary>
        /// Constructor that takes customer name, address and email as parameters
        /// </summary>
        /// <param name="name"> customer name schema for this payment</param>
        /// <param name="address">customer address schema for this payment</param>
        /// <param name="emailAddress">customer email for this payment</param>
        /// <param name="id">customer id for this payment</param>
        public Customer(INameSchema name, IAddressSchema address, string emailAddress, string id = null)
        {
            Name = name ?? throw new ArgumentNullException(nameof(name));
            Address = address ?? throw new ArgumentNullException(nameof(address));
            EmailAddress = emailAddress ?? string.Empty;
            Id = id ?? string.Empty;
        }

        /// <summary>
        /// Returns the customer name schema for this payment
        /// </summary>
        public INameSchema Name { get; }

        /// <summary>
        /// Returns the customer address schema for this payment
        /// </summary>
        public IAddressSchema Address { get; }

        /// <summary>
        /// Returns the customer email address this payment
        /// </summary>
        public string EmailAddress { get; }

        /// <summary>
        /// Returns the customer id for this payment
        /// </summary>
        public string Id { get; }
    }
}
