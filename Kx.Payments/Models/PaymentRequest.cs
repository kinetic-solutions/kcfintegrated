﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Kx.PaymentsSdk.Models
{
  /// <summary>
  /// Root required object to make a payment
  /// </summary>
  public class PaymentRequest
  {
    /// <summary>
    /// Contructor that accepts bookingReference, merchantConfiguration, customer, immediatePayments, scheduledPayments and urls as arguments
    /// Throws ArgumentNullException if there's a null parameter
    /// </summary>
    /// <param name="bookingReference">booking reference for the payment</param>
    /// <param name="merchantConfiguration">merchant configuration object for the payment </param>
    /// <param name="customer">customer information object for the payment</param>
    /// <param name="immediatePayments">immediate payments to be taken</param>
    /// <param name="scheduledPayments">payment instalments for later processing</param>
    /// <param name="metadata">key-pair value to be sent on the request</param>
    /// <param name="urls">URLs object to be notified on later actions</param>
    public PaymentRequest(
        string bookingReference,
        MerchantConfiguration merchantConfiguration,
        Customer customer,
        IEnumerable<ImmediatePayment> immediatePayments,
        IEnumerable<ScheduledPayment> scheduledPayments,
        Urls urls,
        IDictionary<string, string> metadata = null)
    {
      BookingReference = bookingReference ?? throw new ArgumentNullException(nameof(bookingReference));
      MerchantConfiguration = merchantConfiguration ?? throw new ArgumentNullException(nameof(merchantConfiguration));
      Customer = customer ?? throw new ArgumentNullException(nameof(customer));
      ImmediatePayments = (immediatePayments ?? throw new ArgumentNullException(nameof(immediatePayments))).ToArray();
      ScheduledPayments = (scheduledPayments ?? throw new ArgumentNullException(nameof(scheduledPayments))).ToArray();
      Urls = urls ?? throw new ArgumentNullException(nameof(urls));
      Metadata = metadata;
    }

    /// <summary>
    /// Returns the provided booking reference for this payment
    /// </summary>
    public string BookingReference { get; }

    /// <summary>
    /// Returns the provided merchant configuration for this payment
    /// </summary>
    public MerchantConfiguration MerchantConfiguration { get; }

    /// <summary>
    /// Returns the provided customer for this payment
    /// </summary>
    public Customer Customer { get; }

    /// <summary>
    /// Returns the immediate payments for this payment
    /// </summary>
    public IReadOnlyList<ImmediatePayment> ImmediatePayments { get; }

    /// <summary>
    /// Returns the scheduled instalments for this payment
    /// </summary>
    public IReadOnlyList<ScheduledPayment> ScheduledPayments { get; }

    /// <summary>
    /// Returns the callback urls for this payment
    /// </summary>
    public Urls Urls { get; }
    
    /// <summary>
    /// Returns the key-value pair metadata for this payment
    /// </summary>    
    public IDictionary<string, string> Metadata { get; }
  }
}
