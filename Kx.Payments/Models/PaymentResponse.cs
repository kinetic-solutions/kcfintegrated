﻿using System;
using System.Collections.Generic;

namespace Kx.PaymentsSdk.Models
{
  /// <summary>
  /// Interface which defines a succesful payment
  /// </summary>
  public interface IPaymentResponse
  {
    /// <summary>
    /// Returns the received booking reference for this payment
    /// </summary>
    string BookingReference { get; }

    /// <summary>
    /// Returns the received KxPayments payment plan id for this payment
    /// </summary>
    string PaymentPlanId { get; }

    /// <summary>
    /// Returns the received immediate payments for this payment
    /// </summary>
    IReadOnlyList<ImmediatePaymentResponse> ImmediatePayments { get; }

    /// <summary>
    /// Returns the key-value pair metadata for this payment
    /// </summary>    
    IDictionary<string, string> Metadata { get; }

    /// <summary>
    /// Returns the received scheduled payments for this payment
    /// </summary>
    IReadOnlyList<ScheduledPayment> ScheduledPayments { get; }
  }

  /// <summary>
  /// Root object for a successful payment
  /// </summary>
  public class PaymentResponse : IPaymentResponse
  {
    /// <summary>
    /// Contructor that accepts bookingReference, paymentPlanId, immediatePayments and scheduledPayments as arguments
    /// <remarks>
    /// Throws ArgumentNullException if there's a null required parameter
    /// </remarks>
    /// </summary>
    /// <param name="bookingReference">booking reference for this payment</param>
    /// <param name="paymentPlanId">KxPayment plan id for this payment</param>
    /// <param name="immediatePayments">immediate payments taken</param>
    /// <param name="scheduledPayments">payment instalments scheduled for later processing</param>        
    /// <param name="metadata">payment instalments scheduled for later processing</param>        
    public PaymentResponse(string bookingReference,
                           string paymentPlanId,
                           IReadOnlyList<ImmediatePaymentResponse> immediatePayments,
                           IReadOnlyList<ScheduledPayment> scheduledPayments,
                           IDictionary<string, string> metadata = null)
    {
      BookingReference = bookingReference ?? throw new ArgumentNullException(nameof(bookingReference));
      PaymentPlanId = paymentPlanId ?? throw new ArgumentNullException(nameof(paymentPlanId));
      ImmediatePayments = immediatePayments ?? throw new ArgumentNullException(nameof(immediatePayments));
      ScheduledPayments = scheduledPayments ?? Array.Empty<ScheduledPayment>();
      Metadata = metadata;
    }

    /// <summary>
    /// Returns the received booking reference for this payment
    /// </summary>
    public string BookingReference { get; }

    /// <summary>
    /// Returns the received KxPayments payment plan id for this payment
    /// </summary>
    public string PaymentPlanId { get; }

    /// <summary>
    /// Returns the key-value pair metadata for this payment
    /// </summary>    
    public IDictionary<string, string> Metadata { get; }

    /// <summary>
    /// Returns the received immediate payments for this payment
    /// </summary>
    public IReadOnlyList<ImmediatePaymentResponse> ImmediatePayments { get; }

    /// <summary>
    /// Returns the received scheduled payments for this payment
    /// </summary>
    public IReadOnlyList<ScheduledPayment> ScheduledPayments { get; }
  }
}
