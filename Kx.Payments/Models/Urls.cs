﻿using System;

namespace Kx.PaymentsSdk.Models
{
    /// <summary>
    /// Class that defines where the user will be redirected and which url KxPayments is going to post back to after the payment is complete
    /// </summary>
    public class Urls
    {
        /// <summary>
        /// Constructor that takes a webhookUrl and a redirectUrl
        /// </summary>
        /// <param name="webhookUrl">Url that KxPayments will post back to after a successful payment is made</param>
        /// <param name="redirectUrl">Url that KxPayments will redirect the user to after a successful payment is made</param>
        public Urls(Uri webhookUrl, Uri redirectUrl)
        {
            WebhookUrl = webhookUrl ?? throw new ArgumentNullException(nameof(webhookUrl));
            RedirectUrl = redirectUrl ?? throw new ArgumentNullException(nameof(redirectUrl));
        }
        
        /// <summary>
        /// Returns the webhookUrl for this payment
        /// </summary>
        public Uri WebhookUrl { get; }

        /// <summary>
        /// Returns the redirectUrl for this payment
        /// </summary>
        public Uri RedirectUrl { get; }
    }
}
