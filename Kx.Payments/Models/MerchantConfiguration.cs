﻿using System;

namespace Kx.PaymentsSdk.Models
{
    /// <summary>
    /// Class which holds merchant configuration for the payment
    /// </summary>
    public class MerchantConfiguration
    {
        /// <summary>
        ///  Contructor that takes the merchant country, currency and a merchantKrn
        /// </summary>
        /// <param name="currency">currency to be used for the payment in ISO 4217 code format</param>
        /// <param name="country">country to be used for the payment in ISO 3166-1 code format</param>
        /// <param name="merchantKrn">Kinetic resource name to be used</param>
        public MerchantConfiguration(string currency, string country, string merchantKrn)
        {
            Currency = currency ?? throw new ArgumentNullException(nameof(currency));
            Country = country ?? throw new ArgumentNullException(nameof(country));
            MerchantKrn = merchantKrn ?? throw new ArgumentNullException(nameof(merchantKrn));
        }
        
        /// <summary>
        /// Returns the merchant currency for this payment
        /// </summary>
        public string Currency { get; }
        
        /// <summary>
        /// Returns the merchant country for this payment
        /// </summary>
        public string Country { get; }

        /// <summary>
        /// Returns the merchant KRN for this payment
        /// </summary>
        public string MerchantKrn { get; }
    }
}
