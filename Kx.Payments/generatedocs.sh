#!/bin/bash
cd $(dirname $0)

set -e

## xmldocmd needs to be installed as a dotnet core global tool (dotnet tool install xmldocmd -g)
if ! [ -x "$(command -v xmldocmd)" ]; then
  echo 'Error: xmldocmd needs to be installed as a dotnet core global tool (run: dotnet tool install xmldocmd -g). See http://ejball.com/XmlDocMarkdown/' >&2
  exit 1
fi

dotnet build

# Generate docs
rm -rf ./docs
xmldocmd ./bin/Debug/netstandard2.0/Kx.PaymentsSdk.dll  --namespace Kx.Payments.Sdk docs/reference

# Hide title on pages as there are already generate headers
grep -rl '' --include \*.md ./docs | xargs sed -i '1s/^/---\nhide_title: true\n---\n/'

# Copy to the docs directory, replacing current docs
rm -rf ../../../dotnetDocumentation/docs/reference
cp -r ./docs/reference ../../../dotnetDocumentation/docs/