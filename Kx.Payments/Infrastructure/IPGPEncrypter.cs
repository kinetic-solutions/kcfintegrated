﻿using System.Threading.Tasks;

namespace Kx.PaymentsSdk.Infrastructure
{
    internal interface IPGPEncrypter
    {
        byte[] EncryptAndSign(byte[] buffer);
    }
}
