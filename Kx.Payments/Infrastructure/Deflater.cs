﻿using System.IO.Compression;
using System;
using System.IO;

namespace Kx.PaymentsSdk.Infrastructure
{
  internal class Deflater : IDeflater
  {
    public byte[] Deflate(byte[] buffer)
    {
      if (buffer == null) throw new ArgumentNullException(nameof(buffer));

      using (var ms = new MemoryStream())
      {
        using (var deflater = new GZipStream(ms, CompressionLevel.Optimal))
          deflater.Write(buffer, 0, buffer.Length);

        return ms.ToArray();
      }
    }
  }
}
