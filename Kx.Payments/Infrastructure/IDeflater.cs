﻿namespace Kx.PaymentsSdk.Infrastructure
{
    internal interface IDeflater
    {
        byte[] Deflate(byte[] buffer);
    }
}