﻿using System;
using System.IO;
using Kx.PaymentsSdk.Models;
using PgpCore;

namespace Kx.PaymentsSdk.Infrastructure
{
    internal class PGPEncrypter : IPGPEncrypter
    {
        private readonly IPGPConfiguration _pgpConfiguration;

        internal PGPEncrypter(IPGPConfiguration pgpConfiguration)
        {
            _pgpConfiguration = pgpConfiguration ?? throw new ArgumentNullException(nameof(pgpConfiguration));
        }

        public byte[] EncryptAndSign(byte[] buffer)
        {
            if (buffer is null)
            {
                throw new ArgumentNullException(nameof(buffer));
            }

            using (var inputStream = new MemoryStream())
            using (var encryptedMessageOutput = new MemoryStream())
            using (var pgp = new PGP())
            {
                inputStream.Write(buffer, 0, buffer.Length);
                inputStream.Position = 0;

                pgp.EncryptStreamAndSign(
                    inputStream,
                    encryptedMessageOutput,
                    _pgpConfiguration.KxPaymentsPublicKey,
                    _pgpConfiguration.ClientPrivateKey,
                    _pgpConfiguration.PrivateKeyPassphrase,
                    false);

                return encryptedMessageOutput.ToArray();
            }
        }

    }
}
