﻿using System.IO;

namespace Kx.PaymentsSdk.Infrastructure
{
    internal interface IPGPDecrypter
    {
        PGPOutcome DecryptAndVerify(Stream input);
    }
}
