﻿using Kx.PaymentsSdk.Exceptions;
using Kx.PaymentsSdk.Models;
using Org.BouncyCastle.Bcpg.OpenPgp;
using Org.BouncyCastle.Utilities.IO;
using System;
using System.IO;

namespace Kx.PaymentsSdk.Infrastructure
{
    internal struct PGPOutcome : IDisposable
    {
        public PGPOutcome(Stream clearMessage, bool validSignature)
        {
            ClearMessage = clearMessage;
            ValidSignature = validSignature;
        }

        public Stream ClearMessage { get; }

        public bool ValidSignature { get; }

        public void Dispose()
        {
            ClearMessage?.Dispose();
        }
    }

    internal class PGPDecrypter : IPGPDecrypter
    {
        private readonly IPGPConfiguration _pgpConfiguration;

        internal PGPDecrypter(IPGPConfiguration pgpConfiguration)
        {
            _pgpConfiguration = pgpConfiguration ?? throw new ArgumentNullException(nameof(pgpConfiguration));
        }

        public PGPOutcome DecryptAndVerify(Stream input)
        {
            if (input is null) throw new ArgumentNullException(nameof(input));
            if (input.Length == 0) throw new ArgumentException("Input stream cannot be empty");

            var signedMessage = DecryptStream(input);
            try
            {
                (PgpOnePassSignature onePassSignature, PgpSignature signature, MemoryStream messagePayloadStream) = DeconstructSignedMessage(signedMessage);

                var verified = VerifySignature(onePassSignature, signature, messagePayloadStream);
                return new PGPOutcome(messagePayloadStream, verified);
            }
            catch (Exception error)
            {
                throw new KxPaymentsSignatureException("Unable to verify PGP message.", error);
            }
        }

        private bool VerifySignature(PgpOnePassSignature onePassSignature, PgpSignature signature, MemoryStream messagePayloadStream)
        {
            PgpPublicKeyRingBundle pgpRing = new PgpPublicKeyRingBundle(PgpUtilities.GetDecoderStream(_pgpConfiguration.KxPaymentsPublicKey));
            PgpPublicKey publicKey = pgpRing.GetPublicKey(onePassSignature.KeyId);

            if (publicKey == null) return false;

            onePassSignature.InitVerify(publicKey);

            messagePayloadStream.Position = 0;
            onePassSignature.Update(messagePayloadStream.ToArray());

            return onePassSignature.Verify(signature);
        }
        private (PgpOnePassSignature onepassSignature, PgpSignature signature, MemoryStream messagePayloadStream) DeconstructSignedMessage(Stream signedMessage)
        {
            signedMessage.Position = 0;
            var decodedStream = PgpUtilities.GetDecoderStream(signedMessage);

            PgpObjectFactory pgpFact = new PgpObjectFactory(decodedStream);

            PgpOnePassSignatureList signatureList = (PgpOnePassSignatureList)pgpFact.NextPgpObject();

            if (signatureList is null) throw new PgpException("Unexpected message format: Cannot find PGP signature message after decryption");

            PgpOnePassSignature onePassSignature = signatureList[0];

            PgpLiteralData messagePayload = (PgpLiteralData)pgpFact.NextPgpObject();
            Stream messagePayloadStream = messagePayload.GetInputStream();

            var outputStream = new MemoryStream();
            messagePayloadStream.CopyTo(outputStream);

            PgpSignatureList signatuteList = (PgpSignatureList)pgpFact.NextPgpObject();
            PgpSignature signature = signatuteList[0];

            return (onePassSignature, signature, outputStream);
        }

        private Stream DecryptStream(Stream encryptedMessage)
        {
            if (encryptedMessage is null) throw new ArgumentNullException(nameof(encryptedMessage));
            if (encryptedMessage.Length == 0) throw new ArgumentException("Cannot verify empty encryptedMessage.");

            try
            {
                var objFactory = new PgpObjectFactory(PgpUtilities.GetDecoderStream(encryptedMessage));
                var pgpSec = new PgpSecretKeyRingBundle(PgpUtilities.GetDecoderStream(_pgpConfiguration.ClientPrivateKey));
                var outputStream = new MemoryStream();
                var pgpObject = objFactory.NextPgpObject() as PgpEncryptedDataList;

                if (pgpObject is null) throw new NullReferenceException("PGP message format is not in an expected format");

                PgpPrivateKey privateKey = null;
                PgpPublicKeyEncryptedData encryptedData = null;
                foreach (PgpPublicKeyEncryptedData privateEncryptedDataObject in pgpObject.GetEncryptedDataObjects())
                {
                    PgpSecretKey pgpSecKey = pgpSec.GetSecretKey(privateEncryptedDataObject.KeyId);

                    privateKey = pgpSecKey?.ExtractPrivateKey(_pgpConfiguration.PrivateKeyPassphrase.ToCharArray());

                    if (privateKey != null)
                    {
                        encryptedData = privateEncryptedDataObject;
                        break;
                    }
                }

                if (privateKey == null)
                    throw new ArgumentException("Key used to encrypt message does not match with the provided private key");

                using (Stream clearMessage = encryptedData.GetDataStream(privateKey))
                {
                    var plainFactory = new PgpObjectFactory(clearMessage);

                    var message = plainFactory.NextPgpObject();

                    if (message is PgpOnePassSignatureList)
                    {
                        message = plainFactory.NextPgpObject();
                    }

                    if (message is PgpLiteralData)
                    {
                        var literalData = (PgpLiteralData)message;

                        Stream unc = literalData.GetInputStream();
                        Streams.PipeAll(unc, outputStream);

                        if (encryptedData.IsIntegrityProtected() && !encryptedData.Verify()) throw new PgpException("Message failed integrity check.");

                    }
                    else if (message is PgpOnePassSignatureList)
                        throw new PgpException("Encrypted message contains a signed message - not literal data.");
                    else
                        throw new PgpException("Message is not a simple encrypted file.");
                }
                return outputStream;
            }
            catch (Exception error)
            {
                throw new KxPaymentsEncryptionException("Unable to decrypt message", error);
            }
        }
    }
}