﻿using Google.Protobuf;
using Kx.PaymentsSdk.Infrastructure;
using Kx.PaymentsSdk.Mappers;
using Kx.PaymentsSdk.Models;
using System;

namespace Kx.PaymentsSdk
{
    internal interface IPaymentRequestUrlBuilder
    {
        Uri CreateNewRequestUri(PaymentRequest paymentRequest);
    }

    internal class PaymentRequestUrlBuilder : IPaymentRequestUrlBuilder
    {
        private const string KxPaymentsGatewayUrl = "https://paymentsgateway.mykinetic.io";

        private readonly IPaymentRequestToNativePayment _paymentRequestToNativePayment;
        private readonly IDeflater _deflater;
        private readonly IPGPEncrypter _pgpEncrypter;

        internal PaymentRequestUrlBuilder(
            IPaymentRequestToNativePayment paymentRequestToNativePayment,
            IDeflater deflater,
            IPGPEncrypter pgpEncrypter)
        {
            _paymentRequestToNativePayment = paymentRequestToNativePayment ?? throw new ArgumentNullException(nameof(paymentRequestToNativePayment));
            _deflater = deflater ?? throw new ArgumentNullException(nameof(deflater));
            _pgpEncrypter = pgpEncrypter ?? throw new ArgumentNullException(nameof(pgpEncrypter));
        }

        public Uri CreateNewRequestUri(PaymentRequest paymentRequest)
        {
            if (paymentRequest is null)
            {
                throw new ArgumentNullException(nameof(paymentRequest));
            }

            var deflated = MapAndDeflatePaymentRequest(paymentRequest);

            var encryptedAndSignedMessage = _pgpEncrypter.EncryptAndSign(deflated);

            return BuildRequestUrl(encryptedAndSignedMessage);
        }

        private byte[] MapAndDeflatePaymentRequest(PaymentRequest paymentRequest)
        {
            var mapped = _paymentRequestToNativePayment.Map(paymentRequest);
            return _deflater.Deflate(mapped.ToByteArray());
        }

        private Uri BuildRequestUrl(byte[] message)
        {
            var b64 = System.Convert.ToBase64String(message);
            var encoded = b64.TrimEnd('=').Replace('+', '-').Replace('/', '_');

            return new Uri($"{KxPaymentsGatewayUrl}/v1/outbound/new?msg={encoded}");
        }
    }
}