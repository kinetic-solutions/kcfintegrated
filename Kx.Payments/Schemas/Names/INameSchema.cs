﻿using System.Collections.Generic;

namespace Kx.PaymentsSdk.Schemas.Names
{
    /// <summary>
    /// Interface which defines the representation of a name schema 
    /// </summary>
    public interface INameSchema
    {
        /// <summary>
        /// Returns schema id for this schema
        /// </summary>
        string SchemaId { get; }

        /// <summary>
        /// Returns the full name representation for this schema
        /// </summary>
        /// <returns></returns>
        IReadOnlyList<string> BuildName();
    }
}
