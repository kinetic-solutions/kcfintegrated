﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace Kx.PaymentsSdk.Schemas.Names
{
    /// <summary>
    /// Class which defines the representation of a default name schema
    /// </summary>
    public class DefaultName : INameSchema
    {
        /// <summary>
        /// Constructor which takes an array of names to build the full name
        /// </summary>
        /// <param name="names">list of names</param>
        public DefaultName(IEnumerable<string> names)
        {
            Names = names ?? throw new ArgumentNullException(nameof(names));
        }

        /// <summary>
        /// Constructor which creates an empty name
        /// </summary>
        public DefaultName()
            : this(Array.Empty<string>())
        {
        }

        /// <summary>
        /// Returns the full name representation for this schema
        /// </summary>
        public IReadOnlyList<string> BuildName() => new ReadOnlyCollection<string>(Names.ToList());

        /// <summary>
        /// Returns collection of provided names
        /// </summary>
        /// <returns></returns>
        public IEnumerable<string> Names { get; }

        /// <summary>
        /// Returns schema id for this schema
        /// </summary>
        public string SchemaId => "DFT";
    }
}
