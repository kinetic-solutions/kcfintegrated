﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Kx.PaymentsSdk.Schemas.Addresses
{
    /// <summary>
    /// Class which defines the representation of a default address schema
    /// </summary>
    public class DefaultAddress : IAddressSchema
    {
        /// <summary>
        /// Constructor which takes an array of addresses, a postcode and a country
        /// </summary>
        /// <param name="addressLines">array of addresses</param>
        /// <param name="postcode">postcode to be used for customer address</param>
        /// <param name="country">country to be used for customer address</param>
        public DefaultAddress(IEnumerable<string> addressLines, string postcode, string country)
        {
            AddressLines = addressLines ?? throw new ArgumentNullException(nameof(addressLines));
            Postcode = postcode ?? string.Empty;
            Country = country ?? string.Empty;
        }

        /// <summary>
        /// Constructor which creates an empty address
        /// </summary>
        public DefaultAddress()
            : this(Array.Empty<string>(), string.Empty, string.Empty)
        {
        }

        /// <summary>
        /// Returns the full address representation for this schema
        /// </summary>
        public IReadOnlyList<string> BuildAddress()
        {
            return new ReadOnlyCollection<string>(AddressLines.Concat(new[] { Country, Postcode }).ToList());
        }

         /// <summary>
        /// Returns collection of provided addresses
        /// </summary>
        public IEnumerable<string> AddressLines { get; }

        /// <summary>
        /// Returns postcode for this address schema
        /// </summary>
        public string Postcode { get; }

        /// <summary>
        /// Returns country for this address schema
        /// </summary>
        public string Country { get; }

        /// <summary>
        /// Returns schema id this address schema
        /// </summary>
        public string SchemaId => "DFT";
    }
}
