﻿using System.Collections.Generic;

namespace Kx.PaymentsSdk.Schemas.Addresses
{
    /// <summary>
    /// Interface which defines the representation of an address schema 
    /// </summary>
    public interface IAddressSchema
    {
        /// <summary>
        /// Returns schema id this address schema
        /// </summary>
        string SchemaId { get; }

        /// <summary>
        /// Returns the full address representation for this schema
        /// </summary>
        IReadOnlyList<string> BuildAddress();
    }
}
