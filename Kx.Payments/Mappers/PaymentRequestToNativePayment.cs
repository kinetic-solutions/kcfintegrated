﻿using Google.Protobuf.WellKnownTypes;
using Kx.PaymentsSdk.Models;
using Kx.PaymentsSdk.ProtocolBuffers;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Kx.PaymentsSdk.Mappers
{
  internal class PaymentRequestToNativePayment : IPaymentRequestToNativePayment
  {
    public NativePayment Map(PaymentRequest paymentRequest)
    {
      if (paymentRequest == null)
      {
        throw new ArgumentNullException(nameof(paymentRequest));
      }

      var payments =
          paymentRequest
              .ImmediatePayments
              .Select(o => new NativePayment.Types.Payment { Name = o.Name, Amount = o.Amount })
              .Concat(paymentRequest
                          .ScheduledPayments
                          .Select(o => new NativePayment.Types.Payment { Name = o.Name, Amount = o.Amount, DateToBeTaken = Timestamp.FromDateTime(o.DateToBeTaken.ToUniversalTime()) }));

      var nativePayment = new NativePayment
      {
        BookingReference = paymentRequest.BookingReference,
        Currency = paymentRequest.MerchantConfiguration.Currency,
        Country = paymentRequest.MerchantConfiguration.Country,
        Merchant = paymentRequest.MerchantConfiguration.MerchantKrn,
        Customer = new NativePayment.Types.Customer
        {
          Names = { paymentRequest.Customer.Name.BuildName() },
          NameSchema = "DFT",
          Addresses = { paymentRequest.Customer.Address.BuildAddress() },
          AddressSchema = "DFT",
          Email = paymentRequest.Customer.EmailAddress,
          Id = paymentRequest.Customer.Id
        },
        Payments = { payments },
        WebhookUrl = paymentRequest.Urls.WebhookUrl.ToString(),
        RedirectUrl = paymentRequest.Urls.RedirectUrl.ToString(),
      };

      if (paymentRequest.Metadata is Dictionary<string, string>)
        nativePayment.Metadata.Add(paymentRequest.Metadata);

      return nativePayment;
    }
  }
}
