﻿using Kx.PaymentsSdk.Models;
using Kx.PaymentsSdk.ProtocolBuffers;

namespace Kx.PaymentsSdk.Mappers
{
    internal interface IPaymentRequestToNativePayment
    {
        NativePayment Map(PaymentRequest paymentRequest);
    }
}