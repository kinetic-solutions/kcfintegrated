﻿using System;

namespace Kx.PaymentsSdk.Exceptions
{
    /// <summary>
    /// Exception thrown when there is a KxPayments SDK exception
    /// </summary>
    public class KxPaymentsException : Exception
    {
        /// <summary>
        /// Constructor to create Exception
        /// </summary>
        public KxPaymentsException() : base()
        {
        }

        /// <summary>
        /// Constructor to create Exception
        /// </summary>
        /// <param name="message">Exception message</param>
        public KxPaymentsException(string message) : base(message)
        {
        }

        /// <summary>
        /// Constructor to create Exception
        /// </summary>
        /// <param name="message">Exception message</param>
        /// <param name="innerException">Inner exception thrown</param>
        public KxPaymentsException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
