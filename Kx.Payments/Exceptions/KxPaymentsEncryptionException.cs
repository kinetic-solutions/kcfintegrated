﻿using System;

namespace Kx.PaymentsSdk.Exceptions
{
    /// <summary>
    /// Exception thrown when there is an error decrypting a KxPayments message
    /// </summary>
    public class KxPaymentsEncryptionException : KxPaymentsException
    {
        private KxPaymentsEncryptionException()
        {
        }

        private KxPaymentsEncryptionException(string message) : base(message)
        {
        }

        /// <summary>
        /// Constructor to create Exception
        /// </summary>
        /// <param name="message">Exception message</param>
        /// <param name="innerException">Inner exception thrown</param>
        public KxPaymentsEncryptionException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
