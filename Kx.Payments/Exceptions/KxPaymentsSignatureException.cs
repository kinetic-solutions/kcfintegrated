﻿using System;

namespace Kx.PaymentsSdk.Exceptions
{
    /// <summary>
    /// Exception thrown when there is an error verifying a KxPayments message signature
    /// </summary>
    public class KxPaymentsSignatureException : KxPaymentsException
    {
        /// <summary>
        /// Constructor to create Exception
        /// </summary>
        /// <param name="message">Exception message</param>
        /// <param name="innerException">Inner exception thrown</param>
        public KxPaymentsSignatureException(string message, Exception innerException) : base(message, innerException)
        {
        }

        private KxPaymentsSignatureException() : base()
        {
        }

        /// <summary>
        /// Constructor to create Exception
        /// </summary>
        /// <param name="message">Exception message</param>
        public KxPaymentsSignatureException(string message) : base(message)
        {
        }
    }
}
