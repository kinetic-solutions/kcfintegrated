﻿using Kx.PaymentsSdk.Exceptions;
using Kx.PaymentsSdk.Infrastructure;
using Kx.PaymentsSdk.Models;
using System;
using System.IO;
using System.Text.Json;

namespace Kx.PaymentsSdk
{
  internal interface IPaymentResponseVerifier
  {
    IPaymentResponse Verify(PaymentResponseModel paymentResponse);
  }

  internal class PaymentResponseVerifier : IPaymentResponseVerifier
  {
    private readonly IPGPDecrypter _pgpDecrypter;

    internal PaymentResponseVerifier(IPGPDecrypter pgpDecrypter)
    {
      _pgpDecrypter = pgpDecrypter ?? throw new ArgumentNullException(nameof(pgpDecrypter));
    }

    public IPaymentResponse Verify(PaymentResponseModel paymentResponse)
    {
      if (paymentResponse is null) throw new ArgumentNullException(nameof(paymentResponse));
      if (string.IsNullOrWhiteSpace(paymentResponse.Data)) throw new ArgumentException($"Parameter {nameof(paymentResponse.Data)} must have a value.");

      using (var input = new MemoryStream(Convert.FromBase64String(paymentResponse.Data)))
      using (var verifiedReponse = _pgpDecrypter.DecryptAndVerify(input))
      {
        if (!verifiedReponse.ValidSignature) throw new KxPaymentsSignatureException("PGP Signature for PGP message is invalid");
        if (verifiedReponse.ClearMessage == null) throw new KxPaymentsSignatureException("Message verification succeeded but there is no clear message");

        using (var ms = new MemoryStream())
        {
          verifiedReponse.ClearMessage.Position = 0;
          verifiedReponse.ClearMessage.CopyTo(ms);
          ms.Position = 0;

          try
          {
            Console.WriteLine(System.Text.Encoding.UTF8.GetString(ms.ToArray()));
            return JsonSerializer.Deserialize<PaymentResponse>(ms.ToArray(), new JsonSerializerOptions() { PropertyNamingPolicy = JsonNamingPolicy.CamelCase });
          }
          catch (Exception error)
          {
            throw new KxPaymentsException($"Unable to deserialise the clear message response:", error);
          }
        }
      }
    }
  }
}
