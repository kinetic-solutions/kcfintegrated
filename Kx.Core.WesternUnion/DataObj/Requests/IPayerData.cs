﻿namespace Kx.Core.WesternUnion.DataObj.Requests
{
    public interface IPayerData 
    {
        /// <summary>
        /// Email address of the remitter
        /// </summary>
        string StudentId { get; set; }
        string PayerFirstName { get; set; }
        string PayerSurName { get; set; }
        string PayerEmail { get; set; }
        string PayerCustom1 { get; set; }
        string PayerCustom2 { get; set; }
        string PayerCustom3 { get; set; }
        string PayerCustom4 { get; set; }
        string PayerCustom5 { get; set; }
        string PayerCustom6 { get; set; }
        string PayerCustom7 { get; set; }
        string PayerCustom8 { get; set; }
        string PayerCustom9 { get; set; }
        string PayerCustom10 { get; set; }
    }
}