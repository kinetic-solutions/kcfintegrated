﻿#region copyright

// =======================================================
// Product:   KCF.Student
// Project:   Kx.Core.WesternUnion
// File:      IQuoteOrderRequestData.cs
// Created:   31/01/2018
// By:        Steve Ashley
// Modified:  31/01/2018
// 
// Copyright © Kinetic Solutions Ltd 2018
// All rights reserved.
// 
// No portion of this file may be re-created, copied or otherwise
// used without the express permission of Kinetic Solutions Ltd.
// =======================================================

#endregion

namespace Kx.Core.WesternUnion.DataObj.Requests
{
    public interface IQuoteOrderRequestData
    {
        #region Fields and Properties
        /// <summary>
        /// The amount being asked for payment in the recipient currency
        /// </summary>
        decimal Amount { get; set; }
        /// <summary>
        /// This should be set to AmountType.SETTLEMENT to ensure the amount being requested is in the settlement currency
        /// </summary>
        AmountTypes AmountType { get; set; }
        /// <summary>
        /// The institutions identifier as supplied by Western Union
        /// </summary>
        string ClientId { get; set; }
        /// <summary>
        /// A UNIQUE reference number for this request transaction.  Leave empty to use internally generated identifier.
        /// This can be the same as the Item Id
        /// </summary>
        string ForeignOrderId { get; set; }
        /// <summary>
        /// A UNIQUE reference number for this request item transaction.  Leave empty to use internally generated identifier.
        /// </summary>
        string ForeignItemId { get; set; }
        /// <summary>
        /// Language code as supplied from the API in the Language query
        /// </summary>
        string Language { get; set; }
        /// <summary>
        /// ISO 4217 code for the currency that the payment is to be made in. Obtained from the payment currencies for a country request.
        /// </summary>
        string PaymentCurrencyCode { get; set; }
        /// <summary>
        /// Code for the method of payment (typically WIRE) as obtained from the PaymentTypesForCurrency request
        /// </summary>
        string PaymentTypeCode { get; set; }

        IPayerData Payer { get; set; }
        /// <summary>
        /// Details of the person who will be making the payment
        /// </summary>
        IRemitterData Remitter { get; set; }
        /// <summary>
        /// Details of the bank where the payment will be made
        /// </summary>
        IAddressData RemitterBank { get; set; }
        /// <summary>
        /// ISO 4217 code for the currency that the money will finally be converted to.  This is normally a fixed value for the institution.
        /// </summary>
        string SettlementCurrencyCode { get; set; }

        #endregion
    }
    
}