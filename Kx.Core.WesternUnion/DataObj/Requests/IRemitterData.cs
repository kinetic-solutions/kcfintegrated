﻿namespace Kx.Core.WesternUnion.DataObj.Requests
{
    public interface IRemitterData : IAddressData
    {
        /// <summary>
        /// Email address of the remitter
        /// </summary>
        string Email { get; set; }
    }
}