﻿namespace Kx.Core.WesternUnion.DataObj.Requests
{
    public interface IPaymentTypesForCurrencyRequestData
    {
        string ClientId { get; }
        string CountryCode { get; }
        string CurrencyCode { get; }
    }
}