﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kx.Core.WesternUnion.DataObj.Requests
{
    public class PaymentTypesForCurrencyRequestData : IPaymentTypesForCurrencyRequestData
    {
        public string ClientId { get; private set; }
        public string CountryCode { get; private set; }
        public string CurrencyCode { get; private set; }

        public PaymentTypesForCurrencyRequestData(string clientId, string countryCode, string currencyCode)
        {
            this.ClientId = clientId;
            this.CountryCode = countryCode;
            this.CurrencyCode = currencyCode;
        }
    }
}
