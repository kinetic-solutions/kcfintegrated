﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kx.Core.WesternUnion.Helpers;

namespace Kx.Core.WesternUnion.DataObj.Requests
{
    public class AddressData : IAddressData
    {
        [SWIFTString]
        public string Name { get; set; }
        [SWIFTString]
        public string Address1 { get; set; }
        [SWIFTString]
        public string Address2 { get; set; }
        [SWIFTString]
        public string City { get; set; }
        [SWIFTString]
        public string State { get; set; }
        [SWIFTString]
        public string Zip { get; set; }
        public string CountryCode { get; set; }
    }
}
