﻿#region copyright

// =======================================================
// Product:   KCF.Student
// Project:   Kx.Core.WesternUnion
// File:      AmountTypes.cs
// Created:   31/01/2018
// By:        Steve Ashley
// Modified:  31/01/2018
// 
// Copyright © Kinetic Solutions Ltd 2018
// All rights reserved.
// 
// No portion of this file may be re-created, copied or otherwise
// used without the express permission of Kinetic Solutions Ltd.
// =======================================================

#endregion

namespace Kx.Core.WesternUnion.DataObj.Requests
{
    public enum AmountTypes
    {
        SETTLEMENT,
        PAYMENT
    };
}