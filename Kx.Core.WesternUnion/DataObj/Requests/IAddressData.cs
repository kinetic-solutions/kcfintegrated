﻿using Kx.Core.WesternUnion.Helpers;

namespace Kx.Core.WesternUnion.DataObj.Requests
{
    public interface IAddressData
    {
        /// <summary>
        /// Address line 1 for the remitter/bank
        /// Max length 30 characters and only supports A-Za-z0-9/?-.,+():
        /// Mandatory
        /// </summary>
        [SWIFTString]
        string Address1 { get; set; }
        /// <summary>
        /// Address line 2 for the remitter/bank
        /// Max length 30 characters and only supports A-Za-z0-9/?-.,+():
        /// </summary>
        [SWIFTString]
        string Address2 { get; set; }
        /// <summary>
        /// Address city for the remitter/bank
        /// Max length 30 characters and only supports A-Za-z0-9/?-.,+():
        /// Mandatory
        /// </summary>
        [SWIFTString]
        string City { get; set; }
        /// <summary>
        /// ISO 3166 code for the country of the remitter/bank.
        /// Mandatory
        /// </summary>
        string CountryCode { get; set; }
        /// <summary>
        /// Name of the remitter/bank
        /// Max length 30 characters and only supports A-Za-z0-9/?-.,+():
        /// Mandatory
        /// </summary>
        [SWIFTString]
        string Name { get; set; }
        /// <summary>
        /// Address state for the remitter/bank
        /// Max length 30 characters and only supports A-Za-z0-9/?-.,+():
        /// Mandatory
        /// </summary>
        [SWIFTString]
        string State { get; set; }
        /// <summary>
        /// Address zip for the remitter/bank
        /// Max length 30 characters and only supports A-Za-z0-9/?-.,+():
        /// </summary>
        [SWIFTString]
        string Zip { get; set; }
    }
}