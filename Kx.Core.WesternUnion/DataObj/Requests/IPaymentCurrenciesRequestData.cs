﻿namespace Kx.Core.WesternUnion.DataObj.Requests
{
    public interface IPaymentCurrenciesRequestData
    {
        string ClientId { get; }
        string CountryCode { get; }
    }
}