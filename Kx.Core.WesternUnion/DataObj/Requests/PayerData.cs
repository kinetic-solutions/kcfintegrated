﻿#region copyright

// =======================================================
// Product:   KCF.Student
// Project:   Kx.Core.WesternUnion
// File:      RemitterDetails.cs
// Created:   31/01/2018
// By:        Steve Ashley
// Modified:  31/01/2018
// 
// Copyright © Kinetic Solutions Ltd 2018
// All rights reserved.
// 
// No portion of this file may be re-created, copied or otherwise
// used without the express permission of Kinetic Solutions Ltd.
// =======================================================

#endregion

using Kx.Core.WesternUnion.Helpers;

namespace Kx.Core.WesternUnion.DataObj.Requests
{
    public class PayerData : IPayerData
    {
        #region Fields and Properties
        public string StudentId { get; set; }
        public string PayerFirstName { get; set; }
        public string PayerSurName { get; set; }
        public string PayerEmail { get; set; }
        public string PayerCustom1 { get; set; }
        public string PayerCustom2 { get; set; }
        public string PayerCustom3 { get; set; }
        public string PayerCustom4 { get; set; }
        public string PayerCustom5 { get; set; }
        public string PayerCustom6 { get; set; }
        public string PayerCustom7 { get; set; }
        public string PayerCustom8 { get; set; }
        public string PayerCustom9 { get; set; }
        public string PayerCustom10 { get; set; }

        #endregion


    }
}