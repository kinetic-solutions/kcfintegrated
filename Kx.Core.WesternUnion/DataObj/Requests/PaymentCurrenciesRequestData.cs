﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kx.Core.WesternUnion.DataObj.Requests
{
    public class PaymentCurrenciesRequestData : IPaymentCurrenciesRequestData
    {
        public string ClientId { get; private set; }
        public string CountryCode { get; private set; }

        public PaymentCurrenciesRequestData(string clientId, string countryCode)
        {
            this.ClientId = clientId;
            this.CountryCode = countryCode;
        }
    }
}
