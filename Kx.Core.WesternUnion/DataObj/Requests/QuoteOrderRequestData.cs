﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kx.Core.WesternUnion.Helpers;

namespace Kx.Core.WesternUnion.DataObj.Requests
{
    public class QuoteOrderRequestData : IQuoteOrderRequestData
    {
        public string ClientId { get; set; }
        [SWIFTString]
        public string ForeignOrderId { get; set; }
        public string SettlementCurrencyCode { get; set; }
        [SWIFTString]
        public string ForeignItemId { get; set; }
        public string PaymentTypeCode { get; set; }
        public string PaymentCurrencyCode { get; set; }
        public decimal Amount { get; set; }
        public AmountTypes AmountType { get; set; } = AmountTypes.SETTLEMENT;
        public string Language { get; set; }
        public IPayerData Payer { get; set; } = new PayerData();
        public IRemitterData Remitter { get; set; } =  new RemitterData();
        public IAddressData RemitterBank { get; set; } = new AddressData();
    }
}
