﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using Kx.Core.WesternUnion.Lookups;

namespace Kx.Core.WesternUnion.DataObj.Responses
{
    public class InstructionLanguage : WesternUnionObj
    {
        public string LanguageCode { get; private set; }
        public string LanguageName { get; private set; }

        public InstructionLanguage(XElement data)
        {
            LanguageCode = data.Value;
            LanguageName = data.Value;
        }

        public InstructionLanguage(XElement data, ISOLanguageLookup lookup)
        {
            LanguageCode = data.Value;
            LanguageName = lookup[LanguageCode];
        }
    }
}
