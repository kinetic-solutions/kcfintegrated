﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Kx.Core.WesternUnion.DataObj.Responses
{
    public sealed class PaymentInstruction : WesternUnionObj
    {
        [WesternUnionResponseField(XmlFieldName = "byteLength", DefaultValue = "0")]
        public int Length { get; private set; }

        [WesternUnionResponseField(XmlFieldName = "pdfBytes", DefaultValue = "B64:")]
        public byte[] PDFBytes { get; internal set; }

        public PaymentInstruction(XElement data)
        : base (data)
        {
            
        }
    }
}
