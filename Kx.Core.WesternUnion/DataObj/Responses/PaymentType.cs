﻿#region copyright

// =======================================================
// Product:   KCF.Student
// Project:   Kx.Core.WesternUnion
// File:      PaymentType.cs
// Created:   17/01/2018
// By:        Steve Ashley
// Modified:  01/02/2018
// 
// Copyright © Kinetic Solutions Ltd 2018
// All rights reserved.
// 
// No portion of this file may be re-created, copied or otherwise
// used without the express permission of Kinetic Solutions Ltd.
// =======================================================

#endregion

#region usings

using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

#endregion

namespace Kx.Core.WesternUnion.DataObj.Responses
{
    public sealed class PaymentType : WesternUnionObj
    {
        #region Fields and Properties

        private List<PaymentCurrency> _currencies = new List<PaymentCurrency>();

        [WesternUnionResponseField(XmlFieldName = "code", DefaultValue = "")]
        public string Code { get; private set; }

        [WesternUnionResponseField(XmlFieldName = "description", DefaultValue = "")]
        public string Description { get; private set; }

        [WesternUnionResponseField(XmlFieldName = "order", DefaultValue = "0")]
        public int Order { get; private set; }

        public IEnumerable<PaymentCurrency> Currencies
        {
            get { return _currencies.AsEnumerable(); }
        }

        #endregion

        #region Constructors

        public PaymentType(XElement data) : base(data)
        {
            foreach (XElement currency in data.Descendants().Where(s => s.Name.LocalName == "currency"))
            {
                _currencies.Add(new PaymentCurrency(currency));
            }
        }

        #endregion
    }
}