﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Kx.Core.WesternUnion.DataObj.Responses
{
    public sealed class OrderStatus : WesternUnionObj
    {
        private const string STR_WUCancelled = "DEAL_CANCELLED";
        private const string STR_WUCompleted = "DEAL_DISBURSED";
        private const string STR_WUExpired = "DEAL_EXPIRED";

        [WesternUnionResponseField(XmlFieldName = "orderId", DefaultValue = "")]
        public string OrderId { get; private set; }

        [WesternUnionResponseField(XmlFieldName = "clientId", DefaultValue = "")]
        public string ClientId { get; private set; }

        [WesternUnionResponseField(XmlFieldName = "foreignOrderId", DefaultValue = "")]
        public string ForeignOrderId { get; private set; }

        [WesternUnionResponseField(XmlFieldName = "item/dealReference", DefaultValue = "")]
        public string DealReference { get; private set; }

        [WesternUnionResponseField(XmlFieldName = "item/status", DefaultValue = "")]
        public string Status { get; private set; }

        [WesternUnionResponseField(XmlFieldName = "item/statusChangeDate", DefaultValue = "")]
        public DateTime StatusChangeDate { get; private set; }

        [WesternUnionResponseField(XmlFieldName = "item/foreignItemId", DefaultValue = "")]
        public string ForeignItemId { get; private set; }

        [WesternUnionResponseField(XmlFieldName = "item/confirmationNumber", DefaultValue = "")]
        public string ConfirmationNumber { get; private set; }

        [WesternUnionResponseField(XmlFieldName = "item/disbursedDate", DefaultValue = "")]
        public DateTime DisbursedDate { get; private set; }

        [WesternUnionResponseField(XmlFieldName = "item/itemId", DefaultValue = "")]
        public string ItemId { get; private set; }

        [WesternUnionResponseField(XmlFieldName = "item/associatedItemId", DefaultValue = "")]
        public string AssociatedItemId { get; private set; }

        [WesternUnionResponseField(XmlFieldName = "item/expectedReceiveCurrency", DefaultValue = "")]
        public string ExpectedReceiveCurrency { get; private set; }

        [WesternUnionResponseField(XmlFieldName = "item/expectedReceiveAmount", DefaultValue = "0")]
        public decimal ExpectedReceiveAmount { get; private set; }

        [WesternUnionResponseField(XmlFieldName = "item/expectedPayoutCurrency", DefaultValue = "")]
        public string ExpectedPayoutCurrency { get; private set; }

        [WesternUnionResponseField(XmlFieldName = "item/expectedPayoutAmount", DefaultValue = "0")]
        public decimal ExpectedPayoutAmount { get; private set; }

        [WesternUnionResponseField(XmlFieldName = "item/actualReceiveCurrency", DefaultValue = "")]
        public string ActualReceiveCurrency { get; private set; }

        [WesternUnionResponseField(XmlFieldName = "item/actualReceiveAmount", DefaultValue = "0")]
        public decimal ActualReceiveAmount { get; private set; }

        [WesternUnionResponseField(XmlFieldName = "item/actualPayoutCurrency", DefaultValue = "")]
        public string ActualPayoutCurrency { get; private set; }

        [WesternUnionResponseField(XmlFieldName = "item/actualPayoutAmount", DefaultValue = "0")]
        public decimal ActualPayoutAmount { get; private set; }

        public KxActionToTake KxAction
        {
            get
            {
                switch (Status)
                {
                    case STR_WUCancelled:
                    case STR_WUExpired:
                        return KxActionToTake.CancelPayment;
                    case STR_WUCompleted:
                        return KxActionToTake.ConfirmedPayment;
                    default:
                        return KxActionToTake.None;
                }
            }
        }

        public decimal KxPaymentAmount
        {
            get { return ActualPayoutAmount; }
        }

        public OrderStatus(XElement data) 
            : base(data)
        {
            
        }
    }
}
