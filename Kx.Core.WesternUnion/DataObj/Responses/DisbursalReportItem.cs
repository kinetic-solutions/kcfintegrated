﻿#region copyright

// =======================================================
// Product:   KCF.Student
// Project:   Kx.Core.WesternUnion
// File:      DisbursalReportDisbursementItem.cs
// Created:   26/02/2018
// By:        Steve Ashley
// Modified:  26/02/2018
// 
// Copyright © Kinetic Solutions Ltd 2018
// All rights reserved.
// 
// No portion of this file may be re-created, copied or otherwise
// used without the express permission of Kinetic Solutions Ltd.
// =======================================================

#endregion

#region usings

using System;
using System.Xml.Linq;

#endregion

namespace Kx.Core.WesternUnion.DataObj.Responses
{
    public class DisbursalReportItem : WesternUnionObj
    {
        #region Fields and Properties

        [WesternUnionResponseField(XmlFieldName = "orderId", DefaultValue = "")]
        public string OrderId { get; private set; }

        [WesternUnionResponseField(XmlFieldName = "dealReference", DefaultValue = "")]
        public string DealReference { get; private set; }

        [WesternUnionResponseField(XmlFieldName = "confirmationNumber", DefaultValue = "")]
        public string ConfirmationNumber { get; private set; }

        [WesternUnionResponseField(XmlFieldName = "disbursedDate", DefaultValue = "1900-01-01")]
        public DateTime DisbursedDate { get; private set; }

        [WesternUnionResponseField(XmlFieldName = "itemId", DefaultValue = "")]
        public string ItemId { get; private set; }

        [WesternUnionResponseField(XmlFieldName = "expectedReceiveCurrency", DefaultValue = "")]
        public string ExpectedReceiveCurrency { get; private set; }

        [WesternUnionResponseField(XmlFieldName = "expectedReceiveAmount", DefaultValue = "0")]
        public decimal ExpectedReceiveAmount { get; private set; }

        [WesternUnionResponseField(XmlFieldName = "expectedPayoutCurrency", DefaultValue = "")]
        public string ExpectedPayoutCurrency { get; private set; }

        [WesternUnionResponseField(XmlFieldName = "expectedPayoutAmount", DefaultValue = "0")]
        public decimal ExpectedPayoutAmount { get; private set; }

        [WesternUnionResponseField(XmlFieldName = "actualReceiveCurrency", DefaultValue = "")]
        public string ActualReceiveCurrency { get; private set; }

        [WesternUnionResponseField(XmlFieldName = "actualReceiveAmount", DefaultValue = "0")]
        public decimal ActualReceiveAmount { get; private set; }

        [WesternUnionResponseField(XmlFieldName = "actualPayoutCurrency", DefaultValue = "")]
        public string ActualPayoutCurrency { get; private set; }

        [WesternUnionResponseField(XmlFieldName = "actualPayoutAmount", DefaultValue = "0")]
        public decimal ActualPayoutAmount { get; private set; }

        #endregion

        #region Constructors

        public DisbursalReportItem(XElement data)
            : base(data)
        {
        }

        #endregion
    }
}