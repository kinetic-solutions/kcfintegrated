﻿#region copyright

// =======================================================
// Product:   KCF.Student
// Project:   Kx.Core.WesternUnion
// File:      QuoteOrder.cs
// Created:   01/02/2018
// By:        Steve Ashley
// Modified:  01/02/2018
// 
// Copyright © Kinetic Solutions Ltd 2018
// All rights reserved.
// 
// No portion of this file may be re-created, copied or otherwise
// used without the express permission of Kinetic Solutions Ltd.
// =======================================================

#endregion

#region usings

using System;
using System.Xml.Linq;

#endregion

namespace Kx.Core.WesternUnion.DataObj.Responses
{
    public sealed class QuoteOrder : WesternUnionObj
    {
        #region Fields and Properties

        [WesternUnionResponseField(XmlFieldName = "foreignOrderId", DefaultValue = "")]
        public string ForeignOrderId { get; private set; }

        [WesternUnionResponseField(XmlFieldName = "orderId", DefaultValue = "")]
        public string OrderId { get; private set; }

        [WesternUnionResponseField(XmlFieldName = "creationDate", DefaultValue = "")]
        public DateTime CreationDate { get; private set; }

        [WesternUnionResponseField(XmlFieldName = "settlementCurrencyCode", DefaultValue = "")]
        public string SettlementCurrencyCode { get; private set; }

        [WesternUnionResponseField(XmlFieldName = "itemsTotal", DefaultValue = "0")]
        public decimal ItemsTotal { get; private set; }

        [WesternUnionResponseField(XmlFieldName = "itemsTotalPrecision", DefaultValue = "2")]
        public int ItemsTotalPrecision { get; private set; }

        [WesternUnionResponseField(XmlFieldName = "serviceChargeTotal", DefaultValue = "0")]
        public decimal ServiceChargeTotal { get; private set; }

        [WesternUnionResponseField(XmlFieldName = "serviceChargeTotalPrecision", DefaultValue = "4")]
        public int ServiceChargeTotalPrecision { get; private set; }

        [WesternUnionResponseField(XmlFieldName = "grandTotal", DefaultValue = "0")]
        public decimal GrandTotal { get; private set; }

        [WesternUnionResponseField(XmlFieldName = "grandTotalPrecision", DefaultValue = "2")]
        public int GrandTotalPrecision { get; private set; }

        [WesternUnionResponseField(XmlFieldName = "item/foreignItemId", DefaultValue = "")]
        public string ForeignItemId { get; private set; }

        [WesternUnionResponseField(XmlFieldName = "item/rateIndicator", DefaultValue = "")]
        public string RateIndicator { get; private set; }

        [WesternUnionResponseField(XmlFieldName = "item/clientRate", DefaultValue = "0")]
        public decimal ClientRate { get; private set; }

        [WesternUnionResponseField(XmlFieldName = "item/clientRateFormat", DefaultValue = "")]
        public string ClientRateFormat { get; private set; }

        [WesternUnionResponseField(XmlFieldName = "item/clientRatePrecision", DefaultValue = "0")]
        public int ClientRatePrecision { get; private set; }

        [WesternUnionResponseField(XmlFieldName = "item/paymentAmount", DefaultValue = "0")]
        public decimal PaymentAmount { get; private set; }

        [WesternUnionResponseField(XmlFieldName = "item/paymentCurrencyCode", DefaultValue = "")]
        public string PaymentCurrencyCode { get; private set; }

        [WesternUnionResponseField(XmlFieldName = "item/paymentAmountPrecision", DefaultValue = "0")]
        public int PaymentAmountPrecision { get; private set; }

        [WesternUnionResponseField(XmlFieldName = "item/settlementAmount", DefaultValue = "0")]
        public decimal SettlementAmount { get; private set; }

        [WesternUnionResponseField(XmlFieldName = "item/settlementAmountPrecision", DefaultValue = "0")]
        public int SettlementAmountPrecision { get; private set; }

        [WesternUnionResponseField(XmlFieldName = "item/feeAmount", DefaultValue = "0")]
        public decimal FeeAmount { get; private set; }

        [WesternUnionResponseField(XmlFieldName = "item/feeAmountInPaymentCurrency", DefaultValue = "0")]
        public decimal FeeAmountInPaymentCurrency { get; private set; }

        #endregion

        #region Constructors

        public QuoteOrder(XElement data)
            : base(data)
        {
        }

        #endregion
    }
}