﻿#region copyright

// =======================================================
// Product:   KCF.Student
// Project:   Kx.Core.WesternUnion
// File:      PaymentCurrency.cs
// Created:   17/01/2018
// By:        Steve Ashley
// Modified:  01/02/2018
// 
// Copyright © Kinetic Solutions Ltd 2018
// All rights reserved.
// 
// No portion of this file may be re-created, copied or otherwise
// used without the express permission of Kinetic Solutions Ltd.
// =======================================================

#endregion

#region usings

using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

#endregion

namespace Kx.Core.WesternUnion.DataObj.Responses
{
    public sealed class PaymentCurrency : WesternUnionObj
    {
        #region Fields and Properties

        private List<PaymentType> _paymentTypes = new List<PaymentType>();

        [WesternUnionResponseField(XmlFieldName = "code", DefaultValue = "")]
        public string Code { get; private set; }

        [WesternUnionResponseField(XmlFieldName = "description", DefaultValue = "")]
        public string Description { get; private set; }

        [WesternUnionResponseField(XmlFieldName = "precision", DefaultValue = "2")]
        public int Precision { get; private set; }

        public IEnumerable<PaymentType> PaymentTypes
        {
            get { return _paymentTypes.AsEnumerable(); }
        }

        #endregion

        #region Constructors

        public PaymentCurrency(XElement data) : base(data)
        {
            foreach (XElement paymentType in data.Descendants().Where(s => s.Name.LocalName == "paymentType"))
            {
                _paymentTypes.Add(new PaymentType(paymentType));
            }
        }

        #endregion
    }
}