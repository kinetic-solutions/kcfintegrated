﻿#region copyright

// =======================================================
// Product:   KCF.Student
// Project:   Kx.Core.WesternUnion
// File:      KxActionToTake.cs
// Created:   12/02/2018
// By:        Steve Ashley
// Modified:  12/02/2018
// 
// Copyright © Kinetic Solutions Ltd 2018
// All rights reserved.
// 
// No portion of this file may be re-created, copied or otherwise
// used without the express permission of Kinetic Solutions Ltd.
// =======================================================

#endregion

namespace Kx.Core.WesternUnion.DataObj.Responses
{
    public enum KxActionToTake
    {
        None,
        CancelPayment,
        ConfirmedPayment
    }
}