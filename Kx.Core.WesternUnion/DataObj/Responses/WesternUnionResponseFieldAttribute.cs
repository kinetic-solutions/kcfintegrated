﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kx.Core.WesternUnion.DataObj.Responses
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = true)]
    public class WesternUnionResponseFieldAttribute : Attribute
    {
        public string XmlFieldName { get; set; }
        public string DefaultValue { get; set; }
    }
}
