﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Kx.Core.WesternUnion.DataObj.Responses
{
    public class LogQuotedOrder : WesternUnionObj
    {
        [WesternUnionResponseField(XmlFieldName = "foreignOrderId", DefaultValue = "")]
        public string ForeignOrderId { get; set; }

        [WesternUnionResponseField(XmlFieldName = "orderId", DefaultValue = "")]
        public string OrderId { get; set; }

        [WesternUnionResponseField(XmlFieldName = "item/foreignItemId", DefaultValue = "")]
        public string ForeignItemId { get; set; }

        [WesternUnionResponseField(XmlFieldName = "item/dealReference", DefaultValue = "")]
        public string DealReference { get; set; }

        [WesternUnionResponseField(XmlFieldName = "item/bankAccount/accountNumber", DefaultValue = "")]
        public string AccountNumber { get; set; }

        [WesternUnionResponseField(XmlFieldName = "item/bankAccount/bankName", DefaultValue = "")]
        public string BankName { get; set; }

        [WesternUnionResponseField(XmlFieldName = "item/bankAccount/bankAddress", DefaultValue = "")]
        public string BankAddress { get; set; }

        [WesternUnionResponseField(XmlFieldName = "item/bankAccount/beneficiaryName", DefaultValue = "")]
        public string BeneficiaryName { get; set; }

        [WesternUnionResponseField(XmlFieldName = "item/bankAccount/beneficiaryAddress", DefaultValue = "")]
        public string BeneficiaryAddress { get; set; }

        [WesternUnionResponseField(XmlFieldName = "item/bankAccount/beneficiaryCountry", DefaultValue = "")]
        public string BeneficiaryCountry { get; set; }

        [WesternUnionResponseField(XmlFieldName = "item/bankAccount/routingCode", DefaultValue = "")]
        public string RoutingCode { get; set; }

        [WesternUnionResponseField(XmlFieldName = "item/bankAccount/swiftCode", DefaultValue = "")]
        public string SwiftCode { get; set; }

        [WesternUnionResponseField(XmlFieldName = "item/bankAccount/intermediaryBankName", DefaultValue = "")]
        public string IntermediaryBankName { get; set; }

        [WesternUnionResponseField(XmlFieldName = "item/bankAccount/intermediaryBankCountry", DefaultValue = "")]
        public string IntermediaryBankCountry { get; set; }

        [WesternUnionResponseField(XmlFieldName = "item/bankAccount/intermediaryBankSwiftCode", DefaultValue = "")]
        public string IntermediaryBankSwiftCode { get; set; }

        public LogQuotedOrder(XElement data) 
            : base(data)
        {
            
        }
    }
}
