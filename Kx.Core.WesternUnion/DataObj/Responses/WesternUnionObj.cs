﻿#region copyright

// =======================================================
// Product:   KCF.Student
// Project:   Kx.Core.WesternUnion
// File:      WesternUnionObj.cs
// Created:   17/01/2018
// By:        Steve Ashley
// Modified:  01/02/2018
// 
// Copyright © Kinetic Solutions Ltd 2018
// All rights reserved.
// 
// No portion of this file may be re-created, copied or otherwise
// used without the express permission of Kinetic Solutions Ltd.
// =======================================================

#endregion

#region usings

using System;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Xml.Linq;
using Kx.Core.Reflection;
using Kx.Core.Utility;

#endregion

namespace Kx.Core.WesternUnion.DataObj.Responses
{
    public abstract class WesternUnionObj
    {
        #region Constructors

        public WesternUnionObj()
        {
        }

        public WesternUnionObj(XElement data)
        {
            RetrieveDataFromObject(data);
        }

        #endregion

        #region Protected Methods

        protected string GetChildNodeValue(XElement element, string nodeName, string defaultValue)
        {
            StringBuilder result = new StringBuilder();
            var elements = element.Elements().Where(s => s.Name.LocalName == nodeName).ToList();
            if (elements.Count == 0)
            {
                result.Append(defaultValue);
            }
            else
            {
                foreach (XElement xElement in elements)
                {
                    result.AppendFormat("{0}{1}", result.Length > 0 ? Environment.NewLine : string.Empty, xElement.Value);
                }
            }

            return result.ToString();
        }

        protected int GetChildNodeValue(XElement element, string nodeName, int defaultValue)
        {
            string stringResult = GetChildNodeValue(element, nodeName, defaultValue.ToString());
            int result = defaultValue;
            int.TryParse(stringResult, out result);
            return result;
        }

        #endregion

        #region Private Methods

        private void RetrieveDataFromObject(XElement data)
        {
            var props = GetType().GetProperties().Where(prop => Attribute.IsDefined(prop, typeof(WesternUnionResponseFieldAttribute)));
            foreach (PropertyInfo prop in props)
            {
                var attr = prop.GetCustomAttribute<WesternUnionResponseFieldAttribute>();

                XElement localData = data;

                string fieldName = attr.XmlFieldName;

                if (attr.XmlFieldName.Contains("/"))
                {
                    localData = GetSubChild(ref fieldName, localData);
                }

                object objVal = GetChildNodeValue(localData, fieldName, attr.DefaultValue);
                if (prop.PropertyType == typeof(byte[]))
                {
                    objVal = KxBase64.Base64DecodeToByte(objVal.ToString());
                }

                KxReflection.SetProperty(this, prop.Name, objVal);
            }
        }

        private XElement GetSubChild(ref string fieldName, XElement parentElement)
        {
            if (fieldName.Contains("/"))
            {
                string childName = fieldName.Substring(0, fieldName.IndexOf("/"));
                fieldName = fieldName.Substring(childName.Length + 1, fieldName.Length - childName.Length - 1);
                XElement childElement = parentElement.Elements().Where(s => s.Name.LocalName == childName).FirstOrDefault();
                if (childElement != null)
                {
                    return GetSubChild(ref fieldName, childElement);
                }
            }

            return parentElement;
        }

        #endregion
    }
}