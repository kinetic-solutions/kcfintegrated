﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kx.Core.WesternUnion
{
    public sealed class WesternUnionException : Exception
    {
        public WesternUnionException()
            : base()
        {

        }

        public WesternUnionException(string message)
            : base(message)
        {

        }
    }
}
