﻿#region copyright

// =======================================================
// Product:   KCF.Student
// Project:   Kx.Core.WesternUnion
// File:      WesternUnionVerb.cs
// Created:   16/01/2018
// By:        Steve Ashley
// Modified:  16/01/2018
// 
// Copyright © Kinetic Solutions Ltd 2018
// All rights reserved.
// 
// No portion of this file may be re-created, copied or otherwise
// used without the express permission of Kinetic Solutions Ltd.
// =======================================================

#endregion

namespace Kx.Core.WesternUnion
{
    /// <summary>
    /// The Http VERBs supported by the API
    /// </summary>
    public enum WesternUnionVerb
    {
        POST,
        PUT,
        DELETE
    }
}