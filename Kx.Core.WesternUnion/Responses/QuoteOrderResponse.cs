﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using Kx.Core.WesternUnion.DataObj.Responses;

namespace Kx.Core.WesternUnion.Responses
{
    public class QuoteOrderResponse : WesternUnionResponse
    {
        public QuoteOrder Quote { get; private set; }
        internal QuoteOrderResponse(string rawXml) : base(rawXml)
        {
        }

        protected override void LoadResponseData()
        {
            var quotes = from e in XmlDocument.Descendants()
                where e.Name.LocalName == "quoteOrderResponse"
                select e;

            foreach (XElement quote in quotes)
            {
                Quote = new QuoteOrder(quote);
            }
        }
    }
}
