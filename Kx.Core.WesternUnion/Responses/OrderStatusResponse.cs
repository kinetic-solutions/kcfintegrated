﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using Kx.Core.WesternUnion.DataObj.Responses;

namespace Kx.Core.WesternUnion.Responses
{
    public class OrderStatusResponse : WesternUnionResponse
    {
        public OrderStatus OrderStatus { get; private set; }
        internal OrderStatusResponse(string rawXml) 
            : base(rawXml)
        {
        }

        protected override void LoadResponseData()
        {
            var orders = from e in XmlDocument.Descendants()
                where e.Name.LocalName == "order"
                select e;

            foreach (XElement order in orders)
            {
                OrderStatus = new OrderStatus(order);
            }
        }
    }
}
