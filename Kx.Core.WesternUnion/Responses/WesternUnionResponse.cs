﻿#region copyright

// =======================================================
// Product:   KCF.Student
// Project:   Kx.Core.WesternUnion
// File:      WesternUnionResponse.cs
// Created:   16/01/2018
// By:        Steve Ashley
// Modified:  17/01/2018
// 
// Copyright © Kinetic Solutions Ltd 2018
// All rights reserved.
// 
// No portion of this file may be re-created, copied or otherwise
// used without the express permission of Kinetic Solutions Ltd.
// =======================================================

#endregion

#region usings

using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

#endregion

namespace Kx.Core.WesternUnion.Responses
{
    public abstract class WesternUnionResponse
    {
        #region Fields and Properties

        public bool Successful { get; private set; }
        public List<string> Messages { get; } = new List<string>();
        public string RawXml { get; private set; }

        private XDocument _xmlDocument = null;

        public XDocument XmlDocument
        {
            get
            {
                if (_xmlDocument == null)
                {
                    _xmlDocument = XDocument.Parse(RawXml);
                }

                return _xmlDocument;
            }
        }

        #endregion

        #region Constructors

        internal protected WesternUnionResponse(string rawXml)
        {
            RawXml = rawXml;
            LoadResponse();
            LoadResponseData();
        }

        #endregion

        #region Protected Methods

        protected abstract void LoadResponseData();

        #endregion

        #region Private Methods

        private void LoadResponse()
        {
            var successful = from n in XmlDocument.Descendants()
                where StringComparer.InvariantCultureIgnoreCase.Compare(n.Name.LocalName, "status") == 0
                select n;

            foreach (XElement element in successful)
            {
                if (DetermineSuccessProcessed(element))
                {
                    if (!Successful)
                    {
                        LoadMessages(element);
                    }

                    break;
                }
            }
        }

        private void LoadMessages(XElement element)
        {
            var errors = from e in element.Descendants()
                where StringComparer.InvariantCultureIgnoreCase.Compare(e.Name.LocalName, "error") == 0
                select e;

            foreach (XElement xElement in errors)
            {
                Messages.Add(xElement.Value);
            }
        }

        private bool DetermineSuccessProcessed(XElement element)
        {
            var attr = element.Attribute("success");
            if (attr != null)
            {
                if (StringComparer.InvariantCultureIgnoreCase.Compare(attr.Value, bool.TrueString) == 0)
                {
                    Successful = true;
                }
                else
                {
                    Successful = false;
                }

                return true;
            }

            return false;
        }

        #endregion
    }
}