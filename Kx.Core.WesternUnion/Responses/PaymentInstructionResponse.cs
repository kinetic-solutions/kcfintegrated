﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using Kx.Core.Utility;
using Kx.Core.WesternUnion.DataObj.Responses;

namespace Kx.Core.WesternUnion.Responses
{
    public class PaymentInstructionResponse : WesternUnionResponse
    {
        public PaymentInstruction Instruction { get; private set; }

        internal PaymentInstructionResponse(string rawXml) : base(rawXml)
        {
        }

        protected override void LoadResponseData()
        {
            var pdfData = from e in XmlDocument.Descendants()
                where e.Name.LocalName == "pdfData"
                select e;

            foreach (XElement data in pdfData)
            {
                Instruction = new PaymentInstruction(data);
            }
        }
    }
}
