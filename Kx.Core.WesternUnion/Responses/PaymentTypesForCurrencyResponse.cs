﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using Kx.Core.WesternUnion.DataObj.Responses;

namespace Kx.Core.WesternUnion.Responses
{
    public class PaymentTypesForCurrencyResponse : WesternUnionResponse
    {
        private List<PaymentType> _paymentTypes = new List<PaymentType>();

        public IEnumerable<PaymentType> PaymentTypes
        {
            get { return _paymentTypes.AsEnumerable(); }
        }

        internal PaymentTypesForCurrencyResponse(string rawXml) 
            : base(rawXml)
        {
        }

        protected override void LoadResponseData()
        {
            var paymentTypes = from e in XmlDocument.Descendants()
                where e.Name.LocalName == "paymentType"
                select e;

            foreach (XElement paymentType in paymentTypes)
            {
                _paymentTypes.Add(new PaymentType(paymentType));
            }
        }
    }
}
