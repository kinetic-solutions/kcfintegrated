﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using Kx.Core.WesternUnion.DataObj.Responses;

namespace Kx.Core.WesternUnion.Responses
{
    public class DisbursalReportResponse : WesternUnionResponse
    {
        private List<DisbursalReportItem> _items = new List<DisbursalReportItem>();

        public IReadOnlyCollection<DisbursalReportItem> Items
        {
            get { return _items.AsReadOnly(); }
        }
        internal DisbursalReportResponse(string rawXml) 
            : base(rawXml)
        {
        }

        protected override void LoadResponseData()
        {
            var items = from e in XmlDocument.Descendants()
                where e.Name.LocalName == "item"
                select e;

            foreach (XElement item in items)
            {
                _items.Add(new DisbursalReportItem(item));
            }
        }
    }
}
