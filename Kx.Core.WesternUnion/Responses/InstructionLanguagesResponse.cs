﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using Kx.Core.WesternUnion.DataObj.Responses;
using Kx.Core.WesternUnion.Lookups;

namespace Kx.Core.WesternUnion.Responses
{
    public class InstructionLanguagesResponse : WesternUnionResponse
    {
        private List<InstructionLanguage> _languages = new List<InstructionLanguage>();

        public IEnumerable<InstructionLanguage> Languages
        {
            get { return _languages.AsEnumerable(); }
        }

        internal InstructionLanguagesResponse(string rawXml) 
            : base(rawXml)
        {
        }

        protected override void LoadResponseData()
        {
            var languages = from e in XmlDocument.Descendants()
                where e.Name.LocalName == "languageCode"
                select e;

            var lookup = new ISOLanguageLookup();

            foreach (XElement language in languages)
            {
                _languages.Add(new InstructionLanguage(language, lookup));
            }
        }
    }
}
