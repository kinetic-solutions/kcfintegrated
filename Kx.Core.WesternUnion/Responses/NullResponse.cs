﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kx.Core.WesternUnion.Responses
{
    public class NullResponse : WesternUnionResponse
    {
        internal NullResponse(string message)
        : base(message.Contains("<") && message.Contains("/>") ? message : string.Format("<null><status success=\"false\"><error code=\"Kx\">{0}</error></status></null>", message))
        {
            
        }

        protected override void LoadResponseData()
        {
            // Do nothing... :)
        }
    }
}
