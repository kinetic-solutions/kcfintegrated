﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using Kx.Core.WesternUnion.DataObj.Responses;

namespace Kx.Core.WesternUnion.Responses
{
    public class PaymentCurrenciesResponse : WesternUnionResponse
    {
        private List<PaymentCurrency> _currencies = new List<PaymentCurrency>();

        public IEnumerable<PaymentCurrency> Currencies
        {
            get { return _currencies.AsEnumerable(); }
        }

        internal PaymentCurrenciesResponse(string rawXml) 
            : base(rawXml)
        {
        }

        protected override void LoadResponseData()
        {
            var currencies = from e in XmlDocument.Descendants()
                where e.Name.LocalName == "paymentCurrency"
                select e;

            foreach (XElement currency in currencies)
            {
                _currencies.Add(new PaymentCurrency(currency));
            }
        }
    }
}
