﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kx.Core.WesternUnion
{
    public enum AuditLevel
    {
        Info,
        Error,
        Debug
    }
}
