﻿#region copyright

// =======================================================
// Product:   KCF.Student
// Project:   Kx.Core.WesternUnion
// File:      LogQuotedOrderRequest.cs
// Created:   06/02/2018
// By:        Steve Ashley
// Modified:  06/02/2018
// 
// Copyright © Kinetic Solutions Ltd 2018
// All rights reserved.
// 
// No portion of this file may be re-created, copied or otherwise
// used without the express permission of Kinetic Solutions Ltd.
// =======================================================

#endregion

#region usings

using System;
using Kx.Core.WesternUnion.DatabaseObjects;
using Kx.Core.WesternUnion.Responses;

#endregion

namespace Kx.Core.WesternUnion.Requests
{
    public class DisbursalReportRequest : WesternUnionRequest
    {
        #region Fields and Properties

        private const string STR_XmlDefinition = "IF1701disbursalReport";
        private const string STR_ServiceURI = "disbursalReport";

        private DateTime _startDate;
        private DateTime _endDate;

        #endregion

        #region Constructors

        public DisbursalReportRequest(DateTime startDate, DateTime endDate, WriteLogRecord logger)
            : base(STR_ServiceURI, WesternUnionVerb.POST, STR_XmlDefinition, logger)
        {
            AddRequestMergeObject("startDate", startDate);
            AddRequestMergeObject("endDate", endDate);
            _startDate = startDate;
            _endDate = endDate;
        }

        public DisbursalReportRequest(DateTime startDate, DateTime endDate, IWesternUnionDatabaseFactory databaseFactory, WriteLogRecord logger)
            : base(STR_ServiceURI, WesternUnionVerb.POST, databaseFactory, STR_XmlDefinition, logger)
        {
            AddRequestMergeObject("startDate", startDate);
            AddRequestMergeObject("endDate", endDate);
            _startDate = startDate;
            _endDate = endDate;
        }

        #endregion

        #region Protected Methods

        protected override bool Validate()
        {
            return _startDate <= _endDate;
        }

        internal override void MessageSent(string messageContent)
        {
            _logger(AuditLevel.Info, STR_XmlDefinition, messageContent, "OUT");
        }


        protected override void MessageReceived(string messageContent)
        {
            _logger(AuditLevel.Info, STR_XmlDefinition, messageContent, "IN");
        }

        #endregion

        #region Internal Methods

        internal override WesternUnionResponse GetResponse(string responseXml)
        {
            MessageReceived(responseXml);
            var response = new DisbursalReportResponse(responseXml);

            return response;
        }
        

        #endregion
    }
}