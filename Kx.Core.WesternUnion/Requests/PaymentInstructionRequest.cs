﻿#region copyright

// =======================================================
// Product:   KCF.Student
// Project:   Kx.Core.WesternUnion
// File:      LogQuotedOrderRequest.cs
// Created:   06/02/2018
// By:        Steve Ashley
// Modified:  06/02/2018
// 
// Copyright © Kinetic Solutions Ltd 2018
// All rights reserved.
// 
// No portion of this file may be re-created, copied or otherwise
// used without the express permission of Kinetic Solutions Ltd.
// =======================================================

#endregion

#region usings

using System;
using System.Linq;
using System.Xml.Linq;
using Kx.Core.Utility;
using Kx.Core.WesternUnion.DatabaseObjects;
using Kx.Core.WesternUnion.Responses;

#endregion

namespace Kx.Core.WesternUnion.Requests
{
    public class PaymentInstructionRequest : WesternUnionRequest
    {
        #region Fields and Properties

        private const string STR_XmlDefinition = "IF1001paymentInstruction";
        private const string STR_ServiceURI = "paymentInstruction";

        private IKxWesternUnionOrder _order = null;

        #endregion

        #region Constructors

        public PaymentInstructionRequest(int kxOrderId, WriteLogRecord logger)
            : base(STR_ServiceURI, WesternUnionVerb.POST, STR_XmlDefinition, logger)
        {
            _order = DatabaseFactory.GetOrderByOrderId(kxOrderId);
            AddRequestMergeObject("request", _order);
        }

        public PaymentInstructionRequest(int kxOrderId, IWesternUnionDatabaseFactory databaseFactory, WriteLogRecord logger)
            : base(STR_ServiceURI, WesternUnionVerb.POST, databaseFactory, STR_XmlDefinition, logger)
        {
            _order = databaseFactory.GetOrderByOrderId(kxOrderId);
            AddRequestMergeObject("request", _order);
        }

        #endregion

        #region Protected Methods

        protected override bool Validate()
        {
            return !string.IsNullOrWhiteSpace(_order.WuDealReference);
        }

        internal override void MessageSent(string messageContent)
        {
            LogToMessageTable(_order.KxOrderId, "Kx", messageContent);
            _logger(AuditLevel.Info, STR_XmlDefinition, messageContent, "OUT");
        }


        protected override void MessageReceived(string messageContent)
        {
            LogToMessageTable(_order.KxOrderId, "WU", messageContent);
            _logger(AuditLevel.Info, STR_XmlDefinition, messageContent, "IN");
        }

        #endregion

        #region Internal Methods

        internal override WesternUnionResponse GetResponse(string responseXml)
        {
            MessageReceived(RemovePDFBytesForLog(responseXml));
            return new PaymentInstructionResponse(responseXml);
        }

        private string RemovePDFBytesForLog(string responseXml)
        {
            try
            {
                XDocument doc = XDocument.Parse(responseXml);
                var toRemove = from n in doc.Descendants()
                    where n.Name.LocalName == "pdfBytes"
                    select n;

                toRemove.Remove();
                           
                return doc.ToString();
            }
            catch
            {
                return responseXml;
            }
        }

        internal override WesternUnionResponse GetResponse(byte[] byteData)
        {
            var xml = string.Format(@"<paymentInstructionResponse xmlns=""http://api.globalpay.wu.com""><status success=""true"" /><pdfData><byteLength>{0}</byteLength><pdfBytes>{1}</pdfBytes></pdfData></paymentInstructionResponse>", byteData.Length, KxBase64.Base64Encode(byteData));
            return GetResponse(xml);
        }

        #endregion
    }
}