﻿#region copyright

// =======================================================
// Product:   KCF.Student
// Project:   Kx.Core.WesternUnion
// File:      LogQuotedOrderRequest.cs
// Created:   06/02/2018
// By:        Steve Ashley
// Modified:  06/02/2018
// 
// Copyright © Kinetic Solutions Ltd 2018
// All rights reserved.
// 
// No portion of this file may be re-created, copied or otherwise
// used without the express permission of Kinetic Solutions Ltd.
// =======================================================

#endregion

#region usings

using System;
using System.Data.SqlTypes;
using Kx.Core.WesternUnion.DatabaseObjects;
using Kx.Core.WesternUnion.Responses;

#endregion

namespace Kx.Core.WesternUnion.Requests
{
    public class OrderStatusRequest : WesternUnionRequest
    {
        #region Fields and Properties

        private const string STR_XmlDefinition = "IF1101orderStatus";
        private const string STR_ServiceURI = "orderStatus";

        private IKxWesternUnionOrder _order = null;

        #endregion

        #region Constructors

        public OrderStatusRequest(int kxOrderId, WriteLogRecord logger)
            : base(STR_ServiceURI, WesternUnionVerb.POST, STR_XmlDefinition, logger)
        {
            _order = DatabaseFactory.GetOrderByOrderId(kxOrderId);
            AddRequestMergeObject("request", _order);
        }

        public OrderStatusRequest(int kxOrderId, IWesternUnionDatabaseFactory databaseFactory, WriteLogRecord logger)
            : base(STR_ServiceURI, WesternUnionVerb.POST, databaseFactory, STR_XmlDefinition, logger)
        {
            _order = databaseFactory.GetOrderByOrderId(kxOrderId);
            AddRequestMergeObject("request", _order);
        }

        #endregion

        #region Protected Methods

        protected override bool Validate()
        {
            return !string.IsNullOrWhiteSpace(_order.WuorderId);
        }

        internal override void MessageSent(string messageContent)
        {
            LogToMessageTable(_order.KxOrderId, "Kx", messageContent);
            _logger(AuditLevel.Info, STR_XmlDefinition, messageContent, "OUT");
        }


        protected override void MessageReceived(string messageContent)
        {
            LogToMessageTable(_order.KxOrderId, "WU", messageContent);
            _logger(AuditLevel.Info, STR_XmlDefinition, messageContent, "IN");
        }

        #endregion

        #region Internal Methods

        internal override WesternUnionResponse GetResponse(string responseXml)
        {
            MessageReceived(responseXml);
            var response = new OrderStatusResponse(responseXml);
            if (response.Successful)
            {
                UpdateOrderStatus(response);
            }

            return response;
        }

        private void UpdateOrderStatus(OrderStatusResponse response)
        {
            try
            {
                _order.LastStatus = (!(string.IsNullOrWhiteSpace(response.OrderStatus.Status)) ? response.OrderStatus.Status : _order.LastStatus);
                _order.LastStatusCheckDate = DateTime.Now;
                _order.LastStatusChangeDate = (response.OrderStatus.StatusChangeDate < SqlDateTime.MinValue.Value ? _order.LastStatusChangeDate : response.OrderStatus.StatusChangeDate);
                if (!_order.Save())
                {
                    _logger(AuditLevel.Error, "UpdateOrderStatus", _order.LastError, _order.KxBookingIdentifier);
                }

            }
            catch (Exception e)
            {
                _logger(AuditLevel.Error, "UpdateOrderStatus", e.Message, _order.KxBookingIdentifier);
            }
        }

        #endregion
    }
}