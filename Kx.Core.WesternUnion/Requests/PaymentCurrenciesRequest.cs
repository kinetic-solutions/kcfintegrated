﻿using Kx.Core.WesternUnion.DataObj.Requests;
using Kx.Core.WesternUnion.DataObj.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kx.Core.WesternUnion.Responses;
using System.Xml.Linq;

namespace Kx.Core.WesternUnion.Requests
{
    /// <summary>
    /// IF-03-01
    /// Request for all supported payment currencies based upon the country of payment.
    /// </summary>
    public class PaymentCurrenciesRequest : WesternUnionRequest
    {
        private const string STR_XmlDefinition = "IF0301paymentCurrency";
        private const string STR_ServiceURI = "paymentCurrency";

        private IPaymentCurrenciesRequestData _data;
        public PaymentCurrenciesRequest(IPaymentCurrenciesRequestData data, WriteLogRecord logger)
            : base(STR_ServiceURI, WesternUnionVerb.POST, STR_XmlDefinition, logger)
        {
            _data = data;
            this.AddRequestMergeObject("request", data);
        }

        internal override WesternUnionResponse GetResponse(string responseXml)
        {
            MessageReceived(responseXml);
            return new PaymentCurrenciesResponse(responseXml);
        }

        internal override void MessageSent(string messageContent)
        {
            _logger(AuditLevel.Info, STR_XmlDefinition, messageContent, "OUT");
        }

        protected override void MessageReceived(string messageContent)
        {
            _logger(AuditLevel.Info, STR_XmlDefinition, messageContent, "IN");
        }

        protected override bool Validate()
        {
            return
                this._data != null &&
                !string.IsNullOrWhiteSpace(this._data.ClientId) &&
                !string.IsNullOrWhiteSpace(this._data.CountryCode);
        }
    }
}
