﻿#region copyright

// =======================================================
// Product:   KCF.Student
// Project:   Kx.Core.WesternUnion
// File:      QuoteOrderRequest.cs
// Created:   31/01/2018
// By:        Steve Ashley
// Modified:  02/02/2018
// 
// Copyright © Kinetic Solutions Ltd 2018
// All rights reserved.
// 
// No portion of this file may be re-created, copied or otherwise
// used without the express permission of Kinetic Solutions Ltd.
// =======================================================

#endregion

#region usings

using System;
using System.Linq;
using System.Xml.Linq;
using Kx.Core.WesternUnion.DatabaseObjects;
using Kx.Core.WesternUnion.DataObj.Requests;
using Kx.Core.WesternUnion.DataObj.Responses;
using Kx.Core.WesternUnion.Helpers;
using Kx.Core.WesternUnion.Responses;

#endregion

namespace Kx.Core.WesternUnion.Requests
{
    /// <summary>
    /// IF-06-01
    /// places a request to Western Union for a quote for a transfer
    /// </summary>
    public class QuoteOrderRequest : WesternUnionRequest
    {
        private const string STR_XmlDefinition = "IF0601quoteOrder";
        private const string STR_ServiceURI = "quoteOrder";

        #region Fields and Properties

        private IQuoteOrderRequestData _data;
        private int _kxOrderId;
        private string _kxBookingIdentifier;

        public int KxOrderId
        {
            get { return _kxOrderId; }
        }

        #endregion

        #region Constructors

        /// <summary>
        /// Encapsulates the request for a transfer quote that will be passed to Western Union
        /// </summary>
        /// <param name="kxBookingIdentifier">The identifier of the student/application etc that links this request to a specific customer/student etc.</param>
        /// <param name="data">Data structure for the required information that will be passed to Western Union</param>
        /// <param name="logger">Delegate method to provide audit logging within external application</param>
        public QuoteOrderRequest(string kxBookingIdentifier, IQuoteOrderRequestData data, WriteLogRecord logger)
            : base(STR_ServiceURI, WesternUnionVerb.POST, STR_XmlDefinition, logger)
        {
            _kxBookingIdentifier = kxBookingIdentifier;
            _data = data;
            new NonSWIFTCharacterReplacement(_data);
            new NonSWIFTCharacterReplacement(_data.Remitter);
            new NonSWIFTCharacterReplacement(_data.RemitterBank);
            AddRequestMergeObject("request", _data);

            this.PreSend();
        }

        /// <summary>
        /// Encapsulates the request for a transfer quote that will be passed to Western Union
        /// </summary>
        /// <param name="kxBookingIdentifier">The identifier of the student/application etc that links this request to a specific customer/student etc.</param>
        /// <param name="data">Data structure for the required information that will be passed to Western Union</param>
        /// <param name="databaseFactory">Provides a custom database factory to allow separate storage options away from the default.</param>
        /// <param name="logger">Delegate method to provide audit logging within external application</param>
        public QuoteOrderRequest(string kxBookingIdentifier, IQuoteOrderRequestData data, IWesternUnionDatabaseFactory databaseFactory, WriteLogRecord logger)
            : base(STR_ServiceURI, WesternUnionVerb.POST, databaseFactory, STR_XmlDefinition, logger)
        {
            _kxBookingIdentifier = kxBookingIdentifier;
            _data = data;
            new NonSWIFTCharacterReplacement(_data);
            new NonSWIFTCharacterReplacement(_data.Remitter);
            new NonSWIFTCharacterReplacement(_data.RemitterBank);
            AddRequestMergeObject("request", _data);

            this.PreSend();
        }

        #endregion

        #region Protected Methods

        protected override bool Validate()
        {
            return !string.IsNullOrWhiteSpace(_data.ClientId) &&
                   !string.IsNullOrWhiteSpace(_data.ForeignItemId) &&
                   !string.IsNullOrWhiteSpace(_data.ForeignOrderId) &&
                   !string.IsNullOrWhiteSpace(_data.Language) &&
                   !string.IsNullOrWhiteSpace(_data.PaymentCurrencyCode) &&
                   !string.IsNullOrWhiteSpace(_data.PaymentTypeCode) &&
                   !string.IsNullOrWhiteSpace(_data.SettlementCurrencyCode) &&
                   !string.IsNullOrWhiteSpace(_data.Amount.ToString()) &&
                   !string.IsNullOrWhiteSpace(_data.AmountType.ToString()) &&
                   !string.IsNullOrWhiteSpace(_data.Remitter.Name.ToString()) &&
                   !string.IsNullOrWhiteSpace(_data.Remitter.Address1.ToString()) &&
                   !string.IsNullOrWhiteSpace(_data.Remitter.City.ToString()) &&
                   !string.IsNullOrWhiteSpace(_data.Remitter.CountryCode.ToString()) &&
                   !string.IsNullOrWhiteSpace(_data.Payer.StudentId.ToString()) &&
                   // If we've specified a bank name, then validate the whole bank address, otherwise don't.
                   (HasBankDetails() &&
                    !string.IsNullOrWhiteSpace(_data.RemitterBank.Name.ToString()) &&
                    !string.IsNullOrWhiteSpace(_data.RemitterBank.Address1.ToString()) &&
                    !string.IsNullOrWhiteSpace(_data.RemitterBank.City.ToString()) &&
                    !string.IsNullOrWhiteSpace(_data.RemitterBank.CountryCode.ToString())
                    || !HasBankDetails()
                   );
        }

        #endregion

        #region Private Methods

        private bool HasBankDetails()
        {
            return !string.IsNullOrWhiteSpace(_data.RemitterBank.Name) || !string.IsNullOrWhiteSpace(_data.RemitterBank.Address1) || !string.IsNullOrWhiteSpace(_data.RemitterBank.City);
        }
        #endregion

        #region Internal Methods

        internal override WesternUnionResponse GetResponse(string responseXml)
        {
            MessageReceived(responseXml);
            var result = new QuoteOrderResponse(responseXml);
            if (result.Successful)
            {
                UpdateOrderWithWUOrderId(result.Quote);
            }

            return result;
        }

        internal override string GetRequestXml()
        {
            XDocument doc = XDocument.Parse(base.GetRequestXml());
            var emailNode = doc.Descendants().Where(s => s.Name.LocalName == "remitter").Descendants().Where(s => s.Name.LocalName == "email").Where(s => s.Value == "").FirstOrDefault();
            if (emailNode != null)
            {
                emailNode.Remove();
            }

            if (!this.HasBankDetails())
            {
                var bankNode = doc.Descendants().Where(s => s.Name.LocalName == "remitterBank").FirstOrDefault();
                if (bankNode != null)
                {
                    bankNode.Remove();
                }
            }

            if (this.HasCustomValues())
            {
                var payerNode = doc.Descendants().Where(o => o.Name.LocalName == "payer").FirstOrDefault();

                XNamespace westernUnionNs = "http://api.globalpay.wu.com";

                var namespaceAndNode = westernUnionNs + "custom";

                if (!string.IsNullOrWhiteSpace(_data.Payer.PayerCustom1)) payerNode.Add(new XElement(namespaceAndNode, _data.Payer.PayerCustom1));
                if (!string.IsNullOrWhiteSpace(_data.Payer.PayerCustom2)) payerNode.Add(new XElement(namespaceAndNode, _data.Payer.PayerCustom2));
                if (!string.IsNullOrWhiteSpace(_data.Payer.PayerCustom3)) payerNode.Add(new XElement(namespaceAndNode, _data.Payer.PayerCustom3));
                if (!string.IsNullOrWhiteSpace(_data.Payer.PayerCustom4)) payerNode.Add(new XElement(namespaceAndNode, _data.Payer.PayerCustom4));
                if (!string.IsNullOrWhiteSpace(_data.Payer.PayerCustom5)) payerNode.Add(new XElement(namespaceAndNode, _data.Payer.PayerCustom5));
                if (!string.IsNullOrWhiteSpace(_data.Payer.PayerCustom6)) payerNode.Add(new XElement(namespaceAndNode, _data.Payer.PayerCustom6));
                if (!string.IsNullOrWhiteSpace(_data.Payer.PayerCustom7)) payerNode.Add(new XElement(namespaceAndNode, _data.Payer.PayerCustom7));
                if (!string.IsNullOrWhiteSpace(_data.Payer.PayerCustom8)) payerNode.Add(new XElement(namespaceAndNode, _data.Payer.PayerCustom8));
                if (!string.IsNullOrWhiteSpace(_data.Payer.PayerCustom9)) payerNode.Add(new XElement(namespaceAndNode, _data.Payer.PayerCustom9));
                if (!string.IsNullOrWhiteSpace(_data.Payer.PayerCustom10)) payerNode.Add(new XElement(namespaceAndNode, _data.Payer.PayerCustom10));
            }

            return doc.ToString();
        }

        private void UpdateOrderWithWUOrderId(QuoteOrder quote)
        {
            IKxWesternUnionOrder order = DatabaseFactory.GetOrderByOrderId(this._kxOrderId);
            if (order == null)
            {
                string message = string.Format("WesternUnionOrder record NOT found for KxOrderId {0}", _kxOrderId);
                _logger(AuditLevel.Error, "QuoteOrderRequest.UpdateOrderWithWUOrderId", message, quote.OrderId);
                throw new WesternUnionException(message);
            }
            order.WuorderId = quote.OrderId;
            if (!order.Save())
            {
                _logger(AuditLevel.Error, "QuoteOrderRequest.UpdateOrderWithWUOrderId", order.LastError, quote.OrderId);
                throw new WesternUnionException(order.LastError);
            }
        }

        private bool HasCustomValues()
        {
            return (!string.IsNullOrWhiteSpace(_data.Payer.PayerCustom1) || !string.IsNullOrWhiteSpace(_data.Payer.PayerCustom2) || !string.IsNullOrWhiteSpace(_data.Payer.PayerCustom3) ||
                !string.IsNullOrWhiteSpace(_data.Payer.PayerCustom4) || !string.IsNullOrWhiteSpace(_data.Payer.PayerCustom5) || !string.IsNullOrWhiteSpace(_data.Payer.PayerCustom6) ||
                !string.IsNullOrWhiteSpace(_data.Payer.PayerCustom7) || !string.IsNullOrWhiteSpace(_data.Payer.PayerCustom8) || !string.IsNullOrWhiteSpace(_data.Payer.PayerCustom9) || !string.IsNullOrWhiteSpace(_data.Payer.PayerCustom10));
        }

        private void PreSend()
        {
            IKxWesternUnionOrder order = DatabaseFactory.GetNewOrder();
            order.KxBookingIdentifier = this._kxBookingIdentifier;
            order.CreationDate = DateTime.Now;
            if (string.IsNullOrWhiteSpace(_data.ForeignOrderId))
            {
                order.Save();
                _data.ForeignOrderId = order.KxOrderId.ToString();
            }

            if (string.IsNullOrWhiteSpace(_data.ForeignItemId))
            {
                _data.ForeignItemId = _data.ForeignOrderId;
            }

            order.ForeignOrderId = _data.ForeignOrderId;
            if (!order.Save())
            {
                _logger(AuditLevel.Error, "QuoteOrderRequest.PreSend", order.LastError, this._kxBookingIdentifier);
                throw new WesternUnionException(order.LastError);
            }
            _kxOrderId = order.KxOrderId;
        }

        internal override void MessageSent(string messageContent)
        {
            LogToMessageTable(_kxOrderId, "Kx", messageContent);
            _logger(AuditLevel.Info, STR_XmlDefinition, messageContent, "OUT");
        }

        protected override void MessageReceived(string messageContent)
        {
            LogToMessageTable(_kxOrderId, "WU", messageContent);
            _logger(AuditLevel.Info, STR_XmlDefinition, messageContent, "IN");
        }

        #endregion
    }
}