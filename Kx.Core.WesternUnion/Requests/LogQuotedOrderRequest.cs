﻿#region copyright

// =======================================================
// Product:   KCF.Student
// Project:   Kx.Core.WesternUnion
// File:      LogQuotedOrderRequest.cs
// Created:   06/02/2018
// By:        Steve Ashley
// Modified:  06/02/2018
// 
// Copyright © Kinetic Solutions Ltd 2018
// All rights reserved.
// 
// No portion of this file may be re-created, copied or otherwise
// used without the express permission of Kinetic Solutions Ltd.
// =======================================================

#endregion

#region usings

using System;
using Kx.Core.WesternUnion.DatabaseObjects;
using Kx.Core.WesternUnion.Responses;

#endregion

namespace Kx.Core.WesternUnion.Requests
{
    public class LogQuotedOrderRequest : WesternUnionRequest
    {
        #region Fields and Properties

        private const string STR_XmlDefinition = "IF0602logQuotedOrder";
        private const string STR_ServiceURI = "logQuotedOrder";

        private IKxWesternUnionOrder _order = null;

        #endregion

        #region Constructors

        public LogQuotedOrderRequest(int kxOrderId, WriteLogRecord logger)
            : base(STR_ServiceURI, WesternUnionVerb.POST, STR_XmlDefinition, logger)
        {
            _order = DatabaseFactory.GetOrderByOrderId(kxOrderId);
            AddRequestMergeObject("request", _order);
        }

        public LogQuotedOrderRequest(int kxOrderId, IWesternUnionDatabaseFactory databaseFactory, WriteLogRecord logger)
            : base(STR_ServiceURI, WesternUnionVerb.POST, databaseFactory, STR_XmlDefinition, logger)
        {
            _order = databaseFactory.GetOrderByOrderId(kxOrderId);
            AddRequestMergeObject("request", _order);
        }

        #endregion

        #region Protected Methods

        protected override bool Validate()
        {
            return !string.IsNullOrWhiteSpace(_order.WuorderId);
        }

        internal override void MessageSent(string messageContent)
        {
            LogToMessageTable(_order.KxOrderId, "Kx", messageContent);
            _logger(AuditLevel.Info, STR_XmlDefinition, messageContent, "OUT");
        }


        protected override void MessageReceived(string messageContent)
        {
            LogToMessageTable(_order.KxOrderId, "WU", messageContent);
            _logger(AuditLevel.Info, STR_XmlDefinition, messageContent, "IN");
        }

        #endregion

        #region Internal Methods

        internal override WesternUnionResponse GetResponse(string responseXml)
        {
            MessageReceived(responseXml);
            var response = new LogQuotedOrderResponse(responseXml);
            if (response.Successful)
            {
                UpdateDealReferenceForOrder(response);
            }

            return response;
        }

        private void UpdateDealReferenceForOrder(LogQuotedOrderResponse response)
        {
            if (response.QuotedOrder == null || string.IsNullOrWhiteSpace(response.QuotedOrder.DealReference))
            {
                _logger(AuditLevel.Error, STR_XmlDefinition, "Missing QuotedOrder or DealReference", response.RawXml);
                throw new WesternUnionException("Missing QuotedOrder or DealReference");
            }
            _order.WuDealReference = response.QuotedOrder.DealReference;
            if (!_order.Save())
            {
                _logger(AuditLevel.Error, STR_XmlDefinition, _order.LastError, response.RawXml);
                throw new WesternUnionException(_order.LastError);
            }

        }

        #endregion
    }
}