﻿using Kx.Core.WesternUnion.DataObj.Requests;
using Kx.Core.WesternUnion.DataObj.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kx.Core.WesternUnion.Responses;
using System.Xml.Linq;

namespace Kx.Core.WesternUnion.Requests
{
    /// <summary>
    /// IF-15-01
    /// Call to obtain list of supported languages for the payment instructions as supplied by Western Union
    /// </summary>
    public class InstructionLanguagesRequest : WesternUnionRequest
    {
        private const string STR_XmlDefinition = "IF1501languageRequest";
        private const string STR_ServiceURI = "language";

        public InstructionLanguagesRequest(WriteLogRecord logger)
            : base(STR_ServiceURI, WesternUnionVerb.POST, STR_XmlDefinition, logger)
        {
            this.AddRequestMergeObject("request", new {Empty = ""});
        }

        internal override WesternUnionResponse GetResponse(string responseXml)
        {
            MessageReceived(responseXml);
            return new InstructionLanguagesResponse(responseXml);
        }

        internal override void MessageSent(string messageContent)
        {
            _logger(AuditLevel.Info, STR_XmlDefinition, messageContent, "OUT");
        }

        protected override void MessageReceived(string messageContent)
        {
            _logger(AuditLevel.Info, STR_XmlDefinition, messageContent, "IN");
        }

        protected override bool Validate()
        {
            return true;
        }
    }
}
