﻿#region copyright

// =======================================================
// Product:   KCF.Student
// Project:   Kx.Core.WesternUnion
// File:      WesternUnionRequest.cs
// Created:   16/01/2018
// By:        Steve Ashley
// Modified:  31/01/2018
// 
// Copyright © Kinetic Solutions Ltd 2018
// All rights reserved.
// 
// No portion of this file may be re-created, copied or otherwise
// used without the express permission of Kinetic Solutions Ltd.
// =======================================================

#endregion

#region usings

using System;
using System.Xml.Linq;
using Kx.Core.Merge;
using Kx.Core.Web.Student.Classes;
using Kx.Core.WesternUnion.DatabaseObjects;
using Kx.Core.WesternUnion.Responses;

#endregion

namespace Kx.Core.WesternUnion.Requests
{
    public abstract class WesternUnionRequest
    {
        #region Fields and Properties

        KxTemplateMerge _merge = new KxTemplateMerge();

        private string _xmlRequestDefinition;
        protected WriteLogRecord _logger;


        internal string InterfaceName { get; private set; }
        internal WesternUnionVerb Verb { get; private set; }

        protected IWesternUnionDatabaseFactory DatabaseFactory { get; private set; }

        internal bool OkToSend
        {
            get { return Validate(); }
        }

        #endregion

        #region Constructors

        protected WesternUnionRequest(string interfaceName, WesternUnionVerb verb, string xmlRequestDefinition, WriteLogRecord logger)
        {
            _logger = logger ?? ((level, method, message, data) => Console.WriteLine("No Logger"));
            InterfaceName = interfaceName;
            Verb = verb;
            _xmlRequestDefinition = xmlRequestDefinition;
            DatabaseFactory = Kx.Core.ServiceLocator.ServiceLocator.ResolveService<IWesternUnionDatabaseFactory>() ?? new WesternUnionDatabaseFactory();
        }
        protected WesternUnionRequest(string interfaceName, WesternUnionVerb verb, IWesternUnionDatabaseFactory databaseFactory, string xmlRequestDefinition, WriteLogRecord logger)
        {
            _logger = logger ?? ((level, method, message, data) => Console.WriteLine("{0} | {1} | {2} | {3}", level, method, message, data));
            InterfaceName = interfaceName;
            Verb = verb;
            _xmlRequestDefinition = xmlRequestDefinition;
            DatabaseFactory = databaseFactory;
        }

        #endregion

        #region Protected Methods

        protected void AddRequestMergeObject(string objectName, object obj)
        {
            _merge.AddObject(objectName, obj);
        }

        protected string MergeXml(string xml)
        {
            XDocument doc = XDocument.Parse(xml);
            foreach (XElement element in doc.Elements())
            {
                MergeElement(element);
            }

            return doc.ToString();
        }

        protected string GetRequestResource(string interfaceResourceName)
        {
            using (var helper = new EmbeddedResourceHelper())
            {
                return helper.GetEmbeddedResource(string.Format("Kx.Core.WesternUnion.RequestXml.{0}.xml", interfaceResourceName));
            }
        }

        protected abstract bool Validate();

        internal virtual void MessageSent(string messageContent)
        {
            // Do nothing by default.
        }

        protected virtual void MessageReceived(string messageContent)
        {
            // Do nothing by default
        }

        #endregion

        #region Private Methods

        private void MergeElement(XElement element)
        {
            if (!element.HasElements && element.Value.Contains("{"))
            {
                element.Value = _merge.MergeTemplate(element.Value);
            }
            else if (element.HasElements)
            {
                foreach (XElement child in element.Elements())
                {
                    MergeElement(child);
                }
            }
        }

        #endregion

        #region Internal Methods

        internal abstract WesternUnionResponse GetResponse(string responseXml);

        internal virtual WesternUnionResponse GetResponse(byte[] byteData)
        {
            return new NullResponse("Byte data returned for request");
        }

        internal virtual string GetRequestXml()
        {
            return MergeXml(GetRequestResource(_xmlRequestDefinition));
        }

        #endregion

        protected void LogToMessageTable(int kxOrderId, string source, string messageContent)
        {
            IKxWesternUnionOrderMessage message = DatabaseFactory.GetNewOrderMessage();
            message.CreationDate = DateTime.Now;
            message.KxOrderId = kxOrderId;
            message.MessageSource = source;
            message.MessageContent = messageContent;
            if (!message.Save())
            {
                _logger(AuditLevel.Error, "QuoteOrderRequest.LogMessageToTable", message.LastError, messageContent);
            }
        }
    }
}