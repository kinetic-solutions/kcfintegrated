﻿#region copyright

// =======================================================
// Product:   KCF.Student
// Project:   Kx.Core.WesternUnion
// File:      WesternUnionSender.cs
// Created:   16/01/2018
// By:        Steve Ashley
// Modified:  07/02/2018
// 
// Copyright © Kinetic Solutions Ltd 2018
// All rights reserved.
// 
// No portion of this file may be re-created, copied or otherwise
// used without the express permission of Kinetic Solutions Ltd.
// =======================================================

#endregion

#region usings

using System;
using System.Net;
using System.Text;
using Kx.Core.Configuration;
using Kx.Core.WesternUnion.Requests;
using Kx.Core.WesternUnion.Responses;

#endregion

namespace Kx.Core.WesternUnion
{
    public class WesternUnionSender
    {
        #region Fields and Properties

        private string _apiUrl = "https://api.globalpay.wu.com/geo-api/";
        private WriteLogRecord _logger;

        private GetWebClient _getWebClient;

        #endregion

        #region Constructors

        /// <summary>
        /// Object to send the supplied request to Western Union
        /// </summary>
        /// <param name="apiUrl">The base url for the API supplied by Western Union, defaults to https://api.globalpay.wu.com/geo-api/</param>
        /// <param name="logger">Delegated method for writing audit logs</param>
        public WesternUnionSender(string apiUrl, WriteLogRecord logger, GetWebClient getWebClient = null)
        {
            _apiUrl = apiUrl ?? _apiUrl;
            _logger = logger;
            _getWebClient = getWebClient ?? GetWebClient;
        }

        #endregion

        private IWUWebClient GetWebClient()
        {
            return new WUWebClient();
        }

        #region Public Methods

        /// <summary>
        /// Sends the supplied request to Western Union
        /// </summary>
        /// <param name="request">The request that is to be sent</param>
        /// <returns>Response based upon data returned by Western Union</returns>
        public WesternUnionResponse SendRequestToWesternUnion(WesternUnionRequest request)
        {
            string responseString = string.Empty;
            byte[] responseDataBytes = null;
            string postData = string.Empty;
            WesternUnionResponse response = null;

            if (request.OkToSend)
            {
                var trackingId = Guid.NewGuid();
                try
                {
                    using (IWUWebClient client = _getWebClient())
                    {
                        SetRequestHeaders(client);
                        ConfigureTransportSecurity();
                        SetCredentials(client);

                        string url = GetAPIUrl(request.InterfaceName);

                        postData = request.GetRequestXml();

                        _logger(AuditLevel.Info, "SendRequestToWesternUnion", string.Format("(TrackId: {2}) {1} data to {0}", url, request.Verb, trackingId), postData);

                        byte[] postDataBytes = Encoding.UTF8.GetBytes(postData);
                        request.MessageSent(postData);

                        try
                        {
                            responseDataBytes = client.UploadData(url, request.Verb.ToString(), postDataBytes);
                            responseString = DetermineResponseType(client, responseDataBytes, trackingId);
                        }
                        catch (WebException webException)
                        {
                            var exResponse = webException.Response as HttpWebResponse;
                            if (exResponse != null)
                            {
                                _logger(AuditLevel.Error, "ResponseFromWesternUnion", string.Format("(TrackId: {0})", trackingId), string.Format("Error response from Western Union: {0} - {1}", exResponse.StatusCode, exResponse.StatusDescription));
                                throw;
                            }
                            else
                            {
                                _logger(AuditLevel.Error, "ResponseFromWesternUnion", string.Format("(TrackId: {0})", trackingId), string.Format("Error response from Western Union: {0}", webException.Status));
                                throw;
                            }
                        }
                        catch (Exception ex)
                        {
                            _logger(AuditLevel.Error, "ResponseFromWesternUnion", string.Format("(TrackId: {0})", trackingId), string.Format("Error response from Western Union: {0}", ex.Message));
                            throw;
                        }

                        response = LoadResponse(request, responseString, responseDataBytes);
                    }
                }
                catch (Exception ex)
                {
                    _logger(AuditLevel.Error, "SendRequestToWesternUnion", ex.Message, postData);
                    response = request.GetResponse(string.Format("<null><status success=\"false\"><error code=\"Kx\">{0}</error></status></null>", ex.Message));
                    responseString = string.Empty;
                }
            }
            else
            {
                response = request.GetResponse(string.Format("<null><status success=\"false\"><error code=\"Kx\">{0}</error></status></null>", "Request is not valid to send to Western Union"));
            }

            return response;
        }

        #endregion

        #region Private Methods

        private static WesternUnionResponse LoadResponse(WesternUnionRequest request, string responseString, byte[] responseDataBytes)
        {
            WesternUnionResponse result = null;
            if (!string.IsNullOrWhiteSpace(responseString))
            {
                result = request.GetResponse(responseString);
            }
            else if (responseDataBytes != null)
            {
                result = request.GetResponse(responseDataBytes);
            }

            return result ?? new NullResponse("No Response");
        }

        private string DetermineResponseType(IWUWebClient client, byte[] responseDataBytes, Guid trackingId)
        {
            string result = null;
            if ((client.ResponseHeaders["Content-Type"] ?? "application/xml").ToLower().Contains("application/xml"))
            {
                result = Encoding.UTF8.GetString(responseDataBytes);
                _logger(AuditLevel.Info, "ResponseFromWesternUnion", string.Format("(TrackId: {0})", trackingId), result);
            }
            else if ((client.ResponseHeaders["Content-Type"] ?? "application/xml").ToLower().Contains("application/pdf"))
            {
                _logger(AuditLevel.Info, "ResponseFromWesternUnion", string.Format("(TrackId: {0})", trackingId), "PDF data returned");
            }
            else
            {
                _logger(AuditLevel.Info, "ResponseFromWesternUnion", string.Format("(TrackId: {0})", trackingId), string.Format("Invalid content type returned: {0}", client.ResponseHeaders["Content-Type"]));
                throw new WesternUnionException(string.Format("Invalid content type returned: {0}", client.ResponseHeaders["Content-Type"]));
            }

            return result;
        }

        private void SetCredentials(IWUWebClient client)
        {
            client.Credentials = new NetworkCredential(KxDbConfiguration.GetConfiguration("WesternUnion.API.Username"), KxDbConfiguration.GetConfiguration("WesternUnion.API.Password"));
        }

        private string GetAPIUrl(string interfaceName)
        {
            Uri apiUrl = new Uri(_apiUrl);
            Uri result = new Uri(apiUrl, interfaceName);

            return result.AbsoluteUri;
        }

        private static void ConfigureTransportSecurity()
        {
            ServicePointManager.ServerCertificateValidationCallback += (sender, certificate, chain, errors) => true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12 | SecurityProtocolType.Ssl3;
        }

        private static void SetRequestHeaders(IWUWebClient client)
        {
            client.Headers.Add("Content-Type", "application/xml");
        }

        #endregion
    }
}