﻿#region copyright

// =======================================================
// Product:   Kx.WesternUnion
// Project:   Kx.WesternUnion.Service
// File:      IWebClient.cs
// Created:   12/02/2018
// By:        Steve Ashley
// Modified:  12/02/2018
// 
// Copyright © Kinetic Solutions Ltd 2018
// All rights reserved.
// 
// No portion of this file may be re-created, copied or otherwise
// used without the express permission of Kinetic Solutions Ltd.
// =======================================================

#endregion

#region usings

using System;
using System.Net;

#endregion

namespace Kx.Core.WesternUnion
{
    public interface IWUWebClient : IDisposable
    {
        #region Fields and Properties

        WebHeaderCollection Headers { get; set; }
        WebHeaderCollection ResponseHeaders { get; }
        ICredentials Credentials { get; set; }

        #endregion

        #region Public Methods

        byte[] UploadData(string address, string method, byte[] data);

        #endregion
    }
}