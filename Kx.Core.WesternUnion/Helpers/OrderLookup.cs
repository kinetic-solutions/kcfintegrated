﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kx.Core.WesternUnion.DatabaseObjects;

namespace Kx.Core.WesternUnion.Helpers
{
    public class OrderLookup : IDisposable
    {
        public IEnumerable<IKxWesternUnionOrder> LookupOrderForBookingIdentifier(string bookingIdentifier)
        {
            return KxWesternUnionOrder.GetWesternUnionOrdersByKxBookingIdentifier(bookingIdentifier);
        }

        public void Dispose()
        {
            
        }
    }
}
