﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Kx.Core.Reflection;

namespace Kx.Core.WesternUnion.Helpers
{
    public class NonSWIFTCharacterReplacement
    {
        public const string NonSWIFTCharacters = @"[^\sa-zA-Z0-9,\.:'\+\(\)\?'\-]";
        public const string SWIFTCharacters = @"[\sa-zA-Z0-9,\.:'\+\(\)\?'\-]";
        public const int MaxLength = 30;

        public NonSWIFTCharacterReplacement(object obj)
        {
            ReplaceCharacters(obj);
        }

        void ReplaceCharacters(object obj)
        {
            var props = obj.GetType().GetProperties().Where(prop => Attribute.IsDefined(prop, typeof(SWIFTStringAttribute)));
            foreach (PropertyInfo prop in props)
            {
                KxReflection.SetProperty(obj, prop.Name, RemoveNonSWIFTCharacters(KxReflection.GetPropertyString(obj, prop.Name)));
            }
        }

        public static string RemoveNonSWIFTCharacters(string input)
        {
            Regex reg = new Regex(NonSWIFTCharacters);
            return RemoveExcessCharacters(reg.Replace(input, " "));
        }

        public static string RemoveExcessCharacters(string input)
        {
            return input.Substring(0, input.Length > MaxLength ? MaxLength : input.Length);
        }
    }
}
