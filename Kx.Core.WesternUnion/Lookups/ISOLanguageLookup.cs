﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kx.Core.Web.Student.Classes;

namespace Kx.Core.WesternUnion.Lookups
{
    public class ISOLanguageLookup : WesternUnionLookup
    {
        private const string STR_LanguagesResource = "Kx.Core.WesternUnion.Lookups.ISOLanguages.txt";
        public ISOLanguageLookup()
        : base (STR_LanguagesResource)
        {
            LoadLookupFromResource();
        }
    }
}
