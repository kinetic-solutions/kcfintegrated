﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kx.Core.Web.Student.Classes;

namespace Kx.Core.WesternUnion.Lookups
{
    public abstract class WesternUnionLookup : IDisposable
    {
        private Dictionary<string, string> _lookup = new Dictionary<string, string>(StringComparer.InvariantCultureIgnoreCase);
        private string _resourceName;

        public WesternUnionLookup(string resourceName)
        {
            _resourceName = resourceName;
        }

        public string this[string code]
        {
            get
            {
                if (_lookup.ContainsKey(code))
                {
                    return _lookup[code];
                }

                return code;
            }
        }

        public IEnumerable<string> Keys
        {
            get { return _lookup.Keys.ToList().AsEnumerable(); }
        }

        public string KCFList
        {
            get { return GetEmbeddedResource(); }
        }

        protected void LoadLookupFromResource()
        {
            string resource = GetEmbeddedResource();

            foreach (string line in resource.Split('\r', '\n'))
            {
                string[] entries = line.Split('|');
                if (entries.Length == 2)
                {
                    if (!_lookup.ContainsKey(entries[0]))
                    {
                        _lookup.Add(entries[0], entries[1]);
                    }
                }
            }
        }

        private string GetEmbeddedResource()
        {
            return new EmbeddedResourceHelper().GetEmbeddedResource(_resourceName);
        }

        public void Dispose()
        {
            _lookup = null;
            _resourceName = null;
        }
    }
}
