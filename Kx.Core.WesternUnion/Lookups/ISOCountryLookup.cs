﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kx.Core.Web.Student.Classes;

namespace Kx.Core.WesternUnion.Lookups
{
    public class ISOCountryLookup : WesternUnionLookup
    {
        private const string STR_CountriesResource = "Kx.Core.WesternUnion.Lookups.ISOCountries.txt";
        public ISOCountryLookup()
        : base (STR_CountriesResource)
        {
            LoadLookupFromResource();
        }
    }
}
