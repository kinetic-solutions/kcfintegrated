using System;

namespace Kx.Core.WesternUnion.DatabaseObjects
{
    public interface IKxWesternUnionOrder
    {
        int KxOrderId { get; }
        string KxBookingIdentifier { get; set; }
        string ForeignOrderId { get; set; }
        DateTime? CreationDate { get; set; }
        string WuorderId { get; set; }
        string WuDealReference { get; set; }
        string CompletionState { get; set; }
        DateTime? Completed { get; set; }

        string LastStatus { get; set; }
        DateTime? LastStatusCheckDate { get; set; }
        DateTime? LastStatusChangeDate { get; set; }

        string LastError { get; }

        bool Save();
    }
}