using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;

namespace Kx.Core.WesternUnion.DatabaseObjects
{
    [Serializable]
    public class KxWesternUnionOrder : KxBaseClass, IKxWesternUnionOrder
    {
        #region Attributes & Properties

        protected string _kxBookingIdentifier;
        protected string _foreignOrderId;
        protected DateTime? _creationDate;
        protected string _wuorderId;
        protected string _wuDealReference;
        protected string _completionState;
        protected DateTime? _completed;
        protected string _lastStatus;
        protected DateTime? _lastStatusCheckDate;
        protected DateTime? _lastStatusChangeDate;

        public int KxOrderId
        {
            get { return base.dBIdentifier; }
        }

        public string KxBookingIdentifier
        {
            get { return _kxBookingIdentifier; }
            set
            {
                AddToAudit("KxBookingIdentifier", _kxBookingIdentifier, value);
                _kxBookingIdentifier = value;
            }
        }

        public string ForeignOrderId
        {
            get { return _foreignOrderId; }
            set
            { 
                AddToAudit("ForeignOrderId", _foreignOrderId, value);
                _foreignOrderId = value;
            }
        }

        public DateTime? CreationDate
        {
            get { return _creationDate; }
            set
            { 
                AddToAudit("CreationDate", _creationDate, value);
                _creationDate = value;
            }
        }

        public string WuorderId
        {
            get { return _wuorderId; }
            set
            { 
                AddToAudit("WuorderId", _wuorderId, value);
                _wuorderId = value;
            }
        }

        public string WuDealReference
        {
            get { return _wuDealReference; }
            set
            { 
                AddToAudit("WuDealReference", _wuDealReference, value);
                _wuDealReference = value;
            }
        }

        public string CompletionState
        {
            get { return _completionState; }
            set
            { 
                AddToAudit("CompletionState", _completionState, value);
                _completionState = value;
            }
        }

        public DateTime? Completed
        {
            get { return _completed; }
            set
            { 
                AddToAudit("Completed", _completed, value);
                _completed = value;
            }
        }

        public string LastStatus
        {
            get { return _lastStatus; }
            set
            {
                AddToAudit("LastStatus", _lastStatus, value);
                _lastStatus = value;
            }
        }


        public DateTime? LastStatusCheckDate
        {
            get { return _lastStatusCheckDate; }
            set
            {
                AddToAudit("LastStatusCheckDate", _lastStatusCheckDate, value);
                _lastStatusCheckDate = value;
            }
        }

        public DateTime? LastStatusChangeDate
        {
            get { return _lastStatusChangeDate; }
            set
            {
                AddToAudit("LastStatusChangeDate", _lastStatusChangeDate, value);
                _lastStatusChangeDate = value;
            }
        }


        #endregion 


        public KxWesternUnionOrder(KxBaseClass parent):base(parent)
        { }


        public KxWesternUnionOrder(KxBaseClass parent, XmlNode node):base(parent)
        {
             LoadFromXml("", node);
        }

        public KxWesternUnionOrder(KxBaseClass parent,int  kxorderid):base(parent)
        {
             base.dBIdentifier = kxorderid;
             Load();
        }


        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("westernunionorders", "dbo.WesternUnionOrders", TargetType.Table);

            result.AutoKeyField = new KxDataValue("KxOrderId",KxOrderId,KxValueType.Int);


            if ((actiontype == ActionType.Update) && (KxOrderId == 0))
            {
                result.Action = ActionType.Insert;
            }
            else
            {
                result.Action = actiontype;
            }

            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {
                result.AddParameter("kxbookingidentifier", _kxBookingIdentifier, KxValueType.String, 50, true);
                result.AddParameter("foreignOrderId", _foreignOrderId, KxValueType.String, 50, true);
                result.AddParameter("creationDate", _creationDate, KxValueType.DateTime, false);
                result.AddParameter("wuorderId", _wuorderId, KxValueType.String, 50, true);
                result.AddParameter("wudealreference", _wuDealReference, KxValueType.String, 50, true);
                result.AddParameter("completionState", _completionState, KxValueType.String, 50, true);
                result.AddParameter("completed", _completed, KxValueType.DateTime, true);
                result.AddParameter("laststatus", _lastStatus, KxValueType.String, 50, true);
                result.AddParameter("laststatuscheckdate", _lastStatusCheckDate, KxValueType.DateTime, true);
                result.AddParameter("laststatuschangedate", _lastStatusChangeDate, KxValueType.DateTime, true);
            }
            return result;
        }


        public override  void LoadFromXml(string xml,XmlNode node)
        {
            dBIdentifier  = KxXmlUtils.GetNodeValue(node,"kxorderid",0);
            _kxBookingIdentifier  = KxXmlUtils.GetNodeValue(node,"kxbookingidentifier",string.Empty);
            _foreignOrderId  = KxXmlUtils.GetNodeValue(node,"foreignorderid",string.Empty);
            _creationDate  = KxXmlUtils.GetNodeValueDT(node,"creationdate",null);
            _wuorderId  = KxXmlUtils.GetNodeValue(node,"wuorderid",string.Empty);
            _wuDealReference  = KxXmlUtils.GetNodeValue(node,"wudealreference",string.Empty);
            _completionState  = KxXmlUtils.GetNodeValue(node,"completionstate",string.Empty);
            _completed  = KxXmlUtils.GetNodeValueDT(node,"completed",null);
            _lastStatus  = KxXmlUtils.GetNodeValue(node,"laststatus",string.Empty);
            _lastStatusCheckDate = KxXmlUtils.GetNodeValueDT(node, "laststatuscheckdate", null);
            _lastStatusChangeDate = KxXmlUtils.GetNodeValueDT(node, "laststatuschangedate", null);
        }

        public static IEnumerable<KxWesternUnionOrder> GetWesternUnionOrders ()
        {
            KxData data = new KxData("westernunionorders", "dbo.WesternUnionOrders", TargetType.Table);
            if (data.Execute ())
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(data.XmlResults);
                XmlNodeList nodes = doc.SelectNodes(@"//westernunionorders/datarow");
                foreach(XmlNode node in nodes)
                {
                     yield return new KxWesternUnionOrder(null, node);
                }
            }
        }

        public static IEnumerable<KxWesternUnionOrder> GetWesternUnionOrdersByKxBookingIdentifier (string kxBookingIdentifier)
        {
            KxData data = new KxData("westernunionorders", "dbo.WesternUnionOrders", TargetType.Table);
            data.AddKeyField("kxbookingidentifier", kxBookingIdentifier, KxValueType.String);
            if (data.Execute ())
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(data.XmlResults);
                XmlNodeList nodes = doc.SelectNodes(@"//westernunionorders/datarow");
                foreach(XmlNode node in nodes)
                {
                     yield return new KxWesternUnionOrder(null, node);
                }
            }
        }
    }
}

