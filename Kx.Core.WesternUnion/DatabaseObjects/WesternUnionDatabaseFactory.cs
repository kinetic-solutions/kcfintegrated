﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kx.Core.WesternUnion.DatabaseObjects
{
    public class WesternUnionDatabaseFactory : IWesternUnionDatabaseFactory
    {
        public IKxWesternUnionOrder GetNewOrder()
        {
            return new KxWesternUnionOrder(null);
        }

        public IKxWesternUnionOrderMessage GetNewOrderMessage()
        {
            return new KxWesternUnionOrderMessage(null);
        }

        public IKxWesternUnionOrder GetOrderByOrderId(int kxOrderId)
        {
            var result = new KxWesternUnionOrder(null, kxOrderId);
            if (result.CreationDate.HasValue)
            {
                return result;
            }

            return null;
        }
    }
}
