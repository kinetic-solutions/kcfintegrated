﻿namespace Kx.Core.WesternUnion.DatabaseObjects
{
    public interface IWesternUnionDatabaseFactory
    {
        IKxWesternUnionOrder GetNewOrder();
        IKxWesternUnionOrderMessage GetNewOrderMessage();
        IKxWesternUnionOrder GetOrderByOrderId(int kxOrderId);
    }
}