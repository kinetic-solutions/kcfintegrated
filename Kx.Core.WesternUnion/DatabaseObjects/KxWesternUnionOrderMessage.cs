using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Exceptions;

namespace Kx.Core.WesternUnion.DatabaseObjects
{
    [Serializable]
    public class KxWesternUnionOrderMessage : KxBaseClass, IKxWesternUnionOrderMessage
    {
        #region Attributes & Properties

        protected int _kxOrderId;
        protected DateTime? _creationDate;
        protected string _messageSource;
        protected string _messageContent;

        public int MessageId
        {
            get { return base.dBIdentifier; }
        }

        public int KxOrderId
        {
            get { return _kxOrderId; }
            set
            { 
                AddToAudit("KxOrderId", _kxOrderId, value);
                _kxOrderId = value;
            }
        }

        public DateTime? CreationDate
        {
            get { return _creationDate; }
            set
            { 
                AddToAudit("CreationDate", _creationDate, value);
                _creationDate = value;
            }
        }

        public string MessageSource
        {
            get { return _messageSource; }
            set
            { 
                AddToAudit("MessageSource", _messageSource, value);
                _messageSource = value;
            }
        }

        public string MessageContent
        {
            get { return _messageContent; }
            set
            { 
                AddToAudit("MessageContent", _messageContent, value);
                _messageContent = value;
            }
        }

        public XDocument MessageXml
        {
            get { return XDocument.Parse(MessageContent); }
        }


        #endregion 


        public KxWesternUnionOrderMessage(KxBaseClass parent):base(parent)
        { }


        public KxWesternUnionOrderMessage(KxBaseClass parent, XmlNode node):base(parent)
        {
             LoadFromXml("", node);
        }

        public KxWesternUnionOrderMessage(KxBaseClass parent,int  messageid):base(parent)
        {
             base.dBIdentifier = messageid;
             Load();
        }


        public override KxData ConfigureData(ActionType actiontype)
        {
            KxData result = new KxData("westernunionordermessages", "dbo.WesternUnionOrderMessages", TargetType.Table);

            result.AutoKeyField = new KxDataValue("MessageId",MessageId,KxValueType.Int);


            if ((actiontype == ActionType.Update) && (MessageId == 0))
            {
                result.Action = ActionType.Insert;
            }
            else
            {
                result.Action = actiontype;
            }

            if ((actiontype == ActionType.Insert) || (actiontype == ActionType.Update))
            {
                result.AddParameter("kxOrderId", _kxOrderId, KxValueType.Int, false);
                result.AddParameter("creationDate", _creationDate, KxValueType.DateTime, false);
                result.AddParameter("messageSource", _messageSource, KxValueType.String, 50, false);
                result.AddParameter("messageContent", _messageContent, KxValueType.String, 2147483647, true);
            }
            return result;
        }


        public override  void LoadFromXml(string xml,XmlNode node)
        {
            dBIdentifier  = KxXmlUtils.GetNodeValue(node,"messageid",0);
            _kxOrderId  = KxXmlUtils.GetNodeValue(node,"kxorderid",0);
            _creationDate  = KxXmlUtils.GetNodeValueDT(node,"creationdate",null);
            _messageSource  = KxXmlUtils.GetNodeValue(node,"messagesource",string.Empty);
            _messageContent  = KxXmlUtils.GetNodeValue(node,"messagecontent",string.Empty);
        }

        public static IEnumerable<KxWesternUnionOrderMessage> GetWesternUnionOrderMessages (int kxOrderId)
        {
            KxData data = new KxData("westernunionordermessages", "dbo.WesternUnionOrderMessages", TargetType.Table);
            data.AddKeyField("kxorderid", kxOrderId, KxValueType.Int);
            if (data.Execute ())
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(data.XmlResults);
                XmlNodeList nodes = doc.SelectNodes(@"//westernunionordermessages/datarow");
                foreach(XmlNode node in nodes)
                {
                     yield return new KxWesternUnionOrderMessage(null, node);
                }
            }
        }
    }
}

