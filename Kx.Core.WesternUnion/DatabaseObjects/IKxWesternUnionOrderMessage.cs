using System;

namespace Kx.Core.WesternUnion.DatabaseObjects
{
    public interface IKxWesternUnionOrderMessage
    {
        int MessageId { get; }
        int KxOrderId { get; set; }
        DateTime? CreationDate { get; set; }
        string MessageSource { get; set; }
        string MessageContent { get; set; }
        string LastError { get; }
        bool Save();
    }
}