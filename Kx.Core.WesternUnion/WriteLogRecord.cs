﻿#region copyright

// =======================================================
// Product:   KCF.Student
// Project:   Kx.Core.WesternUnion
// File:      WriteLogRecord.cs
// Created:   05/02/2018
// By:        Steve Ashley
// Modified:  05/02/2018
// 
// Copyright © Kinetic Solutions Ltd 2018
// All rights reserved.
// 
// No portion of this file may be re-created, copied or otherwise
// used without the express permission of Kinetic Solutions Ltd.
// =======================================================

#endregion

namespace Kx.Core.WesternUnion
{
    /// <summary>
    /// Delegate method for writing audit records to monitor and report the actions of the API
    /// </summary>
    /// <param name="level">The audit level of the message to allow "noise" filtering</param>
    /// <param name="method">The name of the method that has supplied the message</param>
    /// <param name="message">The main contents of the audit message</param>
    /// <param name="additionalData">Any additional information that is relevant to the message</param>
    public delegate void WriteLogRecord(AuditLevel level, string method, string message, string additionalData);

    public delegate IWUWebClient GetWebClient();
}