﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KcfIntegratedSetup
{
    public partial class Form1 : Form
    {
        List<string> _refs = new List<string>() { "Kx.Core", "Kx.Core.PaymentProcessor", "Kx.Core.Web", "Kx.Core.Web.Student", "Kx.Objects.Common", "Kx.Objects.Student", "Kx.Core.WesternUnion" };
        List<string> _guids = new List<string>() { "{17abcf11-496c-4776-8154-95d6aa48d3c3}", "{9cd14660-f1d2-43f6-adcf-0bf81b174b24}", "{51ec5da1-a10f-43d9-bd90-b9bfcc071990}", "{ca65acea-2cf6-42e8-842c-1052cbfb34e9}", "{608c4bc8-3e6d-455d-8204-ae4f98787b11}", "{0d5e380a-514a-44be-8607-135dd4857965}", "{68aadb5e-6d8c-4702-9923-071115bc47d2}" };
        List<string> _refsfound;
        Dictionary<string, string> replaces = new Dictionary<string, string>() { { @"..\..\Common\References\", @"..\Common\References\" } };
        int _DefaultAcademicYear;
        string _ConnectionString;
        public Form1()
        {
            InitializeComponent();
            // Get all subdirectories  
            string[] directoryEntries = Directory.GetDirectories(@"C:\Users\phil.hannington\Documents\Projects\KcfIntegrated");
            // Loop through them to see if they have any other subdirectories  
            foreach (string dir in directoryEntries)
            {
                if (Path.GetFileName(dir).StartsWith("Kx.Student.Web.UI."))
                    cboProjects.Items.Add(Path.GetFileName(dir).Substring(18));
            }
        }
        private bool RemoveRef(string line)
        {
            foreach(string include in _refs)
            {
                if (line.Contains(@"<Reference Include=""" + include + @", "))
                {
                    _refsfound.Add(include);
                    return true;
                }
            }
            return false;
        }
        private string ReplaceBetween(string s, string r, string s1, string s2)
        {
            int i = s.IndexOf(s1);
            if (i > -1)
            {
                int j = s.IndexOf(s2, i + s1.Length);
                s = s.Substring(0,i+s1.Length) + r + s.Substring(j);
            }
            return s;
        }
        private string GetFile(string pattern)
        {
            string f = Directory.GetFiles(@"C:\Users\phil.hannington\Documents\Projects\KcfIntegrated\Kx.Student.Web.UI." + cboProjects.Text, pattern)[0];
            string fbak = Path.Combine(Path.GetDirectoryName(f), "Copy of " + Path.GetFileName(f));
            if (File.Exists(fbak))
                File.Delete(fbak);
            File.Copy(f, fbak);
            return f;
        }
        private void ProcessProjectFile()
        {
            try
            {
                // TO DO - copy files from NAFF folder if need be
                // Test with Prestige - add Classes folder to the project
                int idx = 0;
                _refsfound = new List<string>();
                List<string> outlines = new List<string>();
                string skipTo = "", outline;
                string f = GetFile("*.csproj");
                string[] lines = File.ReadAllLines(f);
                foreach (string line in lines)
                {
                    if (line.Contains("<PropertyGroup>") && lines[idx + 1].Contains("<VisualStudioVersion "))
                    {
                        // this is where we want to insert the project references
                        outlines.Add("  <ItemGroup>");
                        if (_refsfound.Count == 0)
                        {
                            MessageBox.Show("It looks like this project has already been done", "Error");
                            return;
                        }
                        foreach (string rf in _refsfound)
                        {
                            outlines.Add(string.Format(@"    <ProjectReference Include=""..\{0}\{0}.csproj"">", rf));
                            outlines.Add(string.Format(@"      <Project>{0}</Project>", _guids[_refs.IndexOf(rf)]));
                            outlines.Add(string.Format(@"      <Name>{0}</Name>", rf));
                            outlines.Add(@"    </ProjectReference>");
                        }
                        outlines.Add("  </ItemGroup>");
                    }
                    outline = "";
                    if (line.Contains(@"<Reference Include=""ICSharpCode.SharpZipLib, "))
                    {
                        outlines.Add(@"    <Reference Include=""ICSharpCode.SharpZipLib, Version=1.3.2.10, Culture=neutral, PublicKeyToken=1b03e6acf1164f73, processorArchitecture=MSIL"">");
                        outlines.Add(@"      <HintPath>..\packages\SharpZipLib.1.3.2\lib\net45\ICSharpCode.SharpZipLib.dll</HintPath>");
                        outlines.Add(@"    </Reference>");
                        skipTo = " </Reference>";
                    }
                    else if (RemoveRef(line) || line.Contains(@" < Reference Include=""ICSharpCode.SharpZipLib, "))
                    {
                        skipTo = " </Reference>";
                    }

                    else if (skipTo == "")
                    {
                        outline = line;
                        foreach (KeyValuePair<string, string> kvp in replaces)
                        {
                            outline = outline.Replace(kvp.Key, kvp.Value, StringComparison.InvariantCultureIgnoreCase);
                        }
                        if (outline.Contains("<TargetFrameworkVersion>"))
                        {
                            outline = ReplaceBetween(outline, "v4.8", "<TargetFrameworkVersion>", "</TargetFrameworkVersion>");
                        }
                        outlines.Add(outline);
                    }
                    else if (line.Contains(skipTo))
                    {
                        skipTo = "";
                    }
                    idx++;
                }
                File.WriteAllLines(f, outlines);
                string binfolder = Path.Combine(Path.GetDirectoryName(f), "bin");
                if (Directory.Exists(binfolder))
                    Directory.Delete(Path.Combine(Path.GetDirectoryName(f), "bin"), true);
            }
            catch(System.Exception ex)
            {
                MessageBox.Show("ProcessProjectFile errored: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        private string ExtractKeyValue(string s, string attr = "value")
        {
            int i = s.IndexOf(attr);
            if (i  > 0)
            {
                i = s.IndexOf("=", i + 1);
                if (i > 0)
                {
                    i = s.IndexOf(@"""", i + 1);
                    if (i > 0)
                    {
                        i++;
                        int j = s.IndexOf(@"""", i);
                        return s.Substring(i, j - i);
                    }
                }
            }
            return "";
        }
        private void ProcessWebConfigFile(int defaultAcademicYear = 0)
        {
            try
            { 
                List<string> outlines = new List<string>();
                string f = GetFile("web.config");
                string outline;
                bool bUseApplicationObjectsFound = false;
                bool debugkeysdone = false;
                string[] lines = File.ReadAllLines(f);
                foreach (string line in lines)
                {
                    if (line.Contains(@"<add key=""DefaultAcademicYear"""))
                         _DefaultAcademicYear = int.Parse(ExtractKeyValue(line));
                    else if (line.ToLower().Contains(@"<add name=""web"" connectionstring=") || line.Contains(@"<add name=""kx"" connectionstring="))
                        _ConnectionString = ExtractKeyValue(line, "connectionString");
                    outline = line;
                    if (outline.ToLower().Contains(@"<add key=""workflowobjectpath"""))
                        outline = string.Format(@"    <add key=""WorkflowObjectPath"" value=""{0}""/>", Path.Combine(Path.GetDirectoryName(f), "bin"));
                    else if (outline.ToLower().Contains(@"<add key=""workflowfilelocation"""))
                        outline = @"    <add key=""WorkflowFileLocation"" value=""~\App_Data\Workflows""/>";
                    else if (outline.ToLower().Contains(@"<add key=""useapplicationobjects"""))
                    {
                        outline = @"    <add key=""UseApplicationObjects"" value=""True""/>";
                        bUseApplicationObjectsFound = true;
                    }
                    else if (defaultAcademicYear > 0 && outline.Contains(@"<add key=""DefaultAcademicYear"""))
                    {
                        outline = @"    <add key=""DefaultAcademicYear"" value=""" + defaultAcademicYear + @"""/>";
                        _DefaultAcademicYear = defaultAcademicYear;
                    }
                    else if (outline.Contains("<!-- kcfintegrated debug keys  -->"))
                        debugkeysdone = true;
                    else if (outline.Contains("</appSettings>"))
                    {
                        if (bUseApplicationObjectsFound == false)
                            outlines.Add(@"    <add key=""UseApplicationObjects"" value=""True""/>");
                        if (debugkeysdone == false)
                        {
                            outlines.Add("    <!-- kcfintegrated debug keys  -->");
                            outlines.Add(@"    <add key=""DebugSQL"" value=""True""/>");
                            outlines.Add(@"    <add key=""DebugWorkflow"" value=""True""/>");
                            outlines.Add(@"    <add key=""DebugMergeTags"" value=""True""/>");
                            outlines.Add(@"    <add key=""DebugResources"" value=""False""/>");
                            outlines.Add(@"    <add key=""DebugApplicationGuid"" value=""True""/>");
                            outlines.Add(@"    <add key=""DebugDetermineCompletionState"" value=""False""/>");
                            outlines.Add(@"    <add key=""DebugEmail"" value=""True""/>");
                            outlines.Add(@"    <add key=""DebugReflection"" value=""False""/>");
                            outlines.Add(@"    <add key=""DebugRedirects"" value=""True""/>");
                            outlines.Add(@"    <add key=""DebugDataRequest"" value=""True""/>");
                            outlines.Add(@"    <add key=""DebugGetState"" value=""True""/>");
                            outlines.Add(@"    <add key=""DebugNewWebStudentApp"" value=""True""/>");
                            outlines.Add(@"    <add key=""DebugWorkflowSave"" value=""True""/>");
                            outlines.Add(@"    <add key=""DebugDataTarget"" value=""""/>");
                            outlines.Add(@"    <add key=""DebugDynamicFindReplace"" value=""SSAvailabilityROOM|CanSelect = 0|CanSelect = 1""/>");
                            outlines.Add(string.Format(@"    <add key=""DebugUserName"" value=""{0}""/>", GetUserName()));
                            outlines.Add(@"<!--    <add key=""DebugAppHasNoPayments"" value=""False""/>");
                            outlines.Add(@"    <add key=""DebugAppHasNoDeposits"" value=""False""/>-->");
                        }
                    }
                    outlines.Add(outline);
                }
                File.WriteAllLines(f, outlines);
            }
            catch(System.Exception ex)
            {
                MessageBox.Show("ProcessWebConfigFile errored: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }   
        private string QueryDb(SqlConnection conn, string sql)
        {
            try
            { 
                using (SqlCommand command = new SqlCommand(sql, conn))
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            return reader[0].ToString();
                        }
                    }
                }
                return "";
            }
            catch(System.Exception ex)
            {
                MessageBox.Show("QueryDb errored: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return "";
            }
        }

        /// <summary>
        /// Try and get best User to use to test with.
        /// Name is pasted to Clipboard
        /// </summary>
        /// <returns></returns>
        private string GetUserName()
        {
            try
            { 
                ProcessWebConfigFile();
                if (string.IsNullOrEmpty(_ConnectionString))
                {
                    MessageBox.Show("No connection string");
                    return "";
                }
                string sql = string.Format("SELECT TOP 1 UserGUID FROM WebStudentAllocation wsa INNER JOIN St2StudentAllocations sa ON wsa.AllocationId = sa.AllocationID INNER JOIN ST2StudentApplications apps ON sa.ApplicationID = apps.ApplicationID WHERE wsa.AcceptedDate IS NULL AND sa.OfferExpireDate > GETDATE() AND apps.AcademicYearID = {0} ORDER BY wsa.AllocationId DESC", _DefaultAcademicYear);
                using (SqlConnection conn = new SqlConnection(_ConnectionString))
                {
                    conn.Open();
                    string userGuid = QueryDb(conn, sql);   
                    if (string.IsNullOrEmpty(userGuid))
                    {
                        // TO DO
                        sql = string.Format("SELECT TOP 1 UserGUID FROM WebStudentAllocation wsa INNER JOIN St2StudentAllocations sa ON wsa.AllocationId = sa.AllocationID INNER JOIN ST2StudentApplications apps ON sa.ApplicationID = apps.ApplicationID WHERE wsa.AcceptedDate IS NULL AND apps.AcademicYearID = {0} ORDER BY wsa.AllocationId DESC", _DefaultAcademicYear);
                        userGuid = QueryDb(conn, sql);  // Need to set OfferExpireDate
                        if (string.IsNullOrEmpty(userGuid))
                        {
                            sql = string.Format("SELECT TOP 1 UserGUID FROM WebStudentAllocation wsa INNER JOIN St2StudentAllocations sa ON wsa.AllocationId = sa.AllocationID INNER JOIN ST2StudentApplications apps ON sa.ApplicationID = apps.ApplicationID WHERE apps.AcademicYearID = {0} ORDER BY wsa.AllocationId DESC", _DefaultAcademicYear);
                            userGuid = QueryDb(conn, sql);  // Need to set AcceptedDate to null and OfferExpireDate
                            if (string.IsNullOrEmpty(userGuid))
                            {
                                sql = string.Format("SELECT TOP 1 UserGUID FROM WebStudentAllocation wsa INNER JOIN St2StudentAllocations sa ON wsa.AllocationId = sa.AllocationID INNER JOIN ST2StudentApplications apps ON sa.ApplicationID = apps.ApplicationID WHERE apps.AcademicYearID = {0}-1 ORDER BY wsa.AllocationId DESC", _DefaultAcademicYear);
                                userGuid = QueryDb(conn, sql);  // Need to set AcceptedDate to null and OfferExpireDate and AcademicYearID
                            }
                        }
                        else
                        {
                            sql = string.Format("UPDATE sa SET OfferExpireDate = DATEADD(month, 8, GETDATE()) FROM WebStudentAllocation wsa INNER JOIN St2StudentAllocations sa ON wsa.AllocationId = sa.AllocationID INNER JOIN ST2StudentApplications apps ON sa.ApplicationID = apps.ApplicationID WHERE wsa.AcceptedDate IS NULL AND wsa.UserGUID = '{0}'", userGuid);

                        }
                    }
                    string UserName = "", StudentID = "";
                    if (string.IsNullOrEmpty(userGuid) == false)
                    {
                        UserName = QueryDb(conn, "SELECT UserName FROM WebUser WHERE UserGUID ='" + userGuid + "'");
                        StudentID = QueryDb(conn, "SELECT StudentId FROM WebUser INNER JOIN St2Students ON St2Students.KxStudentID = WebUser.KxStudentID WHERE UserGUID ='" + userGuid + "'");
                    }
                    else
                    {
                        UserName = QueryDb(conn, "SELECT TOP 1 UserName FROM WebUser ORDER BY CreationDate DESC");
                        StudentID = QueryDb(conn, "SELECT TOP 1 StudentId FROM WebUser INNER JOIN St2Students ON St2Students.KxStudentID = WebUser.KxStudentID ORDER BY WebUser.CreationDate DESC");
                    }
                    return UserName;
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show("GetUserName errored: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return "";
            }
        }
        private int GetCurrentAcademicYear()
        {
            try
            {
            using (SqlConnection conn = new SqlConnection(_ConnectionString))
                {
                    conn.Open();
                    string ay = QueryDb(conn, "SELECT AcademicYearID FROM ST2AcademicYears WHERE StartDate <= GETDATE() AND EndDate >= GETDATE()");
                    if (int.TryParse(ay, out int intay))
                        return intay;
                    ay = QueryDb(conn, "SELECT MAX(AcademicYearID) FROM ST2AcademicYears WHERE StartDate <= GETDATE() AND EndDate >= GETDATE()");
                    if (int.TryParse(ay, out int intay2))
                        return intay2;
                    return 0;
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show("GetCurrentAcademicYear errored: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return 0;
            }
        }
        private void button1_Click(object sender, EventArgs e)
        {
            if (chkProcessProjectFile.Checked)
                ProcessProjectFile();
            if (chkProcessWebConfig.Checked)
            {
                ProcessWebConfigFile();
                int ay = GetCurrentAcademicYear();
                if (ay > 0 && ay != _DefaultAcademicYear)
                    ProcessWebConfigFile(ay);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Clipboard.SetText(GetUserName()); 
        }
    }
}
