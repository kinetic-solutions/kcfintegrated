﻿
namespace KcfIntegratedSetup
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.cboProjects = new System.Windows.Forms.ComboBox();
            this.button2 = new System.Windows.Forms.Button();
            this.chkProcessWebConfig = new System.Windows.Forms.CheckBox();
            this.chkProcessProjectFile = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(396, 74);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(94, 29);
            this.button1.TabIndex = 1;
            this.button1.Text = "Go";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // cboProjects
            // 
            this.cboProjects.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboProjects.FormattingEnabled = true;
            this.cboProjects.Location = new System.Drawing.Point(41, 12);
            this.cboProjects.Name = "cboProjects";
            this.cboProjects.Size = new System.Drawing.Size(449, 28);
            this.cboProjects.TabIndex = 2;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(263, 196);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(227, 29);
            this.button2.TabIndex = 3;
            this.button2.Text = "Copy User Name to Clipboard";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // chkProcessWebConfig
            // 
            this.chkProcessWebConfig.AutoSize = true;
            this.chkProcessWebConfig.Checked = true;
            this.chkProcessWebConfig.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkProcessWebConfig.Location = new System.Drawing.Point(41, 90);
            this.chkProcessWebConfig.Name = "chkProcessWebConfig";
            this.chkProcessWebConfig.Size = new System.Drawing.Size(157, 24);
            this.chkProcessWebConfig.TabIndex = 10;
            this.chkProcessWebConfig.Text = "Process web.config";
            this.chkProcessWebConfig.UseVisualStyleBackColor = true;
            // 
            // chkProcessProjectFile
            // 
            this.chkProcessProjectFile.AutoSize = true;
            this.chkProcessProjectFile.Checked = true;
            this.chkProcessProjectFile.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkProcessProjectFile.Location = new System.Drawing.Point(41, 63);
            this.chkProcessProjectFile.Name = "chkProcessProjectFile";
            this.chkProcessProjectFile.Size = new System.Drawing.Size(157, 24);
            this.chkProcessProjectFile.TabIndex = 9;
            this.chkProcessProjectFile.Text = "Process Project File";
            this.chkProcessProjectFile.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(516, 246);
            this.Controls.Add(this.chkProcessWebConfig);
            this.Controls.Add(this.chkProcessProjectFile);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.cboProjects);
            this.Controls.Add(this.button1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ComboBox cboProjects;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.CheckBox chkProcessWebConfig;
        private System.Windows.Forms.CheckBox chkProcessProjectFile;
    }
}

