﻿#region copyright

// =======================================================
// Product:   KCF.Student.PayToStudy
// Project:   Kx.PayToStudy.Service
// File:      PayToStudyResponseElement.cs
// Created:   02/11/2016
// By:        Steve Ashley
// Modified:  02/11/2016
// 
// Copyright © Kinetic Solutions Ltd 2016
// All rights reserved.
// 
// No portion of this file may be re-created, copied or otherwise
// used without the express permission of Kinetic Solutions Ltd.
// =======================================================

#endregion

#region usings

using System;
using System.Xml.Linq;
using System.Xml.XPath;

#endregion

namespace Kx.Core.Web.Student.PayToStudy
{
    public class PayToStudyHistoryResponseElement
    {
        #region Protected Methods

        protected string GetValueFromXPath(XElement response, string xPath)
        {
            try
            {
                string result = response.XPathSelectElement(xPath).Value;
                return result;
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }

        protected string GetValueFromXPath(XDocument response, string xPath)
        {
            try
            {
                string result = response.XPathSelectElement(xPath).Value;
                return result;
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }

        #endregion
    }
}