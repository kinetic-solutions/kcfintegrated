﻿#region copyright

// =======================================================
// Product:   KCF.Student
// Project:   Kx.Core.Web.Student
// File:      PayToStudyRequests.cs
// Created:   22/11/2016
// By:        Steve Ashley
// Modified:  22/11/2016
// 
// Copyright © Kinetic Solutions Ltd 2016
// All rights reserved.
// 
// No portion of this file may be re-created, copied or otherwise
// used without the express permission of Kinetic Solutions Ltd.
// =======================================================

#endregion

#region usings

using System;
using Kx.Core.Data;
using Kx.Core.Exceptions;

#endregion

namespace Kx.Core.Web.Student.PayToStudy
{
    public class PayToStudyRequests : IDisposable
    {
        #region Interface Methods

        public void Dispose()
        {
            // Nothing...
        }

        #endregion

        #region Public Methods

        public bool ApplicationHasPayToStudyPayments(Guid applicationGuid, string paymentType)
        {
            KxData data = new KxData("result", string.Format("SELECT COUNT(*) AS Result FROM WebPaymentCache PC WHERE PC.Provider = 'PAYTOSTUDY' AND PC.AuthorisationCode IS NOT NULL AND PC.TransactionGuid = '{0:D}' AND '{1}' IN ('', (SELECT TOP 1 PaymentType FROM WebPaymentCachePayment P WHERE P.CacheID = PC.CacheID))", applicationGuid, paymentType), TargetType.Static);
            int? result = (int?) data.ExecuteScalar();
            if (string.IsNullOrWhiteSpace(data.LastError) && result.HasValue)
            {
                return result.Value > 0;
            }
            else
            {
                KxLogger.LogActivity(this, "ApplicationHasPayToStudyPayments", data.LastError, applicationGuid.ToString("D"));
            }

            return false;
        }

        public bool ApplicationHasPendingPayToStudyPayments(Guid applicationGuid, string paymentType)
        {
            KxData data = new KxData("result", string.Format("SELECT COUNT(*) AS Result FROM WebPaymentCache PC WHERE PC.Provider = 'PAYTOSTUDY' AND PC.AuthorisationCode IS NOT NULL AND PC.Authorised = 0 AND PC.TransactionGuid = '{0:D}' AND '{1}' IN ('', (SELECT TOP 1 PaymentType FROM WebPaymentCachePayment P WHERE P.CacheID = PC.CacheID))", applicationGuid, paymentType), TargetType.Static);
            int? result = (int?) data.ExecuteScalar();
            if (string.IsNullOrWhiteSpace(data.LastError) && result.HasValue)
            {
                return result.Value > 0;
            }
            else
            {
                KxLogger.LogActivity(this, "ApplicationHasPayToStudyPayments", data.LastError, applicationGuid.ToString("D"));
            }

            return false;
        }

        public bool WebUserHasPayToStudyPayments(Guid userGuid, string paymentType)
        {
            KxData data = new KxData("result", string.Format("SELECT COUNT(*) AS Result FROM WebPaymentCache PC JOIN WebStudentApplication WSA ON WSA.ApplicationGUID = PC.TransactionGUID WHERE Provider = 'PAYTOSTUDY' AND AuthorisationCode IS NOT NULL AND WSA.UserGUID = '{0:D}' AND '{1}' IN ('', (SELECT TOP 1 PaymentType FROM WebPaymentCachePayment P WHERE P.CacheID = PC.CacheID))", userGuid, paymentType), TargetType.Static);
            int? result = (int?) data.ExecuteScalar();
            if (string.IsNullOrWhiteSpace(data.LastError) && result.HasValue)
            {
                return result.Value > 0;
            }
            else
            {
                KxLogger.LogActivity(this, "WebUserHasPayToStudyPayments", data.LastError, userGuid.ToString("D"));
            }

            return false;
        }

        public bool WebUserHasPendingPayToStudyPayments(Guid userGuid, string paymentType)
        {
            KxData data = new KxData("result", string.Format("SELECT COUNT(*) AS Result FROM WebPaymentCache PC JOIN WebStudentApplication WSA ON WSA.ApplicationGUID = PC.TransactionGUID WHERE Provider = 'PAYTOSTUDY' AND AuthorisationCode IS NOT NULL AND PC.Authorised = 0 AND WSA.UserGUID = '{0:D}' AND '{1}' IN ('', (SELECT TOP 1 PaymentType FROM WebPaymentCachePayment P WHERE P.CacheID = PC.CacheID))", userGuid, paymentType), TargetType.Static);
            int? result = (int?) data.ExecuteScalar();
            if (string.IsNullOrWhiteSpace(data.LastError) && result.HasValue)
            {
                return result.Value > 0;
            }
            else
            {
                KxLogger.LogActivity(this, "WebUserHasPayToStudyPayments", data.LastError, userGuid.ToString("D"));
            }

            return false;
        }

        #endregion
    }
}