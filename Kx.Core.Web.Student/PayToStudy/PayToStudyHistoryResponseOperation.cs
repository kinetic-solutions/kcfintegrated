﻿#region copyright

// =======================================================
// Product:   KCF.Student.PayToStudy
// Project:   Kx.PayToStudy.Service
// File:      PayToStudyResponseOperation.cs
// Created:   02/11/2016
// By:        Steve Ashley
// Modified:  02/11/2016
// 
// Copyright © Kinetic Solutions Ltd 2016
// All rights reserved.
// 
// No portion of this file may be re-created, copied or otherwise
// used without the express permission of Kinetic Solutions Ltd.
// =======================================================

#endregion

#region usings

using System;
using System.Xml.Linq;

#endregion

namespace Kx.Core.Web.Student.PayToStudy
{
    public class PayToStudyHistoryResponseOperation : PayToStudyHistoryResponseElement
    {
        #region Fields and Properties

        public string Type { get; set; }
        public string Result { get; set; }

        public bool Successful
        {
            get { return StringComparer.InvariantCultureIgnoreCase.Compare("ok", Result) == 0; }
        }

        public string ErrorCode { get; set; }
        public string ErrorMessage { get; set; }
        public string Payments { get; set; }
        public string Date { get; set; }
        public string TimeGMT { get; set; }

        #endregion

        #region Constructors

        public PayToStudyHistoryResponseOperation(XDocument response)
        {
            DecodeReponse(response);
        }

        #endregion

        #region Private Methods

        private void DecodeReponse(XDocument response)
        {
            Type = GetValueFromXPath(response, "//root/operation/type");
            Result = GetValueFromXPath(response, "//root/operation/result");
            Date = GetValueFromXPath(response, "//root/operation/date");
            TimeGMT = GetValueFromXPath(response, "//root/operation/timeGMT");

            if (Successful)
            {
                Payments = GetValueFromXPath(response, "//root/operation/payments");
            }
            else
            {
                ErrorCode = GetValueFromXPath(response, "//root/operation/errorCode");
                ErrorMessage = GetValueFromXPath(response, "//root/operation/errorMessage");
            }
        }

        #endregion
    }
}