﻿#region copyright

// =======================================================
// Product:   KCF.Student.PayToStudy
// Project:   Kx.PayToStudy.Service
// File:      PayToStudyResponse.cs
// Created:   02/11/2016
// By:        Steve Ashley
// Modified:  02/11/2016
// 
// Copyright © Kinetic Solutions Ltd 2016
// All rights reserved.
// 
// No portion of this file may be re-created, copied or otherwise
// used without the express permission of Kinetic Solutions Ltd.
// =======================================================

#endregion

#region usings

using System.Collections.Generic;
using System.Xml.Linq;
using System.Xml.XPath;

#endregion

namespace Kx.Core.Web.Student.PayToStudy
{
    public class PayToStudyHistoryResponse
    {
        #region Fields and Properties

        public string RawResponse { get; set; }

        public PayToStudyHistoryResponseOperation Operation { get; private set; }
        public List<PayToStudyHistoryResponsePayment> Payments { get; private set; } = new List<PayToStudyHistoryResponsePayment>();

        #endregion

        #region Constructors

        public PayToStudyHistoryResponse(string rawResponse)
        {
            RawResponse = rawResponse;

            DecodeResponse();
        }

        #endregion

        #region Private Methods

        private void DecodeResponse()
        {
            XDocument doc = XDocument.Parse(RawResponse);

            Operation = new PayToStudyHistoryResponseOperation(doc);

            if (Operation.Successful)
            {
                foreach (XElement payment in doc.XPathSelectElements("//root/payments/payment"))
                {
                    Payments.Add(new PayToStudyHistoryResponsePayment(payment));
                }
            }
        }

        #endregion
    }
}