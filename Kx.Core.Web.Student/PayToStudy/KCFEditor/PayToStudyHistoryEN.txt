﻿<!-- PayToStudyHistoryEN -->
<div id="PageContent" class="col-xs-12">
    <div class="col-sm-12">
        <div id="chat_holder">
            <a target="newwindow_chat" onclick="var i = window.open(this.href, this.target, 'height=500,width=540,scrollbars=yes,toolbar=no,resizable=yes,status=yes,menubar=no');return false;" href='https://chatserver.comm100.com/ChatWindow.aspx?planId=300&visitType=1&byHref=1&partnerId=-1&siteid=216282'>Live Chat</a>
        </div>
        <div class="col-xs-12">
            <div class="jumbotron jumbotron-primary">
                <h1 class="col-sm-7 col-xs-12">Payment transfer submitted</h1>
                <div class="col-sm-5 col-xs-12 pull-right" style="background: url('https://paytostudy.com/templates/partners/images/paytostudyLogo.png') no-repeat top right; height: 120px;">&nbsp;</div>
                <div class="clearfix"></div>
                <p>Below you will see all previous requests and their current status.</p>
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>Payment details</th>
                        <th>Amount</th>
                        <th>FX Amount</th>
                        <th>Registered</th>
                        <th>Paid</th>
                        <th>Status</th>
                        <th>Reference</th>
                    </tr>
                    </thead>
                    <tbody data-control-target-table="PaymentHistory"></tbody>
                </table>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        function onComplete(){
            $('.bankNotes').each(function() {
                if($(this).text().trim() === ''){
                    $(this).hide();
                }
            });
        }
        
        KCF.OnComplete = onComplete;
    </script>
    <!-- NAVIGATION POSSIBLY GOES HERE -->
</div>