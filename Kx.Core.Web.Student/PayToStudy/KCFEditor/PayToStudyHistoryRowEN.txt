﻿<!--PayToStudyHistoryRowEN-->
<tr>
    <td>{KX_PCPAYTOSTUDYHISTORYIDX_PNPayingFor}</td>
    <td>{KX_PCPAYTOSTUDYHISTORYIDX_PNPaidAmount}</td>
    <td>{KX_PCPAYTOSTUDYHISTORYIDX_PNPayableAmount}</td>
    <td>{KX_PCPAYTOSTUDYHISTORYIDX_PNDateGMT}</td>
    <td>{KX_PCPAYTOSTUDYHISTORYIDX_PNPaidDate_ND1}</td>
    <td>{KX_PCPAYTOSTUDYHISTORYIDX_PNStatus}</td>
    <td>{KX_PCPAYTOSTUDYHISTORYIDX_PNTransactionID}</td>
</tr>
<tr>
    <td colspan="7">
    <a data-displaycontrolbool="'{KX_PCPAYTOSTUDYHISTORYIDX_PNHasOriginalResponse}'==='True'" href="#" onclick="$('#{KX_PCPAYTOSTUDYHISTORYIDX_PNRequestID}').toggle('fast'); return false;">Details</a>
        <div ID="{KX_PCPAYTOSTUDYHISTORYIDX_PNRequestID}" style="width: 100%; display:none;">
            <div style="width: 100%;background:white; padding: 3px;margin:5px 0 5px -3px; border: 1px solid black;">
                <pre>{KX_PCPAYTOSTUDYHISTORYIDXORIGINALRESPONSE_PNPaymentInstructions}</pre>
                <div data-displaycontrolbool="'{KX_PCPAYTOSTUDYHISTORYIDXORIGINALRESPONSE_PNHasPaymentInstuctions}'!=='True'">
                    <h3 style="color: red;">Please be aware for each payment/instalment you must create a new payment registration</h3>
                    <p>We must receive ou payment in our account within 48 hours. You will need to allow 1 working day for the funds to reach our account.</p>
                    <p>Transfer the funds to our account and ensure that you enter your name and date of birth or the below reference number ({KX_PCPAYTOSTUDYHISTORYIDXORIGINALRESPONSE_PNTransactionId}), so we can allocate your payment. <span style="color: red;">(Cash, Drafts or Cheques will not be accepted)</span></p>
                    <p>To fulfil the FX Contract, you have entered into with PayToStudy please send {KX_PCPAYTOSTUDYHISTORYIDXORIGINALRESPONSE_PNCurrencyAbbr}. Sending any other currency will incur extra cost due to foreign exchange fees.</p>
                    <p>
                    Using your smart phone or tablet, take a picture of the below documents and email to confirm@paytostudy.com
                    <ul>
                    <li>Transfer receipt</li>
                    <li>Passport</li>
                    <li>Offer letter / Invoice / Tenancy Agreement</li>
                    </ul>
                    </p>
                    <p>To check the status of your payment, you may log into your online tracking account after 24 hours.</p>
                </div>
                <h2>Bank transfer details</h2>
                <div class="form">
                    <div class="col-xs-12 col-sm-6">
                        <div class="form-group" data-displaycontrol="{KX_PCPAYTOSTUDYHISTORYIDXORIGINALRESPONSE_PNTransactionId}">
                            <label class="">Transaction ID</label>
                            <span>{KX_PCPAYTOSTUDYHISTORYIDXORIGINALRESPONSE_PNTransactionId}</span>
                        </div>
                        <div class="form-group" data-displaycontrol="{KX_PCPAYTOSTUDYHISTORYIDXORIGINALRESPONSE_PNType}">
                            <label class="">Transaction Type</label>
                            <span>{KX_PCPAYTOSTUDYHISTORYIDXORIGINALRESPONSE_PNType}</span>
                        </div>
                        <div class="form-group" data-displaycontrol="{KX_PCPAYTOSTUDYHISTORYIDXORIGINALRESPONSE_PNParticular}">
                            <label class="">Particular</label>
                            <span>{KX_PCPAYTOSTUDYHISTORYIDXORIGINALRESPONSE_PNParticular}</span>
                        </div>
                        <div class="form-group" data-displaycontrol="{KX_PCPAYTOSTUDYHISTORYIDXORIGINALRESPONSE_PNCurrencyAbbr}">
                            <label class="">Currency</label>
                            <span>{KX_PCPAYTOSTUDYHISTORYIDXORIGINALRESPONSE_PNCurrencyAbbr}</span>
                        </div>
                        <div class="form-group" data-displaycontrol="{KX_PCPAYTOSTUDYHISTORYIDXORIGINALRESPONSE_PNReferenceNumber}">
                            <label class="">Reference Number</label>
                            <span>{KX_PCPAYTOSTUDYHISTORYIDXORIGINALRESPONSE_PNReferenceNumber}</span>
                        </div>
                        <div class="form-group" data-displaycontrol="{KX_PCPAYTOSTUDYHISTORYIDXORIGINALRESPONSE_PNAmount}">
                            <label class="">Amount to pay</label>
                            <span>{KX_PCPAYTOSTUDYHISTORYIDXORIGINALRESPONSE_PNCurrency} {KX_PCPAYTOSTUDYHISTORYIDXORIGINALRESPONSE_PNAmount}</span>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <div class="form-group" data-displaycontrol="{KX_PCPAYTOSTUDYHISTORYIDXORIGINALRESPONSE_PNBankName}">
                            <label class="">Bank to make payment to</label>
                            <div data-displaycontrol="{KX_PCPAYTOSTUDYHISTORYIDXORIGINALRESPONSE_PNBankName}">{KX_PCPAYTOSTUDYHISTORYIDXORIGINALRESPONSE_PNBankName}</div>
                            <div data-displaycontrol="{KX_PCPAYTOSTUDYHISTORYIDXORIGINALRESPONSE_PNBankBranchName}">{KX_PCPAYTOSTUDYHISTORYIDXORIGINALRESPONSE_PNBankBranchName}</div>
                            <div>{KX_PCPAYTOSTUDYHISTORYIDXORIGINALRESPONSE_PNBankAddress}</div>
                        </div>
                        <div class="form-group" data-displaycontrol="{KX_PCPAYTOSTUDYHISTORYIDXORIGINALRESPONSE_PNBankBranchCode}">
                            <label class="">Bank branch code</label>
                            <span>{KX_PCPAYTOSTUDYHISTORYIDXORIGINALRESPONSE_PNBankBranchCode}</span>
                        </div>
                        <div class="form-group" data-displaycontrol="{KX_PCPAYTOSTUDYHISTORYIDXORIGINALRESPONSE_PNAccountName}">
                            <label class="">Account Name</label>
                            <span>{KX_PCPAYTOSTUDYHISTORYIDXORIGINALRESPONSE_PNAccountName}</span>
                        </div>
                        <div class="form-group" data-displaycontrol="{KX_PCPAYTOSTUDYHISTORYIDXORIGINALRESPONSE_PNSortCode}">
                            <label class="">Sort Code</label>
                            <span>{KX_PCPAYTOSTUDYHISTORYIDXORIGINALRESPONSE_PNSortCode}</span>
                        </div>
                        <div class="form-group" data-displaycontrol="{KX_PCPAYTOSTUDYHISTORYIDXORIGINALRESPONSE_PNAccountNumber}">
                            <label class="">Account Number</label>
                            <span>{KX_PCPAYTOSTUDYHISTORYIDXORIGINALRESPONSE_PNAccountNumber}</span>
                        </div>
                        <div class="form-group" data-displaycontrol="{KX_PCPAYTOSTUDYHISTORYIDXORIGINALRESPONSE_PNiban}">
                            <label class="">IBAN</label>
                            <span>{KX_PCPAYTOSTUDYHISTORYIDXORIGINALRESPONSE_PNiban}</span>
                        </div>
                        <div class="form-group" data-displaycontrol="{KX_PCPAYTOSTUDYHISTORYIDXORIGINALRESPONSE_PNswift}">
                            <label class="">SWIFT</label>
                            <span>{KX_PCPAYTOSTUDYHISTORYIDXORIGINALRESPONSE_PNswift}</span>
                        </div>
                        <div class="form-group" data-displaycontrol="{KX_PCPAYTOSTUDYHISTORYIDXORIGINALRESPONSE_PNaba}">
                            <label class="">Fedwire / Routing Number / ABA Number</label>
                            <span>{KX_PCPAYTOSTUDYHISTORYIDXORIGINALRESPONSE_PNaba}</span>
                        </div>
                        <div class="form-group" data-displaycontrol="{KX_PCPAYTOSTUDYHISTORYIDXORIGINALRESPONSE_PNrtgs}">
                            <label class="">RTGS Number</label>
                            <span>{KX_PCPAYTOSTUDYHISTORYIDXORIGINALRESPONSE_PNrtgs}</span>
                        </div>
                        <div class="form-group" data-displaycontrol="{KX_PCPAYTOSTUDYHISTORYIDXORIGINALRESPONSE_PNifsc}">
                            <label class="">IFSC</label>
                            <span>{KX_PCPAYTOSTUDYHISTORYIDXORIGINALRESPONSE_PNifsc}</span>
                        </div>
                        <div class="form-group" data-displaycontrol="{KX_PCPAYTOSTUDYHISTORYIDXORIGINALRESPONSE_PNTransitFIFcode}">
                            <label class="">Transit / FIF (Branch) Code</label>
                            <span>{KX_PCPAYTOSTUDYHISTORYIDXORIGINALRESPONSE_PNTransitFIFcode}</span>
                        </div>
                        <div class="form-group" data-displaycontrol="{KX_PCPAYTOSTUDYHISTORYIDXORIGINALRESPONSE_PNbsb}">
                            <label class="">BSB</label>
                            <span>{KX_PCPAYTOSTUDYHISTORYIDXORIGINALRESPONSE_PNbsb}</span>
                        </div>
                        <div class="form-group" data-displaycontrol="{KX_PCPAYTOSTUDYHISTORYIDXORIGINALRESPONSE_PNbic}">
                            <label class="">BIC</label>
                            <span>{KX_PCPAYTOSTUDYHISTORYIDXORIGINALRESPONSE_PNbic}</span>
                        </div>
                        <div class="form-group" data-displaycontrol="{KX_PCPAYTOSTUDYHISTORYIDXORIGINALRESPONSE_PNCompanyAddress}">
                            <label class="">Company Address</label>
                            <span>{KX_PCPAYTOSTUDYHISTORYIDXORIGINALRESPONSE_PNCompanyAddress}</span>
                        </div>
                    </div>
                    <div class="bankNotes">
                        <div class="form-group col-xs-12 bg-primary">
                            {KX_PCPAYTOSTUDYHISTORYIDXORIGINALRESPONSE_PNBankNotes}
                        </div>
                    </div>
                    <div class="col-xs-12" data-displaycontrol="{KX_PCPAYTOSTUDYHISTORYIDXORIGINALRESPONSE_PNTax1Name}">
                        <p>Taxes listed below are already applied to the amount above.</p>
                        
                        <div class="form-group" data-displaycontrol="{KX_PCPAYTOSTUDYHISTORYIDXORIGINALRESPONSE_PNTax1Name}">
                            <span>{KX_PCPAYTOSTUDYHISTORYIDXORIGINALRESPONSE_PNTax1Name}</span>&nbsp;{KX_PCPAYTOSTUDYHISTORYIDXORIGINALRESPONSE_PNTax1Amount}&nbsp;<span data-displaycontrol="{KX_PCPAYTOSTUDYHISTORYIDXORIGINALRESPONSE_PNTax1Description}">({KX_PCPAYTOSTUDYHISTORYIDXORIGINALRESPONSE_PNTax1Description})</span>
                        </div>
                        <div class="form-group" data-displaycontrol="{KX_PCPAYTOSTUDYHISTORYIDXORIGINALRESPONSE_PNTax2Name}">
                            <span>{KX_PCPAYTOSTUDYHISTORYIDXORIGINALRESPONSE_PNTax2Name}</span>&nbsp;{KX_PCPAYTOSTUDYHISTORYIDXORIGINALRESPONSE_PNTax2Amount}&nbsp;<span data-displaycontrol="{KX_PCPAYTOSTUDYHISTORYIDXORIGINALRESPONSE_PNTax2Description}">({KX_PCPAYTOSTUDYHISTORYIDXORIGINALRESPONSE_PNTax2Description})</span>
                        </div>
                        <div class="form-group" data-displaycontrol="{KX_PCPAYTOSTUDYHISTORYIDXORIGINALRESPONSE_PNTax3Name}">
                            <span>{KX_PCPAYTOSTUDYHISTORYIDXORIGINALRESPONSE_PNTax3Name}</span>&nbsp;{KX_PCPAYTOSTUDYHISTORYIDXORIGINALRESPONSE_PNTax3Amount}&nbsp;<span data-displaycontrol="{KX_PCPAYTOSTUDYHISTORYIDXORIGINALRESPONSE_PNTax3Description}">({KX_PCPAYTOSTUDYHISTORYIDXORIGINALRESPONSE_PNTax3Description})</span>
                        </div>
                        <div class="form-group" data-displaycontrol="{KX_PCPAYTOSTUDYHISTORYIDXORIGINALRESPONSE_PNTax4Name}">
                            <span>{KX_PCPAYTOSTUDYHISTORYIDXORIGINALRESPONSE_PNTax4Name}</span>&nbsp;{KX_PCPAYTOSTUDYHISTORYIDXORIGINALRESPONSE_PNTax4Amount}&nbsp;<span data-displaycontrol="{KX_PCPAYTOSTUDYHISTORYIDXORIGINALRESPONSE_PNTax4Description}">({KX_PCPAYTOSTUDYHISTORYIDXORIGINALRESPONSE_PNTax4Description})</span>
                        </div>
                        <div class="form-group" data-displaycontrol="{KX_PCPAYTOSTUDYHISTORYIDXORIGINALRESPONSE_PNTax5Name}">
                            <span>{KX_PCPAYTOSTUDYHISTORYIDXORIGINALRESPONSE_PNTax5Name}</span>&nbsp;{KX_PCPAYTOSTUDYHISTORYIDXORIGINALRESPONSE_PNTax5Amount}&nbsp;<span data-displaycontrol="{KX_PCPAYTOSTUDYHISTORYIDXORIGINALRESPONSE_PNTax5Description}">({KX_PCPAYTOSTUDYHISTORYIDXORIGINALRESPONSE_PNTax5Description})</span>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </td>
</tr>