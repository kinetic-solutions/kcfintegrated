﻿#region copyright

// =======================================================
// Product:   KCF.Student.PayToStudy
// Project:   Kx.PayToStudy.Service
// File:      PayToStudyResponsePayment.cs
// Created:   02/11/2016
// By:        Steve Ashley
// Modified:  02/11/2016
// 
// Copyright © Kinetic Solutions Ltd 2016
// All rights reserved.
// 
// No portion of this file may be re-created, copied or otherwise
// used without the express permission of Kinetic Solutions Ltd.
// =======================================================

#endregion

#region usings

using System;
using System.Xml.Linq;
using Kx.Core.PaymentProcessor.Cache;
using Kx.Core.PaymentProcessor.Payments;
using Kx.Core.Web.Student.Payments;
using Kx.Core.Data;
using Kx.Core.Exceptions;
using System.Xml;
using Kx.Core.Utility;

#endregion

namespace Kx.Core.Web.Student.PayToStudy
{
    public class PayToStudyHistoryResponsePayment : PayToStudyHistoryResponseElement
    {
        #region Fields and Properties

        public string Status { get; set; }

        public bool ReadyToProcess
        {
            get { return Status == "Paid"; }
        }

        public bool PaymentActive
        {
            get { return Status == "Paid" || Status == "Registered" || Status == "Pending"; }
        }

        public string TransactionID { get; set; }
        public string StudentName { get; set; }
        public string StudentNumber { get; set; }
        public string StudentAddress { get; set; }
        public string PaidAmount { get; set; }


        public string PayableAmount { get; set; }
        public string ReceivedAmount { get; set; }

        public decimal ReceivedAmountValue
        {
            get { return GetPaymentValue(ReceivedAmount); }
        }

        public string PayingFor { get; set; }
        public string ReferenceNumber { get; set; }
        public string DateGMT { get; set; }
        public string PaidDate { get; set; }
        public string LastUpdated { get; set; }
        public string UniqueStudentID { get; set; }
        public string UniquePayerID { get; set; }
        public string BatchID { get; set; }
        public string RequestID { get; set; }
        public string Payer { get; set; }
        public string PayerName { get; set; }
        public string ADIID { get; set; }
        public string ORGID { get; set; }

        private PayToStudyPaymentRequestResponse _originalResponse;
        public PayToStudyPaymentRequestResponse OriginalResponse
        {
            get
            {
                if (_originalResponse == null)
                {
                    _originalResponse = GetOriginalResponse();
                }

                return _originalResponse ?? new PayToStudyPaymentRequestResponse();
            }
        }

        public bool HasOriginalResponse
        {
            get { return OriginalResponse.RequestId == RequestID; }
        }

        private PayToStudyPaymentRequestResponse GetOriginalResponse()
        {
            try
            {
                Guid cacheId = Guid.Parse(RequestID);
                KxData data = new KxData("row", string.Format("SELECT Raw FROM WebStudentPaymentAudit WHERE Reference = '{0:D}' AND Direction = 'IN' AND Raw LIKE '%<root>%'", cacheId), TargetType.Static);
                if (data.Execute())
                {
                    foreach (XmlNode node in data.GetNodesFromData())
                    {
                        string originalXml = KxXmlUtils.GetNodeValue(node, "raw", string.Empty);
                        originalXml = originalXml.Replace("Xml=", "");
                        XDocument doc = XDocument.Parse(originalXml);
                        return new PayToStudyPaymentRequestResponse(doc);
                    }
                }
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "GetOriginalResponse", ex.Message, RequestID);
            }
            return null;
        }

        #endregion

        #region Constructors

        public PayToStudyHistoryResponsePayment(XElement payment)
        {
            DecodeResponse(payment);
        }

        #endregion

        #region Private Methods

        private decimal GetPaymentValue(string inputString)
        {
            string[] paidAmountElements = inputString.Split(new char[] {' '}, StringSplitOptions.RemoveEmptyEntries);
            string amountString = paidAmountElements[paidAmountElements.Length - 1];
            decimal amount = 0m;
            decimal.TryParse(amountString, out amount);
            return amount;
        }

        private void DecodeResponse(XElement payment)
        {
            Status = GetValueFromXPath(payment, "Status");
            TransactionID = GetValueFromXPath(payment, "TransactionID");
            StudentName = GetValueFromXPath(payment, "StudentName");
            StudentNumber = GetValueFromXPath(payment, "StudentNumber");
            StudentAddress = GetValueFromXPath(payment, "StudentAddress");
            PaidAmount = GetValueFromXPath(payment, "PaidAmount");
            PayableAmount = GetValueFromXPath(payment, "PayableAmount");
            ReceivedAmount = GetValueFromXPath(payment, "ReceivedAmount");
            PayingFor = GetValueFromXPath(payment, "PayingFor");
            ReferenceNumber = GetValueFromXPath(payment, "ReferenceNumber");
            DateGMT = GetValueFromXPath(payment, "DateGMT");
            PaidDate = GetValueFromXPath(payment, "PaidDate");
            LastUpdated = GetValueFromXPath(payment, "LastUpdated");
            UniqueStudentID = GetValueFromXPath(payment, "UniqueStudentID");
            UniquePayerID = GetValueFromXPath(payment, "UniquePayerID");
            BatchID = GetValueFromXPath(payment, "BatchID");
            RequestID = GetValueFromXPath(payment, "RequestID");
            Payer = GetValueFromXPath(payment, "Payer");
            PayerName = GetValueFromXPath(payment, "PayerName");
            ADIID = GetValueFromXPath(payment, "ADIID");
            ORGID = GetValueFromXPath(payment, "ORGID");
        }

        #endregion
    }
}