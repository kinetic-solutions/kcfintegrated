﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kx.Core.Web.Student.PayToStudy
{
    public interface IHaveSettingsForPayToStudy
    {
        string PTSUsername { get; }
        string PTSPassword { get; }
        string PaymentUrl { get; }
    }
}
