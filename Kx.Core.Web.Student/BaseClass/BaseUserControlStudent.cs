﻿using System;
using System.Collections.Generic;
using System.Text;
using Kx.Core.Web.Student.WebApplication;
using Kx.Objects.Student.Web;
using Kx.Core.Web.BaseClass;

namespace Kx.Core.Web.Student.BaseClass
{
    public class BaseUserControlStudent : BaseUserControl
    {
        public KxWebApplicationStudent WebApp
        {
            get
            {
                try
                {
                    return ((KxWebApplicationStudent)Session["WebMgr"]);
                }
                catch
                {
                    return null;
                }
            }
        }

        public KxWebUserStudent WebUser
        {
            get
            {
                return WebApp.WorkflowInterface.WebUser;
            }
        }
    }
}
