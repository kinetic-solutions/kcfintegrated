﻿<ol>
<li>The WesternUnion handler <strong>MUST</strong> be specified in the web.config</li>
<li>&lt;add name=&quot;WesternUnionPDFInstructionHandler&quot; verb=&quot;GET&quot; path=&quot;wuinstructions.kx&quot; type=&quot;Kx.Core.Web.Student.WebApplication.Handlers.WesternUnionPDFInstructionHandler, Kx.Core.Web.Student&quot;/&gt;</li>
<li>Ensure the API username and passwords are in the web.config: &lt;add key=&quot;WesternUnion.API.Username&quot; value=&quot;username&quot;/&gt; -  &lt;add key=&quot;WesternUnion.API.Password&quot; value=&quot;password&quot;/&gt;</li>
</ol>