﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Kx.Core.Utility;

namespace Kx.Core.Web.Student.WesternUnion
{
    public class WesternUnionPaymentHistory
    {
        public Guid CacheId { get; set; }
        public DateTime? CreationDate { get; set; }
        public string DealReference { get; private set; }
        public string OrderId { get; private set; }
        public string Status { get; private set; }
        public DateTime? LastStatusChange { get; private set; }
        public decimal Amount { get; private set; }

        public int KxOrderId { get; private set; }

        public WesternUnionPaymentHistory(XmlNode node)
        {
            CacheId = KxXmlUtils.GetNodeValue(node, "cacheid", Guid.Empty);
            KxOrderId = KxXmlUtils.GetNodeValue(node, "kxorderid", 0);
            CreationDate = KxXmlUtils.GetNodeValueDT(node, "creationdate", null);
            DealReference = KxXmlUtils.GetNodeValue(node, "wudealreference", string.Empty);
            OrderId = KxXmlUtils.GetNodeValue(node, "wuorderid", string.Empty);
            Status = KxXmlUtils.GetNodeValue(node, "laststatus", string.Empty);
            LastStatusChange = KxXmlUtils.GetNodeValueDT(node, "laststatuschangedate", null);
            Amount = KxXmlUtils.GetNodeValue(node, "amount", 0m);
        }
    }
}
