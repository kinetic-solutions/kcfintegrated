﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kx.Core.Web.Student.HousingHand
{
    public class HousingHandResponse
    {
        public bool Successful { get; set; }
        public string Message { get; set; }
        public List<string> FormErrors { get; private set; } = new List<string>();
        public string ReferenceValue { get; set; }

        public string GetFullMessageOutput()
        {
            StringBuilder result = new StringBuilder(Message);

            if (FormErrors.Count > 0)
            {
                result.Append(string.Join(Environment.NewLine, FormErrors));
            }

            return result.ToString();
        }
    }
}
