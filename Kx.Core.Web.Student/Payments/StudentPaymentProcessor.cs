﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Xml;
using Kx.Core.Configuration;
using Kx.Core.Data;
using Kx.Core.Exceptions;
using Kx.Core.PaymentProcessor;
using Kx.Core.PaymentProcessor.Cache;
using Kx.Core.PaymentProcessor.Payments;
using Kx.Objects.Student.Web;
using Kx.Core.Web.Student.WebApplication;
namespace Kx.Core.Web.Student.Payments
{
    public class StudentPaymentProcessor : Kx.Core.PaymentProcessor.PaymentProcessor
    {
        public int LastPaymentId = 0;

        public override PaymentStatus ProcessPayment()
        {
            PaymentStatus result = PaymentStatus.None;

            PaymentResponse response = ReturnTransactionWorkflow();
            if (response != null && HttpContext.Current.Session[response.Cache.CacheId.ToString("N")] == null)
            {

                if (response != null && HttpContext.Current.Session["Workflow"] == null)
                {
                    HttpContext.Current.Session["Workflow"] = response.InternalReference;
                }

                if (response != null)
                {
                    if (response.Cache != null && !response.Cache.Processed)
                    {
                        if (!DoesPaymentExist(response.Cache.TransactionGuId, response.Cache.AuthorisationCode))
                        {
                            response.Cache.Processed = ProcessPayment(response);
                            if (response.Cache.Processed)
                            {
                                response.Cache.Changed = true;
                                response.Cache.Save();
                            }


                            HttpContext.Current.Session[response.Cache.CacheId.ToString("N")] = 1;
                            if (response.Cache.Authorised)
                            {
                                result = PaymentStatus.Authorised;
                            }
                            else
                            {
                                result = PaymentStatus.Declined;
                            }

                        }
                    }
                    else
                    {

                        HttpContext.Current.Session[response.Cache.CacheId.ToString("N")] = 1;

                        if (response.Authorised)
                        {
                            result = PaymentStatus.Authorised;
                        }
                        else
                        {
                            result = PaymentStatus.Declined;
                        }
                    }
                }

                var application = (HttpContext.Current.Session["WebMgr"] as KxWebApplicationStudent);
                if (application != null)
                {
                    var webuser = application.WorkflowInterface.WebUser as KxWebUserStudent;
                    if (webuser != null)
                    {
                        webuser.LastPaymentRawData = new Dictionary<string, RawPaymentDataItem>(StringComparer.InvariantCultureIgnoreCase);
                        foreach (var key in response.RawResponseData.Keys)
                        {
                            webuser.LastPaymentRawData.Add(key, new RawPaymentDataItem() {Field = key, Value = response.RawResponseData[key]});
                        }
                    }
                }
            }

            return result;
        }

        public bool ProcessPayment(PaymentResponse response)
        {
            KxWebPaymentCache cache = response.Cache;
            
            if (cache != null && cache.Authorised && !cache.Processed)
            {
                bool processed = false;

                if (cache.Provider == "WPMKX")
                {
                    if (cache.Payments.Items.Count > 0 && cache.Payments.Items[0].ChargeId > 0 && cache.Payments.Items[0].PaymentType == "PAYNOW")
                    {
                        decimal total = 0;
                        foreach (KxWebPaymentCachePayment item in cache.Payments.Items)
                        {
                            total += item.RequestedAmount;
                        }

                        if (Decimal.Round(response.Amount, 2) > Decimal.Round(total, 2))
                        {
                            total = Decimal.Round(response.Amount, 2);
                        }

                        processed = TakePayment(cache.Payments.Items[0], String.Format("PY:{0}", cache.AuthorisationCode), total, cache.Provider, KxPaymentType.Statement);
                        if (response.SurchargeApplied)
                        {
                            processed = TakePayment(cache.Payments.Items[0], String.Format("SUR:{0}", cache.AuthorisationCode), response.Surcharge, cache.Provider, KxPaymentType.Surcharge);
                        }
                    }
                    else
                    {
                        foreach (KxWebPaymentCachePayment pay in cache.Payments.Items)
                        {
                            if (pay.PaymentType == "DD" || pay.PaymentType == "RCP")
                            {
                                processed = TakePayment(pay, String.Format("IN:{0}", cache.AuthorisationCode), 0.0M, cache.Provider, KxPaymentType.Instalment);
                            }
                            else if (pay.PaymentType == "DEPOSIT")
                            {
                                processed = TakePayment(pay, String.Format("DEP:{0}", cache.AuthorisationCode), pay.RequestedAmount, cache.Provider, KxPaymentType.Deposit);

                                KxLogger.LogActivity(this, "ProcessPayment.SurchargeApplied...", response.Surcharge.ToString("F"), response.SurchargeApplied.ToString());
                                if (response.SurchargeApplied)
                                {
                                    processed = TakePayment(pay, String.Format("SUR:{0}", cache.AuthorisationCode), response.Surcharge, cache.Provider, KxPaymentType.Surcharge);
                                }
                            }
                            else if (pay.PaymentType == "PAYNOW")
                            {
                                processed = TakePayment(pay, String.Format("PY:{0}", cache.AuthorisationCode), pay.RequestedAmount, cache.Provider, KxPaymentType.Payment);
                                if (response.SurchargeApplied)
                                {
                                    processed = TakePayment(pay, String.Format("SUR:{0}", cache.AuthorisationCode), response.Surcharge, cache.Provider, KxPaymentType.Surcharge);
                                }
                            }
                        }
                    }
                    cache.Processed = processed;
                    cache.Changed = true;
                }
                else if (cache.Provider == "WPM3WAY")
                {
                    foreach (KxWebPaymentCachePayment pay in cache.Payments.Items)
                    {
                        if (pay.PaymentType == "DD")
                        {
                            // Direct Debit
                            processed = TakePayment(pay, cache.AuthorisationCode2, 0.0M, cache.Provider, KxPaymentType.Instalment);
                        }
                        else
                        {
                            if (pay.Owner.PaidAmount > 0.00M)
                            {
                                //Deposit
                                processed = TakePayment(pay, cache.AuthorisationCode, cache.PaidAmount, cache.Provider, KxPaymentType.Deposit);

                            }

                            if (processed && pay.Owner.PaidAmount2 > 0.00M)
                            {
                                // Payment
                                processed = TakePayment(pay, cache.AuthorisationCode2, cache.PaidAmount2, cache.Provider, KxPaymentType.Payment);

                            }

                           cache.Processed = processed;
                           cache.Changed = true;
                        }
                    }
                }
                else
                {
                    if (cache.Payments[0] != null)
                    {
                        KxWebPaymentCachePayment pay = cache.Payments[0];

                        processed = TakePayment(pay, cache.AuthorisationCode, cache.PaidAmount, cache.Provider, KxWebStudentPayment.PaymentTypeFromString(pay.PaymentType));

                        if (processed && !string.IsNullOrEmpty(cache.AuthorisationCode2))
                        {
                            processed = TakePayment(pay, cache.AuthorisationCode2, 0.0M, cache.Provider, KxPaymentType.Instalment);
                        }


                        if (response.SurchargeApplied == true)
                        {
                            processed = TakePayment(pay, cache.AuthorisationCode + "CC", response.Surcharge, cache.Provider, KxPaymentType.Surcharge);
                        }
                    }
                    cache.Processed = processed;
                    cache.Changed = true;

                    
                }
                
                if (cache.Processed && cache.Payments[0] != null && (cache.Payments[0].PaymentType == "STATEMENT" || cache.Provider == "KXPAYMENT" || cache.Provider == "WPMKX" && cache.Payments[0].ChargeId > 0) && LastPaymentId > 0)
                {
                    ProcessCharges(cache);
                }

                return cache.Processed;
            }
            else
            {
                return false;
            }

         
        }

        public bool TakePayment(KxWebPaymentCachePayment cache, string providerReference, decimal amount, string gatewayname, KxPaymentType payType)
        {
            if (!DoesPaymentExist(cache.Owner.TransactionGuId,providerReference))
            {
                KxWebStudentPayment payment = new KxWebStudentPayment(null);

                payment.ApplicationGuid = cache.Owner.TransactionGuId;
                payment.PaymentDate = cache.Owner.TimeStampIn;
                payment.TransactionReference = providerReference;
                payment.TransactionAmount = amount;
                if (cache.Owner.Provider == "REALEX")
                {
                    payment.GatewayName = cache.Owner.CacheId.ToString();
                }
                else
                {
                    payment.GatewayName = gatewayname;
                }
                payment.PaymentType = payType;
                payment.Changed = true;

                if (payment.Save())
                {
                    try
                    {
                        ((KxWebApplicationStudent)HttpContext.Current.Session["WebMgr"]).WorkflowInterface.WebUser.WriteStudentHistoryRecord("WebPayment" + payment.PaymentType, "Transaction Reference: " + payment.TransactionReference + ", Amount: " + payment.TransactionAmount.ToString("c"));
                    }
                    catch { }

                    if (payment.PaymentType != KxPaymentType.Surcharge)
                    {
                        LastPaymentId = payment.WebPaymentId;
                    }
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return true;
            }
        }

        public bool DoesPaymentExist(Guid ApplicationGuid, string transactionreference)
        {
            if (KxDbConfiguration.GetConfigurationBool("AllowDuplicatePaymentAuthorisationCodes", false))
            {
                return false;
            }

            KxData data = new KxData("webstudentpayment", "webstudentpayment", TargetType.Table, ActionType.Select);

            data.AddKeyField("applicationguid", ApplicationGuid, KxValueType.Guid);
            data.AddKeyField("transactionreference", transactionreference, KxValueType.String);

            if (data.Execute())
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(data.XmlResults);

                XmlNodeList nodes = doc.SelectNodes(@"//webstudentpayment/datarow");

                if (nodes.Count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
           
        }

        public void ProcessCharges(KxWebPaymentCache cache)
        {
           
            KxWebStudentPaymentBatches batches = new KxWebStudentPaymentBatches();

            foreach (KxWebPaymentCachePayment pay in cache.Payments.Items)
            {
                KxWebStudentPaymentBatch batch = new KxWebStudentPaymentBatch(batches);
                batch.OutstandingBalance = pay.RequestedAmount;
                batch.StatementText = pay.Description;
                batch.StudentChargeId = pay.ChargeId;
                batch.WebPaymentId = LastPaymentId;

                batches.Items.Add(batch);
            }
            batches.Changed = false;
            batches.ChildChanged = true;
            if (batches.Save())
            {
                if (KxDbConfiguration.GetConfigurationBool("UseNewStatementProcessPayment", false))
                {
                    KxData data = new KxData("data",
                        KxDbConfiguration.GetConfiguration("PaymentProcessToKxProcedure",
                            "kcf.ProcessPaymentToKxForApplication"), TargetType.StoredProc, ActionType.ExecuteProcedure);
                    data.AddParameter("applicationguid", cache.TransactionGuId, KxValueType.String, 50);
                    data.AddParameter("depositpaymentreasonid",
                        KxDbConfiguration.GetConfigurationInt("DepositPaymentReasonID", 0), KxValueType.Int);
                    data.AddParameter("statementpaymentreasonid",
                        KxDbConfiguration.GetConfigurationInt("StatementPaymentReasonID", 0), KxValueType.Int);
                    data.AddParameter("paynowpaymentreasonid",
                        KxDbConfiguration.GetConfigurationInt("PayNowPaymentReasonID", 0), KxValueType.Int);
                    data.AddParameter("surchargepaymentreasonid",
                        KxDbConfiguration.GetConfigurationInt("SurchargePaymentReasonID", 0), KxValueType.Int);
                    data.AddParameter("surchargechargetypeid",
                        KxDbConfiguration.GetConfigurationInt("SurchargeChargeTypeID", 0), KxValueType.Int);
                    data.AddParameter("paymentmethodid", KxDbConfiguration.GetConfigurationInt("PaymentMethodID", 0),
                        KxValueType.Int);
                    data.AddParameter("webpaymentid", LastPaymentId, KxValueType.Int);
                    if (!data.Execute())
                    {
                        data.LogException("KxWebStudentApplication", "PaymentProcessToKx", data.LastError, "");
                    }
                }
                else
                {
                    KxData data = new KxData("processpayments", "WebProcessConsolidatedPayments", TargetType.StoredProc,
                        ActionType.ExecuteProcedure);

                    data.AddParameter("WebPaymentId", LastPaymentId, KxValueType.Int);

                    data.Execute();
                }
            }

        }

    }
}
