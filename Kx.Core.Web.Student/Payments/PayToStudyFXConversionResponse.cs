﻿#region copyright

// =======================================================
// Product:   KCF.Student.PayToStudy
// Project:   Kx.Core.Web.Student
// File:      PayToStudyFXConversionResponse.cs
// Created:   11/10/2016
// By:        Steve Ashley
// Modified:  11/10/2016
// 
// Copyright © Kinetic Solutions Ltd 2016
// All rights reserved.
// 
// No portion of this file may be re-created, copied or otherwise
// used without the express permission of Kinetic Solutions Ltd.
// =======================================================

#endregion

#region usings

using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;

#endregion

namespace Kx.Core.Web.Student.Payments
{
    public class PayToStudyFXConversionResponse : PayToStudyResponse
    {
        #region Fields and Properties

        public string SourceAmount { get; set; }
        public string SourceCurrency { get; set; }
        public string ConvertedAmount { get; set; }
        public string ConvertedCurrency { get; set; }
        public string FXRate { get; set; }

        #endregion

        #region Constructors

        public PayToStudyFXConversionResponse(XDocument response)
            : base(response)
        {
        }

        #endregion

        #region Protected Methods

        protected override void LoadSuccessfulResponseData(XDocument response)
        {
            SourceAmount = GetValueFromXPath(response, "//root/rate/sourceAmount");
            SourceCurrency = GetValueFromXPath(response, "//root/rate/sourceCurrency");
            ConvertedAmount = GetValueFromXPath(response, "//root/rate/convertedAmount");
            ConvertedCurrency = GetValueFromXPath(response, "//root/rate/convertedCurrency");
            FXRate = GetValueFromXPath(response, "//root/rate/fxRate");

            var taxes = from e in response.XPathSelectElements("//root/rate/taxes/tax").ToList()
                select e;

            _taxes.Clear();
            foreach (XElement tax in taxes)
            {
                _taxes.Add(new PayToStudyResponseTax(tax));
            }
        }

        #endregion
    }
}