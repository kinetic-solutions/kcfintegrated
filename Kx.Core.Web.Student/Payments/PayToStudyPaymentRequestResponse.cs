﻿#region copyright

// =======================================================
// Product:   KCF.Student.PayToStudy
// Project:   Kx.Core.Web.Student
// File:      PayToStudyPaymentRequestResponse.cs
// Created:   04/10/2016
// By:        Steve Ashley
// Modified:  11/10/2016
// 
// Copyright © Kinetic Solutions Ltd 2016
// All rights reserved.
// 
// No portion of this file may be re-created, copied or otherwise
// used without the express permission of Kinetic Solutions Ltd.
// =======================================================

#endregion

#region usings

using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;

#endregion

namespace Kx.Core.Web.Student.Payments
{
    public class PayToStudyPaymentRequestResponse : PayToStudyResponse
    {
        #region Fields and Properties

        public string RequestId { get; set; }

        public string TransactionId { get; private set; }
        public string Type { get; private set; }
        public string Particular { get; private set; }
        public string CurrencyAbbr { get; private set; }
        public string Currency { get; private set; }
        public string FxRate { get; private set; }
        public string ReferenceNumber { get; private set; }
        public string Amount { get; private set; }
        public string AmountRounded { get; private set; }
        public string BankName { get; private set; }
        public string BankBranchName { get; private set; }
        public string BankAddress { get; private set; }
        public string BankBranchCode { get; private set; }
        public string BankNotes { get; private set; }
        public string AccountName { get; private set; }
        public string AccountNumber { get; private set; }
        public string SortCode { get; private set; }
        public string iban { get; private set; }
        public string swift { get; private set; }
        public string aba { get; private set; }
        public string rtgs { get; private set; }
        public string ifsc { get; private set; }
        public string TransitFIFcode { get; private set; }
        public string bsb { get; private set; }
        public string bic { get; private set; }
        public string CompanyAddress { get; private set; }
        public string PaymentInstructions { get; set; }

        public bool HasPaymentInstuctions
        {
            get { return !string.IsNullOrWhiteSpace(PaymentInstructions); }
        }

        #endregion

        #region Constructors

        public PayToStudyPaymentRequestResponse(XDocument response)
            : base(response)
        {
        }

        public PayToStudyPaymentRequestResponse()
            : base ()
        {
            
        }

        #endregion

        #region Protected Methods

        protected override void LoadSuccessfulResponseData(XDocument response)
        {
            RequestId = GetValueFromXPath(response, "//root/operation/requestID");
            TransactionId = GetValueFromXPath(response, "//root/transfer/transactionId");
            Type = GetValueFromXPath(response, "//root/transfer/type");
            Particular = GetValueFromXPath(response, "//root/transfer/particular");
            CurrencyAbbr = GetValueFromXPath(response, "//root/transfer/currencyAbbr");
            Currency = GetValueFromXPath(response, "//root/transfer/currency");
            FxRate = GetValueFromXPath(response, "//root/transfer/fxRate");
            ReferenceNumber = GetValueFromXPath(response, "//root/transfer/referenceNumber");
            Amount = GetValueFromXPath(response, "//root/transfer/amount");
            AmountRounded = GetValueFromXPath(response, "//root/transfer/amountRounded");
            BankName = GetValueFromXPath(response, "//root/transfer/bankName");
            BankBranchName = GetValueFromXPath(response, "//root/transfer/bankBranchName");
            BankAddress = GetValueFromXPath(response, "//root/transfer/bankAddress");
            BankBranchCode = GetValueFromXPath(response, "//root/transfer/bankBranchCode");
            BankNotes = GetValueFromXPath(response, "//root/transfer/bankNotes");
            AccountName = GetValueFromXPath(response, "//root/transfer/accountName");
            AccountNumber = GetValueFromXPath(response, "//root/transfer/accountNumber");
            SortCode = GetValueFromXPath(response, "//root/transfer/sortCode");
            iban = GetValueFromXPath(response, "//root/transfer/iban");
            swift = GetValueFromXPath(response, "//root/transfer/swift");
            aba = GetValueFromXPath(response, "//root/transfer/aba");
            rtgs = GetValueFromXPath(response, "//root/transfer/rtgs");
            ifsc = GetValueFromXPath(response, "//root/transfer/ifsc");
            TransitFIFcode = GetValueFromXPath(response, "//root/transfer/transitFIFcode");
            bsb = GetValueFromXPath(response, "//root/transfer/bsb");
            bic = GetValueFromXPath(response, "//root/transfer/bic");
            CompanyAddress = GetValueFromXPath(response, "//root/transfer/companyAddress");
            PaymentInstructions = GetValueFromXPath(response, "//root/transfer/paymentInstructions");

            var taxes = from e in response.XPathSelectElements("//root/transfer/taxes/tax").ToList()
                select e;

            _taxes.Clear();
            foreach (XElement tax in taxes)
            {
                _taxes.Add(new PayToStudyResponseTax(tax));
            }
        }

        #endregion
    }
}