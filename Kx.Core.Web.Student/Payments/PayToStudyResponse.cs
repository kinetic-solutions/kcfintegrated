﻿#region copyright

// =======================================================
// Product:   KCF.Student.PayToStudy
// Project:   Kx.Core.Web.Student
// File:      PayToStudyResponse.cs
// Created:   11/10/2016
// By:        Steve Ashley
// Modified:  11/10/2016
// 
// Copyright © Kinetic Solutions Ltd 2016
// All rights reserved.
// 
// No portion of this file may be re-created, copied or otherwise
// used without the express permission of Kinetic Solutions Ltd.
// =======================================================

#endregion

#region usings

using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;

#endregion

namespace Kx.Core.Web.Student.Payments
{
    public abstract class PayToStudyResponse
    {
        #region Fields and Properties

        protected readonly List<PayToStudyResponseTax> _taxes = new List<PayToStudyResponseTax>();
        public bool Successful { get; protected set; }

        public string ErrorMessage { get; protected set; }

        public IEnumerable<PayToStudyResponseTax> Taxes
        {
            get { return _taxes; }
        }

        public string Tax1Name
        {
            get { return (Taxes.ElementAtOrDefault(0) ?? new PayToStudyResponseTax()).Name; }
        }


        public string Tax1Amount
        {
            get { return (Taxes.ElementAtOrDefault(0) ?? new PayToStudyResponseTax()).Amount; }
        }

        public string Tax1Percentage
        {
            get { return (Taxes.ElementAtOrDefault(0) ?? new PayToStudyResponseTax()).Percentage; }
        }

        public string Tax1Description
        {
            get { return (Taxes.ElementAtOrDefault(0) ?? new PayToStudyResponseTax()).Description; }
        }

        public string Tax2Name
        {
            get { return (Taxes.ElementAtOrDefault(1) ?? new PayToStudyResponseTax()).Name; }
        }


        public string Tax2Amount
        {
            get { return (Taxes.ElementAtOrDefault(1) ?? new PayToStudyResponseTax()).Amount; }
        }

        public string Tax2Percentage
        {
            get { return (Taxes.ElementAtOrDefault(1) ?? new PayToStudyResponseTax()).Percentage; }
        }

        public string Tax2Description
        {
            get { return (Taxes.ElementAtOrDefault(1) ?? new PayToStudyResponseTax()).Description; }
        }

        public string Tax3Name
        {
            get { return (Taxes.ElementAtOrDefault(2) ?? new PayToStudyResponseTax()).Name; }
        }


        public string Tax3Amount
        {
            get { return (Taxes.ElementAtOrDefault(2) ?? new PayToStudyResponseTax()).Amount; }
        }

        public string Tax3Percentage
        {
            get { return (Taxes.ElementAtOrDefault(2) ?? new PayToStudyResponseTax()).Percentage; }
        }

        public string Tax3Description
        {
            get { return (Taxes.ElementAtOrDefault(2) ?? new PayToStudyResponseTax()).Description; }
        }

        public string Tax4Name
        {
            get { return (Taxes.ElementAtOrDefault(3) ?? new PayToStudyResponseTax()).Name; }
        }


        public string Tax4Amount
        {
            get { return (Taxes.ElementAtOrDefault(3) ?? new PayToStudyResponseTax()).Amount; }
        }

        public string Tax4Percentage
        {
            get { return (Taxes.ElementAtOrDefault(3) ?? new PayToStudyResponseTax()).Percentage; }
        }

        public string Tax4Description
        {
            get { return (Taxes.ElementAtOrDefault(3) ?? new PayToStudyResponseTax()).Description; }
        }

        public string Tax5Name
        {
            get { return (Taxes.ElementAtOrDefault(4) ?? new PayToStudyResponseTax()).Name; }
        }


        public string Tax5Amount
        {
            get { return (Taxes.ElementAtOrDefault(4) ?? new PayToStudyResponseTax()).Amount; }
        }

        public string Tax5Percentage
        {
            get { return (Taxes.ElementAtOrDefault(4) ?? new PayToStudyResponseTax()).Percentage; }
        }

        public string Tax5Description
        {
            get { return (Taxes.ElementAtOrDefault(4) ?? new PayToStudyResponseTax()).Description; }
        }

        #endregion

        #region Constructors

        protected PayToStudyResponse(XDocument response)
        {
            LoadResponseFromXml(response);
        }

        protected PayToStudyResponse()
        {
        }

        #endregion

        #region Protected Methods

        protected string GetValueFromXPath(XDocument response, string xPath)
        {
            var element = response.XPathSelectElement(xPath);
            if (element != null)
            {
                return element.Value;
            }
            return string.Empty;
        }

        protected abstract void LoadSuccessfulResponseData(XDocument response);

        #endregion

        #region Private Methods

        private void LoadResponseFromXml(XDocument response)
        {
            Successful = StringComparer.InvariantCultureIgnoreCase.Compare(GetValueFromXPath(response, "//root/operation/result"), "ok") == 0;
            if (Successful)
            {
                LoadSuccessfulResponseData(response);
            }
            else
            {
                LoadFailure(response);
            }
        }

        private void LoadFailure(XDocument response)
        {
            try
            {
                ErrorMessage = GetValueFromXPath(response, "//root/operation/errorMessage");
            }
            catch
            {
            }
        }

        #endregion
    }
}