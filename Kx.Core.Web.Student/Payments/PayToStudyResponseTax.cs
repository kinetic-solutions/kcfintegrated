﻿#region copyright

// =======================================================
// Product:   KCF.Student
// Project:   Kx.Core.Web.Student
// File:      PayToStudyResponseTax.cs
// Created:   04/10/2016
// By:        Steve Ashley
// Modified:  04/10/2016
// 
// Copyright © Kinetic Solutions Ltd 2016
// All rights reserved.
// 
// No portion of this file may be re-created, copied or otherwise
// used without the express permission of Kinetic Solutions Ltd.
// =======================================================

#endregion

#region usings

using System;
using System.Linq;
using System.Xml.Linq;

#endregion

namespace Kx.Core.Web.Student.Payments
{
    public class PayToStudyResponseTax
    {
        #region Fields and Properties

        public string Name { get; private set; }
        public string Amount { get; private set; }
        public string Percentage { get; private set; }
        public string Description { get; private set; }

        #endregion

        #region Constructors

        public PayToStudyResponseTax(XElement taxElement)
        {
            LoadTaxFromXmlElement(taxElement);
        }

        internal PayToStudyResponseTax()
        {
            Name = string.Empty;
            Amount = string.Empty;
            Percentage = string.Empty;
            Description = string.Empty;
        }

        #endregion

        #region Private Methods

        private void LoadTaxFromXmlElement(XElement taxElement)
        {
            Name = GetValueFromSubElement(taxElement, "name");
            Amount = GetValueFromSubElement(taxElement, "amount");
            Percentage = GetValueFromSubElement(taxElement, "percentage");
            Description = GetValueFromSubElement(taxElement, "description");
        }

        private string GetValueFromSubElement(XElement taxElement, string elementName)
        {
            return taxElement.Descendants(elementName).FirstOrDefault().Value;
        }

        #endregion
    }
}