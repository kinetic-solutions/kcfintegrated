﻿using Kx.Core.Web.PageRenderer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Objects.Student;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class ARRIVALSLOTROW : StudentObjectTranslatorSource
    {
        public ARRIVALSLOTROW(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "ARRIVALSLOTROW", "Induction arrival slot row", "GI")
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return WebUser.AcceptedAllocation.StudentAllocation.InductionArrivalLocation.ArrivalSlots.GeteInductionArrivalRowSlot(attributes.GenericIndex);
        }

        public override Type GetTargetObjectType()
        {
            return typeof (KxeInductionArrivalSlot);
        }
    }
}
