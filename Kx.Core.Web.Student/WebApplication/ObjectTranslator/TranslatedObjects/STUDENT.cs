﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Core.Web.ObjectTranslator;
using Kx.Core.Web.PageRenderer;
using Kx.Objects.Student;
using Kx.Objects.Student.Web;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class STUDENT : StudentObjectTranslatorSource
    {
        public STUDENT(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "STUDENT", "Student", string.Empty)
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return Student;
        }

        public override Type GetTargetObjectType()
        {
            return typeof (KxStudent);
        }
    }
}
