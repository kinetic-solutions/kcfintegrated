﻿using Kx.Core.Web.PageRenderer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Objects.Student.Web;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class USERINHOUSESCHED : StudentObjectTranslatorSource
    {
        public USERINHOUSESCHED(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "USERINHOUSESCHED", "Inhouse allocation schedule", string.Empty)
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return WebUser.InHouseAllocation.Schedule;
        }

        public override Type GetTargetObjectType()
        {
            return typeof (KxWebStudentAllocationSchedule);
        }
    }
}
