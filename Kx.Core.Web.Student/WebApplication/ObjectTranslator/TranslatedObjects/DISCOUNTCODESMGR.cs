﻿using System;
using Kx.Core.Web.PageRenderer;
using Kx.Core.Web.Student.WebApplication.ProcessManagers;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{

    public class DISCOUNTCODESMGR : StudentObjectTranslatorSource
    {
        public DISCOUNTCODESMGR(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "DISCOUNTCODESMGR", "Discount Codes Manager", string.Empty)
        {

        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return WebApp.DiscountCodeManager;
        }

        public override Type GetTargetObjectType()
        {
            return typeof(KxDiscountCodeManager);
        }
    }
}
