﻿using Kx.Core.Web.PageRenderer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class SAYAPP2NDPREFAVAIL : SAYAPP0PREFAVAIL
    {
        public SAYAPP2NDPREFAVAIL(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "SAYAPP2NDPREFAVAIL", "2nd preference availability")
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return WebUser.CurrentApplication.SecondPreference.PreferenceAvailability;
        }
    }
}
