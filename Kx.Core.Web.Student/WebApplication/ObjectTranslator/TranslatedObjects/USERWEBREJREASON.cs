﻿using Kx.Core.Web.PageRenderer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Objects.Student;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class USERWEBREJREASON : StudentObjectTranslatorSource
    {
        public USERWEBREJREASON(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "USERWEBREJREASON", "Rejected web allocation reason", string.Empty)
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return WebUser.RejectedAllocation.Reason;
        }

        public override Type GetTargetObjectType()
        {
            return typeof (KxStudentReason);
        }
    }
}
