﻿using System;
using Kx.Core.Web.PageRenderer;
using Kx.Core.Web.PageRenderer.Utility;
using Kx.Objects.Student.Web;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    /// <summary>
    /// Object translation wrapper class to translate User Discount Code class Object
    /// </summary>
    public class USERDISCOUNTCODES : StudentObjectTranslatorSource
    {
        public USERDISCOUNTCODES(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "USERDISCOUNTCODES", "discounts offfering", string.Empty)
        {

        }

        /// <summary>
        /// Retrieves the target Object
        /// </summary>
        /// <param name="attributes"></param>
        /// <returns></returns>
        public override object GetTargetObject(KxWebAttribute attributes) => WebUser.AppliedDiscountCodes;

        public override Type GetTargetObjectType() => typeof(KxDiscountCode);
    }
}