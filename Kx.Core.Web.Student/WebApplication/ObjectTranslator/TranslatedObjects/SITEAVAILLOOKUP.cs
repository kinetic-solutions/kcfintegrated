﻿using Kx.Core.Web.PageRenderer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Objects.Student;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class SITEAVAILLOOKUP : StudentObjectTranslatorSource
    {
        public SITEAVAILLOOKUP(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "SITEAVAILLOOKUP", "Site availability lookup, current", string.Empty)
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return WebUser.CurrentSiteLookup;
        }

        public override Type GetTargetObjectType()
        {
            return typeof (KxSiteAvailabilityLookup);
        }
    }
}
