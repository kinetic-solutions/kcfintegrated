﻿using Kx.Core.Web.PageRenderer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Objects.Student.Web;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class SAYAPPPREF : StudentObjectTranslatorSource
    {
        public SAYAPPPREF(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "SAYAPPPREF", "Current number preference", string.Empty)
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return WebUser.CurrentApplication.CurrentNumberPreference;
        }

        public override Type GetTargetObjectType()
        {
            return typeof (KxWebStudentPreference);
        }
    }
}
