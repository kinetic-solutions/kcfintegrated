﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Core.Web.PageRenderer;
using Kx.Objects.Common;
using Kx.Objects.Student;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class USERMIXROOMCFG : StudentObjectTranslatorSource
    {
        public USERMIXROOMCFG(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "USERMIXROOMCFG", "Pending or Accepted allocation room Config (ST2RoomConfig)", string.Empty)
        {

        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            if(WebUser.PendingAllocation != null)
            {
                return WebApp.ContractManager.PendingRoomConfiguration;
            }
            else 
            {
                return WebApp.ContractManager.AcceptedRoomConfiguration;
            }
        }

        public override Type GetTargetObjectType()
        {
            return typeof(KxRoomConfig);
        }
    }
}
