﻿using Kx.Core.Web.PageRenderer;
using Kx.Objects.Student;
using Kx.Objects.Student.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class SITEAVAILLOOK : StudentObjectTranslatorSource
    {
        public SITEAVAILLOOK(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "SITEAVAILLOOK", "Site availability lookup by index", "GS")
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return KxStudentObjectManager.SiteAvailabilityLookups.GetSiteAvailabilityLookup(attributes.GenericStringIndex);
        }

        public override Type GetTargetObjectType()
        {
            return typeof (KxSiteAvailabilityLookup);
        }
    }
}
