﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Core.Web.PageRenderer;
using Kx.Objects.Student.Web;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class ALLOCPRICE : StudentObjectTranslatorSource
    {
        public ALLOCPRICE(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "ALLOCPRICE", "Current allocation pricing", string.Empty)
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return WebApp.ApplicationsManager.CurrentAcademicYear.AllocationPricing;
        }

        public override Type GetTargetObjectType()
        {
            return typeof(KxWebAllocationPricingList);
        }
    }
}
