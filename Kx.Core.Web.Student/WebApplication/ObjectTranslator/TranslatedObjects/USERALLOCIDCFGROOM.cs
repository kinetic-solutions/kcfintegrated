﻿using Kx.Core.Web.PageRenderer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Objects.Common;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class USERALLOCIDCFGROOM : StudentObjectTranslatorSource
    {
        public USERALLOCIDCFGROOM(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "USERALLOCIDCFGROOM", "Specified allocation ID room", "GI")
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return WebApp.ContractManager.GetSpecifiedAllocationRoomConfiguration(attributes.GenericIndex).ResidentialRoom;
        }

        public override Type GetTargetObjectType()
        {
            return typeof (KxResidentialRoom);
        }
    }
}
