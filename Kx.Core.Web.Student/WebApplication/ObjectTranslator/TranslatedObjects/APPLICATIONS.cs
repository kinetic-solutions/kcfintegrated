﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Core.Web.PageRenderer;
using Kx.Objects.Student;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class APPLICATIONS : StudentObjectTranslatorSource
    {
        public APPLICATIONS(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "APPLICATIONS", "Student applications (first)", string.Empty)
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return Student.Applications[0];
        }

        public override Type GetTargetObjectType()
        {
            return typeof(KxStudentApplication);
        }
    }
}
