﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Core.Web.PageRenderer;
using Kx.Objects.Student;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class CHARGES : StudentObjectTranslatorSource
    {
        public CHARGES(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "CHARGES", "Student Charges", string.Empty)
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return Student.Account.Charges;
        }

        public override Type GetTargetObjectType()
        {
            return typeof(KxStudentCharges);
        }
    }
}
