﻿using Kx.Core.Web.PageRenderer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Objects.Student;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    [Depricated]
    public class CITYROOMS : StudentObjectTranslatorSource
    {
        public CITYROOMS(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "CITYROOMS", "City living rooms", string.Empty)
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return WebUser.CurrentApplication.CityRoomRates;
        }

        public override Type GetTargetObjectType()
        {
            return typeof (KxWebStudentRoomCityRates);
        }
    }
}
