﻿using Kx.Core.Web.PageRenderer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Objects.Common;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class USERACCROOMCFG : StudentObjectTranslatorSource
    {
        public USERACCROOMCFG(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "USERACCROOMCFG", "Accepted room configuration", string.Empty)
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return WebApp.ContractManager.AcceptedRoomConfiguration;
        }

        public override Type GetTargetObjectType()
        {
            return typeof (KxRoomConfig);
        }
    }
}
