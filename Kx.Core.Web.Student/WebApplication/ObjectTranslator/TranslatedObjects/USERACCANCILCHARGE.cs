﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Core.Web.PageRenderer;
using Kx.Objects.Student.Web;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class USERACCANCILCHARGE : StudentObjectTranslatorSource
    {
        public USERACCANCILCHARGE(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "USERACCANCILCHARGE", "Web User Accepted Allocation Ancillary Charges", string.Empty)
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return WebUser.AcceptedAllocation.StudentAllocation.AllocationAncillaryCharges;
        }

        public override Type GetTargetObjectType()
        {
            return typeof(KxWebAllocationCharges);
        }
    }
}
