﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Core.Web.PageRenderer;
using Kx.Objects.Student;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class STUDENTDEPOSITBALANCE : StudentObjectTranslatorSource
    {
        public STUDENTDEPOSITBALANCE(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "STUDENTDEPOSITBALANCE", "Student deposit balances", string.Empty)
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return Student.DepositBalances;
        }

        public override Type GetTargetObjectType()
        {
            return typeof(KxDepositBalances);
        }
    }
}
