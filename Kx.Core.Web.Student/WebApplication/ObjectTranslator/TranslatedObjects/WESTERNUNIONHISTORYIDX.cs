﻿#region copyright

// =======================================================
// Product:   KCF.Student
// Project:   Kx.Core.Web.Student
// File:      WESTERNUNIONHISTORYIDX.cs
// Created:   13/02/2018
// By:        Steve Ashley
// Modified:  13/02/2018
// 
// Copyright © Kinetic Solutions Ltd 2018
// All rights reserved.
// 
// No portion of this file may be re-created, copied or otherwise
// used without the express permission of Kinetic Solutions Ltd.
// =======================================================

#endregion

#region usings

using System;
using System.Linq;
using Kx.Core.Web.PageRenderer;
using Kx.Core.Web.Student.WesternUnion;

#endregion

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class WESTERNUNIONHISTORYIDX : StudentObjectTranslatorSource
    {
        #region Constructors

        public WESTERNUNIONHISTORYIDX(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "WESTERNUNIONHISTORYIDX", "Western Union Order History Item", string.Empty)
        {
        }

        #endregion

        #region Public Methods

        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            if (string.IsNullOrWhiteSpace(attributes.GenericStringIndex))
            {
                return WebApp.WesternUnionManager.Payments.OrderByDescending(s => s.KxOrderId).FirstOrDefault();
            }
            return WebApp.WesternUnionManager.Payments.FirstOrDefault(s => s.CacheId.ToString("N") == attributes.GenericStringIndex);
        }

        public override Type GetTargetObjectType()
        {
            return typeof(WesternUnionPaymentHistory);
        }

        #endregion
    }
}