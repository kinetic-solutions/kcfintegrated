﻿using Kx.Core.Web.PageRenderer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Objects.Common;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class USERALLOCIDCFGSITE : StudentObjectTranslatorSource
    {
        public USERALLOCIDCFGSITE(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "USERALLOCIDCFGSITE", "Specified allocation ID site", "GI")
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return WebApp.ContractManager.GetSpecifiedAllocationRoomConfiguration(attributes.GenericIndex).ResidentialRoom.Block.Site;
        }

        public override Type GetTargetObjectType()
        {
            return typeof (KxSite);
        }
    }
}
