﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Core.Web.ObjectTranslator;
using Kx.Core.Web.PageRenderer;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class WORKFLOW : StudentObjectTranslatorSource
    {
        public WORKFLOW(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "WORKFLOW", "Current Workflow", string.Empty)
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return PageRenderer.WorkflowInterface.CurrentWorkflow;
        }

        public override Type GetTargetObjectType()
        {
            return typeof(Workflow.Workflow);
        }
    }
}
