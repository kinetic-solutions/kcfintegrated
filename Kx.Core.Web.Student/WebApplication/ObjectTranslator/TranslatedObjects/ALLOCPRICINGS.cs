﻿using Kx.Core.Web.PageRenderer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Objects.Student.Web;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    [Depricated]
    public class ALLOCPRICINGS : StudentObjectTranslatorSource
    {
        public ALLOCPRICINGS(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "ALLOCPRICINGS", "Web allocation pricing list", string.Empty)
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return KxStudentObjectManager.WebAllocationPricingList;
        }

        public override Type GetTargetObjectType()
        {
            return typeof (KxWebAllocationPricingList);
        }
    }
}
