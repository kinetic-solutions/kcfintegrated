﻿using Kx.Core.Web.PageRenderer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Objects.Student.Web;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class SAYAPPPREFIDXAVAIL : StudentObjectTranslatorSource
    {
        public SAYAPPPREFIDXAVAIL(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "SAYAPPPREFIDXAVAIL", "Current index preference availability", string.Empty)
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return WebUser.CurrentApplication.CurrentIndexPreference.PreferenceAvailability;
        }

        public override Type GetTargetObjectType()
        {
            return typeof (KxPreferenceAvailability);
        }
    }
}
