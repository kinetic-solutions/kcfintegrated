﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Core.Web.PageRenderer;
using Kx.Objects.Common;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class USERLASTSUBBLOCKADDR : StudentObjectTranslatorSource
    {
        public USERLASTSUBBLOCKADDR(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "USERLASTSUBBLOCKADDR", "Web User Last Allocation Sub-Block Address", string.Empty)
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return WebUser.LastAllocation.StudentAllocation.ResidentialRoom.SubBlock.Address;
        }

        public override Type GetTargetObjectType()
        {
            return typeof (KxAddress);
        }
    }
}
