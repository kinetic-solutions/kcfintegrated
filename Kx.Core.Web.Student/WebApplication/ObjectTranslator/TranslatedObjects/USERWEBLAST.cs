﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Core.Web.PageRenderer;
using Kx.Objects.Student.Web;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class USERWEBLAST : StudentObjectTranslatorSource
    {
        public USERWEBLAST(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "USERWEBLAST", "Web User Last Allocation", string.Empty)
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return WebUser.LastAllocation;
        }

        public override Type GetTargetObjectType()
        {
            return typeof(KxWebStudentAllocation);
        }
    }
}
