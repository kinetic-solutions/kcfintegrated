﻿using Kx.Core.Web.PageRenderer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Objects.Student.Web;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public abstract class SAYAPP0PREF : StudentObjectTranslatorSource
    {
        public SAYAPP0PREF(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer, string kxAttribute, string friendlyName)
            : base(objectTranslator, pageRenderer, kxAttribute, friendlyName, string.Empty)
        {
            
        }
        public override Type GetTargetObjectType()
        {
            return typeof (KxWebStudentPreference);
        }
    }
}
