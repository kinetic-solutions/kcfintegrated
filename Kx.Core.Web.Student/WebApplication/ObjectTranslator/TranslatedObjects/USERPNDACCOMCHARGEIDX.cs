﻿using Kx.Core.Web.PageRenderer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Objects.Student.Web;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class USERPNDACCOMCHARGEIDX : StudentObjectTranslatorSource
    {
        public USERPNDACCOMCHARGEIDX(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "USERPNDACCOMCHARGEIDX", "Pending allocation accommodation charge (by index GS)", string.Empty)
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return WebUser.PendingAllocation.StudentAllocation.AllocationAccommodationCharges.GetChargeById(attributes.GenericStringIndex);
        }

        public override Type GetTargetObjectType()
        {
            return typeof (KxWebAllocationCharge);
        }
    }
}
