﻿using Kx.Core.Web.PageRenderer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Objects.Student.Web;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class SOCPROFTEMPLATE : StudentObjectTranslatorSource
    {
        public SOCPROFTEMPLATE(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "SOCPROFTEMPLATE", "Social profile template", string.Empty)
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return WebApp.SocialProfileTemplate;
        }

        public override Type GetTargetObjectType()
        {
            return typeof (KxWebSocialProfileTemplate);
        }
    }
}
