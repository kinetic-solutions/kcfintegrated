﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Core.Web.PageRenderer;
using Kx.Objects.Common;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class USERLASTSITESITE : StudentObjectTranslatorSource
    {
        public USERLASTSITESITE(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "USERLASTSITESITE", "Web User Last Allocation Site", string.Empty)
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return WebUser.LastAllocation.StudentAllocation.ResidentialRoom.Block.Site;
        }

        public override Type GetTargetObjectType()
        {
            return typeof(KxSite);
        }
    }
}
