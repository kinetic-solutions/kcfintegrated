﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Core.Merge;
using Kx.Core.Web.PageRenderer;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class USERMIXMERGE : StudentObjectTranslatorSource
    {
        public USERMIXMERGE(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "USERMIXMERGE", "Pending, expired, inhouse or Accepted allocation merge tags", string.Empty)
        {

        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            if (WebUser.PendingAllocation != null)
            {
                WebUser.PendingAllocation.MergeTags.CurrentMergeTag = attributes.MergeTag;
                WebUser.PendingAllocation.MergeTags.CurrentMergeTagIndex = attributes.MergeTagIndex;
                return WebUser.PendingAllocation.MergeTags;
            }
            else if (WebUser.ExpiredAllocation != null)
            {
                WebUser.ExpiredAllocation.MergeTags.CurrentMergeTag = attributes.MergeTag;
                WebUser.ExpiredAllocation.MergeTags.CurrentMergeTagIndex = attributes.MergeTagIndex;
                return WebUser.ExpiredAllocation.MergeTags;
            }
            else if (WebUser.InHouseAllocation != null)
            {
                WebUser.InHouseAllocation.MergeTags.CurrentMergeTag = attributes.MergeTag;
                WebUser.InHouseAllocation.MergeTags.CurrentMergeTagIndex = attributes.MergeTagIndex;
                return WebUser.InHouseAllocation.MergeTags;
            }
            else
            {
                WebUser.AcceptedAllocation.MergeTags.CurrentMergeTag = attributes.MergeTag;
                WebUser.AcceptedAllocation.MergeTags.CurrentMergeTagIndex = attributes.MergeTagIndex;
                return WebUser.AcceptedAllocation.MergeTags;
            }
        }

        public override Type GetTargetObjectType()
        {
            return typeof(KxMergeTagProvider);
        }
    }
}
