﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Core.Web.Configuration;
using Kx.Core.Web.PageRenderer;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class CFG : StudentObjectTranslatorSource
    {
        public CFG(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "CFG", "Configuration manager", string.Empty)
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return WebApp.ConfigurationManager;
        }

        public override Type GetTargetObjectType()
        {
            return typeof(KxConfigurationManager);
        }
    }
}
