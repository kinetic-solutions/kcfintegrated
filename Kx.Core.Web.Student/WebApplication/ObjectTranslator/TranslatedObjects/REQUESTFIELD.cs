﻿using Kx.Core.Web.PageRenderer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Objects.Student;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class REQUESTFIELD : StudentObjectTranslatorSource
    {
        public REQUESTFIELD(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "REQUESTFIELD", "Request field", "GS")
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return WebApp.RequestManager.GetRequestField(attributes.GenericStringIndex);
        }

        public override Type GetTargetObjectType()
        {
            return typeof (KxStudentRequestData);
        }
    }
}
