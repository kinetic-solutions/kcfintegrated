﻿#region copyright

// =======================================================
// Product:   KCF.Student.PayToStudy
// Project:   Kx.Core.Web.Student
// File:      PAYTOSTUDYFXCONVERSION.cs
// Created:   11/10/2016
// By:        Steve Ashley
// Modified:  11/10/2016
// 
// Copyright © Kinetic Solutions Ltd 2016
// All rights reserved.
// 
// No portion of this file may be re-created, copied or otherwise
// used without the express permission of Kinetic Solutions Ltd.
// =======================================================

#endregion

#region usings

using System;
using Kx.Core.Web.PageRenderer;
using Kx.Core.Web.Student.Payments;

#endregion

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class PAYTOSTUDYFXCONVERSION : StudentObjectTranslatorSource
    {
        #region Constructors

        public PAYTOSTUDYFXCONVERSION(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "PAYTOSTUDYFXCONVERSION", "Pay To Study FX Conversion preview", string.Empty)
        {
        }

        #endregion

        #region Public Methods

        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return WebApp.PayToStudyManager.FXConversionResponse;
        }

        public override Type GetTargetObjectType()
        {
            return typeof(PayToStudyFXConversionResponse);
        }

        #endregion
    }
}