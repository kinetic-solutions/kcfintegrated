﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Core.Web.PageRenderer;
using Kx.Objects.Common;
using Kx.Objects.Student;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class USERMIXSITESITE : StudentObjectTranslatorSource
    {
        public USERMIXSITESITE(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "USERMIXSITESITE", "Pending, expired, inhouse or Accepted allocation site (SITE)", string.Empty)
        {

        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            if(WebUser.PendingAllocation != null)
            {
                return WebUser.PendingAllocation.StudentAllocation.ResidentialRoom.Block.Site;
            }
            else if(WebUser.ExpiredAllocation != null)
            {
                return WebUser.ExpiredAllocation.StudentAllocation.ResidentialRoom.Block.Site;
            }
            else if(WebUser.InHouseAllocation != null)
            {
                return WebUser.InHouseAllocation.StudentAllocation.ResidentialRoom.Block.Site;
            }
            else
            {
                return WebUser.AcceptedAllocation.StudentAllocation.ResidentialRoom.Block.Site;
            }
        }

        public override Type GetTargetObjectType()
        {
            return typeof(KxSite);
        }
    }
}
