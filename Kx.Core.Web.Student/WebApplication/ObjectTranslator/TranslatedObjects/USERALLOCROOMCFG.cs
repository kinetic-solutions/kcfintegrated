﻿using Kx.Core.Web.PageRenderer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Objects.Common;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class USERALLOCROOMCFG : StudentObjectTranslatorSource
    {
        public USERALLOCROOMCFG(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "USERALLOCROOMCFG", "Specified allocation room config", string.Empty)
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return WebApp.ContractManager.SpecifiedRoomConfiguration;
        }

        public override Type GetTargetObjectType()
        {
            return typeof (KxRoomConfig);
        }
    }
}
