﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Core.Web.PageRenderer;
using Kx.Objects.Student.Web;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class BEDCHARGETYPEIDX : StudentObjectTranslatorSource
    {
        public BEDCHARGETYPEIDX(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "BEDCHARGETYPEIDX", "Bedroom Type Charge Index", string.Empty)
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            if (WebApp.CurrentBedroomTypeChargeIndex == -1)
            {
                WebApp.AllocationManager.CreateBedroomChargeFromPreference();
            }
            return WebApp.CurrentBedroomTypeCharge;
        }

        public override Type GetTargetObjectType()
        {
            return typeof(KxWebBedroomTypeCharge);
        }
    }
}
