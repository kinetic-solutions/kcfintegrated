﻿using Kx.Core.Web.PageRenderer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Objects.Student;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class EDITSITEAVAILLOOKUP : StudentObjectTranslatorSource
    {
        public EDITSITEAVAILLOOKUP(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "EDITSITEAVAILLOOKUP", "Site availability lookup, edit", string.Empty)
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return WebUser.EditSiteAvailabilityLookup;
        }

        public override Type GetTargetObjectType()
        {
            return typeof (KxSiteAvailabilityLookup);
        }
    }
}
