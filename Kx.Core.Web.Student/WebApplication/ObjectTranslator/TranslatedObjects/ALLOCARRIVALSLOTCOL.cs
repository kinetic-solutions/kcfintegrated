﻿using Kx.Core.Web.PageRenderer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Objects.Student;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class ALLOCARRIVALSLOTCOL : StudentObjectTranslatorSource
    {
        public ALLOCARRIVALSLOTCOL(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "ALLOCARRIVALSLOTCOL", "Induction Allocation arrival slot column", "GI")
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return WebUser.AcceptedAllocation.StudentAllocation.InductionArrivalLocation.AllocationArrivalSlots.GeteInductionArrivalColumnSlot(attributes.GenericIndex);
        }

        public override Type GetTargetObjectType()
        {
            return typeof (KxeInductionArrivalSlot);
        }
    }
}
