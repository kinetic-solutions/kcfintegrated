﻿using Kx.Core.Web.PageRenderer;
using Kx.Objects.Student;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class REQUESTIDXFIELD : StudentObjectTranslatorSource
    {
        public REQUESTIDXFIELD(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "REQUESTIDXFIELD", "Request field value for request", "RI;GS")
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            KxStudentRequest request;
            if (attributes.RequestIdentifier > 0)
                request = WebUser.CurrentApplication.StudentRequests.Find(s => s.StudentRequestId == attributes.RequestIdentifier);
            else
                request =
                    WebUser.CurrentApplication.StudentRequests.Find(
                        s => s.StudentRequestId.ToString() == attributes.GenericStringIndex);
            if (request != null)
            {
                return request.GetDataValueByLabel(attributes.GenericStringIndex);
            }

            return null;
        }

        public override Type GetTargetObjectType()
        {
            return typeof (KxStudentRequestData);
        }
    }
}
