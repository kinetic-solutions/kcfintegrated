﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Core.Web.PageRenderer;
using Kx.Objects.Student;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class PREFERREDADDRESS : StudentObjectTranslatorSource
    {
        public PREFERREDADDRESS(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "PREFERREDADDRESS", "Student preferred email addres", string.Empty)
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return Student.PreferredEmailAddress;
        }

        public override Type GetTargetObjectType()
        {
            return typeof(KxStudentAddress);
        }
    }
}
