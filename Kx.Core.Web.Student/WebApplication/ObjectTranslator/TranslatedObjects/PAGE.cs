﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using Kx.Core.Web.ObjectTranslator;
using Kx.Core.Web.PageRenderer;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class PAGE : StudentObjectTranslatorSource
    {
        public PAGE(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "PAGE", "Current Page", string.Empty)
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return PageRenderer.CurrentPage;
        }

        public override Type GetTargetObjectType()
        {
            return typeof(Page);
        }
    }
}
