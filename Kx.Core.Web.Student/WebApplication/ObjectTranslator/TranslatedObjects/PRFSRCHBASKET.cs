﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Core.Web.PageRenderer;
using Kx.Objects.Student.Web;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class PRFSRCHBASKET : StudentObjectTranslatorSource
    {
        public PRFSRCHBASKET(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "PRFSRCHBASKET", "Preference Search Current Basket Item", string.Empty )
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return WebApp.PreferenceSearchManager.CurrentBasketItem;
        }

        public override Type GetTargetObjectType()
        {
            return typeof(KxWebStudentPreference);
        }
    }
}
