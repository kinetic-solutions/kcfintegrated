﻿#region copyright

// =======================================================
// Product:   KCF.Student
// Project:   Kx.Core.Web.Student
// File:      WESTERNUNIONMGR.cs
// Created:   15/11/2017
// By:        Steve Ashley
// Modified:  15/11/2017
// 
// Copyright © Kinetic Solutions Ltd 2017
// All rights reserved.
// 
// No portion of this file may be re-created, copied or otherwise
// used without the express permission of Kinetic Solutions Ltd.
// =======================================================

#endregion

#region usings

using System;
using Kx.Core.Web.PageRenderer;
using Kx.Core.Web.Student.WebApplication.ProcessManagers;
using Kx.Core.WesternUnion.DataObj.Responses;

#endregion

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class WESTERNUNIONMGR : StudentObjectTranslatorSource
    {
        #region Constructors

        public WESTERNUNIONMGR(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "WESTERNUNIONMGR", "Western Union Manager", string.Empty)
        {
        }

        #endregion

        #region Public Methods

        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return WebApp.WesternUnionManager;
        }

        public override Type GetTargetObjectType()
        {
            return typeof(KxWesternUnionManager);
        }

        #endregion
    }
}