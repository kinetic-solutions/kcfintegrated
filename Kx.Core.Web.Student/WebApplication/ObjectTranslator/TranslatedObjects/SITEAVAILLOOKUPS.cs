﻿using Kx.Core.Web.PageRenderer;
using Kx.Objects.Student;
using Kx.Objects.Student.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class SITEAVAILLOOKUPS : StudentObjectTranslatorSource
    {
        public SITEAVAILLOOKUPS(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "SITEAVAILLOOKUPS", "Site availability lookups", string.Empty)
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return KxStudentObjectManager.SiteAvailabilityLookups;
        }

        public override Type GetTargetObjectType()
        {
            return typeof (KxSiteAvailabilityLookups);
        }
    }
}
