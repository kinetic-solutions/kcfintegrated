﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Core.Web.PageRenderer;
using Kx.Objects.Student;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class COURSECOURSE : StudentObjectTranslatorSource
    {
        public COURSECOURSE(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "COURSECOURSE", "Student current course details", string.Empty)
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return Student.Courses.CurrentCourse.Course;
        }

        public override Type GetTargetObjectType()
        {
            return typeof(KxStudentCourse);
        }
    }


}

