﻿using Kx.Core.Web.PageRenderer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Objects.Student;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class USERINHOUSETERMDATES : StudentObjectTranslatorSource
    {
        public USERINHOUSETERMDATES(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "USERINHOUSETERMDATES", "Inhouse allocation term dates", string.Empty)
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return WebUser.InHouseAllocation.StudentAllocation.TermDates;
        }

        public override Type GetTargetObjectType()
        {
            return typeof(KxStudentTermDates);
        }
    }
}
