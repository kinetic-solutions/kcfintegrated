﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Core.Merge;
using Kx.Core.Web.PageRenderer;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class USERINHOUSEMERGE : StudentObjectTranslatorSource
    {
        public USERINHOUSEMERGE(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "USERINHOUSEMERGE", "Web User In House Allocation Merge Tag", "MT")
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            WebUser.InHouseAllocation.MergeTags.CurrentMergeTag = attributes.MergeTag;
            WebUser.InHouseAllocation.MergeTags.CurrentMergeTagIndex = attributes.MergeTagIndex;
            return WebUser.InHouseAllocation.MergeTags;
        }

        public override Type GetTargetObjectType()
        {
            return typeof(KxMergeTagProvider);
        }
    }
}
