﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Core.Resources;
using Kx.Core.Web.ObjectTranslator;
using Kx.Core.Web.PageRenderer;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class RESOURCEMGR : StudentObjectTranslatorSource
    {
        public RESOURCEMGR(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "RESOURCEMGR", "Resource manager", string.Empty)
        {
            
        }

        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return PageRenderer.ResourceManager.ResourceProvider;
        }

        public override Type GetTargetObjectType()
        {
            return typeof(KxResourceProvider);
        }
    }
}
