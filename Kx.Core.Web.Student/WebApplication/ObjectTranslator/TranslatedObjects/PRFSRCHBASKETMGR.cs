﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Core.Web.PageRenderer;
using Kx.Objects.Student.Web;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class PRFSRCHBASKETMGR : StudentObjectTranslatorSource
    {
        public PRFSRCHBASKETMGR(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "PRFSRCHBASKETMGR", "Preference search manager basket", string.Empty)
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return WebApp.PreferenceSearchManager.Basket;
        }

        public override Type GetTargetObjectType()
        {
            return typeof(KxWebStudentPreferences);
        }
    }
}
