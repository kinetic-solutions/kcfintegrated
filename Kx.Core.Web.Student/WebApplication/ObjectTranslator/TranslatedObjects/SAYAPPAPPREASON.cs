﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Core.Web.PageRenderer;
using Kx.Objects.Student;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class SAYAPPAPPREASON : StudentObjectTranslatorSource
    {
        public SAYAPPAPPREASON(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "SAYAPPAPPREASON", "Current Application (Standard Application) Inactive Reason", string.Empty)
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return WebUser.CurrentApplication.StandardApplication.InactiveReason;
        }

        public override Type GetTargetObjectType()
        {
            return typeof(KxStudentReason);
        }
    }
}
