﻿using Kx.Core.Web.PageRenderer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Objects.Common;
using Kx.Objects.Student.Web;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class USERINHOUSEPROPDETAIL : StudentObjectTranslatorSource
    {
        public USERINHOUSEPROPDETAIL(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "USERINHOUSEPROPDETAIL", "Inhouse allocation property detail", string.Empty)
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return WebUser.InHouseAllocation.PropertyDetail;
        }

        public override Type GetTargetObjectType()
        {
            return typeof (KxPropertyDetail);
        }
    }
}
