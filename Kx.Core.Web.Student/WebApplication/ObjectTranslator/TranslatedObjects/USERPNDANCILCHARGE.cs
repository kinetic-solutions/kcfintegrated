﻿using Kx.Core.Web.PageRenderer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Objects.Student.Web;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class USERPNDANCILCHARGE : StudentObjectTranslatorSource
    {
        public USERPNDANCILCHARGE(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "USERPNDANCILCHARGE", "Pending allocation ancillary charges", string.Empty)
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return WebUser.PendingAllocation.StudentAllocation.AllocationAncillaryCharges;
        }

        public override Type GetTargetObjectType()
        {
            return typeof (KxWebAllocationCharges);
        }
    }
}
