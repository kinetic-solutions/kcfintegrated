﻿using Kx.Core.Web.PageRenderer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Objects.Common;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class USERALLOCIDCFGBLOCK : StudentObjectTranslatorSource
    {
        public USERALLOCIDCFGBLOCK(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "USERALLOCIDCFGBLOCK", "Specified allocation ID block", "GI")
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return WebApp.ContractManager.GetSpecifiedAllocationRoomConfiguration(attributes.GenericIndex).ResidentialRoom.Block;
        }

        public override Type GetTargetObjectType()
        {
            return typeof (KxSiteBlock);
        }
    }
}
