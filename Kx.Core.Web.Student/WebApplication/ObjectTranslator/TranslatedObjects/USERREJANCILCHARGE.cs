﻿using Kx.Core.Web.PageRenderer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Objects.Student.Web;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class USERREJANCILCHARGE : StudentObjectTranslatorSource
    {
        public USERREJANCILCHARGE(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "USERREJANCILCHARGE", "Rejected allocation ancillary charges", string.Empty)
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return WebUser.RejectedAllocation.StudentAllocation.AllocationAncillaryCharges;
        }

        public override Type GetTargetObjectType()
        {
            return typeof (KxWebAllocationCharges);
        }
    }
}
