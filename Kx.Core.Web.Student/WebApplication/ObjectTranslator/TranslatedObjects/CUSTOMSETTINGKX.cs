﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Core.Web.PageRenderer;
using Kx.Objects.Common;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class CUSTOMSETTINGKX : StudentObjectTranslatorSource
    {
        public CUSTOMSETTINGKX(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "CUSTOMSETTINGKX", "Custom Setting", "GS")
        {
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return WebApp.ApplicationsManager.GetKxCustomSetting(attributes.GenericStringIndex);
        }

        public override Type GetTargetObjectType()
        {
            return typeof(KxCustomSetting);
        }
    }
}
