﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Core.Web.PageRenderer;
using Kx.Objects.Student.Web;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class BEDCHARGETYPEIDXSEM : StudentObjectTranslatorSource
    {
        public BEDCHARGETYPEIDXSEM(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "BEDCHARGETYPEIDXSEM", "Current Bedroom Type Charge Semester Payment", "PN")
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            if (WebApp.CurrentBedroomTypeChargeIndex == -1)
            {
                WebApp.AllocationManager.CreateBedroomChargeFromPreference();
            }
            return WebApp.CurrentBedroomTypeCharge.GetSemesterPayment(ObjectTranslator.PaymentNo);
        }

        public override Type GetTargetObjectType()
        {
            return typeof(KxWebPaymentSchedule);
        }
    }
}
