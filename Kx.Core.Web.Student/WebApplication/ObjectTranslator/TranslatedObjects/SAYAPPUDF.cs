﻿using Kx.Core.Web.PageRenderer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Objects.Common.Web;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class SAYAPPUDF : StudentObjectTranslatorSource
    {
        public SAYAPPUDF(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "SAYAPPUDF", "Application UDF", "UD")
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            if (attributes.UDF > 0)
            {
                return WebUser.CurrentApplication.UserDefinedValues.GetValueById(attributes.UDF);
            }

            return WebUser.CurrentApplication.UserDefinedValues.GetValueById(WebApp.ProcessConfiguration.GetUDFId(attributes.UDFProcess));
        }

        public override Type GetTargetObjectType()
        {
            return typeof (KxUserDefinedValue);
        }
    }
}
