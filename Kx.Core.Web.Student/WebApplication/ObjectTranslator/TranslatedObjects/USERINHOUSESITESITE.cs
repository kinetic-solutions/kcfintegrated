﻿using Kx.Core.Web.PageRenderer;
using Kx.Objects.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class USERINHOUSESITESITE : StudentObjectTranslatorSource
    {
        public USERINHOUSESITESITE(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "USERINHOUSESITESITE", "Inhouse allocation site", string.Empty)
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return WebUser.InHouseAllocation.StudentAllocation.ResidentialRoom.Block.Site;
        }

        public override Type GetTargetObjectType()
        {
            return typeof (KxSite);
        }
    }
}
