﻿using Kx.Core.Web.PageRenderer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Objects.Student.Web;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class USERREJANCILCHARGEIDX : StudentObjectTranslatorSource
    {
        public USERREJANCILCHARGEIDX(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "USERREJANCILCHARGEIDX", "Rejected allocation ancillary charge (by index GS)", string.Empty)
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return WebUser.RejectedAllocation.StudentAllocation.AllocationAncillaryCharges.GetChargeById(attributes.GenericStringIndex);
        }

        public override Type GetTargetObjectType()
        {
            return typeof (KxWebAllocationCharge);
        }
    }
}
