﻿using Kx.Core.Web.ObjectTranslator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Core.Web.PageRenderer;
using Kx.Core.Web.WorkflowInterface.WorkflowExtenders;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class EXT : StudentObjectTranslatorSource
    {
        public EXT(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "EXT", "Workflow Extender", string.Empty)
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return PageRenderer.WorkflowInterface.Extender;
        }

        public override Type GetTargetObjectType()
        {
            return typeof(KxWorkflowExtender);
        }
    }
}
