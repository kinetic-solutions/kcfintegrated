﻿using Kx.Core.Web.PageRenderer;
using Kx.Objects.Student.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    [Depricated]
    public class ALLOCPRICINGID : StudentObjectTranslatorSource
    {
        public ALLOCPRICINGID(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "ALLOCPRICINGID", "Web allocation pricing", string.Empty)
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return KxStudentObjectManager.WebAllocationPricingList.GetWebAllocationPricing(attributes.AllocationPricing);
        }

        public override Type GetTargetObjectType()
        {
            return typeof (KxWebAllocationPricing);
        }
    }
}
