﻿using Kx.Core.Web.PageRenderer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Objects.Student;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    [Depricated]
    public class LIBROOMS : StudentObjectTranslatorSource
    {
        public LIBROOMS(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "LIBROOMS", "Liberty Living Room rates", string.Empty)
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return WebUser.CurrentApplication.LibertyRoomRates;
        }

        public override Type GetTargetObjectType()
        {
            return typeof (KxWebStudentRoomLibertyRates);
        }
    }
}
