﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Core.Resources;
using Kx.Core.Web.ObjectTranslator;
using Kx.Core.Web.PageRenderer;
using Kx.Objects.Student.Web;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class WEBRESOURCE : StudentObjectTranslatorSource
    {
        public WEBRESOURCE(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "WEBRESOURCE", "Current Web Resource object", "")
        {
            
        }

        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return PageRenderer.ResourceManager.ResourceProvider.GetResourceObject(WebUser.CurrentWebResource);
        }

        public override Type GetTargetObjectType()
        {
            return typeof(KxWebResource);
        }
    }
}
