﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Core.Web.PageRenderer;
using Kx.Objects.Student;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class SAYAPPDOMICILE : StudentObjectTranslatorSource
    {
        public SAYAPPDOMICILE(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "SAYAPPDOMICILE", "Application Address (Domicile)", string.Empty)
        {
            
        }

        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return WebUser.CurrentApplication.DomicileProxy;
        }

        public override Type GetTargetObjectType()
        {
            return typeof(KxDomicile);
        }
    }
}
