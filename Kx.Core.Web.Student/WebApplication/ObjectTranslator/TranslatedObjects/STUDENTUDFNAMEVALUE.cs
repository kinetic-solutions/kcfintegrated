﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Core.Web.PageRenderer;
using Kx.Objects.Common;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class STUDENTUDFNAMEVALUE : StudentObjectTranslatorSource
    {
        public STUDENTUDFNAMEVALUE(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "STUDENTUDFNAMEVALUE", "Student custom field value by name", "GS")
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return Student.CustomFields.GetFieldValueByName(attributes.GenericStringIndex);
        }

        public override Type GetTargetObjectType()
        {
            return typeof(string);
        }
    }
}
