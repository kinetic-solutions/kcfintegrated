﻿using Kx.Core.Web.PageRenderer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Objects.Student;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class USERINHOUSETERMDATE : StudentObjectTranslatorSource
    {
        public USERINHOUSETERMDATE(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "USERINHOUSETERMDATE", "Inhouse allocation term date", string.Empty)
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return WebUser.InHouseAllocation.StudentAllocation.TermDates.GetTermDateByIndex(this.ObjectTranslator.CurrentTermDate);
        }

        public override Type GetTargetObjectType()
        {
            return typeof (KxStudentTermDate);
        }
    }
}
