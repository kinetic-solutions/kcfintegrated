﻿using Kx.Core.Web.PageRenderer;
using Kx.Objects.Student.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    [Depricated]
    public class DUEDATEID : StudentObjectTranslatorSource
    {
        public DUEDATEID(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "DUEDATEID", "Due date by ID", "DD")
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return KxStudentObjectManager.AdjustedDueDates.GetAdjustedDueDate(attributes.DueDate);
        }

        public override Type GetTargetObjectType()
        {
            return typeof (KxWebAdjustedDueDate);
        }
    }
}
