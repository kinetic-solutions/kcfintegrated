﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Core.Web.PageRenderer;
using Kx.Objects.Common;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class STUDENTUDF : StudentObjectTranslatorSource
    {
        public STUDENTUDF(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "STUDENTUDF", "Student UDF by ID", "GI")
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return Student.CustomFields.GetUDFById(attributes.GenericIndex);
        }

        public override Type GetTargetObjectType()
        {
            return typeof(KxCustomFields2Value);
        }
    }
}
