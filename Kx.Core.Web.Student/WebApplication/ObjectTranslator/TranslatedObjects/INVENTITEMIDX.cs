﻿using Kx.Core.Web.PageRenderer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Objects.Student.Inventory;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class INVENTITEMIDX : StudentObjectTranslatorSource
    {
        public INVENTITEMIDX(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "INVENTITEMIDX", "Inventory room item", "GI;GX")
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return WebApp.InventoryManager.InventoryRoomItem(attributes.GenericIndex,attributes.GenericIndex2);
        }

        public override Type GetTargetObjectType()
        {
            return typeof (OccupantInventoryItem);
        }
    }
}
