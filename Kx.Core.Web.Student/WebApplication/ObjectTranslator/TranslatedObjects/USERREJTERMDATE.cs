﻿using Kx.Core.Web.PageRenderer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Objects.Student;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class USERREJTERMDATE : StudentObjectTranslatorSource
    {
        public USERREJTERMDATE(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "USERREJTERMDATE", "Rejected allocation current term date", string.Empty)
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return WebUser.RejectedAllocation.StudentAllocation.TermDates.GetTermDateByIndex(this.ObjectTranslator.CurrentTermDate);
        }

        public override Type GetTargetObjectType()
        {
            return typeof (KxStudentTermDate);
        }
    }
}
