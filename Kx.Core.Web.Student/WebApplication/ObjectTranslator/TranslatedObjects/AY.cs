﻿using Kx.Core.Web.PageRenderer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Objects.Student;
using Kx.Objects.Student.DataProviders;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class AY : StudentObjectTranslatorSource
    {
        public AY(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "AY", "Academic Year", string.Empty)
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            if (attributes.AcademicYear > 0 && attributes.AcademicYear != 1111)
            {
                return StudentCoreDataProvider.GetAcademicYear(attributes.AcademicYear);
            }

            return WebApp.ApplicationsManager.CurrentAcademicYear;
        }

        public override Type GetTargetObjectType()
        {
            return typeof (KxAcademicYear);
        }
    }
}
