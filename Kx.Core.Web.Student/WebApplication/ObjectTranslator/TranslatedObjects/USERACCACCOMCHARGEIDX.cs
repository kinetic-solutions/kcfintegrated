﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Core.Web.PageRenderer;
using Kx.Objects.Student.Web;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class USERACCACCOMCHARGEIDX : StudentObjectTranslatorSource
    {
        public USERACCACCOMCHARGEIDX(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "USERACCACCOMCHARGEIDX", "Web User Accepted Allocation Charge Index", "GS")
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return WebUser.AcceptedAllocation.StudentAllocation.AllocationAccommodationCharges
                                    .GetChargeById(attributes.GenericStringIndex);
        }

        public override Type GetTargetObjectType()
        {
            return typeof(KxWebAllocationCharge);
        }
    }
}
