﻿using Kx.Core.Web.PageRenderer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Objects.Student.Web;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class SAYAPPCORRESPONDENCE : StudentObjectTranslatorSource
    {
        public SAYAPPCORRESPONDENCE(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "SAYAPPCORRESPONDENCE", "Correspondence", string.Empty)
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return WebUser.CurrentApplication.Correspondence.GetCorrespondence(attributes.CorrespondenceId);
        }

        public override Type GetTargetObjectType()
        {
            return typeof (KxWebStudentCorrespondance);
        }
    }
}
