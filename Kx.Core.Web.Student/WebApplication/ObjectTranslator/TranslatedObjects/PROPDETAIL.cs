﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Core.Exceptions;
using Kx.Core.Web.PageRenderer;
using Kx.Core.Web.PageRenderer.Utility;
using Kx.Objects.Student.Web;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class PROPDETAIL : StudentObjectTranslatorSource
    {
        public PROPDETAIL(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base (objectTranslator, pageRenderer, "PROPDETAIL", "PPP Property details", string.Empty)
        {
            
        }

        public override object GetTargetObject(KxWebAttribute attributes)
        {
            try
            {
                var allocation = WebUser.AcceptedAllocation ?? WebUser.PendingAllocation;
                var application = WebUser.CurrentApplication;

                int siteId = allocation != null
                    ? allocation.StudentAllocation.ResidentialRoom.Block.SiteId
                    : application.FirstPreference.SiteId;
                int areaId = allocation != null
                    ? allocation.StudentAllocation.ResidentialRoom.Block.AreaId
                    : application.FirstPreference.AreaId;
                int blockId = allocation != null
                    ? allocation.StudentAllocation.ResidentialRoom.Block.BlockId
                    : application.FirstPreference.BlockId;

                return KxPropertyDetail.GetPropertyDetail(siteId, areaId, blockId);
            }
            catch (Exception e)
            {
                KxLogger.LogActivity(this, "GetTargetObject", e.Message, "");
                return null;
            }
        }

        public override Type GetTargetObjectType()
        {
            return typeof (KxPropertyDetail);
        }
    }
}
