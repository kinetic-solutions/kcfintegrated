﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Core.Web.PageRenderer;
using Kx.Objects.Common;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class CUSTOMSETTING : StudentObjectTranslatorSource
    {
        public CUSTOMSETTING(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "CUSTOMSETTING", "Custom Setting", "GS|GT")
        {
            AlternateKxAttributes = "CUSTOMSETTINGX";
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return WebApp.ApplicationsManager.GetCustomSetting(attributes.GenericStringIndex, attributes.GenericStringIndex2);
        }

        public override Type GetTargetObjectType()
        {
            return typeof(KxCustomSetting);
        }
    }
}
