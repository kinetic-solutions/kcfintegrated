﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Core.Merge;
using Kx.Core.Web.PageRenderer;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class STUDENTMERGE : StudentObjectTranslatorSource
    {
        public STUDENTMERGE(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "STUDENTMERGE", "Student merge tags", "MT|MI")
        {
            
        }

        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            Student.MergeTags.CurrentMergeTag = attributes.MergeTag;
            Student.MergeTags.CurrentMergeTagIndex = attributes.MergeTagIndex;
            return Student.MergeTags;
        }

        public override Type GetTargetObjectType()
        {
            return typeof(KxMergeTagProvider);
        }
    }
}
