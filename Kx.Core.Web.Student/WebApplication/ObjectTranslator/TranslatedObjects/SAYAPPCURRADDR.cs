﻿using Kx.Core.Web.PageRenderer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Objects.Student.Web;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public  class SAYAPPCURRADDR : StudentObjectTranslatorSource
    {
        public SAYAPPCURRADDR(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "SAYAPPCURRADDR", "Current Address", string.Empty)
        {
            
        }

        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return WebUser.CurrentApplication.CurrentStudentAddress;
        }

        public override Type GetTargetObjectType()
        {
            return typeof (KxWebStudentAddress);
        }
    }
}
