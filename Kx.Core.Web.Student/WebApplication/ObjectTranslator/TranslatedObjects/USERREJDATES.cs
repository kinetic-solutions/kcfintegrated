﻿using Kx.Core.Web.PageRenderer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Objects.Student;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class USERREJDATES : StudentObjectTranslatorSource
    {
        public USERREJDATES(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "USERREJDATES", "Rejected allocation dates", string.Empty)
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return WebUser.RejectedAllocation.StudentAllocation.AllocationDates;
        }

        public override Type GetTargetObjectType()
        {
            return typeof (KxStudentAllocationDates);
        }
    }
}
