﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Core.Web.PageRenderer;
using Kx.Objects.Student;
using Kx.Objects.Student.Web;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class LASTPAYMENTRAWDATA : StudentObjectTranslatorSource
    {
        public LASTPAYMENTRAWDATA(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "LASTPAYMENTRAWDATA", "Last Payment Response Raw Data", "GS")
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return WebUser.GetLastPaymentRaw(attributes.GenericStringIndex);
        }

        public override Type GetTargetObjectType()
        {
            return typeof(RawPaymentDataItem);
        }
    }
}
