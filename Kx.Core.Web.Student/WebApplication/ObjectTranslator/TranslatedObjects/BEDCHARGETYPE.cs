﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Core.Web.PageRenderer;
using Kx.Objects.Student.Web;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class BEDCHARGETYPE : StudentObjectTranslatorSource
    {
        public BEDCHARGETYPE(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "BEDCHARGETYPE", "Bedroom Type Charges", string.Empty)
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            if (attributes.DistinctFields != "")
            {
                WebApp.BedroomTypeCharges.DistinctFields = attributes.DistinctFields;
            }
            return WebApp.BedroomTypeCharges;
        }

        public override Type GetTargetObjectType()
        {
            return typeof(KxWebBedroomTypeCharges);
        }
    }
}
