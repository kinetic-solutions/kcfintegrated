﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Core.Web.PageRenderer;
using Kx.Objects.Common;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class USERLASTROOMCFG : StudentObjectTranslatorSource
    {
        public USERLASTROOMCFG(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "USERLASTROOMCFG", "Web User Last Room Configuration", string.Empty)
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return WebApp.ContractManager.LastRoomConfiguration;
        }

        public override Type GetTargetObjectType()
        {
            return typeof(KxRoomConfig);
        }
    }
}
