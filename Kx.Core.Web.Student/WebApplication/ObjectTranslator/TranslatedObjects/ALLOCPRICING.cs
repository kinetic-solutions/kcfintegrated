﻿using Kx.Core.Web.PageRenderer;
using Kx.Objects.Student.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    [Depricated]
    public class ALLOCPRICING : StudentObjectTranslatorSource
    {
        public ALLOCPRICING(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "ALLOCPRICING", "Web allocation pricing", string.Empty)
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return KxStudentObjectManager.WebAllocationPricingList.GetWebAllocationPricing(WebUser.CurrentAllocationPricing);
        }

        public override Type GetTargetObjectType()
        {
            return typeof (KxWebAllocationPricing);
        }
    }
}
