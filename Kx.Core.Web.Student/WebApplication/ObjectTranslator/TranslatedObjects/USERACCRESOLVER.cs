﻿using Kx.Core.Web.PageRenderer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Objects.Student.Web;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class USERACCRESOLVER : StudentObjectTranslatorSource
    {
        public USERACCRESOLVER(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "USERACCRESOLVER", "Accepted allocation URL Resolver", string.Empty)
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return WebApp.UrlResolver.GetWebUrlResolver(WebUser.AcceptedAllocation.StudentAllocation);
        }

        public override Type GetTargetObjectType()
        {
            return typeof (KxWebUrlResolver);
        }
    }
}
