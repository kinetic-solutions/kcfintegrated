﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Core.Web.PageRenderer;
using Kx.Objects.Student;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class USERLAST : StudentObjectTranslatorSource
    {
        public USERLAST(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "USERLAST", "Web User Last Allocation (KxAllocation)", string.Empty)
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            KxStudentAllocation result = null;
            if (WebUser.LastAllocation != null)
            {
                result = WebUser.LastAllocation.StudentAllocation;
            }
            return result;
        }

        public override Type GetTargetObjectType()
        {
            return typeof(KxStudentAllocation);
        }
    }
}
