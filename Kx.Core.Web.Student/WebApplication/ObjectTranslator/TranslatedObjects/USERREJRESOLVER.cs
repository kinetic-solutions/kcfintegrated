﻿using Kx.Core.Web.PageRenderer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Objects.Student.Web;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class USERREJRESOLVER : StudentObjectTranslatorSource
    {
        public USERREJRESOLVER(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "USERREJRESOLVER", "Rejected allocation URL resolver", string.Empty)
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return WebApp.UrlResolver.GetWebUrlResolver(WebUser.RejectedAllocation.StudentAllocation);
        }

        public override Type GetTargetObjectType()
        {
            return typeof (KxWebUrlResolver);
        }
    }
}
