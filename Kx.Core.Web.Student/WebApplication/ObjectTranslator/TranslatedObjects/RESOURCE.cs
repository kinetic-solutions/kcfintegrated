﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using Kx.Core.Resources;
using Kx.Core.Web.ObjectTranslator;
using Kx.Core.Web.PageRenderer;
using Kx.Core.Reflection;
using Kx.Core.Web.PageRenderer.Utility;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class RESOURCE : StudentObjectTranslatorSource
    {
        public RESOURCE(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "RESOURCE", "Specific Resource", "RN;RP")
        {
            
        }

        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            string resourceName = attributes.ResourceName;
            if (!string.IsNullOrWhiteSpace(attributes.ResourceProperty))
            {
                KxWebAttribute newAttribute = new KxWebAttribute(RemoveRDProperty(attributes.Id));
                ObjectTranslator.ObjectMapping(newAttribute);
                resourceName = (KxReflection.GetPropertyObject(newAttribute.TertiaryObject, attributes.ResourceProperty) ?? KxReflection.GetPropertyObject(newAttribute.SecondaryObject, newAttribute.ResourceProperty) ?? KxReflection.GetPropertyObject(newAttribute.PrimaryObject, attributes.ResourceProperty) ?? "").ToString();
            }
            return PageRenderer.ResourceManager.ResourceProvider.GetResourceObject(resourceName);
        }

        private string RemoveRDProperty(string controlId)
        {
            string result;
            result = controlId;
            string temp = controlId + "_";

            int start = temp.ToUpper().IndexOf("_RD") + 3;
            int end = temp.IndexOf("_", start);

            if (end > start)
            {
                var value = temp.Substring(start, end - start);
                result = result.Replace(string.Format("_RD{0}", value), "");
            }
            return result;
        }

        public override Type GetTargetObjectType()
        {
            return typeof (KxWebResource);
        }
    }
}
