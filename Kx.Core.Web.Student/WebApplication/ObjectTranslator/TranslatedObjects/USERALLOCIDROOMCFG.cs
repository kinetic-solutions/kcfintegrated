﻿using Kx.Core.Web.PageRenderer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Objects.Common;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class USERALLOCIDROOMCFG : StudentObjectTranslatorSource
    {
        public USERALLOCIDROOMCFG(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "USERALLOCIDROOMCFG", "Specified allocation ID room configuration", "GI")
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return WebApp.ContractManager.GetSpecifiedAllocationRoomConfiguration(attributes.GenericIndex);
        }

        public override Type GetTargetObjectType()
        {
            return typeof (KxRoomConfig);
        }
    }
}
