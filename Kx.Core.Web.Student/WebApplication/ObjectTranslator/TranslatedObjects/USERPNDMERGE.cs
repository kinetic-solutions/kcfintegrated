﻿using Kx.Core.Merge;
using Kx.Core.Web.PageRenderer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class USERPNDMERGE : StudentObjectTranslatorSource
    {
        public USERPNDMERGE(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "USERPNDMERGE", "Pending allocation merge tags", string.Empty)
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            WebUser.PendingAllocation.MergeTags.CurrentMergeTag = attributes.MergeTag;
            WebUser.PendingAllocation.MergeTags.CurrentMergeTagIndex = attributes.MergeTagIndex;
            return WebUser.PendingAllocation.MergeTags;
        }

        public override Type GetTargetObjectType()
        {
            return typeof (KxMergeTagProvider);
        }
    }
}
