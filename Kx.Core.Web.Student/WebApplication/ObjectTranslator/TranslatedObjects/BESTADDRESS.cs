﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Core.Web.PageRenderer;
using Kx.Objects.Student;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class BESTADDRESS : StudentObjectTranslatorSource
    {
        public BESTADDRESS(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "BESTADDRESS", "Student best address", string.Empty)
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return Student.BestAddress;
        }

        public override Type GetTargetObjectType()
        {
            return typeof(KxStudentAddress);
        }
    }
}
