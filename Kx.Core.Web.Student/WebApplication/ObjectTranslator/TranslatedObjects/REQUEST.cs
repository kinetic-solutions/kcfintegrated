﻿using Kx.Core.Web.PageRenderer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Objects.Student;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class REQUEST : StudentObjectTranslatorSource
    {
        public REQUEST(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "REQUEST", "Request, current", string.Empty)
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return WebApp.RequestManager.CurrentRequest;
        }

        public override Type GetTargetObjectType()
        {
            return typeof (KxStudentRequest);
        }
    }
}
