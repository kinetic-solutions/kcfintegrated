﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Core.Web.PageRenderer;
using Kx.Objects.Student;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class COURSECOURSETYPE : StudentObjectTranslatorSource
    {
        public COURSECOURSETYPE(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "COURSECOURSETYPE", "Current course type", string.Empty)
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return Student.Courses.CurrentCourse.Course.CourseType;
        }

        public override Type GetTargetObjectType()
        {
            return typeof(KxCourseType);
        }
    }
}
