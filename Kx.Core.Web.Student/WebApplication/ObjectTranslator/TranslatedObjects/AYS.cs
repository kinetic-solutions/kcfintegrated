﻿using Kx.Core.Web.PageRenderer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Objects.Student;
using Kx.Objects.Student.DataProviders;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class AYS : StudentObjectTranslatorSource
    {
        public AYS(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "AYS", "Academic Year Session", string.Empty)
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            if (WebUser.CurrentApplication != null)
            {
                return StudentCoreDataProvider.GetSession(WebUser.CurrentApplication.AcademicYearId,
                    WebUser.CurrentApplication.SessionID);
            }

            return StudentCoreDataProvider.GetSession(WebUser.CurrentAcademicYear, WebUser.CurrentAcademicYearSession);
        }

        public override Type GetTargetObjectType()
        {
            return typeof (KxAcademicYearSession);
        }
    }
}
