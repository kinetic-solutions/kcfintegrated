﻿#region copyright

// =======================================================
// Product:   KCF.Student
// Project:   Kx.Core.Web.Student
// File:      PAYTOSTUDYHISTORYIDX.cs
// Created:   22/11/2016
// By:        Steve Ashley
// Modified:  22/11/2016
// 
// Copyright © Kinetic Solutions Ltd 2016
// All rights reserved.
// 
// No portion of this file may be re-created, copied or otherwise
// used without the express permission of Kinetic Solutions Ltd.
// =======================================================

#endregion

#region usings

using System;
using System.Linq;
using Kx.Core.Web.PageRenderer;
using Kx.Core.Web.Student.PayToStudy;

#endregion

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class PAYTOSTUDYHISTORYIDX : StudentObjectTranslatorSource
    {
        #region Constructors

        public PAYTOSTUDYHISTORYIDX(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "PAYTOSTUDYHISTORYIDX", "Pay To Study History Item (GS)", string.Empty)
        {
        }

        #endregion

        #region Public Methods

        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return WebApp.PayToStudyManager.PaymentHistory.FirstOrDefault(s => s.TransactionID == attributes.GenericStringIndex);
        }

        public override Type GetTargetObjectType()
        {
            return typeof(PayToStudyHistoryResponsePayment);
        }

        #endregion
    }
}