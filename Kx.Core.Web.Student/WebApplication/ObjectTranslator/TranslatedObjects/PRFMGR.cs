﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Core.Web.PageRenderer;
using Kx.Core.Web.Student.WebApplication.ProcessManagers;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class PRFMGR : StudentObjectTranslatorSource
    {
        public PRFMGR(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "PRFMGR", "Preference Manager", string.Empty)
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return WebApp.PreferenceManager;
        }

        public override Type GetTargetObjectType()
        {
            return typeof(KxPreferenceManager);
        }
    }
}
