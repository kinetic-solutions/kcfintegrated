﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Core.Web.PageRenderer;
using Kx.Objects.Student;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class USERACCOFFERLTR : StudentObjectTranslatorSource
    {
        public USERACCOFFERLTR(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "USERACCOFFERLTR", "Student Offer Correspondence", string.Empty)
        {
            
        }

        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return WebUser.AcceptedAllocation.StudentAllocation.OfferCorrespondence;
        }

        public override Type GetTargetObjectType()
        {
            return typeof (KxStudentCorrespondence);
        }
    }
}
