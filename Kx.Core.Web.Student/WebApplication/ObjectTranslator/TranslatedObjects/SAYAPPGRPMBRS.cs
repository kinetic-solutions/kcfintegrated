﻿using Kx.Core.Web.PageRenderer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Objects.Student.Web;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class SAYAPPGRPMBRS : StudentObjectTranslatorSource
    {
        public SAYAPPGRPMBRS(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "SAYAPPGRPMBRS", "Friendship group current member", string.Empty)
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return WebApp.ApplicationsManager.CurrentGroupMember;
        }

        public override Type GetTargetObjectType()
        {
            return typeof (KxWebFriendshipGroupMember);
        }
    }
}
