﻿using Kx.Core.Web.PageRenderer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Objects.Student;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class USERINHOUSEPRICING : StudentObjectTranslatorSource
    {
        public USERINHOUSEPRICING(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "USERINHOUSEPRICING", "Inhouse room pricing", string.Empty)
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return WebUser.InHouseAllocation.StudentAllocation.StandardRoomPricing;
        }

        public override Type GetTargetObjectType()
        {
            return typeof (KxRoomPricing);
        }
    }
}
