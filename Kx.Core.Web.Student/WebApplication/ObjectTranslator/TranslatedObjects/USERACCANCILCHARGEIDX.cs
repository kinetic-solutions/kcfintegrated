﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Core.Web.PageRenderer;
using Kx.Objects.Student.Web;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class USERACCANCILCHARGEIDX : StudentObjectTranslatorSource
    {
        public USERACCANCILCHARGEIDX(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "USERACCANCILCHARGEIDX", "Web User Accepted Allocation Ancillary Charge Index", "GS")
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return WebUser.AcceptedAllocation.StudentAllocation.AllocationAncillaryCharges.GetChargeById(
                                    attributes.GenericStringIndex);
        }

        public override Type GetTargetObjectType()
        {
            return typeof(KxWebAllocationCharge);
        }
    }
}
