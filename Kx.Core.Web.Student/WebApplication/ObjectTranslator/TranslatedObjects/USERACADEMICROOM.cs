﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Core.Web.PageRenderer;
using Kx.Objects.Common;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class USERACADEMICROOM : StudentObjectTranslatorSource
    {
        public USERACADEMICROOM(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "USERACADEMICROOM", "Web User Last Allocation Academic Year Room", string.Empty)
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return WebUser.LastAllocation.StudentAllocation.AcademicYearResidentialRoom;
        }

        public override Type GetTargetObjectType()
        {
            return typeof(KxResidentialRoom);
        }
    }
}
