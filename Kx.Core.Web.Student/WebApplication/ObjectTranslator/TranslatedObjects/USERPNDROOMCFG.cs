﻿using Kx.Core.Web.PageRenderer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Objects.Common;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class USERPNDROOMCFG : StudentObjectTranslatorSource
    {
        public USERPNDROOMCFG(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "USERPNDROOMCFG", "Pending allocation room config", string.Empty)
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return WebApp.ContractManager.PendingRoomConfiguration;
        }

        public override Type GetTargetObjectType()
        {
            return typeof (KxRoomConfig);
        }
    }
}
