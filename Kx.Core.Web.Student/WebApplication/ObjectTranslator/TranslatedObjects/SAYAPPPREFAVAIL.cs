﻿using Kx.Core.Web.PageRenderer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Objects.Student.Web;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class SAYAPPPREFAVAIL : StudentObjectTranslatorSource
    {
        public SAYAPPPREFAVAIL(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "SAYAPPPREFAVAIL", "Current number preference availability", string.Empty)
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return WebUser.CurrentApplication.CurrentNumberPreference.PreferenceAvailability;
        }

        public override Type GetTargetObjectType()
        {
            return typeof (KxPreferenceAvailability);
        }
    }
}
