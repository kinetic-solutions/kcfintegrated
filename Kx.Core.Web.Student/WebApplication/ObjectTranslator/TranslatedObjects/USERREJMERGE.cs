﻿using Kx.Core.Merge;
using Kx.Core.Web.PageRenderer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class USERREJMERGE : StudentObjectTranslatorSource
    {
        public USERREJMERGE(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "USERREJMERGE", "Rejected allocation merge tags", string.Empty)
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            WebUser.RejectedAllocation.MergeTags.CurrentMergeTag = attributes.MergeTag;
            WebUser.RejectedAllocation.MergeTags.CurrentMergeTagIndex = attributes.MergeTagIndex;
            return WebUser.RejectedAllocation.MergeTags;
        }

        public override Type GetTargetObjectType()
        {
            return typeof (KxMergeTagProvider);
        }
    }
}
