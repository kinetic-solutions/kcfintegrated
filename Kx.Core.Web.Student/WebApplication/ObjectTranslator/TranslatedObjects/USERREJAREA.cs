﻿using Kx.Core.Web.PageRenderer;
using Kx.Objects.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class USERREJAREA : StudentObjectTranslatorSource
    {
        public USERREJAREA(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "USERREJAREA", "Rejected allocation area", string.Empty)
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return WebUser.RejectedAllocation.StudentAllocation.ResidentialRoom.Block.Area;
        }

        public override Type GetTargetObjectType()
        {
            return typeof (KxArea);
        }
    }
}
