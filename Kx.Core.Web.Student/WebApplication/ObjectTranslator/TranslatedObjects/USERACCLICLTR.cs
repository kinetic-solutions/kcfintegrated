﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Core.Web.PageRenderer;
using Kx.Objects.Student;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class USERACCLICLTR : StudentObjectTranslatorSource
    {
        public USERACCLICLTR(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "USERACCLICLTR", "Web User Accepted Allocation License Correspondance", string.Empty)
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return WebUser.AcceptedAllocation.StudentAllocation.LicenceCorrespondence;
        }

        public override Type GetTargetObjectType()
        {
            return typeof(KxStudentCorrespondence);
        }
    }
}
