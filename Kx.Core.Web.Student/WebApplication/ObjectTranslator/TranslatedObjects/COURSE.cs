﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Core.Web.PageRenderer;
using Kx.Objects.Student;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class COURSE : StudentObjectTranslatorSource
    {
        public COURSE(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "COURSE", "Student current course", string.Empty)
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return Student.Courses.CurrentCourse;
        }

        public override Type GetTargetObjectType()
        {
            return typeof (KxStudentCourse);
        }
    }
}
