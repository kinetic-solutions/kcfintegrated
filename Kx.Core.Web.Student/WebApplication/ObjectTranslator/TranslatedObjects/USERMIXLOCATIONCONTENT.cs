﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Core.Web.PageRenderer;
using Kx.Objects.Student;
using Kx.Objects.Student.Web;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class USERMIXLOCATIONCONTENT : StudentObjectTranslatorSource
    {
        public USERMIXLOCATIONCONTENT(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "USERMIXLOCATIONCONTENT", "Pending, expired, inhouse or Accepted allocations location content", string.Empty)
        {

        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            if(WebUser.PendingAllocation != null)
            {
                return WebUser.PendingAllocation.LocationContent;
            }
            else if(WebUser.ExpiredAllocation != null)
            {
                return WebUser.ExpiredAllocation.LocationContent;
            }
            else if(WebUser.InHouseAllocation != null)
            {
                return WebUser.InHouseAllocation.LocationContent;
            }
            else 
            {
                return WebUser.AcceptedAllocation.LocationContent;
            }
        }

        public override Type GetTargetObjectType()
        {
            return typeof(KxLocationContent);
        }
    }
}
