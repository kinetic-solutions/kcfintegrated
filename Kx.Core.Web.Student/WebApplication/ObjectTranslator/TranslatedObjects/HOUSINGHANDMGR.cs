﻿#region copyright

// =======================================================
// Product:   KCF.Student
// Project:   Kx.Core.Web.Student
// File:      PAYTOSTUDYMGR.cs
// Created:   27/09/2016
// By:        Steve Ashley
// Modified:  04/10/2016
// 
// Copyright © Kinetic Solutions Ltd 2016
// All rights reserved.
// 
// No portion of this file may be re-created, copied or otherwise
// used without the express permission of Kinetic Solutions Ltd.
// =======================================================

#endregion

#region usings

using System;
using Kx.Core.Web.PageRenderer;
using Kx.Core.Web.Student.WebApplication.ProcessManagers;

#endregion

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class HOUSINGHANDMGR : StudentObjectTranslatorSource
    {
        #region Constructors

        public HOUSINGHANDMGR(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "HOUSINGHANDMGR", "Housing Hand Manager", string.Empty)
        {
        }

        #endregion

        #region Public Methods

        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return WebApp.HousingHandManager;
        }

        public override Type GetTargetObjectType()
        {
            return typeof(KxHousingHandManager);
        }

        #endregion
    }
}