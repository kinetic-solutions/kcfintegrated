﻿using Kx.Core.Web.PageRenderer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Objects.Student.Web;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class USERSOCPROF : StudentObjectTranslatorSource
    {
        public USERSOCPROF(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "USERSOCPROF", "Social profile", string.Empty)
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return WebUser.SocialProfile;
        }

        public override Type GetTargetObjectType()
        {
            return typeof (KxWebStudentSocialProfile);
        }
    }
}
