﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Core.Web.PageRenderer;
using Kx.Objects.Student.Web;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class USERLASTSCHED : StudentObjectTranslatorSource
    {
        public USERLASTSCHED(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "USERLASTSCHED", "Web User Last Allocation Schedule", string.Empty)
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return WebUser.LastAllocation.Schedule;
        }

        public override Type GetTargetObjectType()
        {
            return typeof(KxWebStudentAllocationSchedule);
        }
    }
}
