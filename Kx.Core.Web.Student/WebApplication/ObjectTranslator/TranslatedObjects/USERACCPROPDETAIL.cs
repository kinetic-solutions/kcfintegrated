﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Core.Merge;
using Kx.Core.Web.PageRenderer;
using Kx.Objects.Student.Web;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class USERACCPROPDETAIL : StudentObjectTranslatorSource
    {
        public USERACCPROPDETAIL(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "USERACCPROPDETAIL", "Accepted allocation property detail", string.Empty)
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return WebUser.AcceptedAllocation.PropertyDetail;
        }

        public override Type GetTargetObjectType()
        {
            return typeof (KxPropertyDetail);
        }
    }
}
