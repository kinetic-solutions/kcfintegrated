﻿using Kx.Core.Web.PageRenderer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Objects.Student;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class USERPNDALLOCDATEIDX : StudentObjectTranslatorSource
    {
        public USERPNDALLOCDATEIDX(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "USERPNDALLOCDATEIDX", "Pending allocation date (by index GI)", string.Empty)
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return WebUser.PendingAllocation.StudentAllocation.AllocationModel.GetAllocationModelDateById(attributes.GenericIndex);
        }

        public override Type GetTargetObjectType()
        {
            return typeof (KxAllocationModelDate);
        }
    }
}
