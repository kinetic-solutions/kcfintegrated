﻿using Kx.Core.Web.PageRenderer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Objects.Student.Web;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    [Depricated]
    public class BOOKINGELIGIBILITYRULES : StudentObjectTranslatorSource
    {
        public BOOKINGELIGIBILITYRULES(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "BOOKINGELIGIBILITYRULES", "Eligability rule list", string.Empty)
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return KxStudentObjectManager.BookingEligibilityRuleList;
        }

        public override Type GetTargetObjectType()
        {
            return typeof (KxBookingEligibilityRuleList);
        }
    }
}
