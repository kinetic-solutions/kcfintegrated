﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Core.Web.PageRenderer;
using Kx.Objects.Student;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class STUDENTCUSTOM : StudentObjectTranslatorSource
    {
        public STUDENTCUSTOM(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "STUDENTCUSTOM", "Student Custom Data", "GS")
        {
            
        }

        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return Student.CustomData.GetCustomData(attributes.GenericStringIndex);
        }

        public override Type GetTargetObjectType()
        {
            return typeof(KxStudentCustomData);
        }
    }
}
