﻿using Kx.Core.Web.PageRenderer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Objects.Student.Web;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class SAYAPPGRPMBRIDX : StudentObjectTranslatorSource
    {
        public SAYAPPGRPMBRIDX(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "SAYAPPGRPMBRIDX", "Friendship group member by index", "GM")
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            if (WebUser.CurrentApplication.FriendshipGroup.Members.Count >= attributes.GroupMember)
            {
                return WebUser.CurrentApplication.FriendshipGroup.Members[attributes.GroupMember - 1];
            }

            return null;
        }

        public override Type GetTargetObjectType()
        {
            return typeof (KxWebFriendshipGroupMember);
        }
    }
}
