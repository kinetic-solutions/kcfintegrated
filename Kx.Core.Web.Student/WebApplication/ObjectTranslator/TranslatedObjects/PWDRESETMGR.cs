﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Core.Web.PageRenderer;
using Kx.Core.Web.Student.WebApplication.ProcessManagers;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class PWDRESETMGR : StudentObjectTranslatorSource
    {
        public PWDRESETMGR(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "PWDRESETMGR", "Password Reset Manager", string.Empty)
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return WebApp.PasswordResetManager;
        }

        public override Type GetTargetObjectType()
        {
            return typeof(KxPasswordResetManager);
        }
    }
}
