﻿using Kx.Core.Web.PageRenderer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Objects.Student;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    [Depricated]
    public class CITYROOMID : StudentObjectTranslatorSource
    {
        public CITYROOMID(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "CITYROOMID", "City Living room", string.Empty)
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return WebUser.CurrentApplication.CityRoomRates.GetRoomByUniqueId(WebUser.CurrentCityRoomId);
        }

        public override Type GetTargetObjectType()
        {
            return typeof (KxWebStudentRoomCityRate);
        }
    }
}
