﻿using Kx.Core.Web.PageRenderer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Objects.Student.Web;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class USERALLOCACCOMCHARGE : StudentObjectTranslatorSource
    {
        public USERALLOCACCOMCHARGE(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "USERALLOCACCOMCHARGE", "Specified allocation accommodation charges", string.Empty)
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return WebUser.SpecifiedAllocation.StudentAllocation.AllocationAccommodationCharges;
        }

        public override Type GetTargetObjectType()
        {
            return typeof (KxWebAllocationCharges);
        }
    }
}
