﻿using Kx.Core.Web.PageRenderer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Objects.Student;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class USERALLOCID : StudentObjectTranslatorSource
    {
        public USERALLOCID(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "USERALLOCID", "Specified allocation ID allocation", "GI")
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return WebUser.Allocations.GetAllocationById(attributes.GenericIndex).StudentAllocation;
        }

        public override Type GetTargetObjectType()
        {
            return typeof (KxStudentAllocation);
        }
    }
}
