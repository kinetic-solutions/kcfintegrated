﻿using Kx.Core.Web.PageRenderer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Objects.Student.Web;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class SAYAPPGRPMBR : StudentObjectTranslatorSource
    {
        public SAYAPPGRPMBR(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "SAYAPPGRPMBR", "Friendship group member", string.Empty)
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return WebUser.CurrentApplication.GroupMember;
        }

        public override Type GetTargetObjectType()
        {
            return typeof (KxWebFriendshipGroupMember);
        }
    }
}
