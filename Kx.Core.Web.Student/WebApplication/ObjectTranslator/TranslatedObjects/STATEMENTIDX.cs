﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Core.Web.PageRenderer;
using Kx.Objects.Student;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class STATEMENTIDX : StudentObjectTranslatorSource
    {
        public STATEMENTIDX(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "STATEMENTIDX", "Statement item", "GS")
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return WebApp.StatementManager.GetStudentStatement(attributes.GenericStringIndex);
        }

        public override Type GetTargetObjectType()
        {
            return typeof(KxStudentStatement);
        }
    }
}
