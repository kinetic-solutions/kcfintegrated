﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Core.Web.PageRenderer;
using Kx.Objects.Common;
using Kx.Objects.Student;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class USERMIXSITEADDR : StudentObjectTranslatorSource
    {
        public USERMIXSITEADDR(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "USERMIXSITEADDR", "Pending, expired, inhouse or Accepted allocation block address", string.Empty)
        {

        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            if(WebUser.PendingAllocation != null)
            {
                return WebUser.PendingAllocation.StudentAllocation.ResidentialRoom.Block.Address;
            }
            else if(WebUser.ExpiredAllocation != null)
            {
                return WebUser.ExpiredAllocation.StudentAllocation.ResidentialRoom.Block.Address;
            }
            else if(WebUser.InHouseAllocation != null)
            {
                return WebUser.InHouseAllocation.StudentAllocation.ResidentialRoom.Block.Address;
            }
            else
            {
                return WebUser.AcceptedAllocation.StudentAllocation.ResidentialRoom.Block.Address;
            }
        }

        public override Type GetTargetObjectType()
        {
            return typeof(KxAddress);
        }
    }
}
