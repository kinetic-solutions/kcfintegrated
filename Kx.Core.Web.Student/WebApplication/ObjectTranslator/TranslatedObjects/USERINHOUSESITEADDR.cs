﻿using Kx.Core.Web.PageRenderer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Objects.Common;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class USERINHOUSESITEADDR : StudentObjectTranslatorSource
    {
        public USERINHOUSESITEADDR(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "USERINHOUSESITEADDR", "Inhouse allocation site (block) address", string.Empty)
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return WebUser.InHouseAllocation.StudentAllocation.ResidentialRoom.Block.Address;
        }

        public override Type GetTargetObjectType()
        {
            return typeof (KxAddress);
        }
    }
}
