﻿using Kx.Core.Web.PageRenderer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Objects.Student.Web;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class USERWEBALLOCID : StudentObjectTranslatorSource
    {
        public USERWEBALLOCID(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "USERWEBALLOCID", "Specified allocation ID web allocation", "GI")
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return WebUser.Allocations.GetAllocationById(attributes.GenericIndex);
        }

        public override Type GetTargetObjectType()
        {
            return typeof (KxWebStudentAllocation);
        }
    }
}
