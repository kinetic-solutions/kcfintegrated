﻿using Kx.Core.Web.PageRenderer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Objects.Student.Web;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class USERWEBALLOC : StudentObjectTranslatorSource
    {
        public USERWEBALLOC(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "USERWEBALLOC", "Specified web allocation", string.Empty)
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return WebUser.SpecifiedAllocation;
        }

        public override Type GetTargetObjectType()
        {
            return typeof (KxWebStudentAllocation);
        }
    }
}
