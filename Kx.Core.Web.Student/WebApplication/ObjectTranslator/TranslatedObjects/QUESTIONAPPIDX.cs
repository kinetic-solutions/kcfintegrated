﻿using Kx.Core.Web.PageRenderer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Objects.Student;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class QUESTIONAPPIDX : StudentObjectTranslatorSource
    {
        public QUESTIONAPPIDX(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "QUESTIONAPPIDX", "Induction question attempt by ID", "GI")
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return WebUser.CurrentApplication.QuestionAttempts.GeteInductionQuestionAttempt(attributes.GenericIndex);
        }

        public override Type GetTargetObjectType()
        {
            return typeof (KxeInductionQuestionAttempt);
        }
    }
}
