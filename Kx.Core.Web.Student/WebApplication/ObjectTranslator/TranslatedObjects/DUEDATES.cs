﻿using Kx.Core.Web.PageRenderer;
using Kx.Objects.Student.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    [Depricated]
    public class DUEDATES : StudentObjectTranslatorSource
    {
        public DUEDATES(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "DUEDATES", "Due dates", string.Empty)
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return KxStudentObjectManager.AdjustedDueDates;
        }

        public override Type GetTargetObjectType()
        {
            return typeof (KxWebAdjustedDueDates);
        }
    }
}
