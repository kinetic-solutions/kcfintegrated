﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Core.Web.PageRenderer;
using Kx.Objects.Common;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class USERLASTROOMTYPE : StudentObjectTranslatorSource
    {
        public USERLASTROOMTYPE(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "USERLASTROOMTYPE", "Web User Last Allocation Bedroom Type", string.Empty)
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return WebUser.LastAllocation.StudentAllocation.ResidentialRoom.BedroomType;
        }

        public override Type GetTargetObjectType()
        {
            return typeof(KxBedroomType);
        }
    }
}
