﻿using Kx.Core.Web.PageRenderer;
using Kx.Objects.Student.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    [Depricated]
    public class BOOKINGELIGIBILITYRULEID : StudentObjectTranslatorSource
    {
        public BOOKINGELIGIBILITYRULEID(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "BOOKINGELIGIBILITYRULEID", "Eligibility rule by ID", "GS")
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return KxStudentObjectManager.BookingEligibilityRuleList.GetBookingEligibilityRule(attributes.GenericStringIndex);
        }

        public override Type GetTargetObjectType()
        {
            return typeof (KxBookingEligibilityRule);
        }
    }
}
