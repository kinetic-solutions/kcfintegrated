﻿using Kx.Core.Web.PageRenderer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Objects.Student.Web;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class USERACCSCHED : StudentObjectTranslatorSource
    {
        public USERACCSCHED(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "USERACCSCHED", "Accepted allocation payment schedule", string.Empty)
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return WebUser.AcceptedAllocation.Schedule;
        }

        public override Type GetTargetObjectType()
        {
            return typeof (KxWebStudentAllocationSchedule);
        }
    }
}
