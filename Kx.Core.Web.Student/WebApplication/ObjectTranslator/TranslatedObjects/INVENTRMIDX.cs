﻿using Kx.Core.Web.PageRenderer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Objects.Student.Inventory;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class INVENTRMIDX : StudentObjectTranslatorSource
    {
        public INVENTRMIDX(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "INVENTRMIDX", "Inventory room", "GI")
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return WebApp.InventoryManager.InventoryRoom(attributes.GenericIndex);
        }

        public override Type GetTargetObjectType()
        {
            return typeof (OccupantInventoryRoom);
        }
    }
}
