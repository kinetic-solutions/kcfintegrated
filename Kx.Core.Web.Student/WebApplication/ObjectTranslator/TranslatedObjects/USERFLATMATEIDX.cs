﻿using Kx.Core.Web.PageRenderer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Objects.Student;
using Kx.Objects.Student.Web;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class USERFLATMATEIDX : StudentObjectTranslatorSource
    {
        public USERFLATMATEIDX(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "USERFLATMATEIDX", "Flat mate for inhouse/accepted allocation index", "GI")
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            KxStudentAllocation alloc = null;

            if (WebUser.SpecifiedAllocation != null)
            {
                alloc = WebUser.SpecifiedAllocation.StudentAllocation;
            }
            else if (WebUser.InHouseAllocation != null)
            {
                alloc = WebUser.InHouseAllocation.StudentAllocation;
            }
            else
            {
                alloc = WebUser.AcceptedAllocation.StudentAllocation;
            }

            if (alloc != null)
            {
                return alloc.Flatmates.Flatmates.ElementAtOrDefault(attributes.GenericIndex) ?? new KxWebStudentSocialProfileFlatmate();
            }

            return new KxWebStudentSocialProfileFlatmate();
        }

        public override Type GetTargetObjectType()
        {
            return typeof (KxWebStudentSocialProfileFlatmate);
        }
    }
}
