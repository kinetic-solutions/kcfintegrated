﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Core.Web.PageRenderer;
using Kx.Objects.Student;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class USERACCALLOCDATEIDX : StudentObjectTranslatorSource
    {
        public USERACCALLOCDATEIDX(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "USERACCALLOCDATEIDX", "Accepted Allocation Model Date By ID", "GI")
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return WebUser.AcceptedAllocation.StudentAllocation.AllocationModel.GetAllocationModelDateById(
                                    attributes.GenericIndex);
        }

        public override Type GetTargetObjectType()
        {
            return typeof(KxAllocationModelDate);
        }
    }
}
