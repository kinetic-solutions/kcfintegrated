﻿using Kx.Core.Web.PageRenderer;
using Kx.Objects.Student;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class USERREJREASON : StudentObjectTranslatorSource
    {
        public USERREJREASON(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "USERREJREASON", "Rejected allocation reason", string.Empty)
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return WebUser.RejectedAllocation.StudentAllocation.RejectedReason;
        }

        public override Type GetTargetObjectType()
        {
            return typeof (KxStudentReason);
        }
    }
}
