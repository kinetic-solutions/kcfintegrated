﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Core.Web.PageRenderer;
using Kx.Objects.Student;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class USERLASTREASON : StudentObjectTranslatorSource
    {
        public USERLASTREASON(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "USERLASTREASON", "Web User Last Allocation (KxAllocation) Rejection Reason", string.Empty)
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return WebUser.LastAllocation.StudentAllocation.RejectedReason;
        }

        public override Type GetTargetObjectType()
        {
            return typeof(KxStudentReason);
        }
    }
}
