﻿using Kx.Core.Web.PageRenderer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Objects.Student;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class ALLOCARRIVALFROMSLOTS : StudentObjectTranslatorSource
    {
        public ALLOCARRIVALFROMSLOTS(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "ALLOCARRIVALFROMSLOTS", "Induction arrival slots >= Allocation arrival date", string.Empty)
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return WebUser.AcceptedAllocation.StudentAllocation.InductionArrivalLocation.FromAllocationArrivalSlots;
        }

        public override Type GetTargetObjectType()
        {
            return typeof (KxeInductionArrivalSlots);
        }
    }
}
