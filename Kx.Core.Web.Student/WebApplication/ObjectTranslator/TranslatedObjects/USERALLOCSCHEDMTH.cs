﻿using Kx.Core.Web.PageRenderer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Objects.Student.Web;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class USERALLOCSCHEDMTH : StudentObjectTranslatorSource
    {
        public USERALLOCSCHEDMTH(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "USERALLOCSCHEDMTH", "Specified allocation schedule monthly payment", string.Empty)
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return WebUser.SpecifiedAllocation.Schedule.GetMonthlyPayment(this.ObjectTranslator.PaymentNo);
        }

        public override Type GetTargetObjectType()
        {
            return typeof (KxWebPaymentSchedule);
        }
    }
}
