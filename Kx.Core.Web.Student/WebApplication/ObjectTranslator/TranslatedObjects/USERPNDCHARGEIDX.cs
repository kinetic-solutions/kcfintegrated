﻿using Kx.Core.Web.PageRenderer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Objects.Student.Web;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class USERPNDCHARGEIDX : StudentObjectTranslatorSource
    {
        public USERPNDCHARGEIDX(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "USERPNDCHARGEIDX", "Pending allocation charge (by index GS)", string.Empty)
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return WebUser.PendingAllocation.StudentAllocation.AllocationCharges.GetChargeById(attributes.GenericStringIndex);
        }

        public override Type GetTargetObjectType()
        {
            return typeof (KxWebAllocationCharge);
        }
    }
}
