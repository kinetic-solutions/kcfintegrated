﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Core.Merge;
using Kx.Core.Web.PageRenderer;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class USERACCMERGE : StudentObjectTranslatorSource
    {
        public USERACCMERGE(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "USERACCMERGE", "Accepted allocation merge tag", string.Empty)
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            WebUser.AcceptedAllocation.MergeTags.CurrentMergeTag = attributes.MergeTag;
            WebUser.AcceptedAllocation.MergeTags.CurrentMergeTagIndex = attributes.MergeTagIndex;
            return WebUser.AcceptedAllocation.MergeTags;
        }

        public override Type GetTargetObjectType()
        {
            return typeof (KxMergeTagProvider);
        }
    }
}
