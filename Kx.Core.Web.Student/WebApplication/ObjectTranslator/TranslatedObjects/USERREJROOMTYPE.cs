﻿using Kx.Core.Web.PageRenderer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Objects.Common;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class USERREJROOMTYPE : StudentObjectTranslatorSource
    {
        public USERREJROOMTYPE(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "USERREJROOMTYPE", "Rejected allocation room type", string.Empty)
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return WebUser.RejectedAllocation.StudentAllocation.ResidentialRoom.BedroomType;
        }

        public override Type GetTargetObjectType()
        {
            return typeof (KxBedroomType);
        }
    }
}
