﻿using Kx.Core.Web.PageRenderer;
using Kx.Objects.Student;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class REQUESTIDX : StudentObjectTranslatorSource
    {
        public REQUESTIDX(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "REQUESTIDX", "Request from attributes", string.Empty)
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            KxStudentRequest result;
            if (attributes.RequestIdentifier > 0)
                result = WebUser.CurrentApplication.StudentRequests.Find(s => s.StudentRequestId == attributes.RequestIdentifier);
            else
                result =
                    WebUser.CurrentApplication.StudentRequests.Find(
                        s => s.StudentRequestId.ToString() == attributes.GenericStringIndex);
            return result;

        }

        public override Type GetTargetObjectType()
        {
            return typeof (KxStudentRequest);
        }
    }
}
