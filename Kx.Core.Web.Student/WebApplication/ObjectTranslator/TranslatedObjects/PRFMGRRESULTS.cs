﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Core.Web.PageRenderer;
using Kx.Objects.Student.Web;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class PRFMGRRESULTS : StudentObjectTranslatorSource
    {
        public PRFMGRRESULTS(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "PRFMGRRESULTS", "Preference manager search results", string.Empty)
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return WebApp.PreferenceManager.Results;
        }

        public override Type GetTargetObjectType()
        {
            return typeof(KxWebStudentPreferences);
        }
    }
}
