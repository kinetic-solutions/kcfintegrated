﻿using Kx.Core.Web.PageRenderer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Objects.Student;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class USERALLOCDATES : StudentObjectTranslatorSource
    {
        public USERALLOCDATES(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "USERALLOCDATES", "Specified allocation dates", string.Empty)
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return WebUser.SpecifiedAllocation.StudentAllocation.AllocationDates;
        }

        public override Type GetTargetObjectType()
        {
            return typeof (KxStudentAllocationDates);
        }
    }
}
