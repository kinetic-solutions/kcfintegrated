﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Core.Web.PageRenderer;
using Kx.Objects.Common;
using Kx.Objects.Student;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class USERMIXROOMTYPE : StudentObjectTranslatorSource
    {
        public USERMIXROOMTYPE(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "USERMIXROOMTYPE", "Pending, expired, inhouse or Accepted allocation room type", string.Empty)
        {

        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            if(WebUser.PendingAllocation != null)
            {
                return WebUser.PendingAllocation.StudentAllocation.ResidentialRoom.BedroomType;
            }
            else if(WebUser.ExpiredAllocation != null)
            {
                return WebUser.ExpiredAllocation.StudentAllocation.ResidentialRoom.BedroomType;
            }
            else if(WebUser.InHouseAllocation != null)
            {
                return WebUser.InHouseAllocation.StudentAllocation.ResidentialRoom.BedroomType;
            }
            else 
            {
                return WebUser.AcceptedAllocation.StudentAllocation.ResidentialRoom.BedroomType;
            }
        }

        public override Type GetTargetObjectType()
        {
            return typeof(KxBedroomType);
        }
    }
}
