﻿using Kx.Core.Web.PageRenderer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Objects.Student.Inventory;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class INVENTITEMCNDNTIDX : StudentObjectTranslatorSource
    {
        public INVENTITEMCNDNTIDX(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "INVENTITEMCNDNTIDX", "Inventory room item condition note", "GI;GX;GY")
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return WebApp.InventoryManager.InventoryItemConditionNode(attributes.GenericIndex,attributes.GenericIndex2, attributes.GenericIndex3);
        }

        public override Type GetTargetObjectType()
        {
            return typeof (OccupancyInventoryItemConditionNote);
        }
    }
}
