﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Core.Web.PageRenderer;
using Kx.Objects.Common;
using Kx.Objects.Student;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class USERMIXROOM : StudentObjectTranslatorSource
    {
        public USERMIXROOM(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "USERMIXROOM", "Pending, expired, inhouse or Accepted allocation room", string.Empty)
        {

        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            if(WebUser.PendingAllocation != null)
            {
                return WebUser.PendingAllocation.StudentAllocation.ResidentialRoom;
            }
            else if(WebUser.ExpiredAllocation != null)
            {
                return WebUser.ExpiredAllocation.StudentAllocation.ResidentialRoom;
            }
            else if(WebUser.InHouseAllocation != null)
            {
                return WebUser.InHouseAllocation.StudentAllocation.ResidentialRoom;
            }
            else 
            {
                return WebUser.AcceptedAllocation.StudentAllocation.ResidentialRoom;
            }
        }

        public override Type GetTargetObjectType()
        {
            return typeof(KxResidentialRoom);
        }
    }
}
