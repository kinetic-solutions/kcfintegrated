﻿using Kx.Core.Web.PageRenderer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Objects.Student;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class USERPNDTERMDATES : StudentObjectTranslatorSource
    {
        public USERPNDTERMDATES(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "USERPNDTERMDATES", "Pending allocation term dates", string.Empty)
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return WebUser.PendingAllocation.StudentAllocation.TermDates;
        }

        public override Type GetTargetObjectType()
        {
            return typeof (KxStudentTermDates);
        }
    }
}
