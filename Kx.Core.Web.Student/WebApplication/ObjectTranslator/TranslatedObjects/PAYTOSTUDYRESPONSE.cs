﻿#region copyright

// =======================================================
// Product:   KCF.Student
// Project:   Kx.Core.Web.Student
// File:      PAYTOSTUDYRESPONSE.cs
// Created:   04/10/2016
// By:        Steve Ashley
// Modified:  04/10/2016
// 
// Copyright © Kinetic Solutions Ltd 2016
// All rights reserved.
// 
// No portion of this file may be re-created, copied or otherwise
// used without the express permission of Kinetic Solutions Ltd.
// =======================================================

#endregion

#region usings

using System;
using Kx.Core.Web.PageRenderer;
using Kx.Core.Web.Student.Payments;

#endregion

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class PAYTOSTUDYRESPONSE : StudentObjectTranslatorSource
    {
        #region Constructors

        public PAYTOSTUDYRESPONSE(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "PAYTOSTUDYRESPONSE", "Pay To Study Response", string.Empty)
        {
        }

        #endregion

        #region Public Methods

        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return WebApp.PayToStudyManager.PaymentRequestResponse;
        }

        public override Type GetTargetObjectType()
        {
            return typeof(PayToStudyPaymentRequestResponse);
        }

        #endregion
    }
}