﻿using Kx.Core.Web.PageRenderer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Objects.Common;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class USERREJSITEADDR : StudentObjectTranslatorSource
    {
        public USERREJSITEADDR(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "USERREJSITEADDR", "Rejected allocation site (block) address", string.Empty)
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return WebUser.RejectedAllocation.StudentAllocation.ResidentialRoom.Block.Address;
        }

        public override Type GetTargetObjectType()
        {
            return typeof (KxAddress);
        }
    }
}
