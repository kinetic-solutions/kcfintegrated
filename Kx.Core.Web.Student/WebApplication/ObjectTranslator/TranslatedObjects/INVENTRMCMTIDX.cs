﻿using Kx.Core.Web.PageRenderer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Objects.Student.Inventory;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class INVENTRMCMTIDX : StudentObjectTranslatorSource
    {
        public INVENTRMCMTIDX(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "INVENTRMCMTIDX", "Inventory room comment", "GI;GX")
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return WebApp.InventoryManager.InventoryRoomComment(attributes.GenericIndex,attributes.GenericIndex2);
        }

        public override Type GetTargetObjectType()
        {
            return typeof (OccupantInventoryComment);
        }
    }
}
