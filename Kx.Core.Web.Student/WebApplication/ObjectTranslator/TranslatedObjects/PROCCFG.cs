﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Core.Web.ObjectTranslator;
using Kx.Core.Web.PageRenderer;
using Kx.Core.Web.Student.WebApplication.ProcessManagers;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class PROCCFG : StudentObjectTranslatorSource
    {
        public PROCCFG(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(null, pageRenderer, "PROCCFG", "Process Configuration", string.Empty)
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return ((KxWebApplicationStudent) PageRenderer.WebApp).ProcessConfiguration;
        }

        public override Type GetTargetObjectType()
        {
            return typeof(KxProcessConfiguration);
        }
    }
}
