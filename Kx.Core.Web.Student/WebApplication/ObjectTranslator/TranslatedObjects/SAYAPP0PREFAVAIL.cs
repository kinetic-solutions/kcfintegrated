﻿using Kx.Core.Web.PageRenderer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Objects.Student.Web;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public abstract class SAYAPP0PREFAVAIL : StudentObjectTranslatorSource
    {
        public SAYAPP0PREFAVAIL(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer, string kxattribute, string friendlyname)
            : base(objectTranslator, pageRenderer, kxattribute, friendlyname, string.Empty)
        {
            
        }

        public override Type GetTargetObjectType()
        {
            return typeof (KxPreferenceAvailability);
        }
    }
}
