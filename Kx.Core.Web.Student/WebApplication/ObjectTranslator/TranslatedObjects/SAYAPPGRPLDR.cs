﻿using Kx.Core.Web.PageRenderer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Objects.Student.Web;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class SAYAPPGRPLDR : StudentObjectTranslatorSource
    {
        public SAYAPPGRPLDR(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "SAYAPPGRPLDR", "Friendship group leader", string.Empty)
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return WebUser.CurrentApplication.FriendshipGroup.Leader;
        }

        public override Type GetTargetObjectType()
        {
            return typeof (KxWebUserStudent);
        }
    }
}
