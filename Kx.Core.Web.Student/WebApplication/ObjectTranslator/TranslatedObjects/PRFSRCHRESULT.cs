﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Core.Web.PageRenderer;
using Kx.Objects.Student.Web;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class PRFSRCHRESULT : StudentObjectTranslatorSource
    {
        public PRFSRCHRESULT(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "PRFSRCHRESULT", "Preference search current item", string.Empty)
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return WebApp.PreferenceSearchManager.CurrentSearchItem;
        }

        public override Type GetTargetObjectType()
        {
            return typeof(KxWebStudentPreference);
        }
    }
}
