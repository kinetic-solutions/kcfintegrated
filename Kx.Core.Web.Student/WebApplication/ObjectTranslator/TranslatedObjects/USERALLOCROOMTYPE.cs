﻿using Kx.Core.Web.PageRenderer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Objects.Common;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class USERALLOCROOMTYPE : StudentObjectTranslatorSource
    {
        public USERALLOCROOMTYPE(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "USERALLOCROOMTYPE", "Specified allocation room type", string.Empty)
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return WebUser.SpecifiedAllocation.StudentAllocation.ResidentialRoom.BedroomType;
        }

        public override Type GetTargetObjectType()
        {
            return typeof (KxBedroomType);
        }
    }
}
