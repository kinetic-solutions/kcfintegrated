﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Core.Web.PageRenderer;
using Kx.Objects.Student;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class USERWEBLASTREASON : StudentObjectTranslatorSource
    {
        public USERWEBLASTREASON(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "USERWEBLASTREASON", "Web User Last Allocation Reason", string.Empty)
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return WebUser.LastAllocation.Reason;
        }

        public override Type GetTargetObjectType()
        {
            return typeof(KxStudentReason);
        }
    }
}
