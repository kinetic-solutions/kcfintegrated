﻿using Kx.Core.Web.PageRenderer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Objects.Student.Inventory;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class INVENTITEMCMTIDX : StudentObjectTranslatorSource
    {
        public INVENTITEMCMTIDX(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "INVENTITEMCMTIDX", "Inventory room item comment", "GI;GX;GY")
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return WebApp.InventoryManager.InventoryItemComment(attributes.GenericIndex,attributes.GenericIndex2, attributes.GenericIndex3);
        }

        public override Type GetTargetObjectType()
        {
            return typeof (OccupantInventoryComment);
        }
    }
}
