﻿using Kx.Core.Web.PageRenderer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Objects.Common;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class USERINHOUSESITE : StudentObjectTranslatorSource
    {
        public USERINHOUSESITE(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "USERINHOUSESITE", "Inhouse allocation site (block)", string.Empty)
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return WebUser.InHouseAllocation.StudentAllocation.ResidentialRoom.Block;
        }

        public override Type GetTargetObjectType()
        {
            return typeof (KxSiteBlock);
        }
    }
}
