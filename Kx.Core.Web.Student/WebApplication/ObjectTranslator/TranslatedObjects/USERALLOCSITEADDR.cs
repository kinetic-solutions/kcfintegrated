﻿using Kx.Core.Web.PageRenderer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Objects.Common;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class USERALLOCSITEADDR : StudentObjectTranslatorSource
    {
        public USERALLOCSITEADDR(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "USERALLOCSITEADDR", "Specified allocation site (block) address", string.Empty)
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return WebUser.SpecifiedAllocation.StudentAllocation.ResidentialRoom.Block.Address;
        }

        public override Type GetTargetObjectType()
        {
            return typeof (KxAddress);
        }
    }
}
