﻿using Kx.Core.Web.PageRenderer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Objects.Common;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class USERACCROOMTYPE : StudentObjectTranslatorSource
    {
        public USERACCROOMTYPE(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "USERACCROOMTYPE", "Accepted allocation room type", string.Empty)
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return WebUser.AcceptedAllocation.StudentAllocation.ResidentialRoom.BedroomType;
        }

        public override Type GetTargetObjectType()
        {
            return typeof (KxBedroomType);
        }
    }
}
