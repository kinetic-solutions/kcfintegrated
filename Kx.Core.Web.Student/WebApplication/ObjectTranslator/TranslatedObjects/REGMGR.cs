﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Core.Web.PageRenderer;
using Kx.Core.Web.Student.WebApplication.ProcessManagers;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class REGMGR : StudentObjectTranslatorSource
    {
        public REGMGR(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "REGMGR", "Registration Manager", string.Empty)
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return WebApp.RegistrationManager;
        }

        public override Type GetTargetObjectType()
        {
            return typeof(KxRegistrationManager);
        }
    }
}
