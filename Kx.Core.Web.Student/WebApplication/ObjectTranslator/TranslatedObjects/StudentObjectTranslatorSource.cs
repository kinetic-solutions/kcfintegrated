﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Core.Web.ObjectTranslator;
using Kx.Core.Web.PageRenderer;
using Kx.Objects.Student;
using Kx.Objects.Student.Web;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public abstract class StudentObjectTranslatorSource : ObjectTranslatorSource
    {
        public KxWebUserStudent WebUser
        {
            get
            { return (KxWebUserStudent)PageRenderer.WorkflowInterface.WebUser; }
        }

        public KxStudent Student
        {
            get
            { return (KxStudent)WebUser.Student; }
        }


        public KxWebApplicationStudent WebApp
        {
            get
            { return (KxWebApplicationStudent)PageRenderer.WebApp; }
        }

        protected KxStudentObjectTranslator ObjectTranslator { get; private set; }


        public StudentObjectTranslatorSource(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer, string kxAttribute, string friendlyName, string modifier)
            : base(pageRenderer, kxAttribute, friendlyName, modifier)
        {
            this.ObjectTranslator = objectTranslator;
        }
    }
}
