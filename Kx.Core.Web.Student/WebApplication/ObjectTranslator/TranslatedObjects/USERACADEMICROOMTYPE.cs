﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Core.Web.PageRenderer;
using Kx.Objects.Common;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class USERACADEMICROOMTYPE : StudentObjectTranslatorSource
    {
        public USERACADEMICROOMTYPE(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "USERACADEMICROOMTYPE", "Web User Academic Year Bedroom Type", string.Empty)
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return WebUser.LastAllocation.StudentAllocation.AcademicYearResidentialRoom.BedroomType;
        }

        public override Type GetTargetObjectType()
        {
            return typeof(KxBedroomType);
        }
    }
}
