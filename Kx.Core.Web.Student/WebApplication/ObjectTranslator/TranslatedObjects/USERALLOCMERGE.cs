﻿using Kx.Core.Merge;
using Kx.Core.Web.PageRenderer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class USERALLOCMERGE : StudentObjectTranslatorSource
    {
        public USERALLOCMERGE(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "USERALLOCMERGE", "Specified allocation merge tags", string.Empty)
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            WebUser.SpecifiedAllocation.MergeTags.CurrentMergeTag = attributes.MergeTag;
            WebUser.SpecifiedAllocation.MergeTags.CurrentMergeTagIndex = attributes.MergeTagIndex;
            return WebUser.SpecifiedAllocation.MergeTags;
        }

        public override Type GetTargetObjectType()
        {
            return typeof (KxMergeTagProvider);
        }
    }
}
