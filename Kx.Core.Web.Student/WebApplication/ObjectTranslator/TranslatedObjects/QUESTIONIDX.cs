﻿using Kx.Core.Web.PageRenderer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Objects.Student;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class QUESTIONIDX : StudentObjectTranslatorSource
    {
        public QUESTIONIDX(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "QUESTIONIDX", "Induction question by ID", "GI")
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return WebApp.ApplicationsManager.GetQuestionById(attributes.GenericIndex);
        }

        public override Type GetTargetObjectType()
        {
            return typeof (KxeInductionQuestion);
        }
    }
}
