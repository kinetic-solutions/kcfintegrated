﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Core.Web.Student.WebApplication.ProcessManagers;
using Kx.Objects.Student;
using Kx.Core.Web.PageRenderer;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class USERACCINVOICE : StudentObjectTranslatorSource
    {
        public USERACCINVOICE(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "USERACCINVOICE", "Student Allocation Invoice", string.Empty)
        {
            
        }

        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return WebUser.AcceptedAllocation.StudentAllocation.Invoice;
        }

        public override Type GetTargetObjectType()
        {
            return typeof (KxStudentInvoice);
        }
    }
}
