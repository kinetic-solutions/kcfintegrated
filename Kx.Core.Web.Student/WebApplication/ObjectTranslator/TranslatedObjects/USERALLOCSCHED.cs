﻿using Kx.Core.Web.PageRenderer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Objects.Student.Web;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class USERALLOCSCHED : StudentObjectTranslatorSource
    {
        public USERALLOCSCHED(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "USERALLOCSCHED", "Specified allocation schedule", string.Empty)
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return WebUser.SpecifiedAllocation.Schedule;
        }

        public override Type GetTargetObjectType()
        {
            return typeof (KxWebStudentAllocationSchedule);
        }
    }
}
