﻿using Kx.Core.Web.PageRenderer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Objects.Student.Web;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class USERPNDSCHEDSEM : StudentObjectTranslatorSource
    {
        public USERPNDSCHEDSEM(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "USERPNDSCHEDSEM", "Pending allocation schedule semster payment", string.Empty)
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return WebUser.PendingAllocation.Schedule.GetSemesterPayment(this.ObjectTranslator.PaymentNo);
        }

        public override Type GetTargetObjectType()
        {
            return typeof (KxWebPaymentSchedule);
        }
    }
}
