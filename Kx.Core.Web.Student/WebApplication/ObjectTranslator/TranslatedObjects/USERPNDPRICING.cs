﻿using Kx.Core.Web.PageRenderer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Objects.Student;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class USERPNDPRICING : StudentObjectTranslatorSource
    {
        public USERPNDPRICING(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "USERPNDPRICING", "Pending allocation pricing model", string.Empty)
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return WebUser.PendingAllocation.StudentAllocation.StandardRoomPricing;
        }

        public override Type GetTargetObjectType()
        {
            return typeof (KxRoomPricing);
        }
    }
}
