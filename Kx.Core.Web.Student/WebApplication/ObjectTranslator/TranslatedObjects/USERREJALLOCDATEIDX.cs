﻿using Kx.Core.Web.PageRenderer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Objects.Student;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class USERREJALLOCDATEIDX : StudentObjectTranslatorSource
    {
        public USERREJALLOCDATEIDX(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "USERREJALLOCDATEIDX", "Rejected allocation model date (by index GI)", string.Empty)
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return WebUser.RejectedAllocation.StudentAllocation.AllocationModel.GetAllocationModelDateById(attributes.GenericIndex);
        }

        public override Type GetTargetObjectType()
        {
            return typeof (KxAllocationModelDate);
        }
    }
}
