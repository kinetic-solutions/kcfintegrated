﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Core.Web.PageRenderer;
using Kx.Core.Web.PageRenderer.Utility;
using Kx.Objects.Student.Web;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class SAYAPP : StudentObjectTranslatorSource
    {
        public SAYAPP(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "SAYAPP", "Current Application", string.Empty)
        {
            
        }

        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            if (attributes.DocumentName != "" && !ObjectTranslator.GettingDocumentName)
            {
                string documentName = ObjectTranslator.GetSuffixedDocumentName(attributes);

                if (WebUser.KxStudentId != 0)
                {
                    WebUser.CurrentApplication.GetStudentDocument(WebUser.KxStudentId, documentName);
                }
                else
                {
                    WebUser.CurrentApplication.GetStudentDocumentByAppGuid(WebUser.CurrentApplication.ApplicationGuid, documentName);
                }
            }
            return WebUser.CurrentApplication;
        }

        public override Type GetTargetObjectType()
        {
            return typeof(KxWebStudentApplication);
        }

    }
}
