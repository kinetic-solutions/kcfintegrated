﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Core.Resources;
using Kx.Core.Web.ObjectTranslator;
using Kx.Core.Web.PageRenderer;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    [Depricated]
    public class PAGERESOURCE : StudentObjectTranslatorSource
    {
        public PAGERESOURCE(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "PAGERESOURCE", "Page Resource", "RN")
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return
                PageRenderer.ResourceManager.ResourceProvider.PageDataManager.GetWebUserPageData(PageRenderer.PageName,
                    attributes.ResourceName);
        }

        public override Type GetTargetObjectType()
        {
            return typeof (KxWebUserPageData);
        }
    }
}
