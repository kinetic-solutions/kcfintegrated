﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Core.Merge;
using Kx.Core.Web.PageRenderer;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class USERLASTMERGE : StudentObjectTranslatorSource
    {
        public USERLASTMERGE(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "USERLASTMERGE", "Web User Last Allocation Merge Tag", "MT")
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            WebUser.LastAllocation.MergeTags.CurrentMergeTag = attributes.MergeTag;
            WebUser.LastAllocation.MergeTags.CurrentMergeTagIndex = attributes.MergeTagIndex;
            return WebUser.LastAllocation.MergeTags;
        }

        public override Type GetTargetObjectType()
        {
            return typeof(KxMergeTagProvider);
        }
    }
}
