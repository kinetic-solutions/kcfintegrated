﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Core.Web.PageRenderer;
using Kx.Core.Web.Student.WebApplication.ProcessManagers;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class APPMGR : StudentObjectTranslatorSource
    {
        public APPMGR(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "APPMGR", "Applications manager", string.Empty)
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            if (attributes.DistinctFields != "")
            {
                WebApp.BedroomTypeCharges.DistinctFields = attributes.DistinctFields;
            }
            return WebApp.ApplicationsManager;
        }

        public override Type GetTargetObjectType()
        {
            return typeof(KxApplicationsManager);
        }
    }
}
