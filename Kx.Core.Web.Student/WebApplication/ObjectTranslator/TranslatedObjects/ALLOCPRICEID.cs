﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Core.Web.PageRenderer;
using Kx.Objects.Student.Web;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class ALLOCPRICEID : StudentObjectTranslatorSource
    {
        public ALLOCPRICEID(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "ALLOCPRICEID", "Allocation Pricing Item", "AP")
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return
                WebApp.ApplicationsManager.CurrentAcademicYear.AllocationPricing.GetWebAllocationPricing(
                    attributes.AllocationPricing);
        }

        public override Type GetTargetObjectType()
        {
            return typeof(KxWebAllocationPricing);
        }
    }
}
