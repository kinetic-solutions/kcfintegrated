﻿using Kx.Core.Web.PageRenderer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Objects.Common;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class USERREJROOM : StudentObjectTranslatorSource
    {
        public USERREJROOM(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "USERREJROOM", "Rejected allocation room", string.Empty)
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return WebUser.RejectedAllocation.StudentAllocation.ResidentialRoom;
        }

        public override Type GetTargetObjectType()
        {
            return typeof (KxResidentialRoom);
        }
    }
}
