﻿using Kx.Core.Web.PageRenderer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Objects.Student.Web;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class USERALLOCRESOLVER : StudentObjectTranslatorSource
    {
        public USERALLOCRESOLVER(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "USERALLOCRESOLVER", "Specified allocation URL resolver", string.Empty)
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return WebApp.UrlResolver.GetWebUrlResolver(WebUser.SpecifiedAllocation.StudentAllocation);
        }

        public override Type GetTargetObjectType()
        {
            return typeof (KxWebUrlResolver);
        }
    }
}
