﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Core.Web.PageRenderer;
using Kx.Objects.Student;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class SAYAPPPAYTYPE : StudentObjectTranslatorSource
    {
        public SAYAPPPAYTYPE(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "SAYAPPPAYTYPE", "Current application Payment Schedule Type", string.Empty)
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return WebUser.CurrentApplication.PaymentScheduleType;
        }

        public override Type GetTargetObjectType()
        {
            return typeof(KxPaymentScheduleType);
        }
    }
}
