﻿using Kx.Core.Web.PageRenderer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Objects.Student;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    [Depricated]
    public class LIBROOMID : StudentObjectTranslatorSource
    {
        public LIBROOMID(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "LIBROOMID", "Liberty room ID", string.Empty)
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return WebUser.CurrentApplication.LibertyRoomRates.GetRoomByUniqueId(WebUser.CurrentLibertyRoomId);
        }

        public override Type GetTargetObjectType()
        {
            return typeof (KxWebStudentRoomLibertyRate);
        }
    }
}
