﻿using Kx.Core.Web.PageRenderer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Objects.Student;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class ARRIVALSLOTS : StudentObjectTranslatorSource
    {
        public ARRIVALSLOTS(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "ARRIVALSLOTS", "Induction arrival slots", string.Empty)
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return WebUser.AcceptedAllocation.StudentAllocation.InductionArrivalLocation.ArrivalSlots;
        }

        public override Type GetTargetObjectType()
        {
            return typeof (KxeInductionArrivalSlots);
        }
    }
}
