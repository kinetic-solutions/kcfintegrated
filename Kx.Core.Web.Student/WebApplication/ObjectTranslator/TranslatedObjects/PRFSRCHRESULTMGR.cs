﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Core.Web.PageRenderer;
using Kx.Objects.Student.Web;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class PRFSRCHRESULTMGR : StudentObjectTranslatorSource
    {
        public PRFSRCHRESULTMGR(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "PRFSRCHRESULTMGR", "Preference search manager results", string.Empty)
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return WebApp.PreferenceSearchManager.Results;
        }

        public override Type GetTargetObjectType()
        {
            return typeof(KxWebStudentPreferences);
        }
    }
}
