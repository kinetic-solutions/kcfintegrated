﻿using Kx.Core.Web.PageRenderer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Objects.Student;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects
{
    public class ALLOCARRIVALSLOTS : StudentObjectTranslatorSource
    {
        public ALLOCARRIVALSLOTS(KxStudentObjectTranslator objectTranslator, KxPageRenderer pageRenderer)
            : base(objectTranslator, pageRenderer, "ALLOCARRIVALSLOTS", "Induction arrival slots for Allocation arrival date", string.Empty)
        {
            
        }
        public override object GetTargetObject(Web.PageRenderer.Utility.KxWebAttribute attributes)
        {
            return WebUser.AcceptedAllocation.StudentAllocation.InductionArrivalLocation.AllocationArrivalSlots;
        }

        public override Type GetTargetObjectType()
        {
            return typeof (KxeInductionArrivalSlots);
        }
    }
}
