using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using System.Linq;
using Kx.Core.Exceptions;
using Kx.Core.Reflection;
using Kx.Core.Web.Configuration;
using Kx.Core.Web.Student.WebApplication.BaseClass;
using Kx.Core.Web.ObjectTranslator;
using Kx.Core.Web.PageRenderer;
using Kx.Core.Web.PageRenderer.Utility;
using Kx.Core.Web.Student.WebApplication.PageRenderer;
using Kx.Core.Web.Student.WebApplication.ProcessManagers;

using Kx.Objects.Student;
using Kx.Objects.Student.Web;
using Kx.Objects.Common;
using Kx.Objects.Common.Web;
using Kx.Core.Web.Student.WebApplication.ObjectTranslator.TranslatedObjects;

namespace Kx.Core.Web.Student.WebApplication.ObjectTranslator
{
    [Serializable]
    public class KxStudentObjectTranslator : KxObjectTranslator
    {
        #region Atrributes & Properties

        public int CurrentPrefBasketIndex
        {
            get
            {
                if (ControlPrefBasketIndex != 0)
                {
                    return ControlPrefBasketIndex;
                }
                else
                {
                    return UserPrefBasketIndex;
                }
            }
        }

        public int UserTermDate { get; set; }

        public int ControlTermDate { get; set; }

        public int UserPrefBasketIndex { get; set; }

        public int ControlPrefBasketIndex { get; set; }

        public int CurrentPrefSearchIndex
        {
            get
            {
                if (ControlPrefSearchIndex != 0)
                {
                    return ControlPrefSearchIndex;
                }
                else
                {
                    return UserPrefSearchIndex;
                }
            }
        }

        public int UserPrefSearchIndex { get; set; }

        public int ControlPrefSearchIndex { get; set; }
        public int LastPrefSearchIndex { get; set; }

        public int GenericControlIndex3 { get; set; }

        public int CurrentMemberId
        {
            get
            {
                if (ControlMemberId != 0)
                {
                    return ControlMemberId;
                }
                else
                {
                    return UserMemberId;
                }
            }
        }

        public int CurrentTermDate
        {
            get
            {
                if (ControlTermDate != 0)
                {
                    return ControlTermDate;
                }
                else
                {
                    return UserTermDate;
                }
            }
        }

        public int UserMemberId { get; set; }

        public int ControlMemberId { get; set; }

        protected internal int PaymentNo { get; set; }

        public KxWebUserStudent WebUser
        {
            get
            { return (KxWebUserStudent)pageRenderer.WorkflowInterface.WebUser; }
        }

        public KxStudent Student
        {
            get
            { return (KxStudent)WebUser.Student; }
        }


        public KxWebApplicationStudent WebApp
        {
            get
            { return (KxWebApplicationStudent)pageRenderer.WebApp; }
        }

        #endregion


        #region Constructors

        public KxStudentObjectTranslator(KxPageRenderer pageRenderer)
            : base(pageRenderer)
        {
            PaymentNo = 0;
        }

        #endregion


        public override void MappingOverride(KxWebAttribute attributes)
        {
            if (WebUser != null)
            {
                ControlPrefBasketIndex = attributes.PrefBasketIndex;
                ControlPrefSearchIndex = attributes.PrefSearchIndex;
                if (LastPrefSearchIndex != ControlPrefSearchIndex && ControlPrefSearchIndex > 0)
                {
                    LastPrefSearchIndex = ControlPrefSearchIndex;
                }

                WebUser.ControlAllocationDateId = attributes.AllocationDate;

                if (attributes.AcademicYear != 11111)
                {
                    //if (attributes.AcademicYear == 0 && attributes.Id != "KX_BC_RXS_RVCONTINUE" && attributes.Id != "KX_PCRESOURCE_RNHubBrowserMessage_PNData" && !attributes.Id.StartsWith("KX_PCUSER_VPKxStudentId_CW0") && attributes.Id != "KX_PCSTUDENT_PNStudentId" && attributes.Id != "KX_PCAPPMGR_PNYearsArrayDesc_PACLONE_TSAcademicYearA_TXAY_SCUSER_VPActive_CW1")
                    //    System.Diagnostics.Debugger.Break();
                    WebUser.ControlAcademicYear = attributes.AcademicYear;
                }
                else
                {
                    WebUser.ControlAcademicYear = WebApp.ApplicationsManager.GetAcademicYearForDate(DateTime.Today);
                }

                if (attributes.AcademicYearSession != 11111)
                {
                    WebUser.ControlAcademicYearSession = attributes.AcademicYearSession;
                }
                else
                {
                    WebUser.ControlAcademicYearSession = 0;
                }

                if (attributes.SpecificApplicationGuid != null && WebUser.UserApplicationGuid == null)
                {
                    WebUser.ControlApplicationGuid = attributes.SpecificApplicationGuid;
                }

                WebUser.ControlAllocationPricing = attributes.AllocationPricing;
                WebUser.ControlAdjustedDueDate = attributes.DueDate;
                WebUser.ControlSiteLookupId = attributes.SiteLookup;
                WebUser.ControlCityRoomId = attributes.LibertyRoom;
                WebUser.ControlLibertyRoomId = attributes.LibertyRoom;
                WebUser.ControlOpalRoomId = attributes.OpalRoom;
                WebUser.CurrentApplication.ControlUDF = attributes.UDF;
                WebUser.ControlWebResource = attributes.WebResource;

                ControlMemberId = attributes.GroupMember;
                ControlTermDate = attributes.StudentTermDate;

                PaymentNo = attributes.PaymentNumber;

                WebUser.CurrentApplication.ControlPreferenceIndex = attributes.Preference;
                WebUser.CurrentApplication.ControlPreferenceNumber = attributes.PrefNumber;

                WebApp.ControlBedroomTypeCharge = attributes.BedroomType;
            }
           
        }

        private Dictionary<string, ObjectTranslatorSource> _translatorObjects = null;
        private Dictionary<string, ObjectTranslatorSource> TranslatorObjects
        {
            get
            {
                if (_translatorObjects == null || _translatorObjects != null && _translatorObjects.Count == 0)
                {
                    LoadTranslatorObjects();
                }

                return _translatorObjects;
            }
        }

        private void LoadTranslatorObjects()
        {
            var types = from type in Assembly.GetExecutingAssembly().GetTypes().ToList()
                        where type.IsSubclassOf(typeof(ObjectTranslatorSource)) == true
                              && type.IsAbstract == false
                        select type;

            _translatorObjects = new Dictionary<string, ObjectTranslatorSource>(StringComparer.InvariantCultureIgnoreCase);
            foreach (Type type in types)
            {
                if (type.IsSubclassOf(typeof(StudentObjectTranslatorSource)))
                {
                    var source =
                        (ObjectTranslatorSource) Activator.CreateInstance(type, new object[] {this, PageRenderer});
                    _translatorObjects.Add(source.KxAttribute, source);
                }
                else
                {
                    var source = (ObjectTranslatorSource) Activator.CreateInstance(type, new object[] {PageRenderer});
                    _translatorObjects.Add(source.KxAttribute, source);
                }
            }
        }


        public override object ClassNameToObject(string ClassName, KxWebAttribute attributes)
        {
            object Result = null;
            try
            {
                if (TranslatorObjects.ContainsKey(ClassName))
                {
                    try
                    {
                        Result = TranslatorObjects[ClassName].GetTargetObject(attributes);
                    }
                    catch (NullReferenceException)
                    { 
                        // We don't mind these!
                    }
                }
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "ObjectMapping", ex.Message, ClassName.ToUpper());
                Result = null;
            }

            return Result;
        }

        private KxStudentRequest GetRequestFromAttributes(KxWebAttribute attributes)
        {
            KxStudentRequest result;
            if (attributes.RequestIdentifier > 0)
                result = WebUser.CurrentApplication.StudentRequests.Find(s => s.StudentRequestId == attributes.RequestIdentifier);
            else
                result =
                    WebUser.CurrentApplication.StudentRequests.Find(
                        s => s.StudentRequestId.ToString() == attributes.GenericStringIndex);
            return result;
        }

        internal bool GettingDocumentName { get; set; }

        public string GetSuffixedDocumentName(KxWebAttribute UserAction)
        {
            GettingDocumentName = true;
            string documentName = UserAction.DocumentName;
            try
            {
                if (!string.IsNullOrWhiteSpace(UserAction.DocumentSuffix))
                {
                    documentName += GetMergeDataObjectTertiary(UserAction, UserAction.DocumentSuffix) ?? GetMergeDataObjectSecondary(UserAction, UserAction.DocumentSuffix) ?? GetMergeDataObject(UserAction, UserAction.DocumentSuffix);
                }
            }
            finally
            {
                GettingDocumentName = false;
            }
            return documentName;
        }

        public new Type ObjectTypes(string Id)
        {
            return TranslatorObjects[Id].GetTargetObjectType();
        }

        

        public new string Objects()
        {
            StringBuilder sb = new StringBuilder();

            foreach (string key in TranslatorObjects.Keys)
            {
                sb.AppendLine(string.Format("{0} : {1} : {2}", TranslatorObjects[key].KxAttribute, TranslatorObjects[key].FriendlyName, TranslatorObjects[key].Modifier));
            }

            return sb.ToString();
        }

        public override KxUserDefinedValue GetUserDefinedValue(int comparisonId)
        {
            return WebUser.CurrentApplication.UserDefinedValues.GetValueById(comparisonId);
        }

        public new bool GetCustomMergeStateFlag(KxWebAttribute stdattributes, string StateProperty, bool defaultvalue,
            object compareObject)
        {
            bool result = false;
            KxWebAttribute attributes = (KxWebAttribute) stdattributes;

            if (attributes.CompareUDF != -99)
            {
                KxUserDefinedValue compareWithUDFValue =
                    WebUser.CurrentApplication.UserDefinedValues.GetValueById(attributes.CompareUDF);

                if (compareWithUDFValue != null)
                {
                    if (attributes.ComparisonWith != "")
                    {
                        result = KxWebAttributeComparison.DoComparison(attributes,
                            compareWithUDFValue.FieldValue.ToString(), attributes.ComparisonWith);
                    }
                    else
                    {
                        result = KxWebAttributeComparison.DoComparison(attributes,
                            compareWithUDFValue.FieldValue.ToString(), compareObject.ToString());
                    }
                }
            }
            else if (!string.IsNullOrWhiteSpace(attributes.CompareWithProperty))
            {
                ObjectMapping(attributes);

                object compareProperty = GetMergeDataObjectTertiary(attributes, attributes.CompareWithProperty) ??
                                         GetMergeDataObjectSecondary(attributes, attributes.CompareWithProperty) ??
                                         GetMergeDataObject(attributes, attributes.CompareWithProperty);

                if (compareProperty != null)
                {
                    result = KxWebAttributeComparison.DoComparison(attributes, compareProperty.ToString(), compareObject.ToString());
                }
            }
            return result;
        }

    }
}
