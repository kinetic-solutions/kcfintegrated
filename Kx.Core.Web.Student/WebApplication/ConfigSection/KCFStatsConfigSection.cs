﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace Kx.Core.Web.Student.WebApplication.ConfigSection
{
    public class KCFStatsConfigSection : ConfigurationSection
    {
        [ConfigurationProperty("Applications", DefaultValue = "kcfstats.Applications_Standard")]
        public string Applications
        {
            get
            {
                return (string)this["Applications"];
            }
            set
            {
                this["Applications"] = value;
            }
        }

        public string GetSectionStatsKey(string section)
        {
            return (string) this[section];
        }

        [ConfigurationProperty("ConnectionStringName", DefaultValue = "Kx")]
        public string ConnectionStringName
        {
            get
            {
                return (string)this["ConnectionStringName"];
            }
            set
            {
                this["ConnectionStringName"] = value;
            }
        }

        [ConfigurationProperty("PrivateKey", IsRequired = true)]
        public string PrivateKey
        {
            get
            {
                return (string)this["PrivateKey"];
            }
            set
            {
                this["PrivateKey"] = value;
            }
        }
    }
}
