﻿using System;
using System.Web;
using System.Web.SessionState;
using Kx.Core.PaymentProcessor.Cache;
using Newtonsoft.Json;

namespace Kx.Core.Web.Student.WebApplication.Handlers
{
    public class PayToStudyHandler : IHttpHandler, IReadOnlySessionState
    {
        /// <summary>
        /// You will need to configure this handler in the Web.config file of your 
        /// web and register it with IIS before being able to use it. For more information
        /// see the following link: http://go.microsoft.com/?linkid=8101007
        /// </summary>
        #region IHttpHandler Members

        public bool IsReusable
        {
            // Return false in case your Managed Handler cannot be reused for another request.
            // Usually this would be false in case you have some state information preserved per request.
            get { return false; }
        }

        public void ProcessRequest(HttpContext context)
        {
            context.Response.Cache.SetCacheability(HttpCacheability.NoCache);
            var webApp = GetWebApp(context);

            if (webApp != null && webApp.PayToStudyManager.FXConversionAvailable)
            {
                context.Response.Clear();
                context.Response.ContentType = "application/json";
                context.Response.Write(JsonConvert.SerializeObject(webApp.PayToStudyManager.FXConversionResponse));
            }
            else
            {
                context.Response.Clear();
                context.Response.ContentType = "application/text";
                context.Response.Write(string.Empty);
            }
        }

        #endregion

        private KxWebApplicationStudent GetWebApp(HttpContext context)
        {
            return ((KxWebApplicationStudent)context.Session["WebMgr"]);
        }
    }
}
