﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using Kx.Core.Encryption;
using Kx.Core.Exceptions;
using Kx.Core.Web.Student.WebApplication.ConfigSection;
using Newtonsoft.Json;

namespace Kx.Core.Web.Student.WebApplication.Handlers
{
    public class StatsHandler : IHttpHandler
    {
        public bool IsReusable
        {
            get { return false; }
        }

        public void ProcessRequest(HttpContext context)
        {
            try
            {
                KCFStatsConfigSection config = (KCFStatsConfigSection)System.Configuration.ConfigurationManager.GetSection("kcfStats/kcfstatsConfig");
                string statRequested = context.Request["stat-item"];
                if (!string.IsNullOrWhiteSpace(statRequested))
                {
                    if (statRequested == "KCF.Profile")
                    {
                        
                    }
                    else if (!string.IsNullOrWhiteSpace(config.GetSectionStatsKey(statRequested)))
                    {
                        SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings[config.ConnectionStringName].ConnectionString);
                        SqlCommand command = new SqlCommand(config.GetSectionStatsKey(statRequested), connection);
                        command.CommandType = CommandType.StoredProcedure;
                        DataTable table = new DataTable();
                        SqlDataAdapter adapter = new SqlDataAdapter(command);
                        try
                        {
                            connection.Open();
                            adapter.Fill(table);

                        }
                        finally
                        {
                            connection.Close();
                        }

                        string result = JsonConvert.SerializeObject(table);
                        result = Rijndael.EncryptString(result,config.PrivateKey);

                        context.Response.Clear();
                        context.Response.ContentType = "application/json";
                        context.Response.Write(result);
                    }
                }
            }
            catch (Exception e)
            {
                KxLogger.LogActivity(this, "ProcessRequest.Stats", e.Message, "");
                context.Response.Clear();
                context.Response.ContentType = "application/text";
                context.Response.Write("Error: " + e.Message);
            }
        }
    }
}
