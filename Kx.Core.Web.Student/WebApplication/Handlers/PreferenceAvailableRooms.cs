﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Web;
using System.Xml;
using Kx.Core.Data;
using Kx.Core.Exceptions;
using Kx.Core.Utility;
using Kx.Core.Web.Student.WebApplication.ProcessManagers;
using Kx.Objects.Student.Web;
using Newtonsoft.Json;
using Newtonsoft.Json.Bson;
using Formatting = Newtonsoft.Json.Formatting;

namespace Kx.Core.Web.Student.WebApplication.Handlers
{
    public class PreferenceHandler : IHttpHandler
    {
        /// <summary>
        /// You will need to configure this handler in the web.config file of your 
        /// web and register it with IIS before being able to use it. For more information
        /// see the following link: http://go.microsoft.com/?linkid=8101007
        /// </summary>
        #region IHttpHandler Members

        public bool IsReusable
        {
            // Return false in case your Managed Handler cannot be reused for another request.
            // Usually this would be false in case you have some state information preserved per request.
            get { return false; }
        }

        /// <summary>
        /// Enables processing of HTTP Web requests by a custom HttpHandler that implements the <see cref="T:System.Web.IHttpHandler"/> interface.
        /// </summary>
        /// <param name="context">An <see cref="T:System.Web.HttpContext"/> object that provides references to the intrinsic server objects (for example, Request, Response, Session, and Server) used to service HTTP requests. </param>
        public void ProcessRequest(HttpContext context)
        {
            try
            {
                string operation = context.Request["op"];

                context.Response.Clear();
                context.Response.ContentType = "application/json";
                context.Response.Cache.SetCacheability(HttpCacheability.NoCache);

                object result = new {message = "Operation not identfied"};

                switch (operation)
                {
                    case "availablerooms":
                        result = AvailableRooms(context);
                        break;
                    case "friendfinder":
                        result = FriendFinder(context);
                        break;
                    case "friendprofile":
                        result = FriendProfile(context);
                        break;
                    case "availablefriendrooms":
                        result = AvailableFriendRooms(context);
                        break;
                }

                string responseText =
                    JsonConvert.SerializeObject(
                        result,
                        Formatting.Indented);
                KxLogger.LogActivity(this, "ProcessRequest", "JSON", responseText);
                context.Response.Write(responseText);
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "ProcessRequest", "Error", ex.Message);
            }
        }

        private object AvailableFriendRooms(HttpContext context)
        {
            string residentialroomid = context.Request["rr"];
            int residentialRoomId = 0;
            string academicyearid = context.Request["ay"];
            int academicYearId = 0;
            string kxstudentid = context.Request["kx"];
            int KxStudentID = 0;
            if (int.TryParse(residentialroomid, out residentialRoomId) && int.TryParse(academicyearid, out academicYearId) && int.TryParse(kxstudentid, out KxStudentID))
            {
                KxData data = new KxData("room", "GetAvailableRoomsForFriend", TargetType.StoredProc, ActionType.ExecuteProcedure);
                data.AddParameter("residentialroomid", residentialRoomId, KxValueType.Int);
                data.AddParameter("academicyearid", academicYearId, KxValueType.Int);
                data.AddParameter("kxstudentid", KxStudentID, KxValueType.Int);
                if (data.Execute())
                {
                    XmlDocument doc = new XmlDocument();
                    doc.LoadXml(data.XmlResults);
                    XmlNodeList nodes = doc.SelectNodes(@"//room/datarow");
                    List<AvailableRoom> rooms = new List<AvailableRoom>();
                    foreach (XmlNode node in nodes)
                    {
                        rooms.Add(new AvailableRoom()
                        {
                            ResidentialRoomId = KxXmlUtils.GetNodeValue(node, "residentialroomid", 0),
                            RoomName = KxXmlUtils.GetNodeValue(node, "name", string.Empty),
                            FloorNumber = KxXmlUtils.GetNodeValue(node, "floornumber", 0),
                            SubBlockId = KxXmlUtils.GetNodeValue(node, "subblockid", 0),
                            FlatName = KxXmlUtils.GetNodeValue(node, "flatname", string.Empty),
                            OccupantKxStudentId = KxXmlUtils.GetNodeValue(node, "occupantkxstudentid", 0),
                            ProfileMatch = KxXmlUtils.GetNodeValue(node, "profilematch", 0),
                            ProfileName = KxXmlUtils.GetNodeValue(node, "profilename", string.Empty),
                            BriefDescription = KxXmlUtils.GetNodeValue(node, "briefdescription", string.Empty),
                            Gender = KxXmlUtils.GetNodeValue(node, "gender", "U")
                        });
                    }

                    return new { rooms = rooms.ToArray() };
                }
            }

            return new { message = "Available Friend Rooms : Error converting string to number" };
        }

        private object FriendProfile(HttpContext context)
        {
            string kxstudentid = context.Request["kx"];
            int KxStudentID = 0;
            if (int.TryParse(kxstudentid, out KxStudentID))
            {
                KxWebSocialProfileTemplate dbTemplate = KxWebSocialProfileTemplate.GetWebSocialProfileTemplate();
                KxWebStudentSocialProfile dbProfile = KxWebStudentSocialProfile.GetWebStudentSocialProfile(KxStudentID, dbTemplate);
                return new 
                {
                    ScreenName = dbProfile.ProfileIsPublic ? dbProfile.ScreenName : "Withheld",
                    BriefDescription = dbProfile.ProfileIsPublic ? dbProfile.BriefDescription: "Withheld",
                    Field1Name = dbTemplate.Field1Name,
                    Field2Name = dbTemplate.Field2Name,
                    Field3Name = dbTemplate.Field3Name,
                    Field4Name = dbTemplate.Field4Name,
                    Field5Name = dbTemplate.Field5Name,
                    Field1Value = dbProfile.ProfileIsPublic ? dbProfile.Field1Value : "Withheld",
                    Field2Value = dbProfile.ProfileIsPublic ? dbProfile.Field2Value : "Withheld",
                    Field3Value = dbProfile.ProfileIsPublic ? dbProfile.Field3Value : "Withheld",
                    Field4Value = dbProfile.ProfileIsPublic ? dbProfile.Field4Value : "Withheld",
                    Field5Value = dbProfile.ProfileIsPublic ? dbProfile.Field5Value : "Withheld",
                };
            }
            return new { message = "Friend Profile : Error" };
        }

        private object FriendFinder(HttpContext context)
        {
            string kxstudentid = context.Request["kx"];
            string academicyear = context.Request["ay"];
            string name = context.Request["q"];

            int KxStudentID = 0;
            int AcademicYearID = 0;

            if (int.TryParse(kxstudentid, out KxStudentID) && int.TryParse(academicyear, out AcademicYearID))
            {
                KxData data = new KxData("room", "WebFindAFriend", TargetType.StoredProc, ActionType.ExecuteProcedure);
                data.AddParameter("kxstudentid", KxStudentID, KxValueType.Int);
                data.AddParameter("academicyearid", AcademicYearID, KxValueType.Int);
                data.AddParameter("name", name ?? "", KxValueType.String, 50);
                if (data.Execute())
                {
                    XmlDocument doc = new XmlDocument();
                    doc.LoadXml(data.XmlResults);
                    XmlNodeList nodes = doc.SelectNodes(@"//room/datarow");
                    List<Friend> friends = new List<Friend>();

                    foreach (XmlNode node in nodes)
                    {
                        friends.Add(new Friend()
                        {
                            ProfileMatch = KxXmlUtils.GetNodeValue(node, "profilematch", 0),
                            AreaID = KxXmlUtils.GetNodeValue(node, "areaid", 0),
                            Area = KxXmlUtils.GetNodeValue(node, "area", string.Empty),
                            BlockID = KxXmlUtils.GetNodeValue(node, "blockid", 0),
                            Block = KxXmlUtils.GetNodeValue(node, "blockname", string.Empty),
                            SubBlockID = KxXmlUtils.GetNodeValue(node, "subblockid", 0),
                            SubBlock = KxXmlUtils.GetNodeValue(node, "subblockname", string.Empty),
                            ResidentialRoomID = KxXmlUtils.GetNodeValue(node, "residentialroomid", 0),
                            RoomName = KxXmlUtils.GetNodeValue(node, "roomname", string.Empty),
                            ScreenName = KxXmlUtils.GetNodeValue(node, "screenname", "Withheld"),
                            BriefDescription = KxXmlUtils.GetNodeValue(node, "briefdescription", "Withheld"),
                            Gender = KxXmlUtils.GetNodeValue(node, "gender", "U"),
                            KxStudentID = KxXmlUtils.GetNodeValue(node, "kxstudentid", 0)
                        });
                    }
                    return new {friends = friends.ToArray()};
                }
            }
            return new { message = "Friend Finder : Error" };
        }

        private object AvailableRooms(HttpContext context)
        {
            string preferenceNumber = context.Request["prefnum"];
            string kxstudentid = context.Request["kx"];
            int preferenceId = 0;
            int kxStudentId = 0;
            if (int.TryParse(preferenceNumber, out preferenceId) && int.TryParse(kxstudentid, out kxStudentId))
            {
                KxData data = new KxData("room", "GetAvailableRoomsForPreferenceAvailability", TargetType.StoredProc, ActionType.ExecuteProcedure);
                data.AddParameter("preferenceavailabilityid", preferenceId, KxValueType.Int);
                data.AddParameter("kxstudentid", kxStudentId, KxValueType.Int);
                if (data.Execute())
                {
                    XmlDocument doc = new XmlDocument();
                    doc.LoadXml(data.XmlResults);
                    XmlNodeList nodes = doc.SelectNodes(@"//room/datarow");
                    List<AvailableFlat> flats = new List<AvailableFlat>();
                    int SubBlockID = 0;
                    AvailableFlat flat = null;
                    List<AvailableRoom> rooms = new List<AvailableRoom>();
                    foreach (XmlNode node in nodes)
                    {
                        if (SubBlockID != KxXmlUtils.GetNodeValue(node, "subblockid", 0))
                        {
                            if (flat != null)
                            {
                                flat.Rooms = rooms.ToArray();
                                flats.Add(flat);
                            }
                            flat = new AvailableFlat()
                            {
                                FlatName = KxXmlUtils.GetNodeValue(node, "flatname", string.Empty)
                            };

                            rooms = new List<AvailableRoom>();
                            SubBlockID = KxXmlUtils.GetNodeValue(node, "subblockid", 0);

                        }
                        rooms.Add(new AvailableRoom()
                        {
                            ResidentialRoomId = KxXmlUtils.GetNodeValue(node, "residentialroomid", 0),
                            RoomName = KxXmlUtils.GetNodeValue(node, "name", string.Empty),
                            FloorNumber = KxXmlUtils.GetNodeValue(node, "floornumber", 0),
                            SubBlockId = KxXmlUtils.GetNodeValue(node, "subblockid", 0),
                            FlatName = KxXmlUtils.GetNodeValue(node, "flatname", string.Empty),
                            OccupantKxStudentId = KxXmlUtils.GetNodeValue(node, "occupantkxstudentid", 0),
                            ProfileMatch = KxXmlUtils.GetNodeValue(node, "profilematch", 0),
                            ProfileName = KxXmlUtils.GetNodeValue(node, "profilename", string.Empty),
                            BriefDescription = KxXmlUtils.GetNodeValue(node, "briefdescription", string.Empty),
                            Gender = KxXmlUtils.GetNodeValue(node, "gender", "U")
                        });
                    }
                    if (flat != null)
                    {
                        flat.Rooms = rooms.ToArray();
                        flats.Add(flat);
                    }

                    return new {flats = flats.ToArray()};
                }
            }

            return new {message = "Available Rooms : Error converting string to number"};
        }

        #endregion
    }

    public class AvailableFlat
    {
        public string FlatName { get; set; }
        public AvailableRoom[] Rooms { get; set; }
    }

    public class AvailableRoom
    {
        public int ResidentialRoomId { get; set; }
        public string RoomName { get; set; }
        public int FloorNumber { get; set; }
        public int SubBlockId { get; set; }
        public string FlatName { get; set; }
        public int OccupantKxStudentId { get; set; }
        public int ProfileMatch { get; set; }
        public string ProfileName { get; set; }
        public string BriefDescription { get; set; }
        public string Gender { get; set; }
    }

    public class Friend
    {
        public int ProfileMatch { get; set; }
        public int AreaID { get; set; }
        public string Area { get; set; }
        public int BlockID { get; set; }
        public string Block { get; set; }
        public int SubBlockID { get; set; }
        public string SubBlock { get; set; }
        public int ResidentialRoomID { get; set; }
        public string RoomName { get; set; }
        public string ScreenName { get; set; }
        public string BriefDescription { get; set; }
        public string Gender { get; set; }
        public int KxStudentID { get; set; }
    }
}
