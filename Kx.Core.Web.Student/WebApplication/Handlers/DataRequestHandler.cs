﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Web;
using System.Xml;
using Kx.Core.Data;
using Kx.Core.Exceptions;
using Newtonsoft.Json;
using Kx.Core.Utility;

namespace Kx.Core.Web.Student.WebApplication.Handlers
{
    public class DataRequestHandler : IHttpHandler
    {
        public bool IsReusable
        {
            get { return false; }
        }

        public void ProcessRequest(HttpContext context)
        {
            context.Response.Cache.SetCacheability(HttpCacheability.NoCache);
            
            if (context.Application["ResourceProvider"] == null)
                context.Application["ResourceProvider"] = new Kx.Core.Web.Student.WebApplication.Resource.KxStudentResourceProvider();

            string listName = context.Request["sql"];
            string filter = context.Request["Filter"];
            string filter1 = context.Request["Filter1"];
            string filter2 = context.Request["Filter2"];
            string filter3 = context.Request["Filter3"];
            string filter4 = context.Request["Filter4"];
            string filter5 = context.Request["Filter5"];
            string filter6 = context.Request["Filter6"];
            string filter7 = context.Request["Filter7"];
            string filter8 = context.Request["Filter8"];
            string filter9 = context.Request["Filter9"];

            try
            {
                KxData data = new KxData("result", listName, TargetType.Dynamic, ActionType.Select);
                data.AddParameter("@Filter",  MakeSafe(CheckNull(filter)), KxValueType.Unknown);
                data.AddParameter("@Filter1", MakeSafe(CheckNull(filter1)), KxValueType.Unknown);
                data.AddParameter("@Filter2", MakeSafe(CheckNull(filter2)), KxValueType.Unknown);
                data.AddParameter("@Filter3", MakeSafe(CheckNull(filter3)), KxValueType.Unknown);
                data.AddParameter("@Filter4", MakeSafe(CheckNull(filter4)), KxValueType.Unknown);
                data.AddParameter("@Filter5", MakeSafe(CheckNull(filter5)), KxValueType.Unknown);
                data.AddParameter("@Filter6", MakeSafe(CheckNull(filter6)), KxValueType.Unknown);
                data.AddParameter("@Filter7", MakeSafe(CheckNull(filter7)), KxValueType.Unknown);
                data.AddParameter("@Filter8", MakeSafe(CheckNull(filter8)), KxValueType.Unknown);
                data.AddParameter("@Filter9", MakeSafe(CheckNull(filter9)), KxValueType.Unknown);

                if (System.Configuration.ConfigurationManager.AppSettings["DebugDataRequest"] == "True")
                    data.LogDebug("DataRequest", listName);
                if (data.Execute())
                {
                    var xmlDoc = new XmlDocument();
                    xmlDoc.LoadXml(data.XmlResults);

                    context.Response.Clear();
                    context.Response.ContentType = "application/json";
                    XmlNodeList nodes = xmlDoc.SelectNodes(@"//result/datarow");
                    DataTable table = new DataTable();

                    foreach (XmlNode node in nodes)
                    {
                        if (table.Columns.Count == 0)
                        {
                            ConstructTable(table, node);
                        }

                        DataRow row = table.NewRow();
                        foreach (XmlNode childNode in node.ChildNodes)
                        {
                            row[childNode.Name] = childNode.InnerText;
                            if (childNode.Name.ToLower().EndsWith("b64"))
                            {
                                row[childNode.Name] = KxBase64.Base64Decode(childNode.InnerText);
                            }
                        }
                        table.Rows.Add(row);
                    }
                    table.AcceptChanges();
                    context.Response.Write(JsonConvert.SerializeObject(table));
                }
                else
                {
                    KxLogger.LogActivity(this, "ProcessRequest.Data.Execute", data.LastError, "");
                    context.Response.Clear();
                    context.Response.ContentType = "application/text";
                    context.Response.Write("Error: " + data.LastError);
                }
            }
            catch (Exception e)
            {
                KxLogger.LogActivity(this, "ProcessRequest.Data.Execute", e.Message, "");
                context.Response.Clear();
                context.Response.ContentType = "application/text";
                context.Response.Write("Error: " + e.Message);
            }
        }

        private void ConstructTable(DataTable table, XmlNode node)
        {
            foreach (XmlNode childNode in node.ChildNodes)
            {
                table.Columns.Add(childNode.Name, typeof(string));
            }
        }

        private object CheckNull(object source)
        {
            object result = source;

            if ((result != null) && ((result is string && (string)result == "") || (result is int && (int)result == 0)))
            {
                result = null;
            }
            return result;
        }

        private object MakeSafe(object source)
        {
            if (source != null)
            {
                if (source.ToString().Contains("--") || source.ToString().Contains("''") ||
                    source.ToString().Contains(";"))
                {
                    source = string.Empty;
                }
            }

            return source;
        }
    }
}
