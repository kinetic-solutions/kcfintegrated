﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Security;
using System.Text;
using System.Web;
using Kx.Core.Exceptions;
using Kx.Core.Web.Student.WebApplication.ProcessManagers;
using Kx.Objects.Student.Inventory;
using Newtonsoft.Json;

namespace Kx.Core.Web.Student.WebApplication.Handlers
{
    public class InventoryHandler : IHttpHandler
    {
        public bool IsReusable
        {
            get { return false; }
        }

        public void ProcessRequest(HttpContext context)
        {
            context.Response.Cache.SetCacheability(HttpCacheability.NoCache);

            string allocationIdString = context.Request["allocationId"];
            string url = context.Request["url"];
            int allocationId = 0;
            string json = string.Empty;
            if (int.TryParse(allocationIdString, out allocationId))
            {
                url = string.Format("{0}?allocationId={1}", url, allocationId);
                try
                {
                    using (WebClient client = new WebClient())
                    {
                        SetupSSLTrust();
                        client.Headers[HttpRequestHeader.Accept] = "application/json";
                        json = client.DownloadString(url);
                    }
                }
                catch (Exception ex)
                {
                    KxLogger.LogActivity(this, "InventoryHandler.ProcessRequest", url, ex.Message);
                }
                context.Response.Clear();
                context.Response.ContentType = "application/json";
                context.Response.Write(json);
                return;
            }


            context.Response.Clear();
            context.Response.ContentType = "application/text";
            context.Response.Write("");
        }
        private void SetupSSLTrust()
        {
            ServicePointManager.ServerCertificateValidationCallback =
                new RemoteCertificateValidationCallback(
                    delegate { return true; }
                    );
        }

    }
}