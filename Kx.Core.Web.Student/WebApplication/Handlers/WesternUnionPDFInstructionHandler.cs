﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.SessionState;

namespace Kx.Core.Web.Student.WebApplication.Handlers
{
    public class WesternUnionPDFInstructionHandler : IHttpHandler, IReadOnlySessionState
    {
        public bool IsReusable
        {
            get { return false; }
        }

        public void ProcessRequest(HttpContext context)
        {
            context.Response.Cache.SetCacheability(HttpCacheability.NoCache);
            var webApp = GetWebApp(context);

            string cacheIdString = context.Request["c"];
            Guid cacheId = Guid.Empty;
            if (!string.IsNullOrWhiteSpace(cacheIdString))
            {
                cacheId = Guid.Parse(cacheIdString);
            }


            if (webApp != null)
            {
                var response = webApp.WesternUnionManager.GetPDFInstructionsByte(cacheId);

                if (response != null)
                {
                    context.Response.Clear();
                    context.Response.ContentType = "application/pdf";
                    if(string.IsNullOrWhiteSpace(context.Request["embedded"]))
                    {
                        context.Response.AddHeader("Content-Disposition", "attachment; filename=PaymentInstructions.pdf; size=" + response.Length.ToString());
                    }
                    context.Response.BinaryWrite(response);
                    context.Response.Flush();
                    context.Response.End();
                    return;
                }

                context.Response.Clear();
                context.Response.ContentType = "application/text";
                context.Response.Write(webApp.WesternUnionManager.LastMessage);
            }
            else
            {
                context.Response.Clear();
                context.Response.ContentType = "application/text";
                context.Response.Write(string.Empty);
            }
        }

        private KxWebApplicationStudent GetWebApp(HttpContext context)
        {
            return ((KxWebApplicationStudent)context.Session["WebMgr"]);
        }

    }
}
