﻿using Kx.Core.Web.Configuration;
using Kx.Core.Web.Student.WebApplication.Resource;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace Kx.Core.Web.Student.WebApplication.Handlers
{
    public class ContentRequestHandler : IHttpHandler, System.Web.SessionState.IRequiresSessionState
    {
        public bool IsReusable
        {
            get { return false; }
        }

        public KxWebApplicationStudent WebApp (HttpContext context)
        {
            try
            {
                return ((KxWebApplicationStudent)context.Session["WebMgr"]);
            }
            catch
            {
                return null;
            }
        }

        public void ProcessRequest(HttpContext context)
        {
            context.Response.Cache.SetCacheability(HttpCacheability.NoCache);

            KxStudentResourceProvider provider = KxConfigurationManager.GetSystemObject("ResourceProvider") as KxStudentResourceProvider;

            var result = provider.GetResource(context.Request["resourceid"]) ?? string.Empty;
            var app = WebApp(context);
            if(app != null)
            {
                result = app.ObjectTranslator.MergeTags(result);
            }

            context.Response.Clear();
            context.Response.ContentType = "application/text";
            context.Response.Write(result);
        }
    }
}