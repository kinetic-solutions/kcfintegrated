using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using Kx.Core.Web.BaseClass;
using Kx.Core.Web.Resource;
using Kx.Core.Web.Configuration;
using Kx.Core.Web.Student.WebApplication.ProcessManagers;
using Kx.Core.Web.Student.WebApplication.ObjectTranslator;
using Kx.Core.Web.Student.WebApplication.PageRenderer;
using Kx.Core.Web.PageRenderer;
using Kx.Core.Web.Student.WebApplication.WorkflowInterface;
using Kx.Objects.Student;
using Kx.Objects.Student.Web;


namespace Kx.Core.Web.Student.WebApplication.BaseClass
{
    [Serializable]
    public class WebStudentBaseClass : WebBaseClass
    {
        public new KxWebApplicationStudent WebApp
        {
            get
            {
                return (KxWebApplicationStudent)webApp; 
            }
        }

        public Page CurrentPage
        {
            get
            { return WebApp.CurrentPage; }
        }

        public KxApplicationsManager ApplicationsManager
        {
            get
            {
                return WebApp.ApplicationsManager;
            }
        }

        public KxContractsManager ContractManager
        {
            get
            {
                return WebApp.ContractManager;
            }
        }

        public KxStudentObjectTranslator ObjectTranslator
        {
            get
            {
                return WebApp.ObjectTranslator;
            }
        }
   
        public KxPreferenceSearchManager PreferenceSearchManager
        {
            get
            {
                return WebApp.PreferenceSearchManager;
            }
        }
        public KxRegistrationManager RegistrationManager
        {
            get
            {
                return WebApp.RegistrationManager;
            }
        }

        public KxStudentPageRenderer PageRenderer
        {
            get
            {
                return WebApp.PageRenderer;
            }
        }
        
        public KxStudentWorkflowInterface WorkflowInterface
        {
            get
            {
                return WebApp.WorkflowInterface;
            }
        }

        public WebStudentBaseClass(KxWebApplicationStudent webApp):base(webApp)
        {
            this.webApp = webApp;
        }

        public KxWebUserStudent WebUser
        {
            get
            {
                try
                {
                    return (KxWebUserStudent)WebApp.WorkflowInterface.WebUser;
                }
                catch
                {
                    return null;
                }
            }
        }

        public virtual KxStudent Student
        {
            get
            {
                return WebApp.WorkflowInterface.WebUser.Student;
            }
        }
    }
}
