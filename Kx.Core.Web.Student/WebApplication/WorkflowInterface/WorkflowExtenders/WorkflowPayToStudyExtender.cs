#region copyright

// =======================================================
// Product:   KCF.Student
// Project:   Kx.Core.Web.Student
// File:      WorkflowPayToStudyExtender.cs
// Created:   11/11/2016
// By:        Steve Ashley
// Modified:  21/11/2016
// 
// Copyright � Kinetic Solutions Ltd 2016
// All rights reserved.
// 
// No portion of this file may be re-created, copied or otherwise
// used without the express permission of Kinetic Solutions Ltd.
// =======================================================

#endregion

#region usings

using System;
using System.Collections.Generic;
using System.Text;
using Kx.Core.Exceptions;
using Kx.Core.Localisation;
using Kx.Core.Web.Configuration;
using Kx.Core.Web.Student.Classes;
using Kx.Core.Web.WorkflowInterface.WorkflowExtenders;
using Kx.Core.Workflow;
using Kx.Objects.Student.Web;
using Kx.Core.Web.Student.PayToStudy;

#endregion

namespace Kx.Core.Web.Student.WebApplication.WorkflowInterface.WorkflowExtenders
{
    public class WorkflowPayToStudyExtender : KxStudentEmailExtender, IKCFEditorExtenderHelper, IHaveSettingsForPayToStudy
    {
        #region Fields and Properties

        public string PTSUsername { get; set; }
        public string PTSPassword { get; set; }
        public string BeneficiaryBankAccountId { get; set; }
        public string PayingFor { get; set; }
        public string PaymentAmount { get; set; }
        public string AdditionalField1 { get; set; }
        public string AdditionalField2 { get; set; }
        public string STATEMENTorDEPOSITorPAYNOW { get; set; }
        public string PaymentUrl { get; set; }

        public string OverridePayToStudyTeamEmailResource { get; set; }
        public string OverridePayToStudyTeamEmailAddress { get; set; }


        public new KxWebApplicationStudent WebApp
        {
            get { return (KxWebApplicationStudent) PageRenderer.WebApp; }
        }

        public KxWebApplicationStudent WebAppStudent
        {
            get { return (KxWebApplicationStudent) PageRenderer.WebApp; }
        }

        public KxWebUserStudent WebUser
        {
            get { return (KxWebUserStudent) PageRenderer.WorkflowInterface.WebUser; }
        }

        #endregion

        #region Interface Methods

        public string DescriptiveHelp()
        {
            return new EmbeddedResourceHelper().GetEmbeddedResource("Kx.Core.Web.Student.PayToStudy.KCFEditor.PayToStudyExtenderHelpText.txt");
        }

        public string PageControls()
        {
            return new EmbeddedResourceHelper().GetEmbeddedResource("Kx.Core.Web.Student.PayToStudy.KCFEditor.PayToStudyExtenderPageControls.txt");
        }

        public IEnumerable<string> PageResources()
        {
            yield return "PayToStudyEN";
            yield return "PayToStudySuccessEN";
            yield return "PayToStudy.css";
            yield return "PayToStudyFXConversionEN";
        }

        public IEnumerable<string> EmailTemplates()
        {
            yield return "PayToStudyStudentNotificationEN";
            yield return "PayToStudyTeamNotificationEN";
        }

        public string PageResource(string page)
        {
            return GetKCFEditorHelperResource(page);
        }

        public string EmailTemplate(string templateName)
        {
            return GetKCFEditorHelperResource(templateName);
        }

        #endregion

        #region Public Methods

        public override void AfterLoad()
        {
            base.AfterLoad();
            WebApp.PayToStudyManager.RequestFXConversion(this);
        }

        public override void LoadConfiguration(WorkflowExternalAction externalaction)
        {
            base.LoadConfiguration(externalaction);
            WebApp.PayToStudyManager.InitData();
        }

        public override WorkFlowStatus DetermineCompletionState()
        {
            var result = base.DetermineCompletionState();

            if (result == WorkFlowStatus.Finished && StringComparer.InvariantCultureIgnoreCase.Compare(workflowResult, "makepayment") == 0)
            {
                if (WebApp.PayToStudyManager.ProcessPayment(this))
                {
                    Execute();
                    WorkflowResult = bool.TrueString;
                }
                else
                {
                    result = WorkFlowStatus.Indeterminate;
                    WebApp.PageRenderer.WriteErrorStatus(true, KxLocalisationManager.GetSubstitution(WebApp.PayToStudyManager.LastMessage));
                }
            }

            return result;
        }

        public override string Execute()
        {
            var result = base.Execute();

            SendEmailsToPayToStudy();

            return result;
        }

        #endregion

        #region Private Methods

        private string GetKCFEditorHelperResource(string resourceName)
        {
            return new EmbeddedResourceHelper().GetEmbeddedResource(string.Format("Kx.Core.Web.Student.PayToStudy.KCFEditor.{0}.txt", resourceName));
        }

        private void SendEmailsToPayToStudy()
        {
            string recipientlist = PageRenderer.ObjectTranslator.MergeTags(OverridePayToStudyTeamEmailAddress);
            if (string.IsNullOrWhiteSpace(recipientlist))
            {
                recipientlist = "invoices@paytostudy.com";
            }

            if (recipientlist.IndexOf(";") > -1)
            {
                string[] recips = recipientlist.Split(';');

                foreach (string recip in recips)
                {
                    if (recip != "")
                    {
                        SendPayToStudyMail(recip);
                    }
                }
            }
            else
            {
                SendPayToStudyMail(recipientlist);
            }
        }

        private void SendPayToStudyMail(string recipient)
        {
            if (KxConfigurationManager.Profiling)
            {
                KxLogger.LogActivity(this, "SendPayToStudyMail", "START", recipient);
            }

            bool isPlainText = (BodyFormat == "PlainText") ? true : false;
            string oldTemplate = template;
            if (!string.IsNullOrWhiteSpace(OverridePayToStudyTeamEmailResource))
            {
                template = OverridePayToStudyTeamEmailResource;
            }

            recipient = PageRenderer.ObjectTranslator.MergeTags(recipient);
            string mailSubject = PageRenderer.ObjectTranslator.MergeTags(subject);
            string senderEmail = PageRenderer.ObjectTranslator.MergeTags(senderemail);
            string bodyText = MergeData(isPlainText);

            StringBuilder emailAudit = new StringBuilder();
            emailAudit.AppendLine(string.Format("From: {0}", senderEmail));
            emailAudit.AppendLine(string.Format("To: {0}", recipient));
            emailAudit.AppendLine(string.Format("Subject: {0}", mailSubject));
            emailAudit.AppendLine(string.Format("Body: {0}", bodyText));

            SendMail(recipient, mailSubject, senderEmail, bodyText, bodyFormat);


            template = oldTemplate;

            if (KxConfigurationManager.Profiling)
            {
                KxLogger.LogActivity(this, "SendPayToStudyMail", emailAudit.ToString(), string.Empty);
                KxLogger.LogActivity(this, "SendPayToStudyMail", "END", recipient);
            }
        }

        #endregion
    }
}