using System;
using System.Collections.Generic;
using System.Text;
using Kx.Core.Localisation;
using Kx.Core.Web.WorkflowInterface.WorkflowExtenders;

namespace Kx.Core.Web.Student.WebApplication.WorkflowInterface.WorkflowExtenders
{
    public class WorkflowCardPaymentExtender : WorkflowPaymentExtender
    {
        private string uiCardHolder = "";//"Ian Gordon";
        private string uiCardNumber = "";// "4111111111111111";
        private string uiCardType = "";// "Visa";
        private string uiExpiryMonth = "";// "02";
        private string uiExpiryYear = ""; //"08";
        private string uiStartMonth = ""; //"03";
        private string uiStartYear = ""; //"05";
        private string uiCV2 = ""; //123;
        private int uiIssueNumber = 0; //"";

        private string uiPayerStreet1 = "";//"Street Address 1";
        private string uiPayerStreet2 =  "";//"Street Address 2";
        private string uiPayerCity =  "";//"Milton Keynes";
        private string uiPayerCounty = "";//"Buckinghamshire";
        private string uiPayerPostcode = "";//"MK42 0BZ";
        private string uiPayerEmail = "";//"iang@kinetic-solutions.co.uk";
        private string uiConfirmPayerEmail = "";//"iang@kinetic-solutions.co.uk";
        private string ledgerCode = "";
        private string nominalCode = "";
        private string systemId = "";

        private string accountName = "";
        private string accountNumber = "";
        private string sortCode = "";
        private string bankName = "";
        private string branchName = "";

        public string UiAccountName
        {
            get
            {
                return accountName;
            }
            set
            {
                accountName = value;
            }
        }
        public string UiAccountNumber
        {
            get
            {
                return accountNumber;
            }
            set
            {
                accountNumber = value;
            }
        }
        public string UiSortCode
        {
            get
            {
                return sortCode;
            }
            set
            {
                sortCode = value;
            }
        }
        public string UiBankName
        {
            get
            {
                return bankName;
            }
            set
            {
                bankName = value;
            }
        }
        public string UiBranchName
        {
            get
            {
                return branchName;
            }
            set
            {
                branchName = value;
            }
        }

        public string LedgerCode
        {
            get
            {
                return ledgerCode;
            }
            set
            {
                ledgerCode = value;
            }

        }
        public string NominalCode
        {
            get
            {
                return nominalCode;
            }
            set
            {
                nominalCode = value;
            }
        }
        public string SystemId
        {
            get
            {
                return systemId;
            }
            set
            {
                systemId = value;
            }
        }
        public string UiCardHolder
        {
            get
            { return uiCardHolder; }
            set
            { uiCardHolder = value; }
        }
        public string UiCardNumber
        {
            get
            { return uiCardNumber; }
            set
            { uiCardNumber = value; }
        }
        public string UiCardType
        {
            get
            { return uiCardType; }
            set
            { uiCardType = value; }
        }
        public string UiExpiryMonth
        {
            get
            { return uiExpiryMonth; }
            set
            { uiExpiryMonth = value; }
        }
        public string UiExpiryYear
        {
            get
            { return uiExpiryYear; }
            set
            { uiExpiryYear = value; }
        }
        public string UiStartMonth
        {
            get
            { return uiStartMonth; }
            set
            { uiStartMonth = value; }
        }
        public string UiStartYear
        {
            get
            { return uiStartYear; }
            set
            { uiStartYear = value; }
        }
        public string UiCV2
        {
            get
            { return uiCV2; }
            set
            { uiCV2 = value; }
        }
        public int UiIssueNumber
        {
            get
            { return uiIssueNumber; }
            set
            { uiIssueNumber = value; }
        }
        public string UiPayerStreet1
        {
            get
            { return uiPayerStreet1; }
            set
            { uiPayerStreet1 = value; }
        }
        public string UiPayerStreet2 
        {
            get
            { return uiPayerStreet2; }
            set
            { uiPayerStreet2 = value; }
        }
        public string UiPayerCity
        {
            get
            { return uiPayerCity; }
            set
            { uiPayerCity = value; }
        }
        public string UiPayerCounty
        {
            get
            { return uiPayerCounty; }
            set
            { uiPayerCounty = value; }
        }
        public string UiPayerPostcode
        {
            get
            { return uiPayerPostcode; }
            set
            { uiPayerPostcode = value; }
        }
        public string UiPayerEmail
        {
            get
            { return uiPayerEmail; }
            set
            { uiPayerEmail = value; }
        }
        public string UiConfirmPayerEmail
        {
            get
            { return uiConfirmPayerEmail; }
            set
            { uiConfirmPayerEmail = value; }
        }

   

        public override WorkFlowStatus DetermineCompletionState()
        {
            base.DetermineCompletionState();

            WorkFlowStatus result = base.DetermineCompletionState();

            if (result == WorkFlowStatus.Finished && WorkflowResult == "makecardpayment")
            {
                try
                {
                    string error = WebApp.ContractManager.PostWSPayment(this);

                    if (error == "")
                    {
                        result = WorkFlowStatus.Finished;
                    }
                    else
                    {
                        WebApp.PageRenderer.WriteErrorStatus(!String.IsNullOrEmpty(error), error);
                        result = WorkFlowStatus.Indeterminate;
                    }
                }
                catch (Exception ex)
                {
                    WebApp.PageRenderer.WriteErrorStatus(true, KxLocalisationManager.GetSubstitution("An unknown error occurred") + ": " + ex.Message);
                    result = WorkFlowStatus.Indeterminate;
                }
            }
            else if (result == WorkFlowStatus.Finished && WorkflowResult == "verifydd")
            {
                try
                {
                    string error = WebApp.ContractManager.ValidateDD(this);

                    if (error == "")
                    {
                        result = WorkFlowStatus.Finished;
                    }
                    else
                    {
                        WebApp.PageRenderer.WriteErrorStatus(!String.IsNullOrEmpty(error), error);
                        result = WorkFlowStatus.Indeterminate;
                    }
                }
                catch (Exception ex)
                {
                    WebApp.PageRenderer.WriteErrorStatus(true, KxLocalisationManager.GetSubstitution("An unknown error occurred") + ": " + ex.Message);
                    result = WorkFlowStatus.Indeterminate;
                }
            }
            else if (result == WorkFlowStatus.Finished && WorkflowResult == "confirmdd")
            {
                try
                {
                    string error = WebApp.ContractManager.CreateDirectDebit(this);

                    if (error == "")
                    {
                        result = WorkFlowStatus.Finished;
                    }
                    else
                    {
                        WebApp.PageRenderer.WriteErrorStatus(!String.IsNullOrEmpty(error), error);
                        result = WorkFlowStatus.Indeterminate;
                    }
                }
                catch (Exception ex)
                {
                    WebApp.PageRenderer.WriteErrorStatus(true, KxLocalisationManager.GetSubstitution("An unknown error occurred") + ": " + ex.Message);
                    result = WorkFlowStatus.Indeterminate;
                }
            }
            return result;
        }
    }
}
