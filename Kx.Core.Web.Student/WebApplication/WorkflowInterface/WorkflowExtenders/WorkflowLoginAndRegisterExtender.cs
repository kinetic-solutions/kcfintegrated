﻿using System;
using System.Collections.Generic;
using System.Text;
using Kx.Core.Web.WorkflowInterface.WorkflowExtenders;
using Kx.Core.Localisation;

namespace Kx.Core.Web.Student.WebApplication.WorkflowInterface.WorkflowExtenders
{
    public class WorkflowLoginAndRegisterExtender : WorkflowRegistrationExtender
    {
        private WorkflowLoginExtender _loginExtender = new WorkflowLoginExtender();

        private bool registerEmailAddressRequired = false;

        public bool RequireValidatedEmail
        {
            get
            {
                return _loginExtender.RequireValidatedEmail;
            }
            set
            {
                _loginExtender.RequireValidatedEmail = value;
            }
        }
        public int LoginAttempts
        {
            get
            {
                return _loginExtender.LoginAttempts;
            }
            set
            {
                _loginExtender.LoginAttempts = value;
            }
        }
        public bool LockAccount
        {
            get
            {
                return _loginExtender.LockAccount;
            }
            set
            {
                _loginExtender.LockAccount = value;
            }
        }

        public bool RegisterEmailAddressRequired
        {
            get
            {
                return registerEmailAddressRequired;
            }
            set
            {
                if (registerEmailAddressRequired == value)
                    return;
                registerEmailAddressRequired = value;
            }
        }

        public override void BeforeLoad()
        {
            SetPageRenderer();
            _loginExtender.BeforeLoad();
            base.BeforeLoad();
        }

        private void SetPageRenderer()
        {
            if (_loginExtender.PageRenderer == null && this.pageRenderer != null)
            {
                _loginExtender.PageRenderer = this.pageRenderer;
            }
        }

        public void Reset ()
        {
            SetPageRenderer();
            _loginExtender.Reset();
        }

        public void Logout ()
        {
            SetPageRenderer();
            _loginExtender.Logout();
        }

        public override WorkFlowStatus DetermineCompletionState()
        {
            SetPageRenderer();
            WorkFlowStatus result = WorkFlowStatus.Finished;

            result = base.DetermineCompletionState();

            if (result == WorkFlowStatus.Finished)
            {
                if (workflowResult.ToUpper().IndexOf("DONEWREGISTER") > -1)
                {
                    if (!string.IsNullOrWhiteSpace(WebApp.RegistrationManager.SinglePageRegisterPassword) )
                    {
                        WebApp.RegistrationManager.Password = WebApp.RegistrationManager.SinglePageRegisterPassword;
                        WebApp.RegistrationManager.SinglePageRegisterPassword = null;
                    }

                    WebApp.RegistrationManager.MatchedStudentId = 0;
                    this.IgnoreStudentMatching = true;

                    string Error = Validate();
                    Error = NewRegistrationValidation(Error);

                    if (string.IsNullOrWhiteSpace(WebApp.RegistrationManager.Password))
                    {
                        Error = KxLocalisationManager.GetSubstitution("Password is required.");
                    }

                    if (Error != "")
                    {
                        WebApp.AddStrike(); 
                        result = WorkFlowStatus.Indeterminate;
                        WebApp.PageRenderer.WriteErrorStatus(!String.IsNullOrEmpty(Error), Error);
                        
                    }
                    else
                    {
//                      WebApp.RegistrationManager.CreateWebUser();

                        if (DefaultUserNameToStudentId && WebApp.RegistrationManager.MatchedStudent != null)
                        {
                            WebApp.RegistrationManager.UserName = WebApp.RegistrationManager.MatchedStudent.StudentId;   
                        }
                        else if (EmailUserName)
                        {
                            WebApp.RegistrationManager.UserName = WebApp.RegistrationManager.EmailAddress;
                        }

                        WebApp.RegistrationManager.CreateWebUser();
                    
                    }
                }
            }
            
            if (workflowResult.ToUpper().IndexOf("LOGIN") > -1 || workflowResult.ToUpper().IndexOf("LOGINSSO") > -1 || workflowResult.ToUpper().IndexOf("LOGINBROCK") > -1)
            {
                string error = ValidateFieldsPopulated_Login();
                if (string.IsNullOrEmpty(error))
                {
                    _loginExtender.CaptureMarketingCampaign = CaptureMarketingCampaign;
                    result = _loginExtender.DetermineCompletionState();
                    if (result == WorkFlowStatus.Finished)
                    {
                        this.workflowResult = _loginExtender.WorkflowResult;
                    }
                }
                else
                {
                    WebApp.AddStrike();
                    result = WorkFlowStatus.Indeterminate;
                    WebApp.PageRenderer.WriteErrorStatus(!String.IsNullOrEmpty(error), error);
                }
            }
            
            return result;
        }

        private string NewRegistrationValidation(string result)
        {
            if (registerEmailAddressRequired && WebApp.RegistrationManager.EmailAddress == "")
            {
                result += "\r\n" + KxLocalisationManager.GetSubstitution("Email Address is required.");
            }
            return result;
        }

        private string ValidateFieldsPopulated_Login()
        {
            string result = string.Empty;

            if (workflowResult.ToUpper().IndexOf("LOGIN") > -1)
            {
                if (string.IsNullOrEmpty(WebApp.RegistrationManager.UserName))
                {
                    result += KxLocalisationManager.GetSubstitution("Username is required");
                }
                if (string.IsNullOrEmpty(WebApp.RegistrationManager.Password))
                {
                    result += "\r\n" + KxLocalisationManager.GetSubstitution("Password is required");
                }
            }

            return result;
        }
    }
}
