using System;
using System.Collections.Generic;
using System.Text;
using Kx.Core;
using Kx.Core.Exceptions;
using Kx.Core.PaymentProcessor;
using Kx.Core.PaymentProcessor.Payments;
using Kx.Core.Web.WorkflowInterface.WorkflowExtenders;
using Kx.Objects.Student.Web;
using Kx.Core.Utility;

namespace Kx.Core.Web.Student.WebApplication.WorkflowInterface.WorkflowExtenders
{
    public class WorkflowPaymentExtender : KxWorkflowPaymentExtender
    {
        protected bool IsContractPage = false;

        private string billingStreet1 = "";
        private string billingStreet2 = "";
        private string billingStreet3 = "";
        private string billingCity = "";
        private string billingCountry = "";
        private string billingPostcode = "";
        private string customerTelephone = "";
        private string customerEmail = "";
        private string billingCountryCode= "";
        private bool billingAddressRequired;
        private string paymentPageID = "";

        public string BillingStreet1
        {
            get => billingStreet1;
            set => billingStreet1 = value;
        }

        public string BillingStreet2
        {
            get => billingStreet2;
            set => billingStreet2 = value;
        }
        public string BillingStreet3
        {
            get => billingStreet3;
            set => billingStreet3 = value;
        }
        public string BillingCity
        {
            get => billingCity;
            set => billingCity = value;
        }
        public string BillingCountry
        {
            get => billingCountry;
            set => billingCountry = value;
        }
        public string BillingCountryCode
        {
            get => billingCountryCode;
            set => billingCountryCode = value;
        }
        public string BillingPostcode
        {
            get => billingPostcode;
            set => billingPostcode = value;
        }

        public string CustomerTelephone
        {
            get => customerTelephone;
            set => customerTelephone = value;
        }

        public string CustomerEmail
        {
            get => customerEmail;
            set => customerEmail = value;
        }

        public new KxWebApplicationStudent WebApp
        {
            get
            { return (KxWebApplicationStudent)PageRenderer.WebApp; }
        }

        public KxWebApplicationStudent WebAppStudent
        {
            get
            { return (KxWebApplicationStudent)PageRenderer.WebApp; }
        }

        public KxWebUserStudent WebUser
        {
            get
            { return (KxWebUserStudent)PageRenderer.WorkflowInterface.WebUser; }
        }

        public bool BillingAddressRequired { get => billingAddressRequired; set => billingAddressRequired = value; }
        
        public string PaymentPageID { get => paymentPageID; set => paymentPageID = value; }


        public override void MakePayment()
        {
            if (PaymentType.ToUpper() == "STATEMENT")
            {
                WebAppStudent.StatementManager.PostPayment(this);
            }
            else
            {
                WebAppStudent.ContractManager.PostPayment(this);
            }
        }

        public override WorkFlowStatus DetermineCompletionState()
        {
            return base.DetermineCompletionState();
        }

        public override void AfterRender()
        {
            base.AfterRender();

            if (IsContractPage)
            {
                try
                {
                    try
                    {
                        WebUser.WriteStudentHistoryRecord("ViewedAllocation", String.Format("Student {0} {1} viewed their allocation online.", WebUser.Student.ForeName, WebUser.Student.SurName));
                    }
                    catch { }

                    if (WebApp.WorkflowInterface.WebUser.PendingAllocation != null)
                        WebApp.WorkflowInterface.WebUser.PendingAllocation.AcceptancePage = WebApp.PageRenderer.PageContent;
                    else
                    {
                        if (WebApp.WorkflowInterface.WebUser.AcceptedAllocation.AcceptancePage == null | WebApp.WorkflowInterface.WebUser.AcceptedAllocation.AcceptancePage == string.Empty)
                            WebApp.WorkflowInterface.WebUser.AcceptedAllocation.AcceptancePage = WebApp.PageRenderer.PageContent;
                        if (WebApp.WorkflowInterface.WebUser.AcceptedAllocation.AcceptanceDocument == null | WebApp.WorkflowInterface.WebUser.AcceptedAllocation.AcceptanceDocument == string.Empty)
                            WebApp.WorkflowInterface.WebUser.AcceptedAllocation.AcceptanceDocument = KxStringUtils.PageToDocument(WebApp.WorkflowInterface.WebUser.AcceptedAllocation.AcceptancePage);
                    }
                }
                catch { }
            }
        }

    }
}
