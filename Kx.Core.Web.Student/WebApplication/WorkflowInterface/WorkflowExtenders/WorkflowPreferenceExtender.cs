﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using Kx.Core.Web.WorkflowInterface.WorkflowExtenders;
using Kx.Objects.Student.Web;
using System.Web.UI.WebControls;
using Kx.Core.Web.PageRenderer.Utility;
using Kx.Core.Exceptions;

namespace Kx.Core.Web.Student.WebApplication.WorkflowInterface.WorkflowExtenders
{
    public class WorkflowPreferenceExtender : KxStudentWorkflowExtender
    {
        #region Constants

        private const string STR_Visible = "Visible";

        #endregion

        #region Fields

        protected string PreferenceResultsTableID = "tblResultsTable";
        protected string PreferenceEmptyTableID = "tblNoResultsTable";
        protected string PreferenceResultsTemplateTableID = "tblResultsTemplateTable";

        #endregion

        #region Properties

        public int MinPreferences { get; set; }
        public int MaxPreferences { get; set; }
        public string CustomPreferenceProcedure { get; set; }

        public string Filter1 { get; set; }
        public string Filter2 { get; set; }
        public string Filter3 { get; set; }
        public string Filter4 { get; set; }

        #endregion

        #region Public Methods

        public override void AfterLoad()
        {
            WriteControlProperty(PreferenceResultsTemplateTableID, STR_Visible, true);
            WriteControlProperty(PreferenceEmptyTableID, STR_Visible, true);
            WorkFlowStatus state = base.DetermineCompletionState();

            WebApp.PreferenceManager.GetPreferences();
            PopulateResults(WebApp.PreferenceManager.Results, PreferenceResultsTableID, PreferenceResultsTemplateTableID,
                PreferenceEmptyTableID, "_PS");


            WriteControlProperty(PreferenceResultsTableID, STR_Visible, true);
            WriteControlProperty(PreferenceResultsTemplateTableID, STR_Visible, false);
            WriteControlProperty(PreferenceEmptyTableID, STR_Visible, false);

            base.AfterLoad();
        }

        public override void LoadConfiguration(Workflow.WorkflowExternalAction externalaction)
        {
            base.LoadConfiguration(externalaction);

            try
            {
                WebApp.PreferenceManager.CustomPreferenceProcedure =
                    externalaction.Values["CUSTOMPREFERENCEPROCEDURE:BOTH"].Value;

                if (externalaction.Values["FILTER1:BOTH"].Value.Equals("USERGUID"))
                {
                    WebApp.PreferenceManager.Filter1 = WebUser.UserGuid.ToString();
                }
                else
                {
                    WebApp.PreferenceManager.Filter1 = this.Filter1;
                }

                WebApp.PreferenceManager.Filter2 = this.Filter2;
                WebApp.PreferenceManager.Filter3 = this.Filter3;
                WebApp.PreferenceManager.Filter4 = this.Filter4;

                WebApp.PreferenceManager.GetPreferences();
                // pha KxLogger.LogActivity(this, "ResetMinMaxPref", string.Format("{0} - {1}", WebApp.PreferenceManager.MinPreferences, WebApp.PreferenceManager.MaxPreferences), "");
                if (WebApp.PreferenceManager.MinPreferences > 0)
                {
                    this.MinPreferences = WebApp.PreferenceManager.MinPreferences;
                }
                if (WebApp.PreferenceManager.MaxPreferences > 0)
                {
                    this.MaxPreferences = WebApp.PreferenceManager.MaxPreferences;
                }
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "LoadConfiguration", ex.Message, "");
            }
        }

        public override WorkFlowStatus DetermineCompletionState()
        {
            WorkFlowStatus result = base.DetermineCompletionState();

            if (result == WorkFlowStatus.Finished && base.workflowResult.ToLower() != "back" &&
                PageRenderer.UserAction.ReturnType != "S")
            {
                WebApp.PreferenceManager.SavePreferences();

                if (WebUser.CurrentApplication.Preferences.Count < MinPreferences)
                {
                    WebApp.PageRenderer.WriteErrorStatus(true,
                        string.Format("{0} preferences must be selected", MinPreferences));
                    result = WorkFlowStatus.Indeterminate;
                }
            }


            return result;
        }

        #endregion

        #region Private Methods

        private void PopulateResults(KxWebStudentPreferences source, string Sourcetable, string templatetable,
            string emptytable, string suffix)
        {
            try
            {
                int idx = 0;
                Control sourceControl = KxPageUtility.FindControl(WebApp.CurrentPage, Sourcetable);
                Control templateControl = KxPageUtility.FindControl(WebApp.CurrentPage, templatetable);
                Control emptyTemplateControl = KxPageUtility.FindControl(WebApp.CurrentPage, emptytable);

                string items = "";

                if (sourceControl != null)
                {
                    ClearCurrentSourceControl(sourceControl);

                    if (source.Items.Count > 0)
                    {
                        foreach (KxWebStudentPreference pref in source.Items)
                        {
                            if (pref.UIObject == null)
                            {
                                try
                                {
                                    foreach (
                                        KxWebStudentPreference studentPref in
                                            WebUser.CurrentApplication.Preferences.Items)
                                    {
                                        if ((studentPref.SiteId == pref.SiteId)
                                            && (studentPref.AreaId == pref.AreaId)
                                            && (studentPref.BlockId == pref.BlockId)
                                            && (studentPref.BedroomTypeId == pref.BedroomTypeId)
                                            && (studentPref.CustomField1 == pref.CustomField1)
                                            && (studentPref.CustomField2 == pref.CustomField2)
                                            && (studentPref.CustomValue1 == pref.CustomValue1)
                                            && (studentPref.CustomValue2 == pref.CustomValue2)
                                            && (studentPref.CustomValue3 == pref.CustomValue3)
                                            && (studentPref.CustomValue4 == pref.CustomValue4)
                                            && (studentPref.CustomValue5 == pref.CustomValue5)
                                            && (studentPref.CustomValue6 == pref.CustomValue6)
                                            && (studentPref.CustomValue7 == pref.CustomValue7)
                                            && (studentPref.CustomValue8 == pref.CustomValue8)
                                            && (studentPref.Floor == pref.Floor)
                                            && (studentPref.Smoking == pref.Smoking)
                                            && (studentPref.SingleSexArea == pref.SingleSexArea)
                                            && (studentPref.ContractLength == pref.ContractLength)
                                            )
                                        {
                                            pref.PreferenceNumber = studentPref.PreferenceNumber;
                                        }
                                    }

                                    items = pref.PreferenceId.ToString() + ",";
                                    idx = SetPreferenceUIObject(suffix, pref, sourceControl, templateControl, idx);
                                }
                                catch
                                {
                                }
                            }
                            else
                            {
                                AddControlFromUIObject(sourceControl, pref);
                            }
                        }

                        WebApp.PageRenderer.PopulateControlsList(sourceControl);
                    }
                    else
                    {
                        if (emptyTemplateControl != null)
                        {
                            DisplayEmptyTemplate(suffix, sourceControl, emptyTemplateControl, idx);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "PopulateResults", ex.Message, "");
            }
        }


        #endregion

        #region Protected Methods

        protected virtual void AddControlFromUIObject(Control sourceControl, KxWebStudentPreference pref)
        {
            ((Table) sourceControl).Rows.Add(((TableRow) pref.UIObject));
        }

        protected virtual void ClearCurrentSourceControl(Control sourceControl)
        {
            KxPageUtility.EmptyTableExceptHeader((Table) sourceControl, 1);
        }

        protected virtual int SetPreferenceUIObject(string suffix, KxWebStudentPreference pref, Control tbl,
            Control template, int idx)
        {
            pref.UIObject = KxPageUtility.CloneRow((Table) tbl, ((Table) template).Rows[0],
                suffix + pref.PreferenceId.ToString(), ref idx);
            return idx;
        }

        protected virtual void DisplayEmptyTemplate(string suffix, Control sourceControl, Control emptyTemplateControl, int idx)
        {
            KxPageUtility.CloneRow((Table)sourceControl, ((Table)emptyTemplateControl).Rows[0], "XX" + suffix, ref idx);
        }
        #endregion
    }
}