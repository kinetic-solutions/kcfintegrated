using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using Kx.Core.Localisation;
using Kx.Core.Security;
using Kx.Core.Web.WorkflowInterface.WorkflowExtenders;
using Kx.Core.Exceptions;
using System.Web.Security;
using Kx.Core.Web.Configuration;

namespace Kx.Core.Web.Student.WebApplication.WorkflowInterface.WorkflowExtenders
{
    class WorkflowLoginExtender : KxStudentWorkflowExtender
    {
        private bool requireValidatedEmail = false;
        private int loginAttempts = 3;
        private bool lockAccount = false;
        private bool captureMarketingCampaign = false;

        public bool RequireValidatedEmail
        {
            get
            {
                return requireValidatedEmail;
            }
            set
            {
                requireValidatedEmail = value;
            }
        }
        public int LoginAttempts
        {
            get
            {
                return loginAttempts;
            }
            set
            {
                loginAttempts = value;
            }
        }
        public bool LockAccount
        {
            get
            {
                return lockAccount;
            }
            set
            {
                lockAccount = value;
            }
        }
        
        public bool CaptureMarketingCampaign
        {
            get
            {
                return captureMarketingCampaign;
            }
            set
            {
                captureMarketingCampaign = value;
            }
        }

        public override void BeforeLoad()
        {
            base.BeforeLoad();

            if (HttpContext.Current.Session["SessionTimeout"] != null)
            {
                WebApp.PageRenderer.WriteErrorStatus(true, KxLocalisationManager.GetSubstitution("Your session timed out, please login again"));
            }
        }

        public override WorkFlowStatus DetermineCompletionState()
        {
			WebApp.RegistrationManager.RequireConfirmedEmail = RequireValidatedEmail;
            WebApp.RegistrationManager.CaptureMarketingCampaign = captureMarketingCampaign;
			WorkFlowStatus state = base.DetermineCompletionState();

			if (state == WorkFlowStatus.Finished)
			{
				if (workflowResult.ToUpper().IndexOf("LOGIN") > -1)
				{
					WebApp.RegistrationManager.RetryLimit = loginAttempts;

					if (WebApp.RegistrationManager.ValidateLogin())
					{
						WebApp.RegistrationManager.LoginUser();
						HttpContext.Current.Session["SessionTimeout"] = null;
					}
					else
					{
						WebApp.AddStrike();

						if (WebApp.RegistrationManager.InvalidLogon)
						{
							WebApp.PageRenderer.WriteErrorStatus(true, KxLocalisationManager.GetSubstitution("Incorrect Logon details"));
						}

						if (WebApp.RegistrationManager.InvalidUser)
						{
							WebApp.PageRenderer.WriteErrorStatus(true, KxLocalisationManager.GetSubstitution("User not recognised"));
						}

						if (WebApp.RegistrationManager.RetriesExceeded)
						{
							WebApp.PageRenderer.WriteErrorStatus(true, KxLocalisationManager.GetSubstitution("Login retries exceeded, Please close your browser and leave for 5 minutes prior to attempting to log in again."));
						}

						if (WebApp.RegistrationManager.UnconfirmedEmail)
						{
							WebApp.PageRenderer.WriteErrorStatus(true, KxLocalisationManager.GetSubstitution("Cannot login until E-mail address is confirmed"));
						}
						state = WorkFlowStatus.Indeterminate;
					}
				}
				else if (workflowResult.ToUpper().IndexOf("LOGINSSO") > -1)
				{
					if (WebApp.RegistrationManager.ValidateLoginSSO())
					{
						WebApp.RegistrationManager.LoginUser();
						HttpContext.Current.Session["SessionTimeout"] = null;
					}
					else
					{
						WebApp.AddStrike();
						WebApp.PageRenderer.WriteErrorStatus(true, KxLocalisationManager.GetSubstitution("Incorrect Logon details"));
					}
				}
				else if (workflowResult.ToUpper().IndexOf("LOGINBROCK") > -1)
				{
                    Kx.Core.Web.Providers.AuditProvider.ConfiguredAddToAudit("WorkFlowLoginExtender", "DetermineCompletionState", "LOGINBROCK");

					if (WebApp.RegistrationManager.ValidateLoginBrock())
                    {
                        Kx.Core.Web.Providers.AuditProvider.ConfiguredAddToAudit("WorkFlowLoginExtender", "DetermineCompletionState", "LOGINBROCK: Valid");
						WebApp.RegistrationManager.LoginUser();
						HttpContext.Current.Session["SessionTimeout"] = null;
					}
					else
                    {
                        Kx.Core.Web.Providers.AuditProvider.ConfiguredAddToAudit("WorkFlowLoginExtender", "DetermineCompletionState", "LOGINBROCK: Fail");
						WebApp.AddStrike();
						WebApp.PageRenderer.WriteErrorStatus(true, KxLocalisationManager.GetSubstitution("Incorrect Logon details"));
					}
				}
			}
			return state;
        }

        public void Reset()
        {
            try
            {
                Kx.Core.Web.Providers.AuditProvider.ConfiguredAddToAudit("WorkFlowLoginExtender", "Reset Start", "");

                if (WebApp.RegistrationManager != null)
                {
                    Kx.Core.Web.Providers.AuditProvider.ConfiguredAddToAudit("WorkFlowLoginExtender", "Reset", "");
                    WebApp.RegistrationManager.Reset();
                }

                Kx.Core.Web.Providers.AuditProvider.ConfiguredAddToAudit("WorkFlowLoginExtender", "Reset Finish", "");
            }
            catch
            { }
        }

        public void Logout()
        {
            try
            {
                if (WebApp.RegistrationManager != null)
                {
                    Kx.Core.Web.Providers.AuditProvider.ConfiguredAddToAudit("WorkFlowLoginExtender", "Logout", "");
                    WebApp.RegistrationManager.Reset();
                    FormsAuthentication.SignOut();
                    
                    WebApp.WorkflowInterface.WebUser.LoggedIn = false;                    
                    WebApp.WorkflowInterface.ResetWebUser();
                    WebApp.WorkflowInterface.WebUser.LoadStudent(-1);
                    KxConfigurationManager.ResetSession(WebApp.CurrentPage);
                    HttpContext.Current.Session.Clear();
                }
            }
            catch
            { }
        }
    }
}
