﻿#region usings

#region Copyright

// =======================================================
// 	Product:	NAFF.Student
// 	Project: 	Kx.Core.Web.Student
// 	File:		WorkflowInventoryExtender.cs
// 	Created:	13/08/2013
// 	By:			Steve Ashley
// 	Modified:	13/08/2013
// 
// 	Copyright © Kinetic Solutions Ltd 2013
// 	All rights reserved.
// 
// 	No portion of this file may be re-created, copied or otherwise
// 	used without the express permission of Kinetic Solutions Ltd.
// =======================================================

#endregion

using System;
using Kx.Core.Web.WorkflowInterface.WorkflowExtenders;
using Kx.Core.Workflow;
using Kx.Core.Web.Student.WebApplication.ObjectTranslator;

#endregion

namespace Kx.Core.Web.Student.WebApplication.WorkflowInterface.WorkflowExtenders
{
    public class WorkflowInventoryExtender : KxStudentWorkflowExtender
    {
        #region Properties

        private KxStudentObjectTranslator ObjectTranslator
        {
            get
            { return (KxStudentObjectTranslator)PageRenderer.ObjectTranslator; }
        }
        /// <summary>
        /// Provides the location of the Inventory WebService witin KxInspections
        /// </summary>
        public string WebServiceURL {
            get
            {
                return WebApp.InventoryManager.WebServiceURL;
            }
            set
            {
                WebApp.InventoryManager.WebServiceURL = value;
            }
        }

        /// <summary>
        /// Indicates that a secure connection is used and so will trust self certified SSL certificates
        /// </summary>
        public bool TrustSSLCertificate
        {
            get
            {
                return WebApp.InventoryManager.TrustSSLCertificate;
            }
            set
            {
                WebApp.InventoryManager.TrustSSLCertificate = value;
            }
        }

        /// <summary>
        /// Determines what the "Admin" comments will have as the Comment By field.  If blank then will be the name of the user who entered the comment.
        /// </summary>
        public string AdminDisplay
        {
            get
            {
                return WebApp.InventoryManager.AdminDisplay;
            }
            set
            {
                WebApp.InventoryManager.AdminDisplay = value;
            }
        }

        /// <summary>
        /// Determines what the Student comments will have as the Comment By field when the name of the commenter matches the name of the current student.  If blank then will be the name of the user who entered the comment.
        /// </summary>
        public string YouDisplay
        {
            get
            {
                return WebApp.InventoryManager.YouDisplay;
            }
            set
            {
                WebApp.InventoryManager.YouDisplay = value;
            }
        }


        #endregion

        #region Overrides of KxWorkflowExtender

        public override WorkFlowStatus DetermineCompletionState()
        {
            WorkFlowStatus result = base.DetermineCompletionState();

            if (result == WorkFlowStatus.Finished)
            {
                switch (base.workflowResult.ToLower())
                {
                    case "accept":
                        WebApp.InventoryManager.AcceptInventory(ObjectTranslator.GenericControlIndex);
                        break;
                    case "dispute":
                        WebApp.InventoryManager.DisputeInventory(ObjectTranslator.GenericControlIndex);
                        break;
                    case "comment":
                        WebApp.InventoryManager.SaveComments(ObjectTranslator.GenericControlIndex, true);
                        break;
                    case "commentanddispute":
                        WebApp.InventoryManager.SaveComments(ObjectTranslator.GenericControlIndex, true);
                        WebApp.InventoryManager.DisputeInventory(ObjectTranslator.GenericControlIndex);
                        break;
                }
            }

            /* Always flag the comments as read */
            WebApp.InventoryManager.MarkCommentsAsRead();

            return result;
        }

        #endregion
    }
}