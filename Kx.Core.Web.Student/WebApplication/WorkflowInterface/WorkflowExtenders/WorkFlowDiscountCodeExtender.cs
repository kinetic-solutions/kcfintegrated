﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kx.Core.Exceptions;
using Kx.Core.Localisation;
using Kx.Core.Web.Providers;
using Kx.Core.Web.WorkflowInterface.WorkflowExtenders;

namespace Kx.Core.Web.Student.WebApplication.WorkflowInterface.WorkflowExtenders
{
    /// <summary>
    /// Added the workflow to handle the discount codes
    /// </summary>
    public class WorkFlowDiscountCodeExtender : KxStudentWorkflowExtender
    {
        /// <summary>
        /// constructor
        /// </summary>
        public WorkFlowDiscountCodeExtender()
        {
        }

        /// <summary>
        /// determines the workflow state and would 
        /// </summary>
        /// <returns>WorkFlowStatus</returns>
        public override WorkFlowStatus DetermineCompletionState()
        {
            KxLogger.LogActivity("WorkFlowDiscountCodeExtender", "DetermineCompletionState", "Started", "");

            WorkFlowStatus state = base.DetermineCompletionState();
            if (state == WorkFlowStatus.Finished)
            {
                if (workflowResult.ToUpper().IndexOf("VALIDATE") > -1)
                {
                    if (WebApp.DiscountCodeManager.IsValidDiscountCode())
                    {
                        var control1 = WebApp.PageRenderer.FindKxWebControl("KX_PCDISCOUNTCODESMGR_LAAppliedDiscountCodes");
                        control1.ProcessBulletedList();
                        WebApp.PageRenderer.WriteErrorStatus(false,"");
                        state = WorkFlowStatus.Indeterminate;

                        KxLogger.LogActivity("WorkFlowDiscountCodeExtender", "DetermineCompletionState", "Discount Code applied - WEBUSER", "");
                    }
                    else
                    {

                        WebApp.PageRenderer.WriteErrorStatus(true, KxLocalisationManager.GetSubstitution("Invalid Discount Code"));
                        state = WorkFlowStatus.Indeterminate;

                        KxLogger.LogActivity("WorkFlowDiscountCodeExtender", "DetermineCompletionState", "Invalid Discount Code", "");
                    }
                }

            }


            KxLogger.LogActivity("WorkFlowDiscountCodeExtender", "DetermineCompletionState", "Completed", "");

            return state;
        }

        public void Reset()
        {
            AuditProvider.ConfiguredAddToAudit("WorkFlowDiscountCodeExtender", "Reset Start", "");
            WebApp.DiscountCodeManager.DiscountCode = "";
            AuditProvider.ConfiguredAddToAudit("WorkFlowDiscountCodeExtender", "Reset Finish", "");
        }
    }
}
