#region copyright

// =======================================================
// Product:   KCF.Student
// Project:   Kx.Core.Web.Student
// File:      WorkflowPayToStudyExtender.cs
// Created:   11/11/2016
// By:        Steve Ashley
// Modified:  21/11/2016
// 
// Copyright � Kinetic Solutions Ltd 2016
// All rights reserved.
// 
// No portion of this file may be re-created, copied or otherwise
// used without the express permission of Kinetic Solutions Ltd.
// =======================================================

#endregion

#region usings

using System;
using System.Collections.Generic;
using System.Text;
using Kx.Core.Exceptions;
using Kx.Core.Localisation;
using Kx.Core.Web.Configuration;
using Kx.Core.Web.Student.Classes;
using Kx.Core.Web.WorkflowInterface.WorkflowExtenders;
using Kx.Core.Workflow;
using Kx.Objects.Student.Web;
using Kx.Core.Web.Student.PayToStudy;

#endregion

namespace Kx.Core.Web.Student.WebApplication.WorkflowInterface.WorkflowExtenders
{
    public class WorkflowWesternUnionHistoryExtender : KxStudentWorkflowExtender, IKCFEditorExtenderHelper
    {
        #region Fields and Properties

        [DefaultExtenderValue("https://api.globalpay.wu.com/geo-api/")]
        public string APIUrl { get; set; }

        public string GPFIClientId { get; set; }

        public new KxWebApplicationStudent WebApp
        {
            get { return (KxWebApplicationStudent) PageRenderer.WebApp; }
        }

        public KxWebApplicationStudent WebAppStudent
        {
            get { return (KxWebApplicationStudent) PageRenderer.WebApp; }
        }

        public KxWebUserStudent WebUser
        {
            get { return (KxWebUserStudent) PageRenderer.WorkflowInterface.WebUser; }
        }

        #endregion

        #region Interface Methods

        public string DescriptiveHelp()
        {
            return new EmbeddedResourceHelper().GetEmbeddedResource("Kx.Core.Web.Student.WesternUnion.KCFEditor.WesternUnionExtenderHelpText.txt");
        }

        public string PageControls()
        {
            return new EmbeddedResourceHelper().GetEmbeddedResource("Kx.Core.Web.Student.WesternUnion.KCFEditor.WesternUnionHistoryExtenderPageControls.txt");
        }

        public IEnumerable<string> PageResources()
        {
            yield return "WesternUnionHistoryEN";
            yield return "WesternUnionHistoryRowEN";
        }

        public IEnumerable<string> EmailTemplates()
        {
            yield return "";
        }

        public string PageResource(string page)
        {
            return GetKCFEditorHelperResource(page);
        }

        public string EmailTemplate(string templateName)
        {
            return GetKCFEditorHelperResource(templateName);
        }

        #endregion

        #region Public Methods

        public override void LoadConfiguration(WorkflowExternalAction externalaction)
        {
            base.LoadConfiguration(externalaction);
            WebAppStudent.WesternUnionManager.APIUrl = this.APIUrl;
            WebAppStudent.WesternUnionManager.GPFIClientId = this.GPFIClientId;
        }

        public override void BeforeLoad()
        {
            base.BeforeLoad();

            WebAppStudent.WesternUnionManager.LoadPaymentHistory(this);
        }

        #endregion

        #region Private Methods

        private string GetKCFEditorHelperResource(string resourceName)
        {
            return new EmbeddedResourceHelper().GetEmbeddedResource(string.Format("Kx.Core.Web.Student.WesternUnion.KCFEditor.{0}.txt", resourceName));
        }
        

        #endregion
    }
}