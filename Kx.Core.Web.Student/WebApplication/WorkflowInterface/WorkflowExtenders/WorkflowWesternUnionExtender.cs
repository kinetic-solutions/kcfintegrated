#region copyright

// =======================================================
// Product:   KCF.Student
// Project:   Kx.Core.Web.Student
// File:      WorkflowPayToStudyExtender.cs
// Created:   11/11/2016
// By:        Steve Ashley
// Modified:  21/11/2016
// 
// Copyright � Kinetic Solutions Ltd 2016
// All rights reserved.
// 
// No portion of this file may be re-created, copied or otherwise
// used without the express permission of Kinetic Solutions Ltd.
// =======================================================

#endregion

#region usings

using System;
using System.Collections.Generic;
using System.Text;
using Kx.Core.Exceptions;
using Kx.Core.Localisation;
using Kx.Core.Web.Configuration;
using Kx.Core.Web.Student.Classes;
using Kx.Core.Web.WorkflowInterface.WorkflowExtenders;
using Kx.Core.Workflow;
using Kx.Objects.Student.Web;
using Kx.Core.Web.Student.PayToStudy;

#endregion

namespace Kx.Core.Web.Student.WebApplication.WorkflowInterface.WorkflowExtenders
{
    public class WorkflowWesternUnionExtender : KxStudentEmailExtender, IKCFEditorExtenderHelper
    {
        #region Fields and Properties

        public string PaymentAmount { get; set; }
        [DefaultExtenderValue("STATEMENT")]
        public string STATEMENTorDEPOSITorPAYNOW { get; set; }
        [DefaultExtenderValue("https://api.globalpay.wu.com/geo-api/")]
        public string APIUrl { get; set; }

        [DefaultExtenderValue("GBP")]
        public string SettlementCurrency { get; set; }

        [DefaultExtenderValue("False")]
        public bool StartNewProcess { get; set; }

        public string GPFIClientId { get; set; }

        public string PayingFor { get; set; }

        public string Custom1 { get; set; }
        public string Custom2 { get; set; }
        public string Custom3 { get; set; }
        public string Custom4 { get; set; }
        public string Custom5 { get; set; }
        public string Custom6 { get; set; }
        public string Custom7 { get; set; }
        public string Custom8 { get; set; }
        public string Custom9 { get; set; }
        public string Custom10 { get; set; }

        public new KxWebApplicationStudent WebApp
        {
            get { return (KxWebApplicationStudent) PageRenderer.WebApp; }
        }

        public KxWebApplicationStudent WebAppStudent
        {
            get { return (KxWebApplicationStudent) PageRenderer.WebApp; }
        }

        public KxWebUserStudent WebUser
        {
            get { return (KxWebUserStudent) PageRenderer.WorkflowInterface.WebUser; }
        }

        #endregion

        #region Interface Methods

        public string DescriptiveHelp()
        {
            return new EmbeddedResourceHelper().GetEmbeddedResource("Kx.Core.Web.Student.WesternUnion.KCFEditor.WesternUnionExtenderHelpText.txt");
        }

        public string PageControls()
        {
            return new EmbeddedResourceHelper().GetEmbeddedResource("Kx.Core.Web.Student.WesternUnion.KCFEditor.WesternUnionExtenderPageControls.txt");
        }

        public IEnumerable<string> PageResources()
        {
            yield return "WesternUnionInputEN";
            yield return "WesternUnionQuotedOrderEN";
            yield return "WesternUnionOrderCompleteEN";
        }

        public IEnumerable<string> EmailTemplates()
        {
            yield return "WesternUnionPaymentInstructionsEN";
        }

        public string PageResource(string page)
        {
            return GetKCFEditorHelperResource(page);
        }

        public string EmailTemplate(string templateName)
        {
            return GetKCFEditorHelperResource(templateName);
        }

        #endregion

        #region Public Methods

        public override void LoadConfiguration(WorkflowExternalAction externalaction)
        {
            base.LoadConfiguration(externalaction);
            WebAppStudent.WesternUnionManager.APIUrl = this.APIUrl;
            WebAppStudent.WesternUnionManager.GPFIClientId = this.GPFIClientId;
            WebAppStudent.WesternUnionManager.SettlementCurrency = this.SettlementCurrency;

            if (StartNewProcess)
            {
                WebAppStudent.WesternUnionManager.Reset();
            }
        }

        public override WorkFlowStatus DetermineCompletionState()
        {
            var result = base.DetermineCompletionState();

            if (result == WorkFlowStatus.Finished && StringComparer.InvariantCultureIgnoreCase.Compare(workflowResult, "quote") == 0)
            {
                if (WebApp.WesternUnionManager.QuoteOrder(this))
                {
                    WorkflowResult = bool.TrueString;
                }
                else
                {
                    result = WorkFlowStatus.Indeterminate;
                    WebApp.PageRenderer.WriteErrorStatus(true, KxLocalisationManager.GetSubstitution(WebApp.WesternUnionManager.LastMessage.ToString()));
                }
            }
            else if (result == WorkFlowStatus.Finished && StringComparer.InvariantCultureIgnoreCase.Compare(workflowResult, "order") == 0)
            {
                if (WebApp.WesternUnionManager.LogQuotedOrder(this))
                {
                    Execute();
                    WorkflowResult = bool.TrueString;
                }
                else
                {
                    result = WorkFlowStatus.Indeterminate;
                    WebApp.PageRenderer.WriteErrorStatus(true, KxLocalisationManager.GetSubstitution(WebApp.WesternUnionManager.LastMessage.ToString()));
                }
            }

            return result;
        }

        public override string Execute()
        {

            string fileName = WebAppStudent.WesternUnionManager.SavePDFInstructionsToFile();
            this.Attachments = fileName;

            if (string.IsNullOrWhiteSpace(fileName))
            {
                KxLogger.LogActivity(this, "SavePDFInstructionsToFile", WebAppStudent.WesternUnionManager.LastMessage.ToString(), string.Empty);
            }

            var result = base.Execute();

            this.Attachments = string.Empty;
            return result;
        }

        #endregion

        #region Private Methods

        private string GetKCFEditorHelperResource(string resourceName)
        {
            return new EmbeddedResourceHelper().GetEmbeddedResource(string.Format("Kx.Core.Web.Student.WesternUnion.KCFEditor.{0}.txt", resourceName));
        }
        

        #endregion
    }
}