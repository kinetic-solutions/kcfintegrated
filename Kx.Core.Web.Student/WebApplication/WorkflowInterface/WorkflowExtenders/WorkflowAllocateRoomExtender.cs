﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Web;
using Kx.Core.Exceptions;
using Kx.Core.Web.WorkflowInterface.WorkflowExtenders;

namespace Kx.Core.Web.Student.WebApplication.WorkflowInterface.WorkflowExtenders
{
    public class WorkflowAllocateRoomExtender : KxStudentWorkflowExtender
    {
        public int ResidentialRoomID { get; set; }
        public bool ConfirmApplication { get; set; }

        public override WorkFlowStatus DetermineCompletionState()
        {
            WorkFlowStatus result = base.DetermineCompletionState();;

            if (result == WorkFlowStatus.Finished && workflowResult == "createallocation")
            {
                KxLogger.LogActivity(this, "DetermineCompletionState", "ResidentialRoomID", ResidentialRoomID.ToString());
                if (ResidentialRoomID > 0)
                {
                    if (ConfirmApplication)
                    {
                        WebUser.CurrentApplication.ConfirmApplication();
                        WebUser.Save();
                    }

                    HttpContext.Current.Application[String.Format("{0}:{1}", WebUser.KxStudentId, ResidentialRoomID)] =
                        "PENDING";
                    try
                    {
                        if (!WebApp.AllocationManager.CreateAllocation(ResidentialRoomID))
                        {
                            workflowResult = "error";
                        }
                    }
                    finally
                    {
                        HttpContext.Current.Application.Remove(String.Format("{0}:{1}", WebUser.KxStudentId,
                            ResidentialRoomID));
                    }
                }
            }

            return result;
        }
    }
}
