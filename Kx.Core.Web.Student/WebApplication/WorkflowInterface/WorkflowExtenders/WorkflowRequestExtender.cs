﻿using System;
using System.Collections.Generic;
using System.Text;
using Kx.Core.Web.WorkflowInterface.WorkflowExtenders;
using System.Web;

namespace Kx.Core.Web.Student.WebApplication.WorkflowInterface.WorkflowExtenders
{
    [Serializable]
    public class WorkflowRequestExtender : KxStudentWorkflowExtender
    {
        public string RequestTypeWebKey { get; set; }
        public bool StartNewRequest { get; set; }

        public override WorkFlowStatus DetermineCompletionState()
        {
            WorkFlowStatus state = base.DetermineCompletionState();

            if (state == WorkFlowStatus.Finished && workflowResult.ToLower() == "submit")
            {
                if (!SubmitRequest())
                {
                    state = WorkFlowStatus.Indeterminate;
                }
            }

            return state;
        }

        public override void LoadConfiguration(Workflow.WorkflowExternalAction externalaction)
        {
            base.LoadConfiguration(externalaction);

            if (StartNewRequest)
            {
                NewRequest(RequestTypeWebKey);
            }
        }

        public void NewRequest(string webKey)
        {
            WebApp.RequestManager.StartNewRequest(webKey);
        }

        private bool SubmitRequest()
        {
            return WebApp.RequestManager.SubmitRequest();
        }
    }
}
