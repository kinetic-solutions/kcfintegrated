using System;
using System.Collections.Generic;
using System.Text;
using Kx.Core.Exceptions;
using Kx.Core.Localisation;
using Kx.Core.Utility;
using Kx.Core.Web.Security;
using Kx.Core.Web.Student.WebApplication.ProcessManagers;
using Kx.Core.Web.WorkflowInterface.WorkflowExtenders;
using Kx.Objects.Student.Web;

namespace Kx.Core.Web.Student.WebApplication.WorkflowInterface.WorkflowExtenders
{
    [Serializable]
    public class WorkflowLDAPRegistrationExtender : WorkflowPasswordExtender
    {
        #region Attributes & Properties

        private bool useExtendedLdapAuthentication = false;
        
        protected bool stopBadLogin = false;

        public bool StopBadLogin
        {
            get
            {
                return stopBadLogin;
            }
            set
            {
                stopBadLogin = value;
            }
        }

        public bool UseExtendedLdapAuthentication
        {
            get
            {
                return useExtendedLdapAuthentication;
            }
            set
            {
                useExtendedLdapAuthentication = value;
            }
        }
  
        #endregion
        
        public override WorkFlowStatus DetermineCompletionState()
        {
            KxLogger.LogActivity("WorkflowLDAPRegistrationExtender", "DetermineCompletionState", "Started", "");

            WorkFlowStatus state = base.DetermineCompletionState();

            if (state == WorkFlowStatus.Finished)
            {
                if (workflowResult.ToUpper().IndexOf("LOGIN") > -1)
                {
                    if (WebApp.LdapManager.LoginUser(stopBadLogin, useExtendedLdapAuthentication))
                    {
                        WorkflowResult = WebApp.LdapManager.LdapStatus;
                        state = WorkFlowStatus.Finished;

                        KxLogger.LogActivity("WorkflowLDAPRegistrationExtender", "DetermineCompletionState", "Login Successful", "");
                    }
                    else
                    {
                        WebApp.AddStrike();
                        WebApp.PageRenderer.WriteErrorStatus(true, KxLocalisationManager.GetSubstitution("Login Failed"));
                        state = WorkFlowStatus.Indeterminate;

                        KxLogger.LogActivity("WorkflowLDAPRegistrationExtender", "DetermineCompletionState", "Login Failed", "");
                    }
                }
                else if (workflowResult.ToUpper().IndexOf("DOREGISTER") > -1)
                {
                   
                }
                else if (workflowResult.ToUpper().IndexOf("UPDATEUSER") > -1)
                {
                   
                }
            }

            KxLogger.LogActivity("WorkflowLDAPRegistrationExtender", "DetermineCompletionState", "Completed", "");

            return state;
        }

        public void Reset()
        {
            WebApp.LdapManager.UserName = "";
            WebApp.LdapManager.Password = "";
            WebApp.LdapManager.EmailAddress = "";
            WebApp.LdapManager.LdapStatus = "";
        }
    }
}
