using System;
using System.Collections.Generic;
using System.Text;
using Kx.Core.Localisation;
using Kx.Core.Web.WorkflowInterface.WorkflowExtenders;
using Kx.Core.Utility;
using Kx.Core.Exceptions;

namespace Kx.Core.Web.Student.WebApplication.WorkflowInterface.WorkflowExtenders
{
    public class WorkflowContractExtender : KxStudentEmailExtender
    {
        private bool requireUserLogon = false;
        private bool useActiveDirectoryAuthentication = false;
        private bool useLdapAuthentication = false;
        private bool useExtendedLdapAuthentication = false;
        private bool useStudentIdDOBAuthentication = false;
        private string logonError = "Invalid User Credentials";
        private bool sendEmail = false;

        private bool isContractPage = false;


        public bool IsContractPage
        {
            get
            { return isContractPage; }
            set
            { isContractPage = value; }
        }

        public string LogonErrorMsg
        {
            get
            { return logonError; }
            set
            { logonError = value; }
        }

        public bool RequireUserLogon
        {
            get
            { return requireUserLogon; }
            set
            { requireUserLogon = value; }
        }

        public bool UseActiveDirectoryAuthentication
        {
            get
            { return useActiveDirectoryAuthentication; }
            set
            { useActiveDirectoryAuthentication = value; }
        }

        public bool UseLdapAuthentication
        {
            get
            { return useLdapAuthentication; }
            set
            { useLdapAuthentication = value; }
        }

        public bool UseExtendedLdapAuthentication
        {
            get
            { return useExtendedLdapAuthentication; }
            set
            { useExtendedLdapAuthentication = value; }
        }

        public bool UseStudentIdDOBAuthentication
        {
            get
            {
                return useStudentIdDOBAuthentication;
            }
            set
            {
                useStudentIdDOBAuthentication = value;
            }
        }

        public bool SendEmail
        {
            get
            { return sendEmail; }
            set
            { sendEmail = value; }
        }

        public bool ValidateLogin()
        {

            return true;    

            bool result = false;

            if (useLdapAuthentication)
            {
                result = WebApp.ContractManager.ValidateLdapLogon(false);
            }
            else if (useExtendedLdapAuthentication)
            {
                result = WebApp.ContractManager.ValidateLdapLogon(true);
            }
            else if (useActiveDirectoryAuthentication)
            {
                result = WebApp.ContractManager.ValidateADLogon();
            }
            else if (UseStudentIdDOBAuthentication)
            {
                result = WebApp.ContractManager.ValidateSSOLogon();
            }
            else
            {
                result = WebApp.ContractManager.ValidateLogon();
            }

            return result;
        }

        public override WorkFlowStatus DetermineCompletionState()
        {
            WorkFlowStatus result = base.DetermineCompletionState();

            if (result == WorkFlowStatus.Finished && WorkflowResult.ToLower() == "accept")
            {
                if (RequireUserLogon)
                {
                    if (!ValidateLogin())
                    {
                        result = WorkFlowStatus.Indeterminate;
                        WebApp.PageRenderer.WriteErrorStatus(true , KxLocalisationManager.GetSubstitution(LogonErrorMsg));
                        WebApp.AddStrike();
                    }
                    else
                    {
                        WebApp.ContractManager.AcceptOffer();
                        if (sendEmail)
                        {
                            Execute();
                        }
                    }
                }
                else
                {
                    WebApp.ContractManager.AcceptOffer();
                    if (sendEmail)
                    {
                        Execute();
                    }
                }
            }

            if (result == WorkFlowStatus.Finished && WorkflowResult.ToLower() == "acceptlastallocation")
            {
                if (RequireUserLogon)
                {
                    if (!ValidateLogin())
                    {
                        result = WorkFlowStatus.Indeterminate;
                        WebApp.PageRenderer.WriteErrorStatus(true , KxLocalisationManager.GetSubstitution("Incorrect User Credentials"));
                        WebApp.AddStrike();
                    }
                    else
                    {
                        WebApp.ContractManager.AcceptLastAllocationOffer();
                        if (sendEmail)
                        {
                            Execute();
                        }
                    }
                }
                else
                {
                    WebApp.ContractManager.AcceptLastAllocationOffer();
                    if (sendEmail)
                    {
                        Execute();
                    }
                }
            }
            else if (result == WorkFlowStatus.Finished && WorkflowResult.ToLower() == "reject")
            {
                WebApp.ContractManager.RejectOffer();
                if (sendEmail)
                {
                    Execute();
                }
            }
            else if (result == WorkFlowStatus.Finished && WorkflowResult.ToLower() == "rejectlastallocation")
            {
                WebApp.ContractManager.RejectLastAllocationOffer();
                if (sendEmail)
                {
                    Execute();
                }
            }
            return result;
        }

        public override void AfterRender()
        {
            base.AfterRender();

            if (IsContractPage)
            {
                try
                {
                    try
                    {
                        WebUser.WriteStudentHistoryRecord("ViewedAllocation", String.Format("Student {0} {1} viewed their allocation online.", WebUser.Student.ForeName, WebUser.Student.SurName));
                    }
                    catch { }
                    if (WebApp.WorkflowInterface.WebUser.PendingAllocation != null)
                        WebApp.WorkflowInterface.WebUser.PendingAllocation.AcceptancePage = WebApp.PageRenderer.PageContent;
                    else
                    {
                        if (WebApp.WorkflowInterface.WebUser.AcceptedAllocation.AcceptancePage == null | WebApp.WorkflowInterface.WebUser.AcceptedAllocation.AcceptancePage == string.Empty)
                            WebApp.WorkflowInterface.WebUser.AcceptedAllocation.AcceptancePage = WebApp.PageRenderer.PageContent;
                        if (WebApp.WorkflowInterface.WebUser.AcceptedAllocation.AcceptanceDocument == null | WebApp.WorkflowInterface.WebUser.AcceptedAllocation.AcceptanceDocument == string.Empty)
                            WebApp.WorkflowInterface.WebUser.AcceptedAllocation.AcceptanceDocument = KxStringUtils.PageToDocument(WebApp.WorkflowInterface.WebUser.AcceptedAllocation.AcceptancePage);
                    }

                }
                catch (Exception ex)
                {
                    KxLogger.LogActivity(this, "ContractExtender", ex.Message, "IsContractPage");
                }
            }
        }
    }
}
