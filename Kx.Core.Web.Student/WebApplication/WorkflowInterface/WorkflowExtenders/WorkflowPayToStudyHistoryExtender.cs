#region copyright

// =======================================================
// Product:   KCF.Student
// Project:   Kx.Core.Web.Student
// File:      WorkflowPayToStudyExtender.cs
// Created:   11/11/2016
// By:        Steve Ashley
// Modified:  21/11/2016
// 
// Copyright � Kinetic Solutions Ltd 2016
// All rights reserved.
// 
// No portion of this file may be re-created, copied or otherwise
// used without the express permission of Kinetic Solutions Ltd.
// =======================================================

#endregion

#region usings

using System;
using System.Collections.Generic;
using System.Linq;
using Kx.Core.Exceptions;
using Kx.Core.Localisation;
using Kx.Core.Web.Configuration;
using Kx.Core.Web.Student.Classes;
using Kx.Core.Web.WorkflowInterface.WorkflowExtenders;
using Kx.Core.Workflow;
using Kx.Objects.Student.Web;
using Kx.Core.Web.Student.PayToStudy;

#endregion

namespace Kx.Core.Web.Student.WebApplication.WorkflowInterface.WorkflowExtenders
{
    public class WorkflowPayToStudyHistoryExtender : KxStudentWorkflowExtender, IKCFEditorExtenderHelper, IHaveSettingsForPayToStudy
    {
        #region Fields and Properties

        public string PTSUsername { get; set; }
        public string PTSPassword { get; set; }
        public string PaymentUrl { get; set; }

        public new KxWebApplicationStudent WebApp
        {
            get { return (KxWebApplicationStudent) PageRenderer.WebApp; }
        }

        public KxWebApplicationStudent WebAppStudent
        {
            get { return (KxWebApplicationStudent) PageRenderer.WebApp; }
        }

        public KxWebUserStudent WebUser
        {
            get { return (KxWebUserStudent) PageRenderer.WorkflowInterface.WebUser; }
        }

        #endregion

        #region Interface Methods

        public string DescriptiveHelp()
        {
            return new EmbeddedResourceHelper().GetEmbeddedResource("Kx.Core.Web.Student.PayToStudy.KCFEditor.PayToStudyHistoryExtenderHelpText.txt");
        }

        public string PageControls()
        {
            return new EmbeddedResourceHelper().GetEmbeddedResource("Kx.Core.Web.Student.PayToStudy.KCFEditor.PayToStudyHistoryExtenderPageControls.txt");
        }

        public IEnumerable<string> PageResources()
        {
            yield return "PayToStudyHistoryEN";
            yield return "PayToStudyHistoryRowEN";
        }

        public IEnumerable<string> EmailTemplates()
        {
            return new List<string>().AsEnumerable();
        }

        public string PageResource(string page)
        {
            return GetKCFEditorHelperResource(page);
        }

        public string EmailTemplate(string templateName)
        {
            return string.Empty;
        }

        #endregion

        #region Public Methods

        public override void BeforeLoad()
        {
            base.BeforeLoad();
            WebAppStudent.PayToStudyManager.GetPaymentHistoryFromPayToStudy(this);
        }

        #endregion

        #region Private Methods

        private string GetKCFEditorHelperResource(string resourceName)
        {
            return new EmbeddedResourceHelper().GetEmbeddedResource(string.Format("Kx.Core.Web.Student.PayToStudy.KCFEditor.{0}.txt", resourceName));
        }

        #endregion
    }
}