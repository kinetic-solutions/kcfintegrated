#region Copyright

// // =======================================================
// // 	Product:	KCF.Student
// // 	Project: 	Kx.Core.Web.Student
// // 	File:		WorkflowFriendshipGroupExtender.cs
// // 	Created:	20/08/2014
// // 	By:			Steve Ashley
// // 	Modified:	11/11/2014
// // 
// // 	Copyright � Kinetic Solutions Ltd 2014
// // 	All rights reserved.
// // 
// // 	No portion of this file may be re-created, copied or otherwise
// // 	used without the express permission of Kinetic Solutions Ltd.
// // =======================================================

#endregion

#region usings

using System;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using Kx.Core.Data;
using Kx.Core.Exceptions;
using Kx.Core.Localisation;
using Kx.Core.Merge;
using Kx.Core.Web.Configuration;
using Kx.Core.Web.PageRenderer.Utility;
using Kx.Core.Web.WorkflowInterface.WorkflowExtenders;
using Kx.Core.Workflow;
using Kx.Objects.Student.Web;
using System.Xml;

#endregion

namespace Kx.Core.Web.Student.WebApplication.WorkflowInterface.WorkflowExtenders
{
    internal class WorkflowFriendshipGroupEmailExtender : KxStudentEmailExtender
    {
        public string GroupMemberEmailTemplate { get; set; }
        public string GroupMemberEmailSubject { get; set; }

        public override string Execute()
        {
            string result = "go";
            try
            {
                if (WebUser.CurrentApplication.FriendshipGroup != null &&
                    WebUser.CurrentApplication.FriendshipGroup.FriendshipGroupId > 0 &&
                    WebUser.CurrentApplication.FriendshipGroup.ApplicationGuid == WebUser.CurrentApplication.ApplicationGuid)
                {
                    result = base.Execute();

                    foreach (KxWebFriendshipGroupMember member in
                        WebUser.CurrentApplication.FriendshipGroup.Members.Items.Where(s => s.GroupMemberId > 0))
                    {
                        if (KxConfigurationManager.Profiling)
                        {
                            KxLogger.LogActivity(this, "SendMail2", " email recipient", member.MemberEmailAddress);
                        }

                        bool isPlainText = (BodyFormat == "PlainText") ? true : false;
                        string tempSubject = subject;
                        SendMail(PageRenderer.ObjectTranslator.MergeTags(member.MemberEmailAddress), PageRenderer.ObjectTranslator.MergeTags(GroupMemberEmailSubject), PageRenderer.ObjectTranslator.MergeTags(senderemail), MergeMemberData(isPlainText, member), bodyFormat);
                    }
                }
            }
            catch (Exception e)
            {
                KxData data = new KxData();
                data.LogException("WorkflowFriendshipGroupEmailExtender", "Execute", e.Message, "Error sending emails to group members and leader");
            }
            return result;
        }
        private string MergeMemberData(bool isPlainText, KxWebFriendshipGroupMember member)
        {
            string defaultLang = System.Configuration.ConfigurationManager.AppSettings["DefaultLanguage"];
            string overrideLang = HttpContext.Current.Request["lang"];

            string Source = PageRenderer.ResourceManager.ResourceProvider.GetResource(PageRenderer.ObjectTranslator.MergeTags(GroupMemberEmailTemplate.Replace("-lang-", overrideLang ?? defaultLang ?? string.Empty)));
            if (string.IsNullOrWhiteSpace(Source))
            {
                KxLogger.LogActivity(this, "MergeData", "Data being merged from resource is empty", PageRenderer.ObjectTranslator.MergeTags(GroupMemberEmailTemplate.Replace("-lang-", overrideLang ?? defaultLang ?? string.Empty)));
                Source = string.Empty;
            }

            string result = PageRenderer.ObjectTranslator.MergeTags(Source);

            KxTemplateMerge merger = new KxTemplateMerge();
            merger.AddObject("Member", member);
            result = merger.MergeTemplate(result);

            return result;
        }
    }
}