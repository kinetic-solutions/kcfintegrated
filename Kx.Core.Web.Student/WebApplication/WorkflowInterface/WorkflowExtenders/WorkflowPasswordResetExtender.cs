using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Web;
using Kx.Core.Configuration;
using Kx.Core.Localisation;
using Kx.Core.Utility;
using Kx.Core.Web.Configuration;
using Kx.Core.Web.Student.WebApplication.ProcessManagers;
using Kx.Core.Web.WorkflowInterface.WorkflowExtenders;
using Kx.Objects.Student.Web;
using System.Drawing;

namespace Kx.Core.Web.Student.WebApplication.WorkflowInterface.WorkflowExtenders
{
    public class WorkflowPasswordResetExtender : WorkflowPasswordExtender
    {
        private bool foreNameRequired = false;
        private bool surNameRequired = false;
        private bool emailAddressRequired = false;
        private bool studentIdRequired = false;
        private bool uCASNumberRequired = false;
        private bool dobRequired = false;
        private bool genericIdRequired = false;
        private bool autoConfirmEmail = false;
        private bool uiAutoConfirmStatus = false;
        private bool byPassIfConfirmed = false;
        private bool uiAutoConfirmedAlready = false;
        private bool autoGoToLogin = false;
        private bool autoLoginOnConfirm = false;

        public bool AutoGoToLogin
        {
            get
            {
                return autoGoToLogin;
            }
            set
            {
                autoGoToLogin = value;
            }
        }
        public bool AutoConfirmEmail
        {
            get
            {
                return autoConfirmEmail;
            }
            set
            {
                autoConfirmEmail = value;
            }
        }
        public bool EmailAddressRequired
        {
            get
            {
                return emailAddressRequired;
            }
            set
            {
                emailAddressRequired = value;
            }
        }
        public bool DobRequired
        {
            get
            {
                return dobRequired;
            }
            set
            {
                dobRequired = value;
            }
        }
        public bool ForeNameRequired
        {
            get
            {
                return foreNameRequired;
            }
            set
            {
                foreNameRequired = value;
            }
        }
        public bool StudentIdRequired
        {
            get
            {
                return studentIdRequired;
            }
            set
            {
                studentIdRequired = value;
            }
        }
        public bool GenericIdRequired
        {
            get
            {
                return genericIdRequired;
            }
            set
            {
                genericIdRequired = value;
            }
        }
        public bool SurNameRequired
        {
            get
            {
                return surNameRequired;
            }
            set
            {
                surNameRequired = value;
            }
        }
        public bool UCASNumberRequired
        {
            get
            {
                return uCASNumberRequired;
            }
            set
            {
                uCASNumberRequired = value;
            }
        }
        public bool UiAutoConfirmStatus
        {
            get
            {
                return uiAutoConfirmStatus;
            }
            set
            {
                uiAutoConfirmStatus = value;
            }
        }
        public bool UiAutoConfirmedAlready
        {
            get
            {
                return uiAutoConfirmedAlready;
            }
            set
            {
                uiAutoConfirmedAlready = value;
            }
        }
        public bool ByPassIfConfirmed
        {
            get
            {
                return byPassIfConfirmed;
            }
            set
            {
                byPassIfConfirmed = value;
            }
        }

        public bool AutoLoginOnConfirm
        {
            get
            {
                return autoLoginOnConfirm;
            }
            set
            {
                autoLoginOnConfirm = value;
            }
        }


        public override void BeforeLoad()
        {
            base.BeforeLoad();

            if (HttpContext.Current.Request.QueryString["c"] != null)
            {
                WebApp.PasswordResetManager.UserConfirmationCode = HttpContext.Current.Request.QueryString["c"].ToString();
            }

            if (HttpContext.Current.Request.QueryString["c"] != null)
            {
                String result = WebApp.PasswordResetManager.ValidateConfirmationCodeVerbose();
                if (!String.IsNullOrEmpty(result))
                {
                    WebApp.PageRenderer.WriteErrorStatus(!String.IsNullOrEmpty(result), result);
                }
            }

        }

        public override void AfterLoad()
        {
            base.AfterLoad();
            if (byPassIfConfirmed && uiAutoConfirmStatus)
            {
                KxConfigurationManager.ResetSession(WebApp.CurrentPage);
            }
            manager = WebApp.PasswordResetManager;
        }

        public override WorkFlowStatus DetermineCompletionState()
        {
            WorkFlowStatus state = base.DetermineCompletionState();

            if (state == WorkFlowStatus.Finished)
            {
                if (workflowResult.ToUpper().IndexOf("SENDPWDRESETEMAIL") > -1)
                {
                    #region SENDPWDRESETEMAIL

                    #region Validate

                    string Errors = CheckControlsPopulated(WebApp.PasswordResetManager);

                    if (Errors != "")
                    {
                        state = WorkFlowStatus.Indeterminate;
                        WebApp.PageRenderer.WriteErrorStatus(!String.IsNullOrEmpty(Errors), Errors);

                        return state;
                    }

                    #endregion

                    KxWebUserStudent user = WebApp.PasswordResetManager.DoAccountMatching();

                    if (user != null)
                    {
                        if (WebApp.PasswordResetManager.EmailAddress != user.EmailAddress)
                        {
                            state = WorkFlowStatus.Indeterminate;
                            WebApp.PageRenderer.WriteErrorStatus(true, KxLocalisationManager.GetSubstitution("Unable to find a corresponding student with the email address provided."));
                        }
                        else
                        {
                            WebApp.PasswordResetManager.WebUserStudent = user;
                            SendMail(user.EmailAddress);
                        }
                    }
                    else
                    {
                        state = WorkFlowStatus.Indeterminate;
                        WebApp.PageRenderer.WriteErrorStatus(true, KxLocalisationManager.GetSubstitution("Unable to find a corresponding student"));
                    }

                    #endregion
                }
                else if (workflowResult.ToUpper().IndexOf("RESETME") > -1)
                {
                    #region RESETME

                    #region Validate

                    string Errors = VaildatePasswordReset();

                    if (Errors != "")
                    {
                        state = WorkFlowStatus.Indeterminate;
                        WebApp.PageRenderer.WriteErrorStatus(!String.IsNullOrEmpty(Errors), Errors);

                        return state;
                    }

                    #endregion

                    string result = WebApp.PasswordResetManager.ResetPassword();

                    if (result != "")
                    {
                        WebApp.PageRenderer.WriteErrorStatus(!String.IsNullOrEmpty(result), result);
                        state = WorkFlowStatus.Indeterminate;
                        WebApp.AddStrike();
                    }
                    else
                    {
                        KxWebUserShortUrl shortUrl = WebApp.PasswordResetManager.ShortUrl;
                        shortUrl.CodeUsed = true;
                        shortUrl.Changed = true;
                        shortUrl.Save();
                    }

                    #endregion
                }
            }

            return state;
        }

        public string CheckControlsPopulated(KxPasswordResetManager pwdMan)
        {
            string result = "";

            if (foreNameRequired && pwdMan.ForeName == "")
            {
                result += "\r\n" + KxLocalisationManager.GetSubstitution("Forename is required.");
            }

            if (surNameRequired && pwdMan.SurName == "")
            {
                result += "\r\n" + KxLocalisationManager.GetSubstitution("Surname is required.");
            }

            if (emailAddressRequired && pwdMan.EmailAddress == "")
            {
                result += "\r\n" + KxLocalisationManager.GetSubstitution("Email Address is required.");
            }

            if (studentIdRequired && pwdMan.StudentId == "")
            {
                result += "\r\n" + KxLocalisationManager.GetSubstitution("Student Id is required.");
            }

            if (genericIdRequired && pwdMan.GenericId == "")
            {
                result += "\r\n" + KxLocalisationManager.GetSubstitution("UCAS number / Student Id is required.");
            }

            if (uCASNumberRequired && pwdMan.UCASNumber == "")
            {
                result += "\r\n" + KxLocalisationManager.GetSubstitution("UCAS Number is required.");
            }

            if (dobRequired && !pwdMan.Dob.HasValue)
            {
                result += "\r\n" + KxLocalisationManager.GetSubstitution("Date of Birth is required.");
            }

            return result;
        }

        private string VaildatePasswordReset()
        {
            string error = string.Empty;

            error = WebApp.PasswordResetManager.ValidateConfirmationCodeVerbose();
            error = !(string.IsNullOrEmpty(error)) ? error += "\r\n" + CheckPassword() : CheckPassword();

            return error;
        }
    }
}
