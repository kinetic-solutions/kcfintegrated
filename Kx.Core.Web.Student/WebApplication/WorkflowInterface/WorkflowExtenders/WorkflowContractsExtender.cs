using System;
using System.Collections.Generic;
using System.Text;
using Kx.Core.Web.WorkflowInterface.WorkflowExtenders;
using System.Threading;
using Kx.Core.Utility;
using Kx.Core.Exceptions;


namespace Kx.Core.Web.Student.WebApplication.WorkflowInterface.WorkflowExtenders
{
    public class WorkflowContractsExtender : KxStudentWorkflowExtender
    {
        private bool isContractPage = false;

      
        public bool IsContractPage
        {
            get
            { return isContractPage; }
            set
            { isContractPage = value; }
        }

        public override WorkFlowStatus DetermineCompletionState()
        {           
            return base.DetermineCompletionState();
        }

        public override void AfterRender()
        {
            base.AfterRender();

            if (IsContractPage)
            {
                try
                {
                    try
                    {
                        WebUser.WriteStudentHistoryRecord("ViewedAllocation", String.Format("Student {0} {1} viewed their allocation online.", WebUser.Student.ForeName, WebUser.Student.SurName));
                    }
                    catch { }
                    if (WebApp.WorkflowInterface.WebUser.PendingAllocation != null)
                        WebApp.WorkflowInterface.WebUser.PendingAllocation.AcceptancePage = WebApp.PageRenderer.PageContent;
                    else
                    {
                        if (WebApp.WorkflowInterface.WebUser.AcceptedAllocation.AcceptancePage == null | WebApp.WorkflowInterface.WebUser.AcceptedAllocation.AcceptancePage == string.Empty)
                            WebApp.WorkflowInterface.WebUser.AcceptedAllocation.AcceptancePage = WebApp.PageRenderer.PageContent;
                        if (WebApp.WorkflowInterface.WebUser.AcceptedAllocation.AcceptanceDocument == null | WebApp.WorkflowInterface.WebUser.AcceptedAllocation.AcceptanceDocument == string.Empty)
                            WebApp.WorkflowInterface.WebUser.AcceptedAllocation.AcceptanceDocument = KxStringUtils.PageToDocument(WebApp.WorkflowInterface.WebUser.AcceptedAllocation.AcceptancePage);
                    }

                }
                catch (Exception ex)
                {
                    KxLogger.LogActivity(this, "ContractsExtender", ex.Message, "IsContractPage");
                }
            }
        }
    }
}
