using System;
using System.Collections.Generic;
using System.Text;
using Kx.Core.Exceptions;
using Kx.Core.Reflection;
using Kx.Core.Utility;
using Kx.Core.Web.Configuration;
using Kx.Core.Web.WebApplication;
using Kx.Core.Web.WorkflowInterface.WorkflowExtenders;
using Kx.Objects.Student;
using Kx.Objects.Student.Web;
using Kx.Core.Web.Student.WebApplication;
using Kx.Core.Web.Student.WebApplication.BaseClass;

namespace Kx.Core.Web.Student.WebApplication.WorkflowInterface.WorkflowExtenders
{
    [Serializable]
    public class KxStudentWorkflowExtender : KxWorkflowExtender
    {
        public new KxWebApplicationStudent WebApp
        {
            get
            { return (KxWebApplicationStudent)PageRenderer.WebApp; }
        }

        public KxWebApplicationStudent WebAppStudent
        {
            get
            { return (KxWebApplicationStudent)PageRenderer.WebApp; }
        }

        public KxWebUserStudent WebUser
        {
            get
            { return (KxWebUserStudent)PageRenderer.WorkflowInterface.WebUser; }
        }
    }
}
