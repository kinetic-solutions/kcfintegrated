using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using Kx.Core.Localisation;
using Kx.Core.Utility;
using Kx.Core.Web.Student.WebApplication.ProcessManagers;
using Kx.Core.Web.Security;
using Kx.Core.Web.PageRenderer.Utility;
using Kx.Core.Web.Student.WebApplication.PageRenderer;
using Kx.Objects.Student.Web;
using Kx.Core.Exceptions;
using Kx.Core.Web.WorkflowInterface.WorkflowExtenders;

namespace Kx.Core.Web.Student.WebApplication.WorkflowInterface.WorkflowExtenders
{
    public class WorkflowPrefSearchExtender : KxStudentWorkflowExtender
    {
        private int basketSize = 7;
        private int resultLimit = 25;
        private int minLimit = 0;
        private int minLimitOverride = 0;
        private int duplicateCustomValue3 = 10;
        private int duplicateAreas = 10;

        public int DuplicateCustomValue3
        {
            get
            {
                return duplicateCustomValue3;
            }
            set
            {
               duplicateCustomValue3 = value;
            }
        }

        public int DuplicateAreas
        {
            get
            {
                return duplicateAreas;
            }
            set
            {
                duplicateAreas = value;
            }
        }

        public int BasketSize
        {
            get
            {
                if (minLimitOverride == 0)
                {
                    return basketSize;
                }
                else
                {
                    return minLimitOverride;
                }
            }
            set
            {
                if (minLimitOverride == 0)
                {
                    basketSize = value;
                }
            }
        }

        public int ResultLimit
        {
            get
            { return resultLimit; }
            set
            { resultLimit = value; }
        }

        public int MinLimit
        {
            get
            {
                if (minLimitOverride == 0)
                {
                    return minLimit;
                }
                else
                {
                    return minLimitOverride;
                }
            }
            set
            {
                if (minLimitOverride == 0)
                {
                    minLimit = value;
                }
            }
        }

        public int MinLimitOverride
        {
            get
            {
                return minLimitOverride;
            }
            set
            {
                minLimitOverride = value;
                //BasketSize = value;
            }
        }



        public override void BeforeLoad()
        {
            base.BeforeLoad();
        }

        public override void AfterLoad()
        {
            WriteControlProperty("tblResultsTemplateTable", "Visible", true);
            WriteControlProperty("tblNoSearchResultsTable", "Visible", true);
            WriteControlProperty("tblBasketTemplateTable", "Visible", true);
            WriteControlProperty("tblEmptyBasketTable", "Visible", true);

            WorkFlowStatus state = base.DetermineCompletionState();

            if (workflowResult.ToUpper().IndexOf("DOSEARCH") > -1)
            {
                WebApp.PreferenceSearchManager.DoSearch(resultLimit);
            }

            PopulateSearchResults(WebApp.PreferenceSearchManager.Results, "tblResultsTable", "tblResultsTemplateTable", "tblNoSearchResultsTable", "_PS", "");
                
            if (workflowResult.ToUpper().IndexOf("ADDTOBASKET") > -1)
            {
                WebApp.PreferenceSearchManager.AddSelected(BasketSize,DuplicateAreas,DuplicateCustomValue3);
            }

            UpdateBasket();

            if (workflowResult.ToUpper().IndexOf("REMOVEBSKT") > -1)
            {
                if (WebApp.PreferenceSearchManager.RemoveFromBasket())
                {
                   UpdateBasket();
                }
            }
            else if (workflowResult.ToUpper().IndexOf("MOVEUP") > -1)
            {
                WebApp.PreferenceSearchManager.PrioritiseBasket(true);
                DeselectBasket();
            }
            else if (workflowResult.ToUpper().IndexOf("MOVEDN") > -1)
            {
                WebApp.PreferenceSearchManager.PrioritiseBasket(false);
                DeselectBasket();
            }
            WriteControlProperty("KX_BC_RXU_RVADDTOBASKET", "Visible", WebApp.PreferenceSearchManager.Results.Items.Count > 0 && WebApp.PreferenceSearchManager.Basket.Items.Count < BasketSize);
            WriteControlProperty("KX_BC_RXU_RVADDTOBASKET_X", "Visible", WebApp.PreferenceSearchManager.Results.Items.Count > 0 && WebApp.PreferenceSearchManager.Basket.Items.Count < BasketSize);

            bool EnableBasket = WebApp.PreferenceSearchManager.Basket.Items.Count > 0;

            if (workflowResult.ToUpper().IndexOf("DOSEARCH") == -1)
            {
               CheckHiddenCells(WebApp.PreferenceSearchManager.Results);
            }

            CheckHiddenCells(WebApp.PreferenceSearchManager.Basket);

            UpdateBasket();

            WriteControlProperty("KX_BC_RXU_RVREMOVEBSKT", "Visible", EnableBasket);
            WriteControlProperty("KX_BC_RXU_RVMOVEUP", "Visible", EnableBasket);
            WriteControlProperty("KX_BC_RXU_RVMOVEDN", "Visible", EnableBasket);
            WriteControlProperty("KX_BC_RXA_RVGOCONFIRM", "Visible", EnableBasket);

            WriteControlProperty("tblResultsTable", "Visible", WebApp.PreferenceSearchManager.HasSearched);

            WriteControlProperty("tblResultsTemplateTable", "Visible", false);
            WriteControlProperty("tblNoSearchResultsTable", "Visible", false);
            WriteControlProperty("tblBasketTemplateTable", "Visible", false);
            WriteControlProperty("tblEmptyBasketTable", "Visible", false);

            base.AfterLoad();
            WebApp.PageRenderer.WriteControlProperty("KX_BC_RXA_RVGOCONFIRM", "Enabled",
                WebApp.PreferenceSearchManager.Basket.Count >= MinLimit);

            if (WebApp.PreferenceSearchManager.AreasExceeded)
            {
                WebApp.PageRenderer.WriteControlProperty("KXDisplayLabel", "Text", KxLocalisationManager.GetSubstitution("Some preferences were not added, as the hall appears too many times in your basket (Max. " + DuplicateAreas.ToString() + ")"));
                WebApp.PreferenceSearchManager.AreasExceeded = false;
            }
            else if (WebApp.PreferenceSearchManager.AreaCustomValue3Exceeded)
            {
                WebApp.PageRenderer.WriteControlProperty("KXDisplayLabel", "Text", KxLocalisationManager.GetSubstitution("Some preferences were not added, as the bedroom type already exists for the same hall Max. " + DuplicateCustomValue3.ToString() + " per hall)"));
                WebApp.PreferenceSearchManager.AreaCustomValue3Exceeded = false;
            }
            else if (WebApp.PreferenceSearchManager.PreferenceExists)
            {
                WebApp.PageRenderer.WriteControlProperty("KXDisplayLabel", "Text", KxLocalisationManager.GetSubstitution("Some preferences were not added, as they are already in the basket"));
                WebApp.PreferenceSearchManager.PreferenceExists = false;
            }
            else if (WebApp.PreferenceSearchManager.BasketFull)
            {
                WebApp.PageRenderer.WriteControlProperty("KXDisplayLabel", "Text", KxLocalisationManager.GetSubstitution("Some preferences were not added as the basket is full."));
                WebApp.PreferenceSearchManager.BasketFull = false;
            }
            else if (WebApp.PreferenceSearchManager.Basket.Count < MinLimit)
            {
                WebApp.PageRenderer.WriteControlProperty("KXDisplayLabel", "Text", KxLocalisationManager.GetSubstitution("Basket must contain")+" " + MinLimit.ToString() + " "+KxLocalisationManager.GetSubstitution("preferences before you can confirm your application"));
            }
            else if (WebApp.PreferenceSearchManager.Basket.Count == BasketSize)
            {
                WebApp.PageRenderer.WriteControlProperty("KXDisplayLabel", "Text", KxLocalisationManager.GetSubstitution("If you wish to add another preference, you must remove one first."));
            }
        }

        public void CheckHiddenCells(KxWebStudentPreferences source)
        {
            foreach (KxWebStudentPreference pref in source.Items)
            {
                if (pref.UIObject != null)
                {
                    ((TableRow)pref.UIObject).Visible = pref.SelectionType != SelectedType.Hidden;
                }
            }
        }

        public void DeselectBasket()
        {
            foreach (KxWebStudentPreference pref in WebApp.PreferenceSearchManager.Basket.Items)
            {
                pref.SelectionType = SelectedType.Unselected;
            }
            UpdateBasket();
        }

        public void UpdateBasket()
        {
            PopulateSearchResults(WebApp.PreferenceSearchManager.Basket, "tblBasketTable", "tblBasketTemplateTable", "tblEmptyBasketTable", "_PB", "");
        }

        public void PopulateSearchResults(KxWebStudentPreferences source, string Sourcetable,string templatetable,string emptytable,string suffix,string errorLabel)
        {
            try
            {
                int idx = 0;
                Table tbl = (Table)KxPageUtility.FindControl(WebApp.CurrentPage, Sourcetable);
                Table template = (Table)KxPageUtility.FindControl(WebApp.CurrentPage, templatetable);
                Table emptytemplate = (Table)KxPageUtility.FindControl(WebApp.CurrentPage, emptytable);

                string items = "";

                if (tbl != null)
                {
                    KxPageUtility.EmptyTableExceptHeader(tbl,1);

                    if (source.Items.Count > 0)
                    {
                        foreach (KxWebStudentPreference pref in source.Items)
                        {
                            if (pref.SelectionType != SelectedType.Hidden)
                            {
                                if (pref.UIObject == null)
                                {
                                    try
                                    {
                                        items = pref.PreferenceId.ToString() + ",";
                                        pref.UIObject = KxPageUtility.CloneRow(tbl, template.Rows[0], suffix + pref.PreferenceId.ToString(), ref idx);
                                    }
                                    catch
                                    {

                                    }
                                }
                                else
                                {
                                    tbl.Rows.Add(((TableRow)pref.UIObject));
                                }
                            }
                        }

                        if (source.Items.Count >= resultLimit)
                        {
                            // Show warning label...
                        }

                        WebApp.PageRenderer.PopulateControlsList(tbl);

                        //KxLogger.LogActivity(this,"Writing items",items,"");
                    }
                    else
                    {
                        if (emptytemplate != null)
                        {
                            KxPageUtility.CloneRow(tbl, emptytemplate.Rows[0], "XX" + suffix, ref idx);
                        }
                    }
                }
            } 
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "PopulateSearchResults", ex.Message, "");
            }
        }

        public override WorkFlowStatus DetermineCompletionState()
        {
            WorkFlowStatus result = base.DetermineCompletionState();

            if (result == WorkFlowStatus.Finished)
            {
                WebApp.PreferenceSearchManager.SaveBasket();
                WebApp.PreferenceSearchManager.Basket.ClearUI();
            }
            return result;
        }

        public override void LoadConfiguration(Kx.Core.Workflow.WorkflowExternalAction externalaction)
        {
            // do nothing...
        }
    }
}
