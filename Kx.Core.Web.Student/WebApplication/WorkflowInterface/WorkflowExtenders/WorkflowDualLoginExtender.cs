using System;
using System.Collections.Generic;
using System.Text;
using Kx.Core.Exceptions;
using Kx.Core.Localisation;
using Kx.Core.Utility;
using Kx.Core.Web.Security;
using Kx.Core.Web.Student.WebApplication.ProcessManagers;
using Kx.Core.Web.WorkflowInterface.WorkflowExtenders;
using Kx.Objects.Student.Web;

namespace Kx.Core.Web.Student.WebApplication.WorkflowInterface.WorkflowExtenders
{
	[Serializable]
	public class WorkflowDualLoginExtender : WorkflowPasswordExtender
	{
		#region Attributes & Properties

		private bool useExtendedLdapAuthentication = false;

		protected bool stopBadLogin = true;

		public bool StopBadLogin
		{
			get
			{
				return stopBadLogin;
			}
			set
			{
				stopBadLogin = value;
			}
		}

		public bool UseExtendedLdapAuthentication
		{
			get
			{
				return useExtendedLdapAuthentication;
			}
			set
			{
				useExtendedLdapAuthentication = value;
			}
		}


		private bool requireValidatedEmail = false;
		private int loginAttempts = 3;
		private bool lockAccount = false;

		public bool RequireValidatedEmail
		{
			get
			{
				return requireValidatedEmail;
			}
			set
			{
				requireValidatedEmail = value;
			}
		}
		public int LoginAttempts
		{
			get
			{
				return loginAttempts;
			}
			set
			{
				loginAttempts = value;
			}
		}
		public bool LockAccount
		{
			get
			{
				return lockAccount;
			}
			set
			{
				lockAccount = value;
			}
		}

		#endregion

		public override WorkFlowStatus DetermineCompletionState()
		{
			KxLogger.LogActivity("WorkflowDualLoginExtender", "DetermineCompletionState", "Started", "");

			WorkFlowStatus state = base.DetermineCompletionState();

			if (state == WorkFlowStatus.Finished)
			{
				if (workflowResult.ToUpper().IndexOf("LOGIN") > -1)
				{
					WebApp.RegistrationManager.UserName = WebApp.LdapManager.UserName;
					WebApp.RegistrationManager.Password = WebApp.LdapManager.Password;
					WebApp.RegistrationManager.RetryLimit = loginAttempts;

					if (WebApp.LdapManager.LoginUser(stopBadLogin, useExtendedLdapAuthentication))
					{
						WorkflowResult = WebApp.LdapManager.LdapStatus;
						state = WorkFlowStatus.Finished;
						KxLogger.LogActivity("WorkflowDualLoginExtender", "DetermineCompletionState", "Login Successful - LDAP", "");
					}
					else if (WebApp.RegistrationManager.ValidateLogin())
					{					
						WebApp.RegistrationManager.LoginUser();
						KxLogger.LogActivity("WorkflowDualLoginExtender", "DetermineCompletionState", "Login Successful - WEBUSER", "");
					}
					else
					{
						WebApp.AddStrike();
						WebApp.PageRenderer.WriteErrorStatus(true, KxLocalisationManager.GetSubstitution("Login Failed"));
						state = WorkFlowStatus.Indeterminate;

						KxLogger.LogActivity("WorkflowDualLoginExtender", "DetermineCompletionState", "Login Failed", "");
					}
				}
			}

			KxLogger.LogActivity("WorkflowDualLoginExtender", "DetermineCompletionState", "Completed", "");

			return state;
		}

		public void Reset()
		{
			try
			{
                Kx.Core.Web.Providers.AuditProvider.ConfiguredAddToAudit("WorkflowDualLoginExtender", "Reset Start", "");
                
                if (WebApp.LdapManager != null)
				{
					WebApp.LdapManager.UserName = "";
					WebApp.LdapManager.Password = "";
					WebApp.LdapManager.EmailAddress = "";
					WebApp.LdapManager.LdapStatus = "";
                    
				}
				if (WebApp.RegistrationManager != null)
				{
					WebApp.RegistrationManager.Reset();
				}

				Kx.Core.Web.Providers.AuditProvider.ConfiguredAddToAudit("WorkflowDualLoginExtender", "Reset Finish", "");
			}
			catch
			{ }
		}
	}
}
