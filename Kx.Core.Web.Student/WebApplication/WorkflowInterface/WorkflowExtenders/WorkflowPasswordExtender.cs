using System;
using System.Collections.Generic;
using System.Text;
using Kx.Core.Utility;
using Kx.Core.Localisation;
using Kx.Core.Web.Student.WebApplication.ProcessManagers;
using Kx.Core.Web.Security;
using Kx.Objects.Student.Web;
using Kx.Core.Web.WorkflowInterface.WorkflowExtenders;
using Kx.Core.Web.Student.WebApplication.ProcessManagers.ManagerInterface;

namespace Kx.Core.Web.Student.WebApplication.WorkflowInterface.WorkflowExtenders
{
    public class WorkflowPasswordExtender : KxStudentEmailExtender
    {

        private int minPasswordLength = 8;
        private int minNumericChars = 0;
        private int minUpperChars = 0;
        private int minSpecialChars = 0;
        private int maxPasswordLength = 10;
        private bool mediumComplexityPassword = true;
        protected KxManagerInterface manager;

        public int MaxPasswordlength
        {
            get
            { return maxPasswordLength; }
            set
            { maxPasswordLength = value; }
        }
        public int MinPasswordLength
        {
            get
            { return minPasswordLength; }
            set
            { minPasswordLength = value; }
        }
        public int MinSpecialChars
        {
            get
            { return minSpecialChars; }
            set
            { minSpecialChars = value; }
        }
        public int MinUpperChars
        {
            get
            { return minUpperChars; }
            set
            { minUpperChars = value; }
        }
        public int MinNumericChars
        {
            get
            { return minNumericChars; }
            set
            { minNumericChars = value; }
        }
        public bool MediumComplexityPassword
        {
            get
            {
                return mediumComplexityPassword;
            }
            set
            {
                mediumComplexityPassword = value;
            }
        }
        protected KxManagerInterface Manager
        {
            get { return manager; }
            set { manager = value; }
        }

        protected string CheckPassword()
        {
            string result = "";

            if (Manager.Password.Length > MaxPasswordlength && MaxPasswordlength > 0)
            {
                result += "\r\n" + MaxPasswordlength.ToString() + " " + KxLocalisationManager.GetSubstitution("characters is the maximum permitted for a password, a shorter password is required. ");
            }
            if (Manager.Password.Length < MinPasswordLength && MinPasswordLength > 0)
            {
                result += "\r\n" + MinPasswordLength.ToString() + " " + KxLocalisationManager.GetSubstitution("characters is the minimum number permitted for a password, a longer password is required. ");
            }
            if (CountLowers() < 1)
            {
                result += "\r\n" + KxLocalisationManager.GetSubstitution("Password must contain at least one lowercase character. ");
            }
            if (MediumComplexityPassword)
            {
                if (CountNumerics() < 1 || CountUppers() < 1 || CountLowers() < 1)
                {
                    result += "\r\n" + KxLocalisationManager.GetSubstitution("Password must contain at least one uppercase, one lowercase and one numeric character. ");
                }
            }
            if (CountNumerics() < MinNumericChars)
            {
                result += "\r\n" + MinNumericChars.ToString() + " " + KxLocalisationManager.GetSubstitution("or more numeric characters required in a password. ");
            }
            if (CountUppers() < MinUpperChars)
            {
                result += "\r\n" + MinUpperChars.ToString() + " " + KxLocalisationManager.GetSubstitution("or more upper case characters required in a password. ");
            }
            if (CountSpecialChars() < MinSpecialChars)
            {
                result += "\r\n" + MinSpecialChars.ToString() + " " + KxLocalisationManager.GetSubstitution("or more non alpha-numeric characters required in a password. ");
            }
            return result;
        }

        private int CountNumerics()
        {
            int result = 0;

            foreach (char c in Manager.Password)
            {
                if (char.IsNumber(c))
                {
                    result++;
                }
            }
            return result;
        }

        private int CountUppers()
        {
            int result = 0;

            foreach (char c in Manager.Password)
            {
                if (char.IsUpper(c))
                {
                    result++;
                }
            }
            return result;
        }

        private int CountLowers()
        {
            int result = 0;

            foreach (char c in Manager.Password)
            {
                if (char.IsLower(c))
                {
                    result++;
                }
            }
            return result;
        }

        private int CountSpecialChars()
        {
            int result = 0;

            foreach (char c in Manager.Password)
            {
                if (!char.IsLetter(c) && !char.IsNumber(c))
                {
                    result++;
                }
            }
            return result;
        }

    }
}
