﻿using System;
using Kx.Core.Localisation;
using Kx.Core.Web.WorkflowInterface.WorkflowExtenders;

namespace Kx.Core.Web.Student.WebApplication.WorkflowInterface.WorkflowExtenders
{
    public class WorkflowSwanseaPayExtender : WorkflowPaymentExtender
    {
        private string uiPayerStreet1 = "";
        private string uiPayerStreet2 = "";
        private string uiPayerStreet3 = "";
        private string uiPayerCityTown = "";
        private string uiPayerCountry = "";
        private string uiPayerPostcode = "";
        private string uiPayerPhoneCountryCode = "";
        private string uiTelephone = "";
        private string uiEmail = "";

        private string ledgerCode = "";
        // Note NominalCode is already present in the Payment Extender so we don't need to provide it with a setup.
        private string systemId = "";

        public string UiPayerStreet1
        {
            get => uiPayerStreet1;
            set => uiPayerStreet1 = value;
        }

        public string UiPayerStreet2
        {
            get => uiPayerStreet2;
            set => uiPayerStreet2 = value;
        }

        public string UiPayerStreet3
        {
            get => uiPayerStreet3;
            set => uiPayerStreet3 = value;
        }

        public string UiPayerCityTown
        {
            get => uiPayerCityTown;
            set => uiPayerCityTown = value;
        }

        public string UiPayerCountry
        {
            get => uiPayerCountry;
            set => uiPayerCountry = value;
        }

        public string UiPayerPostcode
        {
            get => uiPayerPostcode;
            set => uiPayerPostcode = value;
        }

        public string UiPayerPhoneCountryCode
        {
            get => uiPayerPhoneCountryCode;
            set => uiPayerPhoneCountryCode = value;
        }

        public string UiTelephone
        {
            get => uiTelephone;
            set => uiTelephone = value;
        }

        public string UiEmail
        {
            get => uiEmail;
            set => uiEmail = value;
        }

        public string LedgerCode
        {
            get => ledgerCode;
            set => ledgerCode = value;
        }

        public string SystemId
        {
            get => systemId;
            set => systemId = value;
        }

        public override WorkFlowStatus DetermineCompletionState()
        {
            WorkFlowStatus result = base.DetermineCompletionState();

            try
            {
                if (result == WorkFlowStatus.Finished && WorkflowResult == "makepayment")
                {
                    var error = WebApp.ContractManager.PostWebServicePayment(this);

                    if (error == "")
                    {
                        result = WorkFlowStatus.Finished;
                    }
                    else
                    {
                        WebApp.PageRenderer.WriteErrorStatus(!string.IsNullOrEmpty(error), error);
                        result = WorkFlowStatus.Indeterminate;
                    }
                }
            }
            catch (Exception ex)
            {
                WebApp.PageRenderer.WriteErrorStatus(true, KxLocalisationManager.GetSubstitution("An unknown error occurred") + ": " + ex.Message);
                result = WorkFlowStatus.Indeterminate;
            }

            return result;
        }

        public override void MakePayment()
        {
            
        }
    }
}
