#region copyright

// =======================================================
// Product:   KCF.Student 2015.4
// Project:   Kx.Core.Web.Student
// File:      WorkflowFriendshipGroupExtender.cs
// Created:   08/07/2016
// By:        Steve Ashley
// Modified:  08/12/2016
// 
// Copyright � Kinetic Solutions Ltd 2016
// All rights reserved.
// 
// No portion of this file may be re-created, copied or otherwise
// used without the express permission of Kinetic Solutions Ltd.
// =======================================================

#endregion

#region usings

using System;
using System.Collections.Generic;
using System.Xml;
using Kx.Core.Data;
using Kx.Core.Localisation;
using Kx.Core.Web.WorkflowInterface.WorkflowExtenders;
using Kx.Objects.Student.Web;

#endregion

namespace Kx.Core.Web.Student.WebApplication.WorkflowInterface.WorkflowExtenders
{
    internal class WorkflowFriendshipGroupExtender : KxStudentWorkflowExtender
    {
        #region Fields and Properties

        private int _memberLimit;

        public int MemberLimit
        {
            get { return _memberLimit; }
            set
            {
                _memberLimit = value;
                WebUser.CurrentApplication.FriendshipGroup.Members.MemberLimit = _memberLimit;
            }
        }


        public string GroupCode { get; set; }
        public string ValidateEmailAddress { get; set; }

        #endregion

        #region Public Methods

        public override WorkFlowStatus DetermineCompletionState()
        {
            WorkFlowStatus state = base.DetermineCompletionState();

            if (state == WorkFlowStatus.Finished)
            {
                if (workflowResult.ToUpper().IndexOf("CREATE") > -1)
                {
                    state = SaveGroupAndMembers();
                }
                else if (workflowResult.ToUpper().IndexOf("INDIVIDUAL") > -1 && WebUser.CurrentApplication.FriendshipGroup.FriendshipGroupId > 0)
                {

                    for (int i = 0; i < WebUser.CurrentApplication.FriendshipGroup.Members.Items.Count; i++)
                    {
                        if (WebUser.CurrentApplication.FriendshipGroup.Members.Items[i].GroupMemberId > 0)
                        {
                            WebUser.CurrentApplication.FriendshipGroup.Members.Items[i].Delete();
                        }
                    }
                    WebUser.CurrentApplication.FriendshipGroup.Children.Clear();
                    WebUser.CurrentApplication.FriendshipGroup.Delete();
                    WebUser.CurrentApplication.FriendshipGroup = null;
                }
                else if (workflowResult.ToUpper().IndexOf("JOIN") > -1)
                {
                    KxWebFriendshipGroupMember validatedMember = null;

                    KxData data = new KxData("member", "kcf.ValidateJoinFriendshipGroup", TargetType.StoredProc, ActionType.ExecuteProcedure);
                    data.AddParameter("groupcode", GroupCode, KxValueType.String, 50);
                    data.AddParameter("validateemailaddress", ValidateEmailAddress, KxValueType.String, 255);
                    data.AddParameter("applicationguid", WebUser.CurrentApplication.ApplicationGuid, KxValueType.Guid);

                    if (data.Execute())
                    {
                        foreach (XmlNode node in data.GetNodesFromData())
                        {
                            validatedMember = new KxWebFriendshipGroupMember(null, node);
                            break;
                        }
                    }

                    if (validatedMember != null)
                    {
                        validatedMember.ApplicationGuid = WebUser.CurrentApplication.ApplicationGuid;
                        validatedMember.Save();
                    }
                    else
                    {
                        WebApp.PageRenderer.WriteErrorStatus(true, KxLocalisationManager.GetSubstitution("Group code not valid"));
                        state = WorkFlowStatus.Indeterminate;
                    }
                }
            }
            else if (state == WorkFlowStatus.Back)
            {
                WebApp.WorkflowInterface.WebUser.CurrentApplication.FriendshipGroup =
                    new KxWebFriendshipGroup(WebApp.WorkflowInterface.WebUser.CurrentApplication);
            }
            return state;
        }

        public WorkFlowStatus SaveGroupAndMembers()
        {
            WorkFlowStatus result = WorkFlowStatus.Finished;

            if (WebUser.CurrentApplication.FriendshipGroup.FriendshipGroupId < 1)
            {
                WebUser.CurrentApplication.FriendshipGroup.AcademicYearId = WebUser.CurrentApplication.AcademicYearId;
                WebUser.CurrentApplication.FriendshipGroup.ApplicationGuid = WebUser.CurrentApplication.ApplicationGuid;
                WebUser.CurrentApplication.FriendshipGroup.CreationDate = DateTime.Now;
                WebUser.CurrentApplication.FriendshipGroup.GroupLeaderUserGuid = WebUser.UserGuid;
                WebUser.CurrentApplication.FriendshipGroup.GroupName = Guid.NewGuid().ToString("N").Substring(1, 9);
            }

            List<KxWebFriendshipGroupMember> toRemove = new List<KxWebFriendshipGroupMember>();

            foreach (KxWebFriendshipGroupMember member in
                WebApp.WorkflowInterface.WebUser.CurrentApplication.FriendshipGroup.Members.Items)
            {
                if (member.GroupMemberId == 0 && !string.IsNullOrWhiteSpace(member.MemberEmailAddress))
                {
                    member.Temporary = false;
                    member.Changed = true;
                }
                if (member.GroupMemberId > 0 && string.IsNullOrWhiteSpace(member.MemberEmailAddress) && member.ApplicationGuid == Guid.Empty)
                {
                    member.Delete();
                }
            }


            WebUser.CurrentApplication.FriendshipGroup.Save();
            WebUser.CurrentApplication.FriendshipGroup = null;
            return result;
        }

        #endregion
    }
}