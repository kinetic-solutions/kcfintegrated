#region copyright

// =======================================================
// Product:   KCF.Student
// Project:   Kx.Core.Web.Student
// File:      KxHousingHandExtender.cs
// Created:   16/05/2017
// By:        Steve Ashley
// Modified:  16/05/2017
// 
// Copyright � Kinetic Solutions Ltd 2017
// All rights reserved.
// 
// No portion of this file may be re-created, copied or otherwise
// used without the express permission of Kinetic Solutions Ltd.
// =======================================================

#endregion

#region usings

using System;
using Kx.Core.Web.WorkflowInterface.WorkflowExtenders;
using Kx.Core.Workflow;
using Kx.Core.Web.Student.Classes;
using System.Collections.Generic;
using Kx.Core.Exceptions;

#endregion

namespace Kx.Core.Web.Student.WebApplication.WorkflowInterface.WorkflowExtenders
{
    [Serializable]
    public class WorkflowHousingHandExtender : KxStudentWorkflowExtender, IKCFEditorExtenderHelper
    {
        public string APIKey { get; set; }
        [DefaultExtenderValue(ValueString = "https://hh-api-staging-v2.herokuapp.com")]
        public string HousingHandURL { get; set; }
        public int C5UDFID { get; set; }
        public int CertificateURLUDFID { get; set; }
        public int QuoteReferenceUDFID { get; set; }

        public bool InitStudentName_Nationality_Phone_Email_DOB_OnLoad { get; set; }

        public string UniversityOrCollegeNameValue { get; set; }
        public string CourseValue { get; set; }
        public string CourseTypeValue { get; set; }
        public string YearOfStudyValue { get; set; }
        public string AccommodationProviderValue { get; set; }
        public string PropertyAddressValue { get; set; }
        public string PropertyCountyValue { get; set; }
        public string PropertyPostcodeValue { get; set; }
        public decimal WeeklyRentValue { get; set; }
        public DateTime TenancyStartDateValue { get; set; }
        public DateTime TenancyEndDateValue { get; set; }

        [DefaultExtenderValue(ValueString = "4.33")]
        public decimal PriceCalcMultiplier1 { get; set; }
        [DefaultExtenderValue(ValueString = "0.8")]
        public decimal PriceCalcMultiplier2 { get; set; }

        [DefaultExtenderValue(ValueString = "295.00")]
        public decimal MinimumPrice { get; set; }


        public override WorkFlowStatus DetermineCompletionState()
        {
            var result = base.DetermineCompletionState();

            if (result == WorkFlowStatus.Finished && StringComparer.InvariantCultureIgnoreCase.Compare(workflowResult, "submit") == 0)
            {
                var response = WebAppStudent.HousingHandManager.MakeRequestToHousingHand();
                if (!response.Successful)
                {
                    result = WorkFlowStatus.Indeterminate;
                    WebApp.PageRenderer.WriteErrorStatus(true, response.GetFullMessageOutput());
                }
            }

            return result;
        }

        public override void LoadConfiguration(WorkflowExternalAction externalaction)
        {
            base.LoadConfiguration(externalaction);

            WebAppStudent.HousingHandManager.APIKey = this.APIKey;
            WebAppStudent.HousingHandManager.HousingHandURL = this.HousingHandURL;
            WebAppStudent.HousingHandManager.C5UDFID = this.C5UDFID;
            WebAppStudent.HousingHandManager.CertificateURLUDFID = this.CertificateURLUDFID;
            WebAppStudent.HousingHandManager.QuoteReferenceUDFID = this.QuoteReferenceUDFID;

            if (this.PriceCalcMultiplier1 != 0m)
            {
                WebAppStudent.HousingHandManager.PriceCalcMultiplier1 = this.PriceCalcMultiplier1;
            }
            if (this.PriceCalcMultiplier2 != 0m)
            {
                WebAppStudent.HousingHandManager.PriceCalcMultiplier2 = this.PriceCalcMultiplier2;
            }
            if (this.MinimumPrice != 0m)
            {
                WebAppStudent.HousingHandManager.MinimumPrice = this.MinimumPrice;
            }

            InitOtherValuesInHousingHandManager();
        }

        private void InitOtherValuesInHousingHandManager()
        {
            if (this.InitStudentName_Nationality_Phone_Email_DOB_OnLoad)
            {
                WebAppStudent.HousingHandManager.InitDataFromStudent();
            }

            WebAppStudent.HousingHandManager.UniversityorCollegeName = InitManagerValue<string>(this.UniversityOrCollegeNameValue, WebAppStudent.HousingHandManager.UniversityorCollegeName);
            WebAppStudent.HousingHandManager.Course = InitManagerValue<string>(this.CourseValue, WebAppStudent.HousingHandManager.Course);
            WebAppStudent.HousingHandManager.CourseType = InitManagerValue<string>(this.CourseTypeValue, WebAppStudent.HousingHandManager.CourseType);
            WebAppStudent.HousingHandManager.YearOfStudy = InitManagerValue<string>(this.YearOfStudyValue, WebAppStudent.HousingHandManager.YearOfStudy);

            WebAppStudent.HousingHandManager.AccommodationProvider = InitManagerValue<string>(this.AccommodationProviderValue, WebAppStudent.HousingHandManager.AccommodationProvider);
            WebAppStudent.HousingHandManager.PropertyAddress = InitManagerValue<string>(this.PropertyAddressValue, WebAppStudent.HousingHandManager.PropertyAddress);
            WebAppStudent.HousingHandManager.PropertyCounty = InitManagerValue<string>(this.PropertyCountyValue, WebAppStudent.HousingHandManager.PropertyCounty);
            WebAppStudent.HousingHandManager.PropertyPostcode = InitManagerValue<string>(this.PropertyPostcodeValue, WebAppStudent.HousingHandManager.PropertyPostcode);
            WebAppStudent.HousingHandManager.WeeklyRent = InitManagerValue<decimal>(this.WeeklyRentValue, WebAppStudent.HousingHandManager.WeeklyRent);
            WebAppStudent.HousingHandManager.TenancyStartDate = InitManagerValue<DateTime>(this.TenancyStartDateValue, WebAppStudent.HousingHandManager.TenancyStartDate);
            WebAppStudent.HousingHandManager.TenancyEndDate = InitManagerValue<DateTime>(this.TenancyEndDateValue, WebAppStudent.HousingHandManager.TenancyEndDate);
        }

        private T InitManagerValue<T>(T extenderValue, T currentManagerValue)
        {
            if (extenderValue != null)
            {
                return extenderValue;
            }

            return currentManagerValue;
        }

        public string PageControls()
        {
            return new EmbeddedResourceHelper().GetEmbeddedResource("Kx.Core.Web.Student.HousingHand.KCFEditor.HousingHandPageControls.txt");
        }

        public IEnumerable<string> PageResources()
        {
            yield return string.Empty;
        }

        public IEnumerable<string> EmailTemplates()
        {
            yield return string.Empty;
        }

        public string PageResource(string page)
        {
            return string.Empty;
        }

        public string EmailTemplate(string templateName)
        {
            return string.Empty;
        }

        public string DescriptiveHelp()
        {
            return new EmbeddedResourceHelper().GetEmbeddedResource("Kx.Core.Web.Student.HousingHand.KCFEditor.HousingHandHelpText.txt");
        }
    }
}