﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using Kx.Core.Web.PageRenderer.Utility;
using Kx.Objects.Student.Web;

namespace Kx.Core.Web.Student.WebApplication.WorkflowInterface.WorkflowExtenders
{
    /// <summary>
    /// Utilises ASP.NET placeholders rather than tables for rendering preferences.
    /// Target ID should be "phPreferenceResults"
    /// Template ID should be "phPreferenceTemplate"
    /// Empty Template ID should be "phNoPreferenceTemplate"
    /// </summary>
    public class WorkflowPreferencePlaceholderExtender : WorkflowPreferenceExtender
    {
        public WorkflowPreferencePlaceholderExtender()
        {
            PreferenceResultsTableID = "phPreferenceResults";
            PreferenceResultsTemplateTableID = "phPreferenceTemplate";
            PreferenceEmptyTableID = "phNoPreferenceTemplate";
        }

        protected override void DisplayEmptyTemplate(string suffix, Control sourceControl, Control emptyTemplateControl, int idx)
        {
            KxPageUtility.ClonePlaceholder((PlaceHolder)emptyTemplateControl, (PlaceHolder)sourceControl, "XX" + suffix, null, null, idx);
        }

        protected override void AddControlFromUIObject(Control sourceControl, KxWebStudentPreference pref)
        {
            PlaceHolder target = sourceControl as PlaceHolder;

            target.Controls.Add(pref.UIObject as Control);
        }

        protected override int SetPreferenceUIObject(string suffix, KxWebStudentPreference pref, Control target, Control template, int idx)
        {
            pref.UIObject = KxPageUtility.ClonePlaceholder((PlaceHolder)template, (PlaceHolder)target, suffix + pref.PreferenceId.ToString(), null,
                null, idx);
            return idx;
        }

        protected override void ClearCurrentSourceControl(Control sourceControl)
        {
            PlaceHolder target = sourceControl as PlaceHolder;
            target.Controls.Clear();
        }
    }
}
