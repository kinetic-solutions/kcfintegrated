using System;
using System.Collections.Generic;
using System.Text;
using Kx.Core.Web.WorkflowInterface.WorkflowExtenders;

namespace Kx.Core.Web.Student.WebApplication.WorkflowInterface.WorkflowExtenders
{
    [Serializable]
    public class WorkflowPreferenceEditorExtender : KxStudentWorkflowExtender
    {
        public override WorkFlowStatus DetermineCompletionState()
        {
            WorkFlowStatus state = base.DetermineCompletionState();

            if (state == WorkFlowStatus.Finished)
            {
                if (WebApp.PageRenderer.UserAction.ReturnValue.ToUpper() == "DELETE")
                {
                    WebApp.ApplicationsManager.DeletePreference();
                }
                else
                {
                    WebApp.ApplicationsManager.SavePreference();
                }
            }
            else if (state == WorkFlowStatus.Back)
            {
                if (WebApp.PageRenderer.UserAction.ReturnValue.ToUpper() != "NODELETE")
                {
                    WebApp.ApplicationsManager.CancelPreference();
                }
            }
            return state;
        }
    }
}
