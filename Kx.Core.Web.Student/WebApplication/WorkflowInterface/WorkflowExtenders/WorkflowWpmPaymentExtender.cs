using System;
using System.Collections.Generic;
using System.Text;
using Kx.Core;
using Kx.Core.Exceptions;
using Kx.Core.PaymentProcessor;
using Kx.Core.PaymentProcessor.Payments;
using Kx.Core.Web.WorkflowInterface.WorkflowExtenders;
using Kx.Objects.Student.Web;

namespace Kx.Core.Web.Student.WebApplication.WorkflowInterface.WorkflowExtenders
{
    public class WorkflowWpmPaymentExtender : KxWpmWorkflowPaymentExtender
    {
        public new KxWebApplicationStudent WebApp
        {
            get
            { return (KxWebApplicationStudent)PageRenderer.WebApp; }
        }

        public KxWebApplicationStudent WebAppStudent
        {
            get
            { return (KxWebApplicationStudent)PageRenderer.WebApp; }
        }

        public KxWebUserStudent WebUser
        {
            get
            { return (KxWebUserStudent)PageRenderer.WorkflowInterface.WebUser; }
        }

        public override void MakePayment()
        {

            if (PaymentTypes.ToUpper() == "STATEMENT")
            {
                WebAppStudent.StatementManager.PostPayment(this);
            }
            else
            {
                WebAppStudent.ContractManager.PostPayment(this);
            }
        }

        public override WorkFlowStatus DetermineCompletionState()
        {
           return base.DetermineCompletionState();
        }

 
    }
}
