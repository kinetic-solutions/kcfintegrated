using System;
using System.Collections.Generic;
using System.Text;
using Kx.Core.Web.WorkflowInterface.WorkflowExtenders;


namespace Kx.Core.Web.Student.WebApplication.WorkflowInterface.WorkflowExtenders
{
    public class WorkflowAllocationExtender : KxStudentWorkflowExtender
    {
        protected bool useFirstPreference = false;
        protected bool checkInstalments = false;
        protected bool confirmApplication = false;

        public bool CheckInstalments
        {
            get
            {
                return checkInstalments;
            }
            set
            {
                checkInstalments = value;
                WebApp.AllocationManager.CheckInstalments = checkInstalments;
            }
        }

        public bool ConfirmApplication
        {
            get
            {
                return confirmApplication;
            }
            set
            {
                confirmApplication = value;
            }
        }

        public bool UseFirstPreference
        {
            get
            {
                return useFirstPreference;
            }
            set
            {
                useFirstPreference = value;
                WebApp.AllocationManager.UseFirstPreference = useFirstPreference;
            }
        }

        public override WorkFlowStatus DetermineCompletionState()
        {
            WorkFlowStatus result = base.DetermineCompletionState();

            if (result == WorkFlowStatus.Finished && WorkflowResult == "createallocation")
            {
                if (confirmApplication)
                {
                    WebUser.CurrentApplication.ConfirmApplication();
                    WebUser.Save();
                }

                if (!WebApp.AllocationManager.CreateAllocation())
                {
                    WebApp.PageRenderer.WriteControlProperty("KxAllocError", "Text", WebApp.AllocationManager.LastError);
                    WebApp.PageRenderer.WriteControlProperty("KxAllocError", "Visible", true);
                    result = WorkFlowStatus.Indeterminate;
                }
                else
                {
                    WebApp.PageRenderer.WriteControlProperty("KxAllocError", "Text", "");
                    WebApp.PageRenderer.WriteControlProperty("KxAllocError", "Visible", false);
                    WebUser.Allocations.Load();
                }
            }
            return result;
        }
    }
}
