using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using Kx.Core.Localisation;
using Kx.Core.Utility;
using Kx.Core.Web.Student.WebApplication.ProcessManagers;
using Kx.Core.Web.Security;
using Kx.Objects.Student.Web;
using Kx.Core.Web.WorkflowInterface.WorkflowExtenders;
using System.Net;
using Kx.Core.Web.Configuration;

namespace Kx.Core.Web.Student.WebApplication.WorkflowInterface.WorkflowExtenders
{
   
    [Serializable]
    public class WorkflowRegistrationExtender : WorkflowPasswordExtender
    {
        #region Attributes & Properties

  
        private bool emailUserName = false;
        private int  minUserNameLength = 5;
        private bool requireStudentRecord = false;
        private bool foreNameRequired = false;
        private bool surNameRequired = false;
        private bool emailAddressRequired = false;
        private bool studentIdRequired = true;
        private bool uCASNumberRequired = false;
        private bool genericIdRequired = false;
        private bool dobRequired = false;
        private bool defaultUserNameToStudentId = false;
        private bool ignoreStudentMatching = false;
        private bool ignorePasswordValidation = false;
        private bool captureMarketingCampaign = false;

        public bool IgnorePasswordValidation
        {
            get
            {
                return ignorePasswordValidation;
            }
            set
            {
                if (ignorePasswordValidation == value)
                    return;
                ignorePasswordValidation = value;
            }
        }

        public bool DefaultUserNameToStudentId
        {
            get
            {
                return defaultUserNameToStudentId;
            }
            set
            {
                if (defaultUserNameToStudentId == value)
                    return;
                defaultUserNameToStudentId = value;

                if (value)
                {
                    SetDefaultUserName();
                }
            }
        }
        public bool EmailAddressRequired
        {
            get
            {
                return emailAddressRequired;
            }
            set
            {
                if (emailAddressRequired == value)
                    return;
                emailAddressRequired = value;
            }
        }
        public bool   DobRequired
        {
            get
            {
                return dobRequired;
            }
            set
            {

                dobRequired = value;
            }
        }
        public bool   EmailUserName
        {
            get
            { return emailUserName; }
            set
            { 
                emailUserName = value;
                try
                {
                    WebApp.RegistrationManager.EmailUserName = emailUserName;
                }
                catch { }
            }
        }

        public bool GenericIdRequired
        {
            get
            { return genericIdRequired; }
            set
            { genericIdRequired = value; }
        }

        public bool   ForeNameRequired
        {
            get
            {
                return foreNameRequired;
            }
            set
            {
                if (foreNameRequired == value)
                    return;
                foreNameRequired = value;
            }
        }

        public int MinUserNameLength
        {
            get
            { return minUserNameLength; }
            set { minUserNameLength = value; }
        }
      
        public bool   RequireStudentRecord
        {
            get
            { return requireStudentRecord; }
            set
            { requireStudentRecord = value; }
        }
        public int    Retries
        {
            get
            {
                try
                {
                    return Convert.ToInt32(HttpContext.Current.Session["LoginAttempts"]);
                }
                catch
                { return 0; }
            }
        }
        public bool   StudentIdRequired
        {
            get
            {
                return studentIdRequired;
            }
            set
            {
                if (studentIdRequired == value)
                    return;
                studentIdRequired = value;
            }
        } 
        public bool   SurNameRequired
        {
            get
            {
                return surNameRequired;
            }
            set
            {
                if (surNameRequired == value)
                    return;
                surNameRequired = value;
            }
        }
        public bool   UCASNumberRequired
        {
            get
            {
                return uCASNumberRequired;
            }
            set
            {
                if (uCASNumberRequired == value)
                    return;
                uCASNumberRequired = value;
            }
        }

        public bool IgnoreStudentMatching
        {
            get
            {
                return ignoreStudentMatching;
            }
            set
            {
                ignoreStudentMatching = value;
            }
        }

        public bool AllowMultipleAccounts { get; set; }

		public Boolean CheckUsernameInAD { get; set; }

        public bool CaptureMarketingCampaign
        {
            get
            {
                return captureMarketingCampaign;
            }
            set
            {
                captureMarketingCampaign = value;
            }
        }

        #endregion
        public override void AfterLoad()
        {
            base.AfterLoad();
            Manager = WebApp.RegistrationManager;
        }

        public override WorkFlowStatus DetermineCompletionState()
        {
            WorkFlowStatus state = base.DetermineCompletionState();

            WebApp.RegistrationManager.CaptureMarketingCampaign = captureMarketingCampaign;

            if (workflowResult.ToUpper().IndexOf("GOLOGIN") > -1)
            {
                state = WorkFlowStatus.Finished;
            }
            else if (state == WorkFlowStatus.Finished)
            {
                if (workflowResult.ToUpper().IndexOf("DOREGISTER") > -1)
                {
                    if (!string.IsNullOrWhiteSpace(WebApp.RegistrationManager.SinglePageRegisterPassword) )
                    {
                        WebApp.RegistrationManager.Password = WebApp.RegistrationManager.SinglePageRegisterPassword;
                        WebApp.RegistrationManager.SinglePageRegisterPassword = null;
                    }

                    WebApp.RegistrationManager.MatchedStudentId = 0;

                    string Error = Validate();

                    if (Error != "")
                    {
                        WebApp.AddStrike(); 
                        state = WorkFlowStatus.Indeterminate;
                        WebApp.PageRenderer.WriteErrorStatus(!String.IsNullOrEmpty(Error), Error);
                        
                    }
                    else
                    {
//                        WebApp.RegistrationManager.CreateWebUser();

                        if (DefaultUserNameToStudentId && WebApp.RegistrationManager.MatchedStudent != null)
                        {
                            WebApp.RegistrationManager.UserName = WebApp.RegistrationManager.MatchedStudent.StudentId;   
                        }
                        else if (EmailUserName)
                        {
                            WebApp.RegistrationManager.UserName = WebApp.RegistrationManager.EmailAddress;
                        }

                        WebApp.RegistrationManager.CreateWebUser();
                    
                    }
                }
                else if (workflowResult.ToUpper().IndexOf("UPDATEUSER") > -1)
                {
                    string Error = Validate();

                    if (Error != "")
                    {
                        state = WorkFlowStatus.Indeterminate;
                        WebApp.PageRenderer.WriteErrorStatus(!String.IsNullOrEmpty(Error), Error);
                    }
                    else
                    {
                        WebApp.RegistrationManager.UpdateWebUser();
                    }
                }
                else if (workflowResult.ToUpper().IndexOf("UPDATEEMAIL") > -1)
                {
                    KxRegistrationManager regMan = WebApp.RegistrationManager;
                    string Error = "";

                    if (emailAddressRequired && regMan.EmailAddress == "")
                    {
                        Error += "\r\n" + KxLocalisationManager.GetSubstitution("Email Address is required.");
                    }

                    if (!String.IsNullOrEmpty(regMan.ConfirmEmailAddress) && regMan.EmailAddress != regMan.ConfirmEmailAddress)
                    {
                        Error += "\r\n" + KxLocalisationManager.GetSubstitution("Email addresses do not match.");
                    }


                    if (Error != "")
                    {
                        state = WorkFlowStatus.Indeterminate;
                        WebApp.PageRenderer.WriteErrorStatus(!String.IsNullOrEmpty(Error), Error);
                    }
                    else
                    {
                        WebUser.EmailAddress = regMan.EmailAddress;
                    }
                }
            }
            return state;
        }

        public string CheckControlsPopulated(KxRegistrationManager regMan)
        {
            string result = "";

            if (foreNameRequired && regMan.ForeName == "")
            {
                result += "\r\n"+KxLocalisationManager.GetSubstitution("ForeName is required.");
            }

            if (surNameRequired && regMan.SurName == "")
            {
                result += "\r\n"+KxLocalisationManager.GetSubstitution("SurName is required.");
            }

            if (emailAddressRequired && regMan.EmailAddress == "")
            {
                result += "\r\n"+KxLocalisationManager.GetSubstitution("Email Address is required.");
            }

            if (studentIdRequired && regMan.StudentId == "")
            {
                result += "\r\n"+KxLocalisationManager.GetSubstitution("Student Id is required.");
            }

            if (genericIdRequired && regMan.GenericId == "")
            {
                result += "\r\n"+KxLocalisationManager.GetSubstitution("Student Id is required.");
            }

            if (uCASNumberRequired && regMan.UCASNumber == "")
            {
                result += "\r\n"+KxLocalisationManager.GetSubstitution("UCAS Number is required.");
            }

            if (dobRequired && !regMan.Dob.HasValue)
            {
                result += "\r\n"+KxLocalisationManager.GetSubstitution("Date of Birth is required.");
            }

            return result;
        }

        public string DoSimpleValidation(KxRegistrationManager regMan)
        {
            string result = "";

            if (!IgnorePasswordValidation && regMan.Password != regMan.ConfirmPassword)
            {
                result += "\r\n"+KxLocalisationManager.GetSubstitution("Passwords do not match.");
            }

            if (!String.IsNullOrEmpty(regMan.ConfirmEmailAddress) && regMan.EmailAddress != regMan.ConfirmEmailAddress)
            {
                result += "\r\n"+KxLocalisationManager.GetSubstitution("Email addresses do not match.");
            }

            else if (regMan.Password != "")
            {
                result += CheckPassword();
            }

            if (emailUserName && !string.IsNullOrEmpty(regMan.EmailAddress))
            {
                regMan.UserName = regMan.EmailAddress;
            }

            if (!string.IsNullOrEmpty(regMan.UserName) && regMan.UserName.Length < minUserNameLength)
            {
                result += "\r\n"+KxLocalisationManager.GetSubstitution("User Name must be at least")+" " + minUserNameLength.ToString() + " "+KxLocalisationManager.GetSubstitution("characters long.");
            }

            return result;
        }

   

        public string DoComplexValidation(KxRegistrationManager regMan)
        {
            string result = "";

            if (regMan.UserName != "")
            {
                KxWebUserStudent user = new KxWebUserStudent();

                user.LoadUserName(regMan.UserName);

				//	Check user exists in web user table
				//	OR (if configured CheckUsernameInAD) check active directory for usename
				if (user.UserGuid != Guid.Empty || (CheckUsernameInAD && UsernameExistsOnAD(regMan.UserName)))
                {
                    result += "\r\n"+KxLocalisationManager.GetSubstitution("User Name already exists.");
                }
                else
                {
                    KxWebUserStudent user2 = new KxWebUserStudent();

                    user2.LoadEmailAddress(regMan.EmailAddress);

                    if (user.UserGuid != Guid.Empty)
                    {
                        result += "\r\n"+KxLocalisationManager.GetSubstitution("Email Address already exists.");
                    }
                }
            }

            if (!IgnoreStudentMatching)
            {
                regMan.DoStudentMatching();
            }
            

            if (requireStudentRecord && regMan.MatchedStudentId == 0)
            {
                result += "\r\n"+KxLocalisationManager.GetSubstitution("No student record available, please try later...");
            }
            else if (!IgnoreStudentMatching && regMan.MatchedStudentId != 0 && (regMan.DoesStudentHaveWebAccount(regMan.MatchedStudentId) && !AllowMultipleAccounts))
            {
                result += "\r\n"+KxLocalisationManager.GetSubstitution("A web account already exists for this user....");
            }

            return result;
        }


        private bool CanVerifyRecaptcha()
        {
            var recaptchaWorkflows = KxConfigurationManager.ReCaptchaWorkflows;
            if (!KxConfigurationManager.ReCaptchaRequired || string.IsNullOrEmpty(recaptchaWorkflows))
                return false;

            var workflowName = workflowResult.ToUpper();
            recaptchaWorkflows = recaptchaWorkflows.ToUpper();
            return recaptchaWorkflows.IndexOf(workflowName) > -1;
        }

        protected string VerifyReCaptcha(KxRegistrationManager regMan)
        {
            string url = "https://www.google.com/recaptcha/api/siteverify?secret=" + KxConfigurationManager.ReCaptchaPrivateKey + "&response=" + regMan.RecaptchaToken;
            var responseJson = (new WebClient()).DownloadString(url);

            dynamic response = Newtonsoft.Json.JsonConvert.DeserializeObject(responseJson);
            bool isValid = response.Value<bool>("success");

            string message = string.Empty;
            if (!isValid)
                message = KxLocalisationManager.GetSubstitution("Captcha required") ;

            return message;
        }

        protected string Validate()
        {
            KxRegistrationManager user = WebApp.RegistrationManager;
            string result = "";
            try
            {
                result = CheckControlsPopulated(user);

                if (result == "")
                {
                    result = DoSimpleValidation(user);
                }

                if (result =="")
                {
                    result = DoComplexValidation(user);
                }

                if (string.IsNullOrEmpty(result) && CanVerifyRecaptcha())
                {
                    result = VerifyReCaptcha(user);
                }

                if (result != string.Empty)
                {
                    WebApp.AddStrike();
                }

                return result;
            }
            catch (Exception ex)
            {
                WebApp.AddStrike(); 
                return ex.Message;

            }
        }

        /// <summary>
        /// Check Active Directory for an existing entry of username
        /// </summary>
        private bool UsernameExistsOnAD(String username)
		{
			return WebApp.LdapManager.Ldap.UserExists(username);
		}

        private void SetDefaultUserName()
        {
            if (WebUser.Student != null && !string.IsNullOrEmpty(WebUser.Student.StudentId))
            {
                WebApp.RegistrationManager.UserName = WebUser.Student.StudentId;
            }
        }
    }
}
