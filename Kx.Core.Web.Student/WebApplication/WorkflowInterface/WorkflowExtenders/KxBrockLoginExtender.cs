﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using Kx.Core.Web.Security;
using System.Collections.Specialized;
using Kx.Core.Data;
using Kx.Core.Exceptions;
using Kx.Core.Localisation;
using Kx.Core.Web.WorkflowInterface.WorkflowExtenders;
using Kx.Objects.Student;
using Kx.Objects.Student.Web;
using System.Web.Security;
using Kx.Core.Web.Configuration;

namespace Kx.Core.Web.Student.WebApplication.WorkflowInterface.WorkflowExtenders
{
    public class KxBrockLoginExtender : KxStudentWorkflowExtender
    {
        public bool HasCookie
        {
            get
            {
                return CheckValidCookie();
            }
        }

        public override string Execute()
        {
            return HasCookie ? "loginbrock" : "fail";
        }

        private bool CheckValidCookie()
        {
            string CookieName = System.Configuration.ConfigurationManager.AppSettings["BrockCookieName"];
            Kx.Core.Web.Providers.AuditProvider.ConfiguredAddToAudit("KxBrockLoginExtender", "CheckValidCookie", "Started. Cookie name: " + CookieName);

            HttpCookie BrockCookie = HttpContext.Current.Request.Cookies[CookieName]; // Cookie name will need changing to reflect the correct name.

            if (BrockCookie != null)
            {
                Kx.Core.Web.Providers.AuditProvider.ConfiguredAddToAudit("KxBrockLoginExtender", "CheckValidCookie", "Cookie found. Value: " + BrockCookie.Value);

                NameValueCollection cookieJar = KxBrockAuthentication.ValidateCookie(BrockCookie.Value);

                if (cookieJar != null)
                {
                    string StudentID = cookieJar["id"];
                    Kx.Core.Web.Providers.AuditProvider.ConfiguredAddToAudit("KxBrockLoginExtender", "CheckValidCookie", "StudentID: " + StudentID);

                    if (StudentID != "")
                    {
                        using (KxStudent student = new KxStudent(null, StudentID))
                        {
                            Kx.Core.Web.Providers.AuditProvider.ConfiguredAddToAudit("KxBrockLoginExtender", "CheckValidCookie", "KxStudentID: " + student.KxStudentId.ToString());

                            if (student.KxStudentId >= 0)
                            {
                                // Create the webuser if it doesn't exist.
                                KxWebUserStudent webUserStudent = new KxWebUserStudent();
                                webUserStudent.LoadStudent(student.KxStudentId);

                                // NOTE: The WebApp.RegistrationManager.DoesStudentHaveWebAccount 
                                //       method also checks email, which isn't valid for Brock.
                                if (!webUserStudent.IsValid() || (string.IsNullOrEmpty(webUserStudent.UserName)))
                                {
                                    CreateWebUser(student);
                                    UpdateAnyApplications();
                                }

                                WebApp.RegistrationManager.BrockLoginStudent(student.KxStudentId);
                            }
                            else
                            {
                                RedirectToFailure(System.Configuration.ConfigurationManager.AppSettings["WorkflowHomePageUrl"]);
                            }
                        }

                        HttpContext.Current.Session["SessionTimeout"] = null;
                        return true;
                    }
                }
            }

            return false;
        }

        private void CreateWebUser(KxStudent student)
        {
            KxWebUserStudent newWU = new KxWebUserStudent();
            newWU.ForeName = student.ForeName;
            newWU.SurName = student.SurName;
            newWU.UserName = student.StudentId;
            newWU.PersonId = student.PersonId;
            newWU.KxStudentId = student.KxStudentId;
            newWU.Password = student.StudentId;
            newWU.EmailAddress = student.EmailAddress;
            newWU.CreationDate = DateTime.Now;
            newWU.Active = 1;
            newWU.Person.WebAccessAllowed = true;
            newWU.Save();
        }

        private void UpdateAnyApplications()
        {
            KxData result = new KxData("", "WebProcessAllocations", TargetType.StoredProc, ActionType.ExecuteProcedure);
            result.Execute();
        }

        public void RedirectToFailure(string returnTarget)
        {
            string BrockFailureURL = string.Format(System.Configuration.ConfigurationManager.AppSettings["BrockFailure"], returnTarget);
            FormsAuthentication.SignOut();

            WebApp.WorkflowInterface.WebUser.LoadStudent(-1);
            KxConfigurationManager.ResetSession(WebApp.CurrentPage);
            HttpContext.Current.Response.Redirect(BrockFailureURL, false);

            KxConfigurationManager.ResetSession(WebApp.CurrentPage);
        }
    }
}
