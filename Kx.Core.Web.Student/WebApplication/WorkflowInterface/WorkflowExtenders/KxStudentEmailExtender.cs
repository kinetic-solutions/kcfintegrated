using System;
using System.Collections.Generic;
using System.Text;
using Kx.Objects.Student.Web;
using System.Net.Mail;
using Kx.Core.Web.Configuration;
using Kx.Core.Web.WorkflowInterface.WorkflowExtenders;

namespace Kx.Core.Web.Student.WebApplication.WorkflowInterface.WorkflowExtenders
{
    public class KxStudentEmailExtender : KxEmailExtender
    {
        
        public new KxWebApplicationStudent WebApp
        {
            get
            { return (KxWebApplicationStudent)PageRenderer.WebApp; }
        }

        public KxWebUserStudent WebUser
        {
            get
            { return (KxWebUserStudent)PageRenderer.WorkflowInterface.WebUser; }
        }


        public void LogEmailMessage(MailMessage msg, bool sendStatus)
        {
            if (sendStatus)
            {
                WebApp.ApplicationsManager.ConvertEmailToCorrespondence(msg);
            }
        }

        public override string Execute()
        {
            try
            {
                if (WebApp.WorkflowInterface.WebUser.Student != null && OnEmailTransmit == null)
                {
                    OnEmailTransmit += new Kx.Core.Email.EmailTransmitEvent(LogEmailMessage);
                }
            }
            catch { }

            return base.Execute();
        }
      
    }
}
