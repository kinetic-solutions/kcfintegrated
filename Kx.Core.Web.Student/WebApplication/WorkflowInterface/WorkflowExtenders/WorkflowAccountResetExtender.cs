using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Web;
using Kx.Core.Configuration;
using Kx.Core.Localisation;
using Kx.Core.Utility;
using Kx.Core.Web.Configuration;
using Kx.Core.Web.Student.WebApplication.ProcessManagers;
using Kx.Core.Web.WorkflowInterface.WorkflowExtenders;
using Kx.Objects.Student.Web;
using System.Drawing;

namespace Kx.Core.Web.Student.WebApplication.WorkflowInterface.WorkflowExtenders
{
    public class WorkflowAccountResetExtender : WorkflowPasswordExtender
    {
        private bool foreNameRequired = false;
        private bool surNameRequired = false;
        private bool emailAddressRequired = false;
        private bool studentIdRequired = false;
        private bool uCASNumberRequired = false;
        private bool dobRequired = false;
        private bool genericIdRequired = false;
        private bool autoConfirmEmail = false;
        private bool uiAutoConfirmStatus = false;
        private bool byPassIfConfirmed = false;
        private bool uiAutoConfirmedAlready = false;
        private bool autoGoToLogin = false;
        private bool autoLoginOnConfirm = false;

        public bool AutoGoToLogin
        {
            get
            {
                return autoGoToLogin;
            }
            set
            {
                autoGoToLogin = value;
            }
        }
        public bool AutoConfirmEmail
        {
            get
            {
                return autoConfirmEmail;
            }
            set
            {
                autoConfirmEmail = value;
            }
        }
        public bool EmailAddressRequired
        {
            get
            {
                return emailAddressRequired;
            }
            set
            {
                emailAddressRequired = value;
            }
        }
        public bool DobRequired
        {
            get
            {
                return dobRequired;
            }
            set
            {
                dobRequired = value;
            }
        }
        public bool ForeNameRequired
        {
            get
            {
                return foreNameRequired;
            }
            set
            {
                foreNameRequired = value;
            }
        }
        public bool StudentIdRequired
        {
            get
            {
                return studentIdRequired;
            }
            set
            {
                studentIdRequired = value;
            }
        }
        public bool GenericIdRequired
        {
            get
            {
                return genericIdRequired;
            }
            set
            {
                genericIdRequired = value;
            }
        }
        public bool SurNameRequired
        {
            get
            {
                return surNameRequired;
            }
            set
            {
                surNameRequired = value;
            }
        }
        public bool UCASNumberRequired
        {
            get
            {
                return uCASNumberRequired;
            }
            set
            {
                uCASNumberRequired = value;
            }
        }
        public bool UiAutoConfirmStatus
        {
            get
            {
                return uiAutoConfirmStatus;
            }
            set
            {
                uiAutoConfirmStatus = value;
            }
        }
        public bool UiAutoConfirmedAlready
        {
            get
            {
                return uiAutoConfirmedAlready;
            }
            set
            {
                uiAutoConfirmedAlready = value;
            }
        }
        public bool ByPassIfConfirmed
        {
            get
            {
                return byPassIfConfirmed;
            }
            set
            {
                byPassIfConfirmed = value;
            }
        }

        public bool AutoLoginOnConfirm
        {
            get
            {
                return autoLoginOnConfirm;
            }
            set
            {
            	autoLoginOnConfirm = value;
            }
        }


        public override void BeforeLoad()
        {
            base.BeforeLoad();

            if (HttpContext.Current.Request.QueryString["c"] != null)
            {
                WebApp.RegistrationManager.UserConfirmationCode = HttpContext.Current.Request.QueryString["c"].ToString();
            }

            if (AutoConfirmEmail && HttpContext.Current.Request.QueryString["c"] != null)
            {

                if (byPassIfConfirmed)
                {
                    String result = WebApp.RegistrationManager.ValidateConfirmationCodeAlreadyConfirmed();
                    if (result == "confirmed")
                    {
                        uiAutoConfirmStatus = true;
                        WebUser.Save();
                        try
                        {
                            string url = KxConfigurationManager.WorkflowHomePageUrl;

                            if (HttpContext.Current.Request.QueryString["culture"] != null)
                            {
                                url = url + "&culture=" + HttpContext.Current.Request.QueryString["culture"];
                            }

                            KxConfigurationManager.ResetSession(WebApp.CurrentPage);
                            HttpContext.Current.Response.Redirect(url);
                        }
                        catch
                        { }
                    }
                    else if (!String.IsNullOrEmpty(result))
                    {
                        uiAutoConfirmStatus = false;
                        WebApp.PageRenderer.WriteErrorStatus(result != "", result);
                    }
                    else
                    {
                        UiAutoConfirmStatus = true;
                        UiAutoConfirmedAlready = true;
                        WebUser.Save();
                        try
                        {
                            string url = KxConfigurationManager.WorkflowHomePageUrl;

                            KxConfigurationManager.ResetSession(WebApp.CurrentPage);
                            HttpContext.Current.Response.Redirect(url);
                        }
                        catch
                        { }
                    }
                }
                else 
                {
                    String result = WebApp.RegistrationManager.ValidateConfirmationCodeVerbose(false);
                    if (!String.IsNullOrEmpty(result))
                    {
                        uiAutoConfirmStatus = false;
                        WebApp.PageRenderer.WriteErrorStatus(!String.IsNullOrEmpty(result), result);
                    }
                    else
                    {
                        uiAutoConfirmStatus = true;
                        WebUser.Save();
                    }
                }
            }

        }

        public override void AfterLoad()
        {
           
            base.AfterLoad();
            if (byPassIfConfirmed && uiAutoConfirmStatus)
            {
                KxConfigurationManager.ResetSession(WebApp.CurrentPage);
            }
            manager = WebApp.RegistrationManager;
        }

        public override WorkFlowStatus DetermineCompletionState()
        {
            WorkFlowStatus state = base.DetermineCompletionState();

            if (state == WorkFlowStatus.Finished)
            {
                if (workflowResult.ToUpper().IndexOf("GOTOMAIN") > -1)
                {
                    #region GOTOMAIN

                    try
                    {
                        string url = KxConfigurationManager.WorkflowHomePageUrl;

                        KxConfigurationManager.ResetSession(WebApp.CurrentPage);
                        HttpContext.Current.Response.Redirect(url);
                    }
                    catch
                    { }

                    #endregion
                }
                else if (workflowResult.ToUpper().IndexOf("SENDME") > -1)
                {
                    #region SENDME

                    #region Validate

                    string Errors = CheckControlsPopulated(WebApp.RegistrationManager);

                    if (Errors != "")
                    {
                        state = WorkFlowStatus.Indeterminate;
                        WebApp.PageRenderer.WriteErrorStatus(!String.IsNullOrEmpty(Errors), Errors);

                        return state;
                    }

                    #endregion

                    KxWebUserStudent user = WebApp.RegistrationManager.DoAccountMatching();

                    if (user != null)
                    {
                        SendMail(user.EmailAddress);
                    }
                    else
                    {
                        state = WorkFlowStatus.Indeterminate;
                        WebApp.PageRenderer.WriteErrorStatus(true, KxLocalisationManager.GetSubstitution("Unable to find a corresponding student"));
                    }

                    #endregion
                }
                else if (workflowResult.ToUpper().IndexOf("CONFIRMME2") > -1)
                {
                    #region CONFIRMME2

                    string result = WebApp.RegistrationManager.ValidateConfirmationCodeVerbose(true);

                    if (!String.IsNullOrEmpty(result))
                    {
                        WebApp.PageRenderer.WriteErrorStatus(!String.IsNullOrEmpty(result), result);
                        state = WorkFlowStatus.Indeterminate;
                        WebApp.AddStrike();
                    }
                    else if (AutoGoToLogin)
                    {
                        string url = KxConfigurationManager.WorkflowHomePageUrl;

                        KxConfigurationManager.ResetSession(WebApp.CurrentPage);
                        HttpContext.Current.Response.Redirect(url);
                    }

                    #endregion
                }
                else if (workflowResult.ToUpper().IndexOf("CONFIRMME") > -1)
                {
                    #region CONFIRMME

                    string result = WebApp.RegistrationManager.ValidateConfirmationCodeVerbose(false);

                    if (!String.IsNullOrEmpty(result))
                    {
                        WebApp.PageRenderer.WriteErrorStatus(!String.IsNullOrEmpty(result), result);
                        state = WorkFlowStatus.Indeterminate;
                        WebApp.AddStrike();
                    }
                    else if (AutoGoToLogin)
                    {
                        string url = KxConfigurationManager.WorkflowHomePageUrl;

                        KxConfigurationManager.ResetSession(WebApp.CurrentPage);
                        HttpContext.Current.Response.Redirect(url);
                    }
                    else if (AutoLoginOnConfirm)
                    {
                        KxWebUserShortUrl url = new KxWebUserShortUrl(null, this.WebApp.RegistrationManager.UserConfirmationCode);
                        if (url.UserGuid != Guid.Empty)
                        {
                            KxWebUserStudent user = new KxWebUserStudent(null, url.UserGuid);
                            if (user.Active == 1)
                            {
                                PageRenderer.WorkflowInterface.CurrentWorkflow.GetObjectByID("webuser").InternalObject = user;
                                PageRenderer.WorkflowInterface.CurrentWorkflow.KeyObjectIdentifier = user.UserGuid;
                            }
                        }
                    }

                    #endregion
                }
				else if (workflowResult.ToUpper().IndexOf("RESETME2") > -1)
                {
                    #region RESETME2

                    #region Validate

                    string Errors = CheckControlsPopulated(WebApp.RegistrationManager);

                    if (Errors != "")
                    {
                        state = WorkFlowStatus.Indeterminate;
                        WebApp.PageRenderer.WriteErrorStatus(!String.IsNullOrEmpty(Errors), Errors);

                        return state;
                    }

                    #endregion

                    string result = WebApp.RegistrationManager.ResetPassword2();

					if (result != "")
					{
						WebApp.PageRenderer.WriteErrorStatus(!String.IsNullOrEmpty(result), result);
						state = WorkFlowStatus.Indeterminate;
						WebApp.AddStrike();
					}

                    #endregion
				}
				else if (workflowResult.ToUpper().IndexOf("RESETME") > -1)
                {
                    #region RESETME

                    #region Validate

                    string Errors = CheckControlsPopulated(WebApp.RegistrationManager);

                    if (Errors != "")
                    {
                        state = WorkFlowStatus.Indeterminate;
                        WebApp.PageRenderer.WriteErrorStatus(!String.IsNullOrEmpty(Errors), Errors);

                        return state;
                    }

                    #endregion

                    string result = WebApp.RegistrationManager.ResetPassword();

                    if (result != "")
                    {
                        WebApp.PageRenderer.WriteErrorStatus(!String.IsNullOrEmpty(result), result);
                        state = WorkFlowStatus.Indeterminate;
                        WebApp.AddStrike();
                    }

                    #endregion
                }
			}

            return state;
        }

        public string CheckControlsPopulated(KxRegistrationManager regMan)
        {
            string result = "";

            if (foreNameRequired && regMan.ForeName == "")
            {
                result += "\r\n"+KxLocalisationManager.GetSubstitution("Forename is required.");
            }

            if (surNameRequired && regMan.SurName == "")
            {
                result += "\r\n"+KxLocalisationManager.GetSubstitution("Surname is required.");
            }

            if (emailAddressRequired && regMan.EmailAddress == "")
            {
                result += "\r\n"+KxLocalisationManager.GetSubstitution("Email Address is required.");
            }

            if (studentIdRequired && regMan.StudentId == "")
            {
                result += "\r\n"+KxLocalisationManager.GetSubstitution("Student Id is required.");
            }

            if (genericIdRequired && regMan.GenericId == "")
            {
                result += "\r\n"+KxLocalisationManager.GetSubstitution("UCAS number / Student Id is required.");
            }

            if (uCASNumberRequired && regMan.UCASNumber == "")
            {
                result += "\r\n"+KxLocalisationManager.GetSubstitution("UCAS Number is required.");
            }

            if (dobRequired && !regMan.Dob.HasValue)
            {
                result += "\r\n"+KxLocalisationManager.GetSubstitution("Date of Birth is required.");
            }

            result += CheckPassword();

            return result;
        }
    }
}
