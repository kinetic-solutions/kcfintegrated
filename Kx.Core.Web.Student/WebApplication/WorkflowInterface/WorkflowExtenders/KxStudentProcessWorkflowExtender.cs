using System;
using System.Collections.Generic;
using System.Text;
using Kx.Core.Exceptions;
using Kx.Core.Reflection;
using Kx.Core.Resources;
using Kx.Core.Utility;
using Kx.Core.Web.Configuration;
using Kx.Core.Web.WebApplication;
using Kx.Core.Web.PageRenderer.Utility;
using Kx.Core.Web.WorkflowInterface.WorkflowExtenders;
using Kx.Objects.Common;
using Kx.Objects.Student;
using Kx.Objects.Student.Web;
using Kx.Core.Web.Student.WebApplication;
using Kx.Core.Web.Student.WebApplication.BaseClass;

namespace Kx.Core.Web.Student.WebApplication.WorkflowInterface.WorkflowExtenders
{
    [Serializable]
    public class KxStudentProcessWorkflowExtender : KxStudentWorkflowExtender
    {
        protected string uiResourceName = "";
        protected string uiCurrentPricing = "";
        

        public string UiResourceName
        {
            get
            {
                return uiResourceName;
            }
            set
            {
                uiResourceName = value;
            }
        }

        public string UiCurrentPricing
        {
            get
            {
                return uiCurrentPricing;
            }
            set
            {
                uiCurrentPricing = value;
            }
        }

        public override void BeforeLoad()
        {
            base.BeforeLoad();
        }

        public override WorkFlowStatus DetermineCompletionState()
        {
            WorkFlowStatus result = base.DetermineCompletionState();

            if (workflowResult.ToUpper().IndexOf("ADDRESOURCE") > -1 && KxPageUtility.IsLocal)
            {
                if (PageRenderer.ResourceManager.ResourceProvider.AddResource(UiResourceName, ""))
                {
                    WebUser.UserWebResource = UiResourceName; 
                    UiResourceName = "";
                    
                    result = WorkFlowStatus.Finished;
                }
                else
                {
                    result = WorkFlowStatus.Indeterminate;
                }
            } 
            else if (workflowResult.ToUpper().IndexOf("SAVERESOURCE") > -1 && KxPageUtility.IsLocal)
            {
                KxWebResource resrc = PageRenderer.ResourceManager.ResourceProvider.GetResourceObject(WebUser.CurrentWebResource);
                if (resrc != null)
                {
                    resrc.Save();
                }
                WebUser.UserWebResource = "";

                result = WorkFlowStatus.Finished;
            }

            else if (workflowResult.ToUpper().IndexOf("ADDALLOC") > -1 && KxPageUtility.IsLocal)
            {
                WebUser.EditAllocationPricing =  KxStudentObjectManager.WebAllocationPricingList.AddAllocationPricing();

                result = WorkFlowStatus.Finished;
            }
            else if (workflowResult.ToUpper().IndexOf("SAVEALLOC") > -1 && KxPageUtility.IsLocal)
            {
                if (result == WorkFlowStatus.Finished)
                {
                    KxWebAllocationPricing pricing = WebUser.EditAllocationPricing;

                    if (pricing != null)
                    {
                        if (pricing.Save())
                        {
                            WebUser.EditAllocationPricing = null;
                            result = WorkFlowStatus.Finished;
                        }
                        else
                        {
                            WebApp.PageRenderer.WriteErrorStatus(true, pricing.LastError);
                            result = WorkFlowStatus.Indeterminate;

                        }
                    }
                    else
                    {
                        WebApp.PageRenderer.WriteErrorStatus(true, "An error has occurred, please go back and try again.");
                        result = WorkFlowStatus.Indeterminate;
                    }

                    WebUser.UserAllocationPricing = "";
                }
            }
            else if (workflowResult.ToUpper().IndexOf("SAVEOPALALLOCLIST") > -1 && KxPageUtility.IsLocal)
            {
                KxStudentObjectManager.WebOpalAllocationPricingList.SaveAllocationPricingModels();

                result = WorkFlowStatus.Finished;
            }
            else if (workflowResult.ToUpper().IndexOf("ADDOPALALLOC") > -1 && KxPageUtility.IsLocal)
            {
                WebUser.EditOpalAllocationPricing = KxStudentObjectManager.WebOpalAllocationPricingList.AddAllocationPricing();
                

                result = WorkFlowStatus.Finished;
            }
            else if (workflowResult.ToUpper().IndexOf("SAVEOPALALLOC") > -1 && KxPageUtility.IsLocal)
            {
                if (result == WorkFlowStatus.Finished)
                {
                    KxWebOpalAllocationPricing pricing = WebUser.EditOpalAllocationPricing;

                    if (pricing != null)
                    {
                        if (pricing.Save())
                        {
                            WebUser.EditOpalAllocationPricing = null;
                            result = WorkFlowStatus.Finished;
                        }
                        else
                        {
                            WebApp.PageRenderer.WriteErrorStatus(true, pricing.LastError);
                            result = WorkFlowStatus.Indeterminate;

                        }
                    }
                    else
                    {
                        WebApp.PageRenderer.WriteErrorStatus(true, "An error has occurred, please go back and try again.");
                        result = WorkFlowStatus.Indeterminate;
                    }

                    WebUser.UserAllocationPricing = "";
                }
            }
            else if (workflowResult.ToUpper().IndexOf("SAVEBOOKINGRULES") > -1 && KxPageUtility.IsLocal)
            {
                KxStudentObjectManager.BookingEligibilityRuleList.SaveBookingEligibilityRules(CurrentPage.Session["ADMNLOGIN"].ToString());

                result = WorkFlowStatus.Finished;

            }
            else if (workflowResult.ToUpper().IndexOf("ADDBOOKINGRULE") > -1 && KxPageUtility.IsLocal)
            {
                WebUser.EditBookingEligibilityRule = KxStudentObjectManager.BookingEligibilityRuleList.AddBookingEligibilityRule();
                WebUser.EditBookingEligibilityRule.LastUpdatedByUser = CurrentPage.Session["ADMNLOGIN"].ToString();

                result = WorkFlowStatus.Finished;
            }
            else if (workflowResult.ToUpper().IndexOf("CANCELNEWRULE") > -1 && KxPageUtility.IsLocal)
            {
                KxStudentObjectManager.BookingEligibilityRuleList.RemoveBookingEligibilityRule(WebUser.EditBookingEligibilityRule);

                result = WorkFlowStatus.Finished;
            }
            else if (workflowResult.ToUpper().IndexOf("SAVENEWRULE") > -1 && KxPageUtility.IsLocal)
            {
                if (result == WorkFlowStatus.Finished)
                {
                    KxBookingEligibilityRule rule = WebUser.EditBookingEligibilityRule;


                    if (rule != null)
                    {
                        if (KxStudentObjectManager.BookingEligibilityRuleList.RuleIsUnique(rule))
                        {
                            if (rule.Save())
                            {
                                WebUser.EditBookingEligibilityRule = null;
                                result = WorkFlowStatus.Finished;
                            }
                            else
                            {
                                WebApp.PageRenderer.WriteErrorStatus(true, rule.LastError);
                                result = WorkFlowStatus.Indeterminate;

                            }
                        }
                        else
                        {
                            WebApp.PageRenderer.WriteErrorStatus(true, "This rule already exists. Please update the existing rule.");
                            result = WorkFlowStatus.Indeterminate;
                        }



                    }
                    else
                    {
                        WebApp.PageRenderer.WriteErrorStatus(true, "An error has occurred, please go back and try again.");
                        result = WorkFlowStatus.Indeterminate;
                    }


                }
            }
            else if (workflowResult.ToUpper().IndexOf("ADDSITEAVAIL") > -1 && KxPageUtility.IsLocal)
            {
                WebUser.EditSiteAvailabilityLookup = KxStudentObjectManager.SiteAvailabilityLookups.AddSiteAvailability();

                result = WorkFlowStatus.Finished;
            }
            else if (workflowResult.ToUpper().IndexOf("SAVESITEAVAIL") > -1 && KxPageUtility.IsLocal)
            {
                if (result == WorkFlowStatus.Finished)
                {
                    KxSiteAvailabilityLookup lookup = WebUser.EditSiteAvailabilityLookup;

                    if (lookup != null)
                    {
                        bool insert = !lookup.Loaded;
                        if (lookup.Save())
                        {
                            WebUser.EditSiteAvailabilityLookup = null;
                            result = WorkFlowStatus.Finished;
                            if (insert)
                            {
                                KxStudentObjectManager.SiteAvailabilityLookups.AllSiteLookups = null;
                                KxStudentObjectManager.SiteAvailabilityLookups.Load();
                            }
                        }
                        else
                        {
                            WebApp.PageRenderer.WriteErrorStatus(true, lookup.LastError);
                            result = WorkFlowStatus.Indeterminate;

                        }
                    }
                    else
                    {
                        WebApp.PageRenderer.WriteErrorStatus(true, "An error has occurred, please go back and try again.");
                        result = WorkFlowStatus.Indeterminate;
                    }

                    WebUser.UserSiteLookupId = "";

                }
            }

            else if (workflowResult.ToUpper().IndexOf("ADDDUEDATE") > -1 && KxPageUtility.IsLocal)
            {
                WebUser.EditAdjustedDueDate = KxStudentObjectManager.AdjustedDueDates.AddDueDate();

                result = WorkFlowStatus.Finished;
            }
            else if (workflowResult.ToUpper().IndexOf("SAVEDUEDATE") > -1 && KxPageUtility.IsLocal)
            {
                if (result == WorkFlowStatus.Finished)
                {
                    KxWebAdjustedDueDate due = WebUser.EditAdjustedDueDate;

                    if (due != null)
                    {

                        if (due.Save())
                        {
                            WebUser.EditAdjustedDueDate = null;
                            result = WorkFlowStatus.Finished;

                        }
                        else
                        {
                            WebApp.PageRenderer.WriteErrorStatus(true, due.LastError);
                            result = WorkFlowStatus.Indeterminate;

                        }
                    }
                    else
                    {
                        WebApp.PageRenderer.WriteErrorStatus(true, "An error has occurred, please go back and try again.");
                        result = WorkFlowStatus.Indeterminate;
                    }

                    WebUser.UserAdjustedDueDate = "";

                }
            }

            else if (workflowResult.ToUpper().IndexOf("SAVE") > -1 && KxPageUtility.IsLocal)
            {
                WebApp.ProcessConfiguration.Save();
                result = WorkFlowStatus.Finished;
            }
            

            return result;
        }
    }
}
