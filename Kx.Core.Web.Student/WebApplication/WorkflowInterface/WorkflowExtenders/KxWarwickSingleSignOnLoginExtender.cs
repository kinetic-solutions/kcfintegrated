﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using Kx.Core.Web.Security;
using System.Collections.Specialized;
using Kx.Core.Data;
using Kx.Core.Localisation;
using Kx.Core.Web.WorkflowInterface.WorkflowExtenders;
using Kx.Objects.Student;
using Kx.Objects.Student.Web;
using System.Web.Security;
using Kx.Core.Web.Configuration;

namespace Kx.Core.Web.Student.WebApplication.WorkflowInterface.WorkflowExtenders
{
    public class KxWarwickSingleSignOnLoginExtender : KxStudentWorkflowExtender
    {

        private bool requireValidatedEmail = false;
        private int loginAttempts = 3;
        private bool lockAccount = false;


        public bool RequireValidatedEmail
        {
            get
            {
                return requireValidatedEmail;
            }
            set
            {
                requireValidatedEmail = value;
            }
        }
        public int LoginAttempts
        {
            get
            {
                return loginAttempts;
            }
            set
            {
                loginAttempts = value;
            }
        }
        public bool LockAccount
        {
            get
            {
                return lockAccount;
            }
            set
            {
                lockAccount = value;
            }
        }

        public bool HasCookie
        {
            get
            {
                return CheckValidCookie();
            }
        }
        public static bool IsSSO
        {
            get
            {
                try
                {
                    return bool.Parse(HttpContext.Current.Session["IsSSO"].ToString());
                }
                catch
                {
                    return false;
                }

            }
            set
            {
                HttpContext.Current.Session["IsSSO"] = value;
            }
        }


        public override string Execute()
        {
            return HasCookie ? "loginsso" : "fail";
        }

        public override WorkFlowStatus DetermineCompletionState()
        {
            WorkFlowStatus state = base.DetermineCompletionState();
            if (state == WorkFlowStatus.Finished)
            {
                if (workflowResult.ToUpper().IndexOf("LOGINSSO") > -1)
                {
                    Execute();
                }
                else if (workflowResult.ToUpper().IndexOf("LOGIN") > -1)
                {
                    WebApp.RegistrationManager.RetryLimit = LoginAttempts;

                    if (WebApp.RegistrationManager.ValidateLogin())
                    {
                        WebApp.RegistrationManager.LoginUser();
                        KxWarwickSingleSignOnLoginExtender.IsSSO = false;
                        HttpContext.Current.Session["SessionTimeout"] = null;
                    }
                    else
                    {
                        WebApp.AddStrike();

                        if (WebApp.RegistrationManager.InvalidLogon)
                        {
                            WebApp.PageRenderer.WriteErrorStatus(true, KxLocalisationManager.GetSubstitution("Incorrect Logon details"));
                        }

                        if (WebApp.RegistrationManager.InvalidUser)
                        {
                            WebApp.PageRenderer.WriteErrorStatus(true, KxLocalisationManager.GetSubstitution("User not recognised"));
                        }

                        if (WebApp.RegistrationManager.RetriesExceeded)
                        {
                            WebApp.PageRenderer.WriteErrorStatus(true, KxLocalisationManager.GetSubstitution("Login retries exceeded, Please close your browser and leave for 5 minutes prior to attempting to log in again."));
                        }

                        if (WebApp.RegistrationManager.UnconfirmedEmail)
                        {
                            WebApp.PageRenderer.WriteErrorStatus(true, KxLocalisationManager.GetSubstitution("Cannot login until E-mail address is confirmed"));
                        }
                        state = WorkFlowStatus.Indeterminate;
                    }
                }


            }

            return state;
        }

        /// <summary>
        /// Check to see if we have a valid Warwick cookie, if so, log in!
        /// </summary>
        /// <returns></returns>
        private bool CheckValidCookie()
        {
            HttpCookie WarwickSOOCookie = HttpContext.Current.Request.Cookies["WarwickSSO"];

            if (WebApp.RegistrationManager.WebUser.LoggedIn || KxWarwickSingleSignOnLoginExtender.IsSSO)
            {
                KxWarwickSingleSignOnLoginExtender.IsSSO = true;
                return true;
            }
            else if (WarwickSOOCookie != null)
            {
                NameValueCollection cookieJar = KxSingleSignOnAuthentication.ValidateToken(WarwickSOOCookie.Value);

                if (Convert.ToInt32(cookieJar["returnType"]) < 50)
                {
                    if (cookieJar["id"] != null)
                    {
                        using (KxStudent student = new KxStudent(null, cookieJar["id"]))
                        {
                            if (student.KxStudentId >= 0)
                            {
                                //Create the webuser if it doesn't exist.
                                if (!WebApp.RegistrationManager.DoesStudentHaveWebAccount(student.KxStudentId))
                                {
                                    CreateWebUser(student);
									UpdateAnyApplications();
                                }


                                KxWarwickSingleSignOnLoginExtender.IsSSO = true;
                                WebApp.RegistrationManager.SSOLoginStudent(student.KxStudentId);
                            }
                            else
                            {
                                RedirectToLogout(System.Configuration.ConfigurationManager.AppSettings["WorkflowHomePageUrl"]);
                            }
                        }
                    }

                    HttpContext.Current.Session["SessionTimeout"] = null;
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// A cleaner way to redirect via NAFF..
        /// </summary>
        /// <param name="returnTarget"></param>
        public void RedirectToLogin(string returnTarget)
        {
            string SSOLoginURL = string.Format(System.Configuration.ConfigurationManager.AppSettings["SSOLogin"], System.Configuration.ConfigurationManager.AppSettings["SSOProvider"], returnTarget);
            HttpContext.Current.Session.Abandon();
            HttpContext.Current.Response.Redirect(SSOLoginURL);
        }

        public void RedirectToLogout(string returnTarget)
        {
            string SSOLogoutURL = string.Format(System.Configuration.ConfigurationManager.AppSettings["SSOLogout"], returnTarget);
            FormsAuthentication.SignOut();
        
            if (KxWarwickSingleSignOnLoginExtender.IsSSO)
            {
                WebApp.WorkflowInterface.WebUser.LoadStudent(-1);
                KxWarwickSingleSignOnLoginExtender.IsSSO = false;
                KxConfigurationManager.ResetSession(WebApp.CurrentPage);
                HttpContext.Current.Response.Redirect(SSOLogoutURL, false);
            }

         
            KxConfigurationManager.ResetSession(WebApp.CurrentPage);

        }

        private void CreateWebUser(KxStudent student)
        {
            KxWebUserStudent newWU = new KxWebUserStudent();

            newWU.ForeName = student.ForeName;
            newWU.SurName = student.SurName;
            newWU.UserName = student.StudentId;
            newWU.PersonId = student.PersonId;
            newWU.KxStudentId = student.KxStudentId;
            newWU.Password = student.StudentId;
            newWU.EmailAddress = student.EmailAddress;
            newWU.CreationDate = DateTime.Now;
            newWU.Active = 1;
            newWU.Person.WebAccessAllowed = true;
            newWU.Save();
        }

		private void UpdateAnyApplications()
		{
			KxData result = null;

			result = new KxData("", "WebProcessAllocations",
				TargetType.StoredProc, ActionType.ExecuteProcedure);

			//result.AddParameter("UserID", userId, KxValueType.Int, false);
			//result.AddParameter("FriendshipGroupID", dBIdentifier, KxValueType.Int, false);

			result.Execute();
		}
    }
}
