using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using Kx.Core.Utility;
using Kx.Core.Workflow;
using Kx.Core.Web.Configuration;
using Kx.Core.Web.PageRenderer;
using Kx.Core.Web.PageRenderer.Utility;
using Kx.Core.Web.WorkflowInterface;
using Kx.Core.Web.WorkflowInterface.WorkflowExtenders;
using Kx.Objects.Student;
using Kx.Objects.Student.Web;
using Kx.Core.Web.Student.WebApplication.ObjectTranslator;
using Kx.Core.Web.Student.WebApplication.BaseClass;
using Kx.Core.Web.Student.WebApplication.WorkflowInterface.WorkflowExtenders;
using Kx.Core.Exceptions;
using System.Web.UI;
using System.Configuration;
using System.Text.RegularExpressions;
using System.IO;
using Kx.Core.Configuration;


namespace Kx.Core.Web.Student.WebApplication.WorkflowInterface
{
    public class KxStudentWorkflowInterface : KxWorkflowInterface
    {

        private String _eurl = string.Empty;
        public String Eurl
        {
            get
            {
                return _eurl;
            }
            set
            {
                _eurl = value;
            }
        }

        public new KxWebUserStudent WebUser
        {
            get
            {
                try
                {
                    return (KxWebUserStudent)base.WebUser;
                }
                catch
                { return null; }
            }
        }

        public KxWebApplicationStudent WebApp
        {
            get
            { return (KxWebApplicationStudent)PageRenderer.WebApp; }
        }

        public KxStudentObjectTranslator ObjectTranslator
        {
            get
            { return (KxStudentObjectTranslator)PageRenderer.ObjectTranslator; }
        }

        public new KxWorkflowExtender Extender
        {
            get
            {
                try
                {
                    if (ExtenderName != "")
                    {
                        KxWorkflowExtender result = (KxWorkflowExtender)CurrentWorkflow.GetObjectByID(ExtenderName).InternalObject;
                        result.PageRenderer = PageRenderer;
                       
                        return result;
                    }
                    else
                    {
                        return null;
                    }
                }
                catch
                {
                    return null;
                }
            }
        }
        public KxStudentPhoto Sphoto // added by vasu
        {
            get { return new KxStudentPhoto(); }
        }

        public KxStudentWorkflowInterface(KxPageRenderer _pageRenderer)
            : base(_pageRenderer)
        { }
        
        public override void ProcessButtonTrigger(KxWebAttribute UserAction)
        {
            if (!String.IsNullOrEmpty(UserAction.SiteLookup))
            {
                WebUser.UserSiteLookupId = UserAction.SiteLookup;
            }

            if (!String.IsNullOrEmpty(UserAction.LibertyRoom))
            {
                WebUser.UserLibertyRoomId = UserAction.LibertyRoom;
                WebUser.UserCityRoomId = UserAction.LibertyRoom;
            }

            if (!String.IsNullOrEmpty(UserAction.OpalRoom))
            {
                WebUser.UserOpalRoomId = UserAction.OpalRoom;
            }

            if (!String.IsNullOrEmpty(UserAction.RoomFilter))
            {
                WebUser.CurrentApplication.CityRoomRates.UserRoomFilter = UserAction.RoomFilter;
            }

            if (!String.IsNullOrEmpty(UserAction.WebResource))
            {
                WebUser.UserWebResource = UserAction.WebResource;
            }

            if (!String.IsNullOrEmpty(UserAction.DocumentName))
            {
                if (UserAction.ReturnValue.ToUpper().IndexOf("ADDDOCUMENT") > -1)
                {
                    var documentName = ObjectTranslator.GetSuffixedDocumentName(UserAction);
                    // THIS SHOULD NOT BE CHANGED                                   ======\/\/\/\/\/=======
                    Control cntl = KxPageUtility.FindControl(CurrentPage, "KX_DN" + UserAction.DocumentName);
                    if ((cntl != null) && (CurrentPage.IsPostBack == true))
                    {
                        if (cntl is System.Web.UI.HtmlControls.HtmlInputFile)
                        {
                            System.Web.UI.HtmlControls.HtmlInputFile inp = (System.Web.UI.HtmlControls.HtmlInputFile)cntl;
                            HttpPostedFile myFile = inp.PostedFile;
                            if (myFile.FileName != "")
                            {
                                int maxDocumentSize = KxDbConfiguration.GetConfigurationInt("DocumentUploadMaxSize", myFile.ContentLength + 1000);

                                int nFileLen = myFile.ContentLength;
                                if (nFileLen > 0 && nFileLen <= maxDocumentSize)
                                {
                                    string filename = System.IO.Path.GetFileName(myFile.FileName);
                                    string extension = System.IO.Path.GetExtension(myFile.FileName);
                                    string validDocumentTypes = KxDbConfiguration.GetConfiguration("DocumentUploadTypes");
                                    bool isvalidDocumentType = string.IsNullOrWhiteSpace(validDocumentTypes) ? true : Regex.IsMatch(validDocumentTypes.ToLower(), (extension ?? "").TrimStart('.').ToLower());
                                    if (isvalidDocumentType)
                                    {
                                        byte[] myData = new byte[nFileLen];
                                        myFile.InputStream.Read(myData, 0, nFileLen);

                                        byte[] compData = new byte[myData.Length];
                                        compData = KxDocumentCompression.CompressBytes(myData, filename);

                                        if (WebUser.KxStudentId != 0)
                                        {
                                            WebApp.DeleteDocument(WebUser.KxStudentId, documentName);
                                            WebApp.UploadDocument(WebUser.KxStudentId, compData, documentName, filename);
                                        }
                                        else
                                        {
                                            WebApp.DeleteDocumentByAppGuid(WebUser.CurrentApplication.ApplicationGuid, documentName);
                                            WebApp.UploadDocumentByAppGuid(WebUser.CurrentApplication.ApplicationGuid, compData, documentName, filename);
                                        }
                                    }
                                    else
                                    {    
                                        KxLogger.LogActivity(this, "ProcessButtonTrigger", "KX_DN" + UserAction.DocumentName, "Document upload failed, invalid type");
                                    }
                                }
                                else
                                {
                                    KxLogger.LogActivity(this, "ProcessButtonTrigger", "KX_DN" + UserAction.DocumentName, "Document upload failed, invalid file size");
                                }
                            }
                            if (WebUser.KxStudentId != 0)
                            {
                                WebUser.CurrentApplication.GetStudentDocument(WebUser.KxStudentId, documentName);
                            }
                            else
                            {
                                WebUser.CurrentApplication.GetStudentDocumentByAppGuid(WebUser.CurrentApplication.ApplicationGuid, documentName);
                            }
                            
                        }
                        else
                        {
                            KxLogger.LogActivity(this, "ProcessButtonTrigger", "KX_DN" + UserAction.DocumentName, "Control of wrong type, should be input!");
                        }
                    }

                }
                else if (UserAction.ReturnValue.ToUpper().IndexOf("DELDOCUMENT") > -1)
                {
                    var documentName = ObjectTranslator.GetSuffixedDocumentName(UserAction);

                    if (WebUser.KxStudentId != 0)
                    {
                        WebUser.CurrentApplication.GetStudentDocument(WebUser.KxStudentId, documentName);
                        if (WebUser.CurrentApplication.DocumentExists != "")
                        {
                            WebApp.DeleteDocument(WebUser.KxStudentId, documentName);
                        }
                    }
                    else
                    {
                        WebUser.CurrentApplication.GetStudentDocumentByAppGuid(WebUser.CurrentApplication.ApplicationGuid, documentName);
                        if (WebUser.CurrentApplication.DocumentExists != "")
                        {
                            WebApp.DeleteDocumentByAppGuid(WebUser.CurrentApplication.ApplicationGuid, documentName);
                        }
                    }
                }
            }

            // photo upload functionality added by vasu on 04/07/2013 - Fudged by Alan late on 09/11/2020

            if (!String.IsNullOrEmpty(UserAction.DocumentName))
            {
                if (UserAction.ReturnValue.ToUpper().IndexOf("ADDPHOTO") > -1)
                {
                    Control cntl = KxPageUtility.FindControl(CurrentPage, "KX_DN" + UserAction.DocumentName);

                    if (cntl != null && CurrentPage.IsPostBack)
                    {
                        if (cntl is System.Web.UI.HtmlControls.HtmlInputFile)
                        {
                            System.Web.UI.HtmlControls.HtmlInputFile ifile = (System.Web.UI.HtmlControls.HtmlInputFile)cntl;

                            HttpPostedFile postedFile = ifile.PostedFile;
                            try
                            {
                                if (postedFile.FileName != "")
                                {
                                    int fileLength = postedFile.ContentLength;

                                    int maxSize = ConfigurationManager.AppSettings["ImageMaxSize"] == null ? 0 : Convert.ToInt32(ConfigurationManager.AppSettings["ImageMaxSize"]);

                                    FileInfo fi = new FileInfo(postedFile.FileName);

                                    string fileExtension = fi.Extension;
    
                                    bool isvalidImage = ConfigurationManager.AppSettings["ImageTypes"] == null ? false : Regex.IsMatch(ConfigurationManager.AppSettings["ImageTypes"].ToString().ToLower(), fileExtension.TrimStart('.').ToLower());

                                    if ((fileLength > 0 && fileLength <= maxSize) && isvalidImage) //  
                                    {
                                        if (KxDocumentProcessor.IsValidFile(postedFile.FileName))
                                        {
                                            string imgDirectory = ConfigurationManager.AppSettings["ImageLocation"] == null ? "" : ConfigurationManager.AppSettings["ImageLocation"].ToString();

                                            if (!string.IsNullOrEmpty(imgDirectory))
                                            {
                                                postedFile.SaveAs(imgDirectory + @"\" + WebApp.ApplicationsManager.Student.StudentId + fileExtension);

                                                Sphoto.PhotoFileName = WebApp.ApplicationsManager.Student.StudentId + fileExtension;

                                                HttpContext.Current.Session["ActualPhotoFileName"] = postedFile.FileName;

                                                Sphoto.ActualPhotoFileName = postedFile.FileName;

                                                Sphoto.isPhotoUploaded = true;
                                            }
                                        }

                                    }
                                    else
                                    { 
                                        KxLogger.LogActivity(this, "ProcessButtonTrigger", "KX_DN" + UserAction.DocumentName, "Image Upload failed, incorrect type or size");
                                    }

                                }
                            }
                            catch {
                                KxLogger.LogActivity(this, "ProcessButtonTrigger", "KX_DN" + UserAction.DocumentName, "Image Upload failed, exception error");
                            }
                        }
                    }

                }
            }

            // remove photo functionality added by vasu on 04/07/2013

            if (!String.IsNullOrEmpty(UserAction.DocumentName))
            {
                if (UserAction.ReturnValue.ToUpper().IndexOf("DELPHOTO") > -1)
                {
                    try
                    {
                        string imgDirectory = ConfigurationManager.AppSettings["ImageLocation"] == null ? "" : ConfigurationManager.AppSettings["ImageLocation"].ToString();

                        if (!string.IsNullOrEmpty(imgDirectory))
                        {
                            if (File.Exists(imgDirectory + @"\" + Sphoto.PhotoFileName))
                            {
                                FileInfo fi = new FileInfo(imgDirectory + @"\" + Sphoto.PhotoFileName);
                                fi.Delete();

                                WebUser.WriteStudentHistoryRecord("Photo Deleted", "Photo deleted by the student on online on " + DateTime.Now);

                                Sphoto.isPhotoUploaded = false;
                            }
                        }
                    }
                    catch {
                        KxLogger.LogActivity(this, "ProcessButtonTrigger", "KX_DN" + UserAction.DocumentName, "Image Removal failed");
                    }
                }
            }


            if (UserAction.AcademicYear > 0)
            {
                if (UserAction.AcademicYear != 11111)
                {
                    WebUser.UserAcademicYear = UserAction.AcademicYear;
                }
                else
                {
                    WebUser.UserAcademicYear = WebApp.ApplicationsManager.GetAcademicYearForDate(DateTime.Today);
                }
            }

            if (UserAction.AcademicYearSession > -1)
            {
                if (UserAction.AcademicYearSession != 11111)
                {
                    WebUser.UserAcademicYearSession = UserAction.AcademicYearSession;
                }
            }

            if (UserAction.SpecificApplicationGuid != null)
            {
                WebUser.UserApplicationGuid = UserAction.SpecificApplicationGuid;
                WebUser.ControlApplicationGuid = null;
            }

            if (UserAction.RequestIdentifier > 0 && WebApp.RequestManager != null)
            {
                WebApp.RequestManager.SetCurrentRequest(UserAction.RequestIdentifier);
            }

            if (UserAction.AllocationPricing != "")
            {
                WebUser.UserAllocationPricing = UserAction.AllocationPricing;
            }

            if (UserAction.DueDate != "")
            {
                WebUser.UserAdjustedDueDate = UserAction.DueDate;
            }


            if (UserAction.BedroomType > -1)
            {
                WebApp.UserBedroomTypeCharge = UserAction.BedroomType;
            }

            if (UserAction.UDF > 0)
            {
                WebUser.CurrentApplication.UserUDF = UserAction.UDF;
            }

            if (UserAction.AllocationDate > 0)
            {
                WebUser.UserAllocationDateId = UserAction.AllocationDate;
            }

            if (UserAction.Preference > 0)
            {
                WebUser.CurrentApplication.UserPreferenceIndex = UserAction.Preference;
            }

            if (UserAction.PrefNumber > 0)
            {
                WebUser.CurrentApplication.UserPreferenceNumber = UserAction.PrefNumber;
            }

            if (UserAction.GroupMember > 0)
            {
                ObjectTranslator.UserMemberId = UserAction.GroupMember;
            }

            if (UserAction.StudentTermDate > 0)
            {
                ObjectTranslator.UserTermDate = UserAction.StudentTermDate;
            }

            if (UserAction.PrefBasketIndex > 0)
            {
                ObjectTranslator.UserPrefBasketIndex = UserAction.PrefBasketIndex;
            }

            if (UserAction.PrefSearchIndex > 0)
            {
                ObjectTranslator.UserPrefSearchIndex = UserAction.PrefSearchIndex;
            }

            if (UserAction.GenericIndex > 0)
            {
                ObjectTranslator.GenericControlIndex = UserAction.GenericIndex;
            }
            if (UserAction.GenericIndex2 > 0)
            {
                ObjectTranslator.GenericControlIndex2 = UserAction.GenericIndex2;
            }
            if (UserAction.GenericIndex3 > 0)
            {
                ObjectTranslator.GenericControlIndex3 = UserAction.GenericIndex3;
            }
        }

        public override void ProcessPaymentResponse()
        {
            CurrentWorkflow.Resume(WebApp.ContractManager.AuthorisedText);
        }

        public override void SaveState()
        {
            PageRenderer.WorkflowInterface.RefreshWorkflow();
            HttpContext.Current.Session["WebMgr"] = PageRenderer.WebApp;

            HttpContext.Current.Session["StudentId"] = WebApp.ApplicationsManager.Student.StudentId; // added by vasu
        }

        public override string MergeActionId(string source)
        {
            if (source != null && source.Contains("{"))
            {
                return WebApp.ObjectTranslator.MergeTags(source);
            }
            else
            {
                return source;
            }
        }
    }
}
