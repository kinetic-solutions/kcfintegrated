﻿using Kx.Objects.Student;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace Kx.Core.Web.Student.WebApplication.ProcessManagers.SSO
{
    public class eURLSSO : ISingleSignon
    {
        // SJA Unable to test, which client?
        public int AcademicYearId { get; private set; }

        public int ApplicationTypeId
        {
            get { return 0; }
        }

        public string StudentId { get; private set; }

        public bool PerformSingleSignon(KxWebApplicationStudent application, HttpContextWrapper context)
        {
            if (context.Session["eurl"] != null)
            {
                string eURL = context.Session["eurl"].ToString();

                EUrlValuesCollection eURLValues = new EUrlValuesCollection(eURL, "p455w0rd");

                StudentId = eURLValues["studentId"];
                int academicYearId = 0;
                Int32.TryParse(eURLValues["AcademicYear"], out academicYearId);

                this.AcademicYearId = academicYearId;

                if (application.WorkflowInterface.WebUser != null && application.WorkflowInterface.WebUser.Student != null && !application.WorkflowInterface.WebUser.Student.StudentId.Equals(StudentId, StringComparison.InvariantCultureIgnoreCase))
                {
                    application.WorkflowInterface.WebUser.ClearLoggedInUser();
                }


                return true;
            }
            return false;
        }
    }
}
