﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace Kx.Core.Web.Student.WebApplication.ProcessManagers.SSO
{
    public interface ISingleSignon
    {
        string StudentId { get;  }
        int AcademicYearId { get;  }
        int ApplicationTypeId { get; }
        bool PerformSingleSignon(KxWebApplicationStudent application, HttpContextWrapper context);
    }
}
