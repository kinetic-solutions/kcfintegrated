﻿using Kx.Core.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace Kx.Core.Web.Student.WebApplication.ProcessManagers.SSO
{
    public class StudentBypassSSO : ISingleSignon
    {
        public int AcademicYearId
        {
            get { return 0; }
        }

        public int ApplicationTypeId
        {
            get { return 0; }
        }

        public string StudentId { get; private set; }

        public bool PerformSingleSignon(KxWebApplicationStudent application, HttpContextWrapper context)
        {
            bool result = false;
            //if (KxDbConfiguration.GetConfiguration("AllowStudentId") != "")   pha
            //{
                StudentId = context.Request.QueryString["studentid"];
                result = true;
            //}

            return result;
        }
    }
}
