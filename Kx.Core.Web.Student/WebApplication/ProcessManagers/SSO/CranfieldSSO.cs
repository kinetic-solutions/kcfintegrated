﻿using Kx.Core.Encryption;
using Kx.Core.Exceptions;
using Kx.Objects.Student;
using Kx.Objects.Student.Web;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Web;

namespace Kx.Core.Web.Student.WebApplication.ProcessManagers.SSO
{
    public class CranfieldSSO : ISingleSignon
    {
        // TESTED SJA 20160405
        public int AcademicYearId
        {
            get { return 0; }
        }

        public int ApplicationTypeId
        {
            get { return 0; }
        }

        public string StudentId
        {
            get { return string.Empty; }
        }

        public bool PerformSingleSignon(KxWebApplicationStudent application, HttpContextWrapper context)
        {
            NameValueCollection qs = context.Request.QueryString;

            //Use the coalesce funciton to return string.empty is the results are null - saves on problems later
            string ID = qs["STU"] ?? string.Empty;
            string email = qs["email"] ?? string.Empty;
            string DT = qs["DT"] ?? string.Empty;
            string hash = qs["hash"] ?? string.Empty;
            bool result = false;

            if(!string.IsNullOrWhiteSpace(ID) && !string.IsNullOrWhiteSpace(email) && !string.IsNullOrWhiteSpace(hash) && !string.IsNullOrWhiteSpace(DT))
            {
                if (application.WorkflowInterface.WebUser != null && application.WorkflowInterface.WebUser.Student != null && application.WorkflowInterface.WebUser.KxStudentId > 0 && !application.WorkflowInterface.WebUser.Student.StudentId.Equals(ID, StringComparison.InvariantCultureIgnoreCase))
                {
                    context.Session.Remove("CranfieldIn");
                    application.WorkflowInterface.WebUser.ClearLoggedInUser();
                }

                if (context.Session["CranfieldIn"] == null)
                {
                    if (!TransferStudent(application, ID, DT, hash, email))
                    {
                        if (application.WorkflowInterface.CurrentWorkflow != null)
                        {
                            context.Session["CranfieldIn"] = "true";
                            application.WorkflowInterface.CurrentWorkflow.ExecuteStep("corefailure");
                        }
                    }
                    else
                    {
                        result = application.WorkflowInterface.WebUser != null && application.WorkflowInterface.WebUser.LoggedIn;
                    }
                }
            }

            return result;
        }


        private bool TransferStudent(KxWebApplicationStudent webApp, string ID, string dateTime, string hash, string email)
        {
            bool result = false;

            if (webApp.WorkflowInterface.WebUser != null)
            {
                if (!webApp.WorkflowInterface.WebUser.Loaded)
                {
                    KxStudent student = GetStudent(ID);


                    if (student.KxStudentId != 0) //switched this around from the abertay manager
                    //Whats the point of even attempting the hash if the data is rubbish?
                    {

                        if (ValidateHash(student.StudentId, dateTime, hash))
                        {
                            // transfer is valid, lets create the web user.
                            KxWebUserStudent webuser = new KxWebUserStudent(null);
                            webuser.LoadUserName(student.StudentId);

                            if (!webuser.Loaded)
                            {
                                webuser = new KxWebUserStudent(null);
                                webuser.UserName = student.StudentId;
                                webuser.EmailAddress = email;
                                webuser.ForeName = student.ForeName;
                                webuser.SurName = student.SurName;
                                webuser.Active = 1;
                                webuser.KxStudentId = student.KxStudentId;
                                webuser.CreationDate = DateTime.Now;
                                webuser.EmailConfirmed = DateTime.Now;
                                webuser.Save();
                            }
                            else //just update the email address as per the spec
                            {
                                webuser.EmailAddress = email;
                                webuser.Save();
                            }
                            webApp.WorkflowInterface.WebUser.LoadGuid(webuser.UserGuid);
                            webApp.WorkflowInterface.WebUser.LoggedIn = true;

                            result = true;

                            if (!webApp.WorkflowInterface.WebUser.CurrentApplication.IsConfirmed)
                            {
                                KxWebStudentApplication app = webApp.WorkflowInterface.WebUser.CurrentApplication;
                                // Update application from transfer record...
                                app.ForeName = student.ForeName;
                                app.SurName = student.SurName;
                                app.CurrentStudentAddress.Address1 = student.HomeAddress.Address;
                                app.CurrentStudentAddress.Address2 = student.HomeAddress.Town;
                                app.CurrentStudentAddress.Address3 = student.HomeAddress.County;
                                app.CurrentStudentAddress.Address4 = student.HomeAddress.Country;
                                app.CurrentStudentAddress.Postcode = student.HomeAddress.Postcode;
                                app.CurrentStudentAddress.TelephoneNumber = student.HomeAddress.Telephone;
                                app.Gender = student.Gender;
                                app.Dob = student.Dob;
                                webApp.WorkflowInterface.WebUser.Save();

                            }
                        }
                        else
                        {
                            KxLogger.LogActivity("KxCranfieldStudentManager", "Cranfield E-Vision Transfer; Invalid Hash", hash, ID);
                            result = false;
                        }
                    }
                    else
                    {
                        result = false;
                        KxLogger.LogActivity("KxCranfieldStudentManager", "Cranfield E-Vision Transfer; Invalid Data", hash, ID);
                    }

                }
                else
                {
                    result = true;
                }
            }
            else
            {
                result = false;
            }
            return result;
        }
        private KxStudent GetStudent(string ID)
        {
            KxStudent student = new KxStudent(null, ID);
            return student;
        }

        private bool ValidateHash(string ID, string dt, string hash)
        {
            //Hash format: STUDENTID DATE SECRET
            //All without spaces
            bool result = false;


            DateTime currentDT = DateTime.Now;
            string now = currentDT.ToString("yyyyMMddHH");
            string HourlessDT = currentDT.AddHours(-1).ToString("yyyyMMddHH");

            string key = "6HC.OGVzVR~qrKAL#t,RB[LJU.o#E3@]OFxv2Ra5n=k<qt7==O\".m6O%7avj|aAcc[S42g=_VjPZ%,{x2IbicJ;DL&ZzAfMrmu^~u~]U_5|F*^#^LnP+tg"; //massive long key!

            string comparehash = KxMD5.Hash(String.Format("{0}{1}{2}", ID, now, key));
            string comparehash2 = KxMD5.Hash(String.Format("{0}{1}{2}", ID, HourlessDT, key));


            if (String.Compare(hash, comparehash, true) == 0)
            {
                result = true;
            }
            else if (String.Compare(hash, comparehash2, true) == 0)
            {
                result = true;
            }

            return result;
        }
    }
}
