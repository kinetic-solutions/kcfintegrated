﻿using Kx.Core.Configuration;
using Kx.Objects.Student;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace Kx.Core.Web.Student.WebApplication.ProcessManagers.SSO
{
    // TESTED SJA 20160405
    public class EdinburgheUrl : ISingleSignon
    {
        public int AcademicYearId { get; private set; }

        public string StudentId{ get; private set; }

        public int ApplicationTypeId
        {
            get { return 0; }
        }

        public bool PerformSingleSignon(KxWebApplicationStudent application, HttpContextWrapper context)
        {
            bool result = false;
            if (context.Request.QueryString["eurl"] != null && context.Request.QueryString["wf"] == null)
            {
                #region

                Kx.Core.Web.Providers.AuditProvider.ConfiguredAddToAudit("KxWebApplicationStudent", "CheckForUserTransfer", "eurl and wf");

                HttpContext.Current.Response.Redirect(KxDbConfiguration.GetConfiguration("WorkflowHomePageUrl") + "&eurl=" + HttpContext.Current.Request.QueryString["eurl"]);

                #endregion
            }
            else if (context.Request.QueryString["eurl"] != null)
            {
                #region

                string eURL = context.Request.QueryString["eurl"];

                //changed method of loading eURL - as now trying to load the everything after the first ten letter will result in A dodgy string..
                EUrlValuesCollection eURLValues = new EUrlValuesCollection(eURL, "p455w0rd");

                StudentId = eURLValues["StudentId"];

                int academicYearId = 0;
                Int32.TryParse(eURLValues["AcademicYearId"], out academicYearId);

                this.AcademicYearId = academicYearId;

                if (application.WorkflowInterface.WebUser != null && application.WorkflowInterface.WebUser.Student != null && !application.WorkflowInterface.WebUser.Student.StudentId.Equals(StudentId, StringComparison.InvariantCultureIgnoreCase))
                {
                    application.WorkflowInterface.WebUser.ClearLoggedInUser();
                }

                result = true;

                #endregion
            }

            return result;
        }
    }
}
