﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using Kx.Objects.Student;
using Kx.Objects.Student.Web;
using Kx.Core.Exceptions;
using Kx.Core.Configuration;
using System.Security.Cryptography;

namespace Kx.Core.Web.Student.WebApplication.ProcessManagers.SSO
{
    // TESTED SJA 2016-04-05
    public class OttawaSSO : ISingleSignon
    {
        private const string STR_DATEFORMAT = "yyyyMMddHHmmss";
        public int AcademicYearId
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public int ApplicationTypeId
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public string StudentId
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public bool PerformSingleSignon(KxWebApplicationStudent application, HttpContextWrapper context)
        {
            string id = context.Request.QueryString["StudentId"];
            string timestamp = context.Request.QueryString["tmst"];
            string nonce = context.Request.QueryString["nonce"];
            string hash = context.Request.QueryString["hash"];

            bool result = false;

            if (!string.IsNullOrWhiteSpace(id) && !string.IsNullOrWhiteSpace(timestamp) && !string.IsNullOrWhiteSpace(nonce) && !string.IsNullOrWhiteSpace(hash))
            {
                if (application.WorkflowInterface.WebUser != null && application.WorkflowInterface.WebUser.Student != null && application.WorkflowInterface.WebUser.KxStudentId > 0 && !application.WorkflowInterface.WebUser.Student.StudentId.Equals(id, StringComparison.InvariantCultureIgnoreCase))
                {
                    context.Session.Remove("OttawaIn");
                    application.WorkflowInterface.WebUser.ClearLoggedInUser();
                }

                if (context.Session["OttawaIn"] == null)
                {
                    if (!ValidateSSOAndLoadWebUser(application, id, timestamp, nonce, hash))
                    {
                        context.Session["OttawaIn"] = "true";
                        application.WorkflowInterface.CurrentWorkflow.ExecuteStep(application.WorkflowInterface.CurrentWorkflow.ErrorStep);
                    }
                    else
                    {
                        result = application.WorkflowInterface.WebUser != null && !application.WorkflowInterface.WebUser.LoggedIn;
                    }
                }
            }

            return result;
        }
        public bool ValidateSSOAndLoadWebUser(KxWebApplicationStudent webApp, string studentId, string timestamp, string nonce, string hash)
        {
            if (webApp.WorkflowInterface.WebUser != null && !webApp.WorkflowInterface.WebUser.Loaded)
            {


                // Check Nonce does not already exist
                if (nonce.Length != 8 || !IsNonceNew(nonce, studentId, timestamp, hash))
                {
                    return false;
                }

                // Check timestamp is < 5 minutes old and in format YYYYMMDDHHmmSS
                if (!ValidateTimestampFormat(timestamp) || !ValidateTimestampLife(timestamp))
                {
                    return false;
                }

                // Check Hash is correct
                if (!ValidateHashParameter(studentId, timestamp, nonce, hash))
                {
                    return false;
                }

                // Try and load previous webuser record.
                KxWebUserStudent webuser = new KxWebUserStudent(null);
                webuser.LoadUserName(studentId);

                if (!webuser.Loaded)
                {
                    // No previous webuser record for student
                    // Try to load student record and create a new webuser record

                    KxStudent student = new KxStudent(null, studentId);
                    if (student.KxStudentId != 0)
                    {
                        // Student record exists in ST2Student, now create webuser record.
                        webuser = CreateNewWebUserFromStudent(student, webuser);
                    }
                    else
                    {
                        return false;
                    }

                }

                LogInToWorkflow(webApp, webuser);

            }

            return true;
        }
        private bool ValidateTimestampFormat(string timestamp)
        {
            return true;
        }

        private bool IsNonceNew(string nonceReceived, string studentId, string timestamp, string hashreceived)
        {

            KxOttawaSSONonce record = new KxOttawaSSONonce(null);
            if (record.FindExistingRecordWithNonce(nonceReceived))
            {
                KxLogger.LogActivity(this, "IsNonceNew", "Nonce has previously been used. SSO Failed.", "Nonce submitted:" + nonceReceived);
                return false;
            }
            else
            {
                record.StudentId = studentId;
                record.Nonce = nonceReceived;
                record.CreationDate = DateTime.Now;
                record.HashReceived = hashreceived;

                if (!record.InsertToTable())
                {
                    KxLogger.LogActivity(this, "IsNonceNew", "Nonce has previously been used. Insert into OttawaSSONonces Failed.", "Nonce submitted:" + nonceReceived);
                    return false;
                }
            }

            return true;
        }

        private bool ValidateTimestampLife(string timestamp)
        {
            int validForMins = 0;
            try
            {
                validForMins = KxDbConfiguration.GetConfigurationInt("OttawaSSOTimestampLife", 10);
            }
            catch
            {
                KxLogger.LogActivity(this, "ValidateTimestampLife", "Unable to find OttawaSSOTimestampLife setting in app.settings or web.config file", "");
                return false;
            }


            DateTime timestampReceived = DateTime.ParseExact(timestamp, STR_DATEFORMAT, null);
            DateTime minTimestamp = DateTime.Now.AddMinutes(-validForMins);

            if (timestampReceived < minTimestamp)
            {
                KxLogger.LogActivity(this, "ValidateTimestampLife", "Timestamp received is more than " + Convert.ToString(validForMins) + " minutes old.", "Timestamp Received: " + timestamp + "-- Min Timestamp Allowed: " + Convert.ToString(minTimestamp));
                return false;
            }

            return true;
        }

        private bool ValidateHashParameter(string studentId, string timestamp, string nonce, string hashReceived)
        {
            String secretPassword = KxDbConfiguration.GetConfiguration("OttawaSSOSecretPassword");

            byte[] keyBytes = Encoding.UTF8.GetBytes(secretPassword);

            if (secretPassword == null)
            {
                KxLogger.LogActivity(this, "ValidateHashParameter", "Unable to find OttawaSSOSecretPassword setting in app.settings or web.config file", "");
                return false;
            }

            String constructedString = studentId + timestamp + nonce + secretPassword;
            String hashGenerated = GenerateHash(keyBytes, constructedString);
            String hashToCheck = HttpUtility.UrlEncode(hashGenerated);

            if (hashGenerated != hashReceived)
            {
                KxLogger.LogActivity(this, "ValidateHashParameter", "Hash has not been validated", "Hash Received: " + hashReceived + " is not Hash Expected: " + hashToCheck);
                return false;
            }


            return true;
        }

        private string GenerateHash(byte[] key, string construct)
        {
            HMACSHA256 hashGenerator = new HMACSHA256(key);

            byte[] dataBytes = Encoding.UTF8.GetBytes(construct);

            //use the SHA256Managed Class to compute the hash
            byte[] hmacBytes = hashGenerator.ComputeHash(dataBytes);

            //retun as base64 string.
            return Convert.ToBase64String(hmacBytes);
        }

        private KxWebUserStudent CreateNewWebUserFromStudent(KxStudent student, KxWebUserStudent webuser)
        {
            webuser = new KxWebUserStudent(null);
            webuser.UserName = student.StudentId;
            webuser.EmailAddress = student.EmailAddress;
            webuser.ForeName = student.ForeName;
            webuser.SurName = student.SurName;
            webuser.Active = 1;
            webuser.KxStudentId = student.KxStudentId;
            webuser.CreationDate = DateTime.Now;
            webuser.EmailConfirmed = DateTime.Now;
            webuser.PersonId = student.PersonId;
            webuser.Save();
            return webuser;
        }

        private void LogInToWorkflow(KxWebApplicationStudent webApp, KxWebUserStudent webuser)
        {
            webApp.WorkflowInterface.WebUser.LoadGuid(webuser.UserGuid);
            webApp.WorkflowInterface.WebUser.LoggedIn = true;
        }
    }
}
