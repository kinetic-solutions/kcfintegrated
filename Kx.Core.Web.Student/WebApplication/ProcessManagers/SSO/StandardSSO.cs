﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using Kx.Core.Configuration;
using Kx.Core.Encryption;
using Kx.Core.Exceptions;
using Kx.Objects.Student;
using Kx.Objects.Student.Web;

namespace Kx.Core.Web.Student.WebApplication.ProcessManagers.SSO
{
    public class StandardSSO : ISingleSignon
    {
        // TESTED SJA 20160405

        private delegate string DoHash(string hashString);

        enum HashType
        {
            MD5,
            SHA1,
            SHA256
        }

        private const string STR_DATEFORMAT = "yyyyMMddHH";

        public int AcademicYearId
        {
            get { return 0; }
        }

        public int ApplicationTypeId
        {
            get { return 0; }
        }

        public string StudentId
        {
            get { return string.Empty; }
        }

        public bool PerformSingleSignon(KxWebApplicationStudent application, HttpContextWrapper context)
        {
            string id = "";
            string emailAddress = "";
            string hash = context.Request["HASH"];

            bool result = false;

            if (context.Request["StudentId"] != null)
            {
                id = context.Request["StudentId"];
            }
            else   
            {
                id = KxDbConfiguration.GetConfiguration("StudentId");                   // pha
            }

            if (context.Request["EmailAddress"] != null)
            {
                emailAddress = context.Request["EmailAddress"];
            }

            //if(!string.IsNullOrWhiteSpace(hash) && (!string.IsNullOrWhiteSpace(id) || !string.IsNullOrWhiteSpace(emailAddress)))  
            if((!string.IsNullOrWhiteSpace(id) || !string.IsNullOrWhiteSpace(emailAddress)))    //pha
            {
                if (application.WorkflowInterface.WebUser != null && application.WorkflowInterface.WebUser.Student != null && application.WorkflowInterface.WebUser.KxStudentId > 0 && !application.WorkflowInterface.WebUser.Student.StudentId.Equals(id, StringComparison.InvariantCultureIgnoreCase))
                {
                    context.Session.Remove("StandardSSOIn");
                    application.WorkflowInterface.WebUser.ClearLoggedInUser();
                }

                if (context.Session["StandardSSOIn"] == null)
                {
                    if (!GetStandardTransferStudentId(application, id, emailAddress, hash))
                    {
                        context.Session["StandardSSOIn"] = "true";
                        application.WorkflowInterface.CurrentWorkflow.ExecuteStep(application.WorkflowInterface.CurrentWorkflow.ErrorStep);
                    }
                    else
                    {
                        result = application.WorkflowInterface.WebUser != null && application.WorkflowInterface.WebUser.LoggedIn;
                    }
                }
            }

            return result;
        }

        private bool LoadStudentAndLogin(KxWebApplicationStudent webApp, string studentId, string emailAddress, string hash)
        {
            KxStudent student = new KxStudent(null, studentId);

            // pha
            //if (CheckValidHash(student.StudentId, emailAddress, hash))
            //{
                if (student.KxStudentId != 0)
                {
                    return ValidUserCreateWebUserRecord(webApp, student);
                }
           // }

            LogInvalidData(studentId, emailAddress, hash);
            return false;
        }

        private bool ValidUserCreateWebUserRecord(KxWebApplicationStudent webApp, KxStudent student)
        {
            // Valid Transfer - Create WebUser record
            KxWebUserStudent webuser = new KxWebUserStudent(null);
            webuser.LoadUserName(student.StudentId);

            if (!webuser.Loaded)
            {
                webuser = CreateNewWebUserFromStudent(student, webuser);
            }

            LogInToWorkflow(webApp, webuser);

            if (!webApp.WorkflowInterface.WebUser.CurrentApplication.IsConfirmed)
            {
                UpdateApplicationFromTransferRecord(webApp, student);
            }

            return true;
        }

        private void UpdateApplicationFromTransferRecord(KxWebApplicationStudent webApp, KxStudent student)
        {
            KxWebStudentApplication app = webApp.WorkflowInterface.WebUser.CurrentApplication;
            app.ForeName = student.ForeName;
            app.SurName = student.SurName;
            app.CurrentStudentAddress.Address1 = student.HomeAddress.Address;
            app.CurrentStudentAddress.Address2 = student.HomeAddress.Town;
            app.CurrentStudentAddress.Address3 = student.HomeAddress.County;
            app.CurrentStudentAddress.Address4 = student.HomeAddress.Country;
            app.CurrentStudentAddress.Postcode = student.HomeAddress.Postcode;
            app.CurrentStudentAddress.TelephoneNumber = student.HomeAddress.Telephone;
            app.Gender = student.Gender;
            app.Dob = student.Dob;

            webApp.WorkflowInterface.WebUser.Save();
        }

        private void LogInToWorkflow(KxWebApplicationStudent webApp, KxWebUserStudent webuser)
        {
            webApp.WorkflowInterface.WebUser.LoadGuid(webuser.UserGuid);
            webApp.WorkflowInterface.WebUser.LoggedIn = true;
        }

        private KxWebUserStudent CreateNewWebUserFromStudent(KxStudent student, KxWebUserStudent webuser)
        {
            webuser = new KxWebUserStudent(null);
            webuser.UserName = student.StudentId;
            webuser.EmailAddress = student.EmailAddress;
            webuser.ForeName = student.ForeName;
            webuser.SurName = student.SurName;
            webuser.Active = 1;
            webuser.KxStudentId = student.KxStudentId;
            webuser.CreationDate = DateTime.Now;
            webuser.EmailConfirmed = DateTime.Now;
            webuser.PersonId = student.PersonId;
            webuser.Save();
            return webuser;
        }

        private void LogInvalidData(string studentId, string emailAddress, string hash)
        {
            KxLogger.LogActivity("StandardManager", "SSO Transfer; Invalid Data", String.Format("Parameter Details -- Hash: {0}", hash), String.Format("Student Id: {0} Email Address: {1}", studentId, emailAddress));
        }

        private bool CheckValidHash(string studentId, string emailAddress, string hash)
        {
            AddBracketsIfRequired(ref emailAddress, ref studentId);

            return CheckHash(hash, studentId, emailAddress);
        }

        private bool CheckHash(string hash, string stuId, string emailAdd)
        {
            DateTime current = DateTime.Now;
            string nowTime = current.ToString(STR_DATEFORMAT);
            string earlierTime = current.AddHours(-1).ToString(STR_DATEFORMAT);
            // Get all required values for use in generating hashes
            string ssoAuthMethod = KxDbConfiguration.GetConfiguration("SSOAuthMethod").ToUpper();

            try
            {
                HashType type = GetHashType(ssoAuthMethod);

                switch (type)
                {
                    case HashType.MD5:
                        return CheckHash(hash, stuId, emailAdd, nowTime, earlierTime,  KxMD5.Hash );
                    case HashType.SHA1:
                        return CheckHash(hash, stuId, emailAdd, nowTime, earlierTime,  KxSHA1.Hash);
                    case HashType.SHA256:
                        return CheckHash(hash, stuId, emailAdd, nowTime, earlierTime,  KxSHA256.Hash);
                }
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "CheckHash", ex.Message, "");
            }

            return false;
        }

        private HashType GetHashType(string ssoAuthMethod)
        {
            HashType type = HashType.MD5;

            type = (HashType)Enum.Parse(typeof(HashType), ssoAuthMethod);

            return type;
        }

        private void AddBracketsIfRequired(ref string emailAdd, ref string stuId)
        {
            bool ssoWrapWithBrackets = Convert.ToBoolean(KxDbConfiguration.GetConfiguration("SSOWrapWithBrackets"));

            // Check whether wrap is required.
            if (ssoWrapWithBrackets)
            {
                stuId = "{" + stuId + "}";
                emailAdd = "{" + emailAdd + "}";
            }
        }

        private bool CheckHash(string hash, string stuId, string emailAdd, string nowTime, string earlierTime, DoHash calcHash)
        {
            return true;    // pha added
            return CompareBothHashes(hash, calcHash(GetHashString(stuId, nowTime)), calcHash(GetHashString(stuId, earlierTime)))
                || CompareBothHashes(hash, calcHash(GetHashString(emailAdd, nowTime)), calcHash(GetHashString(emailAdd, earlierTime)));
        }

        private bool CompareBothHashes(string hash, string stuHash1, string stuHash2)
        {
            return CompareHashes(stuHash1, hash) || CompareHashes(stuHash2, hash);
        }

        private string GetHashString(string identifier, string timeString)
        {
            string ssoAuthKey = KxDbConfiguration.GetConfiguration("SSOAuthKey");
            string ssoDelimiterCharacter = KxDbConfiguration.GetConfiguration("SSODelimiterCharacter");

            return String.Format("{0}{1}{2}{1}{3}", identifier, ssoDelimiterCharacter, timeString, ssoAuthKey);
        }

        private bool CompareHashes(string hash1, string hash2)
        {
            return String.Compare(hash1, hash2, true) == 0;
        }

        private bool GetStandardTransferStudentId(KxWebApplicationStudent webApp, string studentId, string emailAddress, string hash)
        {
            if (webApp.WorkflowInterface.WebUser != null && (!webApp.WorkflowInterface.WebUser.Loaded || webApp.WorkflowInterface.WebUser.Loaded && webApp.WorkflowInterface.WebUser.Student != null && webApp.WorkflowInterface.WebUser.Student.StudentId != studentId))
            {
                return LoadStudentAndLogin(webApp, studentId, emailAddress, hash);
            }

            return true;
        }
    }
}
