﻿using Kx.Core.Encryption;
using Kx.Core.Exceptions;
using Kx.Objects.Student;
using Kx.Objects.Student.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace Kx.Core.Web.Student.WebApplication.ProcessManagers.SSO
{
    public class AbertaySSO : ISingleSignon
    {
        // TESTED SJA - 20160405
        public int AcademicYearId
        {
            get { return 0; }
        }

        public int ApplicationTypeId
        {
            get { return 0; }
        }

        public string StudentId { get; private set; }

        public bool PerformSingleSignon(KxWebApplicationStudent application, HttpContextWrapper context)
        {
            string ID = context.Request.QueryString["AID"];
            string mash = context.Request.QueryString["MASH"];
            bool result = false;

            StudentId = string.Empty;

            if(!string.IsNullOrWhiteSpace(ID) && !string.IsNullOrWhiteSpace(mash))
            {
                if (application.WorkflowInterface.WebUser != null && application.WorkflowInterface.WebUser.Student != null && application.WorkflowInterface.WebUser.KxStudentId > 0 && !application.WorkflowInterface.WebUser.Student.StudentId.Equals(ID, StringComparison.InvariantCultureIgnoreCase))
                {
                    context.Session.Remove("abertayin");
                    application.WorkflowInterface.WebUser.ClearLoggedInUser();
                }

                if (context.Session["abertayin"] == null)
                {
                    if (!GetTransferStudentId(application, ID, mash))
                    {
                        context.Session["abertayin"] = "true";
                        application.WorkflowInterface.CurrentWorkflow.ExecuteStep(application.WorkflowInterface.CurrentWorkflow.ErrorStep);
                    }
                    else
                    {
                        result = application.WorkflowInterface.WebUser != null && application.WorkflowInterface.WebUser.LoggedIn;
                    }
                }
            }

            return result;
        }

        private bool GetTransferStudentId(KxWebApplicationStudent webApp, string ID, string hash)
        {
            bool result = false;

            if (webApp.WorkflowInterface.WebUser != null && !webApp.WorkflowInterface.WebUser.Loaded)
            {
                KxStudent student = GetStudent(ID);

                if (CheckValidHash(student.StudentId, hash))
                {

                    if (student.KxStudentId != 0)
                    {
                        // transfer is valid, lets create the web user.
                        KxWebUserStudent webuser = new KxWebUserStudent(null);
                        webuser.LoadUserName(student.StudentId);

                        if (!webuser.Loaded)
                        {
                            webuser = new KxWebUserStudent(null);
                            webuser.UserName = student.StudentId;
                            webuser.EmailAddress = student.EmailAddress;
                            webuser.ForeName = student.ForeName;
                            webuser.SurName = student.SurName;
                            webuser.Active = 1;
                            webuser.KxStudentId = student.KxStudentId;
                            webuser.CreationDate = DateTime.Now;
                            webuser.EmailConfirmed = DateTime.Now;
                            webuser.Save();
                        }
                        webApp.WorkflowInterface.WebUser.LoadGuid(webuser.UserGuid);
                        webApp.WorkflowInterface.WebUser.LoggedIn = true;

                        result = true;

                        if (!webApp.WorkflowInterface.WebUser.CurrentApplication.IsConfirmed)
                        {
                            KxWebStudentApplication app = webApp.WorkflowInterface.WebUser.CurrentApplication;
                            // Update application from transfer record...
                            app.ForeName = student.ForeName;
                            app.SurName = student.SurName;
                            app.CurrentStudentAddress.Address1 = student.HomeAddress.Address;
                            app.CurrentStudentAddress.Address2 = student.HomeAddress.Town;
                            app.CurrentStudentAddress.Address3 = student.HomeAddress.County;
                            app.CurrentStudentAddress.Address4 = student.HomeAddress.Country;
                            app.CurrentStudentAddress.Postcode = student.HomeAddress.Postcode;
                            app.CurrentStudentAddress.TelephoneNumber = student.HomeAddress.Telephone;
                            app.Gender = student.Gender;
                            app.Dob = student.Dob;

                            webApp.WorkflowInterface.WebUser.Save();

                        }
                    }
                    else
                    {
                        KxLogger.LogActivity("AbertayManager", "Abertay Transfer; Invalid Data", hash, ID);
                    }
                }
                else
                {
                    KxLogger.LogActivity("AbertayManager", "Abertay Transfer; Invalid Hash", hash, ID);
                }

            }
            else
            {
                result = true;
            }
            return result;
        }

        private KxStudent GetStudent(string ID)
        {
            string studentID = ID.Remove(ID.IndexOf(ID.Substring(ID.Length - 8, 8)), 8);
            KxStudent student = new KxStudent(null, studentID);
            return student;
        }

        public bool CheckValidHash(string ID, string hash)
        {
            bool result = false;
            DateTime current = DateTime.Now;

            string nowtime = current.ToString("yyMMddHH");
            string hourlesstime = current.AddHours(-1).ToString("yyMMddHH");

            string sAbertayKey =
            Convert.ToString((char)82) +
            Convert.ToString((char)101) +
            Convert.ToString((char)100) +
            Convert.ToString((char)57) +
            Convert.ToString((char)78) +
            Convert.ToString((char)97) +
            Convert.ToString((char)120) +
            Convert.ToString((char)101) +
            Convert.ToString((char)108) +
            Convert.ToString((char)97) +
            Convert.ToString((char)50) +
            Convert.ToString((char)56);

            string comparehash = KxMD5.Hash(ID + nowtime + sAbertayKey);
            string comparehash2 = KxMD5.Hash(ID + hourlesstime + sAbertayKey);

            if (String.Compare(hash, comparehash, true) == 0)
            {
                result = true;
            }
            else if (String.Compare(hash, comparehash2, true) == 0)
            {
                result = true;
            }
            return result;
        }
    }
}
