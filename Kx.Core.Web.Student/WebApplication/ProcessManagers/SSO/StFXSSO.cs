﻿using Kx.Core.Configuration;
using Kx.Core.Encryption;
using Kx.Core.Exceptions;
using Kx.Core.Utility;
using Kx.Objects.Student;
using Kx.Objects.Student.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace Kx.Core.Web.Student.WebApplication.ProcessManagers.SSO
{
    public class StFXSSO : ISingleSignon
    {
        // TESTED SJA 20160405

        private delegate string DoHash(string hashString);

        private const string STR_DATEFORMAT = "yyyyMMddHH";

        public int AcademicYearId
        {
            get { return 0; }
        }

        public int ApplicationTypeId
        {
            get { return 0; }
        }

        public string StudentId
        {
            get { return string.Empty; }
        }

        public bool PerformSingleSignon(KxWebApplicationStudent application, HttpContextWrapper context)
        {
            string id = context.Request.QueryString["StudentId"]; ;
            string aidm = context.Request.QueryString["AIDM"];
            string hash = context.Request.QueryString["HASH"];

            bool result = false;


            if(!string.IsNullOrWhiteSpace(id) && !string.IsNullOrWhiteSpace(aidm) && !string.IsNullOrWhiteSpace(hash))
            {
                if (context.Session["StFXIn"] == null)
                {
                    if (application.WorkflowInterface.WebUser != null && application.WorkflowInterface.WebUser.Student != null && application.WorkflowInterface.WebUser.KxStudentId > 0 && 
                        (!application.WorkflowInterface.WebUser.Student.StudentId.Equals(id, StringComparison.InvariantCultureIgnoreCase)
                        || !application.WorkflowInterface.WebUser.UserName.Equals(aidm, StringComparison.InvariantCultureIgnoreCase)))
                    {
                        context.Session.Remove("StFXIn");
                        application.WorkflowInterface.WebUser.ClearLoggedInUser();
                    }

                    if (!GetTransferStudentId(application, id, aidm, hash))
                    {
                        context.Session["StFXIn"] = "true";
                        application.WorkflowInterface.CurrentWorkflow.ExecuteStep(application.WorkflowInterface.CurrentWorkflow.ErrorStep);
                    }
                    else
                    {
                        result = application.WorkflowInterface.WebUser != null && application.WorkflowInterface.WebUser.LoggedIn;
                    }
                }
            }

            return result;
        }

        private bool GetTransferStudentId(KxWebApplicationStudent webApp, string studentId, string aidm, string hash)
        {
            if (webApp.WorkflowInterface.WebUser != null && !webApp.WorkflowInterface.WebUser.Loaded)
            {
                return LoadStudentAndLogin(webApp, studentId, aidm, hash); ;
            }

            return true;
        }

        private bool LoadStudentAndLogin(KxWebApplicationStudent webApp, string studentId, string aidm, string hash)
        {
            if (CheckHash(aidm, hash))
            {
                if (studentId.Equals("0"))
                {
                    // Try and load previous webuser record.
                    KxWebUserStudent webuser = new KxWebUserStudent(null);
                    webuser.LoadUserID(KxStringUtils.ConvertToInt(aidm, 0));

                    if (!webuser.Loaded)
                    {
                        // Create New WebUser Record - Put AIDM in UserId. -- Return True
                        webuser = new KxWebUserStudent(null);
                        webuser = CreateNewWebUserWithAIDM(aidm);
                    }

                    LogInToWorkflow(webApp, webuser);
                    return true;
                }
                else
                {
                    KxStudent student = new KxStudent(null, studentId);
                    if (student.KxStudentId != 0)
                    {
                        return ValidUserCreateWebUserRecord(webApp, student);
                    }

                }

            }

            LogInvalidData(studentId, aidm, hash);
            return false;
        }
        #region Hash Checking
        
        private bool CheckHash(string aidm, string hash)
        {
            DateTime current = DateTime.Now;
            string nowTime = current.ToString(STR_DATEFORMAT);
            string earlierTime = current.AddHours(-1).ToString(STR_DATEFORMAT);

            try
            {
                return CheckHash(aidm, hash, nowTime, earlierTime, s => KxMD5.Hash(s));
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "CheckHash", ex.Message, "");
            }

            return false;
        }

        private bool CheckHash(string aidm, string hash, string nowTime, string earlierTime, DoHash calcHash)
        {
            return CompareBothHashes(hash, calcHash(GetHashString(aidm, nowTime)), calcHash(GetHashString(aidm, earlierTime)));
        }

        private bool CompareBothHashes(string hash, string stuHash1, string stuHash2)
        {
            return CompareHashes(stuHash1, hash) || CompareHashes(stuHash2, hash);
        }

        private string GetHashString(string identifier, string timeString)
        {
            string ssoAuthKey = KxDbConfiguration.GetConfiguration("SSOAuthKey");

            return String.Format("{{{0}}}{1}{2}", identifier, timeString, ssoAuthKey);
        }

        private bool CompareHashes(string hash1, string hash2)
        {
            return String.Compare(hash1, hash2, true) == 0;
        }

        private void LogInvalidData(string studentId, string aidm, string hash)
        {
            KxLogger.LogActivity("StFx SSO", "SSO Transfer; Invalid Data", String.Format("Parameter Details -- Hash: {0}", hash), String.Format("Student Id: {0} AIDM: {1}", studentId, aidm));
        }

        #endregion

        private bool ValidUserCreateWebUserRecord(KxWebApplicationStudent webApp, KxStudent student)
        {
            // Valid Transfer - Create WebUser record
            KxWebUserStudent webuser = new KxWebUserStudent(null);
            webuser.LoadUserName(student.StudentId);

            if (!webuser.Loaded)
            {
                webuser = CreateNewWebUserFromStudent(student, webuser);
            }

            LogInToWorkflow(webApp, webuser);

            if (!webApp.WorkflowInterface.WebUser.CurrentApplication.IsConfirmed)
            {
                UpdateApplicationFromTransferRecord(webApp, student);
            }

            return true;
        }

        private void UpdateApplicationFromTransferRecord(KxWebApplicationStudent webApp, KxStudent student)
        {
            KxWebStudentApplication app = webApp.WorkflowInterface.WebUser.CurrentApplication;
            app.ForeName = student.ForeName;
            app.SurName = student.SurName;

            webApp.WorkflowInterface.WebUser.Save();
        }

        private void LogInToWorkflow(KxWebApplicationStudent webApp, KxWebUserStudent webuser)
        {
            webApp.WorkflowInterface.WebUser.LoadGuid(webuser.UserGuid);
            webApp.WorkflowInterface.WebUser.LoggedIn = true;
        }

        private KxWebUserStudent CreateNewWebUserWithAIDM(string aidm)
        {
            KxWebUserStudent webuser = new KxWebUserStudent(null);
            webuser.Active = 1;
            webuser.KxStudentId = 0;
            webuser.CreationDate = DateTime.Now;
            webuser.EmailConfirmed = DateTime.Now;
            webuser.UserId = KxStringUtils.ConvertToInt(aidm, 0);
            webuser.PersonId = 0;
            webuser.Save();
            return webuser;
        }

        private KxWebUserStudent CreateNewWebUserFromStudent(KxStudent student, KxWebUserStudent webuser)
        {
            webuser = new KxWebUserStudent(null);
            webuser.UserName = student.StudentId;
            webuser.EmailAddress = student.EmailAddress;
            webuser.ForeName = student.ForeName;
            webuser.SurName = student.SurName;
            webuser.Active = 1;
            webuser.KxStudentId = student.KxStudentId;
            webuser.CreationDate = DateTime.Now;
            webuser.EmailConfirmed = DateTime.Now;
            webuser.PersonId = student.PersonId;
            webuser.Save();
            return webuser;
        }
    }
}
