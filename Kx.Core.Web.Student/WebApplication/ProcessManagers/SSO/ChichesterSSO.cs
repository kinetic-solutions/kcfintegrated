﻿using Kx.Core.Encryption;
using Kx.Core.Exceptions;
using Kx.Objects.Student;
using Kx.Objects.Student.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace Kx.Core.Web.Student.WebApplication.ProcessManagers.SSO
{
    public class ChichesterSSO : ISingleSignon
    {
        // TESTED SJA 20160405
        public int AcademicYearId
        {
            get { return 0; }
        }

        public int ApplicationTypeId
        {
            get { return 0; }
        }

        public string StudentId { get; private set; }

        public bool PerformSingleSignon(KxWebApplicationStudent application, HttpContextWrapper context)
        {
            Kx.Core.Web.Providers.AuditProvider.ConfiguredAddToAudit("KxWebApplicationStudent", "CheckForUserTransfer", "CID, HASH and Portal");

            string ID = context.Request.QueryString["CID"];
            string hash = context.Request.QueryString["HASH"];
            string portal = context.Request.QueryString["Portal"];
            StudentId = "";
            bool result = false;

            if(!string.IsNullOrWhiteSpace(ID) && !string.IsNullOrWhiteSpace(hash))
            {
                if (application.WorkflowInterface.WebUser != null && application.WorkflowInterface.WebUser.Student != null && application.WorkflowInterface.WebUser.KxStudentId > 0 && !application.WorkflowInterface.WebUser.Student.StudentId.Equals(ID, StringComparison.InvariantCultureIgnoreCase))
                {
                    context.Session.Remove("chichesterin");
                    application.WorkflowInterface.WebUser.ClearLoggedInUser();
                }

                if (context.Session["chichesterin"] == null)
                {
                    if (!GetTransferStudentId(application, ID, hash, portal))
                    {
                        context.Session["chichesterin"] = "true";
                        application.WorkflowInterface.CurrentWorkflow.ExecuteStep(application.WorkflowInterface.CurrentWorkflow.StartStep);
                    }
                    else
                    {
                        result = application.WorkflowInterface.WebUser != null && application.WorkflowInterface.WebUser.LoggedIn;
                    }
                }
            }

            return result;
        }

        private bool GetTransferStudentId(KxWebApplicationStudent webApp, string ID, string hash, string portal)
        {
            bool result = false;

            if (webApp.WorkflowInterface.WebUser != null && !webApp.WorkflowInterface.WebUser.Loaded)
            {
                KxStudent student = GetStudent(ID);

                if (CheckValidHash(student.StudentId, hash))
                {
                    if (student.KxStudentId != 0)
                    {
                        // transfer is valid, lets create the web user.
                        KxWebUserStudent webuser = new KxWebUserStudent(null);
                        webuser.LoadUserName(student.StudentId);


                        KxStudentLoginManager manager = new KxStudentLoginManager(webApp);

                        if (!webuser.Loaded)
                        {
                            manager.CreateActiveWebUserRecordForSSOStudent(student.StudentId, student.EmailAddress, student.ForeName, student.SurName, student.KxStudentId, ref webuser);
                        }


                        manager.LogInToWorkflow(webApp, webuser);

                        result = true;

                        KxWebStudentApplication app = webApp.WorkflowInterface.WebUser.CurrentApplication;

                        if (!app.IsConfirmed)
                        {
                            // Update application from transfer record...
                            manager.UpdateApplicationFromStudentData(app, student.Title, student.ForeName, student.SurName, student.HomeAddress.Address, student.HomeAddress.Town, student.HomeAddress.County, student.HomeAddress.Country, student.HomeAddress.Postcode, student.HomeAddress.Mobile, student.HomeAddress.Telephone, student.Gender, student.Dob);
                            KxLogger.LogActivity("ChichesterManager", "Chichester Transfer; Workflow user:", webApp.WorkflowInterface.WebUser.KxStudentId.ToString(), webApp.WorkflowInterface.WebUser.Student.StudentId);
                        }

                        app.UserDefinedValues.SetUDF(18, portal);
                        webApp.WorkflowInterface.WebUser.Save();
                    }
                    else
                    {
                        KxLogger.LogActivity("ChichesterManager", "Chichester Transfer; Invalid Data", hash, ID);
                    }
                }
                else
                {
                    KxLogger.LogActivity("ChichesterManager", "Chichester Transfer; Invalid Hash", hash, ID);
                }
            }
            else
            {
                result = true;
            }

            return result;
        }
        private KxStudent GetStudent(string ID)
        {
            KxStudent student = new KxStudent(null, ID);
            return student;
        }

        public bool CheckValidHash(string ID, string hash)
        {
            bool result = false;
            DateTime current = DateTime.Now;

            string nowtime = current.ToString("yyyyMMddHH");
            string hourlesstime = current.AddHours(-1).ToString("yyyyMMddHH");

            string sChichKey =
                Convert.ToString((char)112) +
                Convert.ToString((char)104) +
                Convert.ToString((char)111) +
                Convert.ToString((char)101) +
                Convert.ToString((char)110) +
                Convert.ToString((char)105) +
                Convert.ToString((char)120) +
                Convert.ToString((char)108) +
                Convert.ToString((char)52) +
                Convert.ToString((char)48) +
                Convert.ToString((char)116) +
                Convert.ToString((char)104);

            string comparehash = KxMD5.Hash(ID + nowtime + sChichKey);
            string comparehash2 = KxMD5.Hash(ID + hourlesstime + sChichKey);

            if (String.Compare(hash, comparehash, true) == 0)
            {
                result = true;
            }
            else if (String.Compare(hash, comparehash2, true) == 0)
            {
                result = true;
            }

            return result;
        }
    }
}
