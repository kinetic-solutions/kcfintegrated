﻿using Kx.Core.Data;
using Kx.Core.Exceptions;
using Kx.Core.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Windows.Forms.VisualStyles;
using System.Xml;

namespace Kx.Core.Web.Student.WebApplication.ProcessManagers.SSO
{
    public class StudentTransferTableSSO : ISingleSignon
    {
        public int AcademicYearId { get; private set; }

        public int ApplicationTypeId { get; private set; }

        public string StudentId { get; private set; }

        public bool PerformSingleSignon(KxWebApplicationStudent application, HttpContextWrapper context)
        {
            bool result = false;
            StudentId = GetTransferStudentId(context.Request.QueryString["ID"]);

            if (application.WorkflowInterface.WebUser != null 
                && application.WorkflowInterface.WebUser.Loaded 
                && application.WorkflowInterface.WebUser.Student != null 
                && (application.WorkflowInterface.WebUser.Student.StudentId.ToLower().Trim() != StudentId.ToLower().Trim() || string.IsNullOrWhiteSpace(StudentId)))
            {
                application.WorkflowInterface.WebUser.Loaded = false;
                if (string.IsNullOrWhiteSpace(StudentId))
                {
                    throw new KxAuthenticationException("Invalid ID passed");
                }
            }
            else
            {
                result = true;
            }

            return result;
        }


        private string GetTransferStudentId(string eStudentID)
        {
            string result = string.Empty;

            KxData data = new KxData("studenttransfer", "StudentTransfer", TargetType.Table, ActionType.Select);

            data.AddKeyField("studentguid", eStudentID, KxValueType.String);

            if (data.Execute())
            {
                foreach (XmlNode node in data.GetNodesFromData())
                {
                    if (node != null)
                    {
                        result = KxXmlUtils.GetNodeValue(node, "studentid", "");
                        ApplicationTypeId = KxXmlUtils.GetNodeValue(node, "applicationtypeid", 0);
                        AcademicYearId = KxXmlUtils.GetNodeValue(node, "academicyearid", 0);
                    }
                }
            }
            return result;
        }
    }
}
