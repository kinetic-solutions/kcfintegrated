﻿using Kx.Objects.Student;
using Kx.Objects.Student.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace Kx.Core.Web.Student.WebApplication.ProcessManagers.SSO
{
    public class ShibbolethSSO : ISingleSignon
    {
        private const string STR_ShibbolethSessionKey = "ShibbolethValue";
        public int AcademicYearId
        {
            get { return 0; }
        }

        public int ApplicationTypeId
        {
            get { return 0; }
        }

        public string StudentId
        {
            get { return string.Empty; }
        }

        public bool PerformSingleSignon(KxWebApplicationStudent application, HttpContextWrapper context)
        {
            string id = context.Request.ServerVariables[System.Configuration.ConfigurationManager.AppSettings["ShibbolethHeaderParam"]];

            bool result = false;

            if (!string.IsNullOrWhiteSpace(context.Request.ServerVariables[System.Configuration.ConfigurationManager.AppSettings["ShibbolethHeaderParam"]]) && HttpContext.Current.Session[STR_ShibbolethSessionKey] == null)
            {
                if (ValidateAndLoadStudent(application, id))
                {
                    application.WorkflowInterface.CurrentWorkflow.ExecuteStep(application.WorkflowInterface.CurrentWorkflow.StartStep);
                    result = true;
                }
            }

            return result;
        }

        public bool ValidateAndLoadStudent(KxWebApplicationStudent webApp, string udfValue)
        {
            if (webApp.WorkflowInterface.WebUser != null && !webApp.WorkflowInterface.WebUser.Loaded)
            {
                if (HttpContext.Current.Session[STR_ShibbolethSessionKey] != null &&
                    HttpContext.Current.Session[STR_ShibbolethSessionKey].ToString() != udfValue)
                {
                    return false;
                }

                KxStudent student = new KxStudent();
                student.LoadFromUDFValue(udfValue, System.Configuration.ConfigurationManager.AppSettings["StudentLookupUDFFieldName"]);
                KxWebUserStudent webStudent = new KxWebUserStudent();

                KxStudentLoginManager manager = new KxStudentLoginManager(webApp);

                webStudent.LoadStudent(student.KxStudentId);
                if (!webStudent.Loaded)
                {
                    manager.CreateActiveWebUserRecordForSSOStudent(udfValue, student.EmailAddress, student.ForeName, student.SurName, student.KxStudentId, ref webStudent);
                }
                KxWebStudentApplication app = webApp.WorkflowInterface.WebUser.CurrentApplication;

                if (!app.IsConfirmed)
                {
                    // Update application from transfer record...
                    manager.UpdateApplicationFromStudentData(app, student.Title, student.ForeName, student.SurName, student.HomeAddress.Address, student.HomeAddress.Town, student.HomeAddress.County, student.HomeAddress.Country, student.HomeAddress.Postcode, student.HomeAddress.Mobile, student.HomeAddress.Telephone, student.Gender, student.Dob);
                }
                manager.LogInToWorkflow(webApp, webStudent);
            }
            else
            {
                if (HttpContext.Current.Session[STR_ShibbolethSessionKey] == null ||
                    HttpContext.Current.Session[STR_ShibbolethSessionKey].ToString() != udfValue)
                {
                    return false;
                }
            }

            return true;
        }
    }
}
