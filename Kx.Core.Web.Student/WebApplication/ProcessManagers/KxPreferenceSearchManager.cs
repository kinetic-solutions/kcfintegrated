using System;
using System.Collections.Generic;
using System.Text;
using Kx.Core.Data;
using Kx.Core.Exceptions;
using Kx.Core.Utility;
using Kx.Core.Reflection;
using Kx.Core.Web.Student.WebApplication.BaseClass;
using Kx.Core.Web.Student.WebApplication.PageRenderer;
using Kx.Objects.Student;
using Kx.Objects.Student.Web;


namespace Kx.Core.Web.Student.WebApplication.ProcessManagers
{
    public class KxPreferenceSearchManager : WebStudentBaseClass
    {
        #region Attributes & properties

        private KxWebStudentPreferences basket = new KxWebStudentPreferences(null);
        private KxWebStudentPreferences results = new KxWebStudentPreferences(null);
        private string sortType = "";
        private bool initialised = false;
        private bool preferenceExists = false;
        private bool basketFull = false;
        private int duplicateAreas;
        private int duplicateCustomValue3;
        private int basketSize;
        private bool areasExceeded = false;
        private bool areaCustomValue3Exceeded = false;

        protected int siteId = 0;
        protected int areaId = 0;
        protected int blockId = 0;
        protected int bedroomTypeId = 0;
        protected int floor = 0;
        protected int singleSexArea = 2;
        protected int smoking = 2;
        protected int courseTypeId = 0;
        protected int customField1 = 2;
        protected int customField2 = 2;
        protected string customValue1 = "";
        protected string customValue2 = "";
        protected string customValue3 = "";
        protected string customValue4 = "";
        protected string customValue5 = "";
        protected string customValue6 = "";
        protected string customValue7 = "";
        protected string customValue8 = "";
        protected bool hasSearched = false;

        public bool AreasExceeded
        {
            get
            { return areasExceeded; }
            set
            { areasExceeded = value; }
        }

        public bool AreaCustomValue3Exceeded
        {
            get
            { return areaCustomValue3Exceeded; }
            set
            { areaCustomValue3Exceeded = value; }
        }

        public int SiteId
        {
            get
            { return siteId; }
            set
            { siteId = value; }
        }
        public int AreaId
        {
            get
            { return areaId; }
            set
            { areaId = value; }
        }
        public int BlockId
        {
            get
            { return blockId; }
            set
            { blockId = value; }
        }
        public int BedroomTypeId
        {
            get
            { return bedroomTypeId; }
            set
            { bedroomTypeId = value; }
        }
        public int Floor
        {
            get
            { return floor; }
            set
            { floor = value; }
        }
        public int SingleSexArea
        {
            get
            { return singleSexArea; }
            set
            {
                singleSexArea = value;
            }
        }
        public int Smoking
        {
            get
            { return smoking; }
            set
            { smoking = value; }
        }
        public int CourseTypeId
        {
            get
            { return courseTypeId; }
            set
            { courseTypeId = value; }
        }
        public int CustomField1
        {
            get
            { return customField1; }
            set
            { customField1 = value; }
        }
        public int CustomField2
        {
            get
            { return customField2; }
            set
            { customField2 = value; }
        }
        public string CustomValue1
        {
            get
            { return customValue1; }
            set
            { customValue1 = value; }
        }
        public string CustomValue2
        {
            get
            { return customValue2; }
            set
            { customValue2 = value; }
        }
        public string CustomValue3
        {
            get
            { return customValue3; }
            set
            { customValue3 = value; }
        }
        public string CustomValue4
        {
            get
            { return customValue4; }
            set
            { customValue4 = value; }
        }
        public string CustomValue5
        {
            get
            { return customValue5; }
            set
            { customValue5 = value; }
        }
        public string CustomValue6
        {
            get
            { return customValue6; }
            set
            { customValue6 = value; }
        }
        public string CustomValue7
        {
            get
            { return customValue7; }
            set
            { customValue7 = value; }
        }
        public string CustomValue8
        {
            get
            { return customValue8; }
            set
            { customValue8 = value; }
        }
        public string CustomField1Name
        {
            get
            { return basket.CustomField1Name; }
        }
        public string CustomField2Name
        {
            get
            { return basket.CustomField2Name; }
        }
        public string CustomValue1Name
        {
            get
            { return basket.CustomValue1Name; }
        }
        public string CustomValue1Options
        {
            get
            { return basket.CustomValue1Options; }
        }
        public string CustomValue2Name
        {
            get
            { return basket.CustomValue2Name; }
        }
        public string CustomValue2Options
        {
            get
            { return basket.CustomValue2Options; }
        }
        public string CustomValue3Name
        {
            get
            { return basket.CustomValue3Name; }
        }
        public string CustomValue3Options
        {
            get
            { return basket.CustomValue3Options; }
        }
        public string CustomValue4Name
        {
            get
            { return basket.CustomValue4Name; }
        }
        public string CustomValue4Options
        {
            get
            { return basket.CustomValue4Options; }
        }
        public string CustomValue5Name
        {
            get
            { return basket.CustomValue5Name; }
        }
        public string CustomValue5Options
        {
            get
            { return basket.CustomValue5Options; }
        }
        public string CustomValue6Name
        {
            get
            { return basket.CustomValue6Name; }
        }
        public string CustomValue6Options
        {
            get
            { return basket.CustomValue6Options; }
        }
        public string CustomValue7Name
        {
            get
            { return basket.CustomValue7Name; }
        }
        public string CustomValue7Options
        {
            get
            { return basket.CustomValue7Options; }
        }
        public string CustomValue8Name
        {
            get
            { return basket.CustomValue8Name; }
        }
        public string CustomValue8Options
        {
            get
            { return basket.CustomValue8Options; }
        }
        public bool PreferenceExists
        {
            get
            {
                return preferenceExists;
            }
            set
            {
                preferenceExists = value;
            }
        }
        public bool BasketFull
        {
            get
            {
                return basketFull;
            }
            set
            {
                basketFull = value;
            }
        }

        public string SortType
        {
            get
            { return sortType; }
            set
            {
                if (sortType != value)
                {
                    Results.SortType = value;
                    Results.Sort();
                }
                sortType = value;
            }
        }

        public KxWebStudentPreference CurrentSearchItem
        {
            get
            {
                return results.PreferenceByIndex(ObjectTranslator.CurrentPrefSearchIndex);
            }
        }

        public bool HasSearched
        {
            get
            { return hasSearched; }
        }

        public bool HasResults
        {
            get
            { return Results.Items.Count > 0; }
        }

        public bool HasBasketItem
        {
            get
            { return Basket.Items.Count > 0; }
        }

        public KxWebStudentPreference CurrentBasketItem
        {
            get
            {
                return basket.PreferenceByIndex(ObjectTranslator.CurrentPrefBasketIndex);
            }
        }

        public bool Initialised
        {
            get
            { return initialised; }
            set
            { initialised = value; }
        }

        #endregion

        public KxPreferenceSearchManager(KxWebApplicationStudent webApp)
            : base(webApp)
        {
            basket.LoadCustomSettings();
        }

        public KxWebStudentPreferences Basket
        {
            get
            {
                return basket;
            }
        }

        public KxWebStudentPreferences Results
        {
            get
            {
                return results;
            }
        }

        public void DoSearch(int resultLimit)
        {
            hasSearched = true;

            KxData search = new KxData("search", "PreferencesSearch", TargetType.StoredProc);

            search.Parameters.Add(new KxDataValue("academicyearid", WebUser.CurrentAcademicYear, KxValueType.Int));

            search.Parameters.Add(new KxDataValue("kxstudentid", WebUser.KxStudentId, KxValueType.Int));

            if (courseTypeId != 0)
            {
                search.Parameters.Add(new KxDataValue("coursetypeid", courseTypeId, KxValueType.Int));
            }

            if (siteId != 0)
            {
                search.Parameters.Add(new KxDataValue("siteId", siteId, KxValueType.Int));
            }
            if (areaId != 0)
            {
                search.Parameters.Add(new KxDataValue("areaId", areaId, KxValueType.Int));
            }
            if (blockId != 0)
            {
                search.Parameters.Add(new KxDataValue("blockId", blockId, KxValueType.Int));
            }
            if (bedroomTypeId != 0)
            {
                search.Parameters.Add(new KxDataValue("bedroomTypeId", bedroomTypeId, KxValueType.Int));
            }
            if (floor != 0)
            {
                search.Parameters.Add(new KxDataValue("floor", floor, KxValueType.Int));
            }
            if (singleSexArea != 2)
            {
                search.Parameters.Add(new KxDataValue("singleSexArea", singleSexArea, KxValueType.Int));
            }
            if (smoking != 2)
            {
                search.Parameters.Add(new KxDataValue("smoking", smoking == 1, KxValueType.Boolean));
            }
            if (customField1 != 2)
            {
                search.Parameters.Add(new KxDataValue("customField1", customField1 == 1, KxValueType.Boolean));
            }
            if (customField2 != 2)
            {
                search.Parameters.Add(new KxDataValue("customField2", customField2 == 1, KxValueType.Boolean));
            }
            if (customValue1 != "")
            {
                search.Parameters.Add(new KxDataValue("customValue1", customValue1, KxValueType.String));
            }
            if (customValue2 != "")
            {
                search.Parameters.Add(new KxDataValue("customValue2", customValue2, KxValueType.String));
            }
            if (customValue3 != "")
            {
                search.Parameters.Add(new KxDataValue("customValue3", customValue3, KxValueType.String));
            }
            if (customValue4 != "")
            {
                search.Parameters.Add(new KxDataValue("customValue4", customValue4, KxValueType.String));
            }
            if (customValue5 != "")
            {
                search.Parameters.Add(new KxDataValue("customValue5", customValue5, KxValueType.String));
            }
            if (customValue6 != "")
            {
                search.Parameters.Add(new KxDataValue("customValue6", customValue6, KxValueType.String));
            }
            if (customValue7 != "")
            {
                search.Parameters.Add(new KxDataValue("customValue7", customValue7, KxValueType.String));
            }
            if (customValue8 != "")
            {
                search.Parameters.Add(new KxDataValue("customValue8", customValue8, KxValueType.String));
            }

            if (search.Execute())
            {
                results.LoadFromXmlSearch(basket, search.XmlResults, "search", resultLimit);
            }
        }

        private bool CanAddToBasket(KxWebStudentPreference pref)
        {
            bool result = basket.PreferenceByIndex(pref.PreferenceId) == null && basket.Items.Count < basketSize;

            /* 117884 : Changes to utilise Blocks following restructuring of site/area/blocks within Imperial */

            if (result)
            {
                if (pref.BlockId > 0)
                {
                    if (basket.GetBlockCount(pref.BlockId) < duplicateAreas)
                    {
                        if (!String.IsNullOrEmpty(pref.CustomValue3))
                        {
                            if (basket.GetBlockCustomValue3Count(pref.BlockId, pref.CustomValue3) >= duplicateCustomValue3)
                            {
                                AreaCustomValue3Exceeded = true;
                                result = false;
                            }
                        }
                    }
                    else
                    {
                        AreasExceeded = true;
                        result = false;
                    }

                }
            }

            return result;
        }

        public void AddSelected(int BasketSize, int DuplicateAreas, int DuplicateCustomValue3)
        {
            PreferenceExists = false;
            BasketFull = false;
            AreasExceeded = false;
            AreaCustomValue3Exceeded = false;

            basketSize = BasketSize;
            duplicateAreas = DuplicateAreas;
            duplicateCustomValue3 = DuplicateCustomValue3;

            try
            {
                foreach (KxWebStudentPreference pref in results.Items)
                {
                    if (pref.SelectionType == SelectedType.Selected)
                    {
                        if (CanAddToBasket(pref))
                        {
                            pref.SelectionType = SelectedType.Unselected;
                            KxWebStudentPreference newpref = (KxWebStudentPreference)KxReflection.CloneObject(pref);

                            newpref.UIObject = null;
                            newpref.SearchDescription = pref.SearchDescription;
                            if (pref.BlockUrl != null)
                            {
                                newpref.BlockUrl = pref.BlockUrl;
                            }

                            newpref.CustomValue1 = pref.CustomValue1;
                            newpref.CustomValue2 = pref.CustomValue2;
                            newpref.CustomValue3 = pref.CustomValue3;
                            newpref.CustomValue4 = pref.CustomValue4;
                            newpref.CustomValue5 = pref.CustomValue5;
                            newpref.CustomValue6 = pref.CustomValue6;
                            newpref.CustomValue7 = pref.CustomValue7;
                            newpref.CustomValue8 = pref.CustomValue8;

                            newpref.PreferenceNumber = basket.Items.Count + 1;
                            newpref.Parent = basket;
                            newpref.PreferenceId = pref.PreferenceId;
                            basket.Items.Add(newpref);
                            pref.SelectionType = SelectedType.Hidden;
                        }
                        else
                        {
                            PreferenceExists = basket.PreferenceByIndex(pref.PreferenceId) != null;
                            BasketFull = basket.Items.Count >= BasketSize;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "AddSelected", ex.Message, string.Format("BasketSize: {0}, DuplicateAreas: {1}, DuplicateCustomValue3: {2}", BasketSize, DuplicateAreas, DuplicateCustomValue3));
            }
        }

        public bool RemoveFromBasket()
        {
            return basket.RemoveSelected();
        }

        public void PrioritiseBasket(bool up)
        {
            //basket.SortType = sorttype;
            //basket.Sort();

            if (up)
            {
                if (basket.Items.Count > 1)
                {
                    for (int i = 1; i < basket.Items.Count; i++)
                    {
                        KxWebStudentPreference temp2 = basket.Items[i - 1];

                        if (basket.Items[i].SelectionType == SelectedType.Selected)
                        {
                            int lowpref = basket.Items[i].PreferenceNumber;
                            int highpref = basket.Items[i - 1].PreferenceNumber;

                            basket.Items[i - 1] = basket.Items[i];
                            basket.Items[i] = temp2;

                            basket.Items[i].PreferenceNumber = lowpref;
                            basket.Items[i - 1].PreferenceNumber = highpref;
                        }
                    }
                }
            }
            else
            {
                if (basket.Items.Count > 1)
                {
                    for (int i = basket.Items.Count - 2; i >= 0; i--)
                    {
                        KxWebStudentPreference temp2 = basket.Items[i + 1];

                        if (basket.Items[i].SelectionType == SelectedType.Selected)
                        {
                            int lowpref = basket.Items[i].PreferenceNumber;
                            int highpref = basket.Items[i + 1].PreferenceNumber;

                            basket.Items[i + 1] = basket.Items[i];
                            basket.Items[i] = temp2;

                            basket.Items[i].PreferenceNumber = lowpref;
                            basket.Items[i + 1].PreferenceNumber = highpref;
                        }
                    }
                }
            }



        }

        public void SaveBasket()
        {
            // Copy Basket To Preferences.....
            KxWebStudentApplication app = WebUser.CurrentApplication;

            app.Preferences.DeleteAll();

            foreach (KxWebStudentPreference pref in basket.Items)
            {
                pref.UIObject = null;
                KxWebStudentPreference newpref = new KxWebStudentPreference(null);
                KxReflection.CloneObject(pref, newpref);

                newpref.Parent = app.Preferences;
                newpref.ApplicationGuid = app.ApplicationGuid;
                newpref.PreferenceNumber = app.Preferences.Items.Count + 1;
                newpref.PreferenceId = 0;
                newpref.Temporary = false;
                newpref.Changed = true;

                newpref.CustomValue1 = pref.CustomValue1;
                newpref.CustomValue2 = pref.CustomValue2;
                newpref.CustomValue3 = pref.CustomValue3;
                newpref.CustomValue4 = pref.CustomValue4;
                newpref.CustomValue5 = pref.CustomValue5;
                newpref.CustomValue6 = pref.CustomValue6;
                newpref.CustomValue7 = pref.CustomValue7;
                newpref.CustomValue8 = pref.CustomValue8;

                app.Preferences.Changed = true;
                app.Preferences.Items.Add(newpref);
            }
            //Set Application to Confirmed...
            app.Changed = true;
            app.ChildChanged = true;
            app.Preferences.Changed = true;
            app.Preferences.ChildChanged = true;
        }

        public void InitaliseBasket()
        {
            KxWebStudentApplication app = WebUser.CurrentApplication;

            basket.DeleteAll();

            foreach (KxWebStudentPreference pref in app.Preferences.Items)
            {
                if (!pref.Temporary)
                {
                    pref.UIObject = null;
                    KxWebStudentPreference newpref = new KxWebStudentPreference(null);
                    KxReflection.CloneObject(pref, newpref);

                    newpref.Parent = basket;
                    newpref.PreferenceNumber = basket.Items.Count + 1;
                    //newpref.PreferenceId = basket.Items.Count + 1; 
                    newpref.Temporary = true;
                    newpref.Changed = false;
                    if (!basket.DuplicateExists(newpref))
                    {
                        basket.Items.Add(newpref);
                    }
                }
            }
        }

        public void ClearResults()
        {
            try
            {
                results.Items.Clear();
                hasSearched = false;
            }
            catch { }
        }

        public void ClearBasket()
        {
            try
            {
                basket.Items.Clear();
            }
            catch { }
        }
    }
}
