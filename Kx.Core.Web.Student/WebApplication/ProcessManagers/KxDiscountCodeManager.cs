﻿using Kx.Core.Localisation;
using Kx.Core.Web.Student.WebApplication.BaseClass;
using Kx.Objects.Student.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kx.Core.Web.Student.WebApplication.ProcessManagers
{
    public class KxDiscountCodeManager : WebStudentBaseClass
    {
        List<KxDiscountCode> lstKxDiscountCodes = null;

        public string DiscountCode { get; set; }
        public string[] AppliedDiscountCodes { get => GetAppliedDiscountCodes().Select(x => x.DiscountCode).ToArray(); }

        public KxDiscountCodeManager(KxWebApplicationStudent webApp)
            : base(webApp)
        {
        }

        public bool IsValidDiscountCode()
        {
            bool result = false;
            result = ValidateDiscountCode();
            return result;
        }

        private bool ValidateDiscountCode()
        {
            //DiscountCodeError = string.Empty;
            // Step1: validates the discount code is not null or empty
            if (string.IsNullOrWhiteSpace(DiscountCode))
            {
                return false;
            }

            // Step2: validates if discount code has already been applied
            var appliedCode = AppliedDiscountCodes.FirstOrDefault(x => x == DiscountCode);
            if (appliedCode == DiscountCode)
            {
                // already applied
                return false;
            }

            // Step3: fetch discountCode and validate if Discount code is active 
            var discountCode = KxDiscountCode.GetDiscountCode(DiscountCode);
            if (!KxDiscountCode.IsDiscountCodeValid(discountCode))
            {
                return false;
            }

            bool HasPreferenceAndCodeConfigured = false;
            foreach (var item in WebUser.CurrentApplication.Preferences.Items)
            {
                bool isValid = KxDiscountCode.IsPreferenceValid(item.PreferenceAvailabilityId, discountCode.DiscountCodeID);
                if (isValid)
                {
                    HasPreferenceAndCodeConfigured = true;
                    break;
                }
            }

            if (!HasPreferenceAndCodeConfigured)
            {
                return false;
            }

            //Step5: Insert a record in kcf.studentdiscountcodes table
            var allocation = WebUser.AcceptedAllocation ?? WebUser.PendingAllocation;
            var allocationGuid = allocation == null ? Guid.Empty : allocation.WebAllocationGuid;
            var applicationGuid = WebUser.CurrentApplication.ApplicationGuid;

            var isAdded = KxDiscountCode.AddAppliedDiscountCode(WebUser.UserGuid, allocationGuid, applicationGuid, discountCode);

            if (isAdded)
                return true;

            return false;

        }

        public List<KxDiscountCode> GetAppliedDiscountCodes()
        {


            //Step5: Insert a record in kcf.studentdiscountcodes table
            var currentAllocation = WebUser.AcceptedAllocation ?? WebUser.PendingAllocation;

            var allocationGuid = currentAllocation == null ? Guid.Empty : currentAllocation.WebAllocationGuid;
            
            return KxDiscountCode.GetAppliedDiscountCodes(WebUser.UserGuid, allocationGuid);
        }


        public void Reset()
        {
            DiscountCode = string.Empty;
        }
    }
}
