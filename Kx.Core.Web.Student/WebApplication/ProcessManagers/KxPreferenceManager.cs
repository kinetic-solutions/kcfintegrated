﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;
using Kx.Core.Web.Student.WebApplication.BaseClass;
using Kx.Objects.Student.Web;
using System.Web.UI.WebControls;
using System.Xml;
using Kx.Core.Reflection;
using Kx.Core.Data;
using Kx.Core.Exceptions;
using Kx.Core.Utility;

namespace Kx.Core.Web.Student.WebApplication.ProcessManagers
{
    public class KxPreferenceManager : WebStudentBaseClass
    {
        private KxWebStudentPreferences _results = new KxWebStudentPreferences(null);

        public int MinPreferences { get; set; }
        public int MaxPreferences { get; set; }

        public string CustomPreferenceProcedure { get; set; }

        public string Filter1 { get; set; }
        public string Filter2 { get; set; }
        public string Filter3 { get; set; }
        public string Filter4 { get; set; }

        public KxWebStudentPreference CurrentResultItem
        {
            get
            {
                var result = _results.PreferenceByIndex(ObjectTranslator.CurrentPrefSearchIndex);
                if (result == null)
                {
                    result = _results.PreferenceByIndex(ObjectTranslator.LastPrefSearchIndex);
                }

                return result;
            }
        }

        public KxWebStudentPreferences Results
        {
            get
            {
                return _results;
            }
        }

        public KxPreferenceManager(KxWebApplicationStudent webApp)
            : base(webApp)
        {
            
        }

        private static bool sessionParamWorks = true;

        public void GetPreferences()
        {
            string sp = !string.IsNullOrEmpty(CustomPreferenceProcedure) ? CustomPreferenceProcedure : "WebGetPreferences"; // pha

            new KxData().LogDebug("CustomPreferenceProcedure", sp); // pha

            KxData search = new KxData("pref", sp, TargetType.StoredProc, ActionType.ExecuteProcedure);
            search.AddParameter("applicationtypeid", WebUser.CurrentApplication.ApplicationTypeId, KxValueType.Int);
            search.AddParameter("academicyearid", WebUser.CurrentApplication.AcademicYearId, KxValueType.Int);
            search.AddParameter("sessionid", WebUser.CurrentApplication.SessionID, KxValueType.Int);
            search.AddParameter("filter1", Filter1, KxValueType.String, 50);
            search.AddParameter("filter2", Filter2, KxValueType.String, 50);
            search.AddParameter("filter3", Filter3, KxValueType.String, 50);
            search.AddParameter("filter4", Filter4, KxValueType.String, 50);

            MinPreferences = -1;
            MaxPreferences = -1;

            if (search.Execute())
            {
                LoadPreferenceSearchResults(search);
            }
            else
            {
                GetPreferences_OldMethod();
            }
        }

        private void GetPreferences_OldMethod()
        {
            sessionParamWorks = false;
            KxData search2 = new KxData("pref",
                !string.IsNullOrEmpty(CustomPreferenceProcedure) ? CustomPreferenceProcedure : "WebGetPreferences",
                TargetType.StoredProc, ActionType.ExecuteProcedure);
            search2.AddParameter("applicationtypeid", WebUser.CurrentApplication.ApplicationTypeId, KxValueType.Int);
            search2.AddParameter("academicyearid", WebUser.CurrentApplication.AcademicYearId, KxValueType.Int);
            search2.AddParameter("filter1", Filter1, KxValueType.String, 50);
            search2.AddParameter("filter2", Filter2, KxValueType.String, 50);
            search2.AddParameter("filter3", Filter3, KxValueType.String, 50);
            search2.AddParameter("filter4", Filter4, KxValueType.String, 50);

            MinPreferences = -1;
            MaxPreferences = -1;

            if (search2.Execute())
            {
                LoadPreferenceSearchResults(search2);
            }
        }

        private void LoadPreferenceSearchResults(KxData search)
        {
            Results.LoadFromXmlPreferenceManager(search.XmlResults, "pref");

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(search.XmlResults);
            XmlNodeList nodes = doc.SelectNodes(@"//pref/datarow");

            foreach (XmlNode node in nodes)
            {
                MinPreferences = KxXmlUtils.GetNodeValue(node, "minpreferences", MinPreferences);
                MaxPreferences = KxXmlUtils.GetNodeValue(node, "maxpreferences", MaxPreferences);
                break;
            }
        }

        public void SavePreferences ()
        {
            KxWebStudentApplication app = WebUser.CurrentApplication;

            app.Preferences.DeleteAll();

            foreach (KxWebStudentPreference pref in Results.Items)
            {
                if (pref.PreferenceNumber > 0)
                {
                    pref.UIObject = null;
                    KxWebStudentPreference newpref = new KxWebStudentPreference(null);
                    KxReflection.CloneObject(pref, newpref);

                    newpref.SiteId = pref.SiteId;
                    newpref.AreaId = pref.AreaId;
                    newpref.BlockId = pref.BlockId;
                    newpref.BedroomTypeId = pref.BedroomTypeId;

                    newpref.Parent = app.Preferences;
                    newpref.ApplicationGuid = app.ApplicationGuid;
                    newpref.PreferenceId = 0;
                    newpref.Temporary = false;
                    newpref.Changed = true;
                    newpref.PreferenceAvailabilityId = pref.PreferenceAvailabilityId;
                    if (newpref.PreferenceAvailabilityId == 0)
                    {
                        newpref.PreferenceAvailabilityId = pref.PreferenceId;
                    }

                    newpref.CustomValue1 = pref.CustomValue1;
                    newpref.CustomValue2 = pref.CustomValue2;
                    newpref.CustomValue3 = pref.CustomValue3;
                    newpref.CustomValue4 = pref.CustomValue4;
                    newpref.CustomValue5 = pref.CustomValue5;
                    newpref.CustomValue6 = pref.CustomValue6;
                    newpref.CustomValue7 = pref.CustomValue7;
                    newpref.CustomValue8 = pref.CustomValue8;

                    app.Preferences.Changed = true;
                    app.Preferences.Items.Add(newpref);
                }
            }

            app.Changed = true;
            app.ChildChanged = true;
            app.Preferences.Changed = true;
            app.Preferences.ChildChanged = true;
        }
    }
}
