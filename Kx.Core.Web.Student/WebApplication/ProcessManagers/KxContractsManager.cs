using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;
using System.Web;
using Kx.Core.Configuration;
using Kx.Core.Data;
using Kx.Core.Exceptions;
using Kx.Core.Localisation;
using Kx.Core.PaymentProcessor;
using Kx.Core.PaymentProcessor.Payments;
using Kx.Core.Security;
using Kx.Core.Utility;
using Kx.Objects.Common;
using Kx.Objects.Student.Web;
using Kx.Core.Web.Security;
using Kx.Core.Web.Student.WebApplication.BaseClass;
using Kx.Core.Web.Student.WebApplication.PageRenderer;
using Kx.Core.Web.Student.WebApplication.WorkflowInterface.WorkflowExtenders;
using Winnovative;
using Winnovative.WnvRtfToPdf;
using Kx.Core.Web.Configuration;

namespace Kx.Core.Web.Student.WebApplication.ProcessManagers
{
    public partial class KxContractsManager : WebStudentBaseClass
    {


        private string userName = "";
        private string password = "";
        private string acceptancePage = "";


        public string DateOfBirthString
        {
            get;
            set;

        }

        public string StudentId
        {
            get;
            set;
        }


        public string UserName
        {
            get
            {
                return userName;
            }
            set
            {
                if (userName == value)
                    return;
                userName = value;
            }
        }
        public string Password
        {
            get
            {
                return password;
            }
            set
            {
                password = value;
            }
        }

        public string PendingOfferUrl
        {
            get
            {
                try
                {
                    return GetUrl(WebUser.PendingAllocation.StudentAllocation.OfferCorrespondanceId);
                }
                catch
                {
                    return "";
                }
            }
        }

        public string PendingLicenceUrl
        {
            get
            {
                try
                {
                    return GetUrl(WebUser.PendingAllocation.StudentAllocation.LicenceCorrespondanceId);
                }
                catch
                {
                    return "";
                }
            }
        }

        public string AcceptedOfferUrl
        {
            get
            {
                try
                {
                    return GetUrl(WebUser.AcceptedAllocation.StudentAllocation.OfferCorrespondanceId);
                }
                catch
                {
                    return "";
                }
            }
        }

        public string AcceptedLicenceUrl
        {
            get
            {
                try
                {
                    return GetUrl(WebUser.AcceptedAllocation.StudentAllocation.LicenceCorrespondanceId);
                }
                catch
                {
                    return "";
                }
            }
        }

        public string SpecifiedOfferUrl
        {
            get
            {
                try
                {
                    return GetUrl(WebUser.SpecifiedAllocation.StudentAllocation.OfferCorrespondanceId);
                }
                catch
                {
                    return "";
                }
            }
        }

        public string SpecifiedLicenceUrl
        {
            get
            {
                try
                {
                    return GetUrl(WebUser.SpecifiedAllocation.StudentAllocation.LicenceCorrespondanceId);
                }
                catch
                {
                    return "";
                }
            }
        }


        public KxRoomConfig PendingRoomConfiguration
        {
            get
            {
                return WebUser.PendingAllocation.StudentAllocation.ResidentialRoom.RoomConfiguration.GetRoomConfig(WebUser.CurrentAcademicYear);
            }
        }

        public KxRoomConfig SpecifiedRoomConfiguration
        {
            get
            {
                return WebUser.SpecifiedAllocation.StudentAllocation.ResidentialRoom.RoomConfiguration.GetRoomConfig(WebUser.CurrentAcademicYear);
            }
        }

        public KxRoomConfig AcceptedRoomConfiguration
        {
            get
            {
                return WebUser.AcceptedAllocation.StudentAllocation.ResidentialRoom.RoomConfiguration.GetRoomConfig(WebUser.CurrentAcademicYear);
            }
        }

        public KxRoomConfig RejectedRoomConfiguration
        {
            get
            {
                return WebUser.RejectedAllocation.StudentAllocation.ResidentialRoom.RoomConfiguration.GetRoomConfig(WebUser.CurrentAcademicYear);
            }
        }

        public KxRoomConfig LastRoomConfiguration
        {
            get
            {
                return WebUser.LastAllocation.StudentAllocation.ResidentialRoom.RoomConfiguration.GetRoomConfig(WebUser.CurrentAcademicYear);
            }
        }




        public KxContractsManager(KxWebApplicationStudent webApp)
            : base(webApp)
        {
        }

        public bool ValidateLdapLogon(bool useExtended)
        {
            KxCustomSetting setting = KxCustomSetting.GetCustomSettingByCustomSetting("Kx.Web.Allow.StudentID");

            bool spoofing = false;

            if (setting.CustomSettingId > 0)
            {
                spoofing = StringComparer.InvariantCultureIgnoreCase.Compare(setting.Value, bool.TrueString) == 0;
            }

            if (useExtended)
            {
                return WebApp.LdapManager.Ldap.AuthenticateExtended(UserName, Password);
            }
            else
            {
                return WebApp.LdapManager.Ldap.Authenticate(UserName, Password);
            }
        }

        public bool ValidateADLogon()
        {
            KxCustomSetting setting = KxCustomSetting.GetCustomSettingByCustomSetting("Kx.Web.Allow.StudentID");

            bool spoofing = false;

            if (setting.CustomSettingId > 0)
            {
                spoofing = StringComparer.InvariantCultureIgnoreCase.Compare(setting.Value, bool.TrueString) == 0;
            }

            if (spoofing || WebUser.UserName.Trim().ToUpper() == UserName.Trim().ToUpper())
            {
                return KxActiveDirectoryAuthentication.Authenticate(UserName, Password);
            }
            else
            {
                return false;
            }
        }

        public bool ValidateLogon()
        {
            if (WebUser.UserName.Trim().ToUpper() == UserName.Trim().ToUpper())
            {
                return Kx.Core.Web.Security.KxWebSecurity.ValidateLogin(Password, WebUser.Password, WebUser.Salt);
            }
            else
            {
                return false;
            }
        }

        public bool ValidateSSOLogon()
        {
            DateTime dob;

            if (DateTime.TryParse(DateOfBirthString, out dob))
            {
                if (WebUser.Student != null && (WebUser.Student.Dob.Value == dob && WebUser.Student.StudentId == StudentId))
                {
                    return true;
                }
            }

            return false;
        }

        public bool AcceptOffer()
        {
			try
            {
				KxLogger.LogActivity(this, "KxContactsManager.AcceptOffer", HttpContext.Current.Request.UserHostAddress, ""); //TE20100212
				return WebUser.AcceptAllocation(GetUserDetails(), HttpContext.Current.Request.UserHostAddress);
            }
            catch
            {
				KxLogger.LogActivity(this, "KxContactsManage.AcceptOffer", "Unknown", ""); //TE20100212
				return WebUser.AcceptAllocation(GetUserDetails(), "Unknown");
            }
        }



        public bool CreateAllocationFromFirstPreference()
        {
            WebApp.AllocationManager.UseFirstPreference = true;
            WebApp.AllocationManager.CheckInstalments = true;
            WebApp.AllocationManager.UsePaymentSchedule = false;
            return WebApp.AllocationManager.CreateAllocation();
        }

        public bool CreateAllocationFromFirstPreferenceNoPST()
        {
            WebApp.AllocationManager.UseFirstPreference = true;
            WebApp.AllocationManager.CheckInstalments = false;
            WebApp.AllocationManager.UsePaymentSchedule = false;
            return WebApp.AllocationManager.CreateAllocation();
        }

        public bool CreateAllocationViaPaymentSchedule()
        {
            WebApp.AllocationManager.UseFirstPreference = true;
            WebApp.AllocationManager.CheckInstalments = true;
            WebApp.AllocationManager.UsePaymentSchedule = true;
            return WebApp.AllocationManager.CreateAllocation();
        }

        public bool CreateStudentAllocation(bool UseFirstPreference, bool CheckInstalments, bool UsePaymentSchedule)
        {
            WebApp.AllocationManager.UseFirstPreference = UseFirstPreference;
            WebApp.AllocationManager.CheckInstalments = CheckInstalments;
            WebApp.AllocationManager.UsePaymentSchedule = UsePaymentSchedule;
            return WebApp.AllocationManager.CreateAllocation();
        }

        public bool RejectOffer()
        {
            return WebUser.RejectAllocation(GetUserDetails(), HttpContext.Current.Request.UserHostAddress);
        }


        public bool AcceptLastAllocationOffer()
        {
            return WebUser.AcceptLastAllocationOffer(GetUserDetails(), HttpContext.Current.Request.UserHostAddress);
        }

        public bool RejectLastAllocationOffer()
        {
            return WebUser.RejectLastAllocationOffer(GetUserDetails(), HttpContext.Current.Request.UserHostAddress);
        }

        private string GetUserDetails()
        {
            StringBuilder sb = new StringBuilder();
            try
            {
                sb.AppendLine("Page Url: " + HttpContext.Current.Request.Url.OriginalString);
            }
            catch { }
            try
            {
                sb.AppendLine("User Browser: " + HttpContext.Current.Request.UserAgent);
            }
            catch { }
            try
            {
                sb.AppendLine("User Host IP: " + HttpContext.Current.Request.UserHostAddress);
            }
            catch { }
            try
            {
                sb.AppendLine("User HostName: " + HttpContext.Current.Request.UserHostName);
            }
            catch { }

            return sb.ToString();
        }

        private string GetUrl(int Id)
        {
            string downloadurl = WebApp.ConfigurationManager.HomePageUrl.ToLower().Replace("default.aspx", "download.aspx");

            downloadurl += "?document=" + Id.ToString();

            return downloadurl;
        }



        public string GetEmailTemplate(string TemplateName)
        {
            string result = "";
            try
            {
                Random RandomClass = new Random();

                string pdfno = "";

                if (WebApp.AllocationManager.CurrentAllocation != null)
                {
                    pdfno = WebApp.AllocationManager.CurrentAllocation.AllocationId.ToString();
                }
                else if (WebUser.PendingAllocation != null)
                {
                    pdfno = WebUser.PendingAllocation.AllocationId.ToString();
                }


                if (String.IsNullOrEmpty(pdfno) || pdfno == "0")
                {
                    pdfno = Guid.NewGuid().ToString("N");
                }

                string pdfFile = GetTemplatePdf(TemplateName, pdfno);

                if (KxConfigurationManager.Profiling)
                {
                    KxLogger.LogActivity(this, "Stream PDF", "Start : " + pdfFile, "");
                }

                StreamReader streamReader = new StreamReader(pdfFile);
                string pdfString = streamReader.ReadToEnd();
                streamReader.Close();

                if (KxConfigurationManager.Profiling)
                {
                    KxLogger.LogActivity(this, "Stream PDF", "Finish : " + pdfFile, "");
                }

                result = pdfString;

            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "GetEmailTemplateAsContract", ex.Message, "");
            }
            return result;

        }

        public string GetTemplatePdf(string emailtemplate, string pdfno)
        {
            string result = "";

            try
            {
                Winnovative.PdfConverter pdfConverter = new Winnovative.PdfConverter();

                pdfConverter.PdfDocumentOptions.PdfPageSize = Winnovative.PdfPageSize.A4;
                pdfConverter.PdfDocumentOptions.PdfCompressionLevel = Winnovative.PdfCompressionLevel.NoCompression;
                pdfConverter.PdfDocumentOptions.ShowHeader = false;
                pdfConverter.PdfDocumentOptions.ShowFooter = false;
                pdfConverter.PdfDocumentOptions.LeftMargin = 15;
                pdfConverter.PdfDocumentOptions.RightMargin = 15;
                pdfConverter.PdfDocumentOptions.TopMargin = 35;
                pdfConverter.PdfDocumentOptions.BottomMargin = 35;
                //pdfConverter.PdfDocumentOptions.Width = 1024;                
                pdfConverter.PdfDocumentOptions.PdfPageSize = Winnovative.PdfPageSize.A4;
                pdfConverter.PdfDocumentOptions.AvoidTextBreak = true;
                //pdfConverter.HtmlViewerWidth = 1024;
                pdfConverter.PdfDocumentOptions.FitWidth = true;
                //pdfConverter.LicenseKey = "8tnA0sPSysTSxNzC0sHD3MPA3MvLy8s=";
                pdfConverter.LicenseKey = "Nbuouqu6oqmvuqurtKq6qau0q6i0o6Ojow==";

                string Source = PageRenderer.ResourceManager.ResourceProvider.GetResource(PageRenderer.ObjectTranslator.MergeTags(emailtemplate));

                string doc = PageRenderer.ObjectTranslator.MergeTags(Source);

                if (!doc.ToLower().Contains("<html"))
                {
                    doc = doc.Replace("\r", "<br>");
                    doc = doc.Replace("\n", "<br>");
                }
                
                string tempfilepath = System.Configuration.ConfigurationManager.AppSettings["TempFileLocation"].ToString();
                string filename = Path.Combine(tempfilepath, emailtemplate + "_" + pdfno + ".pdf");

                acceptancePage = doc;

                if (!File.Exists(filename))
                {
                    if (KxConfigurationManager.Profiling)
                    {
                        KxLogger.LogActivity(this, "SavePdfFromHtmlStringToFile", String.Format("Start : {0}", filename), "");
                    }


                    pdfConverter.SavePdfFromHtmlStringToFile(doc, filename);

                    if (KxConfigurationManager.Profiling)
                    {
                        KxLogger.LogActivity(this, "SavePdfFromHtmlStringToFile", String.Format("Finish : {0}", filename), "");
                    }
                }
                result = filename;
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "GetTemplatePdf", ex.Message, "");
                result = "";
            }
            return result;
        }

        public string GetWordTemplate(int WordTemplateId, string wordTemplateAllocationType)
        {
            string result = "";
            try
            {
                Random RandomClass = new Random();

                string pdfno = "";

                if (WebApp.AllocationManager.CurrentAllocation != null)
                {
                    pdfno = WebApp.AllocationManager.CurrentAllocation.AllocationId.ToString();
                }
                else if (WebUser.PendingAllocation != null)
                {
                    pdfno = WebUser.PendingAllocation.AllocationId.ToString();
                }


                if (String.IsNullOrEmpty(pdfno) || pdfno == "0")
                {
                    pdfno = Guid.NewGuid().ToString("N");
                }

                string pdfFile = GetWordTemplatePdf(WordTemplateId, pdfno, wordTemplateAllocationType);

                StreamReader streamReader = new StreamReader(pdfFile);
                string pdfString = streamReader.ReadToEnd();
                streamReader.Close();

                result = pdfString;
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "GetWordTemplateAsContract", ex.Message, "");
            }
            return result;

        }

        public string GetWordTemplatePdf(int wordtemplateid, string pdfno, string wordtemplateallocationtype)
        {
            string result = "";

            try
            {
                Winnovative.WnvRtfToPdf.PdfConverter pdfConverter = new Winnovative.WnvRtfToPdf.PdfConverter();

                pdfConverter.PdfDocumentOptions.PdfPageSize = Winnovative.WnvRtfToPdf.PdfPageSize.A4;
                pdfConverter.PdfDocumentOptions.PdfCompressionLevel = Winnovative.WnvRtfToPdf.PdfCompressionLevel.NoCompression;
                pdfConverter.PdfDocumentOptions.ShowHeader = false;
                pdfConverter.PdfDocumentOptions.ShowFooter = false;
                //pdfConverter.PdfDocumentOptions.LeftMargin = 0;
                //pdfConverter.PdfDocumentOptions.RightMargin = 0;
                //pdfConverter.PdfDocumentOptions.TopMargin = 0;
                //pdfConverter.PdfDocumentOptions.BottomMargin = 0;
                pdfConverter.PageWidth = 1024;
                pdfConverter.PdfDocumentOptions.PdfPageSize = Winnovative.WnvRtfToPdf.PdfPageSize.A4;
                pdfConverter.AvoidTextBreak = true;

                pdfConverter.LicenseKey = "Gvp4NrNd5kq4KSYM/ueNjjhqKiMR9J9eDGChmSf4W394F2uHligBj97MshWVUwN/";

                string doc = "";

                KxWordTemplate wordTemplate = new KxWordTemplate(wordtemplateid);

                doc = wordTemplate.Template.ToString();

                // Add conversion for Kx Merge Tags to Web Merge Tags.

                int mtlpos = 0;
                int mtpos = 0;
                int mtpose = 0;
                string mt = "";

                mtpos = doc.IndexOf('<', mtlpos);

                while (mtpos > 0)
                {
                    if (doc.Substring(mtpos, 4) == "<SA:")
                    {
                        mtpose = doc.IndexOf('>', mtpos);
                        mt = "{KX_PCUSER" + wordtemplateallocationtype + "MERGE_PNMergeTag_MT" + doc.Substring(mtpos + 4, mtpose - mtpos - 4) + "}";
                        doc = doc.Substring(0, mtpos - 1) + " " + mt + " " + doc.Substring(mtpose + 1, doc.Length - mtpose - 1);
                    }
                    else
                        if (doc.Substring(mtpos, 4) == "<SD:")
                        {
                            mtpose = doc.IndexOf('>', mtpos);
                            mt = "{KX_PCSTUDENTMERGE_PNMergeTag_MT" + doc.Substring(mtpos + 4, mtpose - mtpos - 4) + "}";
                            doc = doc.Substring(0, mtpos - 1) + " " + mt + " " + doc.Substring(mtpose + 1, doc.Length - mtpose - 1);
                        }
                        else
                        {
                            mtlpos = mtpos + 1;
                        }

                    mtpos = doc.IndexOf('<', mtlpos);
                }

                string mergeddoc = PageRenderer.ObjectTranslator.MergeTags(doc);

                pdfConverter.LicenseFilePath = KxDbConfiguration.GetConfiguration("WorkflowObjectPath");

                // pha
                if (!System.IO.Directory.Exists(pdfConverter.LicenseFilePath))
                {
                    System.Diagnostics.Debugger.Break();
                }

                string tempfilepath = KxDbConfiguration.GetConfiguration("TempFileLocation");

                string filename = Path.Combine(tempfilepath, wordTemplate.Description + "_" + pdfno + ".pdf");

                acceptancePage = mergeddoc;

                if (!File.Exists(filename))
                {
                    byte[] pdffile = pdfConverter.GetPdfBytesFromRtfString(mergeddoc);
                    int ArraySize = new int();
                    ArraySize = pdffile.GetUpperBound(0);
                    FileStream fs1 = new FileStream(filename, FileMode.OpenOrCreate, FileAccess.Write);
                    fs1.Write(pdffile, 0, ArraySize);
                    fs1.Close();
                }
                result = filename;
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "GetWordTemplatePdf", ex.Message, "");
                result = "";
            }
            return result;
        }

        public bool StoreEmailTemplateAsContract(string TemplateName)
        {
            bool result = true;
            try
            {
                string template = GetEmailTemplate(TemplateName);
                WebApp.AllocationManager.CurrentAllocation.AcceptanceDocument = GetEmailTemplate(TemplateName);
                WebApp.AllocationManager.CurrentAllocation.AcceptancePage = template;
                if (KxConfigurationManager.Profiling)
                {
                    KxLogger.LogActivity(this, "StoreEmailTemplateAsContract", "WebApp.AllocationManager.CurrentAllocation.Save().Start", template.Length.ToString());
                }

                WebApp.AllocationManager.CurrentAllocation.Save();

                if (KxConfigurationManager.Profiling)
                {
                    KxLogger.LogActivity(this, "StoreEmailTemplateAsContract", "WebApp.AllocationManager.CurrentAllocation.Save().End", "");
                }
            }
            catch
            {
                result = false;
            }
            return result;
        }

        public bool StoreWordTemplateAsContract(int WordTemplateId, string wordTemplateAllocationType)
        {
            bool result = true;
            try
            {
                WebApp.AllocationManager.CurrentAllocation.AcceptanceDocument = GetWordTemplate(WordTemplateId, wordTemplateAllocationType);
                WebApp.AllocationManager.CurrentAllocation.AcceptancePage = acceptancePage;
                WebApp.AllocationManager.CurrentAllocation.Save();
            }
            catch
            {
                result = false;
            }
            return result;
        }

        public KxRoomConfig GetSpecifiedAllocationRoomConfiguration(int allocationId)
        {
            return WebUser.Allocations.GetAllocationById(allocationId).StudentAllocation.ResidentialRoom.RoomConfiguration.GetRoomConfig(WebUser.CurrentAcademicYear);
        }

        public int GetContractMonths(string StartDate, string EndDate)
        {

            int result = 0;

            try
            {
                DateTime start = KxDateTime.Parse(StartDate);
                DateTime end = KxDateTime.Parse(EndDate);

                if (end != null && start != null)
                {
                    TimeSpan ts = end.Subtract(start);

                    result = Convert.ToInt32(Math.Floor(ts.TotalDays / 30));
                }
            }
            catch { }

            return result;
        }

    }
}
