using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Xml;
using System.Web.UI;
using System.Web.UI.WebControls;
using Kx.Core.Configuration;
using Kx.Core.Data;
using Kx.Core.Utility;
using Kx.Core.Web.Student.WebApplication.BaseClass;
using Kx.Core.Web.Student.WebApplication.PageRenderer;
using Kx.Objects.Common;
using Kx.Objects.Student;
using Kx.Objects.Student.DataProviders;
using Kx.Objects.Student.Web;
using Kx.Core.Exceptions;

namespace Kx.Core.Web.Student.WebApplication.ProcessManagers
{
    [Serializable]
    public class KxApplicationsManager : WebStudentBaseClass
    {
        #region Attributes & Properties     

        protected List<KxApplicationType> _appTypes = null;
        protected KxWebStudentPreference tempPreference;
        protected static KxControlStudents controlStudents = new KxControlStudents(null);


        public string[] YearsArray
        {
            get
            { return StudentCoreDataProvider.GetAcademicYears().GetYearIds(); }
        }

        public string[] YearsArrayDesc
        {
            get
            { 
                return StudentCoreDataProvider.GetAcademicYears().GetYearIdsDesc(); 
            }
        }
        public List<KxApplicationType> AppTypes
        {
            get
            {
                if (_appTypes == null)
                {
                    _appTypes = KxApplicationType.GetApplicationTypes().ToList();
                }
                return _appTypes;
            }
        }

        public object GetYearDescription
        {
            get
            {
                try
                {
                    return StudentCoreDataProvider.GetAcademicYear(WebUser.CurrentAcademicYear).Title;
                }
                catch (Exception ex)
                {
                    if ((WebUser != null) && (WebUser.CurrentAcademicYear > 0 ))
                    {
                        KxLogger.LogActivity(this, "GetYearDescription", ex.Message + " : " + WebUser.CurrentAcademicYear.ToString(), "");
                    }
                    else
                    {
                        KxLogger.LogActivity(this, "GetYearDescription", ex.Message, "");
                    }

                    return null;
                }
            }
        }

        public object YearDescriptionShort
        {
            get
            {
                return GetYearDescriptionShort();
            }
        }

        public string YearDescriptionVeryShort
        {
            get
            {
                return GetYearDescriptionVeryShort();
            }
        }

            
        public ListItemCollection AppTypeList
        {
            get
            {
                return GetApplicationTypeList();
            }
        }

        public TableRowCollection PopulateAppTypeTableButton
        {
            get
            {
                return GetApplicationTypeButton();
            }
        }

        public KxWebFriendshipGroupMember CurrentGroupMember
        {
            get
            {
                return GroupMemberByIndex(WebApp.ObjectTranslator.CurrentMemberId);
            }
        }

        public KxWebFriendshipGroup CurrentGroup
        {
            get
            {
                return WebUser.CurrentApplication.FriendshipGroup;
            }
        }

        public KxApplicationType CurrentApplicationType
        {
            get
            { return AppTypes.FirstOrDefault(s => s.ApplicationTypeId == WebUser.CurrentApplication.ApplicationTypeId); }
        }

        public string CurrentApplicationTypeName
        {
            get
            {
                try
                {
                    return CurrentApplicationType.Description;
                }
                catch
                {
                    return "";
                }
            }
        }

        public int ApplicationTypeId
        {
            get
            { return WebUser.CurrentApplication.ApplicationTypeId; }
            set
            { SetApplicationTypeId(value); }
        }

        public KxControlStudents ControlStudents
        {
            get
            {
                return controlStudents;
            }  
        }
       
        public string[] AvailiableRooms
        {
            get
            { return WebApp.BedroomTypeCharges.GetMatchingBedrooms(WebUser.CurrentAcademicYear,WebApp.AllocationManager.GetPendingRoomIds(WebUser.CurrentAcademicYear)); }
        }

    

        public int AgeAtAcademicYearStart
        {
            get
            {
                return GetAgeAtAcademicYearStart();
            }
        }

        public bool IsOver18AtAcademicYearStart
        {
            get
            {
                return AgeAtAcademicYearStart >= 18;
            }
        }

        public bool IsUnder18AtAcademicYearStart
        {
            get
            {
                return AgeAtAcademicYearStart < 18;
            }
        }

        public KxAcademicYear CurrentAcademicYear
        {
            get
            {
                return StudentCoreDataProvider.GetAcademicYear(WebUser.CurrentAcademicYear);
            }
        }

        public KxAcademicYearSession CurrentAcademicYearSession
        {
            get { return CurrentAcademicYear.Sessions.FirstOrDefault(s => s.SessionId == WebUser.CurrentAcademicYearSession); }
        }

        public int[] CurrentQuestions
        {
            get
            {
                return KxStudentObjectManager.InductionQuestions.GeteInductionQuestionIdsByYear(WebUser.CurrentAcademicYear);
            }
        }

        public string[] ApplicationGuidsInCurrentSession
        {
            get
            {
                List<string> result = new List<string>();
                foreach (KxWebStudentApplication application in WebUser.Applications.GetWebApplicationsForYearAndSession(WebUser.CurrentAcademicYear, WebUser.CurrentAcademicYearSession, true))
                {
                    result.Add(application.ApplicationGuid.ToString("N"));
                }

                return result.ToArray();
            }
        }

        public string CustomUrl1
        {
            get
            {
                return WebApp.ObjectTranslator.MergeTags(KxDbConfiguration.GetConfiguration("Customurl1"));
            }
        }

        public string CustomUrl2
        {
            get
            {
                return WebApp.ObjectTranslator.MergeTags(KxDbConfiguration.GetConfiguration("Customurl2"));
            }
        }

        public string CustomUrl3
        {
            get
            {
                return WebApp.ObjectTranslator.MergeTags(KxDbConfiguration.GetConfiguration("Customurl3"));
            }
        }

  
        #endregion

        public KxApplicationsManager(KxWebApplicationStudent webApp) : base(webApp)
        {
            
        }

        protected ListItemCollection GetApplicationTypeList()
        {
            ListItemCollection result = new ListItemCollection();

            foreach (KxApplicationType apptype in _appTypes)
            {
                if (apptype.WebPublished)
                {
                    ListItem li = new ListItem(apptype.Description, apptype.ApplicationTypeId.ToString());
                    result.Add(li);
                }
            }
            return result;
        }

        protected TableRowCollection GetApplicationTypeButton()
        {
            return null;
        }

        public KxWebFriendshipGroupMember GroupMemberByIndex(int groupmemberId)
        {
            KxWebFriendshipGroupMember result = null;

            if (WebUser.CurrentApplication != null)
            {
                result = WebUser.CurrentApplication.FriendshipGroup.GetGroupMemberById(groupmemberId);
                return result;
            }
            else
            {
                return null;
            }
        }

        public void RejectInvitation()
        {
            KxWebFriendshipGroupMember member = WebUser.CurrentApplication.GroupMember;

            if (member != null)
            {
                member.RejectApplication();
                WebUser.CurrentApplication.FriendshipGroup = null;
            }
        }

        public void AcceptInvitation()
        {
            KxWebFriendshipGroupMember member = WebUser.CurrentApplication.GroupMember;

            if (member != null)
            {
                member.AcceptedDate = DateTime.Now;
            }
        }
     
        public void SavePreference()
        {
            if (WebUser.CurrentApplication.CurrentIndexPreference != null)
            {
                WebUser.CurrentApplication.CurrentIndexPreference.Temporary = false;
                WebUser.CurrentApplication.CurrentIndexPreference.Changed = true;

                if (WebUser.CurrentApplication.CurrentIndexPreference.PreferenceNumber == 0)
                {
                    WebUser.CurrentApplication.CurrentIndexPreference.PreferenceNumber = WebUser.CurrentApplication.Preferences.Items.Count;
                }
            }
            WebUser.CurrentApplication.UserPreferenceIndex = 0;
        }

        public void CancelPreference()
        {
            if (WebUser.CurrentApplication.CurrentIndexPreference != null)
            {
                if (WebUser.CurrentApplication.CurrentIndexPreference.PreferenceNumber == 0)
                {
                    WebUser.CurrentApplication.CurrentIndexPreference.Delete();
                    WebUser.CurrentApplication.Preferences.Items.Remove(WebUser.CurrentApplication.CurrentIndexPreference);
                }
            }
            WebUser.CurrentApplication.UserPreferenceIndex = 0;
        }

        public void DeletePreference()
        {
            if (WebUser.CurrentApplication.CurrentIndexPreference != null)
            {
                WebUser.CurrentApplication.Preferences.DeletePreference(WebUser.CurrentApplication.CurrentIndexPreference);

            }
            WebUser.CurrentApplication.UserPreferenceIndex = 0;
        }

        public void AddWebUserToGroup()
        {
            CurrentGroup.GroupLeaderUserGuid = WebUser.UserGuid;
            CurrentGroup.AcademicYearId = WebUser.CurrentApplication.AcademicYearId;
            CurrentGroup.ApplicationGuid = WebUser.CurrentApplication.ApplicationGuid;
            CurrentGroup.CreationDate = DateTime.Now;
            KxWebFriendshipGroupMember member = CurrentGroup.AddMember(Student.StudentId, Student.SurName);
            member.Temporary = false;
            member.AcceptedDate = DateTime.Now;
        }

        public void SetApplicationTypeId(int ApplicationTypeId)
        {
            KxApplicationType apptype = AppTypes.FirstOrDefault(s => s.ApplicationTypeId == ApplicationTypeId);
            if (WebUser.CurrentApplication != null)
            {
                WebUser.CurrentApplication.ApplicationTypeId = ApplicationTypeId;

                if (apptype != null && apptype.WebPublished)
                {
                    try
                    {
                        WebUser.CurrentApplication.ApplicationTypeId = apptype.ApplicationTypeId;
                        WebUser.CurrentApplication.LicenceTypeId = apptype.LicenceTypeId;
                        WebUser.CurrentApplication.StudentYearId = apptype.StudentYearId;
                        WebUser.CurrentApplication.CourseTypeId = apptype.CourseTypeId;
                    }
                    catch
                    { }
                }
            }
        }

        public bool IsDateInCurrentYear(DateTime? dte)
        {
            bool result = false;
            try
            {
                result = (GetAcademicYearForDate(dte) == WebUser.CurrentAcademicYear);
            }
            catch { }

            return result;
        }

        public bool GetIsCourseStartYearEarlier(DateTime? dte)
        {
            bool result = false; 

            int year = GetAcademicYearForDate(dte);

            if (year < WebUser.CurrentAcademicYear)
            {
                result = true;
            }
           
            return result;
        }

        public int GetAcademicYearForDate(DateTime? dte)
        {
            int result = 0;

            foreach (KxAcademicYear year in StudentCoreDataProvider.GetAcademicYears().Items)
            {
                if (dte != null && year != null && year.WebPublished && (year.StartDate <= dte.Value.Date && year.EndDate >= dte.Value.Date))
                {
                    result = year.AcademicYearId;
                    //break;
                }
            }
            return result;
        }

        public KxControlStudent GetControlStudent(string source)
        {
            return ControlStudents.GetControlStudent(source);
        }

        public string GetControlStudentValue(string source)
        {
            return ControlStudents.GetControlStudent(source).ControlValue;
        }

        public DateTime? GetControlStudentDateTime(string source)
        {
            return ControlStudents.GetControlStudent(source).ControlValueDateTime;
        }

        public bool ControlValueIsHistoric(string source)
        {
            DateTime? value = GetControlStudentDateTime(source);

            if (value == null)
            {
                KxLogger.LogActivity(this, "ControlValueIsHistoric", source, "No valid datetime value");
            }

            return value != null && value.Value <= DateTime.Now;
        }

        public bool ControlValueIsFuturistic(string source)
        {
            return !ControlValueIsHistoric(source);
        }

        private int GetAgeAtAcademicYearStart()
        {
            int result = 0;

            if (WebUser.Student != null)
            {
                KxAcademicYear year = StudentCoreDataProvider.GetAcademicYear(WebUser.CurrentAcademicYear);

                if (year != null && WebUser.Student.Dob != null)
                {
                    result = KxDateTime.CalculateAge(WebUser.Student.Dob,year.StartDate.Value);
                }
            }
            return result;
        }

        public KxCustomSetting GetCustomSetting(string setting, string value)
        {
            KxCustomSetting result = new KxCustomSetting(null, setting, value);

            return result;
        }

        public KxCustomSetting GetKxCustomSetting(string setting)
        {
            return KxCustomSetting.GetCustomSettingByCustomSetting(setting);
        }

        public string GetCustomSettingValue(string setting, string value)
        {
            string result = "";

            KxCustomSetting newsetting = GetCustomSetting(setting, value);

            if (newsetting != null)
            {
                result = newsetting.String1;
            }

            return result;
        }

        public int GetCustomSettingValueInt(string setting, string value)
        {
            int result = 0;

            try
            {
                result = Convert.ToInt32(GetCustomSettingValue(setting,value));
            }
            catch { }

            return result;
        }

        public bool CompareStrings(object source1,object Source2)
        {
            bool result = false;

            try
            {
                result = (source1.ToString().ToUpper().Trim() == Source2.ToString().ToUpper().Trim());
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "Compare strings", ex.Message, string.Format("{0} : {1}", source1, Source2));
            }

            return result;
        }

        public bool AnswerQuestion(int QuestionId, int answerIndex)
        {
            bool result = false;
            try
            {
                KxeInductionQuestionAttempt question = WebUser.CurrentApplication.QuestionAttempts.GeteInductionQuestionAttempt(QuestionId);

                if (question != null)
                {
                    question.Answer = answerIndex;
                    result = question.IsCorrect;
                }
            }
            catch { }

            return result;
        }

        public KxeInductionQuestion GetQuestionById(int questionId)
        {
            return KxStudentObjectManager.InductionQuestions.GeteInductionQuestion(questionId);
        }

        public KxeInductionArrivalSlot ArrivalSlotXY(int x, int y)
        {
            KxeInductionArrivalSlot result = WebUser.AcceptedAllocation.StudentAllocation.InductionArrivalLocation.ArrivalSlots.GeteInductionArrivalSlot(x, y);
            
            if (result != null)
            {
                if (WebUser.CurrentApplication.InductionArrival != null && WebUser.CurrentApplication.InductionArrival.ArrivalSlotId > 0)
                {
                    result.IsCurrent = result.ArrivalSlotId == WebUser.CurrentApplication.InductionArrival.ArrivalSlotId;
                }
                else
                {
                    result.IsCurrent = false;
                }
            }
            return result;
        }
        public KxeInductionArrivalSlot AllocArrivalSlotXY(int x, int y)
        {
            KxeInductionArrivalSlot result = WebUser.AcceptedAllocation.StudentAllocation.InductionArrivalLocation.AllocationArrivalSlots.GeteInductionArrivalSlot(x, y);
            
            if (result != null)
            {
                if (WebUser.CurrentApplication.InductionArrival != null && WebUser.CurrentApplication.InductionArrival.ArrivalSlotId > 0)
                {
                    result.IsCurrent = result.ArrivalSlotId == WebUser.CurrentApplication.InductionArrival.ArrivalSlotId;
                }
                else
                {
                    result.IsCurrent = false;
                }
            }
            return result;
        }

        public KxeInductionArrivalSlot AllocArrivalFromSlotXY(int x, int y)
        {
            KxeInductionArrivalSlot result = WebUser.AcceptedAllocation.StudentAllocation.InductionArrivalLocation.FromAllocationArrivalSlots.GeteInductionArrivalSlot(x, y);
            
            if (result != null)
            {
                if (WebUser.CurrentApplication.InductionArrival != null && WebUser.CurrentApplication.InductionArrival.ArrivalSlotId > 0)
                {
                    result.IsCurrent = result.ArrivalSlotId == WebUser.CurrentApplication.InductionArrival.ArrivalSlotId;
                }
                else
                {
                    result.IsCurrent = false;
                }
            }
            return result;
        }

        public bool CustomValueExists(int id, string value)
        {
            if (!String.IsNullOrEmpty(value))
            {
                return KxRoomConfiguration.CustomValueExists(id, value);
            }
            else
            {
                return false;
            }
        }

        protected string GetYearDescriptionShort()
        {
            string result = "";

            try
            {
                KxAcademicYear year = StudentCoreDataProvider.GetAcademicYear(WebUser.CurrentAcademicYear);

                result = year.StartDate.Value.ToString("yyyy") + "-" + year.EndDate.Value.ToString("yy");
            }
            catch
            {}

            return result;
        }

        protected string GetYearDescriptionVeryShort()
        {
            string result = "";

            try
            {
                KxAcademicYear year = StudentCoreDataProvider.GetAcademicYear(WebUser.CurrentAcademicYear);

                result = year.StartDate.Value.ToString("yy") + "/" + year.EndDate.Value.ToString("yy");
            }
            catch
            { }

            return result;
        }

        public void ConvertEmailToCorrespondence(MailMessage msg)
        {
            try
            {
                KxStudentCorrespondence corresp = new KxStudentCorrespondence(null);
                corresp.KxStudentId = WebUser.KxStudentId;
                corresp.CreationDate = DateTime.Now;
                corresp.CreationUserId = 1;
                corresp.DocumentType = "Web System Email";
                corresp.Description = msg.Subject;
                if (msg.IsBodyHtml)
                {
                    corresp.DocumentName = String.Format("Email {0}.html", KxStringUtils.RemoveFileCharacters(msg.Subject));
                }
                else
                {
                    corresp.DocumentName = String.Format("Email {0}.txt", KxStringUtils.RemoveFileCharacters(msg.Subject));
                }
                corresp.DocumentImageType = 0;
                corresp.Document = Encoding.UTF8.GetBytes(msg.Body);
                corresp.DocumentText = msg.Body;
                corresp.ResponseRequestedBy = KxDateTime.KxNullDate;
                corresp.ResponseReceived = KxDateTime.KxNullDate;
                corresp.ApplicationId = WebUser.CurrentApplication.ApplicationId;
                corresp.DocumentTypeId = 0;
                corresp.DocumentCreated = DateTime.Now;
                corresp.Sent = DateTime.Now;
                corresp.PrintToFileDestination = msg.To[0].Address;

                corresp.Save();
            }
            catch { }
       

        }
    }
}
