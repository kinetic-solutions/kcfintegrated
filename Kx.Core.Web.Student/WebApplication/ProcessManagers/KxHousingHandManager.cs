﻿#region copyright

// =======================================================
// Product:   KCF.Student
// Project:   Kx.Core.Web.Student
// File:      KxHousingHandManager.cs
// Created:   19/06/2017
// By:        Steve Ashley
// Modified:  19/06/2017
// 
// Copyright © Kinetic Solutions Ltd 2017
// All rights reserved.
// 
// No portion of this file may be re-created, copied or otherwise
// used without the express permission of Kinetic Solutions Ltd.
// =======================================================

#endregion

#region usings

using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using Kx.Core.Exceptions;
using Kx.Core.Localisation;
using Kx.Core.Web.PageRenderer.Utility;
using Kx.Core.Web.Student.HousingHand;
using Kx.Core.Web.Student.WebApplication.BaseClass;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

#endregion

namespace Kx.Core.Web.Student.WebApplication.ProcessManagers
{
    public class KxHousingHandManager : WebStudentBaseClass
    {
        #region Nested Types

        internal enum Tasks
        {
            registration
        }

        #endregion

        #region Fields and Properties

        internal string HousingHandURL { get; set; }
        internal string APIKey { get; set; }
        internal int C5UDFID { get; set; }
        internal int CertificateURLUDFID { get; set; }
        internal int QuoteReferenceUDFID { get; set; }

        /* Input fields for Housing Hands */
        //STAGE 1
        public string ForeName { get; set; }
        public string SurName { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
        public string Nationality { get; set; }
        public string PhoneNumber { get; set; }
        public string EmailAddress { get; set; }

        //STAGE 2
        public DateTime DOB { get; set; } = DateTime.MinValue;

        public string UniversityorCollegeName { get; set; }
        public string Course { get; set; }
        public string CourseType { get; set; }
        public string YearOfStudy { get; set; }

        //STAGE 3
        public string AccommodationProvider { get; set; }

        public string PropertyAddress { get; set; }
        public string PropertyCounty { get; set; }
        public string PropertyPostcode { get; set; }
        public decimal WeeklyRent { get; set; } = 0m;
        public DateTime TenancyStartDate { get; set; } = DateTime.MinValue;
        public DateTime TenancyEndDate { get; set; } = DateTime.MinValue;

        public decimal PriceCalcMultiplier1 { get; set; } = 4.33m;
        public decimal PriceCalcMultiplier2 { get; set; } = 0.8m;

        public decimal MinimumPrice { get; set; } = 295m;

        public decimal HousingHandPolicyPrice
        {
            get
            {
                var result = decimal.Round((WeeklyRent * (PriceCalcMultiplier1 == 0m ? 4.33m : PriceCalcMultiplier1)) * (PriceCalcMultiplier2 == 0m ? 0.8m : PriceCalcMultiplier2), 2);
                if (result < MinimumPrice)
                {
                    result = MinimumPrice;
                }

                return result;
            }
        }

        #endregion

        #region Constructors

        public KxHousingHandManager(KxWebApplicationStudent webApp) : base(webApp)
        {
        }

        #endregion

        #region Public Methods

        public void InitDataFromStudent()
        {
            ForeName = WebUser.ForeName;
            SurName = WebUser.SurName;
            Nationality = WebUser.CurrentApplication.NationalityName;
            PhoneNumber = WebUser.CurrentApplication.CurrentStudentAddress.TelephoneNumber;
            EmailAddress = WebUser.EmailAddress;
            DOB = WebUser.CurrentApplication.Dob.Value;
        }

        #endregion

        #region Private Methods

        public bool VerifyRequest(string refno)
        {
            string url = this.HousingHandURL.Remove(this.HousingHandURL.IndexOf("-v2"), 3) + "/" + "tenant/verify/refno?ref=" + refno;
            WebRequest request = (WebRequest)WebRequest.Create(url);
            request.Method = "GET";
            request.ContentType = "text/html; charset=utf-8";

            string responseMessage = null;
            WebResponse response = request.GetResponse();
            using (StreamReader responseStream = new StreamReader(response.GetResponseStream()))
            {
                responseMessage = responseStream.ReadToEnd();
            }

            var responseObject = JsonConvert.DeserializeObject<dynamic>(responseMessage);
            return responseObject ?.status ?? false;
        }

        internal HousingHandResponse MakeRequestToHousingHand()
        {
            HousingHandResponse response;

            if ((WebUser.DynamicValue4 ?? "").ToLower() == "existing")
            {
                var PolicyRefNumber = WebUser.CurrentApplication.UserDefinedValues.GetUDF(QuoteReferenceUDFID);
                var isSuccessful = VerifyRequest(PolicyRefNumber);

                response = new HousingHandResponse()
                {
                    Successful = isSuccessful,
                    ReferenceValue = PolicyRefNumber,
                    Message = isSuccessful ? "" : "Invalid Housing Hand Policy reference."
                };
            }
            else
            {
                response = SendRequestToHousingHand();
                if (response.Successful)
                {
                    UpdateUDFValuesFromResponse(response);
                }
            }

            return response;
        }


        internal HousingHandResponse SendRequestToHousingHand(Tasks task = Tasks.registration)
        {
            HousingHandResponse result = new HousingHandResponse() {Successful = false, Message = "Unknown response"};

            ValidateMinimumData();

            try
            {
                using (var client = new HttpClient())
                {
                    var requestObject = GetPopulatedData();
                    string payloadStr = JsonConvert.SerializeObject(requestObject);
                    Log("SendRequestToHousingHand", "POST DATA : " + payloadStr);
                    byte[] payloadDataBytes = Encoding.UTF8.GetBytes(payloadStr);
                    var uri = new Uri(HousingHandURL + "/" + "tenant");
                    client.DefaultRequestHeaders.Add("api_key", APIKey);
                    client.DefaultRequestHeaders.Add("source", "8");
                    var content = new StringContent(payloadStr, Encoding.UTF8, "application/json");
                    var response = client.PostAsync(uri, content);
                    var responseMessage = string.Empty;
                    using (var stream = response.Result.Content.ReadAsStreamAsync())
                    {
                        using (StreamReader responseStream = new StreamReader(stream.Result))
                        {
                            responseMessage = responseStream.ReadToEnd();
                        }
                    }

                    Log("SendRequestToHousingHand", "RESPONSE : " + responseMessage);

                    result = ExtractResultFromResponse(responseMessage);
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.Message;
                result.Successful = false;
            }

            return result;
        }

        private HousingHandResponse ExtractResultFromResponse(string response)
        {
            var result = new HousingHandResponse();
            var responseObject = JsonConvert.DeserializeObject<dynamic>(response);
            result.Message = responseObject.message;

            if (responseObject.code == HttpStatusCode.Created || responseObject.code == HttpStatusCode.OK)
            {
                result.Successful = true;
                result.ReferenceValue = responseObject?.data?.account_info?.ref_no;
            }
            else
            {
                result.Successful = false;

                if (responseObject.form_errors != null)
                {
                    foreach (JProperty child in (responseObject.form_errors as JObject).Children())
                    {
                        result.FormErrors.Add((child.Value ?? string.Empty).ToString().Replace("[", "").Replace("]", "").Replace(Environment.NewLine, ""));
                    }
                }
            }

            return result;
        }

        private void ValidateMinimumData()
        {
            StringBuilder errors = new StringBuilder();

            if (string.IsNullOrWhiteSpace(ForeName))
            {
                errors.AppendLine(KxLocalisationManager.GetSubstitution("Forename is required"));
            }

            if (string.IsNullOrWhiteSpace(SurName))
            {
                errors.AppendLine(KxLocalisationManager.GetSubstitution("Surname is required"));
            }

            if (string.IsNullOrWhiteSpace(EmailAddress))
            {
                errors.AppendLine(KxLocalisationManager.GetSubstitution("Email address is required"));
            }

            if (!KxPageUtility.ValidateEmail(EmailAddress))
            {
                errors.AppendLine(KxLocalisationManager.GetSubstitution("Email address must be a valid email address"));
            }

            if (string.IsNullOrWhiteSpace(PhoneNumber))
            {
                errors.AppendLine(KxLocalisationManager.GetSubstitution("Phone number is required"));
            }

            if (!KxPageUtility.ValidatePhone(PhoneNumber))
            {
                errors.AppendLine(KxLocalisationManager.GetSubstitution("Phone number must be a valid phone number"));
            }

            if (!string.IsNullOrWhiteSpace(errors.ToString()))
            {
                throw new KxException(errors.ToString());
            }
        }

        private void Log(string method, string logMessage)
        {
            KxLogger.LogActivity(this, method, logMessage, string.Empty);
        }

        private object GetPopulatedData()
        {
            object personal_info = new
            {
                first_name = ForeName,
                last_name = SurName,
                phone_number=PhoneNumber,
                date_of_birth= DOB.ToString("dd-MM-yyyy"),

                nationality = new {
                    text = Nationality,
                    type = Nationality == "UK" ? 1 : 2
                },
                place_of_study = new
                {
                    text=UniversityorCollegeName
                },
                type_of_study = new
                {
                    text=Course
                },
                type_of_course = new
                {
                    text=CourseType
                },
                year_of_study = new
                {
                    text=YearOfStudy,
                    type = 1 // default value
                }
            };
            object account_info = new
            {
                email = EmailAddress,
                password = Password,
                type = 1 //For Student=1, WorkProfessional=2
            };
            object application_info = new
            {
                ap_details = new
                {
                    ap = new
                    {
                        name = $"Homes For Students ({AccommodationProvider})",
                        accomType = "with_property"
                    },
                    property = new
                    {
                        address = PropertyAddress,
                        postcode = PropertyPostcode
                    },
                    rent = new
                    {
                        fee = WeeklyRent,
                        duration = "weekly",
                        start_date = TenancyStartDate.ToString("yyyy-MM-dd"),
                        end_date = TenancyEndDate.ToString("yyyy-MM-dd")
                    }
                }
            };

            object requestObject = new
            {
                personal_info,
                account_info,
                application_info
            };

            return requestObject;
        }


        private void UpdateUDFValuesFromResponse(HousingHandResponse response)
        {
            if (!string.IsNullOrWhiteSpace(response.ReferenceValue) && QuoteReferenceUDFID > 0)
            {
                WebUser.CurrentApplication.UserDefinedValues.SetUDFAndTransferToKx(QuoteReferenceUDFID, response.ReferenceValue);
            }
        }

        #endregion

    }
}