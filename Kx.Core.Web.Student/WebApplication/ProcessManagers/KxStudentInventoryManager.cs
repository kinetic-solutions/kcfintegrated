﻿#region usings

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Policy;
using System.Text;
using System.Web;
using System.Web.Caching;
using System.Web.Services;
using System.Web.Services.Description;
using Kx.Core.Exceptions;
using Kx.Core.Web.Student.WebApplication.BaseClass;
using Kx.Objects.Student.Inventory;
using Kx.Objects.Student.Web;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Net.Security;
using Kx.Core.Configuration;

#endregion

namespace Kx.Core.Web.Student.WebApplication.ProcessManagers
{
    public class KxStudentInventoryManager : WebStudentBaseClass
    {
        #region Properties

        public string WebServiceURL
        {
            get { return (string) HttpContext.Current.Session["KxStudentInventoryManager.WebServiceUrl"]; }
            set { HttpContext.Current.Session["KxStudentInventoryManager.WebServiceUrl"] = value; }
        }

        public bool TrustSSLCertificate
        {
            get { return bool.Parse((HttpContext.Current.Session["KxStudentInventoryManager.TrustSSLCertificate"] ?? "False").ToString()); }
            set { HttpContext.Current.Session["KxStudentInventoryManager.TrustSSLCertificate"] = value; }
        }

        public string AdminDisplay
        {
            get { return (string)HttpContext.Current.Session["KxStudentInventoryManager.AdminDisplay"]; }
            set { HttpContext.Current.Session["KxStudentInventoryManager.AdminDisplay"] = value; }
        }

        public string YouDisplay
        {
            get { return (string)HttpContext.Current.Session["KxStudentInventoryManager.YouDisplay"]; }
            set { HttpContext.Current.Session["KxStudentInventoryManager.YouDisplay"] = value; }
        }


        public string[] InventoryRoomIDs
        {
            get
            {
                var result = new List<string>();
                OccupantInventory inventory = GetInventoryForCurrentRoom();
                if (inventory != null)
                {
                    foreach (OccupantInventoryRoom room in inventory.Rooms)
                    {
                        result.Add(room.OccupantInventoryRoomId.ToString());
                    }
                }

                return result.ToArray();
            }
        }

        public OccupantInventoryRoom InventoryRoom(int inventoryRoomId)
        {
            var inventory = GetInventoryForCurrentRoom();
            if (inventory != null && inventory.Rooms != null)
            {
                foreach (var room in inventory.Rooms)
                {
                    if (room.OccupantInventoryRoomId == inventoryRoomId)
                    {
                        return room;
                    }
                }
            }

            return null;
        }

        public OccupantInventoryItem InventoryRoomItem(int inventoryRoomId, int roomItemId)
        {
            var room = InventoryRoom(inventoryRoomId);
            if (room != null)
            {
                foreach (var item in room.Items)
                {
                    if (item.OccupantInventoryItemId == roomItemId)
                    {
                        return item;
                    }
                }
            }

            return null;
        }

        public OccupantInventoryComment InventoryRoomComment(int inventoryRoomId, int roomCommentId)
        {
            var room = InventoryRoom(inventoryRoomId);
            if (room != null)
            {
                return room.RoomComments[roomCommentId];
            }

            return null;
        }

        public OccupantInventoryComment InventoryItemComment(int inventoryRoomId, int roomItemId, int itemCommentId)
        {
            var item = InventoryRoomItem(inventoryRoomId, roomItemId);
            if (item != null)
            {
                return item.Comments[itemCommentId];
            }

            return null;
        }

        public OccupancyInventoryItemConditionNote InventoryItemConditionNode(int inventoryRoomId, int roomItemId, int itemConditionNoteId)
        {
            var item = InventoryRoomItem(inventoryRoomId, roomItemId);
            if (item != null)
            {
                return item.ConditionNotes.FirstOrDefault(s => s.Id == itemConditionNoteId);
            }

            return null;
        }

        public int AcceptedRoomCount
        {
            get
            {
                int result = 0;
                var inventory = GetInventoryForCurrentRoom();
                if (inventory != null)
                {
                    foreach (var room in inventory.Rooms)
                    {
                        if (room.Accepted)
                        {
                            ++result;
                        }
                    }
                }
                return result;
            }
        }

        public int DisputedRoomCount
        {
            get
            {
                int result = 0;
                var inventory = GetInventoryForCurrentRoom();
                if (inventory != null)
                {
                    foreach (var room in inventory.Rooms)
                    {
                        if (room.IsDisputed)
                        {
                            ++result;
                        }
                    }
                }
                return result;
            }
        }

        public int PendingRoomCount
        {
            get
            {
                try
                {
                    int result = 0;
                    var inventory = GetInventoryForCurrentRoom();
                    if (inventory != null)
                    {
                        foreach (var room in inventory.Rooms)
                        {
                            if (!room.Disputed && !room.Accepted)
                            {
                                ++result;
                            }
                        }
                    }
                    return result;
                }
                catch (Exception ex)
                {
                    KxLogger.LogActivity(this, "PendingRoomCount", ex.Message, "");
                    throw;
                }
            }
        }

        public bool HasPendingRooms
        {
            get
            {
                return PendingRoomCount > 0;
            }
        }

        public bool HasInventory
        {
            get
            {
                return PendingRoomCount + AcceptedRoomCount + DisputedRoomCount > 0;
            }
        }

        public int UnreadCommentCount
        {
            get
            {
                int result = 0;
                var inventory = GetInventoryForCurrentRoom();
                if (inventory != null)
                {
                    foreach (var room in inventory.Rooms)
                    {
                        foreach (var comment in room.RoomComments)
                        {
                            if (!comment.Read)
                            {
                                ++result;
                            }
                            foreach (var item in room.Items)
                            {
                                foreach (var inventoryComment in item.Comments)
                                {
                                    if (!inventoryComment.Read)
                                    {
                                        ++result;
                                    }
                                }
                            }
                        }
                    }
                }
                return result;
            }
        }

        public bool HasUnreadComments
        {
            get
            {
                return UnreadCommentCount > 0;
            }
        }

        public int CurrentAcademicYear {
            get { return ((KxWebApplicationStudent) webApp).ApplicationsManager.CurrentAcademicYear.AcademicYearId; }
        }

        public int CurrentAcademicYearSession
        {
            get { return WebUser.CurrentAcademicYearSession; }
        }

        #endregion

        #region Constructors

        public KxStudentInventoryManager(KxWebApplicationStudent webApplication)
            : base(webApplication)
        {
        }

        #endregion

        private const string STR_CurrentInventory = "CurrentInventory";

        #region Private Methods

        private OccupantInventory GetInventoryForCurrentRoom()
        {
            int allocationId = GetAllocationIDForCurrentRoom();
            if (allocationId > 0)
            {
                return GetOccupantInventory(allocationId);
            }

            return null;
        }

        private int GetAllocationIDForCurrentRoom()
        {
            KxWebStudentAllocation allocation;

            if (KxDbConfiguration.GetConfigurationBool("CheckedInInventoryOnly", false))
            {
                if (CurrentAcademicYear > 0)
                {
                    allocation = WebUser.Allocations.GetInHouseAllocation(WebUser.Applications.GetWebApplicationForYearAndSession(CurrentAcademicYear, CurrentAcademicYearSession));
                }
                else
                {
                    allocation = WebUser.InHouseAllocation;
                }
            }
            else
            {
                if (CurrentAcademicYear > 0)
                {
                    allocation = WebUser.Allocations.GetInHouseAllocation(WebUser.Applications.GetWebApplicationForYearAndSession(CurrentAcademicYear, CurrentAcademicYearSession)) ??
                                 WebUser.Allocations.GetAcceptedAllocation(WebUser.Applications.GetWebApplicationForYearAndSession(CurrentAcademicYear, CurrentAcademicYearSession)) ??
                                 WebUser.Allocations.GetPendingAllocation(WebUser.Applications.GetWebApplicationForYearAndSession(CurrentAcademicYear, CurrentAcademicYearSession));
                }
                else
                {
                    allocation = WebUser.InHouseAllocation ?? WebUser.AcceptedAllocation ?? WebUser.PendingAllocation;
                }
            }

            if (allocation != null)
            {
                return allocation.AllocationId;
            }

            return -1;
        }

        private OccupantInventory GetOccupantInventory(int allocationId)
        {    
            object currentInventory = HttpContext.Current.Session[STR_CurrentInventory];
            if (currentInventory != null && (currentInventory as OccupantInventory).AllocationId == allocationId)
            {
                return RelabelItems(currentInventory as OccupantInventory);
            }

            OccupantInventory result = new OccupantInventory();
            string json = string.Empty;
            var serviceUri = GetServiceUri(string.Format("API/InventoryRoomAPI?allocationId={0}", allocationId));

            if (!string.IsNullOrWhiteSpace(KxDbConfiguration.GetConfiguration("DebugInventoryUrl", string.Empty)))
            {
                serviceUri = new Uri(KxDbConfiguration.GetConfiguration("DebugInventoryUrl"));
            }

            try
            {
                using (WebClient client = new WebClient())
                {
                    SetupSSLTrust();
                    
                    client.Headers[HttpRequestHeader.Accept] = "application/json";
                    client.Encoding = Encoding.UTF8;
                    json = client.DownloadString(serviceUri);

                    if (!string.IsNullOrEmpty(json))
                    {
                        result = JsonConvert.DeserializeObject<OccupantInventory>(json);
                    }
                }
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "GetOccupantInventory", serviceUri.ToString(), ex.Message);
            }

            HttpContext.Current.Session[STR_CurrentInventory] = result;

            return result;
        }

        private OccupantInventory RelabelItems(OccupantInventory occupantInventory)
        {
            foreach (var room in occupantInventory.Rooms)
            {
                foreach (var comment in room.RoomComments)
                {
                    RelabelComment(comment);
                }

                foreach (var item in room.Items)
                {
                    foreach (var comment in item.Comments)
                    {
                        RelabelComment(comment);
                    }
                }
            }

            return occupantInventory;
        }

        private void RelabelComment(OccupantInventoryComment comment)
        {
            if (!comment.StudentComment && !string.IsNullOrEmpty(AdminDisplay))
            {
                comment.CommentedBy = AdminDisplay;
            }
            else if (comment.StudentComment &&
                     StringComparer.InvariantCultureIgnoreCase.Compare(comment.CommentedBy,
                         string.Format("{0} {1}", WebUser.ForeName, WebUser.SurName)) == 0 && !string.IsNullOrEmpty(YouDisplay))
            {
                comment.CommentedBy = YouDisplay;
            }
        }

        private Uri GetServiceUri(string relativePath)
        {
            Uri result;

            if (!WebServiceURL.EndsWith("/"))
            {
                WebServiceURL += "/";
            }

            string path = string.Format("{0}{1}", WebServiceURL, relativePath);
            result = new Uri(path);

            KxLogger.LogActivity(this, "GetServiceUri", result.ToString(), string.Empty);
            return result;
        }

        private void SetupSSLTrust()
        {
            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            //if (this.TrustSSLCertificate)
            //{
            //    ServicePointManager.ServerCertificateValidationCallback =
            //        new RemoteCertificateValidationCallback(
            //            delegate { return true; }
            //            );
            //}
        }

        private void PostCommentToService(int occupantInventoryRoomId, int occupantInventoryItemId, string comment)
        {
            using (var webClient = new WebClient())
            {
                SetupSSLTrust();
                string postCommentString =
                    string.Format(
                        "occupantInventoryRoomId={0}&occupantInventoryItemId={1}&userid=0&kxStudentId={2}&comment={3}",
                        occupantInventoryRoomId, occupantInventoryItemId, WebUser.KxStudentId,
                        HttpUtility.UrlEncode(comment));

                byte[] postCommentBytes = Encoding.UTF8.GetBytes(postCommentString);

                try
                {
                    webClient.Headers.Add(HttpRequestHeader.ContentType, "application/x-www-form-urlencoded");
                    webClient.UploadData(GetServiceUri("API/InventoryCommentAPI"), "POST", postCommentBytes);
                }
                catch (WebException e)
                {
                    KxLogger.LogActivity(this, "PostCommentToService", postCommentString,
                        ((HttpWebResponse) e.Response).StatusCode.ToString());
                }
            }
        }

        internal void SaveComments(int occupantInventoryRoomId, bool clearCache = false)
        {
            foreach (var room in GetInventoryForCurrentRoom().Rooms)
            {
                if (room.OccupantInventoryRoomId == occupantInventoryRoomId)
                {
                    if (!string.IsNullOrEmpty(room.NewComment))
                    {
                        PostCommentToService(room.OccupantInventoryRoomId, 0, room.NewComment);
                        room.NewComment = string.Empty;
                    }

                    foreach (var item in room.Items)
                    {
                        if (!string.IsNullOrEmpty(item.NewComment))
                        {
                            PostCommentToService(room.OccupantInventoryRoomId, item.OccupantInventoryItemId,
                                item.NewComment);
                            item.NewComment = string.Empty;
                        }
                    }
                }
            }

            if (clearCache)
            {
                RemoveInventoryFromCache();
            }
        }

        private static void RemoveInventoryFromCache()
        {
            HttpContext.Current.Session.Remove(STR_CurrentInventory);
        }

        private void AcceptInventoryToWebService(int occupantInventoryRoomId)
        {
            using (var webClient = new WebClient())
            {
                SetupSSLTrust();
                string postString =
                    string.Format(
                        "userid=0&kxStudentID={0}&occupantInventoryRoomIds={1}", WebUser.KxStudentId,
                        occupantInventoryRoomId);

                byte[] postBytes = Encoding.UTF8.GetBytes(postString);

                try
                {
                    webClient.Headers.Add(HttpRequestHeader.ContentType, "application/x-www-form-urlencoded");
                    webClient.UploadData(GetServiceUri("API/InventoryRoomAPI"), "PUT", postBytes);
                }
                catch (WebException e)
                {
                    KxLogger.LogActivity(this, "AcceptInventoryToWebService", postString,
                        ((HttpWebResponse) e.Response).StatusCode.ToString());
                }
            }
        }

        private void DisputeInventoryToWebService(int occupantInventoryRoomId)
        {
            using (var webClient = new WebClient())
            {
                SetupSSLTrust();
                string postString =
                    string.Format(
                        "userid=0&kxStudentID={0}&occupantInventoryRoomIds={1}", WebUser.KxStudentId,
                        occupantInventoryRoomId);

                byte[] postBytes = Encoding.UTF8.GetBytes(postString);

                try
                {
                    webClient.Headers.Add(HttpRequestHeader.ContentType, "application/x-www-form-urlencoded");
                    webClient.UploadData(GetServiceUri("API/InventoryRoomDisputeAPI"), "POST", postBytes);
                }
                catch (WebException e)
                {
                    KxLogger.LogActivity(this, "DisputeInventoryToWebService", postString,
                        ((HttpWebResponse) e.Response).StatusCode.ToString());
                }
            }
        }

        #endregion

        #region Internal Methods

        internal void AcceptInventory(int occupantInventoryRoomId)
        {
            KxLogger.LogActivity(this, "AcceptInventory", "RoomID", occupantInventoryRoomId.ToString());
            SaveComments(occupantInventoryRoomId);
            AcceptInventoryToWebService(occupantInventoryRoomId);
            RemoveInventoryFromCache();
        }

        internal void DisputeInventory(int occupantInventoryRoomId)
        {
            KxLogger.LogActivity(this, "DisputeInventory", "RoomID", occupantInventoryRoomId.ToString());
            SaveComments(occupantInventoryRoomId);
            DisputeInventoryToWebService(occupantInventoryRoomId);
            RemoveInventoryFromCache();
        }

        internal void MarkCommentsAsRead()
        {
            int allocationId = GetAllocationIDForCurrentRoom();

            if (allocationId > 0)
            {
                MarkCommentsAsReadToWebService(allocationId);
            }
        }

        private void MarkCommentsAsReadToWebService(int allocationId)
        {
            using (var webClient = new WebClient())
            {
                SetupSSLTrust();
                string postString =
                    string.Format(
                        "userid=0&kxStudentID={0}&allocationId={1}", WebUser.KxStudentId, allocationId);

                byte[] postBytes = Encoding.UTF8.GetBytes(postString);

                try
                {
                    webClient.Headers.Add(HttpRequestHeader.ContentType, "application/x-www-form-urlencoded");
                    webClient.UploadData(GetServiceUri("API/InventoryCommentApi"), "PUT", postBytes);
                }
                catch (WebException e)
                {
                    KxLogger.LogActivity(this, "MarkCommentsAsReadToWebService", postString,
                        ((HttpWebResponse)e.Response).StatusCode.ToString());
                }
            }
        }

        #endregion

    }
}