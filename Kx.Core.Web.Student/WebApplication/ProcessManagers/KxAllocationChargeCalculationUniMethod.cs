using System;
using System.Collections.Generic;
using System.Text;
using Kx.Core.Base;
using Kx.Objects.Student;
using Kx.Core.Exceptions;
using Kx.Core.Configuration;

namespace Kx.Core.Web.Student.WebApplication.ProcessManagers
{
    public class KxAllocationChargeCalculationUniMethod : KxAllocationChargeCalculation
    {
        public int ResidentialRoomID { get; private set; }
        public int SiteID { get; private set; }
        public int AreaID { get; private set; }
        public int AcademicYearID { get; set; }

        public KxAllocationChargeCalculationUniMethod(KxBaseClass parent, KxWebApplicationStudent webApp, int siteId, int areaId, int residentialRoomId, int academicYearId)
            : base(parent, webApp)
        {
            ResidentialRoomID = residentialRoomId;
            SiteID = siteId;
            AreaID = areaId;
            AcademicYearID = academicYearId;
        }

        protected override KxStudentCharge AddStudentCharges(int allocDate, KxStudentChargeType StudentChargeType, DateTime? fromDate,
            DateTime? toDate, decimal Amount, int instalment, DateTime? dueDate)
        {
            KxStudentCharge result = new KxStudentCharge(this);

            try
            {

                result.KxStudentId = WebApp.WorkflowInterface.WebUser.KxStudentId;
                result.ApplicationId = WebApp.WorkflowInterface.WebUser.CurrentApplication.ApplicationId;
                result.AllocationDateId = allocDate;
                result.StudentChargeTypeId = StudentChargeType.StudentChargeTypeId;
                result.SiteId = SiteID;
                result.AreaId = AreaID;
                result.CreationDate = DateTime.Now;
                result.CreationUserId = 1;
                result.DateFrom = fromDate;
                result.DateTo = toDate;
                result.StatementText = StudentChargeType.StatementText;
                result.Amount = Amount;
                result.VatId = StudentChargeType.VatId;
                result.InstallmentNo = instalment;
                result.DueDate = dueDate;

                if (result.Vat != null)
                {
                    result.TotalAmount = Amount + (Amount * ((Decimal)((result.Vat.Rate) / 100)));
                }
                else
                {
                    result.TotalAmount = Amount;
                }

                result.Amount = Decimal.Round(result.Amount, 2);
                result.TotalAmount = Decimal.Round(result.TotalAmount, 2);

                result.PrintOnStatement = false;
                result.ResidentialRoomId = ResidentialRoomID;
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "AddStudentCharges", ex.Message, "");
            }
            return result;
        }

        public override decimal ProcessAllocationDate(KxStudentChargeType chargeType, KxStudentAllocationDate allocDate,
            KxAllocationModelDate modelDate, bool Save)
        {
            KxPricingModelCharge priceCharge = PricingModel.GetChargeByType(chargeType.StudentChargeTypeId, AcademicYearID);

            decimal result = 0.0M;

            if (priceCharge != null)
            {
                result = (priceCharge.WeeklyAmount * modelDate.FullWeeks) + (priceCharge.DailyAmount * modelDate.RemainderDays);
            }
            else
            {
                result = modelDate.StudentCharge1.DefaultCharge;
            }

            try
            {


                if ((KxDbConfiguration.GetConfiguration("SinglePaymentDiscount") != "") && Save)
                {
                    decimal discpercent = KxDbConfiguration.GetConfigurationDecimal("SinglePaymentDiscount", 0.00M);

                    result = result * (1 - (discpercent / 100));
                }


            }
            catch { }

            if (Save)
            {
                AddStudentCharges(allocDate.AllocationDateId, chargeType, allocDate.ArrivalDate, allocDate.DepartureDate, result, 0, modelDate.DueDate ?? modelDate.StartDate);
            }

            return result;
        }
    }
}
