﻿#region copyright

// =======================================================
// Product:   KCF.Student
// Project:   Kx.Core.Web.Student
// File:      KxPayToStudyManager.cs
// Created:   11/11/2016
// By:        Steve Ashley
// Modified:  22/11/2016
// 
// Copyright © Kinetic Solutions Ltd 2016
// All rights reserved.
// 
// No portion of this file may be re-created, copied or otherwise
// used without the express permission of Kinetic Solutions Ltd.
// =======================================================

#endregion

#region usings

using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data.SqlTypes;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Xml.Linq;
using Kx.Core.Configuration;
using Kx.Core.Exceptions;
using Kx.Core.PaymentProcessor.Cache;
using Kx.Core.PaymentProcessor.Payments;
using Kx.Core.Web.Student.Classes;
using Kx.Core.Web.Student.Payments;
using Kx.Core.Web.Student.PayToStudy;
using Kx.Core.Web.Student.WebApplication.BaseClass;
using Kx.Core.Web.Student.WebApplication.WorkflowInterface.WorkflowExtenders;
using Kx.Objects.Student;

#endregion

namespace Kx.Core.Web.Student.WebApplication.ProcessManagers
{
    public class KxPayToStudyManager : WebStudentBaseClass
    {
        #region Fields and Properties

        private const string STR_HistoryCacheID = "PayToStudy.PaymentHistory";

        internal PaymentCache Cache
        {
            get
            {
                if (HttpContext.Current.Application["paymentcache"] == null)
                {
                    HttpContext.Current.Application["paymentcache"] = new PaymentCache();
                }
                return (PaymentCache) HttpContext.Current.Application["paymentcache"];
            }
        }

        public string WhoIsMakingPayment { get; set; }
        public string PayerName { get; set; }
        public string PayerAddress { get; set; }
        public string PayerExternalUniqueId { get; set; }
        public string PayerCPFNumber { get; set; }
        public string PayerSSNumber { get; set; }
        public bool IndiaFeeExceeded { get; set; }
        public string PayerPAN { get; set; }
        public string PayerCIN { get; set; }
        public string PayerChineseName { get; set; }
        public string PayerEmail { get; set; }
        public string PayerPhoneNumber { get; set; }
        public string StudentName { get; set; }
        public string StudentEmail { get; set; }
        public string StudentPhoneNumber { get; set; }
        public string StudentNumber { get; set; }
        public string StudentCIN { get; set; }

        public string StudentChineseName { get; set; }
        public DateTime StudentDOB { get; set; }
        public string UniqueStudentID { get; set; }
        public string StudentHomeAddress { get; set; }
        public string PayFromCountry { get; set; }
        public string PayFromState { get; set; }
        public string ExternalRequestId { get; set; }
        public bool AcceptTerms { get; set; }
        public string Countries => GetCountryList();
        public string States => GetStateList();
        public string WhoIsPayingList => GetWhoIsPayingList();

        public string LastMessage { get; set; }

        public PayToStudyPaymentRequestResponse PaymentRequestResponse { get; private set; }
        public PayToStudyFXConversionResponse FXConversionResponse { get; private set; }

        public bool FXConversionAvailable
        {
            get { return FXConversionResponse != null; }
        }

        public List<PayToStudyHistoryResponsePayment> PaymentHistory { get; private set; } = new List<PayToStudyHistoryResponsePayment>();

        public string[] PaymentHistoryIds
        {
            get { return (from p in PaymentHistory select p.TransactionID).ToArray(); }
        }

        #endregion

        #region Constructors

        public KxPayToStudyManager(KxWebApplicationStudent webApp)
            : base(webApp)
        {
        }

        #endregion

        #region Public Methods

        public void GetPaymentHistoryFromPayToStudy(WorkflowPayToStudyHistoryExtender ext)
        {
            string response = SendRequestToPayToStudy(ext, GetPaymentHistoryInputs(ext));
            var responseObj = new PayToStudyHistoryResponse(response);
            PaymentHistory.Clear();
            PaymentHistory.AddRange(responseObj.Payments.AsEnumerable());
        }

        public bool ApplicationHasRequests(string paymentType)
        {
            return (WebUser.CurrentApplication.ApplicationGuid != Guid.Empty) && new PayToStudyRequests().ApplicationHasPayToStudyPayments(WebUser.CurrentApplication.ApplicationGuid, paymentType);
        }

        public bool ApplicationHasPendingRequests(string paymentType)
        {
            return (WebUser.CurrentApplication.ApplicationGuid != Guid.Empty) && new PayToStudyRequests().ApplicationHasPendingPayToStudyPayments(WebUser.CurrentApplication.ApplicationGuid, paymentType);
        }

        public bool WebUserHasRequests(string paymentType)
        {
            return (WebUser.UserGuid != Guid.Empty) && (WebUser.Active == 1) && new PayToStudyRequests().WebUserHasPayToStudyPayments(WebUser.UserGuid, paymentType);
        }

        public bool WebUserHasPendingRequests(string paymentType)
        {
            return (WebUser.UserGuid != Guid.Empty) && (WebUser.Active == 1) && new PayToStudyRequests().WebUserHasPendingPayToStudyPayments(WebUser.UserGuid, paymentType);
        }

        #endregion

        #region Private Methods

        private string GetWhoIsPayingList()
        {
            string result = string.Empty;
            string overrideResource = KxDbConfiguration.GetConfiguration("PayToStudyWhoIsPayingListResourceId");
            if (!string.IsNullOrWhiteSpace(overrideResource))
            {
                string whoisPayingXml = WebApp.ResourceProvider.GetResource(overrideResource);
                if (!string.IsNullOrWhiteSpace(whoisPayingXml))
                {
                    try
                    {
                        result = GetListFromLookupXml(whoisPayingXml);
                    }
                    catch (Exception ex)
                    {
                        KxLogger.LogActivity(this, "GetWhoIsPayingList", string.Format("Error getting list from resource: {0}", overrideResource), ex.Message);
                        result = string.Empty;
                    }
                }
            }

            if (string.IsNullOrWhiteSpace(result))
            {
                result = PopulateDefaultWhoIsPayingList();
            }

            return result;
        }


        private string GetStateList()
        {
            string result = string.Empty;
            string overrideResource = KxDbConfiguration.GetConfiguration("PayToStudyStateListResourceId");
            if (!string.IsNullOrWhiteSpace(overrideResource))
            {
                string countryXml = WebApp.ResourceProvider.GetResource(overrideResource);
                if (!string.IsNullOrWhiteSpace(countryXml))
                {
                    try
                    {
                        result = GetListFromLookupXml(countryXml);
                    }
                    catch (Exception ex)
                    {
                        KxLogger.LogActivity(this, "GetStateList", string.Format("Error getting list from resource: {0}", overrideResource), ex.Message);
                        result = string.Empty;
                    }
                }
            }

            if (string.IsNullOrWhiteSpace(result))
            {
                result = PopulateDefaultStateList();
            }

            return result;
        }

        private string PopulateDefaultWhoIsPayingList()
        {
            return GetLookupString("Kx.Core.Web.Student.PayToStudy.Lookups.PTSWhoIsPaying.txt");
        }

        private string PopulateDefaultStateList()
        {
            return GetLookupString("Kx.Core.Web.Student.PayToStudy.Lookups.PTSStates.txt");
        }

        private string GetCountryList()
        {
            string result = string.Empty;
            string overrideResource = KxDbConfiguration.GetConfiguration("PayToStudyCountryListResourceId");
            if (!string.IsNullOrWhiteSpace(overrideResource))
            {
                string countryXml = WebApp.ResourceProvider.GetResource(overrideResource);
                if (!string.IsNullOrWhiteSpace(countryXml))
                {
                    try
                    {
                        result = GetListFromLookupXml(countryXml);
                    }
                    catch (Exception ex)
                    {
                        KxLogger.LogActivity(this, "GetCountryList", string.Format("Error getting list from resource: {0}", overrideResource), ex.Message);
                        result = string.Empty;
                    }
                }
            }

            if (string.IsNullOrWhiteSpace(result))
            {
                result = PopulateDefaultCountryList();
            }

            return result;
        }

        private string PopulateDefaultCountryList()
        {
            return GetLookupString("Kx.Core.Web.Student.PayToStudy.Lookups.PTSCountries.txt");
        }

        private string GetLookupString(string resourceName)
        {
            return new EmbeddedResourceHelper().GetEmbeddedResource(resourceName);
        }

        private string GetListFromLookupXml(string xml)
        {
            XDocument doc = XDocument.Parse(xml);
            var results = from d in doc.Element("options").Elements("option")
                select new
                {
                    value = d.Attribute("value").Value,
                    text = d.Attribute("text").Value
                };

            StringBuilder result = new StringBuilder();
            foreach (var lookup in results)
            {
                result.AppendLine(string.Format("{0}|{1}", lookup.value, lookup.text));
            }

            return result.ToString();
        }

        private bool Validate(WorkflowPayToStudyExtender ext)
        {
            LastMessage = string.Empty;
            if (this.WebUser.Active < 1)
            {
                LastMessage = "Not Logged In";
            }
            else if (string.IsNullOrWhiteSpace(ext.PTSUsername))
            {
                LastMessage = "Pay To Study Username is missing";
            }
            else if (string.IsNullOrWhiteSpace(ext.PTSPassword))
            {
                LastMessage = "Pay To Study password is missing";
            }
            else if (string.IsNullOrWhiteSpace(ext.BeneficiaryBankAccountId))
            {
                LastMessage = "Beneficiary bank account is missing";
            }
            else if (string.IsNullOrWhiteSpace(ext.PaymentUrl))
            {
                LastMessage = "Pay to study payment URL is missing";
            }
            else if (string.IsNullOrWhiteSpace(StudentName))
            {
                LastMessage = "Student name is missing";
            }
            else if (string.IsNullOrWhiteSpace(StudentEmail))
            {
                LastMessage = "Student email address is missing";
            }
            else if (string.IsNullOrWhiteSpace(StudentPhoneNumber))
            {
                LastMessage = "Student telephone number is missing";
            }
            else if (string.IsNullOrWhiteSpace(StudentNumber))
            {
                LastMessage = "Student number is missing";
            }
            else if (string.IsNullOrWhiteSpace(StudentNumber))
            {
                LastMessage = "Student number is missing";
            }
            else if (string.IsNullOrWhiteSpace(UniqueStudentID))
            {
                LastMessage = "Student unique ID is missing";
            }
            else if (string.IsNullOrWhiteSpace(StudentHomeAddress))
            {
                LastMessage = "Student home address is missing";
            }
            else if (string.IsNullOrWhiteSpace(GetPayingFor(ext.PayingFor, ext.STATEMENTorDEPOSITorPAYNOW)))
            {
                LastMessage = "Paying For is missing";
            }
            else if (GetPaymentAmount(ext.PaymentAmount, ext.STATEMENTorDEPOSITorPAYNOW) <= 0m)
            {
                LastMessage = "Payment amount is missing";
            }
            else if (StudentDOB < new DateTime(1901, 1, 1))
            {
                LastMessage = "Student Date of Birth is missing";
            }
            else if (string.IsNullOrWhiteSpace(WhoIsMakingPayment))
            {
                LastMessage = "Who is making payment is required";
            }
            else if (string.IsNullOrWhiteSpace(PayerName))
            {
                LastMessage = "Payer Name is required";
            }
            else if (string.IsNullOrWhiteSpace(PayerAddress))
            {
                LastMessage = "Payer Address is required";
            }
            else if (string.IsNullOrWhiteSpace(PayerEmail))
            {
                LastMessage = "Payer Email Address is required";
            }
            else if (string.IsNullOrWhiteSpace(PayerPhoneNumber))
            {
                LastMessage = "Payer Telephone Number is required";
            }
            else if (string.IsNullOrWhiteSpace(PayFromCountry))
            {
                LastMessage = "Paying From Country is required";
            }
            if (string.IsNullOrWhiteSpace(LastMessage))
            {
                ValidateFormatting();
            }

            return string.IsNullOrWhiteSpace(LastMessage);
        }

        private bool ValidateFormatting()
        {
            LastMessage = string.Empty;

            if (ValidateBrazilFormatting())
            {
                if (ValidateUSFormatting())
                {
                    if (ValidateIndiaFormatting())
                    {
                        ValidateChinaFormatting();
                    }
                }
            }

            return string.IsNullOrWhiteSpace(LastMessage);
        }

        private bool ValidateChinaFormatting()
        {
            LastMessage = string.Empty;
            if (StringComparer.InvariantCultureIgnoreCase.Compare(PayFromCountry, "cn") == 0 && string.IsNullOrWhiteSpace(PayerCIN))
            {
                LastMessage = "Payer CIN is required for payments from China";
            }
            else if (StringComparer.InvariantCultureIgnoreCase.Compare(PayFromCountry, "cn") == 0 && !ValidateFormat(PayerCIN, @"[0-9A-Za-z]{18}"))
            {
                LastMessage = "Payer CIN should be 18 alpha-numeric characters only";
            }
            else if (StringComparer.InvariantCultureIgnoreCase.Compare(PayFromCountry, "cn") == 0 && string.IsNullOrWhiteSpace(PayerChineseName))
            {
                LastMessage = "Payer Chinese Name is required for payments from China";
            }
            else if (StringComparer.InvariantCultureIgnoreCase.Compare(PayFromCountry, "cn") == 0 && !ValidateFormat(PayerChineseName, @"^[\u4e00-\u9fa5]{2,50}"))
            {
                LastMessage = "Payer Chinese Name must be 2-50 Chinese characters only";
            }
            else if (StringComparer.InvariantCultureIgnoreCase.Compare(PayFromCountry, "cn") == 0 && string.IsNullOrWhiteSpace(StudentCIN))
            {
                LastMessage = "Student CIN is required for payments from China";
            }
            else if (StringComparer.InvariantCultureIgnoreCase.Compare(PayFromCountry, "cn") == 0 && !ValidateFormat(StudentCIN, @"[0-9A-Za-z]{18}"))
            {
                LastMessage = "Student CIN should be 18 alpha-numeric characters only";
            }
            else if (StringComparer.InvariantCultureIgnoreCase.Compare(PayFromCountry, "cn") == 0 && string.IsNullOrWhiteSpace(StudentChineseName))
            {
                LastMessage = "Student Chinese Name is required for payments from China";
            }
            else if (StringComparer.InvariantCultureIgnoreCase.Compare(PayFromCountry, "cn") == 0 && !ValidateFormat(StudentChineseName, @"^[\u4e00-\u9fa5]{2,50}"))
            {
                LastMessage = "Student Chinese Name must be 2-50 Chinese characters only";
            }
            return string.IsNullOrWhiteSpace(LastMessage);
        }

        private bool ValidateIndiaFormatting()
        {
            LastMessage = string.Empty;
            if (StringComparer.InvariantCultureIgnoreCase.Compare(PayFromCountry, "in") == 0 && string.IsNullOrWhiteSpace(PayerPAN))
            {
                LastMessage = "Payer PAN is required for payments from India";
            }
            else if (StringComparer.InvariantCultureIgnoreCase.Compare(PayFromCountry, "in") == 0 && !ValidateFormat(PayerPAN, @"[0-9A-Za-z]{10}"))
            {
                LastMessage = "Payer PAN should be 10 alpha-numeric characters only";
            }

            return string.IsNullOrWhiteSpace(LastMessage);
        }

        private bool ValidateUSFormatting()
        {
            LastMessage = string.Empty;
            if (StringComparer.InvariantCultureIgnoreCase.Compare(PayFromCountry, "us") == 0 && string.IsNullOrWhiteSpace(PayerSSNumber))
            {
                LastMessage = "Payer Social Security Number is required for payments from USA";
            }
            else if (StringComparer.InvariantCultureIgnoreCase.Compare(PayFromCountry, "us") == 0 && !ValidateFormat(PayerSSNumber, @"\d{3}-\d{2}-\d{4}"))
            {
                LastMessage = "Payer Social Security Number should be in the format nnn-nn-nnnn";
            }
            else if (StringComparer.InvariantCultureIgnoreCase.Compare(PayFromCountry, "us") == 0 && string.IsNullOrWhiteSpace(PayFromState))
            {
                LastMessage = "State is required for payments from USA";
            }

            return string.IsNullOrWhiteSpace(LastMessage);
        }

        private bool ValidateBrazilFormatting()
        {
            LastMessage = string.Empty;
            if (StringComparer.InvariantCultureIgnoreCase.Compare(PayFromCountry, "br") == 0 && string.IsNullOrWhiteSpace(PayerCPFNumber))
            {
                LastMessage = "Payer CPF is required for payments from Brazil";
            }
            else if (StringComparer.InvariantCultureIgnoreCase.Compare(PayFromCountry, "br") == 0 && !ValidateFormat(PayerCPFNumber, @"\d{3}.\d{3}.\d{3}-\d{2}"))
            {
                LastMessage = "Payer CPF should be in the format nnn.nnn.nnn-nn";
            }

            return string.IsNullOrWhiteSpace(LastMessage);
        }

        private decimal GetPaymentAmount(string paymentAmount, string paymentType)
        {
            string amountString = WebApp.ObjectTranslator.MergeTags(paymentAmount);
            if (string.IsNullOrWhiteSpace(amountString) || paymentType == "STATEMENT")
            {
                amountString = WebApp.StatementManager.SelectedBalance.ToString("F2");
            }

            decimal result = 0m;
            decimal.TryParse(amountString, out result);
            return result;
        }

        private string GetPayingFor(string payingFor, string paymentType)
        {
            string result = WebApp.ObjectTranslator.MergeTags(payingFor);
            if (string.IsNullOrWhiteSpace(result) || paymentType == "STATEMENT")
            {
                foreach (string s in WebApp.StatementManager.PayableCharges)
                {
                    KxStudentStatement stat = WebApp.StatementManager.GetStudentStatement(s);
                    if (stat.Selected)
                    {
                        result = stat.StatementText;
                    }
                }
            }
            return result;
        }

        private bool ValidateFormat(string payerCPFNumber, string regExString)
        {
            return Regex.IsMatch(payerCPFNumber, regExString, RegexOptions.CultureInvariant | RegexOptions.IgnoreCase | RegexOptions.IgnorePatternWhitespace);
        }

        private PaymentRequest GeneratePaymentRequestObject(WorkflowPayToStudyExtender ext)
        {
            PaymentRequest pr = new PaymentRequest();
            pr.UniqueIdentifier = Guid.NewGuid();
            pr.PaymentProvider = "PAYTOSTUDY";
            pr.WorkflowGuid = WebApp.WorkflowInterface.WorkflowGuid.ToString();
            pr.System = KxSystem.Student;
            if (WebUser.Student != null)
            {
                pr.StudentId = WebUser.Student.StudentId;
            }
            pr.CustomerId = pr.StudentId;
            pr.BookingReference = WebApp.WorkflowInterface.WorkflowGuid.ToString();
            pr.ApplicationId = WebUser.CurrentApplication.ApplicationId.ToString();
            pr.ApplicationGuid = WebUser.CurrentApplication.ApplicationGuid;
            pr.Email = WebUser.EmailAddress;
            pr.ProviderUrl = ext.PaymentUrl;
            pr.ProviderCallbackUrl = string.Empty;
            if (StringComparer.InvariantCultureIgnoreCase.Compare(ext.STATEMENTorDEPOSITorPAYNOW, "STATEMENT") == 0)
            {
                pr.ConsolidatedPayments = GetStatementPayments();
            }
            pr.Description = GetPayingFor(ext.PayingFor, ext.STATEMENTorDEPOSITorPAYNOW);
            pr.Amount = GetPaymentAmount(ext.PaymentAmount, ext.STATEMENTorDEPOSITorPAYNOW);
            pr.PaymentType = ext.STATEMENTorDEPOSITorPAYNOW;
            return pr;
        }

        private List<KxConsolidatedPayment> GetStatementPayments()
        {
            List<KxConsolidatedPayment> payments = new List<KxConsolidatedPayment>();
            foreach (string s in WebApp.StatementManager.PayableCharges)
            {
                KxStudentStatement stat = WebApp.StatementManager.GetStudentStatement(s);
                if (stat.Selected)
                {
                    KxConsolidatedPayment pay = new KxConsolidatedPayment(stat.StudentChargeId, stat.StatementText, stat.OutStandingBalance);
                    payments.Add(pay);
                }
            }
            return payments;
        }

        private NameValueCollection GetPaymentRequestInputs(PaymentRequest pr, WorkflowPayToStudyExtender ext)
        {
            NameValueCollection result = new NameValueCollection();
            result.Add("username_loginto", ext.PTSUsername);
            result.Add("password_loginto", ext.PTSPassword);
            if (!string.IsNullOrWhiteSpace(ext.BeneficiaryBankAccountId))
            {
                result.Add("bank_account_id", WebApp.ObjectTranslator.MergeTags(ext.BeneficiaryBankAccountId));
            }
            result.Add("who_is_making_the_payment", WhoIsMakingPayment);
            result.Add("payer_name", PayerName);
            result.Add("payer_address", PayerAddress);
            if (PayFromCountry == "br")
            {
                result.Add("payer_cpf", PayerCPFNumber);
            }
            else if (PayFromCountry == "us")
            {
                result.Add("payer_ssn", PayerSSNumber);
                result.Add("state", PayFromState);
            }
            else if (PayFromCountry == "in")
            {
                result.Add("india_fee_exceed", IndiaFeeExceeded ? "1" : "0");
                result.Add("payer_pan", PayerPAN);
            }
            else if (PayFromCountry == "cn")
            {
                result.Add("payer_cin", PayerCIN);
                result.Add("payer_chinese_name", PayerChineseName);
                result.Add("student_cin", StudentCIN);
                result.Add("student_chinese_name", StudentChineseName);
            }
            result.Add("email", PayerEmail);
            result.Add("phone_number", PayerPhoneNumber);
            result.Add("student_name", StudentName);
            result.Add("student_email", StudentEmail);
            result.Add("student_phone_number", StudentPhoneNumber);
            result.Add("student_number", StudentNumber);
            result.Add("dob_student", StudentDOB.ToString("dd-MM-yyyy"));
            result.Add("external_unique_id", UniqueStudentID);
            result.Add("home_address", StudentHomeAddress);
            result.Add("country_pay_from", PayFromCountry);
            result.Add("paying_for", pr.Description);
            result.Add("payment_amount", pr.Amount.ToString("F2"));
            result.Add("external_request_id", pr.UniqueIdentifier.ToString("N"));
            if (!string.IsNullOrWhiteSpace(ext.AdditionalField1))
            {
                result.Add("adi_id", WebApp.ObjectTranslator.MergeTags(ext.AdditionalField1));
            }
            if (!string.IsNullOrWhiteSpace(ext.AdditionalField2))
            {
                result.Add("org_id", WebApp.ObjectTranslator.MergeTags(ext.AdditionalField2));
            }

            return result;
        }

        private NameValueCollection GetPaymentHistoryInputs(WorkflowPayToStudyHistoryExtender ext)
        {
            NameValueCollection result = new NameValueCollection();
            result.Add("username_loginto", ext.PTSUsername);
            result.Add("password_loginto", ext.PTSPassword);
            result.Add("PaymentDetails", "1");
            result.Add("paymentDetails_ext_id", WebUser.Student.KxStudentId.ToString());

            return result;
        }

        private string SendRequestToPayToStudy(IHaveSettingsForPayToStudy ext, NameValueCollection inputs)
        {
            string response = string.Empty;
            StringBuilder postData = new StringBuilder();
            try
            {
                using (WebClient client = new WebClient())
                {
                    client.Headers.Add("Content-Type", "application/x-www-form-urlencoded");
                    ServicePointManager.ServerCertificateValidationCallback += (sender, certificate, chain, errors) => true;
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12 | SecurityProtocolType.Ssl3;

                    foreach (string key in inputs.AllKeys)
                    {
                        postData.AppendFormat("{2}{0}={1}", key, HttpUtility.UrlEncode(inputs[key]), postData.Length > 0 ? "&" : string.Empty);
                    }
                    KxLogger.LogActivity(this, "SendPaymentRequestToPayToStudy", "POST data", postData.ToString());
                    // Convert Post Data to Bytes
                    byte[] postDataBytes = Encoding.UTF8.GetBytes(postData.ToString());

                    // Call the web server
                    byte[] responseDataBytes = client.UploadData(ext.PaymentUrl, "POST", postDataBytes);

                    // Return the Result
                    response = Encoding.UTF8.GetString(responseDataBytes);
                }
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "SendPaymentRequestToPayToStudy", ex.Message, postData.ToString());
                response = string.Empty;
            }

            return response;
        }

        private void DecodePaymentRequestResponse(string responseString, PaymentRequest pr, WorkflowPayToStudyExtender ext)
        {
            PaymentResponse paymentResponse = new PaymentResponse();
            paymentResponse.InternalReference = pr.UniqueIdentifier.ToString("N");
            paymentResponse.OriginalRequest = Cache.GetPayment(paymentResponse);

            var inputs = new Dictionary<string, string>();
            inputs.Add("Xml", responseString);

            KxWebStudentPaymentAudit.LogInBoundPayment(paymentResponse, inputs);

            try
            {
                XDocument doc = XDocument.Parse(responseString);
                PaymentRequestResponse = new PayToStudyPaymentRequestResponse(doc);
                if (PaymentRequestResponse.Successful)
                {
                    paymentResponse.ProviderReference = PaymentRequestResponse.TransactionId;
                    paymentResponse.InternalReference = PaymentRequestResponse.RequestId;
                    paymentResponse.OriginalRequest = Cache.GetPaymentSafe(paymentResponse, true);
                }
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "DecodeResponse", ex.Message, responseString);
                LastMessage = ex.Message;
            }
        }

        private void DecodeFXConversionResponse(string responseString, WorkflowPayToStudyExtender ext)
        {
            try
            {
                XDocument doc = XDocument.Parse(responseString);
                FXConversionResponse = new PayToStudyFXConversionResponse(doc);
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "DecodeResponse", ex.Message, responseString);
                LastMessage = ex.Message;
            }
        }

        private NameValueCollection GetFxConversionRequestInputs(WorkflowPayToStudyExtender ext)
        {
            NameValueCollection result = new NameValueCollection();
            result.Add("username_loginto", ext.PTSUsername);
            result.Add("password_loginto", ext.PTSPassword);
            result.Add("convertOnly", "1");
            if (!string.IsNullOrWhiteSpace(ext.BeneficiaryBankAccountId))
            {
                result.Add("bank_account_id", WebApp.ObjectTranslator.MergeTags(ext.BeneficiaryBankAccountId));
            }
            result.Add("country_pay_from", PayFromCountry);
            result.Add("payment_amount", GetPaymentAmount(ext.PaymentAmount, ext.STATEMENTorDEPOSITorPAYNOW).ToString("F02"));
            if (PayFromCountry == "br")
            {
                result.Add("payer_cpf", PayerCPFNumber);
            }

            return result;
        }

        private bool EnoughDataToRequestFxConversion(WorkflowPayToStudyExtender ext)
        {
            return !string.IsNullOrWhiteSpace(ext.BeneficiaryBankAccountId) && GetPaymentAmount(ext.PaymentAmount, ext.STATEMENTorDEPOSITorPAYNOW) != 0m && !string.IsNullOrWhiteSpace(PayFromCountry) && ValidateBrazilFormatting();
        }

        #endregion

        #region Internal Methods

        internal bool RequestFXConversion(WorkflowPayToStudyExtender ext)
        {
            FXConversionResponse = null;
            if (EnoughDataToRequestFxConversion(ext))
            {
                string response = SendRequestToPayToStudy(ext, GetFxConversionRequestInputs(ext));
                if (!string.IsNullOrWhiteSpace(response))
                {
                    DecodeFXConversionResponse(response, ext);
                    if (FXConversionResponse.Successful)
                    {
                        return true;
                    }
                    else
                    {
                        LastMessage = FXConversionResponse.ErrorMessage;
                    }
                }
                else
                {
                    LastMessage = "No response from Pay To Study";
                }
            }

            return false;
        }

        internal void InitData()
        {
            PaymentRequestResponse = null;
            WhoIsMakingPayment = string.Empty;
            PayerName = string.Empty;
            PayerAddress = string.Empty;
            PayerExternalUniqueId = string.Empty;
            PayerCPFNumber = string.Empty;
            PayerSSNumber = string.Empty;
            IndiaFeeExceeded = false;
            PayerPAN = string.Empty;
            PayerCIN = string.Empty;
            PayerChineseName = string.Empty;
            PayerEmail = string.Empty;
            PayerPhoneNumber = string.Empty;
            StudentName = string.Empty;
            StudentEmail = string.Empty;
            StudentPhoneNumber = string.Empty;
            StudentNumber = string.Empty;
            StudentCIN = string.Empty;
            StudentChineseName = string.Empty;
            StudentDOB = DateTime.MinValue;
            UniqueStudentID = string.Empty;
            StudentHomeAddress = string.Empty;
            PayFromCountry = string.Empty;
            PayFromState = string.Empty;
            ExternalRequestId = string.Empty;
            LastMessage = string.Empty;

            if (WebUser.Student != null)
            {
                StudentName = string.Format("{0} {1}", WebUser.Student.ForeName, WebUser.Student.SurName);
                StudentEmail = WebUser.Student.EmailAddress;
                StudentPhoneNumber = WebUser.Student.CurrentAddress.Telephone;
                StudentNumber = WebUser.Student.StudentId;
                StudentDOB = WebUser.Student.Dob.GetValueOrDefault(SqlDateTime.MinValue.Value);
                UniqueStudentID = WebUser.Student.KxStudentId.ToString();
                StudentHomeAddress = WebUser.Student.HomeAddress.FormattedAddressSL;
            }
        }

        internal bool ProcessPayment(WorkflowPayToStudyExtender ext)
        {
            if (Validate(ext))
            {
                var pr = GeneratePaymentRequestObject(ext);
                if (Cache.AddPayment(pr))
                {
                    var inputs = GetPaymentRequestInputs(pr, ext);
                    KxWebStudentPaymentAudit.LogOutBoundPayment(pr, inputs);
                    string response = SendRequestToPayToStudy(ext, inputs);
                    if (!string.IsNullOrWhiteSpace(response))
                    {
                        DecodePaymentRequestResponse(response, pr, ext);
                        if (PaymentRequestResponse.Successful)
                        {
                            return true;
                        }
                        else
                        {
                            LastMessage = PaymentRequestResponse.ErrorMessage;
                        }
                    }
                    else
                    {
                        LastMessage = "No response from Pay To Study";
                    }
                }
                else
                {
                    LastMessage = "Error creating payment cache row";
                }
            }

            return false;
        }

        #endregion
    }
}