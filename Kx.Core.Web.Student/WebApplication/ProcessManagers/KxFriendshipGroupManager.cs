﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Core.Web.Student.WebApplication.BaseClass;
using Kx.Objects.Student.Web;

namespace Kx.Core.Web.Student.WebApplication.ProcessManagers
{
    public class KxFriendshipGroupManager : WebStudentBaseClass
    {
        public string GroupMembershipStatus { get; set; }
        public string GroupCode { get; set; }
        public string GroupCodeValidationEmail { get; set; }


        public string Member1Name { get; set; }
        public string Member2Name { get; set; }
        public string Member3Name { get; set; }
        public string Member4Name { get; set; }
        public string Member5Name { get; set; }
        public string Member6Name { get; set; }
        public string Member7Name { get; set; }
        public string Member8Name { get; set; }
        public string Member9Name { get; set; }
        public string Member1Email { get; set; }
        public string Member2Email { get; set; }
        public string Member3Email { get; set; }
        public string Member4Email { get; set; }
        public string Member5Email { get; set; }
        public string Member6Email { get; set; }
        public string Member7Email { get; set; }
        public string Member8Email { get; set; }
        public string Member9Email { get; set; }

        public KxFriendshipGroupManager(KxWebApplicationStudent webApp)
            : base(webApp)
        {
            
        }

    }
}
