﻿#region copyright

// =======================================================
// Product:   KCF.Student
// Project:   Kx.Core.Web.Student
// File:      KxWesternUnionManager.cs
// Created:   02/01/2018
// By:        Steve Ashley
// Modified:  07/02/2018
// 
// Copyright © Kinetic Solutions Ltd 2018
// All rights reserved.
// 
// No portion of this file may be re-created, copied or otherwise
// used without the express permission of Kinetic Solutions Ltd.
// =======================================================

#endregion

#region usings

using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Caching;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Linq;
using Kx.Core.Configuration;
using Kx.Core.Data;
using Kx.Core.Exceptions;
using Kx.Core.Localisation;
using Kx.Core.PaymentProcessor.Cache;
using Kx.Core.PaymentProcessor.Payments;
using Kx.Core.Utility;
using Kx.Core.Web.Student.WebApplication.BaseClass;
using Kx.Core.Web.Student.WebApplication.WorkflowInterface.WorkflowExtenders;
using Kx.Core.Web.Student.WesternUnion;
using Kx.Core.WesternUnion;
using Kx.Core.WesternUnion.DataObj.Requests;
using Kx.Core.WesternUnion.DataObj.Responses;
using Kx.Core.WesternUnion.Helpers;
using Kx.Core.WesternUnion.Lookups;
using Kx.Core.WesternUnion.Requests;
using Kx.Core.WesternUnion.Responses;
using Kx.Objects.Student;
using Kx.Objects.Student.Web;

#endregion

namespace Kx.Core.Web.Student.WebApplication.ProcessManagers
{
    public class KxWesternUnionManager : WebStudentBaseClass
    {
        #region Fields and Properties

        private const string STR_HistoryCacheID = "WesternUnion.PaymentHistory";

        internal PaymentCache Cache
        {
            get
            {
                if (HttpContext.Current.Application["paymentcache"] == null)
                {
                    HttpContext.Current.Application["paymentcache"] = new PaymentCache();
                }

                return (PaymentCache) HttpContext.Current.Application["paymentcache"];
            }
        }

        public string Countries => GetCountryList();

        public string PayFromCountry { get; set; }

        public string PaymentCurrency { get; set; }
        public string SettlementCurrency { get; set; }
        public string PaymentType { get; set; }

        public string InstructionLanguage { get; set; } = "en_GB";

        public string RemitterName { get; set; }
        public string RemitterAddress1 { get; set; }
        public string RemitterAddress2 { get; set; }
        public string RemitterCity { get; set; }
        public string RemitterState { get; set; }
        public string RemitterZip { get; set; }

        public string PayerStudentId { get; set; }
        public string PayerFirstName { get; set; }
        public string PayerSurName { get; set; }
        public string PayerEmail { get; set; }
        public string RemitterEmail { get; set; }
        public string BankName { get; set; }
        public string BankAddress1 { get; set; }
        public string BankAddress2 { get; set; }
        public string BankCity { get; set; }
        public string BankState { get; set; }
        public string BankZip { get; set; }
        private PaymentRequest LastPaymentRequest { get; set; }
        private int KxOrderId { get; set; }
        private string PayerCustom1 { get; set; }
        private string PayerCustom2 { get; set; }
        private string PayerCustom3 { get; set; }
        private string PayerCustom4 { get; set; }
        private string PayerCustom5 { get; set; }
        private string PayerCustom6 { get; set; }
        private string PayerCustom7 { get; set; }
        private string PayerCustom8 { get; set; }
        private string PayerCustom9 { get; set; }
        private string PayerCustom10 { get; set; }
        public QuoteOrder Quote { get; private set; }
        public LogQuotedOrder Order { get; private set; }

        public ListItemCollection CountriesListItems => GetCountriesListItems();
        public ListItemCollection PaymentCurrenciesListItems => GetPaymentCurrencyListItems();

        public ListItemCollection PaymentTypesForCurrencyListItems => GetPaymentTypesForCurrencyListItems();

        public string InstructionLanguages => GetLanguageList();

        public StringBuilder LastMessage { get; set; } = new StringBuilder();

        public string APIUrl { get; set; }
        public string GPFIClientId { get; set; }

        internal List<WesternUnionPaymentHistory> Payments = new List<WesternUnionPaymentHistory>();

        public string[] PaymentIds
        {
            get
            {
                return (from p in Payments
                    select p.CacheId.ToString("N")).ToArray();
            }
        }

        public void LoadPaymentHistory(WorkflowWesternUnionHistoryExtender ext)
        {
            Payments.Clear();
            KxData data = new KxData("payments", @"SELECT WPC.CacheId, WUO.KxOrderId, WUO.WUDealReference, WUO.WUOrderId, WUO.CreationDate, COALESCE(WUO.LastStatus, 'PENDING') AS LastStatus, COALESCE(WUO.LastStatusChangeDate, WUO.CreationDate) AS LastStatusChangeDate, SUM(WPCP.RequestedAmount) AS Amount FROM WesternUnionOrders WUO JOIN WebPaymentCache WPC ON WPC.CacheId = WUO.KxBookingIdentifier 
JOIN WebPaymentCachePayment WPCP ON WPCP.CacheId = WPC.CacheId
WHERE ISNULL(WUO.WUDealReference, '') <> '' AND WPC.TransactionGuid = @ApplicationGuid
GROUP BY WPC.CacheId, WUO.WUDealReference, WUO.WUOrderId, COALESCE(WUO.LastStatus, 'PENDING'), COALESCE(WUO.LastStatusChangeDate, WUO.CreationDate), WUO.CreationDate, WUO.KxOrderId
ORDER BY WUO.CreationDate DESC", TargetType.Static);
            data.AddParameter("applicationguid", WebUser.CurrentApplication.ApplicationGuid, KxValueType.Guid);
            if (data.Execute())
            {
                foreach (XmlNode node in data.GetNodesFromData())
                {
                    Payments.Add(new WesternUnionPaymentHistory(node));
                }
            }

        }

        #endregion

        #region Constructors

        public KxWesternUnionManager(KxWebApplicationStudent webApp)
            : base(webApp)
        {
        }

        #endregion

        #region Public Methods

        public void Reset()
        {
            Quote = null;
            Order = null;
            KxOrderId = 0;
            PayFromCountry = string.Empty;
            PaymentCurrency = string.Empty;
            PaymentType = string.Empty;
            InstructionLanguage = "en_GB";
            RemitterName = string.Empty;
            RemitterAddress1 = string.Empty;
            RemitterAddress2 = string.Empty;
            RemitterCity = string.Empty;
            RemitterState = string.Empty;
            RemitterZip = string.Empty;
            RemitterEmail = string.Empty;
            PayerStudentId = string.Empty;
            PayerFirstName = string.Empty;
            PayerSurName = string.Empty;
            PayerEmail = string.Empty;
            PayerCustom1 = string.Empty;
            PayerCustom2 = string.Empty;
            PayerCustom3 = string.Empty;
            PayerCustom4 = string.Empty;
            PayerCustom5 = string.Empty;
            PayerCustom6 = string.Empty;
            PayerCustom7 = string.Empty;
            PayerCustom8 = string.Empty;
            PayerCustom9 = string.Empty;
            PayerCustom10 = string.Empty;
            BankName = string.Empty;
            BankAddress1 = string.Empty;
            BankAddress2 = string.Empty;
            BankCity = string.Empty;
            BankState = string.Empty;
            BankZip = string.Empty;
        }
        
        public bool QuoteOrder(WorkflowWesternUnionExtender ext)
        {
            Quote = null;
            this.KxOrderId = 0;

            if (ValidateForQuote(ext))
            {
                LastMessage.Clear();
                LastPaymentRequest = GeneratePaymentRequestObject(ext);
                GetCustomValues(ext);
                if (Cache.AddPayment(LastPaymentRequest))
                {
                    QuoteOrderRequest request = new QuoteOrderRequest(LastPaymentRequest.UniqueIdentifier.ToString("D"), GetQuoteOrderRequestData(LastPaymentRequest), (level, method, message, data) =>
                    {
                        WesternUnionLogger(level, method, message, data);
                        if (data == "OUT")
                        {
                            var inputs = new NameValueCollection();
                            inputs.Add("Xml", message);
                            KxWebStudentPaymentAudit.LogOutBoundPayment(LastPaymentRequest, inputs);
                        }
                    });

                    var response = SendRequestToWesternUnion(request) as QuoteOrderResponse;
                    LogQuoteResponseToCache(response);
                    this.KxOrderId = request.KxOrderId;
                    if (response.Successful)
                    {
                        Quote = response.Quote;
                        return true;
                    }
                    else
                    {
                        foreach (var message in response.Messages)
                        {
                            LastMessage.AppendLine(message);
                        }
                    }
                }
            }

            return false;
        }

        public bool ApplicationHasWesternUnionOrders(string paymentType)
        {
            KxData data = new KxData("result", string.Format("SELECT COUNT(*) AS Result FROM WebPaymentCache PC WHERE PC.Provider = 'WesternUnion' AND PC.AuthorisationCode IS NOT NULL AND PC.TransactionGuid = @applicationguid AND @paymenttype IN ('', (SELECT TOP 1 PaymentType FROM WebPaymentCachePayment P WHERE P.CacheID = PC.CacheID))", WebUser.CurrentApplication.ApplicationGuid, paymentType), TargetType.Static);
            data.AddParameter("applicationguid", WebUser.CurrentApplication.ApplicationGuid.ToString("D"), KxValueType.String, 50);
            data.AddParameter("paymenttype", paymentType, KxValueType.String, 50);
            int? result = (int?)data.ExecuteScalar();
            if (string.IsNullOrWhiteSpace(data.LastError) && result.HasValue)
            {
                return result.Value > 0;
            }
            else
            {
                KxLogger.LogActivity(this, "ApplicationHasWesternUnionPayments", data.LastError, WebUser.CurrentApplication.ApplicationGuid.ToString("D"));
            }

            return false;
        }

        public bool ApplicationHasDisbursedOrPostedWesternUnionOrders(string paymentType)
        {
            KxData data = new KxData("result", string.Format("SELECT COUNT(*) AS Result FROM WebPaymentCache PC WHERE PC.Provider = 'WesternUnion' AND PC.AuthorisationCode IS NOT NULL AND (PC.Authorised = 1 OR EXISTS (SELECT NULL FROM WesternUnionOrders WO WHERE WO.KxBookingIdentifier = PC.CacheId AND WO.LastStatus = 'DEAL_POSTED')) AND PC.TransactionGuid = @applicationguid AND @paymenttype IN ('', (SELECT TOP 1 PaymentType FROM WebPaymentCachePayment P WHERE P.CacheID = PC.CacheID))", WebUser.CurrentApplication.ApplicationGuid, paymentType), TargetType.Static);
            data.AddParameter("applicationguid", WebUser.CurrentApplication.ApplicationGuid.ToString("D"), KxValueType.String, 50);
            data.AddParameter("paymenttype", paymentType, KxValueType.String, 50);
            int? result = (int?)data.ExecuteScalar();
            if (string.IsNullOrWhiteSpace(data.LastError) && result.HasValue)
            {
                return result.Value > 0;
            }
            else
            {
                KxLogger.LogActivity(this, "ApplicationHasDisbursedOrPostedWesternUnionOrders", data.LastError, WebUser.CurrentApplication.ApplicationGuid.ToString("D"));
            }

            return false;
        }

        public bool ApplicationHasDisbursedWesternUnionOrders(string paymentType)
        {
            KxData data = new KxData("result", string.Format("SELECT COUNT(*) AS Result FROM WebPaymentCache PC WHERE PC.Provider = 'WesternUnion' AND PC.AuthorisationCode IS NOT NULL AND PC.Authorised = 1 AND PC.TransactionGuid = @applicationguid AND @paymenttype IN ('', (SELECT TOP 1 PaymentType FROM WebPaymentCachePayment P WHERE P.CacheID = PC.CacheID))", WebUser.CurrentApplication.ApplicationGuid, paymentType), TargetType.Static);
            data.AddParameter("applicationguid", WebUser.CurrentApplication.ApplicationGuid.ToString("D"), KxValueType.String, 50);
            data.AddParameter("paymenttype", paymentType, KxValueType.String, 50);
            int? result = (int?)data.ExecuteScalar();
            if (string.IsNullOrWhiteSpace(data.LastError) && result.HasValue)
            {
                return result.Value > 0;
            }
            else
            {
                KxLogger.LogActivity(this, "ApplicationHasDisbursedWesternUnionOrders", data.LastError, WebUser.CurrentApplication.ApplicationGuid.ToString("D"));
            }

            return false;
        }

        private void LogQuoteResponseToCache(QuoteOrderResponse response)
        {
            ClearSessionVariableToAllowCacheINWrite();
            KxWebStudentPaymentAudit.LogInBoundPayment(new PaymentResponse() {InternalReference = LastPaymentRequest.UniqueIdentifier.ToString(), Authorised = false, OriginalRequest = LastPaymentRequest, ProviderReference = response.Successful ? response.Quote.OrderId : ""}, LoadResponseXmlIntoDictionary(response));
        }

        public string SavePDFInstructionsToFile()
        {
            LastMessage.Clear();
            string tempPath = KxDbConfiguration.GetConfiguration("TempFileLocation", @"C:\Temp\");
            PaymentInstructionRequest request = new PaymentInstructionRequest(this.KxOrderId, WesternUnionLogger);
            var response = SendRequestToWesternUnion(request);
            if(response.Successful)
            {
                var pdfResponse = response as PaymentInstructionResponse;
                if (pdfResponse != null)
                {
                    string fileName = Path.Combine(tempPath, string.Format("PaymentInstructions-{0}.pdf", KxOrderId));
                    File.WriteAllBytes(fileName, pdfResponse.Instruction.PDFBytes);

                    SaveInstructionsToCorrespondence(pdfResponse);

                    return fileName;
                }
            }
            else
            {
                foreach (string message in response.Messages)
                {
                    LastMessage.AppendLine(message);
                }
            }


            return string.Empty;
        }

        public byte[] GetPDFInstructionsByte(Guid? cacheId = null)
        {
            if (cacheId.HasValue)
            {
                var payment = (from p in Payments
                    where p.CacheId == cacheId.Value
                    select p).FirstOrDefault();
                if (payment != null)
                {
                    this.KxOrderId = payment.KxOrderId;
                }
            }
            LastMessage.Clear();
            if (this.KxOrderId > 0)
            {
                PaymentInstructionRequest request = new PaymentInstructionRequest(this.KxOrderId, WesternUnionLogger);
                var response = SendRequestToWesternUnion(request);
                if (response.Successful)
                {
                    var pdfResponse = response as PaymentInstructionResponse;
                    if (pdfResponse != null)
                    {
                        return pdfResponse.Instruction.PDFBytes;
                    }
                }
                else
                {
                    foreach (string message in response.Messages)
                    {
                        LastMessage.AppendLine(message);
                    }
                }
            }
            else
            {
                this.LastMessage.AppendLine("No valid order found");
            }


            return null;
        }

        private void SaveInstructionsToCorrespondence(PaymentInstructionResponse pdfResponse)
        {
            try
            {
                KxStudentCorrespondence corresp = new KxStudentCorrespondence(null);
                corresp.KxStudentId = WebUser.KxStudentId;
                corresp.ApplicationId = WebUser.CurrentApplication.ApplicationId;
                corresp.CreationDate = DateTime.Now;
                corresp.CreationUserId = 1;
                corresp.DocumentType = "Western Union Payment Instructions";
                corresp.Description = "Western Union Payment Instructions";
                corresp.DocumentName = "PaymentInstructions.pdf";
                corresp.DocumentImageType = 0;
                corresp.Document = KxDocumentCompression.CompressBytes(pdfResponse.Instruction.PDFBytes, corresp.DocumentName);
                corresp.DocumentText = string.Empty;
                corresp.ResponseRequestedBy = KxDateTime.KxNullDate;
                corresp.ResponseReceived = KxDateTime.KxNullDate;
                corresp.DocumentTypeId = 0;
                corresp.DocumentCreated = DateTime.Now;
                corresp.Sent = DateTime.Now;
                if (!corresp.Save())
                {
                    KxLogger.LogActivity(this, "SaveInstructionsToCorrespondence", corresp.LastError, string.Empty);
                }
            }
            catch (Exception e)
            {
                KxLogger.LogActivity(this, "SaveInstructionsToCorrespondence", e.Message, string.Empty);

                // We don't care if it hasn't saved...
            }
        }

        private void ClearSessionVariableToAllowCacheINWrite()
        {
            HttpContext.Current.Session[string.Format("KxWebStudentPaymentAudit-{0}", LastPaymentRequest.UniqueIdentifier.ToString())] = null;
        }

        public bool LogQuotedOrder(WorkflowWesternUnionExtender ext)
        {
            Order = null;

            if (ValidateForOrder(ext))
            {
                LastMessage.Clear();
                LogQuotedOrderRequest request = new LogQuotedOrderRequest(this.KxOrderId, (level, method, message, data) =>
                {
                    WesternUnionLogger(level, method, message, data);
                    if (data == "OUT")
                    {
                        var inputs = new NameValueCollection();
                        inputs.Add("Xml", message);
                        KxWebStudentPaymentAudit.LogOutBoundPayment(LastPaymentRequest, inputs);
                    }
                });

                var response = SendRequestToWesternUnion(request) as LogQuotedOrderResponse;
                LogOrderResponseToCache(response);
                if (response.Successful)
                {
                    Order = response.QuotedOrder;
                    return true;
                }
                else
                {
                    foreach (var message in response.Messages)
                    {
                        LastMessage.AppendLine(message);
                    }
                }
            }

            return false;
        }

        private void LogOrderResponseToCache(LogQuotedOrderResponse response)
        {
            ClearSessionVariableToAllowCacheINWrite();
            var paymentResponse = new PaymentResponse() {InternalReference = LastPaymentRequest.UniqueIdentifier.ToString(), Authorised = response.Successful, OriginalRequest = LastPaymentRequest, Amount = Quote.GrandTotal, ProviderReference = response.Successful ? response.QuotedOrder.DealReference : ""};
            KxWebStudentPaymentAudit.LogInBoundPayment(paymentResponse, LoadResponseXmlIntoDictionary(response));
            if (response.Successful)
            {
                KxWebPaymentCache cache = new KxWebPaymentCache(null, LastPaymentRequest.UniqueIdentifier);
                cache.AuthorisationCode = response.QuotedOrder.DealReference;
                cache.Changed = true;
                cache.Save();
            }
        }

        private static Dictionary<string, string> LoadResponseXmlIntoDictionary(WesternUnionResponse response)
        {
            var dict = new Dictionary<string, string>();
            dict.Add("Xml", response.RawXml);
            return dict;
        }

        #endregion

        #region Private Methods

        private ListItemCollection GetPaymentTypesForCurrencyListItems()
        {
            ListItemCollection result = new ListItemCollection();
            if (!string.IsNullOrWhiteSpace(PayFromCountry) && !string.IsNullOrWhiteSpace(PaymentCurrency))
            {
                string cacheKey = string.Format("PaymentTypes-{0}-{1}", PayFromCountry, PaymentCurrency);
                var tempResult = GetObjectFromCache<ListItemCollection>(cacheKey);
                if (tempResult != null)
                {
                    result = tempResult;
                }
                else
                {
                    PaymentTypesForCurrencyResponse response = SendRequestToWesternUnion(new PaymentTypesForCurrencyRequest(new PaymentTypesForCurrencyRequestData(GPFIClientId, PayFromCountry, PaymentCurrency), null)) as PaymentTypesForCurrencyResponse;
                    foreach (PaymentType responsePaymentType in response.PaymentTypes)
                    {
                        ListItem item = new ListItem(responsePaymentType.Description, responsePaymentType.Code);
                        result.Add(item);
                    }

                    AddLookuptoCache(cacheKey, result);
                }
            }

            return result;
        }

        private string GetLanguageList()
        {
            StringBuilder result = new StringBuilder();
            string cacheKey = "Languages";
            var tempResult = GetObjectFromCache<string>(cacheKey);
            if (tempResult != null)
            {
                result.Append(tempResult);
            }
            else
            {
                InstructionLanguagesResponse response = SendRequestToWesternUnion(new InstructionLanguagesRequest(null)) as InstructionLanguagesResponse;
                foreach (InstructionLanguage language in response.Languages)
                {
                    result.AppendLine(string.Format("{0}|{1}", language.LanguageCode, language.LanguageName));
                }

                AddLookuptoCache(cacheKey, result.ToString());
            }

            return result.ToString();
        }

        private ListItemCollection GetPaymentCurrencyListItems()
        {
            ListItemCollection result = new ListItemCollection();
            if (!string.IsNullOrWhiteSpace(PayFromCountry))
            {
                string cacheKey = string.Format("PaymentCurrencyList-{0}", PayFromCountry);
                var tempResult = GetObjectFromCache<ListItemCollection>(cacheKey);
                if (tempResult != null)
                {
                    result = tempResult;
                }
                else
                {
                    PaymentCurrenciesResponse response = SendRequestToWesternUnion(new PaymentCurrenciesRequest(new PaymentCurrenciesRequestData(GPFIClientId, PayFromCountry), null)) as PaymentCurrenciesResponse;
                    foreach (PaymentCurrency currency in response.Currencies)
                    {
                        ListItem item = new ListItem(currency.Description, currency.Code);
                        result.Add(item);
                    }

                    AddLookuptoCache(cacheKey, result);
                }
            }

            return result;
        }

        private ListItemCollection GetCountriesListItems()
        {
            ListItemCollection result = new ListItemCollection();
            string[] countries = GetCountryList().Split('\n');
            foreach (string country in countries)
            {
                string[] countryEntry = country.Split('|');
                if (countryEntry.Length == 2)
                {
                    ListItem item = new ListItem(countryEntry[1], countryEntry[0]);
                    result.Add(item);
                }
                else if (countryEntry.Length == 1)
                {
                    ListItem item = new ListItem(countryEntry[0], countryEntry[0]);
                    result.Add(item);
                }
            }

            return result;
        }

        private T GetObjectFromCache<T>(string key)
        {
            return (T)HttpContext.Current.Cache[key];
        }

        private void AddLookuptoCache(string key, object obj)
        {
            var currentCache = HttpContext.Current.Cache[key];
            if (currentCache == null)
            {
                HttpContext.Current.Cache.Add(key, obj, null, System.Web.Caching.Cache.NoAbsoluteExpiration, new TimeSpan(0, 1, 0), CacheItemPriority.Normal, null);
            }
        }

        private WesternUnionResponse SendRequestToWesternUnion(WesternUnionRequest request)
        {
            WesternUnionSender sender = new WesternUnionSender(APIUrl, WesternUnionLogger);
            return sender.SendRequestToWesternUnion(request);
        }

        private void WesternUnionLogger(AuditLevel level, string method, string message, string data)
        {
            KxLogger.LogActivity(this, string.Format("{0} - {1}", level, method), message, data);
        }

        private string GetCountryList()
        {
            string result = string.Empty;
            string overrideResource = KxDbConfiguration.GetConfiguration("WesternUnionCountryListResourceId");
            if (!string.IsNullOrWhiteSpace(overrideResource))
            {
                string countryXml = WebApp.ResourceProvider.GetResource(overrideResource);
                if (!string.IsNullOrWhiteSpace(countryXml))
                {
                    try
                    {
                        result = GetListFromLookupXml(countryXml);
                    }
                    catch (Exception ex)
                    {
                        KxLogger.LogActivity(this, "GetCountryList", string.Format("Error getting list from resource: {0}", overrideResource), ex.Message);
                        result = string.Empty;
                    }
                }
            }

            if (string.IsNullOrWhiteSpace(result))
            {
                result = PopulateDefaultCountryList();
            }

            return result;
        }

        private string GetListFromLookupXml(string xml)
        {
            XDocument doc = XDocument.Parse(xml);
            var results = from d in doc.Element("options").Elements("option")
                select new
                {
                    value = d.Attribute("value").Value,
                    text = d.Attribute("text").Value
                };

            StringBuilder result = new StringBuilder();
            foreach (var lookup in results)
            {
                result.AppendLine(string.Format("{0}|{1}", lookup.value, lookup.text));
            }

            return result.ToString();
        }

        private string PopulateDefaultCountryList()
        {
            using (var countries = new ISOCountryLookup())
            {
                return countries.KCFList;
            }
        }

        private void GetCustomValues(WorkflowWesternUnionExtender ext)
        {
            PayerCustom1 = GetCustomData(ext.Custom1);
            PayerCustom2 = GetCustomData(ext.Custom2);
            PayerCustom3 = GetCustomData(ext.Custom3);
            PayerCustom4 = GetCustomData(ext.Custom4);
            PayerCustom5 = GetCustomData(ext.Custom5);
            PayerCustom6 = GetCustomData(ext.Custom6);
            PayerCustom7 = GetCustomData(ext.Custom7);
            PayerCustom8 = GetCustomData(ext.Custom8);
            PayerCustom9 = GetCustomData(ext.Custom9);
            PayerCustom10 = GetCustomData(ext.Custom10);
        }

        private PaymentRequest GeneratePaymentRequestObject(WorkflowWesternUnionExtender ext)
        {
            PaymentRequest pr = new PaymentRequest();
            pr.UniqueIdentifier = Guid.NewGuid();
            pr.PaymentProvider = "WesternUnion";
            pr.WorkflowGuid = WebApp.WorkflowInterface.WorkflowGuid.ToString();
            pr.System = KxSystem.Student;
            if (WebUser.Student != null)
            {
                pr.StudentId = WebUser.Student.StudentId;
            }
            
            pr.CustomerId = pr.StudentId;
            pr.BookingReference = WebApp.WorkflowInterface.WorkflowGuid.ToString();
            pr.ApplicationId = WebUser.CurrentApplication.ApplicationId.ToString();
            pr.ApplicationGuid = WebUser.CurrentApplication.ApplicationGuid;
            pr.Email = WebUser.EmailAddress;
            pr.ProviderUrl = ext.APIUrl;
            pr.ProviderCallbackUrl = string.Empty;
            if (StringComparer.InvariantCultureIgnoreCase.Compare(ext.STATEMENTorDEPOSITorPAYNOW, "STATEMENT") == 0)
            {
                pr.ConsolidatedPayments = GetStatementPayments();
            }

            pr.Description = GetPayingFor(ext.PayingFor, ext.STATEMENTorDEPOSITorPAYNOW);
            pr.Amount = GetPaymentAmount(ext.PaymentAmount, ext.STATEMENTorDEPOSITorPAYNOW);
            pr.PaymentType = ext.STATEMENTorDEPOSITorPAYNOW;
            return pr;
        }

        private decimal GetPaymentAmount(string paymentAmount, string paymentType)
        {
            string amountString = WebApp.ObjectTranslator.MergeTags(paymentAmount);
            if (string.IsNullOrWhiteSpace(amountString) || paymentType == "STATEMENT")
            {
                amountString = WebApp.StatementManager.SelectedBalance.ToString("F2");
            }

            decimal result = 0m;
            decimal.TryParse(amountString, out result);
            return result;
        }

        private string GetPayingFor(string payingFor, string paymentType)
        {
            string result = WebApp.ObjectTranslator.MergeTags(payingFor);
            if (string.IsNullOrWhiteSpace(result) || paymentType == "STATEMENT")
            {
                foreach (string s in WebApp.StatementManager.PayableCharges)
                {
                    KxStudentStatement stat = WebApp.StatementManager.GetStudentStatement(s);
                    if (stat.Selected)
                    {
                        result = stat.StatementText;
                    }
                }
            }

            return result;
        }

        private string GetCustomData(string customData)
        {
            string result = customData;

            if (!string.IsNullOrWhiteSpace(customData)) {
                result = WebApp.ObjectTranslator.MergeTags(customData);
            }
            
            return result;
        }

        private List<KxConsolidatedPayment> GetStatementPayments()
        {
            List<KxConsolidatedPayment> payments = new List<KxConsolidatedPayment>();
            foreach (string s in WebApp.StatementManager.PayableCharges)
            {
                KxStudentStatement stat = WebApp.StatementManager.GetStudentStatement(s);
                if (stat.Selected)
                {
                    KxConsolidatedPayment pay = new KxConsolidatedPayment(stat.StudentChargeId, stat.StatementText, stat.OutStandingBalance);
                    payments.Add(pay);
                }
            }

            return payments;
        }

        private IQuoteOrderRequestData GetQuoteOrderRequestData(PaymentRequest paymentRequest)
        {
            PayerStudentId = paymentRequest.StudentId;
            
            QuoteOrderRequestData result = new QuoteOrderRequestData();
            result.Amount = paymentRequest.Amount;
            result.ClientId = GPFIClientId;
            result.Language = InstructionLanguage;
            result.PaymentCurrencyCode = PaymentCurrency;
            result.PaymentTypeCode = PaymentType;
            result.SettlementCurrencyCode = SettlementCurrency;
            result.Remitter.Name = RemitterName;
            result.Remitter.Address1 = RemitterAddress1;
            result.Remitter.Address2 = RemitterAddress2;
            result.Remitter.City = RemitterCity;
            result.Remitter.State = RemitterState;
            result.Remitter.Zip = RemitterZip;
            result.Remitter.CountryCode = this.PayFromCountry;
            result.Remitter.Email = RemitterEmail;
            result.Payer.StudentId = PayerStudentId;
            if (!string.IsNullOrWhiteSpace(paymentRequest.StudentId))
            {
                result.Payer.PayerFirstName = WebUser.ForeName;
                result.Payer.PayerSurName = WebUser.SurName;
                result.Payer.PayerEmail = WebUser.EmailAddress;
            }
            result.Payer.PayerCustom1 = PayerCustom1;
            result.Payer.PayerCustom2 = PayerCustom2;
            result.Payer.PayerCustom3 = PayerCustom3;
            result.Payer.PayerCustom4 = PayerCustom4;
            result.Payer.PayerCustom5 = PayerCustom5;
            result.Payer.PayerCustom6 = PayerCustom6;
            result.Payer.PayerCustom7 = PayerCustom7;
            result.Payer.PayerCustom8 = PayerCustom8;
            result.Payer.PayerCustom9 = PayerCustom9;
            result.Payer.PayerCustom10 = PayerCustom10;
            result.RemitterBank.Name = BankName;
            result.RemitterBank.Address1 = BankAddress1;
            result.RemitterBank.Address2 = BankAddress2;
            result.RemitterBank.City = BankCity;
            result.RemitterBank.State = BankState;
            result.RemitterBank.Zip = BankZip;
            result.RemitterBank.CountryCode = this.PayFromCountry;

            return result;
        }

        private bool ValidateForOrder(WorkflowWesternUnionExtender ext)
        {
            LastMessage.Clear();
            if (Quote == null)
            {
                LastMessage.AppendLine(KxLocalisationManager.GetSubstitution("No valid quote available to order"));
            }

            if (string.IsNullOrWhiteSpace(Quote.OrderId))
            {
                LastMessage.AppendLine(KxLocalisationManager.GetSubstitution("No valid quote available to order"));
            }

            return string.IsNullOrWhiteSpace(LastMessage.ToString());
        }

        private bool ValidateForQuote(WorkflowWesternUnionExtender ext)
        {
            LastMessage.Clear();
            bool result = true;
            result = result && SwiftStringCheck(RemitterName, "Remitter Name");
            result = result && SwiftStringCheck(RemitterAddress1, "Remitter Address 1");
            result = result && (string.IsNullOrWhiteSpace(RemitterAddress2) || SwiftStringCheck(RemitterAddress2, "Remitter Address 2"));
            result = result && SwiftStringCheck(RemitterCity, "Remitter City");
            result = result && SwiftStringCheck(RemitterState, "Remitter State");
            result = result && (string.IsNullOrWhiteSpace(RemitterZip) || SwiftStringCheck(RemitterZip, "Remitter Zip/Postcode"));
            result = result && (HasBankInput() && SwiftStringCheck(BankName, "Bank Name") || !HasBankInput());
            result = result && (HasBankInput() && SwiftStringCheck(BankAddress1, "Bank Address 1") || !HasBankInput());
            result = result && (string.IsNullOrWhiteSpace(BankAddress2) || SwiftStringCheck(BankAddress2, "Bank Address 2"));
            result = result && (HasBankInput() && SwiftStringCheck(BankCity, "Bank City") || !HasBankInput());
            result = result && (HasBankInput() && SwiftStringCheck(BankState, "Bank State") || !HasBankInput());
            result = result && (string.IsNullOrWhiteSpace(BankZip) || SwiftStringCheck(BankZip, "Bank Zip/Postcode"));
            result = result && RequiredField(RemitterName, "Remitter Name");
            result = result && RequiredField(RemitterAddress1, "Remitter Address 1");
            result = result && RequiredField(RemitterCity, "Remitter City");
            result = result && RequiredField(RemitterState, "Remitter State");
            result = result && (HasBankInput() && RequiredField(BankName, "Bank Name") || !HasBankInput());
            result = result && (HasBankInput() && RequiredField(BankAddress1, "Bank Address 1") || !HasBankInput());
            result = result && (HasBankInput() && RequiredField(BankCity, "Bank City") || !HasBankInput());
            result = result && (HasBankInput() && RequiredField(BankState, "Bank State") || !HasBankInput());
            result = result && RequiredField(PayFromCountry, "Pay From Country");
            result = result && RequiredField(InstructionLanguage, "Instruction Language");
            result = result && RequiredField(PaymentType, "Payment Type");
            result = result && RequiredField(PaymentCurrency, "Payment Currency");
            result = result && RequiredField(SettlementCurrency, "Settlement Currency");

            return result;
        }

        private bool HasBankInput()
        {
            return !string.IsNullOrWhiteSpace(BankName) || !string.IsNullOrWhiteSpace(BankAddress1) || !string.IsNullOrWhiteSpace(BankCity) || !string.IsNullOrWhiteSpace(BankState) || !string.IsNullOrWhiteSpace(BankZip);
        }

        private bool RequiredField(string fieldValue, string fieldName)
        {
            LastMessage.AppendLine(KxLocalisationManager.GetSubstitution(string.Format("{0} {1}", KxLocalisationManager.GetSubstitution(fieldName), KxLocalisationManager.GetSubstitution("is a required field"))));
            return !string.IsNullOrWhiteSpace(fieldValue);
        }

        private bool SwiftStringCheck(string fieldValue, string fieldName)
        {
            Regex check = new Regex(NonSWIFTCharacterReplacement.SWIFTCharacters);

            if (!check.IsMatch(fieldValue))
            {
                LastMessage.AppendLine(KxLocalisationManager.GetSubstitution(string.Format("{0} {1}", KxLocalisationManager.GetSubstitution(fieldName), KxLocalisationManager.GetSubstitution("can only contain characters A-Z a-z 0-9 ,.:'?+()-"))));
            }

            if (fieldValue.Length > NonSWIFTCharacterReplacement.MaxLength)
            {
                LastMessage.AppendLine(KxLocalisationManager.GetSubstitution(string.Format("{0} {1}", KxLocalisationManager.GetSubstitution(fieldName), KxLocalisationManager.GetSubstitution(string.Format("can only be {0} characters long", NonSWIFTCharacterReplacement.MaxLength)))));
            }

            return fieldValue.Length <= NonSWIFTCharacterReplacement.MaxLength &&
                   check.IsMatch(fieldValue);
        }

        private bool HasCustomValues()
        {
            return (!string.IsNullOrWhiteSpace(PayerCustom1) || !string.IsNullOrWhiteSpace(PayerCustom2) || !string.IsNullOrWhiteSpace(PayerCustom3) ||
                !string.IsNullOrWhiteSpace(PayerCustom4) || !string.IsNullOrWhiteSpace(PayerCustom5) || !string.IsNullOrWhiteSpace(PayerCustom6) ||
                !string.IsNullOrWhiteSpace(PayerCustom7) || !string.IsNullOrWhiteSpace(PayerCustom8) || !string.IsNullOrWhiteSpace(PayerCustom9) || !string.IsNullOrWhiteSpace(PayerCustom10));
        }

        #endregion
    }
}