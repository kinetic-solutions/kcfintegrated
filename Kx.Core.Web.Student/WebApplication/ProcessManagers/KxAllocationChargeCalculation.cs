using System;
using System.Collections.Generic;
using System.Text;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Configuration;
using Kx.Core.Exceptions;
using Kx.Core.Utility;
using Kx.Objects.Student;
using Kx.Objects.Student.Web;

namespace Kx.Core.Web.Student.WebApplication.ProcessManagers
{
    public class KxAllocationChargeCalculation : KxBaseClass
    {
        protected int pricingModelId;
        protected int allocationModelId;

        protected KxPricingModel pricingModel;
        protected KxAllocationModel allocationModel;
        protected KxWebApplicationStudent webApp;

        public int PricingModelId
        {
            get
            {
                return pricingModelId;
            }
            set
            {
                pricingModelId = value;
            }
        }
        public int AllocationModelId
        {
            get
            {
                return allocationModelId;
            }
            set
            {
                
                allocationModelId = value;
            }
        }

        public KxPricingModel PricingModel
        {
            get
            {
                if (pricingModel == null)
                {
                    pricingModel = new KxPricingModel(null, pricingModelId);
                }
                return pricingModel;
            }
        }
        public KxAllocationModel AllocationModel
        {
            get
            {
                if (allocationModel == null)
                {
                    allocationModel = KxStudentObjectManager.AllocationModels.GetModelById(allocationModelId,webApp.WorkflowInterface.WebUser.CurrentAcademicYear);
                }
                return allocationModel;
            }
        }

        public KxWebApplicationStudent WebApp
        {
            get
            {
                return webApp;
            }
            set
            {
                if (webApp == value)
                    return;
                webApp = value;
            }
        }
        public KxWebBedroomTypeCharge BedroomCharge
        {
            get
            {
                return WebApp.CurrentBedroomTypeCharge;
            }
        }

        public KxAllocationChargeCalculation(KxBaseClass parent, KxWebApplicationStudent webApp)
            : base(parent)
        {
            this.pricingModelId = webApp.AllocationManager.CurrentAllocation.StudentAllocation.PricingModelId;
            this.allocationModelId = webApp.AllocationManager.CurrentAllocation.StudentAllocation.AllocationModelId;
            this.webApp = webApp;
        }

        public bool ProcessInstalmentCharges()
        {
            bool result = false;
            try
            {

                KxPaymentScheduleType paytype = WebApp.WorkflowInterface.WebUser.CurrentApplication.PaymentScheduleType;

                foreach (KxStudentChargeType type in GetChargeTypes())
                {
                    decimal totalcharges = GetChargesTotal(type.StudentChargeTypeId);
                    decimal appliedCharges = 0.0M;

                    foreach (KxPaymentScheduleTypeModel model in paytype.Models.Items)
                    {
                        if (model.FixedDate < new DateTime(1980, 1, 1) || model.FixedDate >= WebApp.AllocationManager.CurrentAllocation.StudentAllocation.AllocationDates.AllocationStartDate)
                        {
                            if (model.PaymentIntervalType.Trim() == "Percentage On Date")
                            {
                                model.AmountToCharge = 0;
                                model.AmountToCharge = Math.Round((totalcharges / 100.00m) * ((decimal)model.Percentage), 2);
                                model.NbrOfMonths = 0;

                                appliedCharges += model.AmountToCharge;
                            }
                            else if (model.PaymentIntervalType.Trim() == "Monthly")
                            {
                                model.NbrOfMonths = 0;
                                KxStudentAllocationDate amdate = WebApp.AllocationManager.CurrentAllocation.StudentAllocation.AllocationDates.GetFirstAllocation();

                                DateTime mydate = (DateTime)amdate.ArrivalDate;

                                bool monthok = true;

                                while (monthok)
                                {
                                    model.NbrOfMonths++;
                                    mydate = mydate.AddMonths(1);

                                    if (WebApp.AllocationManager.CurrentAllocation.StudentAllocation.AllocationDates.GetAllocationDate(mydate) == null)
                                    {
                                        monthok = false;
                                    }
                                }
                            }
                        }
                    }

                    if (totalcharges > appliedCharges)
                    {
                        paytype.Models[0].AmountToCharge += Math.Round((totalcharges - appliedCharges), 2);
                    }
                    else
                    if (totalcharges < appliedCharges)
                    {
                        paytype.Models[0].AmountToCharge += Math.Round((totalcharges - appliedCharges), 2);
                    }

                    int count = 1;

                    foreach (KxPaymentScheduleTypeModel model in paytype.Models.Items)
                    {
                        if (model.FixedDate < new DateTime(1980, 1, 1) || model.FixedDate >= WebApp.AllocationManager.CurrentAllocation.StudentAllocation.AllocationDates.AllocationStartDate)
                        {
                            KxStudentAllocationDate allocDate = null;

                            if (model.FixedDate.Value < KxDateTime.KxApplicableDate)
                            {
                                allocDate = WebApp.AllocationManager.CurrentAllocation.StudentAllocation.AllocationDates.GetFirstAllocation();
                            }
                            else
                            {
                                allocDate = WebApp.AllocationManager.CurrentAllocation.StudentAllocation.AllocationDates.GetAllocationDate(model.FixedDate.Value);
                            }


                            DateTime? fromDate;
                            DateTime? toDate = null;
                            int allocDateId = 0;

                            if (allocDate != null)
                            {
                                allocDateId = allocDate.AllocationDateId;
                            }
                            else
                            {
                                allocDateId = 0;
                            }

                            if (model.FixedDate < KxDateTime.KxApplicableDate)
                            {
                                fromDate = WebApp.AllocationManager.CurrentAllocation.StudentAllocation.AllocationDates.AllocationStartDate;
                            }
                            else
                            {
                                fromDate = model.FixedDate;
                            }

                            if (paytype.Models.Items.IndexOf(model) < paytype.Models.Items.Count - 1)
                            {
                                if (pricingModel.ModelType == "Daily")
                                {
                                    for (int i = paytype.Models.Items.IndexOf(model) + 1; i < paytype.Models.Items.Count; i++)
                                    {
                                        if (paytype.Models.Items[(i)].FixedDate >= fromDate)
                                        {
                                            toDate = paytype.Models.Items[(i)].FixedDate.Value.AddDays(-1);
                                            break;
                                        }
                                    }
                                    if (toDate == null)
                                    {
                                        toDate = paytype.Models.Items[(paytype.Models.Items.IndexOf(model) + 1)].FixedDate.Value.AddDays(-1);
                                    }
                                }
                                else
                                {
                                    for (int i = paytype.Models.Items.IndexOf(model) + 1; i < paytype.Models.Items.Count; i++)
                                    {
                                        if (paytype.Models.Items[(i)].FixedDate >= fromDate)
                                        {
                                            toDate = paytype.Models.Items[(i)].FixedDate.Value;
                                            break;
                                        }
                                    }
                                    if (toDate == null)
                                    {
                                        toDate = paytype.Models.Items[(paytype.Models.Items.IndexOf(model) + 1)].FixedDate.Value;
                                    }
                                }
                            }
                            else if (allocDate != null)
                            {
                                toDate = allocDate.DepartureDate;
                            }
                            else
                            {
                                toDate = fromDate;
                            }

                            if (model.NbrOfMonths == 0)
                            {
                                AddStudentCharges(allocDateId, type, fromDate, toDate, model.AmountToCharge, count, fromDate);
                                count++;
                            }
                            else
                            {
                                decimal monthlypayment = Math.Round(totalcharges / model.NbrOfMonths, 2);
                                decimal firstpayment = Math.Round(totalcharges - (monthlypayment * (model.NbrOfMonths - 1)), 2);

                                DateTime paymentdate = (DateTime)fromDate;
                                if (paymentdate.Day != model.FixedDay)
                                {
                                    paymentdate = paymentdate.AddDays(model.FixedDay - paymentdate.Day);
                                }

                                for (int monthno = 1; monthno <= model.NbrOfMonths; monthno++)
                                {
                                    if (monthno == 1)
                                    {
                                        AddStudentCharges(allocDateId, type, paymentdate, paymentdate.AddMonths(1), firstpayment, count, fromDate);
                                    }
                                    else
                                    {
                                        AddStudentCharges(allocDateId, type, paymentdate, paymentdate.AddMonths(1), monthlypayment, count, fromDate);
                                    }
                                    paymentdate = paymentdate.AddMonths(1);
                                    count++;
                                }
                            }
                        }
                    }
                }
                Changed = false;
                result = Save();

            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "Process Instalments", ex.Message, "");
            }
            return result;
        }

        public bool ProcessCharges(bool checkInstalments, bool usePaymentSchedule, int SiteId)
        {
            bool result = false;

            if (checkInstalments && WebApp.WorkflowInterface.WebUser.CurrentApplication.PaymentScheduleTypeId > 0)
            {
               return  ProcessInstalmentCharges();
            }
            else
            if (usePaymentSchedule && WebApp.WorkflowInterface.WebUser.CurrentApplication.PaymentScheduleTypeId == 0)
            {
                bool chargesresult = false;
                
                KxData data = new KxData("", "SitePaymentScheduleType", TargetType.StoredProc);

                data.AddParameter("SiteID", SiteId, KxValueType.String);

                object dbresult = data.ExecuteScalar();

                if (dbresult != null)
                {
                    try
                    {
                        WebApp.WorkflowInterface.WebUser.CurrentApplication.PaymentScheduleTypeId = Convert.ToInt32(dbresult);
                        chargesresult = ProcessInstalmentCharges();
                    }
                    catch
                    { }
                }
                WebApp.WorkflowInterface.WebUser.CurrentApplication.PaymentScheduleTypeId = 0;

                return chargesresult;
            }
            else
            {
                try
                {
                //process charges for the allocation
                    
                    if (AllocationModel.StudentCharge1 != null)
                    {
                        AddStudentCharges(-WebApp.AllocationManager.CurrentAllocation.AllocationId,
                                          AllocationModel.StudentCharge1,
                                          WebApp.AllocationManager.CurrentAllocation.Schedule.ActualArrivalDate,
                                          WebApp.AllocationManager.CurrentAllocation.Schedule.ActualDepartureDate,
                                          AllocationModel.StudentCharge1.DefaultCharge, 0, WebApp.AllocationManager.CurrentAllocation.Schedule.ActualArrivalDate);
                    }

                    if (AllocationModel.StudentCharge2 != null)
                    {
                        AddStudentCharges(-WebApp.AllocationManager.CurrentAllocation.AllocationId,
                                          AllocationModel.StudentCharge2,
                                          WebApp.AllocationManager.CurrentAllocation.Schedule.ActualArrivalDate,
                                          WebApp.AllocationManager.CurrentAllocation.Schedule.ActualDepartureDate,
                                          AllocationModel.StudentCharge2.DefaultCharge, 0, WebApp.AllocationManager.CurrentAllocation.Schedule.ActualArrivalDate);
                    }

                    if (AllocationModel.StudentCharge3 != null)
                    {
                        AddStudentCharges(-WebApp.AllocationManager.CurrentAllocation.AllocationId,
                                          AllocationModel.StudentCharge3,
                                          WebApp.AllocationManager.CurrentAllocation.Schedule.ActualArrivalDate,
                                          WebApp.AllocationManager.CurrentAllocation.Schedule.ActualDepartureDate,
                                          AllocationModel.StudentCharge3.DefaultCharge, 0, WebApp.AllocationManager.CurrentAllocation.Schedule.ActualArrivalDate);
                    }

                    if (AllocationModel.StudentCharge4 != null)
                    {
                        AddStudentCharges(-WebApp.AllocationManager.CurrentAllocation.AllocationId,
                                          AllocationModel.StudentCharge4,
                                          WebApp.AllocationManager.CurrentAllocation.Schedule.ActualArrivalDate,
                                          WebApp.AllocationManager.CurrentAllocation.Schedule.ActualDepartureDate,
                                          AllocationModel.StudentCharge4.DefaultCharge, 0, WebApp.AllocationManager.CurrentAllocation.Schedule.ActualArrivalDate);
                    }


                    TimeSpan span = WebApp.AllocationManager.CurrentAllocation.Schedule.ActualDepartureDate.Subtract(WebApp.AllocationManager.CurrentAllocation.Schedule.ActualArrivalDate);

                    decimal totalnoweeks = span.Days / 7;

                    if (KxDbConfiguration.GetConfiguration("WeeklyBasedCharges") != "")
                    {
                        KxStudentAllocationDate keepallocdate = null;
                        KxPricingModelCharge priceCharge = null;

                        foreach (KxStudentAllocationDate allocdate in WebApp.AllocationManager.CurrentAllocation.StudentAllocation.AllocationDates.Items)
                        {
                            KxAllocationModelDate matchingDate = MatchingModelDate(allocdate);

                            if (matchingDate != null && keepallocdate == null)
                            {
                                keepallocdate = allocdate;

                                priceCharge = PricingModel.GetChargeByType(matchingDate.StudentChargeId1, WebApp.CurrentBedroomTypeCharge.AcademicYearId);
                            }

                        }

                        if (keepallocdate != null && priceCharge != null)
                        {

                            decimal totalamount = totalnoweeks * priceCharge.WeeklyAmount;
                            KxAllocationModelDate chargematchingDate = MatchingModelDate(keepallocdate);

                            try
                            {

                                if (KxDbConfiguration.GetConfiguration("SinglePaymentDiscount") != "")
                                {
                                    
                                    decimal discpercent = KxDbConfiguration.GetConfigurationDecimal("SinglePaymentDiscount",0.00M);

                                    totalamount = totalamount * (1 - (discpercent / 100));
                               
                                }

                            }
                            catch { }

                            AddStudentCharges(keepallocdate.AllocationDateId, chargematchingDate.StudentCharge1, WebApp.AllocationManager.CurrentAllocation.Schedule.ActualArrivalDate, WebApp.AllocationManager.CurrentAllocation.Schedule.ActualDepartureDate, totalamount, 0, WebApp.AllocationManager.CurrentAllocation.Schedule.ActualArrivalDate);
                        }
                    }
                    else
                    {
                        foreach (KxStudentAllocationDate allocdate in WebApp.AllocationManager.CurrentAllocation.StudentAllocation.AllocationDates.Items)
                        {
                            KxAllocationModelDate matchingDate = MatchingModelDate(allocdate);

                            if (matchingDate != null)
                            {
                                if (matchingDate.StudentCharge1 != null)
                                {
                                    ProcessAllocationDate(matchingDate.StudentCharge1, allocdate, matchingDate, true);
                                }

                                if (matchingDate.StudentCharge2 != null)
                                {
                                    ProcessAllocationDate(matchingDate.StudentCharge2, allocdate, matchingDate, true);
                                }

                                if (matchingDate.StudentCharge3 != null)
                                {
                                    ProcessAllocationDate(matchingDate.StudentCharge3, allocdate, matchingDate, true);
                                }

                                if (matchingDate.StudentCharge4 != null)
                                {
                                    ProcessAllocationDate(matchingDate.StudentCharge4, allocdate, matchingDate, true);
                                }
                            }
                        }
                    }

            
                Changed = false;
                }
                catch (Exception ex)
                {
                     KxLogger.LogActivity(this, "Process charges", ex.Message, "");
                }
                result = Save();

                return result;
            }
        }

        public  List<KxStudentChargeType> GetChargeTypes()
        {
            List<KxStudentChargeType> charges = new List<KxStudentChargeType>();

            if (!ChargeListContainsId(charges,AllocationModel.StudentChargeId1))
                charges.Add(AllocationModel.StudentCharge1);

            if (!ChargeListContainsId(charges,AllocationModel.StudentChargeId2))
                charges.Add(AllocationModel.StudentCharge2);

            if (!ChargeListContainsId(charges,AllocationModel.StudentChargeId3))
                charges.Add(AllocationModel.StudentCharge3);

            if (!ChargeListContainsId(charges,AllocationModel.StudentChargeId4))
                charges.Add(AllocationModel.StudentCharge4);
         
            foreach (KxStudentAllocationDate allocdate in WebApp.AllocationManager.CurrentAllocation.StudentAllocation.AllocationDates.Items)
            {
                KxAllocationModelDate matchingDate = MatchingModelDate(allocdate);

                if (matchingDate != null)
                {
                    if (!ChargeListContainsId(charges,matchingDate.StudentChargeId1))
                        charges.Add(matchingDate.StudentCharge1);

                    if (!ChargeListContainsId(charges,matchingDate.StudentChargeId2))
                        charges.Add(matchingDate.StudentCharge2);

                    if (!ChargeListContainsId(charges,matchingDate.StudentChargeId3))
                        charges.Add(matchingDate.StudentCharge3);

                    if (!ChargeListContainsId(charges,matchingDate.StudentChargeId4))
                        charges.Add(matchingDate.StudentCharge4);
                }
            }

            return charges;
        }

        private bool ChargeListContainsId( List<KxStudentChargeType> charges, int Id)
        {
            bool result = false;

            if (Id > 0)
            {
                foreach (KxStudentChargeType type in charges)
                {
                    if (type.StudentChargeTypeId == Id)
                    {
                        result = true;
                        break;
                    }
                }
            }
            else
            {
                result = true;
            }
            return result;
        }


        public Decimal GetChargesTotal(int chargeType)
        {
            decimal Result = 0.0M;

            if (AllocationModel.StudentChargeId1 == chargeType)
                Result += AllocationModel.StudentCharge1.DefaultCharge;

            if (AllocationModel.StudentChargeId2 == chargeType)
                Result += AllocationModel.StudentCharge2.DefaultCharge;

            if (AllocationModel.StudentChargeId3 == chargeType)
                Result += AllocationModel.StudentCharge3.DefaultCharge;

            if (AllocationModel.StudentChargeId4 == chargeType)
                Result += AllocationModel.StudentCharge4.DefaultCharge;

            foreach (KxStudentAllocationDate allocdate in WebApp.AllocationManager.CurrentAllocation.StudentAllocation.AllocationDates.Items)
            {
                KxAllocationModelDate matchingDate = MatchingModelDate(allocdate);

                if (matchingDate != null)
                {
                    if (matchingDate.StudentChargeId1 == chargeType)
                        Result += ProcessAllocationDate(matchingDate.StudentCharge1, allocdate, matchingDate, false);

                    if (matchingDate.StudentChargeId2 == chargeType)
                        Result += ProcessAllocationDate(matchingDate.StudentCharge2, allocdate, matchingDate, false);

                    if (matchingDate.StudentChargeId3 == chargeType)
                        Result += ProcessAllocationDate(matchingDate.StudentCharge3, allocdate, matchingDate, false);

                    if (matchingDate.StudentChargeId4 == chargeType)
                        Result += ProcessAllocationDate(matchingDate.StudentCharge4, allocdate, matchingDate, false);
                }
            }

            return Result;
        }


        public override KxData ConfigureData(Kx.Core.Data.ActionType actiontype)
        {
            Changed = false;
            return new KxData();
        }

        public virtual decimal ProcessAllocationDate(KxStudentChargeType chargeType, KxStudentAllocationDate allocDate,KxAllocationModelDate modelDate,bool Save)
        {
            KxPricingModelCharge priceCharge = PricingModel.GetChargeByType(chargeType.StudentChargeTypeId, WebApp.CurrentBedroomTypeCharge.AcademicYearId);

            decimal result = 0.0M;

            if (priceCharge != null)
            {
                result = (priceCharge.WeeklyAmount * modelDate.FullWeeks) + (priceCharge.DailyAmount * modelDate.RemainderDays);
            }
            else
            {
                result = modelDate.StudentCharge1.DefaultCharge;
            }

            try
            {

               
                    if ((KxDbConfiguration.GetConfiguration("SinglePaymentDiscount") != "") && Save)
                    {
                        decimal discpercent = KxDbConfiguration.GetConfigurationDecimal("SinglePaymentDiscount",0.00M);

                        result = result * (1 - (discpercent / 100));
                    }
        

            }
            catch { }

            if (Save)
            {
                AddStudentCharges(allocDate.AllocationDateId, chargeType, allocDate.ArrivalDate, allocDate.DepartureDate, result, 0, modelDate.DueDate ?? modelDate.StartDate);
            }

            return result;
        }

        private KxAllocationModelDate MatchingModelDate(KxStudentAllocationDate allocdate)
        {
            KxAllocationModelDate result = null;

            foreach (KxAllocationModelDate modelDate in AllocationModel.Dates.Items)
            {
                if ((modelDate.StartDate == allocdate.ArrivalDate) && (modelDate.EndDate == allocdate.DepartureDate))
                {
                    result = modelDate;
                    break;
                }
            }
            return result;
        }

        protected virtual KxStudentCharge AddStudentCharges(int allocDate, KxStudentChargeType StudentChargeType, DateTime? fromDate, DateTime? toDate, Decimal Amount,int instalment, DateTime? dueDate)
        {
            KxStudentCharge result = new KxStudentCharge(this);

            try
            {

                result.KxStudentId = WebApp.WorkflowInterface.WebUser.KxStudentId;
                result.ApplicationId = WebApp.WorkflowInterface.WebUser.CurrentApplication.ApplicationId;
                result.AllocationDateId = allocDate;
                result.StudentChargeTypeId = StudentChargeType.StudentChargeTypeId;
                result.SiteId = WebApp.CurrentBedroomTypeCharge.SiteId;
                result.AreaId = WebApp.CurrentBedroomTypeCharge.AreaId;
                result.CreationDate = DateTime.Now;
                result.CreationUserId = 1;
                result.DateFrom = fromDate;
                result.DateTo = toDate;
                result.StatementText = StudentChargeType.StatementText;
                result.Amount = Amount;
                result.VatId = StudentChargeType.VatId;
                result.InstallmentNo = instalment;
                result.DueDate = dueDate;
              
                if (result.Vat != null)
                {
                    result.TotalAmount = Amount + (Amount * ((Decimal)((result.Vat.Rate) / 100)));
                }
                else
                {
                    result.TotalAmount = Amount;
                }

                result.Amount = Decimal.Round(result.Amount, 2);
                result.TotalAmount = Decimal.Round(result.TotalAmount,2);

                result.PrintOnStatement = false;
                result.ResidentialRoomId = WebApp.CurrentBedroomTypeCharge.ResidentialRoomId;
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "AddStudentCharges", ex.Message, "");
            }
            return result;
        }
    }
}
