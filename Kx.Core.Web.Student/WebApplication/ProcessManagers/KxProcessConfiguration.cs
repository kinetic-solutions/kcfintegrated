﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Reflection;
using Kx.Core.Configuration;
using Kx.Core.Utility;
using Kx.Core.Web.PageRenderer.Utility;
using Kx.Core.Web.Student.WebApplication.BaseClass;
using System.Reflection;

namespace Kx.Core.Web.Student.WebApplication.ProcessManagers
{
    public class KxProcessConfiguration : WebStudentBaseClass
    {
        private int numberOfPreferences = 3;

        private bool useDOBToValidate = false;
        private bool useForenameToValidate = false;
        private bool useSurnameToValidate = false;

        private string applicationName = "Kinetic Apps and Contracts";
        private string registrationMessageBody = "Dear Student\r\n\r\nThank you for registering with {KX_PCPROCCFG_PNApplicationName}.\r\n\r\nFor your reference,your username is {KX_PCUSER_PNUserName} \r\n\r\nTo proceed with your application for accommodation, please confirm your details by clicking on the link below, or by pasting it into your browser.  \r\n\r\n{KX_PCREGMGR_PNShortConfirmationUrl}\r\n\r\nIf you encounter issues registering using the link above, follow the link below and paste the confirmation code into the text box on that page...\r\n\r\nUrl:\r\n\r\n{KX_PCCFG_PNEmailConfirmationUrl}\r\n\r\nConfirmation Code:\r\n\r\n{KX_PCREGMGR_PNUniqueConfirmationCode}\r\n\r\n\r\nYours Sincerely";

        private string confirmationMessageHeader = "";
        private string confirmationMessageFooter = "";

        private string acceptanceMessageSubject = "";
        private string acceptanceMessageBody = ""; 
        private string rejectionMessageSubject = "";
        private string rejectionMessageBody = "";
        private string rejectionMessageBCC = "";


        private int contractSections = 1;
        private bool loginToConfirmContract = true;

        private bool sendAcceptanceEmail = true;
        private bool sendRejectionEmail = true;


        private bool useCustomField1 = false;
        private bool useCustomField2 = false;

        private bool useCustomValue1 = false;
        private bool useCustomValue2 = false;
        private bool useCustomValue3 = false;
        private bool useCustomValue4 = false;

        private int udfMapping1 = 0;
        private int udfMapping2 = 0;
        private int udfMapping3 = 0;
        private int udfMapping4 = 0;
        private int udfMapping5 = 0;
        private int udfMapping6 = 0;

        private bool askSmokingQuestion = true;
        private bool askDisabledQuestion = true;
        private bool askSingleSexAreaQuestion = true;
        private bool askAdditionalInformationQuestion = true;
        private bool askForPaymentDuringApps = false;
        private bool askForPaymentDuringContracts = false;

        private bool requestCurrentAddressDetails = true;
        private bool requestHomeAddressDetails = true;
        private bool requestEmergencyAddressDetails = true;
        private bool requestApplicationType = true;

        private bool enableInductions = false;

        private string senderEmail = "";
        private string contactStrapLine = "";

        private bool showApplicationOptionsPage = true;



        public bool IsLocal
        {
            get
            {
                return KxPageUtility.IsLocal;
            }
        }
        
        public int NumberOfPreferences
        {
            get
            {
                return numberOfPreferences;
            }
            set
            {
                numberOfPreferences = value;
            }
        }

        public bool UseDOBToValidate
        {
            get
            {
                return useDOBToValidate;
            }
            set
            {
                useDOBToValidate = value;
            }
        }

        public bool UseForenameToValidate
        {
            get
            {
                return useForenameToValidate;
            }
            set
            {
                useForenameToValidate = value;
            }
        }

        public bool EnableInductions
        {
            get
            {
                return enableInductions;
            }
            set
            {
                enableInductions = value;
            }
        }

        public bool UseSurnameToValidate
        {
            get
            {
                return useSurnameToValidate;
            }
            set
            {
                useSurnameToValidate = value;
            }
        }

        public string ApplicationName
        {
            get
            {
                return applicationName;
            }
            set
            {
                applicationName = value;
            }
        }

        public string RegistrationEmailBodyMerged
        {
            get
            {
                return WebApp.ObjectTranslator.MergeTags(RegistrationMessageBody);
            }
        }

        public string ConfirmationEmailHeader
        {
            get
            {
                return WebApp.ObjectTranslator.MergeTags(confirmationMessageHeader);
            }
        }
        public string ConfirmationEmailFooter
        {
            get
            {
                return WebApp.ObjectTranslator.MergeTags(confirmationMessageFooter);
            }
        }
        public string RejectionMessageBCC
        {
            get
            {
                return rejectionMessageBCC;
            }
            set
            {
                rejectionMessageBCC = value;
            }
        }

        public string MergedRejectionMessageBCC
        {
            get
            {
                return WebApp.ObjectTranslator.MergeTags(rejectionMessageBCC).Trim();
            }
        }

        public string RegistrationMessageBody
        {
            get
            {
                return registrationMessageBody;
            }
            set
            {
                registrationMessageBody = value;
            }
        }
        public string ConfirmationMessageHeader
        {
            get
            {
                return confirmationMessageHeader;
            }
            set
            {
                confirmationMessageHeader = value;
            }
        }
        public string ConfirmationMessageFooter
        {
            get
            {
                return confirmationMessageFooter;
            }
            set
            {
                confirmationMessageFooter = value;
            }
        }

        public bool UseCustomField1
        {
            get
            {
                return useCustomField1;
            }
            set
            {
                useCustomField1 = value;
            }
        }
        public bool UseCustomField2
        {
            get
            {
                return useCustomField2;
            }
            set
            {
                useCustomField2 = value;
            }
        }

        public bool UseCustomValue1
        {
            get
            {
                return useCustomValue1;
            }
            set
            {
                useCustomValue1 = value;
            }
        }
        public bool UseCustomValue2
        {
            get
            {
                return useCustomValue2;
            }
            set
            {
                useCustomValue2 = value;
            }
        }
        public bool UseCustomValue3
        {
            get
            {
                return useCustomValue3;
            }
            set
            {
                useCustomValue3 = value;
            }
        }
        public bool UseCustomValue4
        {
            get
            {
                return useCustomValue4;
            }
            set
            {
                useCustomValue4 = value;
            }
        }

        public bool AskSmokingQuestion
        {
            get
            {
                return askSmokingQuestion;
            }
            set
            {
                askSmokingQuestion = value;
            }
        }
        public bool AskDisabledQuestion
        {
            get
            {
                return askDisabledQuestion;
            }
            set
            {
                askDisabledQuestion = value;
            }
        }
        public bool AskSingleSexAreaQuestion
        {
            get
            {
                return askSingleSexAreaQuestion;
            }
            set
            {
                askSingleSexAreaQuestion = value;
            }
        }
        public bool AskAdditionalInformationQuestion
        {
            get
            {
                return askAdditionalInformationQuestion;
            }
            set
            {
                askAdditionalInformationQuestion = value;
            }
        }

        public bool RequestApplicationType
        {
            get
            {
                return requestApplicationType;
            }
            set
            {
                requestApplicationType = value;
            }
        }

        public bool RequestCurrentAddressDetails
        {
            get
            {
                return requestCurrentAddressDetails;
            }
            set
            {
                requestCurrentAddressDetails = value;
            }
        }
        public bool RequestHomeAddressDetails
        {
            get
            {
                return requestHomeAddressDetails;
            }
            set
            {
                requestHomeAddressDetails = value;
            }
        }
        public bool RequestEmergencyAddressDetails
        {
            get
            {
                return requestEmergencyAddressDetails;
            }
            set
            {
                requestEmergencyAddressDetails = value;
            }
        }

        public string SenderEmail
        {
            get
            {
                return senderEmail;
            }
            set
            {
                senderEmail = value;
            }
        }
        public string ContactStrapLine
        {
            get
            {
                return contactStrapLine;
            }
            set
            {
                contactStrapLine = value;
            }
        }

        public bool ShowApplicationOptionsPage
        {
            get
            {
                return showApplicationOptionsPage;
            }
            set
            {
                showApplicationOptionsPage = value;
            }
        }



        public string AcceptanceMessageSubject
        {
            get
            {
                return acceptanceMessageSubject;
            }
            set
            {
                acceptanceMessageSubject = value;
            }
        }

        public string MergedAcceptanceMessageSubject
        {
            get
            {
                return WebApp.ObjectTranslator.MergeTags(AcceptanceMessageSubject);
            }
        }

        public string AcceptanceMessageBody
        {
            get
            {
                return acceptanceMessageBody;
            }
            set
            {
                acceptanceMessageBody = value;
            }
        }

        public string MergedAcceptanceMessageBody
        {
            get
            {
                return WebApp.ObjectTranslator.MergeTags(AcceptanceMessageBody);
            }
        }


        public string RejectionMessageSubject
        {
            get
            {
                return rejectionMessageSubject;
            }
            set
            {
                rejectionMessageSubject = value;
            }
        }

        public string MergedRejectionMessageSubject
        {
            get
            {
                return WebApp.ObjectTranslator.MergeTags(RejectionMessageSubject);
            }
        }

        public string RejectionMessageBody
        {
            get
            {
                return rejectionMessageBody;
            }
            set
            {
                rejectionMessageBody = value;
            }
        }

        public string MergedRejectionMessageBody
        {
            get
            {
                return WebApp.ObjectTranslator.MergeTags(RejectionMessageBody);
            }
        }

        public int ContractSections
        {
            get
            {
                return contractSections;
            }
            set
            {
                contractSections = value;
            }
        }
        public bool LoginToConfirmContract
        {
            get
            {
                return loginToConfirmContract;
            }
            set
            {
                loginToConfirmContract = value;
            }
        }

        public bool SendAcceptanceEmail
        {
            get
            {
                return sendAcceptanceEmail;
            }
            set
            {
                sendAcceptanceEmail = value;
            }
        }
        public bool SendRejectionEmail
        {
            get
            {
                return sendRejectionEmail;
            }
            set
            {
                sendRejectionEmail = value;
            }
        }

        public int UdfMapping1
        {
            get
            {
                return udfMapping1;
            }
            set
            {
                udfMapping1 = value;
            }
        }
        public int UdfMapping2
        {
            get
            {
                return udfMapping2;
            }
            set
            {
                udfMapping2 = value;
            }
        }
        public int UdfMapping3
        {
            get
            {
                return udfMapping3;
            }
            set
            {
                udfMapping3 = value;
            }
        }
        public int UdfMapping4
        {
            get
            {
                return udfMapping4;
            }
            set
            {
                udfMapping4 = value;
            }
        }
        public int UdfMapping5
        {
            get
            {
                return udfMapping5;
            }
            set
            {
                udfMapping5 = value;
            }
        }
        public int UdfMapping6
        {
            get
            {
                return udfMapping6;
            }
            set
            {
                udfMapping6 = value;
            }
        }

        public bool AskForPaymentDuringApps
        {
            get
            {
                return askForPaymentDuringApps;
            }
            set
            {
                askForPaymentDuringApps = value;
            }
        }

        public bool AskForPaymentDuringContracts
        {
            get
            {
                return askForPaymentDuringContracts;
            }
            set
            {
                askForPaymentDuringContracts = value;
            }
        }

        public KxProcessConfiguration(KxWebApplicationStudent webApp)
            : base(webApp)
        {
            Load();
        }


        public string ToXml()
        {
            StringBuilder result = new StringBuilder();

            Type typ = this.GetType();

            result.AppendLine("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
            result.AppendLine("<ConfigurationProperties>");


            foreach (PropertyInfo pi in typ.GetProperties())
            {
                if (pi.CanWrite && pi.CanRead)
                {
                   result.AppendLine("    <PropertyValue>");
                   result.AppendLine(String.Format("        <name><![CDATA[{0}]]></name> ", pi.Name));
                   result.AppendLine(String.Format("        <value><![CDATA[{0}]]></value> ", pi.GetValue(this, null).ToString()));
                   result.AppendLine("    </PropertyValue>");
                }
            }
            result.AppendLine("</ConfigurationProperties>");

            return result.ToString();
        }

        public bool Save()
        {
           return KxDbConfiguration.SetConfiguration("ContractConfig",ToXml());
        }

        public bool Load()
        {
            string config = KxDbConfiguration.GetConfiguration("ContractConfig");
            return FromXml(config);

        }

        public bool FromXml(string source)
        {
            bool result = false;
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(source);

                XmlNodeList nodes = doc.SelectNodes("ConfigurationProperties/PropertyValue");

                foreach (XmlNode n in nodes)
                {
                    string name = KxXmlUtils.GetNodeValue(n,"name","");
                    string value = KxXmlUtils.GetNodeValue(n, "value", "");

                    KxReflection.SetProperty(this, name, value);

                }
                result = true;
            }
            catch { }

            return result;
        }

        public int GetUDFId(int Id)
        {
            switch (Id)
            {
                case 1: return UdfMapping1;
                case 2: return UdfMapping2;
                case 3: return UdfMapping3;
                case 4: return UdfMapping4;
                case 5: return UdfMapping5;
                case 6: return UdfMapping6;
                default: return 0;
            }
        }
        

    }
}
