using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Configuration;
using Kx.Core.Data;
using Kx.Core.Exceptions;
using Kx.Core.Localisation;
using Kx.Core.Utility;
using Kx.Core.Web.Student.WebApplication.BaseClass;
using Kx.Core.Web.Student.WebApplication.PageRenderer;
using Kx.Objects.Student;
using Kx.Objects.Student.Web;
using Kx.Objects.Common;
using Kx.Core.Web.Configuration;

namespace Kx.Core.Web.Student.WebApplication.ProcessManagers
{
    public class KxPendingRoom
    {
        protected int residentialRoomId;
        protected int academicYearId;
        
        public int ResidentialRoomId
        {
          get
          { return residentialRoomId; }
          set
          { residentialRoomId = value; }
        }

        public int AcademicYearId
        {
          get
          { return academicYearId; }
          set
          { academicYearId = value;}
        }

        public KxPendingRoom (int year,int room)
        {
            residentialRoomId = room;
            AcademicYearId = room;
        }
    }
    
    public class KxAllocationManager : WebStudentBaseClass
    {
        protected static List<KxPendingRoom> pendingRooms;

        protected KxWebStudentAllocation currentAllocation;

        protected bool useFirstPreference = false;
        protected bool checkInstalments = false;
        protected bool usePaymentSchedule = false;
        protected string lastError = "";

        public bool CheckInstalments
        {
            get
            {
                return checkInstalments;
            }
            set
            {
                checkInstalments = value;
            }
        }

        public bool UsePaymentSchedule
        {
            get
            {
                return usePaymentSchedule;
            }
            set
            {
                usePaymentSchedule = value;
            }
        }

        public bool UseFirstPreference
        {
            get
            {
                return useFirstPreference;
            }
            set
            {
                useFirstPreference = value;
            }
        }

        public bool HasError
        {
            get
            { return LastError != ""; }
        }

        public string LastError
        {
            get
            {
                return lastError;
            }
            set
            {
                lastError = value;
            }
        }



        public static List<KxPendingRoom> PendingRooms
        {
            get
            {
                if (pendingRooms == null)
                {
                    pendingRooms = new List<KxPendingRoom>();
                }
                return pendingRooms;
            }
        }

        public KxWebStudentAllocation CurrentAllocation
        {
            get
            { return currentAllocation; }
            set
            { currentAllocation = value; }
        }



        public KxAllocationManager(KxWebApplicationStudent webApp) : base(webApp)
        {
            
        }
        
        public void AddPendingRoom(int year, int room)
        {
            PendingRooms.Add(new KxPendingRoom(year, room));
        }

        public void RemovePendingRoom(int year, int roomid)
        {
            foreach (KxPendingRoom room in PendingRooms)
            {
                if (room.AcademicYearId == year && room.ResidentialRoomId == roomid)
                {
                    PendingRooms.Remove(room);
                    break;
                }
            }
        }

        public bool CheckPendingRoom(int year, int roomid)
        {
            bool result = false;

            foreach (KxPendingRoom room in PendingRooms)
            {
                if (room.AcademicYearId == year && room.ResidentialRoomId == roomid)
                {
                    result = true;
                    break;
                }
            }
            return result;
        }


        public List<int> GetPendingRoomIds(int year)
        {
            List<int> result = new List<int>();

            foreach (KxPendingRoom room in PendingRooms)
            {
                if (room.AcademicYearId == year)
                {
                    result.Add(room.ResidentialRoomId);
                }
            }
            return result;
        }

        public int GetNextResidentialRoom()
        {
            int result = 0;

            if (WebApp.CurrentBedroomTypeChargeIndex != -1)
            {
                KxData data = WebApp.CurrentBedroomTypeCharge.AddGenericParameters("availability", "GetStudentRoomAvailability");

                if (data.Execute())
                {
                    XmlDocument doc = new XmlDocument();
                    doc.LoadXml(data.XmlResults);

                    XmlNodeList nodes = doc.SelectNodes(@"//availability/datarow");

                    foreach (XmlNode node in nodes)
                    {
                        if (node != null)
                        {
                            int year = KxXmlUtils.GetNodeValue(node, "academicyearid", 0);
                            int room = KxXmlUtils.GetNodeValue(node, "residentialroomid", 0);
                            if (!CheckPendingRoom(year, room))
                            {
                                result = room;
                                WebApp.CurrentBedroomTypeCharge.ResidentialRoomId = room;
                                break;
                            }
                        }
                    }
                }
                else
                {
                    KxLogger.LogActivity(this, "GetNextResidentialRoom", data.LastError,"");
                }
            }
            return result;
        }

        public bool CreateAllocation(int residentialRoomId)
        {
            int allocationId = 0;
            bool success = false;
            // Hold the room while we create the allocation
            KxResidentialRoom room = new KxResidentialRoom(null, residentialRoomId);
            if (room.BlockId == 0)
            {
                KxLogger.LogActivity(this, "CreateAllocation", "ResidentialRoom supplied did not return a row", residentialRoomId.ToString());
                return false;
            }

            AddPendingRoom(WebUser.CurrentApplication.AcademicYearId, residentialRoomId);

            try
            {
                KxData data = new KxData("allocations", "CreateAllocation", TargetType.StoredProc, ActionType.Scalar);

                data.AddParameter("ApplicationGuid", WebUser.CurrentApplication.ApplicationGuid, KxValueType.Guid);
                data.AddParameter("ResidentialRoomId", residentialRoomId, KxValueType.Int);
                data.AddParameter("BlockID", room.BlockId, KxValueType.Int);
                data.AddParameter("AcademicYearID", WebUser.CurrentApplication.AcademicYearId, KxValueType.Int);

                data.ScalarField = "allocationid";

                object obj = data.ExecuteScalar();

                if (obj != null)
                {
                    try
                    {
                        allocationId = Convert.ToInt32(obj);
                    }
                    catch (Exception)
                    {
                        LastError = KxLocalisationManager.GetSubstitution("Unable to Create Allocation");
                    }
                }
                else
                {
                    LastError = KxLocalisationManager.GetSubstitution("Unable to Create Allocation");
                }

                if (allocationId < 0)
                {
                    KxLogger.LogActivity(this, "CreateAllocation", residentialRoomId.ToString(), "Room has already been allocated");
                    return false;
                }

                if (allocationId != 0)
                {
                    // Calculate charges
                    try
                    {
                        WebUser.Allocations.Load();
                        success = true;

                        currentAllocation = WebUser.Allocations.GetAllocationById(allocationId);

                        if (WebUser.CurrentApplication.ApplicationId == 0)
                        {
                            WebUser.CurrentApplication.ApplicationId = currentAllocation.StudentAllocation.ApplicationId;
                            WebUser.CurrentApplication.ConfirmedDate = DateTime.Now;
                        }

                        if (currentAllocation.StudentAllocation.Application != null)
                        {
                            try
                            {
                                WebUser.Children.Remove(Student);
                                WebUser.Student = null;
                                WebUser.KxStudentId = currentAllocation.StudentAllocation.Application.KxStudentId;
                                WebUser.CurrentApplication.KxStudentId = currentAllocation.StudentAllocation.Application.KxStudentId;
                            }
                            catch (Exception ex)
                            {
                                KxLogger.LogActivity(this, "CreateAllocation #4", ex.Message, "");
                            }
                        }
                        try
                        {
                            KxAllocationChargeCalculationUniMethod ChargeCalculation = new KxAllocationChargeCalculationUniMethod(null, WebApp, room.Block.SiteId, room.BlockId, residentialRoomId, WebUser.CurrentApplication.AcademicYearId);

                            if (KxConfigurationManager.Profiling)
                            {
                                KxLogger.LogActivity(this, "ChargeCalculation.ProcessCharges", "Start", "");
                            }
                            success = ChargeCalculation.ProcessCharges(false, false, room.Block.SiteId);
                            if (KxConfigurationManager.Profiling)
                            {
                                KxLogger.LogActivity(this, "ChargeCalculation.ProcessCharges", "End", "");
                            }
                        }
                        catch (Exception ex)
                        {
                            LastError = ex.Message;
                            KxLogger.LogActivity(this, "CreateAllocation #5", ex.Message, allocationId.ToString());
                        }
                    }
                    catch (Exception ex)
                    {
                        LastError = ex.Message;
                        KxLogger.LogActivity(this, "CreateAllocation #5a", ex.Message, allocationId.ToString());
                    }
                }
            }
            finally
            {
                if (!success)
                {
                    RemovePendingRoom(WebUser.CurrentApplication.AcademicYearId, residentialRoomId);
                }
            }

            return success;
        }

  
        public bool CreateAllocation()
        {
            bool result = false;
            int allocationId = 0;
            LastError = "";

            if (UseFirstPreference)
            {

                CreateBedroomChargeFromPreference();
            }

            if (WebApp.CurrentBedroomTypeCharge != null)
            {
                GetNextResidentialRoom();

                bool success = false;

                if (WebApp.CurrentBedroomTypeCharge.ResidentialRoomId != 0)
                {
                    // Hold the room while we create the allocation
                    AddPendingRoom(WebApp.CurrentBedroomTypeCharge.AcademicYearId, WebApp.CurrentBedroomTypeCharge.ResidentialRoomId);
                    try
                    {
                        KxData data = new KxData("allocations", "CreateAllocation", TargetType.StoredProc, ActionType.Scalar);

                        data.AddParameter("ApplicationGuid", WebUser.CurrentApplication.ApplicationGuid, KxValueType.Guid);
                        data.AddParameter("ResidentialRoomId", WebApp.CurrentBedroomTypeCharge.ResidentialRoomId, KxValueType.Int);
                        data.AddParameter("BlockID", WebApp.CurrentBedroomTypeCharge.BlockId, KxValueType.Int);
                        data.AddParameter("AcademicYearID", WebApp.CurrentBedroomTypeCharge.AcademicYearId, KxValueType.Int);

                        data.ScalarField = "allocationid";

                        object obj = data.ExecuteScalar();

                        if (obj != null)
                        {
                            try
                            {
                                allocationId = Convert.ToInt32(obj);
                            }
                            catch (Exception ex)
                            {
                                LastError = KxLocalisationManager.GetSubstitution("Unable to Create Allocation");
                                KxLogger.LogActivity(this, "CreateAllocation #1", ex.Message, data.LastError);
                            }
                        }
                        else
                        {
                            LastError = KxLocalisationManager.GetSubstitution("Unable to Create Allocation");
                            KxLogger.LogActivity(this, "CreateAllocation #1.6",data.LastError,"");
                        }

                        if (allocationId != 0)
                        {
                            // Calculate charges
                            try
                            {
                                KxLogger.LogActivity(this, "IG1 #4", "", "");

                                WebUser.Allocations.Load();

                                KxLogger.LogActivity(this, "IG2 #4", "", "");

                                success = true;

                                currentAllocation = WebUser.Allocations.GetAllocationById(allocationId);

                                KxLogger.LogActivity(this, "IG3 #4", "", "");

                                if (WebUser.CurrentApplication.ApplicationId == 0)
                                {
                                    KxLogger.LogActivity(this, "IG4 #4", "", "");
                                    WebUser.CurrentApplication.ApplicationId = currentAllocation.StudentAllocation.ApplicationId;
                                    KxLogger.LogActivity(this, "IG5 #4", "", "");
                                    WebUser.CurrentApplication.ConfirmedDate = DateTime.Now;
                                    KxLogger.LogActivity(this, "IG6 #4", "", "");
                                }

                                KxLogger.LogActivity(this, "IG7 #4", "", "");

                                if (currentAllocation.StudentAllocation.Application != null)
                                {
                                    try
                                    {
                                        WebUser.Children.Remove(Student);
                                        WebUser.Student = null;
                                        WebUser.KxStudentId = currentAllocation.StudentAllocation.Application.KxStudentId;
                                        WebUser.CurrentApplication.KxStudentId = currentAllocation.StudentAllocation.Application.KxStudentId;
                                    }
                                    catch
                                    {
                                        KxLogger.LogActivity(this, "CreateAllocation #4", "", "");
                                    }
                                }
                                try
                                {
                                    KxAllocationChargeCalculation ChargeCalculation = new KxAllocationChargeCalculation(null, WebApp);

                                    if (KxConfigurationManager.Profiling)
                                    {
                                        KxLogger.LogActivity(this, "ChargeCalculation.ProcessCharges", "Start", "");
                                    }
                                    result = ChargeCalculation.ProcessCharges(checkInstalments, usePaymentSchedule, WebUser.CurrentApplication.FirstPreference.SiteId);
                                    if (KxConfigurationManager.Profiling)
                                    {
                                        KxLogger.LogActivity(this, "ChargeCalculation.ProcessCharges", "End", "");
                                    }
                                }
                                catch (Exception ex)
                                {
                                    LastError = ex.Message;
                                    KxLogger.LogActivity(this, "CreateAllocation #5", ex.Message, allocationId.ToString());
                                }
                            }
                            catch (Exception ex)
                            {
                                LastError = ex.Message;
                                KxLogger.LogActivity(this, "CreateAllocation #5a", ex.Message, allocationId.ToString());
                            }
                        }
                    }
                    finally
                    {
                        if (!success)
                        {
                            RemovePendingRoom(WebApp.CurrentBedroomTypeCharge.AcademicYearId, WebApp.CurrentBedroomTypeCharge.ResidentialRoomId);
                        }
                    }
                }
                else
                {
                    LastError = KxLocalisationManager.GetSubstitution("Unable to to Allocate a Room, the room type you selected is no longer available");
                    KxLogger.LogActivity(this, "CreateAllocation #2", LastError,"");
                }
            }
            else
            {
                LastError = KxLocalisationManager.GetSubstitution("Unable to to Allocate a Room, based on the choice you have made");
                KxLogger.LogActivity(this, "CreateAllocation #3", LastError, "");
            }

            return LastError =="";
        }

        public bool CreateBedroomChargeFromPreference()
        {
            bool result = true;

            try
            {

                WebApp.BedroomTypeCharges.SiteId = WebUser.CurrentApplication.FirstPreference.SiteId;
                WebApp.BedroomTypeCharges.BedroomTypeId = WebUser.CurrentApplication.FirstPreference.BedroomTypeId;
                WebApp.BedroomTypeCharges.ContractLength = WebUser.CurrentApplication.FirstPreference.ContractLength;


                if (KxDbConfiguration.GetConfiguration("ApplicationId").ToUpper() == "LIBERTY" || KxDbConfiguration.GetConfiguration("ApplicationId").ToUpper() == "CITYLIVING2")
                {
                    int allocationModelId = 0;
                    if (Int32.TryParse(WebUser.CurrentApplication.FirstPreference.CustomValue1, out allocationModelId))
                    {
                        WebApp.BedroomTypeCharges.AllocationModelId = allocationModelId;
                    }
                    else
                    {
                        LastError = KxLocalisationManager.GetSubstitution("Allocation Model is invalid :" + WebUser.CurrentApplication.FirstPreference.CustomValue1);
                        KxLogger.LogActivity(this, "CreateBedroomChargeFromPreference", LastError, "");
                    }
                    WebApp.BedroomTypeCharges.CustomValue1 = WebUser.CurrentApplication.FirstPreference.CustomValue1;
                    WebApp.BedroomTypeCharges.CustomValue2 = WebUser.CurrentApplication.FirstPreference.CustomValue2;
                    WebApp.BedroomTypeCharges.BedroomTypeId = 0;
                    WebApp.BedroomTypeCharges.BlockId = WebUser.CurrentApplication.FirstPreference.BlockId;
                    WebApp.BedroomTypeCharges.WebDescription = WebUser.CurrentApplication.FirstPreference.BedroomWebDescription;
                }
                else if (KxDbConfiguration.GetConfiguration("ApplicationId").ToUpper() == "OPAL")
                {
                    WebApp.BedroomTypeCharges.AreaId = WebUser.CurrentApplication.FirstPreference.AreaId;
                    WebApp.BedroomTypeCharges.AllocationModelId = Convert.ToInt32(WebUser.CurrentApplication.FirstPreference.CustomValue1);
                    WebApp.BedroomTypeCharges.DefaultPricingModelId = Convert.ToInt32(WebUser.CurrentApplication.FirstPreference.CustomValue2);
                }

                // Logged added to debug CityLiving allocation issues...
                //KxLogger.LogActivity(this, "CreateBedroomChargeFromPreference", "WebApp.BedroomTypeCharges.SiteId", WebApp.BedroomTypeCharges.SiteId.ToString());
                //KxLogger.LogActivity(this, "CreateBedroomChargeFromPreference", "WebApp.BedroomTypeCharges.BedroomTypeId", WebApp.BedroomTypeCharges.BedroomTypeId.ToString());
                //KxLogger.LogActivity(this, "CreateBedroomChargeFromPreference", "WebApp.BedroomTypeCharges.ContractLength", WebApp.BedroomTypeCharges.ContractLength.ToString());
                //KxLogger.LogActivity(this, "CreateBedroomChargeFromPreference", "WebApp.BedroomTypeCharges.AllocationModelId", WebApp.BedroomTypeCharges.AllocationModelId.ToString());
                //KxLogger.LogActivity(this, "CreateBedroomChargeFromPreference", "WebApp.BedroomTypeCharges.CustomValue1", WebApp.BedroomTypeCharges.CustomValue1);
                //KxLogger.LogActivity(this, "CreateBedroomChargeFromPreference", "WebApp.BedroomTypeCharges.CustomValue2", WebApp.BedroomTypeCharges.CustomValue2);
                //KxLogger.LogActivity(this, "CreateBedroomChargeFromPreference", "WebApp.BedroomTypeCharges.BlockId", WebApp.BedroomTypeCharges.BlockId.ToString());
                //KxLogger.LogActivity(this, "CreateBedroomChargeFromPreference", "WebApp.BedroomTypeCharges.WebDescription", WebApp.BedroomTypeCharges.WebDescription);

                string[] results = WebApp.BedroomTypeCharges.GetMatchingBedrooms(WebUser.CurrentAcademicYear, null);

                //KxLogger.LogActivity(this, "CreateBedroomChargeFromPreference", "Results Returned", results.Length.ToString());
                if (results.Length > 0)
                {
                    //KxLogger.LogActivity(this, "CreateBedroomChargeFromPreference", "First Results Returned", results[0].ToString());
                    WebApp.UserBedroomTypeCharge = Convert.ToInt32(results[0]);
                }
                //KxLogger.LogActivity(this, "CreateBedroomChargeFromPreference", "WebApp.UserBedroomTypeCharge", WebApp.UserBedroomTypeCharge.ToString());
            }
            catch { }

            return result;
        }
    }
}
