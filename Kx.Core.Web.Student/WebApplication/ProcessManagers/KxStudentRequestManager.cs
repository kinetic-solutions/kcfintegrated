﻿using System;
using System.Collections.Generic;
using System.Text;
using Kx.Core.Web.Student.WebApplication.BaseClass;
using Kx.Objects.Student;
using Kx.Core.Exceptions;

namespace Kx.Core.Web.Student.WebApplication.ProcessManagers
{
    public class KxStudentRequestManager : WebStudentBaseClass
    {
        private KxStudentRequest _currentRequest;

        public KxStudentRequest CurrentRequest
        {
            get
            {
                return _currentRequest;
            }
        }

        public string [] RequestArray
        {
            get
            {
                List<string> result = new List<string>();
                foreach (KxStudentRequest item in WebUser.CurrentApplication.StudentRequests)
                {
                    result.Add(item.StudentRequestId.ToString());
                }

                return result.ToArray();
            }
        }

        #region Constructors

        public KxStudentRequestManager(KxWebApplicationStudent webApp)
            : base(webApp)
        {

        }
        #endregion

        #region Internal Methods
        internal void StartNewRequest(string webKey)
        {
            _currentRequest = new KxStudentRequest(null);

            SetRequestType(webKey);
            SetIdentifiers();
            _currentRequest.RequestDate = DateTime.Now;
            _currentRequest.LastStatusUpdate = DateTime.Now;
            _currentRequest.LastStatusUpdateBy = WebUser.UserName;
        }

        internal void SetCurrentRequest(int studentRequestId)
        {
            var request = WebUser.CurrentApplication.StudentRequests.Find(s => s.StudentRequestId == studentRequestId);
            if (request != null)
            {
                _currentRequest = request;
            }
        }

        internal bool SubmitRequest()
        {
            if (_currentRequest.Save())
            {
                WebUser.CurrentApplication.ClearRequestCache();
                return true;
            }

            return false;
        }

        internal KxStudentRequestData GetRequestField(string label)
        {
            return _currentRequest.GetDataValueByLabel(label);
        }
        #endregion

        #region Private Methods
        private void SetIdentifiers()
        {
            _currentRequest.KxStudentId = WebUser.KxStudentId;
            if (WebUser.CurrentApplication != null)
            {
                _currentRequest.ApplicationId = WebUser.CurrentApplication.ApplicationId;
            }
            else
            {
                _currentRequest.ApplicationId = 0;
            }
            if (WebUser.InHouseAllocation != null)
            {
                _currentRequest.AllocationId = WebUser.InHouseAllocation.AllocationId;
            }
            else if (WebUser.AcceptedAllocation != null)
            {
                _currentRequest.AllocationId = WebUser.AcceptedAllocation.AllocationId;
            }
            else
            {
                _currentRequest.AllocationId = 0;
            }
        }

        private void SetRequestType(string webKey)
        {
            KxRequestType requestType = KxRequestType.GetST2RequestTypes(webKey);
            if (requestType != null)
            {
                _currentRequest.RequestTypeId = requestType.RequestTypeId;
                _currentRequest.RequestStatusId = requestType.InitialStatusId;
            }
            else
            {
                KxLogger.LogActivity(this, "SetRequestType", string.Format("Request type {0} not found", webKey), string.Empty);
            }
        }
        #endregion
    }
}
