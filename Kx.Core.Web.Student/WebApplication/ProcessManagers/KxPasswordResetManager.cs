using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;
using System.Xml;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using Kx.Core.Configuration;
using Kx.Core.Data;
using Kx.Core.Exceptions;
using Kx.Core.Localisation;
using Kx.Core.Utility;
using Kx.Core.Web.Security;
using Kx.Core.Web.Student.WebApplication.BaseClass;
using Kx.Core.Web.Student.WebApplication.PageRenderer;
using Kx.Objects.Student;
using Kx.Objects.Student.Web;
using Kx.Core.Web.Student.WebApplication.ProcessManagers.ManagerInterface;
using Kx.Core.Web.Configuration;
using System.Net;
using Kx.Objects.Common;
using Kx.Objects.Common.Web;

namespace Kx.Core.Web.Student.WebApplication.ProcessManagers
{
    public class KxPasswordResetManager : WebStudentBaseClass, KxManagerInterface
    {
        #region Attributes & Properties

        private string userName = "";
        private string password = "";
        private string singlepageRegisterPassword = "";
        private string confirmPassword = "";
        private string foreName = "";
        private string surName = "";
        private string salt = "";
        private string emailAddress = "";
        private string confirmEmailAddress = "";
        private int retryCount;
        private int retryLimit;
        private string studentId = "";
        private string uCASNumber = "";
        private string genericId = "";
        private string userConfirmationCode = "";
        private string recaptchaToken = "";
        private DateTime? dob;
        private int matchedStudentId;
        private KxStudent matchedStudent = null;

        private bool retriesExceeded = false;
        private bool invalidUser = false;
        private bool invalidLogon = false;
        private bool unconfirmedEmail = false;
        private bool requireConfirmedEmail = false;
        private bool emailUserName = false;
        private bool captureMarketingCampaign = false;
        private string campaignId = string.Empty;
        private string marketingSource = string.Empty;

        protected KxWebUserShortUrl shortUrl = null;
        protected KxWebUserStudent webUserStudent = null;

        public string UserName
        {
            get
            {
                return userName;
            }
            set
            {
                if (userName == value)
                    return;
                userName = value;
            }
        }
        public string Password
        {
            get
            {
                return password;
            }
            set
            {
                if (password == value)
                    return;
                password = value;
            }
        }

        public string SinglePageRegisterPassword
        {
            get
            {
                return singlepageRegisterPassword;
            }
            set
            {
                if (singlepageRegisterPassword == value)
                    return;
                singlepageRegisterPassword = value;
            }
        }
        public string ConfirmPassword
        {
            get
            {
                return confirmPassword;
            }
            set
            {
                if (confirmPassword == value)
                    return;
                confirmPassword = value;
            }
        }
        public string ForeName
        {
            get
            {
                return foreName;
            }
            set
            {
                if (foreName == value)
                    return;
                foreName = value;
            }
        }
        public string SurName
        {
            get
            {
                return surName;
            }
            set
            {
                if (surName == value)
                    return;
                surName = value;
            }
        }
        public string Salt
        {
            get
            {
                return salt;
            }
            set
            {
                if (salt == value)
                    return;
                salt = value;
            }
        }
        public string EmailAddress
        {
            get
            {
                return emailAddress;
            }
            set
            {
                if (emailAddress == value)
                    return;
                emailAddress = value;
            }
        }

        public bool EmailUserName
        {
            get
            {
                return emailUserName;
            }
            set
            {
                emailUserName = value;
            }
        }

        public string ConfirmEmailAddress
        {
            get
            {
                return confirmEmailAddress;
            }
            set
            {
                if (confirmEmailAddress == value)
                    return;
                confirmEmailAddress = value;
            }
        }
        public int RetryCount
        {
            get
            {
                return retryCount;
            }
            set
            {
                if (retryCount == value)
                    return;
                retryCount = value;
            }
        }
        public int RetryLimit
        {
            get
            {
                return retryLimit;
            }
            set
            {
                if (retryLimit == value)
                    return;
                retryLimit = value;
            }
        }
        public string StudentId
        {
            get
            {
                return studentId;
            }
            set
            {
                if (studentId == value)
                    return;
                studentId = value;
            }
        }
        public string UCASNumber
        {
            get
            {
                return uCASNumber;
            }
            set
            {
                if (uCASNumber == value)
                    return;
                uCASNumber = value;
            }
        }
        public string GenericId
        {
            get
            {
                return genericId;
            }
            set
            {
                if (genericId == value)
                    return;
                genericId = value;
            }
        }
        public DateTime? Dob
        {
            get
            { return dob; }
            set
            {
                dob = value;
            }
        }
        public int MatchedStudentId
        {
            get
            {
                return matchedStudentId;
            }
            set
            {

                matchedStudentId = value;
                matchedStudent = null;
            }
        }
        public bool RetriesExceeded
        {
            get
            {
                return retriesExceeded;
            }
            set
            {
                if (retriesExceeded == value)
                    return;
                retriesExceeded = value;
            }
        }
        public bool InvalidUser
        {
            get
            {
                return invalidUser;
            }
            set
            {
                if (invalidUser == value)
                    return;
                invalidUser = value;
            }
        }
        public bool InvalidLogon
        {
            get
            {
                return invalidLogon;
            }
            set
            {
                if (invalidLogon == value)
                    return;
                invalidLogon = value;
            }
        }
        public bool RequireConfirmedEmail
        {
            get
            {
                return requireConfirmedEmail;
            }
            set
            {
                if (requireConfirmedEmail == value)
                    return;
                requireConfirmedEmail = value;
            }
        }
        public bool UnconfirmedEmail
        {
            get
            {
                return unconfirmedEmail;
            }
            set
            {
                if (unconfirmedEmail == value)
                    return;
                unconfirmedEmail = value;
            }
        }

        public KxWebUserShortUrl ShortUrl
        {
            get
            {
                if (shortUrl == null)
                {
                    shortUrl = new KxWebUserShortUrl(null, WebUserStudent.UserGuid, CodeType.PasswordReset);

                }
                return shortUrl;
            }
            set
            {
                shortUrl = value;
            }
        }

        public KxWebUserStudent WebUserStudent
        {
            get { return webUserStudent; }
            set { webUserStudent = value; }
        }

        public string UniqueConfirmationCode
        {
            get
            { return ShortUrl.ShortCode; }
        }

        public string UserConfirmationCode
        {
            get
            { return GetUserConfirmationCode(); }
            set
            { userConfirmationCode = KxStringUtils.HardTrim(value); }
        }

        public string ConfirmationUrl
        {
            get
            {
                return ShortUrl.LongUrl;
            }
        }

        public string ShortConfirmationUrl
        {
            get
            {
                return ShortUrl.ShortUrl;
            }
        }

        public KxStudent MatchedStudent
        {
            get
            {
                if (matchedStudent == null && matchedStudentId > 0)
                {
                    matchedStudent = new KxStudent(null, MatchedStudentId);
                }
                return matchedStudent;
            }
        }

        public string CampaignId
        {
            get
            {
                return campaignId;
            }
            set
            {
                campaignId = value;
            }
        }

        public string MarketingSource
        {
            get
            {
                return marketingSource;
            }
            set
            {
                marketingSource = value;
            }
        }

        public bool CaptureMarketingCampaign
        {
            get
            {
                return captureMarketingCampaign;
            }
            set
            {
                captureMarketingCampaign = value;
            }
        }

        #endregion

        public KxPasswordResetManager(KxWebApplicationStudent webApp)
            : base(webApp)
        {

        }

        public string ResetPassword()
        {
            if (password == confirmPassword)
            {
                KxWebUserStudent user = new KxWebUserStudent(null, shortUrl.UserGuid);
                
                if (user.IsValid())
                {
                    user.Salt = KxWebSecurity.GenerateSalt(8);
                    user.Password = KxWebSecurity.Hash(user.Salt + Password);
                    if (!KxDbConfiguration.GetConfigurationBool("RetainPassword", false))
                    {
                        Password = string.Empty;
                    }
                    //Reset();
                    UserName = user.UserName;
                    EmailAddress = user.EmailAddress;
                    user.Save();

                    return "";
                }
                else
                {
                    return KxLocalisationManager.GetSubstitution("No matching account found, please register");
                }
            }
            else
            {
                return KxLocalisationManager.GetSubstitution("New passwords must match");
            }
        }

        public string ResetPassword2()
        {
            if (password == confirmPassword)
            {
                KxWebUserStudent user = DoWebMatching();

                if (user != null)
                {
                    if (user.IsValid())
                    {
                        user.Salt = KxWebSecurity.GenerateSalt(8);
                        user.Password = KxWebSecurity.Hash(user.Salt + Password);
                        if (!KxDbConfiguration.GetConfigurationBool("RetainPassword", false))
                        {
                            Password = string.Empty;
                        }
                        Reset();
                        UserName = user.UserName;
                        user.Save();

                        return "";
                    }
                    else
                    {
                        return KxLocalisationManager.GetSubstitution("No matching account found, please register");
                    }
                }
                else
                {
                    return KxLocalisationManager.GetSubstitution("No matching student found");
                }
            }
            else
            {
                return KxLocalisationManager.GetSubstitution("New passwords must match");
            }
        }

        public void DoStudentMatching()
        {
            //matchedStudentId = 0;

            KxData data = new KxData("", "StudentMatching", TargetType.StoredProc);

            if (!String.IsNullOrEmpty(foreName))
            { data.AddParameter("foreName", foreName, KxValueType.String); }

            if (!String.IsNullOrEmpty(surName))
            { data.AddParameter("surName", surName, KxValueType.String); }

            if (!String.IsNullOrEmpty(emailAddress))
            { data.AddParameter("emailAddress", emailAddress, KxValueType.String); }

            if (!String.IsNullOrEmpty(studentId))
            { data.AddParameter("studentId", studentId, KxValueType.String); }

            if (!String.IsNullOrEmpty(uCASNumber))
            { data.AddParameter("uCASNumber", uCASNumber, KxValueType.String); }

            if (!String.IsNullOrEmpty(genericId))
            { data.AddParameter("GenericId", genericId, KxValueType.String); }

            if (dob != null)
            { data.AddParameter("dob", dob, KxValueType.DateTime); }

            object result = data.ExecuteScalar();

            if (result != null)
            {
                try
                {
                    matchedStudentId = Convert.ToInt32(result);
                }
                catch
                { }
            }
        }

        public bool DoesStudentHaveWebAccount(int studentId)
        {
            KxWebUserStudent wu = new KxWebUserStudent();

            wu.LoadStudent(studentId);

            return wu.IsValid() && !string.IsNullOrEmpty(wu.UserName) && !string.IsNullOrEmpty(wu.Password) && !string.IsNullOrEmpty(wu.EmailAddress);
        }

        public KxWebUserStudent DoAccountMatching()
        {
            KxWebUserStudent result = null;

            if (WebUser.Password != "")
            {
                result = WebUser;
                //UniqueConfirmationCode = WebUser.UniqueCode;
            }
            else
            {
                KxData data = new KxData("", "WebAccountMatching", TargetType.StoredProc);

                if (foreName != "")
                { data.AddParameter("foreName", foreName, KxValueType.String); }

                if (surName != "")
                { data.AddParameter("surName", surName, KxValueType.String); }

                if (emailAddress != "")
                { data.AddParameter("emailAddress", emailAddress, KxValueType.String); }

                if (studentId != "")
                { data.AddParameter("studentId", studentId, KxValueType.String); }

                if (uCASNumber != "")
                { data.AddParameter("uCASNumber", uCASNumber, KxValueType.String); }

                if (dob != null)
                { data.AddParameter("dob", dob, KxValueType.DateTime); }

                object obj = data.ExecuteScalar();

                if (obj != null)
                {
                    try
                    {
                        KxWebUserStudent user = new KxWebUserStudent(null, (Guid)obj);
                        if (user.IsValid())
                        {
                            // UniqueConfirmationCode = user.UniqueCode;
                            result = user;
                        }
                    }
                    catch
                    { }
                }
            }
            return result;
        }

        public KxWebUserStudent DoWebMatching()
        {
            KxWebUserStudent result = null;

            if (WebUser.Password != "")
            {
                result = WebUser;
                //UniqueConfirmationCode = WebUser.UniqueCode;
            }
            else
            {
                KxData data = new KxData("", "WebUserMatching", TargetType.StoredProc);

                if (foreName != "")
                {
                    data.AddParameter("foreName", foreName, KxValueType.String);
                }

                if (surName != "")
                {
                    data.AddParameter("surName", surName, KxValueType.String);
                }

                if (emailAddress != "")
                {
                    data.AddParameter("emailAddress", emailAddress, KxValueType.String);
                }

                object obj = data.ExecuteScalar();

                if (obj != null)
                {
                    try
                    {
                        KxWebUserStudent user = new KxWebUserStudent(null, (Guid)obj);
                        if (user.IsValid())
                        {
                            // UniqueConfirmationCode = user.UniqueCode;
                            result = user;
                        }
                    }
                    catch
                    {
                    }
                }
            }
            return result;
        }

        public bool ValidateConfirmationCode(bool ignoreActiveFlag)
        {
            try
            {
                return KxWebUserStudent.ValidateUniqueCode(KxStringUtils.HardTrim(UserConfirmationCode), ignoreActiveFlag);
            }
            catch
            {
                return false;
            }
        }

        public string ValidateConfirmationCodeAlreadyConfirmed()
        {
            try
            {
                string result = KxWebUserStudent.ValidateUniqueCodeVerbose(KxStringUtils.HardTrim(UserConfirmationCode), false);

                if (result == "")
                {
                    result = "confirmed";
                }
                else if (result == KxLocalisationManager.GetSubstitution("Account for this student already exists, login using your credentials"))
                {
                    result = "";
                }
                return result;
            }
            catch
            {
                return KxLocalisationManager.GetSubstitution("Unknown Error");
            }

        }

        public string ValidateConfirmationCodeVerbose()
        {
            try
            {
                string result = string.Empty;
                result = KxLocalisationManager.GetSubstitution(KxWebUserStudent.ValidatePasswordResetCodeVerbose(KxStringUtils.HardTrim(UserConfirmationCode)));
                if (result == "")
                    shortUrl = new KxWebUserShortUrl(null, UserConfirmationCode);
                return result;
            }
            catch
            {
                return KxLocalisationManager.GetSubstitution("Unknown Error");
            }
        }

        public string GetUserConfirmationCode()
        {
            if (HttpContext.Current.Request["c"] != null && HttpContext.Current.Request["c"].Length > userConfirmationCode.Length)
            {
                userConfirmationCode = HttpContext.Current.Request["c"].ToString();
            }
            return userConfirmationCode;
        }

        public void Reset()
        {
            genericId = "";
            userName = "";
            password = "";
            confirmPassword = "";
            foreName = "";
            surName = "";
            salt = "";
            emailAddress = "";
            confirmEmailAddress = "";
            studentId = "";
            uCASNumber = "";
            userConfirmationCode = "";
            dob = null;
            recaptchaToken = "";
            campaignId = string.Empty;
            marketingSource = string.Empty;
        }
    }
}
