﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;
using System.Web;
using Kx.Core.Configuration;
using Kx.Core.Data;
using Kx.Core.Exceptions;
using Kx.Core.Localisation;
using Kx.Core.PaymentProcessor;
using Kx.Core.PaymentProcessor.Cache;
using Kx.Core.PaymentProcessor.Payments;
using Kx.Core.Security;
using Kx.Core.Utility;
using Kx.Objects.Common;
using Kx.Objects.Student.Web;
using Kx.Objects.Student;
using Kx.Core.Web.Security;
using Kx.Core.Web.Student.WebApplication.BaseClass;
using Kx.Core.Web.Student.WebApplication.PageRenderer;
using Kx.Core.Web.Student.WebApplication.WorkflowInterface.WorkflowExtenders;
using Winnovative;
using Winnovative.WnvRtfToPdf;

namespace Kx.Core.Web.Student.WebApplication.ProcessManagers
{
    public partial class KxContractsManager : WebStudentBaseClass
    {
        private PaymentStatus payStatus = PaymentStatus.None;
        private PaymentResponse payResponse = null;
        private const string STR_DATEFORMAT = "yyyy";

        public PaymentResponse PayResponse
        {
            get
            { return payResponse; }
            set
            { payResponse = value; }
        }

        public PaymentStatus PayStatus
        {
            get
            { return payStatus; }
            set
            { payStatus = value; }
        }


        public string AuthorisedText
        {
            get
            {
                if (payResponse != null)
                {
                    return payResponse.AuthorisedText;
                }
                else
                {
                    return PayStatus == PaymentStatus.Authorised ? "true" : "false";
                }
            }

        }

        public string PaymentFailureMessage
        {
            get
            {
                if (payResponse != null)
                {
                    if (payResponse.AuthorisationMessage != "")
                    {
                        return payResponse.AuthorisationMessage;
                    }
                    else
                    {
                        return KxLocalisationManager.GetSubstitution("Unknown Error");
                    }
                }
                else
                {
                    return KxLocalisationManager.GetSubstitution("Unknown Error");
                }
            }
        }

        public void PostPayment(WorkflowWpmPaymentExtender ext)
        {
            PostPayment(ext, true);
        }

        public virtual PaymentRequest PostPayment(WorkflowWpmPaymentExtender ext, bool followthrough)
        {
            PaymentRequest req = new PaymentRequest();

            req.UniqueIdentifier = Guid.NewGuid();

            AddStudentInfo(ref req, true);

            req.PaymentProvider = "WPMKX";
            req.BookingReference = Guid.NewGuid().ToString();
            req.WorkflowGuid = WebApp.WorkflowInterface.WorkflowGuid.ToString();
            req.System = KxSystem.Student;
            req.CustomerId = req.StudentId;
            req.ApplicationId = WebUser.CurrentApplication.ApplicationId.ToString();
            req.ApplicationGuid = WebUser.CurrentApplication.ApplicationGuid;
            req.TestMode = ext.TestMode;
            req.ProviderUrl = WebApp.ObjectTranslator.MergeTags(ext.ProviderUrl);
            req.ProviderCallbackUrl = GetPaymentUrl(ext.ProviderCallbackUrl);


            if (ext.PaymentTypes.Contains("DD") || ext.PaymentTypes.Contains("RCP"))
            {
                Payment pay = new Payment();
                pay.Description = ext.PaymentDescription;

                if (ext.PaymentTypes.Contains("DD"))
                {
                    pay.PayType = KxWpmPaymentType.DD;
                }
                else
                {
                    pay.PayType = KxWpmPaymentType.RCP;
                }

                string instalmentdates = "";
                string instalmentamounts = "";

                if (ext.InstalmentMethod != "PAYMENTSCHEDULE")
                {
                    instalmentamounts = WebApp.ObjectTranslator.MergeTags(ext.InstalmentAmounts.ToString());
                    instalmentdates = WebApp.ObjectTranslator.MergeTags(ext.InstalmentDates);
                }
                else
                {
                    if (WebUser.CurrentApplication.PaymentScheduleTypeId != 0)
                    {
                        try
                        {
                            instalmentdates = WebUser.CurrentApplication.ApplicationCharges.GetPaymentDateString(",");
                            instalmentamounts = WebUser.CurrentApplication.ApplicationCharges.GetPaymentAmounts(",");

                        }
                        catch { }
                        //pr.InstallmentCount = 3;
                        //  instalmentdates = "01/09/2009,01/01/2010,01/05/2010";
                        //  instalmentamounts = "100.00,200.00,300.00";
                    }
                    else
                    {
                        instalmentdates = WebUser.CurrentApplication.ApplicationCharges.GetFirstPaymentDateString();
                        instalmentamounts = Math.Round(WebUser.CurrentApplication.ApplicationCharges.GetTotalCharges(), 2).ToString();
                    }
                }

                string[] dates = instalmentdates.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                string[] amounts = instalmentamounts.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                if (dates.Length > 0 && dates.Length == amounts.Length)
                {
                    for (int i = 0; i < dates.Length; i++)
                    {
                        PaymentInstalment pi = new PaymentInstalment();

                        pi.Amount = Decimal.Parse(amounts[i]);
                        pi.InstalmentDate = DateTime.Parse(dates[i]);
                        pi.Description = ext.PaymentDescription;
                        pay.Instalments.Add(pi);
                    }
                }
                else
                {
                    //Raise Error 
                }
                req.Payments.Add(pay);

            }

            if (!ext.PaymentTypes.Contains("DD"))
            {
                if (ext.PaymentTypes.Contains("PAYNOW"))
                {
                    Payment pay1 = new Payment();
                    pay1.PayType = KxWpmPaymentType.PayNow;
                    pay1.Amount = Convert.ToDecimal(WebApp.ObjectTranslator.MergeTags(ext.PaymentAmount));
                    pay1.Description = WebApp.ObjectTranslator.MergeTags(ext.PaymentDescription);

                    req.Payments.Add(pay1);

                }

                if (ext.PaymentTypes.Contains("DEPOSIT"))
                {
                    Payment pay2 = new Payment();
                    pay2.PayType = KxWpmPaymentType.Deposit;
                    pay2.Amount = Convert.ToDecimal(WebApp.ObjectTranslator.MergeTags(ext.DepositAmount));
                    pay2.Description = WebApp.ObjectTranslator.MergeTags(ext.DepositDescription);

                    req.Payments.Add(pay2);

                }

            }

            if (!String.IsNullOrEmpty(ext.Customfield1Name))
            {
                req.AddCustomField(ext.Customfield1Name, WebApp.ObjectTranslator.MergeTags(ext.Customfield1Value));
            }

            if (!String.IsNullOrEmpty(ext.Customfield2Name))
            {
                req.AddCustomField(ext.Customfield2Name, WebApp.ObjectTranslator.MergeTags(ext.Customfield2Value));
            }

            if (!String.IsNullOrEmpty(ext.Customfield3Name))
            {
                req.AddCustomField(ext.Customfield3Name, WebApp.ObjectTranslator.MergeTags(ext.Customfield3Value));
            }

            if (!String.IsNullOrEmpty(ext.Customfield4Name))
            {
                req.AddCustomField(ext.Customfield4Name, WebApp.ObjectTranslator.MergeTags(ext.Customfield4Value));
            }

            if (!String.IsNullOrEmpty(ext.Customfield5Name))
            {
                req.AddCustomField(ext.Customfield5Name, WebApp.ObjectTranslator.MergeTags(ext.Customfield5Value));
            }

            if (followthrough)
            {
                KxData newdata = new KxData();

                newdata.ExecuteAuditLog("Outbound Payment", "PrePost", req.ToString());

                Kx.Core.PaymentProcessor.PaymentProcessor pp = new Kx.Core.PaymentProcessor.PaymentProcessor();

                pp.PostPayment(req);
            }
            return req;
        }

        public void PostPayment(WorkflowPaymentExtender ext)
        {
            PostPayment(ext, true);
        }

        /// <summary>
        /// Main outbound payment mechanism from contract pages, messy code! handles various payment providers
        /// </summary>
        /// <param name="requester">Payment extender</param>
        /// <param name="followthrough"></param>
        /// <returns>Payment request to post</returns>
        public virtual PaymentRequest PostPayment(WorkflowPaymentExtender requester, bool followthrough)
        {

            PaymentRequest pr = new PaymentRequest();

            pr.UniqueIdentifier = Guid.NewGuid();


            if (requester.IsExtendedWPMProvider)
            {
                if (requester.MonthlyPaymentOption)
                {
                    pr.InstallmentCount = WebUser.PendingAllocation.Schedule.NumberOfMonthlyPayments;
                    pr.StartDate = WebUser.PendingAllocation.Schedule.ActualArrivalDate;
                    pr.EndDate = WebUser.PendingAllocation.Schedule.ActualDepartureDate;
                    pr.Amount = WebUser.PendingAllocation.Schedule.TotalFees;
                    pr.FirstDate = WebUser.PendingAllocation.Schedule.GetMonthlyPayment(1).PaymentDate;
                }
                else if (requester.SemesterPaymentOption)
                {
                    pr.InstallmentCount = WebUser.PendingAllocation.Schedule.NumberOfSemesterPayments;
                    pr.StartDate = WebUser.PendingAllocation.Schedule.ActualArrivalDate;
                    pr.EndDate = WebUser.PendingAllocation.Schedule.ActualDepartureDate;
                    pr.Amount = WebUser.PendingAllocation.Schedule.TotalFees;
                    pr.FirstDate = WebUser.PendingAllocation.Schedule.GetSemesterPayment(1).PaymentDate;
                }

                pr.RecurringType = requester.ExtendedPaymentType == "RC";
                pr.DirectDebitType = requester.ExtendedPaymentType == "DD";

                AddStudentInfo(ref pr, requester.SendAddressDetails);
            }

            // First switch case replaces lots of IFs
            switch (requester.PaymentProvider.ToUpper())
            {
                case "SECPAY":
                    pr.SecPayPassword = requester.SecPayPassword;
                    pr.SecPaySendEmail = requester.SecPaySendEmail;
                    pr.Description = requester.Description + "(***" + WebUser.Student.StudentId + "***)";
                    break;

                case "WPM":
                    pr.ProviderReference = "1";
                    pr.AcademicYearId = WebUser.CurrentAcademicYear;
                    try
                    {
                        pr.CustomValue1 = WebApp.ApplicationsManager.YearDescriptionVeryShort;
                    }
                    catch { }
                    break;
                case "WPLMIBERTY":
                    if (requester.MonthlyPaymentOption)
                    {
                        if (WebUser.CurrentApplication.PaymentScheduleTypeId != 0)
                        {
                            try
                            {
                                try
                                {
                                    pr.InstallmentCount = WebUser.CurrentApplication.ApplicationCharges.GetPaymentDates().Count;
                                    KxLogger.LogActivity(this, "InstallmentCount", pr.InstallmentCount.ToString(), "");
                                }
                                catch { }
                                try
                                {
                                    pr.PaymentDates = WebUser.CurrentApplication.ApplicationCharges.GetPaymentDateString(",");
                                    KxLogger.LogActivity(this, "PaymentDates", pr.PaymentDates, "");
                                }
                                catch { }
                                try
                                {
                                    pr.PaymentAmounts = WebUser.CurrentApplication.ApplicationCharges.GetPaymentAmounts(",");
                                    KxLogger.LogActivity(this, "PaymentAmounts", pr.PaymentAmounts, "");
                                }
                                catch { }
                            }
                            catch { }
                            //pr.InstallmentCount = 3;
                            //pr.PaymentDates = "01/09/2009,01/01/2010,01/05/2010";
                            //pr.PaymentAmounts = "100.00,200.00,300.00";
                        }
                        else
                        {
                            pr.InstallmentCount = 1;
                            pr.PaymentDates = WebUser.CurrentApplication.ApplicationCharges.GetFirstPaymentDateString();
                            pr.PaymentAmounts = Math.Round(WebUser.CurrentApplication.ApplicationCharges.GetTotalCharges(), 2).ToString();

                            KxLogger.LogActivity(this, "PaymentDates", pr.PaymentDates, "");
                            KxLogger.LogActivity(this, "PaymentAmounts", pr.PaymentAmounts, "");

                        }
                        if (WebUser.CurrentApplication.PaymentScheduleTypeId == 1 && KxDbConfiguration.GetConfiguration("ApplicationId").ToUpper() == "COSMOPOLITAN")
                        {

                            pr.InstallmentCount = 1;

                            if (WebUser.HasPendingAllocation)
                            {
                                pr.PaymentDates = WebUser.PendingAllocation.StudentAllocation.AllocationStartDate.ToString("dd/MM/yyyy");
                                pr.PaymentAmounts = Math.Round(WebUser.PendingAllocation.StudentAllocation.AllocationFees, 2).ToString();
                            }
                            else
                            {
                                pr.PaymentDates = WebUser.AcceptedAllocation.StudentAllocation.AllocationStartDate.ToString("dd/MM/yyyy");
                                pr.PaymentAmounts = Math.Round(WebUser.AcceptedAllocation.StudentAllocation.AllocationFees, 2).ToString();
                            }
                        }
                    }
                    if (WebUser.HasPendingAllocation)
                    {
                        pr.SiteId = WebUser.CurrentApplication.FirstPreference.SiteId;
                    }
                    else
                    {
                        pr.SiteId = WebUser.AcceptedAllocation.StudentAllocation.ResidentialRoom.Block.SiteId;
                    }
                    pr.Title = WebUser.Student.Title;
                    pr.ProviderStudentId = WebUser.KxStudentId.ToString();
                    pr.StudentId = WebUser.Student.StudentId.ToString();
                    pr.FirstName = WebUser.ForeName;
                    pr.Surname = WebUser.SurName;
                    pr.Address1 = WebUser.CurrentApplication.HomeStudentAddress.Address1;
                    pr.Address2 = WebUser.CurrentApplication.HomeStudentAddress.Address2;
                    pr.Town = WebUser.CurrentApplication.HomeStudentAddress.Address3;
                    pr.Country = WebUser.CurrentApplication.HomeStudentAddress.Address4;
                    pr.Postcode = WebUser.CurrentApplication.HomeStudentAddress.Postcode;
                    pr.Telephone = WebUser.CurrentApplication.HomeStudentAddress.TelephoneNumber;
                    pr.Fax = WebUser.CurrentApplication.HomeStudentAddress.FaxNumber;
                    pr.Amount = Convert.ToDecimal(WebApp.ObjectTranslator.MergeTags(requester.Amount.ToString()));
                    break;
                case "WPMMANCHESTER":
                    pr.InstallmentCount = Convert.ToInt32(WebApp.ObjectTranslator.MergeTags(requester.PaymentMethod));
                    pr.ProviderStudentId = WebUser.KxStudentId.ToString();
                    pr.StudentId = WebUser.Student.StudentId.ToString();
                    requester.PaymentProvider = "WPM3WAY";
                    pr.DirectDebitType = true;
                    pr.FirstName = WebUser.ForeName;
                    pr.Surname = WebUser.SurName;
                    pr.Email = WebUser.EmailAddress;
                    pr.Address1 = WebUser.CurrentApplication.HomeStudentAddress.Address1;
                    pr.Address2 = WebUser.CurrentApplication.HomeStudentAddress.Address2;
                    pr.Town = WebUser.CurrentApplication.HomeStudentAddress.Address3;
                    pr.Country = WebUser.CurrentApplication.HomeStudentAddress.Address4;
                    pr.Postcode = WebUser.CurrentApplication.HomeStudentAddress.Postcode;
                    break;
                default:
                    try
                    {
                        pr.PaymentAmounts = WebApp.ObjectTranslator.MergeTags(requester.Amount2.ToString());
                    }
                    catch { }
                    pr.ProviderAccountId = WebApp.ObjectTranslator.MergeTags(requester.ProviderAccountId);
                    pr.Description = WebApp.ObjectTranslator.MergeTags(requester.Description);
                    pr.Currency = requester.Currency;
                    try
                    {
                        pr.Amount = Convert.ToDecimal(WebApp.ObjectTranslator.MergeTags(requester.Amount.ToString()));
                    }
                    catch (FormatException) { pr.Amount = 0.00M; }
                    AddStudentInfo(ref pr, requester.SendAddressDetails);
                    break;
            }

            try
            {
                pr.CustomValue1 = WebApp.ApplicationsManager.YearDescriptionVeryShort;
            }
            catch { }


            if (requester.PaymentProvider.ToUpper() == "SAGEPAY")
            {
                pr.ClientEmail = requester.ClientEmail;
                pr.MerchantDisplayName = requester.MerchantDisplayName;
                pr.Email = WebUser.EmailAddress;

                if (WebUser.CurrentApplication.PreferredEmailAddress != null
                    && !String.IsNullOrEmpty(WebUser.CurrentApplication.PreferredEmailAddress.ContactName)
                    && !String.IsNullOrEmpty(WebUser.CurrentApplication.PreferredEmailAddress.Address1)
                    && !String.IsNullOrEmpty(WebUser.CurrentApplication.PreferredEmailAddress.Address2)
                    && !String.IsNullOrEmpty(WebUser.CurrentApplication.PreferredEmailAddress.Address3)
                    && !String.IsNullOrEmpty(WebUser.CurrentApplication.PreferredEmailAddress.Address4)
                    && !String.IsNullOrEmpty(WebUser.CurrentApplication.PreferredEmailAddress.Postcode)
                    && !String.IsNullOrEmpty(WebUser.CurrentApplication.PreferredEmailAddress.TelephoneNumber)
                    )
                {
                    String[] names = WebUser.CurrentApplication.PreferredEmailAddress.ContactName.Split(' ');

                    pr.FirstName = names[0];
                    pr.Surname = names[names.Length - 1];

                    pr.Address1 = WebUser.CurrentApplication.PreferredEmailAddress.Address1;
                    pr.Address2 = WebUser.CurrentApplication.PreferredEmailAddress.Address2;
                    pr.Town = WebUser.CurrentApplication.PreferredEmailAddress.Address3;
                    pr.Country = WebUser.CurrentApplication.PreferredEmailAddress.Address4;
                    pr.Postcode = WebUser.CurrentApplication.PreferredEmailAddress.Postcode;
                    pr.Telephone = WebUser.CurrentApplication.PreferredEmailAddress.TelephoneNumber;
                }
                else
                {


                    pr.FirstName = WebUser.CurrentApplication.ForeName;
                    pr.Surname = WebUser.CurrentApplication.SurName;

                    pr.Address1 = WebUser.CurrentApplication.HomeStudentAddress.Address1;
                    pr.Address2 = WebUser.CurrentApplication.HomeStudentAddress.Address2;
                    pr.Town = WebUser.CurrentApplication.HomeStudentAddress.Address3;
                    pr.Country = WebUser.CurrentApplication.HomeStudentAddress.Address4;
                    pr.Postcode = WebUser.CurrentApplication.HomeStudentAddress.Postcode;
                    pr.Telephone = WebUser.CurrentApplication.HomeStudentAddress.TelephoneNumber;
                }

            }


            pr.ProviderPassword = requester.ProviderPassword;
            pr.PaymentType = requester.PaymentType;
            pr.TestMode = requester.TestMode;
            pr.TestTransactionOutcome = requester.TestTransactionOutcome;
            pr.ProviderUrl = WebApp.ObjectTranslator.MergeTags(requester.ProviderUrl);
            pr.PaymentMethod = WebApp.ObjectTranslator.MergeTags(requester.PaymentMethod);
            pr.BookingReference = WebApp.WorkflowInterface.WorkflowGuid.ToString();
            pr.ApplicationId = WebUser.CurrentApplication.ApplicationId.ToString();
            pr.ApplicationGuid = WebUser.CurrentApplication.ApplicationGuid;
            pr.Email = WebUser.EmailAddress;

            if (!string.IsNullOrWhiteSpace(WebApp.ObjectTranslator.MergeTags(requester.CustomValue1.ToString())))
            {
                pr.CustomValue1 = WebApp.ObjectTranslator.MergeTags(requester.CustomValue1.ToString());
            }

            if (!string.IsNullOrWhiteSpace(WebApp.ObjectTranslator.MergeTags(requester.CustomValue2.ToString())))
            {
                pr.CustomValue2 = WebApp.ObjectTranslator.MergeTags(requester.CustomValue2.ToString());
            }


            try
            {
                if (WebUser.Student != null)
                {
                    pr.StudentId = WebUser.Student.StudentId;
                }
            }
            catch { }


            try
            {
                if (string.IsNullOrEmpty(pr.StudentId) && WebUser.CurrentApplication != null && !string.IsNullOrEmpty(WebUser.CurrentApplication.StudentIdentifier))
                {
                    pr.StudentId = WebUser.CurrentApplication.StudentIdentifier;
                }
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "PostPayment", ex.Message, "");
            }


            pr.SiteCode = WebApp.ObjectTranslator.MergeTags(requester.SiteCode);

            pr.PaymentProvider = requester.PaymentProvider;
            pr.ProviderCallbackUrl = GetPaymentUrl(requester.ProviderCallbackUrl);
            pr.ConfirmationPageUrl = GetPaymentUrl(requester.ConfirmationPageUrl);
            pr.CancellationPageUrl = GetPaymentUrl(requester.CancellationPageUrl);
            pr.NominalCode = requester.NominalCode;
            pr.PaymentPageID = requester.PaymentPageID;
            pr.SH1Key = requester.SH1Key;
            pr.CardPaymentMethod = WebUser.CurrentApplication.PaymentMethod;

            // Second Switch case due to concerns about ordering in this mess of a method
            switch (requester.PaymentProvider.ToUpper())
            {
                case "ERA":
                    HttpContext.Current.Session["Resume" + requester.EraReturnCode] = null;
                    pr.EraDepartmentId = WebApp.ObjectTranslator.MergeTags(requester.EraDepartmentId);
                    pr.EraContactId = WebApp.ObjectTranslator.MergeTags(requester.EraContactId);
                    pr.EraCustomerId = WebApp.ObjectTranslator.MergeTags(requester.EraCustomerId);
                    pr.EraItemList = WebApp.ObjectTranslator.MergeTags(requester.EraItemList);
                    pr.Amount = Decimal.Round(pr.Amount, 2);
                    break;
                case "LANCASTERDD":
                    pr.CustomValue1 = Student.KxStudentId.ToString();
                    pr.CustomValue2 = Student.PersonId.ToString();
                    pr.CustomValue3 = Student.HomeStudentAddressId.ToString();
                    pr.CustomValue4 = Student.CurrentStudentAddressId.ToString();
                    pr.CustomValue5 = Student.EthnicityId.ToString();
                    pr.CustomValue6 = Student.UcasNumber;
                    break;
                case "BCSP":
                    KxAcademicYear thisYear = new KxAcademicYear(null, WebUser.CurrentApplication.AcademicYearId);
                    DateTime dtStartDate = thisYear.StartDate ?? DateTime.MinValue;
                    pr.CustomValue1 = requester.CustomValue1;
                    pr.CustomValue2 = requester.CustomValue2;
                    pr.CustomValue3 = dtStartDate.ToString("yyyy");
                    break;
                case "WORLDPAYCORPORATE":
                    pr.MerchantDisplayName = requester.MerchantDisplayName;
                    pr.Country = WebUser.CurrentApplication.CurrentStudentAddress.Address4;
                    pr.Currency = WebApp.ObjectTranslator.MergeTags(requester.Currency);
                    break;
                case "KXPAYMENT":
                    pr.Country = WebUser.CurrentApplication.CurrentStudentAddress.Address4;
                    pr.Currency = WebApp.ObjectTranslator.MergeTags(requester.Currency);
                    pr.MerchantCountry = WebApp.ObjectTranslator.MergeTags(requester.MerchantCountry);

                    bool isDDPayment = requester.PaymentType.Contains("DD");
                    bool isRCPPayment = requester.PaymentType.Contains("RCP");

                    if (isDDPayment || isRCPPayment)
                    {
                        Payment pay = new Payment()
                        {
                            Description = requester.Description,
                            PayType = isDDPayment ? KxWpmPaymentType.DD : KxWpmPaymentType.RCP
                        };

                        string instalmentdates = "";
                        string instalmentamounts = "";

                        if (requester.InstalmentMethod != "PAYMENTSCHEDULE")
                        {
                            instalmentamounts = WebApp.ObjectTranslator.MergeTags(requester.InstalmentAmounts);
                            instalmentdates = WebApp.ObjectTranslator.MergeTags(requester.InstalmentDates);
                        }
                        else
                        {
                            if (WebUser.CurrentApplication.PaymentScheduleTypeId != 0)
                            {
                                instalmentdates = WebUser.CurrentApplication.ApplicationCharges.GetPaymentDateString(",");
                                instalmentamounts = WebUser.CurrentApplication.ApplicationCharges.GetPaymentAmounts(",");
                            }
                            else
                            {
                                instalmentdates = WebUser.CurrentApplication.ApplicationCharges.GetFirstPaymentDateString();
                                instalmentamounts = Math.Round(WebUser.CurrentApplication.ApplicationCharges.GetTotalCharges(), 2).ToString();
                            }
                        }

                        string[] dates = instalmentdates.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                        string[] amounts = instalmentamounts.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                        if (dates.Length > 0 && dates.Length == amounts.Length)
                        {
                            for (int i = 0; i < dates.Length; i++)
                            {
                                var pi = new PaymentInstalment
                                {
                                    Amount = decimal.Parse(amounts[i]),
                                    InstalmentDate = DateTime.Parse(dates[i]),
                                    Description = requester.Description
                                };

                                pay.Instalments.Add(pi);
                            }
                        }
                        else
                        {
                            KxLogger.LogActivity(this, "PostPayment", "instalment dates not greater than zero or not matching amounts length, check merge tag", $"Instalment dates: {dates.ToString()}, Instalment amounts: {amounts.ToString()}");
                        }

                        pr.Payments.Add(pay);
             
                    }
                    break;
                case "REALEX":
                    pr.Email = requester.CustomerEmail;
                    pr.Telephone = requester.CustomerTelephone;
                    pr.Address1 = requester.BillingStreet1;
                    pr.Address2 = requester.BillingStreet2;
                    pr.County = requester.BillingStreet3;
                    pr.Town = requester.BillingCity;
                    pr.Postcode = requester.BillingPostcode;
                    pr.CountryCode = requester.BillingCountryCode;
                    pr.Country = requester.BillingCountry;
                    break;
            }


            if (requester.BillingAddressRequired)
            {
                pr.Email = requester.CustomerEmail;
                pr.Telephone = requester.CustomerTelephone;
                pr.Address1 = requester.BillingStreet1;
                pr.Address2 = requester.BillingStreet2;
                pr.County = requester.BillingStreet3;
                pr.Town = requester.BillingCity;
                pr.Postcode = requester.BillingPostcode;
                pr.CountryCode = requester.BillingCountryCode;
                pr.Country = requester.BillingCountry;
            }

            if (requester.PaymentProvider == "PAY360")
            {
                pr.Amount = Convert.ToDecimal(WebApp.ObjectTranslator.MergeTags(requester.Amount.ToString()));
                pr.SiteId = requester.SiteId;
                pr.SCPId = requester.SCPId;
                pr.HMACKeyId = requester.HMACKeyId;
                pr.HMACKey = requester.HMACKey;

            }

            if (followthrough)
            {
                KxData newdata = new KxData();

                newdata.ExecuteAuditLog("Outbound Payment", "PrePost", pr.ToString());

                Kx.Core.PaymentProcessor.PaymentProcessor pp = new Kx.Core.PaymentProcessor.PaymentProcessor();

                pp.PostPayment(pr);

            }

            return pr;
        }

        public void ProcessPayment()
        {
            #region Audit

            string auditDetails = "payResponse: " + (payResponse == null).ToString();
            if (payResponse.Authorised) auditDetails += ", Authorised: " + payResponse.Authorised.ToString();
            if (payResponse.Processed) auditDetails += ", Processed: " + payResponse.Processed.ToString();
            if ((payResponse.OriginalRequest != null) && (payResponse.OriginalRequest.PaymentProvider != null)) auditDetails += ", PaymentProvider: " + payResponse.OriginalRequest.PaymentProvider;
            if (payResponse.ProviderName != null) auditDetails += ", ProviderName: " + payResponse.ProviderName;

            KxData auditData = new KxData();
            auditData.ExecuteAuditLog("Kx.Core.Web.Student.WebApplication.ProcessManagers.KxContractsManager", "ProcessPayment()", auditDetails);

            #endregion

            if (payResponse != null && payResponse.Authorised && !payResponse.Processed)
            {
                if (payResponse.OriginalRequest.PaymentProvider == "WPMKX")
                {
                    bool processed = false;

                    foreach (Payment pay in payResponse.Payments)
                    {
                        if (pay.PayType == KxWpmPaymentType.DD || pay.PayType == KxWpmPaymentType.RCP)
                        {
                            processed = WebUser.TakePayment(pay.TransactionDate.Value, "IN:" + pay.TransactionReference, 0.0M, payResponse.ProviderName, KxPaymentType.Instalment);
                        }
                        else if (pay.PayType == KxWpmPaymentType.Deposit)
                        {
                            processed = WebUser.TakePayment(pay.TransactionDate.Value, "DEP:" + pay.TransactionReference, pay.Amount, payResponse.ProviderName, KxPaymentType.Deposit);
                        }
                        else if (pay.PayType == KxWpmPaymentType.PayNow)
                        {
                            processed = WebUser.TakePayment(pay.TransactionDate.Value, "PY:" + pay.TransactionReference, pay.Amount, payResponse.ProviderName, KxPaymentType.Payment);
                        }
                    }

                    payResponse.Processed = processed;
                }
                else if (payResponse.OriginalRequest.PaymentProvider == "WPM3WAY")
                {
                    if (payResponse.OriginalRequest.DirectDebitType)
                    {
                        // Direct Debit
                        payResponse.Processed = WebUser.TakePayment(((DateTime)payResponse.TransactionDate), payResponse.ProviderReference2, 0.0M, payResponse.ProviderName, KxPaymentType.Instalment);
                    }
                    else
                    {
                        bool processed = true;

                        if (payResponse.Amount > 0.00M)
                        {
                            //Deposit
                            processed = WebUser.TakePayment(((DateTime)payResponse.TransactionDate), payResponse.ProviderReference, payResponse.Amount, payResponse.ProviderName, KxPaymentType.Deposit);
                        }

                        if (processed && payResponse.Amount2 > 0.00M)
                        {
                            // Payment
                            processed = WebUser.TakePayment(((DateTime)payResponse.TransactionDate), payResponse.ProviderReference2, payResponse.Amount2, payResponse.ProviderName, KxPaymentType.Payment);
                        }

                        payResponse.Processed = processed;
                    }
                }
                else
                {
                    payResponse.Processed = WebUser.TakePayment(((DateTime)payResponse.TransactionDate), payResponse.ProviderReference, payResponse.Amount, payResponse.ProviderName, KxWebStudentPayment.PaymentTypeFromString(payResponse.OriginalRequest.PaymentType));

                    if (payResponse.Processed && !string.IsNullOrEmpty(payResponse.ProviderReference2))
                    {
                        payResponse.Processed = WebUser.TakePayment(((DateTime)payResponse.TransactionDate), payResponse.ProviderReference2, 0.0M, payResponse.ProviderName, KxPaymentType.Instalment);
                    }

                    if (payResponse.SurchargeApplied == true)
                    {
                        payResponse.Processed = WebUser.TakePayment(((DateTime)payResponse.TransactionDate), payResponse.ProviderReference + "CC", payResponse.Surcharge, payResponse.ProviderName, KxPaymentType.Surcharge);
                    }
                }
            }
            else
            {
                if (payResponse == null)
                {
                    KxLogger.LogActivity(this, "ProcessPayment - Null", KxStringUtils.GetObjectValue(HttpContext.Current.Session["Workflow"]), "");
                }
                else if (!payResponse.Authorised)
                {
                    KxLogger.LogActivity(this, "ProcessPayment - Not Authorised", KxStringUtils.GetObjectValue(HttpContext.Current.Session["Workflow"]), "");
                }
                else if (payResponse.Processed)
                {
                    KxLogger.LogActivity(this, "ProcessPayment - Already processed", KxStringUtils.GetObjectValue(HttpContext.Current.Session["Workflow"]), "");
                }
            }

            if (payResponse.Processed && payResponse.OriginalRequest != null && payResponse.OriginalRequest.PaymentType == "STATEMENT")
            {
                WebApp.StatementManager.ProcessCharges(payResponse);
            }
        }

        private void AddStudentInfo(ref PaymentRequest pr, bool SendAddressDetails)
        {
            try
            {
                pr.Title = WebUser.Student.Title;
                pr.ProviderStudentId = WebUser.Student.StudentId;
                pr.StudentId = WebUser.Student.StudentId.ToString();
                pr.UcasNumber = WebUser.Student.UcasNumber;
                pr.Dob = WebUser.Student.Dob;
            }
            catch { }
            pr.FirstName = WebUser.ForeName;
            pr.Surname = WebUser.SurName;

            if (SendAddressDetails)
            {
                pr.Address1 = WebUser.CurrentApplication.CurrentStudentAddress.Address1;
                pr.Address2 = WebUser.CurrentApplication.CurrentStudentAddress.Address2;

                if (WebUser.CurrentApplication.CurrentStudentAddress.Address3 == "" && WebUser.CurrentApplication.CurrentStudentAddress.Address4 != "")
                {
                    pr.Town = WebUser.CurrentApplication.CurrentStudentAddress.Address4;
                }
                else
                {
                    pr.Town = WebUser.CurrentApplication.CurrentStudentAddress.Address3;
                    pr.County = WebUser.CurrentApplication.CurrentStudentAddress.Address4;
                }
                // pr.Country = WebUser.CurrentApplication.CurrentStudentAddress.Country;
                pr.Postcode = WebUser.CurrentApplication.CurrentStudentAddress.Postcode;
                pr.Telephone = WebUser.CurrentApplication.CurrentStudentAddress.TelephoneNumber;
                pr.Fax = WebUser.CurrentApplication.CurrentStudentAddress.FaxNumber;
            }
            try
            {
                if (!String.IsNullOrEmpty(WebUser.EmailAddress))
                {
                    pr.Email = WebUser.EmailAddress;
                }
                else if (!String.IsNullOrEmpty(WebUser.Student.PreferredEmailAddress.EmailAddress))
                {
                    pr.Email = WebUser.Student.PreferredEmailAddress.EmailAddress;
                }
                else if (!String.IsNullOrEmpty(WebUser.Student.BestAddress.EmailAddress))
                {
                    pr.Email = WebUser.Student.BestAddress.EmailAddress;
                }
            }
            catch
            {

            }
        }

        private string GetPaymentUrl(string PaymentUrl)
        {
            bool Append = false;
            try
            {
                Uri uri = new Uri(PaymentUrl);
                Append = String.IsNullOrEmpty(uri.Host);

            }
            catch
            {
                Append = true;
            }

            if (Append)
            {
                return WebApp.ConfigurationManager.HomePageUrl.ToLower().Replace("default.aspx", PaymentUrl);
            }
            else
            {
                return PaymentUrl;
            }
        }

        public string PostWebServicePayment(WorkflowPaymentExtender requester)
        {
            if (requester is WorkflowSwanseaPayExtender)
            {
                var details = (WorkflowSwanseaPayExtender)requester;
                var pr = new PaymentRequest();

                pr.PaymentType = details.PaymentType;
                pr.TestMode = details.TestMode;
                pr.TestTransactionOutcome = details.TestTransactionOutcome;
                pr.ProviderUrl = WebApp.ObjectTranslator.MergeTags(details.ProviderUrl);
                pr.PaymentMethod = details.PaymentMethod;
                pr.BookingReference = WebApp.WorkflowInterface.WorkflowGuid.ToString();
                pr.ProviderAccountId = details.ProviderAccountId;
                pr.PaymentProvider = details.PaymentProvider;
                pr.ProviderCallbackUrl = GetPaymentUrl(details.ProviderCallbackUrl);
                pr.ConfirmationPageUrl = GetPaymentUrl(details.ConfirmationPageUrl);
                pr.CancellationPageUrl = GetPaymentUrl(details.CancellationPageUrl);
                pr.Description = details.Description;

                pr.Amount = Convert.ToDecimal(WebApp.ObjectTranslator.MergeTags(requester.Amount));

                pr.Address1 = details.UiPayerStreet1;
                pr.Address2 = details.UiPayerStreet2;
                pr.County = details.UiPayerStreet3;
                pr.Town = details.UiPayerCityTown;
                pr.Country = details.UiPayerCountry;
                pr.Postcode = details.UiPayerPostcode;
                pr.Telephone = string.Format("{0}|{1}", details.UiPayerPhoneCountryCode.Replace("-", ""), details.UiTelephone);

                pr.Email = details.UiEmail;
                pr.LedgerCode = details.LedgerCode;
                pr.NominalCode = details.NominalCode;
                pr.SystemId = details.SystemId;
                pr.ProviderAccountId = details.ProviderAccountId;

                if (WebUser?.Student != null && !string.IsNullOrWhiteSpace(WebUser.Student.StudentId))
                {
                    pr.ProviderStudentId = pr.StudentId = pr.CustomerId = WebUser.Student.StudentId;
                    pr.FirstName = WebUser.Student.ForeName;
                    pr.Surname = WebUser.Student.SurName;
                    pr.ApplicationGuid = WebUser.CurrentApplication.ApplicationGuid;
                    pr.ApplicationId = WebUser.CurrentApplication.ApplicationId.ToString();
                }
                else
                {
                    return KxLocalisationManager.GetSubstitution("An error has occurred, please close your browser and try again.");
                }

                Kx.Core.PaymentProcessor.PaymentProcessor pp = new Kx.Core.PaymentProcessor.PaymentProcessor();

                try
                {
                    pp.PostPayment(pr);
                }
                catch (Exception)
                { }

                return "";
            }

            return "";
        }

        public string PostWSPayment(WorkflowCardPaymentExtender requester)
        {
            PaymentRequest pr = new PaymentRequest();

            pr.UniqueIdentifier = Guid.NewGuid();

            pr.Address1 = requester.UiPayerStreet1;
            pr.Address2 = requester.UiPayerStreet2;
            pr.County = requester.UiPayerCounty;
            pr.Postcode = requester.UiPayerPostcode;
            pr.Town = requester.UiPayerCity;

            pr.Amount = Convert.ToDecimal(WebApp.ObjectTranslator.MergeTags(requester.Amount.ToString()));

            pr.PaymentType = requester.PaymentType;
            pr.TestMode = requester.TestMode;
            pr.TestTransactionOutcome = requester.TestTransactionOutcome;
            pr.ProviderUrl = WebApp.ObjectTranslator.MergeTags(requester.ProviderUrl);
            pr.PaymentMethod = requester.PaymentMethod;
            pr.BookingReference = WebApp.WorkflowInterface.WorkflowGuid.ToString();
            pr.ProviderAccountId = requester.ProviderAccountId;
            pr.PaymentProvider = requester.PaymentProvider;
            pr.ProviderCallbackUrl = GetPaymentUrl(requester.ProviderCallbackUrl);
            pr.ConfirmationPageUrl = GetPaymentUrl(requester.ConfirmationPageUrl);
            pr.CancellationPageUrl = GetPaymentUrl(requester.CancellationPageUrl);
            pr.SH1Key = requester.SH1Key;
            pr.PaymentMethod = requester.PaymentMethod;

            pr.Email = requester.UiPayerEmail;
            pr.ProviderAccountId = requester.ProviderAccountId;
            pr.LedgerCode = requester.LedgerCode;
            pr.NominalCode = requester.NominalCode;
            pr.SystemId = requester.SystemId;

            if (WebUser != null && WebUser.Student != null & !string.IsNullOrWhiteSpace(WebUser.Student.StudentId))
            {
                pr.ProviderStudentId = WebUser.Student.StudentId;
                pr.StudentId = WebUser.Student.StudentId.ToString();
            }
            else
            {
                return KxLocalisationManager.GetSubstitution("An error has occurred, please close your browser and try again.");
            }

            pr.CardholderName = requester.UiCardHolder;
            pr.CardNumber = requester.UiCardNumber;
            pr.CardType = requester.UiCardType;
            pr.ExpiryDate = requester.UiExpiryMonth + "/" + requester.UiExpiryYear;
            pr.CV2 = requester.UiCV2;
            pr.IssueNumber = requester.UiIssueNumber;

            if (requester.UiStartMonth != "" && requester.UiStartYear != "")
            {
                pr.ValidFrom = requester.UiStartMonth + "/" + requester.UiStartYear;
            }

            Kx.Core.PaymentProcessor.PaymentProcessor pp = new Kx.Core.PaymentProcessor.PaymentProcessor();


            try
            {
                payResponse = pp.PostPayment(pr);
            }
            catch (Exception)
            { }

            try
            {
                ProcessPayment();
            }
            catch (Exception)
            { }

            if (payResponse == null)
            {
                return KxLocalisationManager.GetSubstitution("An unknown error has occurred, please check your details and try again");
            }
            else
            {
                if (payResponse.Authorised)
                {
                    return "";
                }
                else if (payResponse.AuthorisationMessage != "")
                {
                    return payResponse.AuthorisationMessage;
                }
                else
                {
                    return KxLocalisationManager.GetSubstitution("An unknown error has occurred, please check your details and try again");
                }
            }

            // return payResponse.Authorised.ToString();         
        }

        public string ValidateDD(WorkflowCardPaymentExtender requester)
        {
            if (requester.UiPayerEmail.Trim().ToUpper() == requester.UiConfirmPayerEmail.Trim().ToUpper())
            {

                PaymentRequest pr = new PaymentRequest();

                pr.SortCode = requester.UiSortCode;
                pr.AccountId = requester.UiAccountNumber;
                pr.SystemId = requester.SystemId;
                pr.PaymentProvider = "SWANSEADDCHECK";

                Kx.Core.PaymentProcessor.PaymentProcessor pp = new Kx.Core.PaymentProcessor.PaymentProcessor();

                payResponse = pp.PostPayment(pr);

                if (payResponse == null)
                {
                    return KxLocalisationManager.GetSubstitution("An unknown error has occurred, please check your details and try again");
                }
                else
                {
                    if (payResponse.Authorised)
                    {
                        requester.UiBankName = payResponse.BankName;
                        requester.UiBranchName = payResponse.BranchName;
                        return "";
                    }
                    else if (payResponse.AuthorisationMessage != "")
                    {
                        return payResponse.AuthorisationMessage;
                    }
                    else
                    {
                        return KxLocalisationManager.GetSubstitution("An unknown error has occurred, please check your details and try again");
                    }
                }
            }
            else
            {
                return KxLocalisationManager.GetSubstitution("Email and confirmation emails must match");
            }
        }

        public string CreateDirectDebit(WorkflowCardPaymentExtender requester)
        {
            PaymentRequest pr = new PaymentRequest();

            pr.SortCode = requester.UiSortCode;
            pr.AccountId = requester.UiAccountNumber;
            pr.SystemId = requester.SystemId;

            pr.PaymentProvider = "SWANSEADD";

            pr.StudentId = WebUser.Student.StudentId;
            pr.ProviderAccountId = requester.LedgerCode;
            pr.BankName = requester.UiBankName;
            pr.BranchName = requester.UiBranchName;
            pr.AccountName = requester.UiAccountName;
            pr.Email = WebUser.EmailAddress;
            pr.PaymentType = "INSTALMENT";
            pr.Amount = 0.00M;

            Kx.Core.PaymentProcessor.PaymentProcessor pp = new Kx.Core.PaymentProcessor.PaymentProcessor();

            payResponse = pp.PostPayment(pr);

            try
            {
                ProcessPayment();
            }
            catch (Exception)
            { }


            if (payResponse == null)
            {
                return KxLocalisationManager.GetSubstitution("An unknown error has occurred, please check your details and try again");
            }
            else
            {
                if (payResponse.Authorised)
                {
                    return "";
                }
                else if (payResponse.AuthorisationMessage != "")
                {
                    return payResponse.AuthorisationMessage;
                }
                else
                {
                    return KxLocalisationManager.GetSubstitution("An unknown error has occurred, please check your details and try again");
                }
            }
        }

    }
}
