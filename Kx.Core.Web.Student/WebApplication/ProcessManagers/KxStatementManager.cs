using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Kx.Core.Configuration;
using Kx.Core.Data;
using Kx.Core.Exceptions;
using Kx.Core.Localisation;
using Kx.Core.Utility;
using Kx.Core.PaymentProcessor;
using Kx.Core.PaymentProcessor.Payments;
using Kx.Core.Web.Student.WebApplication.BaseClass;
using Kx.Core.Web.Student.WebApplication.PageRenderer;
using Kx.Core.Web.Student.WebApplication.WorkflowInterface.WorkflowExtenders;
using Kx.Objects.Student;
using Kx.Objects.Student.Web;
using Kx.Objects.Common;

namespace Kx.Core.Web.Student.WebApplication.ProcessManagers
{

    public class KxStatementManager : WebStudentBaseClass
    {
        protected string userStatementId;
        protected string controlStatementId;
        protected string ignoreChargeTypes = null;

        protected int currentLimit = 99;
        protected decimal totalCharges = 0.00M;
        protected decimal totalPayments = 0.00M;
        protected decimal outstandingBalance = 0.00M;
        protected decimal futureBalance = 0.00M;
        protected decimal depositBalance = 0.00M;
        protected decimal currentBalanceLessPayments = 0.00M;

        protected List<string> currentItems = null;
        protected List<string> futureItems = null;
        protected List<string> depositItems = null;
        protected List<string> payableCharges = null;
        protected List<string> currentPayableCharges = null;
        protected List<string> futurePayableCharges = null;

        bool loaded = false;

        public string IgnoreChargeTypes
        {
            get
            {
                if ((ignoreChargeTypes == null))
                {
                    ignoreChargeTypes = KxDbConfiguration.GetConfiguration("StudentChargeTypesToIgnore", "");
                    if (ignoreChargeTypes != String.Empty)
                    {
                        ignoreChargeTypes = String.Format(",{0},", ignoreChargeTypes);
                    }
                   
                }
                return ignoreChargeTypes;
            }
        }

        public List<string> CurrentItems
        {
            get
            {
                if (!loaded)
                {
                    SetupItems();
                }
                return currentItems;
            }
            set
            {
                currentItems = value;
            }
        }
        public List<string> FutureItems
        {
            get
            {
                if (!loaded)
                {
                    SetupItems();
                }
                return futureItems;
            }
            set
            {
                futureItems = value;
            }
        }
        public List<string> DepositItems
        {
            get
            {
                if (!loaded)
                {
                    SetupItems();
                }
                return depositItems;
            }
            set
            {
                depositItems = value;
            }
        }

        public Decimal SelectedBalance
        {
            get
            {
                return GetBalance(false);
            }
        }

        public Decimal ChargeableBalance
        {
            get
            {
                return GetBalance(true);
            }
        }


        public bool HasSelected
        {
            get
            {
                return GetHasSelected();
            }
        }

        public bool HasFutureSelected
        {
            get
            {
                return GetFutureHasSelected();
            }
        }
        
        public string[] PayableChargesArray
        {
            get
            {

                return PayableCharges.ToArray();
            }
        }

        public List<string> PayableCharges
        {
            get
            {
                if (!loaded)
                {
                    SetupItems();
                }
                return payableCharges;
            }
            set
            {
                payableCharges = value;
            }

        }

        public string CommaSeparatedSelectedCharges
        {
            get
            {
                var result = new List<string>();
                foreach (string chargeId in PayableCharges)
                {
                    KxStudentStatement stat = GetStudentStatement(chargeId);
                    if (stat.Selected)
                    {
                        result.Add(chargeId);
                    }
                }
                return string.Join(",", result);
            }
        }

        public string[] CurrentPayableChargesArray
        {
            get
            {
                return CurrentPayableCharges.ToArray();
            }
        }

        public List<string> CurrentPayableCharges
        {
            get
            {
                if (!loaded)
                {
                    SetupItems();
                }
                return currentPayableCharges;
            }
            set
            {
                currentPayableCharges = value;
            }
           
        }

        public string[] FuturePayableChargesArray
        {
            get
            {

                return FuturePayableCharges.ToArray();
            }
        }
        
        public List<string> FuturePayableCharges
        {
            get
            {
                if (!loaded)
                {
                    SetupItems();
                }
                return futurePayableCharges;
            }
            set
            {
                futurePayableCharges = value;
            }
        }

        public bool HasPayable
        {
            get
            {
                return PayableCharges.Count > 0;
            }
        }

        public bool HasCurrentPayable
        {
            get
            {
                return CurrentPayableCharges.Count > 0;
            }
        }

        public bool HasFuturePayable
        {
            get
            {
                return FuturePayableCharges.Count > 0;
            }
        }

        public string[] CurrentArray
        {
            get
            {
                try
                {
                    if (currentLimit > CurrentCount)
                    {
                        return CurrentItems.GetRange(0, CurrentCount).ToArray();
                    }
                    else
                    {
                        return CurrentItems.GetRange((CurrentCount) - (CurrentLimit + 1), currentLimit + 1).ToArray();
                    }
                }
                catch
                {
                    return CurrentItems.ToArray();
                }
            }
        }

        public string[] FutureArray
        {
            get
            {
                return FutureItems.ToArray();
            }
        }

        public string[] DepositArray
        {
            get
            {
                return DepositItems.ToArray();
            }
        }

        public int CurrentLimit
        {
            get
            {
                return currentLimit;
            }
            set
            {
                currentLimit = value;
            }
        }

        public int CurrentCount
        {
            get
            {
                return CurrentItems.Count;
            }
        }

        public decimal FutureBalance
        {
            get
            {
                if (!loaded)
                {
                    SetupItems();
                }
                return futureBalance;
            }
        }

        public decimal DepositBalance
        {
            get
            {
                if (!loaded)
                {
                    SetupItems();
                }
                return depositBalance;
            }
        }

        public decimal CurrentBalance
        {
            get
            {
                if (!loaded)
                {
                    SetupItems();
                }
                return outstandingBalance;
            }
        }

        public decimal CurrentBalanceLessPayments
        {
            get
            {
                if (!loaded)
                {
                    SetupItems();
                }
                return currentBalanceLessPayments;
            }
        }

        public decimal TotalCharges
        {
            get
            {
                if (!loaded)
                {
                    SetupItems();
                }
                return totalCharges;
            }
        }

        public decimal TotalPayments
        {
            get
            {
                if (!loaded)
                {
                    SetupItems();
                }
                return totalPayments;
            }
        }

        public KxStatementManager(KxWebApplicationStudent webApp)
            : base(webApp)
        {
            
        }

        private void SetupItems()
        {
            
            outstandingBalance = 0.00M;
            futureBalance = 0.00M;
            depositBalance = 0.00M;
            currentBalanceLessPayments = 0.00M;

            depositItems = new List<string>();
            currentItems = new List<string>();
            futureItems = new List<string>();
            currentPayableCharges = new List<string>();
            futurePayableCharges = new List<string>();
            payableCharges = new List<string>();

            try
            {
                totalCharges = WebUser.CurrentApplication.StandardApplication.StudentStatements.TotalCharges;
                totalPayments = WebUser.CurrentApplication.StandardApplication.StudentStatements.TotalPayments;

                foreach (KxStudentStatement ss in WebUser.CurrentApplication.StandardApplication.StudentStatements.Items)
                {
                    if (!IsExcluded(ss.StudentChargeTypeId))
                    {
                        if (ss.IsDeposit)
                        {
                            depositItems.Add(ss.UniqueId);

                            if (ss.IsCharge && ss.OutStandingBalance > 0.00M)
                            {
                                depositBalance += ss.OutStandingBalance;
                            }
                        }
                        else
                        {


                            if ((ss.DateDue ?? ss.DateFrom).Value > DateTime.Today.AddDays(1))
                            {
                                futureItems.Add(ss.UniqueId);

                                if (ss.IsCharge && ss.OutStandingBalance > 0.00M)
                                {
                                    futureBalance += ss.OutStandingBalance;
                                }

                                if (ss.IsPayable)
                                {
                                    payableCharges.Add(ss.UniqueId);
                                    futurePayableCharges.Add(ss.UniqueId);
                                }
                            }
                            else
                            {
                                currentItems.Add(ss.UniqueId);

                                if (ss.IsCharge && ss.OutStandingBalance > 0.00M)
                                {
                                    outstandingBalance += ss.OutStandingBalance;
                                }

                                currentBalanceLessPayments += ss.TotalAmount;

                                if (ss.IsPayable)
                                {
                                    payableCharges.Add(ss.UniqueId);
                                    currentPayableCharges.Add(ss.UniqueId);
                                }
                            }
                        }
                    }
                }
                loaded = true;
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "SetupItems", ex.Message, "");
            }
        }

        public KxStudentStatement GetStudentStatement(string id)
        {
            try
            {
                return WebUser.CurrentApplication.StandardApplication.StudentStatements.GetStudentStatement(id);
            }
            catch { return null; }
        }

        public void PostPayment(WorkflowPaymentExtender requester)
        {
            PaymentRequest request = WebApp.ContractManager.PostPayment(requester, false);
            ProcessPayment(request);
        }

        public void PostPayment(WorkflowWpmPaymentExtender requester)
        {
            PaymentRequest request = WebApp.ContractManager.PostPayment(requester, false);
            request.Description = requester.PaymentDescription;
            ProcessPayment(request);
        }

        protected void ProcessPayment(PaymentRequest request)
        {
            request.Amount = SelectedBalance;
  
            if (request.PaymentProvider == "WPMKX")
            {
                request.PaymentType = "PAYNOW";
            }

            foreach (string s in PayableCharges)
            {
                KxStudentStatement stat = GetStudentStatement(s);
                if (stat.Selected)
                {
                    KxConsolidatedPayment pay = new KxConsolidatedPayment(stat.StudentChargeId, stat.StatementText, stat.OutStandingBalance);
                    request.ConsolidatedPayments.Add(pay);
                }
            }

            if (request.ConsolidatedPayments.Count == 1)
            {
                request.Description = request.ConsolidatedPayments[0].StatementText;
            }
            else if (request.ConsolidatedPayments.Count > 1 && string.IsNullOrEmpty(request.Description))
            {
                request.Description = request.ConsolidatedPayments[0].StatementText;
            }

            KxData newdata = new KxData();

            newdata.ExecuteAuditLog("Outbound Payment", "PrePost", request.ToString());

            Kx.Core.PaymentProcessor.PaymentProcessor pp = new Kx.Core.PaymentProcessor.PaymentProcessor();

            pp.PostPayment(request);
        }

        public void ProcessCharges(PaymentResponse response)
        {
            KxWebStudentPayment pay = WebUser.CurrentApplication.Payments.GetPaymentByReference(response.ProviderReference);

            if (pay != null && response.Authorised && response.OriginalRequest != null)
            {
                KxWebStudentPaymentBatches batches = new KxWebStudentPaymentBatches();
                
                foreach (KxConsolidatedPayment cp in response.OriginalRequest.ConsolidatedPayments)
                {
                    KxWebStudentPaymentBatch batch = new KxWebStudentPaymentBatch(batches);
                    batch.OutstandingBalance = cp.Amount;
                    batch.StatementText = cp.StatementText;
                    batch.StudentChargeId = cp.StudentChargeId;
                    batch.WebPaymentId = pay.WebPaymentId;
                    batches.Items.Add(batch);
                }
                batches.Changed = false;
                batches.ChildChanged = true;
                if (batches.Save())
                {
                    if (KxDbConfiguration.GetConfigurationBool("UseNewStatementProcessPayment", false))
                    {
                        KxData data = new KxData("data", KxDbConfiguration.GetConfiguration("PaymentProcessToKxProcedure", "kcf.ProcessPaymentToKxForApplication"), TargetType.StoredProc, ActionType.ExecuteProcedure);
                        data.AddParameter("applicationguid", pay.ApplicationGuid, KxValueType.String, 50);
                        data.AddParameter("depositpaymentreasonid", KxDbConfiguration.GetConfigurationInt("DepositPaymentReasonID", 0), KxValueType.Int);
                        data.AddParameter("statementpaymentreasonid", KxDbConfiguration.GetConfigurationInt("StatementPaymentReasonID", 0), KxValueType.Int);
                        data.AddParameter("paynowpaymentreasonid", KxDbConfiguration.GetConfigurationInt("PayNowPaymentReasonID", 0), KxValueType.Int);
                        data.AddParameter("surchargepaymentreasonid", KxDbConfiguration.GetConfigurationInt("SurchargePaymentReasonID", 0), KxValueType.Int);
                        data.AddParameter("surchargechargetypeid", KxDbConfiguration.GetConfigurationInt("SurchargeChargeTypeID", 0), KxValueType.Int);
                        data.AddParameter("paymentmethodid", KxDbConfiguration.GetConfigurationInt("PaymentMethodID", 0), KxValueType.Int);
                        data.AddParameter("webpaymentid", pay.WebPaymentId, KxValueType.Int);
                        if (data.Execute())
                        {
                            Reset();
                        }
                        else
                        {
                            data.LogException("KxWebStudentApplication", "PaymentProcessToKx", data.LastError, "");
                        }
                    }
                    else
                    {
                        KxData data = new KxData("processpayments", "WebProcessConsolidatedPayments",
                            TargetType.StoredProc, ActionType.ExecuteProcedure);
                        data.AddParameter("WebPaymentId", pay.WebPaymentId, KxValueType.Int);
                        if (data.Execute())
                            Reset();
                    }

                }
                
            }
        }

        public void Reset()
        {
            try
            {
                loaded = false;
                WebUser.CurrentApplication.StandardApplication.StudentStatements.Items.Clear();
                WebUser.CurrentApplication.StandardApplication.StudentStatements = null;
                SetupItems();
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "Reset", ex.Message, ""); 
            }
        }

        private decimal GetBalance(bool all)
        {
            Decimal result = 0.00M;

            foreach (string s in PayableCharges)
            {
                KxStudentStatement stat = GetStudentStatement(s);
                if (all || stat.Selected)
                {
                    result += stat.OutStandingBalance;
                }
            }

            return result;
        }

        private bool GetHasSelected()
        {
            bool result = false;

            foreach (string s in PayableCharges)
            {
                KxStudentStatement stat = GetStudentStatement(s);
                if (stat.Selected)
                {
                    result = true;
                    break;
                }
            }

            return result;
        }

        private bool GetFutureHasSelected()
        {
            bool result = false;

            foreach (string s in FuturePayableCharges)
            {
                KxStudentStatement stat = GetStudentStatement(s);
                if (stat.Selected)
                {
                    result = true;
                    break;
                }
            }

            return result;
        }

        private bool IsExcluded (int ChargeTypeId)
        {
            return IgnoreChargeTypes.Contains(String.Format(",{0},", ChargeTypeId));

        }
    }
}
