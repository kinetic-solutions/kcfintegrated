﻿using Kx.Core.Web.Student.WebApplication.BaseClass;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kx.Objects.Student.Web;
using Kx.Objects.Student;

namespace Kx.Core.Web.Student.WebApplication.ProcessManagers
{
    public class KxStudentLoginManager : WebStudentBaseClass
    {
        public KxStudentLoginManager(KxWebApplicationStudent webApp)
            : base(webApp)
        {

        }

        #region Protected Methods
        public void LogInToWorkflow(KxWebApplicationStudent webApp, KxWebUserStudent webuser)
        {
            webApp.WorkflowInterface.WebUser.LoadGuid(webuser.UserGuid);
            webApp.WorkflowInterface.WebUser.LoggedIn = true;
        }

        public void CreateActiveWebUserRecordForSSOStudent(string userName, string emailAddress, string foreName, string surName, int kxStudentID, ref KxWebUserStudent webStudent)
        {
            webStudent = new KxWebUserStudent(null);
            webStudent.UserName = userName;
            webStudent.EmailAddress = emailAddress;
            webStudent.ForeName = foreName;
            webStudent.SurName = surName;
            webStudent.Active = 1;
            webStudent.CreationDate = DateTime.Now.Date;
            webStudent.EmailConfirmed = DateTime.Now.Date;
            webStudent.KxStudentId = kxStudentID;
            webStudent.Save();
        }

        public void UpdateApplicationFromStudentData(KxWebStudentApplication app, string title, string foreName, string surName, string address1, string address2, string address3, string address4, string postcode, string mobile, string telephone, string gender, DateTime? dob)
        {
            app.Title = title ?? app.Title;
            app.ForeName = foreName ?? app.ForeName;
            app.SurName = surName ?? app.SurName;
            app.CurrentStudentAddress.Address1 = address1 ?? app.CurrentStudentAddress.Address1;
            app.CurrentStudentAddress.Address2 = address2 ?? app.CurrentStudentAddress.Address2;
            app.CurrentStudentAddress.Address3 = address3 ?? app.CurrentStudentAddress.Address3;
            app.CurrentStudentAddress.Address4 = address4 ?? app.CurrentStudentAddress.Address4;
            app.CurrentStudentAddress.Postcode = postcode ?? app.CurrentStudentAddress.Postcode;
            app.CurrentStudentAddress.TelephoneNumber = telephone ?? app.CurrentStudentAddress.TelephoneNumber;
            app.Gender = gender ?? app.Gender;
            app.Dob = dob ?? app.Dob;
        }
        #endregion
    }
}
