﻿using System;
using System.Collections.Generic;
using System.Text;
using Kx.Core.Base;
using Kx.Core.Data;
using Kx.Core.Configuration;
using Kx.Core.Exceptions;
using Kx.Core.Utility;
using Kx.Objects.Student;
using Kx.Objects.Student.Web;

namespace Kx.Core.Web.Student.WebApplication.ProcessManagers
{
    public class KxImagoAllocationChargeCalculation : KxBaseClass
    {
        private Int32 _KxStudentId;
        private Int32 _ApplicationId;
        private Int32 _SiteId;
        private Int32 _AreaId;
        private Int32 _ResidentialRoomId;
        private Int32 _AcademicYearId;
        private KxPaymentScheduleType _PaymentScheduleType;
        private Int32 _PaymentScheduleTypeId;
        private KxStudentAllocationDates _AllocationDates;
        private KxAllocationModel _AllocationModel;
        private Int32 _PricingModelId;
        private KxPricingModel _PricingModel;

        private bool ProcessInstalmentCharges()                        
        {
            bool result = false;
            try
            {
                KxPaymentScheduleType paytype = _PaymentScheduleType;

                foreach (KxStudentChargeType type in GetChargeTypes())
                {
                    decimal totalcharges = GetChargesTotal(type.StudentChargeTypeId);
                    decimal appliedCharges = 0.0M;

                    foreach (KxPaymentScheduleTypeModel model in paytype.Models.Items)
                    {
                        if (model.FixedDate < new DateTime(1980, 1, 1) || model.FixedDate >= _AllocationDates.AllocationStartDate)
                        {
                            if (model.PaymentIntervalType.Trim() == "Percentage On Date")
                            {
                                model.AmountToCharge = 0;
                                model.AmountToCharge = Math.Round((totalcharges / 100.00m) * ((decimal)model.Percentage), 2);
                                model.NbrOfMonths = 0;

                                appliedCharges += model.AmountToCharge;
                            }
                            else if (model.PaymentIntervalType.Trim() == "Monthly")
                            {
                                model.NbrOfMonths = 0;
                                KxStudentAllocationDate amdate = _AllocationDates.GetFirstAllocation();

                                DateTime mydate = (DateTime)amdate.ArrivalDate;

                                bool monthok = true;

                                while (monthok)
                                {
                                    model.NbrOfMonths++;
                                    mydate = mydate.AddMonths(1);

                                    if (_AllocationDates.GetAllocationDate(mydate) == null)
                                    {
                                        monthok = false;
                                    }
                                }
                            }
                        }
                    }

                    if (totalcharges > appliedCharges)
                    {
                        paytype.Models[0].AmountToCharge += Math.Round((totalcharges - appliedCharges), 2);
                    }
                    else
                        if (totalcharges < appliedCharges)
                        {
                            paytype.Models[0].AmountToCharge += Math.Round((totalcharges - appliedCharges), 2);
                        }

                    int count = 1;

                    foreach (KxPaymentScheduleTypeModel model in paytype.Models.Items)
                    {
                        if (model.FixedDate < new DateTime(1980, 1, 1) || model.FixedDate >= _AllocationDates.AllocationStartDate)
                        {
                            KxStudentAllocationDate allocDate = null;

                            if (model.FixedDate.Value < KxDateTime.KxApplicableDate)
                            {
                                allocDate = _AllocationDates.GetFirstAllocation();
                            }
                            else
                            {
                                allocDate = _AllocationDates.GetAllocationDate(model.FixedDate.Value);
                            }


                            DateTime? fromDate;
                            DateTime? toDate = null;
                            int allocDateId = 0;

                            if (allocDate != null)
                            {
                                allocDateId = allocDate.AllocationDateId;
                            }
                            else
                            {
                                allocDateId = 0;
                            }

                            if (model.FixedDate < KxDateTime.KxApplicableDate)
                            {
                                fromDate = _AllocationDates.AllocationStartDate;
                            }
                            else
                            {
                                fromDate = model.FixedDate;
                            }

                            if (paytype.Models.Items.IndexOf(model) < paytype.Models.Items.Count - 1)
                            {
                                if (_PricingModel.ModelType == "Daily")
                                {
                                    for (int i = paytype.Models.Items.IndexOf(model) + 1; i < paytype.Models.Items.Count; i++)
                                    {
                                        if (paytype.Models.Items[(i)].FixedDate >= fromDate)
                                        {
                                            toDate = paytype.Models.Items[(i)].FixedDate.Value.AddDays(-1);
                                            break;
                                        }
                                    }
                                    if (toDate == null)
                                    {
                                        toDate = paytype.Models.Items[(paytype.Models.Items.IndexOf(model) + 1)].FixedDate.Value.AddDays(-1);
                                    }
                                }
                                else
                                {
                                    for (int i = paytype.Models.Items.IndexOf(model) + 1; i < paytype.Models.Items.Count; i++)
                                    {
                                        if (paytype.Models.Items[(i)].FixedDate >= fromDate)
                                        {
                                            toDate = paytype.Models.Items[(i)].FixedDate.Value;
                                            break;
                                        }
                                    }
                                    if (toDate == null)
                                    {
                                        toDate = paytype.Models.Items[(paytype.Models.Items.IndexOf(model) + 1)].FixedDate.Value;
                                    }
                                }
                            }
                            else if (allocDate != null)
                            {
                                toDate = allocDate.DepartureDate;
                            }
                            else
                            {
                                toDate = fromDate;
                            }

                            if (model.NbrOfMonths == 0)
                            {
                                AddStudentCharges(allocDateId, type, fromDate, toDate, model.AmountToCharge, count, fromDate);
                                count++;
                            }
                            else
                            {
                                decimal monthlypayment = Math.Round(totalcharges / model.NbrOfMonths, 2);
                                decimal firstpayment = Math.Round(totalcharges - (monthlypayment * (model.NbrOfMonths - 1)), 2);

                                DateTime paymentdate = (DateTime)fromDate;
                                if (paymentdate.Day != model.FixedDay)
                                {
                                    paymentdate = paymentdate.AddDays(model.FixedDay - paymentdate.Day);
                                }

                                for (int monthno = 1; monthno <= model.NbrOfMonths; monthno++)
                                {
                                    if (monthno == 1)
                                    {
                                        AddStudentCharges(allocDateId, type, paymentdate, paymentdate.AddMonths(1), firstpayment, count, fromDate);
                                    }
                                    else
                                    {
                                        AddStudentCharges(allocDateId, type, paymentdate, paymentdate.AddMonths(1), monthlypayment, count, fromDate);
                                    }
                                    paymentdate = paymentdate.AddMonths(1);
                                    count++;
                                }
                            }
                        }
                    }
                }
                Changed = false;
                result = Save();

            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "Process Instalments", ex.Message, "");
            }
            return result;
        }
        
        public bool ProcessCharges(Boolean checkInstalments,
                                    Boolean usePaymentSchedule,
                                    Int32 siteId,
                                    Int32 areaId,
                                    Int32 residentialRoomId,
                                    KxPaymentScheduleType paymentScheduleType,
                                    Boolean charge1Exists,
                                    Boolean charge2Exists,
                                    Boolean charge3Exists,
                                    Boolean charge4Exists,
                                    Int32 allocationId,
                                    DateTime actualArrivalDate,
                                    DateTime actualDepartureDate,
                                    KxStudentAllocationDates allocationDates,
                                    Decimal defaultChargeAmount,
                                    KxStudentChargeType studentCharge1,
                                    KxStudentChargeType studentCharge2,
                                    KxStudentChargeType studentCharge3,
                                    KxStudentChargeType studentCharge4,
                                    Int32 kxStudentId,
                                    Int32 applicationId,
                                    KxAllocationModel allocationModel
                                   )
        {
            bool result = false;
            _PaymentScheduleType = paymentScheduleType;
            _KxStudentId = kxStudentId;
            _ApplicationId = applicationId;
            _SiteId = siteId;
            _AreaId = areaId;
            _ResidentialRoomId = residentialRoomId;
            _AcademicYearId = allocationModel.AcademicYearId;
            _AllocationDates = allocationDates;
            _AllocationModel = allocationModel;

            if (checkInstalments && paymentScheduleType.PaymentScheduleTypeId > 0)
            {                
                return ProcessInstalmentCharges();                
            }
            else
                if (usePaymentSchedule && paymentScheduleType.PaymentScheduleTypeId == 0)
                {
                    bool chargesresult = false;

                    KxData data = new KxData("", "SitePaymentScheduleType", TargetType.StoredProc);

                    data.AddParameter("SiteID", siteId, KxValueType.String);

                    object dbresult = data.ExecuteScalar();

                    if (dbresult != null)
                    {
                        try
                        {
                            _PaymentScheduleTypeId = Convert.ToInt32(dbresult);
                            chargesresult = ProcessInstalmentCharges();
                        }
                        catch
                        { }
                    }
                    _PaymentScheduleTypeId = 0;

                    return chargesresult;
                }
                else
                {
                    try
                    {
                        //process charges for the allocation

                        if (!charge1Exists)
                        {
                            AddStudentCharges(-allocationId,
                                              studentCharge1,
                                              actualArrivalDate,
                                              actualDepartureDate,
                                              defaultChargeAmount,
                                              0,
                                              actualArrivalDate
                                              );
                        }

                        if (!charge2Exists)
                        {
                            AddStudentCharges(-allocationId,
                                              studentCharge2,
                                              actualArrivalDate,
                                              actualDepartureDate,
                                              defaultChargeAmount,
                                              0,
                                              actualArrivalDate
                                              );
                        }

                        if (!charge3Exists)
                        {
                            AddStudentCharges(-allocationId,
                                              studentCharge3,
                                              actualArrivalDate,
                                              actualDepartureDate,
                                              defaultChargeAmount,
                                              0,
                                              actualArrivalDate
                                              );
                        }

                        if (!charge4Exists)
                        {
                            AddStudentCharges(-allocationId,
                                              studentCharge4,
                                              actualArrivalDate,
                                              actualDepartureDate,
                                              defaultChargeAmount,
                                              0,
                                              actualArrivalDate
                                              );
                        }

                        TimeSpan span = actualDepartureDate.Subtract(actualArrivalDate);

                        decimal totalnoweeks = span.Days / 7;

                        if (KxDbConfiguration.GetConfiguration("WeeklyBasedCharges") != "")
                        {
                            KxStudentAllocationDate keepallocdate = null;
                            KxPricingModelCharge priceCharge = null;

                            foreach (KxStudentAllocationDate allocdate in allocationDates.Items)
                            {
                                KxAllocationModelDate matchingDate = MatchingModelDate(allocdate);

                                if (matchingDate != null && keepallocdate == null)
                                {
                                    keepallocdate = allocdate;

                                    priceCharge = _PricingModel.GetChargeByType(matchingDate.StudentChargeId1, allocationModel.AcademicYearId);
                                }

                            }

                            if (keepallocdate != null && priceCharge != null)
                            {
                                decimal totalamount = totalnoweeks * priceCharge.WeeklyAmount;
                                KxAllocationModelDate chargematchingDate = MatchingModelDate(keepallocdate);

                                try
                                {
                                    if (KxDbConfiguration.GetConfiguration("SinglePaymentDiscount") != "")
                                    {
                                        decimal discpercent = KxDbConfiguration.GetConfigurationDecimal("SinglePaymentDiscount", 0.00M);
                                        totalamount = totalamount * (1 - (discpercent / 100));
                                    }
                                }
                                catch
                                {
                                    // No single payment discount??
                                }

                                AddStudentCharges(keepallocdate.AllocationDateId, chargematchingDate.StudentCharge1, actualArrivalDate, actualDepartureDate, totalamount, 0, actualArrivalDate);
                            }
                        }
                        else
                        {
                            foreach (KxStudentAllocationDate allocdate in allocationDates.Items)
                            {
                                KxAllocationModelDate matchingDate = MatchingModelDate(allocdate);

                                if (matchingDate != null)
                                {
                                    if (matchingDate.StudentCharge1 != null)
                                    {
                                        ProcessAllocationDate(matchingDate.StudentCharge1, allocdate, matchingDate, true);
                                    }

                                    if (matchingDate.StudentCharge2 != null)
                                    {
                                        ProcessAllocationDate(matchingDate.StudentCharge2, allocdate, matchingDate, true);
                                    }

                                    if (matchingDate.StudentCharge3 != null)
                                    {
                                        ProcessAllocationDate(matchingDate.StudentCharge3, allocdate, matchingDate, true);
                                    }

                                    if (matchingDate.StudentCharge4 != null)
                                    {
                                        ProcessAllocationDate(matchingDate.StudentCharge4, allocdate, matchingDate, true);
                                    }
                                }
                            }
                        }

                        Changed = false;
                    }
                    catch (Exception ex)
                    {
                        KxLogger.LogActivity(this, "Process charges", ex.Message, "");
                    }
                    result = Save();

                    return result;
                }
        }
        
        private KxAllocationModelDate MatchingModelDate(KxStudentAllocationDate allocdate)
        {
            KxAllocationModelDate result = null;

            foreach (KxAllocationModelDate modelDate in _AllocationModel.Dates.Items)
            {
                if ((modelDate.StartDate == allocdate.ArrivalDate) && (modelDate.EndDate == allocdate.DepartureDate))
                {
                    result = modelDate;
                    break;
                }
            }
            return result;
        }
        
        private KxStudentCharge AddStudentCharges(Int32 allocDate,
                                                    KxStudentChargeType StudentChargeType,
                                                    DateTime? fromDate,
                                                    DateTime? toDate,
                                                    Decimal chargeAmount,
                                                    Int32 instalment,
                                                    DateTime? dueDate
                                                    )
        {
            KxStudentCharge result = new KxStudentCharge(this);

            try
            {
                result.KxStudentId = _KxStudentId;
                result.ApplicationId = _ApplicationId;
                result.AllocationDateId = allocDate;
                result.StudentChargeTypeId = StudentChargeType.StudentChargeTypeId;
                result.SiteId = _SiteId;
                result.AreaId = _AreaId;
                result.CreationDate = DateTime.Now;
                result.CreationUserId = 1;
                result.DateFrom = fromDate;
                result.DateTo = toDate;
                result.StatementText = StudentChargeType.StatementText;
                result.Amount = chargeAmount;
                result.VatId = StudentChargeType.VatId;
                result.InstallmentNo = instalment;
                result.DueDate = dueDate;

                if (result.Vat != null)
                {
                    result.TotalAmount = chargeAmount + (chargeAmount * ((Decimal)((result.Vat.Rate) / 100)));
                }
                else
                {
                    result.TotalAmount = chargeAmount;
                }

                result.Amount = Decimal.Round(result.Amount, 2);
                result.TotalAmount = Decimal.Round(result.TotalAmount, 2);

                result.PrintOnStatement = false;
                result.ResidentialRoomId = _ResidentialRoomId;
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "AddStudentCharges", ex.Message, "");
            }
            return result;
        }
        

        private decimal ProcessAllocationDate(KxStudentChargeType chargeType, KxStudentAllocationDate allocDate, KxAllocationModelDate modelDate, Boolean save)
        {
            KxPricingModelCharge priceCharge = _PricingModel.GetChargeByType(chargeType.StudentChargeTypeId, _AcademicYearId);

            decimal result = 0.0M;

            if (priceCharge != null)
            {
                result = (priceCharge.WeeklyAmount * modelDate.FullWeeks) + (priceCharge.DailyAmount * modelDate.RemainderDays);
            }
            else
            {
                result = modelDate.StudentCharge1.DefaultCharge;
            }

            try
            {
                if ((KxDbConfiguration.GetConfiguration("SinglePaymentDiscount") != "") && save)
                {
                    decimal discpercent = KxDbConfiguration.GetConfigurationDecimal("SinglePaymentDiscount", 0.00M);

                    result = result * (1 - (discpercent / 100));
                }
            }
            catch { }

            if (save)
            {
                AddStudentCharges(
                    allocDate.AllocationDateId, 
                    chargeType, 
                    allocDate.ArrivalDate, 
                    allocDate.DepartureDate, 
                    result, 
                    0, 
                    modelDate.DueDate ?? modelDate.StartDate
                    );
            }

            return result;
        }

        private List<KxStudentChargeType> GetChargeTypes()
        {
            List<KxStudentChargeType> charges = new List<KxStudentChargeType>();

            if (!ChargeListContainsId(charges, _AllocationModel.StudentChargeId1))
                charges.Add(_AllocationModel.StudentCharge1);

            if (!ChargeListContainsId(charges, _AllocationModel.StudentChargeId2))
                charges.Add(_AllocationModel.StudentCharge2);

            if (!ChargeListContainsId(charges, _AllocationModel.StudentChargeId3))
                charges.Add(_AllocationModel.StudentCharge3);

            if (!ChargeListContainsId(charges, _AllocationModel.StudentChargeId4))
                charges.Add(_AllocationModel.StudentCharge4);

            foreach (KxStudentAllocationDate allocdate in _AllocationDates.Items)
            {
                KxAllocationModelDate matchingDate = MatchingModelDate(allocdate);

                if (matchingDate != null)
                {
                    if (!ChargeListContainsId(charges, matchingDate.StudentChargeId1))
                        charges.Add(matchingDate.StudentCharge1);

                    if (!ChargeListContainsId(charges, matchingDate.StudentChargeId2))
                        charges.Add(matchingDate.StudentCharge2);

                    if (!ChargeListContainsId(charges, matchingDate.StudentChargeId3))
                        charges.Add(matchingDate.StudentCharge3);

                    if (!ChargeListContainsId(charges, matchingDate.StudentChargeId4))
                        charges.Add(matchingDate.StudentCharge4);
                }
            }

            return charges;
        }

        private Boolean ChargeListContainsId(List<KxStudentChargeType> charges, Int32 Id)
        {
            bool result = false;

            if (Id > 0)
            {
                foreach (KxStudentChargeType type in charges)
                {
                    if (type.StudentChargeTypeId == Id)
                    {
                        result = true;
                        break;
                    }
                }
            }
            else
            {
                result = true;
            }
            return result;
        }

        private Decimal GetChargesTotal(Int32 chargeType)
        {
            decimal Result = 0.0M;

            if (_AllocationModel.StudentChargeId1 == chargeType)
                Result += _AllocationModel.StudentCharge1.DefaultCharge;

            if (_AllocationModel.StudentChargeId2 == chargeType)
                Result += _AllocationModel.StudentCharge2.DefaultCharge;

            if (_AllocationModel.StudentChargeId3 == chargeType)
                Result += _AllocationModel.StudentCharge3.DefaultCharge;

            if (_AllocationModel.StudentChargeId4 == chargeType)
                Result += _AllocationModel.StudentCharge4.DefaultCharge;

            foreach (KxStudentAllocationDate allocdate in _AllocationDates.Items)
            {
                KxAllocationModelDate matchingDate = MatchingModelDate(allocdate);

                if (matchingDate != null)
                {
                    if (matchingDate.StudentChargeId1 == chargeType)
                        Result += ProcessAllocationDate(matchingDate.StudentCharge1, allocdate, matchingDate, false);

                    if (matchingDate.StudentChargeId2 == chargeType)
                        Result += ProcessAllocationDate(matchingDate.StudentCharge2, allocdate, matchingDate, false);

                    if (matchingDate.StudentChargeId3 == chargeType)
                        Result += ProcessAllocationDate(matchingDate.StudentCharge3, allocdate, matchingDate, false);

                    if (matchingDate.StudentChargeId4 == chargeType)
                        Result += ProcessAllocationDate(matchingDate.StudentCharge4, allocdate, matchingDate, false);
                }
            }

            return Result;
        }

    }
}
