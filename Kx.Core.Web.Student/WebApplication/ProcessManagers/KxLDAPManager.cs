using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using Kx.Core.Exceptions;
using Kx.Core.Security;
using Kx.Core.Web.Student.WebApplication.BaseClass;
using Kx.Objects.Student;
using Kx.Objects.Student.Web;
using Kx.Core.Web.Providers;

namespace Kx.Core.Web.Student.WebApplication.ProcessManagers
{
    public class KxLDAPManager : WebStudentBaseClass
    {
        #region Attributes & Properties

        private string userName = "";
        private string password = "";
        private string emailAddress = "";
        private string confirmEmailAddress = "";
        private string studentId = "";
        private string ldapStatus = "NONE";
        private int matchedStudentId;
        private KxStudent matchedStudent = null;

        protected bool loggedIn = false;
        protected KxWebUserShortUrl shortUrl = null;
        protected KxLDAPAuthentication ldap = null;

        public string UserName
        {
            get
            {
                return userName;
            }
            set
            {
                if (userName == value)
                    return;
                userName = value;
            }
        }

        public string Password
        {
            get
            {
                return password;
            }
            set
            {
                if (password == value)
                    return;
                password = value;
            }
        }

        public string EmailAddress
        {
            get
            {
                return emailAddress;
            }
            set
            {
                if (emailAddress == value)
                    return;
                emailAddress = value;
            }
        }

        public string ConfirmEmailAddress
        {
            get
            {
                return confirmEmailAddress;
            }
            set
            {
                if (confirmEmailAddress == value)
                    return;
                confirmEmailAddress = value;
            }
        }

        public string StudentId
        {
            get
            {
                return studentId;
            }
            set
            {
                if (studentId == value)
                    return;
                studentId = value;
            }
        }

        public int MatchedStudentId
        {
            get
            {
                return matchedStudentId;
            }
            set
            {
                matchedStudentId = value;
                matchedStudent = null;
            }
        }

        public bool LoggedIn
        {
            get
            {
                return loggedIn;
            }
            set
            {
                loggedIn = value;
            }
        }

        public KxStudent MatchedStudent
        {
            get
            {
                if (matchedStudent == null && matchedStudentId > 0)
                {
                    matchedStudent = new KxStudent(null, MatchedStudentId);
                }

                return matchedStudent;
            }
        }

        public KxLDAPAuthentication Ldap
        {
            get
            {
                if (ldap == null)
                {
                    ldap = new KxLDAPAuthentication();
                }

                return ldap;
            }
        }

        public string LdapStatus
        {
            get
            {
                return ldapStatus;
            }
            set
            {
                ldapStatus = value;
            }
        }

        #endregion

        public KxLDAPManager(KxWebApplicationStudent webApp)
            : base(webApp)
        {

        }

        public bool LoginUser(bool stopBadLogin, bool useExtendedLdapAuthentication)
        {
            Kx.Core.Web.Providers.AuditProvider.ConfiguredAddToAudit("KxLDAPManager", "LoginUser", "Started");

            bool result = false;
            string studentId = "";

            if (useExtendedLdapAuthentication)
            {
                studentId = Ldap.GetStudentIdForUserExtended(userName, password);
                Kx.Core.Web.Providers.AuditProvider.ConfiguredAddToAudit("KxLDAPManager", "LoginUser", "GetStudentIdForUserExtended. StudentId: " + studentId);
            }
            else
            {
               // studentId = Ldap.GetStudentIdForUser(userName, password); pha
                studentId = userName;   //pha
                Kx.Core.Web.Providers.AuditProvider.ConfiguredAddToAudit("KxLDAPManager", "LoginUser", "GetStudentIdForUser. StudentId: " + studentId);
            }

            Kx.Core.Web.Providers.AuditProvider.ConfiguredAddToAudit("KxLDAPManager", "LoginUser", String.Format("Requested StudentId. {0}", Ldap.Log));

            if (!String.IsNullOrEmpty(studentId))
            {
                KxWebUserStudent webuser = new KxWebUserStudent();
                webuser.LoadUserName(studentId);

                Kx.Core.Web.Providers.AuditProvider.ConfiguredAddToAudit("KxLDAPManager", "LoginUser", String.Format("Load UserName. StudentId:{0}", studentId));

                if (webuser.UserGuid != Guid.Empty)
                {
                    WebUser.LoadGuid(webuser.GuidIdentifier);
                    LdapStatus = "OK";
                    result = true;

                    Kx.Core.Web.Providers.AuditProvider.ConfiguredAddToAudit("KxLDAPManager", "LoginUser", String.Format("User Loaded. GuidIdentifier:{0}, Username: {1}", webuser.GuidIdentifier, webuser.UserName));
                }
                else
                {
                    webuser.LoadUserNameNew(studentId);

                    if (webuser.UserGuid != Guid.Empty)
                    {
                        if (!String.IsNullOrEmpty(webuser.EmailAddress))
                        {
                            WebUser.LoadGuid(webuser.GuidIdentifier);
                            result = true;
                            LdapStatus = "UNCONFIRMED";
                            Kx.Core.Web.Providers.AuditProvider.ConfiguredAddToAudit("KxLDAPManager", "LoginUser", String.Format("User {0} has UserGUID and Email Address. LDAP status UNCONFIRMED.", studentId));
                        }
                        else
                        {
                            result = true;
                            LdapStatus = "UNREGISTERED";
                            Kx.Core.Web.Providers.AuditProvider.ConfiguredAddToAudit("KxLDAPManager", "LoginUser", String.Format("User {0} has UserGUID and doesn't have Email Address. LDAP status UNREGISTERED.", studentId));
                        }
                    }
                    else
                    {
                        KxStudent stud = new KxStudent(null, studentId);

                        if (stud.KxStudentId > 0)
                        {
                            result = true;
                            LdapStatus = "UNREGISTERED";
                            CreateWebUser(stud);
                            Kx.Core.Web.Providers.AuditProvider.ConfiguredAddToAudit("KxLDAPManager", "LoginUser", String.Format("User {0} doesn't have UserGUID but does exist, new WebUser created. LDAP status UNREGISTERED.", studentId));
                        }
                        else
                        {
                            result = !stopBadLogin;
                            LdapStatus = "INVALID";
                            Kx.Core.Web.Providers.AuditProvider.ConfiguredAddToAudit("KxLDAPManager", "LoginUser", String.Format("User {0} doesn't have UserGUID and does not exist, invalid user. LDAP status INVALID.", studentId));
                        }
                    }
                }
            }
            else
            {
                LdapStatus = "INVALID";
                result = !stopBadLogin;
                Kx.Core.Web.Providers.AuditProvider.ConfiguredAddToAudit("KxLDAPManager", "LoginUser", "Login failed, unable to retrieve StudentId.");
            }

            KxLogger.LogActivity("KxLDAPManager", "LoginUser", "Finished", "");

            return result;
        }

        public bool ValidateLogin()
        {
            bool result = false;

            return result;
        }

        public bool DoesStudentHaveWebAccount(int studentId)
        {
            KxLogger.LogActivity("KxLDAPManager", "DoesStudentHaveWebAccount", "Started", "studentId: " + studentId);

            KxWebUserStudent wu = new KxWebUserStudent();
            wu.LoadStudent(studentId);

            KxLogger.LogActivity("KxLDAPManager", "DoesStudentHaveWebAccount", "Finished", "");

            return wu.IsValid() && !string.IsNullOrEmpty(wu.UserName) && !string.IsNullOrEmpty(wu.Password) && !string.IsNullOrEmpty(wu.EmailAddress);
        }

        public void CreateWebUser(KxStudent student)
        {
            KxLogger.LogActivity("KxLDAPManager", "CreateWebUser", "Started", "");
            Kx.Core.Web.Providers.AuditProvider.ConfiguredAddToAudit("KxLDAPManager", "CreateWebUser", String.Format("Creating WebUser for StudentId: {0}", student.StudentId));

            KxWebUserStudent user = WebUser;

            if (!String.IsNullOrEmpty(userName))
            {
                user.UserName = student.StudentId;
            }

            user.EmailAddress = EmailAddress;
            user.CreationDate = DateTime.Now;
            user.KxStudentId = student.KxStudentId;
            user.PersonId = student.PersonId;

            if (String.IsNullOrEmpty(user.ForeName))
            {
                user.ForeName = student.ForeName;
            }

            if (String.IsNullOrEmpty(user.SurName))
            {
                user.SurName = student.SurName;
            }

            if (String.IsNullOrEmpty(user.EmailAddress))
            {
                Kx.Core.Web.Providers.AuditProvider.ConfiguredAddToAudit("KxLDAPManager", "CreateWebUser", "Default WebUser email empty, retrieving from Kx");

                try
                {
                    user.EmailAddress = student.PreferredEmailAddress.EmailAddress;

                    if (EmailAddress == "")
                    {
                        EmailAddress = user.EmailAddress;
                        ConfirmEmailAddress = user.EmailAddress;
                    }
                }
                catch
                {
                    Kx.Core.Web.Providers.AuditProvider.ConfiguredAddToAudit("KxLDAPManager", "CreateWebUser", String.Format("Failed to assign email address for new WebUser record. StudentId: {0}.", studentId));
                }
            }

            user.Changed = true;
            user.Save();

            Kx.Core.Web.Providers.AuditProvider.ConfiguredAddToAudit("KxLDAPManager", "CreateWebUser", "Finished creating WebUser");
        }

        public void Reset()
        {
            userName = "";
            password = "";
            emailAddress = "";
            confirmEmailAddress = "";
            studentId = "";
        }
    }
}
