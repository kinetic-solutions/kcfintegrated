using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using Kx.Core.Exceptions;
using Kx.Core.Localisation;
using Kx.Core.Reflection;
using Kx.Core.Web.BaseClass;
using Kx.Core.Web.Student.WebApplication;
using Kx.Core.Web.Student.WebApplication.BaseClass;
using Kx.Core.Web.PageRenderer;
using Kx.Core.Web.PageRenderer.Utility;
using Kx.Core.Web.Providers;
using Kx.Core.Web.Student.WebApplication.PageRenderer;
using Kx.Objects.Student;
using Kx.Objects.Student.Web;
using Kx.Objects.Common;
using Kx.Objects.Common.Web;

namespace Kx.Core.Web.Student.WebApplication.PageRenderer
{

    public class KxWebControlStudent : KxWebControl
    {
        public KxWebApplicationStudent WebApp
        {
            get
            { return (KxWebApplicationStudent)webApp; }
        }

        public KxWebControlStudent(object _webApp, Control control, Page _currentPage,KxListProvider _listProvider)
            : base(_webApp, control, _currentPage, _listProvider)
        {

        }

        public override void InitialiseValues()
        {
            classValue = WebApp.ObjectTranslator.GetMergeDataObject(attributes);
            classValueType = ((KxWebApplicationStudent)webApp).ObjectTranslator.GetMergeDataType(attributes);
        }

        public override object GetSecondaryValue(KxWebAttribute attrib, string alternateProperty)
        {
            return WebApp.ObjectTranslator.GetMergeDataObjectSecondary(attrib, alternateProperty);
        }

        //pha
        private void WriteDebugXML(string state, string objectname, bool retval)
        {
            if (System.Configuration.ConfigurationManager.AppSettings["DebugGetState"] == "True")
            {
                using (System.IO.StreamWriter sw = new System.IO.StreamWriter(@"c:\temp\workflow.xml", true))
                {
                    if (string.IsNullOrEmpty(objectname))
                        objectname = "DEFAULT";
                    else
                    {
                        switch (state)
                        {
                            case "Visible":
                                objectname += "." + attributes.VisibleProperty;
                                break;
                            case "ReadOnly":
                                objectname += "." + attributes.ReadOnlyProperty;
                                break;
                            case "Mandatory":
                                objectname += "." + attributes.MandatoryProperty;
                                break;
                        }
                    }
                    if (ParentControl.ID.Contains("_VPApplicationId_CW"))
                        System.Diagnostics.Debugger.Break();
                    sw.WriteLine("<GetState>");
                    sw.WriteLine("\t<Type>" + state + "</Type>");
                    sw.WriteLine("\t<ControlId>" + ParentControl.ID + "</ControlId>");
                    sw.WriteLine("\t<ObjectName>" + objectname + "</ObjectName>");
                    sw.WriteLine("\t<Value>" + retval + "</Value>");
                    sw.WriteLine("</GetState>");
                }
            }
        }

        public override bool GetReadOnlyState()
        {
            string objectname = "";
            bool retval = !WebApp.ObjectTranslator.GetMergeReadonlyStateFlag(attributes, out objectname); //pha

            //pha
            if (!string.IsNullOrEmpty(attributes.ReadOnlyProperty))
                WriteDebugXML("ReadOnly", objectname, retval);
            return retval;
        }

        public override bool GetVisibleState()
        {
            string objectname = "";
            bool retval = WebApp.ObjectTranslator.GetMergeVisibleStateFlag(attributes, out objectname); //pha

            //pha
            //if (retval == false && attributes.Id.Contains("_VPApplicationId_CW0"))
            //{
                
            //    System.Diagnostics.Debugger.Break();
            //}
            if (!string.IsNullOrEmpty(attributes.VisibleProperty))
                WriteDebugXML("Visible", objectname, retval);
            return retval;
        }

        public override bool GetMandatoryState()
        {
            string objectname = "";
            bool retval = WebApp.ObjectTranslator.GetMergeMandatoryStateFlag(attributes, out objectname);   //pha

            //pha
            if (!string.IsNullOrEmpty(attributes.MandatoryProperty))
                WriteDebugXML("Mandatory", objectname, retval);
            return retval;
        }

        public override bool GetMergeStateFlag(string StateProperty, bool defaultvalue)
        {
            string objectname = "";
            bool retval = WebApp.ObjectTranslator.GetMergeStateFlag(attributes, StateProperty, defaultvalue, out objectname); //pha
            return retval;
        }

        public override string MergeTags(string tag)
        {
            return WebApp.ObjectTranslator.MergeTags(tag);
        }

        public override string GetExtendedValid()
        {
            string result = "";

            if (attributes.PreferenceValidation != -99 && ParentControl.Visible && PostbackValueString != "")
            {
                result += ValidatePreference();
            }

            if (result == "" && attributes.PreferenceCFCount != -99 && ParentControl.Visible && PostbackValueString != "")
            {
                result += ValidatePreferenceCount(attributes.PreferenceCFCount);
            }


            if (result == "" &&  attributes.ValidateUDF != -99 && parentControl.Visible)
            {
                result += ValidateUDF(attributes.ValidateUDF);
            }

            if (result == "" && attributes.ValidateUDF2 != -99 && parentControl.Visible)
            {
                result += ValidateUDF(attributes.ValidateUDF2);
            }

            if (result == "" && attributes.ValidateQuestion && attributes.PrimaryObject is KxeInductionQuestionAttempt)
            {
                KxeInductionQuestionAttempt qa = ((KxeInductionQuestionAttempt)attributes.PrimaryObject);
                if (!qa.IsCorrect)
                {
                    result += KxLocalisationManager.GetSubstitution("Question " + qa.Question.QuestionIndex.ToString() + " is incorrect") + ".\r\n";
                }
            }

            return result;
        }

        public string ValidateUDF(int udfId)
        {
            string result = "";

            KxUserDefinedValue comparewith = ((KxWebApplicationStudent)webApp).WorkflowInterface.WebUser.CurrentApplication.UserDefinedValues.GetValueById(udfId);

            if (comparewith != null)
            {
                if (!KxWebAttributeComparison.DoValidationComparison(attributes, CurrentValueString, comparewith.FieldValue))
                {
                    result = DisplayName + KxWebAttributeComparison.GetValidationText(attributes, comparewith.FieldValue);
                }

                if (comparewith.FieldValue == CurrentValueString)
                {
                    try
                    {
                        result = DisplayName + " "+KxLocalisationManager.GetSubstitution("cannot be the same as")+" " + comparewith.FieldName+"\r\n";
                    }
                    catch (Exception ex)
                    {
                        result = " " + KxLocalisationManager.GetSubstitution("Unable to validate user defined value, because") + ": " + ex.Message + "\r\n";
                    }
                }
            }
            return result;
        }

        public string ValidatePreference()
        {
            string result = "";
            try
            {
                if (!((KxWebApplicationStudent)webApp).WorkflowInterface.WebUser.CurrentApplication.Preferences.IsUnique((KxWebStudentPreference)attributes.PrimaryObject))
                {
                    result = KxLocalisationManager.GetSubstitution("All Preferences must be unique") + ".\r\n";
                }

                if (result == "" && attributes.EnsuiteLimit != -1 && ParentControl.Visible && PostbackValueString != "")
                {
                    result += ValidateEnsuiteCount(attributes.EnsuiteLimit);
                }

            }
            catch (Exception ex)
            {
                KxLogger.LogActivity("WebControl", "ValidatePreference", ex.Message, attributes.Id);
            }

            return result;
        }

        public string ValidatePreferenceCount(int count)
        {
            string result = "";
            try
            {
                if (!((KxWebApplicationStudent)webApp).WorkflowInterface.WebUser.CurrentApplication.Preferences.CustomField2CountLimit(attributes.PreferenceCFCount))
                {
                    result = KxLocalisationManager.GetSubstitution("Only " + attributes.PreferenceCFCount.ToString() + " " + ((KxWebStudentPreference)attributes.PrimaryObject).CustomField2Name + " can be selected") + ".\r\n";
                }
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity("WebControl", "ValidatePreferenceCount", ex.Message, attributes.Id);
            }

            return result;
        }

        public string ValidateEnsuiteCount(int limit)
        {
            string result = "";
            try
            {
                if (!((KxWebApplicationStudent)webApp).WorkflowInterface.WebUser.CurrentApplication.Preferences.EnsuiteCountLimit(limit))
                {
                    result = KxLocalisationManager.GetSubstitution(String.Format("Too many en-suite options have been selected", limit)) + ".\r\n";
                }
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity("WebControl", "ValidateEnsuiteCount", ex.Message, attributes.Id);
            }

            return result;
        }

      
    }
}
