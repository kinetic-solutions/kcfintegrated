using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using Kx.Core.Data;
using Kx.Core.Exceptions;
using Kx.Core.Web.Configuration;
using Kx.Core.Web.ObjectTranslator;
using Kx.Core.Web.WorkflowInterface;
using Kx.Core.Web.PageRenderer;
using Kx.Core.Web.PageRenderer.Utility;
using Kx.Core.Web.Providers;
using Kx.Core.Web.WebApplication;
using Kx.Core.Reflection;
using Kx.Core.Web.Student.WebApplication.ObjectTranslator;
using Kx.Objects.Student.Web;
using Kx.Core.Web.Student.WebApplication.BaseClass;
using Kx.Core.Web.Student.WebApplication.WorkflowInterface;

namespace Kx.Core.Web.Student.WebApplication.PageRenderer
{
        public class KxStudentPageRenderer : KxPageRenderer
        {

            public KxStudentPageRenderer(KxWebApplication _webApp)
                : base(_webApp)
            {
            }

            public override void AddWebControl(object webApp, Control control, Page currentPage, KxListProvider listProvider)
            {
                KxWebControlStudent webcon = new KxWebControlStudent(webApp, control, currentPage, listProvider);
                KxControls.Add(webcon);

            }

            public override void CreateWorkflowInterface()
            {
                workflowInterface = new KxStudentWorkflowInterface(this);
            }

            public override void CreateObjectTranslator()
            {
                objectTranslator = new KxStudentObjectTranslator(this);
            }


            public override void ProcessPaymentResponse()
            {
                try
                {
                    //KxLogger.LogActivity(this, "ProcessPaymentResponse1", "", "");
                    WorkflowInterface.ExternalResume();
                    //KxLogger.LogActivity(this, "ProcessPaymentResponse2", "", "");
                    if (((KxWebApplicationStudent)WebApp).ContractManager.PayStatus == Kx.Core.PaymentProcessor.Cache.PaymentStatus.Authorised)
                    {
                        //KxLogger.LogActivity(this, "ProcessPaymentResponse3", "", "");

                        ((KxWebApplicationStudent)WebApp).WorkflowInterface.WebUser.CurrentApplication.Payments.ReloadPayments();

                        //KxLogger.LogActivity(this, "ProcessPaymentResponse4", "", "");

                        //KxLogger.LogActivity(this, "ProcessPaymentResponse5", "", "");
                        try
                        {
                            ((KxWebApplicationStudent)WebApp).ResetStatement();
                        }
                        catch (Exception ex)
                        {
                            KxLogger.LogActivity(this, "Statement Update Failed", ex.Message, "");
                        }

                    }
                    else
                    {
                        KxLogger.LogActivity(this, "Transaction Not Authorised", "", "");
                    }
                }
                catch { }
               
            }
    }
}
