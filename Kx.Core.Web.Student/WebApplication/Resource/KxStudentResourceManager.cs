using System;
using System.Collections.Generic;
using System.Text;
using Kx.Core.Resources;
using Kx.Core.Web.Configuration;
using Kx.Core.Web.Resource;
using Kx.Core.Web.Student.WebApplication;
using Kx.Objects.Student.Web;


namespace Kx.Core.Web.Student.WebApplication.Resource
{
    public class KxStudentResourceManager : KxResourceManager
    {

        public KxWebUrlResolverManager UrlResolver
        {
            get
            {
                try
                {
                    if (KxConfigurationManager.GetSystemObject("UrlResolver") == null)
                    {
                        return (KxWebUrlResolverManager)KxConfigurationManager.SetSystemObject("UrlResolver", new KxWebUrlResolverManager());
                    }
                    else
                    {
                        return (KxWebUrlResolverManager)KxConfigurationManager.GetSystemObject("UrlResolver");
                    }
                }
                catch
                {
                    return null;
                }
            }
        }

        public KxStudentResourceManager(KxWebApplicationStudent webApp)  : base(webApp)
        {
          
        }

    }
}
