using System;
using System.Collections.Generic;
using System.Text;
using Kx.Core.Resources;
using Kx.Objects.Student.Web;
using Kx.Objects.Common;
using System.Runtime.Caching;
using Kx.Core.Web.Configuration;

namespace Kx.Core.Web.Student.WebApplication.Resource
{
    /// <summary>
    /// Extension of standard Resource provider to provide caching in addition. 
    /// </summary>
    public class KxStudentResourceProvider : KxResourceProvider
    {
        private static MemoryCache _cache = MemoryCache.Default;
        private static int expireAfterSeconds = 300;
        private static object _lock = new object();
        private static KxStudentResourceProvider _instance = null;

        /// <summary>
        /// KxStudentResourceProvider, gets Expiration Time from cache if available. 
        /// </summary>
        public KxStudentResourceProvider()
        {
            expireAfterSeconds = Core.Configuration.KxDbConfiguration.GetConfigurationInt("CacheExpirationTime", expireAfterSeconds);
        }
        /// <summary>
        /// Singleton KxStudentResourceProvider Instance using locks.
        /// </summary>
        public static KxStudentResourceProvider Instance
        {
            get
            {
                if (_instance == null)
                {
                   lock (_lock)
                    {
                        if (_instance != null)
                            return _instance;

                        _instance = new KxStudentResourceProvider();
                    }
                }

                return _instance;
            }
        }

        /// <summary>
        /// The GetResourceObject mechanism here implements caching on top of the standard one
        /// </summary>
        /// <param name="id">Web resource identifier</param>
        /// <returns></returns>
        public override KxWebResource GetResourceObject(string id)
        {
            KxWebResource cachedItem = null;
            try
            {
                cachedItem = _cache.Get(id) as KxWebResource;
                if (cachedItem == null)
                {
                    cachedItem = base.GetResourceObject(id);
                    AddItem(id, cachedItem);
                }
                    }
            catch(Exception e) { System.Diagnostics.Debug.WriteLine("Unable to retrieve resource, exception: "+ e); }
            return cachedItem;
        }


        /// <summary>
        /// Override of the GetResource mechanism. 
        /// </summary>
        /// <param name="identifier">Takes resource id</param>
        /// <returns>Content (data) of resource as a string</returns>
        public override string GetResource(string identifier)
        {
            string result = "";

            KxWebResource resrc = GetResourceObject(identifier);

            if (resrc != null)
            {
                result = resrc.Data;
            }

            return result;
        }

        /// <summary>
        /// Adds a Web Resource to the Memory cache
        /// </summary>
        /// <param name="key">Unique key for resource</param>
        /// <param name="data">KxWebResource data</param>
        public void AddItem(string key, KxWebResource data)
        {
            lock (_lock)
            {
                // to avoid race condition
                var objData = _cache.Get(key);
                if (objData == null)
                {
                    CacheItemPolicy policy = new CacheItemPolicy()
                    {
                        AbsoluteExpiration = DateTimeOffset.UtcNow.AddSeconds(expireAfterSeconds)
                    };
                    _cache.Set(key, data, policy);
                    System.Diagnostics.Debug.WriteLine("Item added to cache: " + key + "Expires in.. : " + expireAfterSeconds + " seconds.");
                }
            }

        }

      

    }
}
