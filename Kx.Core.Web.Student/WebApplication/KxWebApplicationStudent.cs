using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Xml;
using System.Web.UI;
using Kx.Core.Configuration;
using Kx.Core.Exceptions;
using Kx.Core.Utility;
using Kx.Core.Web.BaseClass;
using Kx.Core.Web.Configuration;
using Kx.Core.Web.Student.WebApplication.ObjectTranslator;
using Kx.Core.Web.Student.WebApplication.PageRenderer;
using Kx.Core.Web.PageRenderer;
using Kx.Core.Web.Resource;
using Kx.Core.Web.WebApplication;
using Kx.Core.Web.Student.WebApplication.WorkflowInterface;
using Kx.Core.Web.Student.WebApplication.ProcessManagers;
using Kx.Core.PaymentProcessor.Cache;
using Kx.Core.PaymentProcessor.Payments;
using Kx.Objects.Student;
using Kx.Objects.Common.Web;
using Kx.Objects.Student.Web;
using Kx.Core.Web.Student.WebApplication.Resource;
using Kx.Core.Data;
using System.Collections.Specialized;
using Kx.Core.Web.Student.WebApplication.ProcessManagers.SSO;

namespace Kx.Core.Web.Student.WebApplication
{
    [Serializable]
    public class KxWebApplicationStudent : KxWebApplication
    {
        protected KxApplicationsManager applicationsManager = null;
        protected KxRegistrationManager registrationManager = null;
        protected KxPasswordResetManager passwordResetManager = null;
        protected KxStudentObjectTranslator objectTranslator = null;
        protected new KxStudentPageRenderer pageRenderer = null;
        protected KxContractsManager contractManager = null; 
        protected KxPreferenceSearchManager preferenceSearchManager = null;
        protected KxPreferenceManager _preferenceManager = null;
        protected new KxStudentResourceManager resourceManager = null;
        protected KxAllocationManager allocationManager = null;
        protected KxWebBedroomTypeCharges bedroomTypeCharges = null;
        protected KxLDAPManager ldapManager = null;
        protected KxStatementManager statementManager = null;
        protected KxProcessConfiguration processConfig = null;
        protected KxPayToStudyManager payToStudyManager = null;
        protected KxWesternUnionManager _westernUnionManager = null;
        protected KxHousingHandManager _housingHandManager = null;
        protected KxDiscountCodeManager _discountCodeManager;

        protected int userBedroomTypeCharge = -1;
        protected int controlBedroomTypeCharge = -1;

        protected int transferApplicationTypeId = 0;
        protected int transferAcademicYearId = 0;
        protected string documentName = "";

        public int UserBedroomTypeCharge
        {
            get
            {
                return userBedroomTypeCharge;
            }
            set
            {
                userBedroomTypeCharge = value;
            }
        }
        public int ControlBedroomTypeCharge
        {
            get
            {
                return controlBedroomTypeCharge;
            }
            set
            {
                controlBedroomTypeCharge = value;
            }
        }

        public int CurrentBedroomTypeChargeIndex
        {
            get
            {
                if (ControlBedroomTypeCharge != -1)
                {
                    return ControlBedroomTypeCharge;
                }
                else
                {
                    return UserBedroomTypeCharge;
                }
            }
        }

        public KxWebBedroomTypeCharge CurrentBedroomTypeCharge
        {
            get
            {
                return BedroomTypeCharges.GetBedroomTypeChargeByIndex(CurrentBedroomTypeChargeIndex);
            }
        }

        public KxApplicationsManager ApplicationsManager
        {
            get
            {
                if (applicationsManager == null)
                {
                    applicationsManager = new KxApplicationsManager(this);
                }
                return applicationsManager;
            }
        }

        public KxProcessConfiguration ProcessConfiguration
        {
            get
            {
                if (processConfig == null)
                {
                    processConfig = new KxProcessConfiguration(this);
                }
                return processConfig;
            }
        }

        private KxWebSocialProfileTemplate _socialProfileTemplate;
        public KxWebSocialProfileTemplate SocialProfileTemplate
        {
            get
            {
                if (_socialProfileTemplate == null)
                {
                    _socialProfileTemplate = KxWebSocialProfileTemplate.GetWebSocialProfileTemplate();
                }

                return _socialProfileTemplate;
            }
        }

        public KxStatementManager StatementManager
        {
            get
            {
                if (statementManager == null)
                {
                    statementManager = new KxStatementManager(this);
                }
                return statementManager;
            }
            set
            {
                statementManager = value;
            }
        }

        public KxStudentObjectTranslator ObjectTranslator
        {
            get
            {
                return (KxStudentObjectTranslator)PageRenderer.ObjectTranslator;
            }
        }
        public KxStudentPageRenderer PageRenderer
        {
            get
            {
                if (pageRenderer == null )
                {
                    pageRenderer = new KxStudentPageRenderer(this);
                }
                return pageRenderer;
            }
        }

        public KxContractsManager ContractManager
        {
            get
            {
                if (contractManager == null)
                {
                    string objType = System.Configuration.ConfigurationManager.AppSettings["KxContractsManager"];
                    if (!string.IsNullOrEmpty(objType))
                    {
                        Type managerType = Type.GetType(objType);
                        if (managerType != null)
                        {
                            contractManager = (KxContractsManager)Activator.CreateInstance(managerType, this);
                        }
                    }

                    if (contractManager == null)
                    {
                        contractManager = new KxContractsManager(this);
                    }
                }
                return contractManager;
            }
            set
            {
                contractManager = value;
            }
        }
        public KxPayToStudyManager PayToStudyManager
        {
            get
            {
                if (payToStudyManager == null)
                {
                    string objType = System.Configuration.ConfigurationManager.AppSettings["KxPayToStudyManager"];
                    if (!string.IsNullOrEmpty(objType))
                    {
                        Type managerType = Type.GetType(objType);
                        if (managerType != null)
                        {
                            payToStudyManager = (KxPayToStudyManager)Activator.CreateInstance(managerType, this);
                        }
                    }

                    if (payToStudyManager == null)
                    {
                        payToStudyManager = new KxPayToStudyManager(this);
                    }
                }
                return payToStudyManager;
            }
            set
            {
                payToStudyManager = value;
            }
        }

        public KxWesternUnionManager WesternUnionManager
        {
            get
            {
                if (_westernUnionManager == null)
                {
                    string objType = System.Configuration.ConfigurationManager.AppSettings["KxWesternUnionManager"];
                    if (!string.IsNullOrEmpty(objType))
                    {
                        Type managerType = Type.GetType(objType);
                        if (managerType != null)
                        {
                            _westernUnionManager = (KxWesternUnionManager)Activator.CreateInstance(managerType, this);
                        }
                    }

                    if (_westernUnionManager == null)
                    {
                        _westernUnionManager = new KxWesternUnionManager(this);
                    }
                }
                return _westernUnionManager;
            }
            set
            {
                _westernUnionManager = value;
            }
        }

        public KxHousingHandManager HousingHandManager
        {
            get
            {
                if (_housingHandManager == null)
                {
                    string objType = System.Configuration.ConfigurationManager.AppSettings["KxHousingHandManager"];
                    if (!string.IsNullOrEmpty(objType))
                    {
                        Type managerType = Type.GetType(objType);
                        if (managerType != null)
                        {
                            _housingHandManager = (KxHousingHandManager)Activator.CreateInstance(managerType, this);
                        }
                    }

                    if (_housingHandManager == null)
                    {
                        _housingHandManager = new KxHousingHandManager(this);
                    }
                }
                return _housingHandManager;
            }
            set
            {
                _housingHandManager = value;
            }
        }

        public KxPreferenceManager PreferenceManager
        {
            get
            {
                if (_preferenceManager == null)
                {
                    _preferenceManager = new KxPreferenceManager(this);
                }

                return _preferenceManager;
            }
        }

        public KxPreferenceSearchManager PreferenceSearchManager
        {
            get
            {
                if (preferenceSearchManager == null)
                {
                    preferenceSearchManager = new KxPreferenceSearchManager(this);
                }
                return preferenceSearchManager;
            }
            set
            {
                if (preferenceSearchManager == value)
                    return;
                preferenceSearchManager = value;
            }
        }

        private KxStudentRequestManager _requestManager;
        public KxStudentRequestManager RequestManager
        {
            get
            {
                if (_requestManager == null)
                {
                    _requestManager = new KxStudentRequestManager(this);
                }

                return _requestManager;
            }
        }

        protected KxStudentInventoryManager _inventoryManager;
        public KxStudentInventoryManager InventoryManager
        {
            get
            {
                if(_inventoryManager == null)
                {
                    _inventoryManager = new KxStudentInventoryManager(this);
                }
                return _inventoryManager;
            }
            set
            {
                _inventoryManager = value;
            }
        }
        

        public KxRegistrationManager RegistrationManager
        {
            get
            {
                if (registrationManager == null)
                {
                    registrationManager = new KxRegistrationManager(this);
                }
                return registrationManager;
            }
            set
            {
                if (registrationManager == value)
                    return;
                registrationManager = value;
            }
        }

        public KxPasswordResetManager PasswordResetManager
        {
            get
            {
                if (passwordResetManager == null)
                {
                    passwordResetManager = new KxPasswordResetManager(this);
                }
                return passwordResetManager;
            }
            set
            {
                if (passwordResetManager == value)
                    return;
                passwordResetManager = value;
            }
        }

        public KxStudentResourceProvider ResourceProvider
        {
            get
            {
                return (KxStudentResourceProvider)KxConfigurationManager.GetSystemObject("ResourceProvider");
            }
        }

        public KxWebBedroomTypeCharges BedroomTypeCharges
        {
            get
            {
                if (bedroomTypeCharges == null)
                {
                    bedroomTypeCharges = new KxWebBedroomTypeCharges(null, WorkflowInterface.WebUser.CurrentApplication.AcademicYearId);
                }
                return bedroomTypeCharges;
            }
        }

        public new KxStudentResourceManager ResourceManager
        {
            get
            {
                if (resourceManager == null)
                {
                    resourceManager = new KxStudentResourceManager(this);
                }
                return resourceManager;
            }
            set
            {
                if (resourceManager == value)
                    return;
                resourceManager = value;
            }
        }

        public KxStudentWorkflowInterface WorkflowInterface
        {
            get
            {
                return (KxStudentWorkflowInterface)PageRenderer.WorkflowInterface;
            }
        }

        public KxAllocationManager AllocationManager
        {
            get
            {
                if (allocationManager == null)
                {
                    allocationManager = new KxAllocationManager(this);
                }
                return allocationManager;
            }
        }

        public KxLDAPManager LdapManager
        {
            get
            {
                if (ldapManager == null)
                {
                    ldapManager = new KxLDAPManager(this);
                }
                return ldapManager;
            }
        }
        public KxDiscountCodeManager DiscountCodeManager
        {
            get
            {
                if (_discountCodeManager == null)
                {
                    _discountCodeManager = new KxDiscountCodeManager(this);
                }

                return _discountCodeManager;
            }
        }
        public KxWebUrlResolverManager UrlResolver
        {
            get
            {
                return ResourceManager.UrlResolver;
            }
        }

        public KxWebApplicationStudent() : base()
        { }

        public KxWebApplicationStudent(Page currentPage)
        {
            InboundPayment = false;
            this.currentPage = currentPage;
            CheckForUserTransfer();
            PageRenderer.ProcessPage(currentPage,false);
            CheckForUserTransferPost();
        }

        public KxWebApplicationStudent(Page currentPage, PaymentResponse paymentResponse)
        {
            InboundPayment = paymentResponse != null;
            ContractManager.PayResponse = paymentResponse;
            this.currentPage = currentPage;
            CheckForUserTransfer();
            PageRenderer.ProcessPage(currentPage, paymentResponse != null);
            CheckForUserTransferPost();
        }

        public KxWebApplicationStudent(Page currentPage, PaymentStatus status)
        {
            InboundPayment = status != PaymentStatus.None;
            ContractManager.PayStatus = status;
            this.currentPage = currentPage;
            CheckForUserTransfer();
            PageRenderer.ProcessPage(currentPage, InboundPayment);
            CheckForUserTransferPost();
        }

        public bool ProcessWebAllocations()
        {
            KxData data = new KxData("allocations", "CreateWebAllocations", TargetType.StoredProc, ActionType.Scalar);
            object obj = data.ExecuteScalar();

            return true;
        }

        public bool ProcessWebApplications()
        {
            KxData data = new KxData("applications", "CreateWebApplications", TargetType.StoredProc, ActionType.Scalar);
            object obj = data.ExecuteScalar();

            return true;
        }

        public void ProcessPage(Page currentPage)
        {
            InboundPayment = false;
            this.currentPage = currentPage;
            CheckForUserTransfer();
            PageRenderer.ProcessPage(currentPage,false);
            CheckForUserTransferPost();
        }

        public void ProcessPage(Page currentPage, PaymentResponse paymentResponse)
        {
            InboundPayment = paymentResponse != null;
            ContractManager.PayResponse = paymentResponse;
            this.currentPage = currentPage;
            CheckForUserTransfer();
            PageRenderer.ProcessPage(currentPage, paymentResponse != null);
            CheckForUserTransferPost();
        }

        public void ProcessPage(Page currentPage, PaymentStatus status)
        {
            InboundPayment = status != PaymentStatus.None;
            ContractManager.PayStatus = status;
            this.currentPage = currentPage;
            CheckForUserTransfer();
            PageRenderer.ProcessPage(currentPage, InboundPayment);
            CheckForUserTransferPost();
        }

        public void ProcessAjax(Page currentPage, Control HostPanel, Control trigger)
        {
            ProcessAjax(currentPage, HostPanel, trigger, true);
        }

        public void ProcessAjax(Page currentPage, Control HostPanel, Control trigger, bool ProcessWorkflow)
        {
            InboundPayment = false;
            this.currentPage = currentPage;
            PageRenderer.ProcessAjax(HostPanel, trigger, ProcessWorkflow);
        }

        public void ProcessRendering(Page currentPage,string PageContent)
        {
            PageContent = PageContent.Replace("�", "&pound;");
            PageRenderer.PageContent = PageContent;
            
            if (WorkflowInterface.Extender != null)
            {
                WorkflowInterface.Extender.AfterRender();
            }
        }


        public void CheckForUserTransfer()
        {
            string studentId = "";
            string eURL = String.Empty;
            ISingleSignon mgr = GetSSOManager();

            if (currentPage.Request.QueryString["contractlength"] != null && WorkflowInterface.WebUser != null)
            {
                #region

                Kx.Core.Web.Providers.AuditProvider.ConfiguredAddToAudit("KxWebApplicationStudent", "CheckForUserTransfer", "contractlength and WebUser");

                // http://localhost/WorkflowWeb/BlackStone/default.aspx?siteid=1&contractlength=52&bedroomtype=32

                try
                {
                    WorkflowInterface.WebUser.CurrentApplication.FirstPreference.ContractLength = Convert.ToInt32(currentPage.Request.QueryString["contractlength"]);
                    WorkflowInterface.WebUser.CurrentApplication.FirstPreference.SiteId = Convert.ToInt32(currentPage.Request.QueryString["siteid"]);
                    WorkflowInterface.WebUser.CurrentApplication.FirstPreference.BlockId = Convert.ToInt32(currentPage.Request.QueryString["blockid"]);
                    WorkflowInterface.WebUser.CurrentApplication.FirstPreference.BedroomTypeId = Convert.ToInt32(currentPage.Request.QueryString["bedroomtype"]);
                    WorkflowInterface.WebUser.CurrentApplication.FirstPreference.CustomValue1 = currentPage.Request.QueryString["allocationmodel"];
                    WorkflowInterface.WebUser.CurrentApplication.FirstPreference.CustomValue2 = currentPage.Request.QueryString["pricingmodel"];
                    try
                    {
                        WorkflowInterface.WebUser.CurrentApplication.PaymentScheduleTypeId = Convert.ToInt32(currentPage.Request.QueryString["paymenttypeid"]);
                    }
                    catch { }

                    WorkflowInterface.WebUser.Changed = true;
                    WorkflowInterface.WebUser.CurrentApplication.Changed = true;
                    WorkflowInterface.WebUser.CurrentApplication.FirstPreference.Changed = true;
                    WorkflowInterface.WebUser.Save();
                }
                catch
                {

                }

                #endregion
            }
            else if (currentPage.Request.QueryString["resume"] != null)
            {
                #region

                Kx.Core.Web.Providers.AuditProvider.ConfiguredAddToAudit("KxWebApplicationStudent", "CheckForUserTransfer", "resume");

                string Resume = currentPage.Request.QueryString["resume"].ToString();
                string pid = KxStringUtils.ObjectToString(currentPage.Request.QueryString["PID"]);

                if (String.IsNullOrEmpty(pid))
                {
                    if (currentPage.Session["Resume" + Resume] == null)
                    {
                        KxLogger.LogActivity(this, "Processing Resume", Resume, WorkflowInterface.LastAction);
                        InboundPayment = true;
                        currentPage.Session["Resume" + Resume] = Resume;
                        WorkflowInterface.Resume(Resume);
                    }
                }
                else
                {
                    if (currentPage.Session["Resume" + Resume + pid] == null)
                    {
                        KxLogger.LogActivity(this, "Processing Resume", Resume, WorkflowInterface.LastAction);
                        InboundPayment = true;
                        currentPage.Session["Resume" + Resume+pid] = Resume;
                        WorkflowInterface.Resume(Resume);
                    }
                }

                #endregion
            }
            else if (currentPage.Request.QueryString["sessionresume"] != null)
            {
                #region

                Kx.Core.Web.Providers.AuditProvider.ConfiguredAddToAudit("KxWebApplicationStudent", "CheckForUserTransfer", "sessionresume");
                
                try
                {
                    string resume = currentPage.Request.QueryString["sessionresume"].ToString();
                    string ResumeFull = resume + currentPage.Request.QueryString["UID"].ToString();

                    Kx.Core.Web.Providers.AuditProvider.ConfiguredAddToAudit("KxWebApplicationStudent", "CheckForUserTransfer", "ResumeFull: " + ResumeFull);

                    if (currentPage.Session["sessionresume" + ResumeFull] == null)
                    {
                        currentPage.Session["Workflow"] = new Guid(currentPage.Request.QueryString["UID"].ToString());

                        Kx.Core.Web.Providers.AuditProvider.ConfiguredAddToAudit("KxWebApplicationStudent", "CheckForUserTransfer", "Processing Session Resume. Resume: " + resume + ". WorkflowInterface.LastAction: " + WorkflowInterface.LastAction);

                        InboundPayment = true;
                        currentPage.Session["sessionresume" + ResumeFull] = resume;
                        WorkflowInterface.Resume(resume);
                    }
                }
                catch { }

                #endregion
            }
            else if(mgr != null)
            {
                if (mgr.PerformSingleSignon(this, new HttpContextWrapper(HttpContext.Current)))
                {
                    if (mgr.AcademicYearId > 0)
                    {
                        transferAcademicYearId = mgr.AcademicYearId;
                    }
                    if (!string.IsNullOrWhiteSpace(mgr.StudentId))
                    {
                        studentId = mgr.StudentId;
                    }
                    if (mgr.ApplicationTypeId > 0)
                    {
                        transferApplicationTypeId = mgr.ApplicationTypeId;
                    }
                }
            }
            

            if (!String.IsNullOrEmpty(studentId) && ((WorkflowInterface.WebUser != null) && (WorkflowInterface.Eurl != eURL || !WorkflowInterface.WebUser.Loaded)))
            {
                Kx.Core.Web.Providers.AuditProvider.ConfiguredAddToAudit("KxWebApplicationStudent", "CheckForUserTransfer", "Last Check. studentId: " + studentId);

                currentPage.Session["eurl"] = null;
              
                KxStudent student = new KxStudent(null, studentId);

                if (student.KxStudentId > 0)
                {
                    WorkflowInterface.WebUser.LoadStudent(student.KxStudentId);
                    WorkflowInterface.WebUser.LoggedIn = true;

                    if (WorkflowInterface.WebUser.Loaded && WorkflowInterface.Eurl != eURL) //only set this when we have actually loaded - we might need to come back in here!!
                    {
                        //Kx.Core.Web.Configuration.KxConfigurationManager.InitialiseObjects();
                        WorkflowInterface.Eurl = eURL;
                        HttpContext.Current.Session.Clear();
                    }

                    if (transferAcademicYearId > 0)
                    {
                        WorkflowInterface.WebUser.UserAcademicYear = transferAcademicYearId;
                    }

                    if (transferApplicationTypeId > 0)
                    {
                        if (WorkflowInterface.WebUser.CurrentApplication.IsUnconfirmed)
                        {
                            ApplicationsManager.ApplicationTypeId = transferApplicationTypeId;
                            //WorkflowInterface.WebUser.CurrentApplication.ApplicationTypeId = transferApplicationTypeId;
                        }
                    }

                    //if (currentPage.Request.QueryString["ID"] != null)
                    //{
                        if (WorkflowInterface.WebUser.UserGuid == Guid.Empty && WorkflowInterface.WebUser.KxStudentId > 0)
                        {
                            WorkflowInterface.WebUser.UserName = student.StudentId;
                            WorkflowInterface.WebUser.ForeName = student.ForeName;
                            WorkflowInterface.WebUser.SurName = student.SurName;
                            WorkflowInterface.WebUser.EmailAddress = student.EmailAddress;
                            WorkflowInterface.WebUser.Active = 1;
                            WorkflowInterface.WebUser.EmailConfirmed = DateTime.Now;
                            WorkflowInterface.WebUser.UserGuid = Guid.NewGuid();
                            WorkflowInterface.WebUser.Save();
                            WorkflowInterface.WebUser.Loaded = true;
                        }

                        DeleteTransferStudent(studentId);
                   // }
                    try
                    {
                        if (WorkflowInterface.CurrentWorkflow.CurrentStep.PreviousStep != null)
                        {
                            WorkflowInterface.Back();
                        }
                        else
                        {
                            
                        }
                    }
                    catch 
                    {
                       // WorkflowInterface.Back();
                    }
                }
            }          
        }

        private ISingleSignon GetSSOManager()
        {
            return Kx.Core.ServiceLocator.ServiceLocator.ResolveService<ISingleSignon>();
        }

        public void CheckForUserTransferPost()
        {
            if (currentPage.Request.QueryString["siteid"] != null)
            {
                string Resume = currentPage.Request.QueryString["siteid"].ToString();

                if (currentPage.Session["siteid" + Resume] == null)
                {
                    Kx.Core.Web.Providers.AuditProvider.ConfiguredAddToAudit("KxWebApplicationStudent", "CheckForUserTransferPost", "Processing start. Resume: " + Resume + ". WorkflowInterface.LastAction: " + WorkflowInterface.LastAction);

                    InboundPayment = true;
                    currentPage.Session["siteid" + Resume] = Resume;
                    WorkflowInterface.Resume("overview");
                    // PageRenderer.ProcessPage(currentPage, false);
                }
            }
        }


        public void DeleteTransferStudent(string studentid)
        {
            try
            {
                if (KxDbConfiguration.GetConfiguration("TransferTest") == "")
                {
                    KxData data = new KxData("studenttransfer", "StudentTransfer", TargetType.Table, ActionType.Delete);
                    data.AddKeyField("studentid", studentid, KxValueType.String);
                    data.Execute();
                }
            }
            catch
            { }
        }

        public bool SaveFromWorkflow()
        {
            return PageRenderer.WorkflowInterface.WorkflowSave();
        }

        public void AddStrike()
        {
            WorkflowInterface.AddStrike();
        }

        public void ChangeWorkflowGuid()
        {
            WorkflowInterface.WorkflowGuid = Guid.NewGuid();
        }

        public void ResetStatement()
        {
            StatementManager.Reset();
            WorkflowInterface.WebUser.CurrentApplication.StandardApplication.StudentStatements = null;
            statementManager = null;
        }


        /// <summary>
        /// Delete a document from the WebStudentCorrespondance database to be processed
        /// </summary>
        /// <param name="kxStudentId"></param>
        /// <param name="documentName"></param>
        public void DeleteDocument(int kxStudentId, string documentName)
        {
            KxData data = new KxData("webstudentcorrespondance", "WebStudentCorrespondance", TargetType.Table, ActionType.Delete);

            data.AddKeyField("KxStudentId", kxStudentId, KxValueType.Int);
            data.AddKeyField("DocumentName", documentName != string.Empty ? documentName : "Web Application Document", KxValueType.String);

            data.Execute();
        }

        public void DeleteDocumentByAppGuid(Guid appGuid, string documentName)
        {
            KxData data = new KxData("webstudentcorrespondance", "WebStudentCorrespondance", TargetType.Table, ActionType.Delete);

            data.AddKeyField("ApplicationGuid", appGuid, KxValueType.Guid);
            data.AddKeyField("DocumentName", documentName != string.Empty ? documentName : "Web Application Document", KxValueType.String);

            data.Execute();
        }

        /// <summary>
        /// Uploads A document to the WebStudentCorrespondance database to be processed
        /// </summary>
        /// <param name="kxStudentId"></param>
        /// <param name="binaryDocumentString"></param>
        /// <param name="documentName"></param>
        public void UploadDocument(int kxStudentId, byte[] binaryDocumentString, string documentName)
        {
            KxData data = new KxData("webstudentcorrespondance", "WebStudentCorrespondance", TargetType.Table,ActionType.Insert);

            data.AddParameter("KxStudentId", kxStudentId, KxValueType.Int);
            data.AddParameter("DocumentName", documentName != string.Empty ? documentName : "Web Application Document", KxValueType.String);
            data.AddParameter("BinaryDocument", binaryDocumentString, KxValueType.Text);
            data.AddParameter("ApplicationGuid", WorkflowInterface.WebUser.CurrentApplication.ApplicationGuid, KxValueType.Guid);
            data.AddParameter("FileName", "{Unknown}", KxValueType.String);
            data.AddParameter("Processed", false, KxValueType.Boolean);

            data.Execute();
        }

        public void UploadDocumentByAppGuid(Guid appGuid, byte[] binaryDocumentString, string documentName)
        {
            KxData data = new KxData("webstudentcorrespondance", "WebStudentCorrespondance", TargetType.Table, ActionType.Insert);

            data.AddParameter("KxStudentId", 0, KxValueType.Int);
            data.AddParameter("DocumentName", documentName != string.Empty ? documentName : "Web Application Document", KxValueType.String);
            data.AddParameter("BinaryDocument", binaryDocumentString, KxValueType.Text);
            data.AddParameter("ApplicationGuid", WorkflowInterface.WebUser.CurrentApplication.ApplicationGuid, KxValueType.Guid);
            data.AddParameter("FileName", "{Unknown}", KxValueType.String);
            data.AddParameter("Processed", false, KxValueType.Boolean);

            data.Execute();
        }

        /// <summary>
        /// Uploads A document to the WebStudentCorrespondance database to be processed
        /// </summary>
        /// <param name="kxStudentId"></param>
        /// <param name="binaryDocumentString"></param>
        /// <param name="documentName"></param>
        /// <param name="documentFileName"></param>
        public void UploadDocument(int kxStudentId, byte[] binaryDocumentString, string documentName, string documentFileName)
        {
            KxData data = new KxData("webstudentcorrespondance", "WebStudentCorrespondance", TargetType.Table, ActionType.Insert);

            data.AddParameter("KxStudentId", kxStudentId, KxValueType.Int);
            data.AddParameter("DocumentName", documentName != string.Empty ? documentName : "Web Application Document", KxValueType.String);
            data.AddParameter("BinaryDocument", binaryDocumentString, KxValueType.Text);
            data.AddParameter("ApplicationGuid", WorkflowInterface.WebUser.CurrentApplication.ApplicationGuid, KxValueType.Guid);
            data.AddParameter("FileName", documentName != string.Empty ? documentFileName : "{Unknown}", KxValueType.String);
            data.AddParameter("Processed", false, KxValueType.Boolean);

            data.Execute();
        }

        public void UploadDocumentByAppGuid(Guid appGuid, byte[] binaryDocumentString, string documentName, string documentFileName)
        {
            KxData data = new KxData("webstudentcorrespondance", "WebStudentCorrespondance", TargetType.Table, ActionType.Insert);

            data.AddParameter("KxStudentId", 0, KxValueType.Int);
            data.AddParameter("DocumentName", documentName != string.Empty ? documentName : "Web Application Document", KxValueType.String);
            data.AddParameter("BinaryDocument", binaryDocumentString, KxValueType.Text);
            data.AddParameter("ApplicationGuid", WorkflowInterface.WebUser.CurrentApplication.ApplicationGuid, KxValueType.Guid);
            data.AddParameter("FileName", documentName != string.Empty ? documentFileName : "{Unknown}", KxValueType.String);
            data.AddParameter("Processed", false, KxValueType.Boolean);

            data.Execute();
        }


        public object ExecuteStoredProcedureScalar(string storedProcedureName, object Parm1, object Parm2, object Parm3, object Parm4)
        {
            if (string.IsNullOrEmpty(storedProcedureName))
            {
                return null;
            }

            KxData data = new KxData("", storedProcedureName, TargetType.StoredProc);
            data.AddParameter("Parm1", Parm1, KxValueType.Unknown);
            data.AddParameter("Parm2", Parm2, KxValueType.Unknown);
            data.AddParameter("Parm3", Parm3, KxValueType.Unknown);
            data.AddParameter("Parm4", Parm4, KxValueType.Unknown);

            return data.ExecuteScalar();
        }

        public bool RunStoreProcedure(string StoredProcName, string Parm1, int Parm2)
        {
            if (StoredProcName != string.Empty)
            {
                int numberOfRows = 0;

                KxData data = new KxData("", StoredProcName, TargetType.StoredProc);
                data.AddParameter("Parm1", Parm1, KxValueType.String);
                data.AddParameter("Parm2", Parm2, KxValueType.Int);

                object dbresult = data.ExecuteScalar();

                if (dbresult != null)
                {
                    try
                    {
                        numberOfRows = Convert.ToInt32(dbresult);
                    }
                    catch
                    { }
                }
                if (numberOfRows > 0)
                    return true;
                else return false;
            }
            else return false;
        }

        public int RunStoreProcedureInt(string StoredProcName, string Parm1, int Parm2)
        {
            if (StoredProcName != string.Empty)
            {
                int intValue = 0;

                KxData data = new KxData("", StoredProcName, TargetType.StoredProc);
                data.AddParameter("Parm1", Parm1, KxValueType.String);
                data.AddParameter("Parm2", Parm2, KxValueType.Int);

                object dbresult = data.ExecuteScalar();

                if (dbresult != null)
                {
                    try
                    {
                        intValue = Convert.ToInt32(dbresult);
                    }
                    catch
                    { }
                }
                if (intValue > 0)
                    return intValue;
                else return 0;
            }
            else return 0;
        }

        public int RunStoreProcedureInt1(string StoredProcName, Guid Parm1, int Parm2)
        {
            if (StoredProcName != string.Empty)
            {
                int intValue = 0;

                KxData data = new KxData("", StoredProcName, TargetType.StoredProc);
                data.AddParameter("Parm1", Parm1, KxValueType.Guid);
                data.AddParameter("Parm2", Parm2, KxValueType.Int);

                object dbresult = data.ExecuteScalar();

                if (dbresult != null)
                {
                    try
                    {
                        intValue = Convert.ToInt32(dbresult);
                    }
                    catch
                    { }
                }
                if (intValue > 0)
                    return intValue;
                else return 0;
            }
            else return 0;
        }

        public string GetApplicationGuidAsString()
        {
            string result = WorkflowInterface.WebUser.CurrentApplication.ApplicationGuid.ToString();
            return result;
        }

        public void LogWebUserError(String stepName, String identifier, String reason)
        {
            KxLogger.LogActivity("WFE:" + this.WorkflowInterface.CurrentWorkflow.WorkflowName, "Step: " + stepName, "UserGuid: " + identifier, reason);
        }

        public void LogWebApplicationError(String stepName, String identifier, String reason)
        {
            KxLogger.LogActivity("WFE:" + this.WorkflowInterface.CurrentWorkflow.WorkflowName, "Step: " + stepName, "AppGuid: " + identifier, reason);
        }
    }
}
