﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Web;
using Kx.Core.Exceptions;
using Kx.Core.Reflection;
using Kx.Core.Web.Configuration;
using Kx.Core.Workflow.Stats;
using Kx.Objects.Student.Web;
using System.Xml.Linq;

namespace Kx.Core.Web.Student.Classes
{
    public class KCFWebStatsLogger : IKCFStatsLogger
    {
        private const string STR_StatsTrackingId = "StatsTrackingId";


        public void LogStats(string activity)
        {
            KxKCFWebStat stat = new KxKCFWebStat(null);
            stat.Activity = activity;
            stat.TrackingId = GetTrackingId();
            stat.StatData = GetStatData();
            stat.TrackingTimestamp = DateTime.Now;
            if (!stat.Save())
            {
                KxLogger.LogActivity(this, "LogStats", "Error saving stats record", stat.LastError);
            }
        }

        private string GetStatData()
        {
            string fieldsString = KxConfigurationManager.GetConfiguration("StatsFields");

            XDocument doc = new XDocument(new XDeclaration("1.0", "UTF-8", "yes"));
            XElement root = new XElement("StatsData");
            doc.Add(root);

            if (string.IsNullOrWhiteSpace(fieldsString))
            {
                fieldsString = "Browser.Browser|Browser.Platform|Browser.Version|UserAgent|Browser.MobileDeviceManufacturer|Browser.MobileDeviceModel";
            }
            string[] fields = fieldsString.Split(new string[] {"|"}, StringSplitOptions.RemoveEmptyEntries);

            var browser = new XElement("Browser");
            root.Add(browser);

            foreach (var field in fields.Where(s => s.StartsWith("Browser.", StringComparison.InvariantCultureIgnoreCase)))
            {
                var shortField = field.Replace("Browser.", "");
                browser.Add(new XElement(shortField, new XCData(KxReflection.GetPropertyString(HttpContext.Current.Request.Browser, shortField))));
            }
            foreach (var field in fields.Where(s => !s.StartsWith("Browser.", StringComparison.InvariantCultureIgnoreCase)))
            {
                root.Add(new XElement(field, new XCData(KxReflection.GetPropertyString(HttpContext.Current.Request, field))));
            }
            return doc.ToString();
        }

        private string GetTrackingId()
        {
            if (HttpContext.Current.Session[STR_StatsTrackingId] == null)
            {
                HttpContext.Current.Session[STR_StatsTrackingId] = Guid.NewGuid().ToString("N");
            }

            return HttpContext.Current.Session[STR_StatsTrackingId].ToString();
        }
    }
}
