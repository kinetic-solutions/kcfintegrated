﻿#region copyright

// =======================================================
// Product:   KCF.Student
// Project:   Kx.Core.Web.Student
// File:      WorkflowPage.cs
// Created:   08/03/2017
// By:        Steve Ashley
// Modified:  06/04/2017
// 
// Copyright © Kinetic Solutions Ltd 2017
// All rights reserved.
// 
// No portion of this file may be re-created, copied or otherwise
// used without the express permission of Kinetic Solutions Ltd.
// =======================================================

#endregion

#region usings

using System;
using System.Configuration;
using System.IO;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.UI;
using Kx.Core.Configuration;
using Kx.Core.Exceptions;
using Kx.Core.PaymentProcessor.Cache;
using Kx.Core.Web.Student.Payments;
using Kx.Core.Web.Student.WebApplication;

#endregion

namespace Kx.Core.Web.Student.Classes
{
    public class WorkflowPage : Page, INamingContainer
    {
        #region Fields and Properties

        PaymentStatus status = PaymentStatus.None;

        /// <summary>
        /// The URI path name
        /// </summary>
        public string UIPathName { get; private set; }

        public KxWebApplicationStudent WebApp
        {
            get
            {
                try
                {
                    if (Session["WebMgr"] == null)
                    {
                        Session["WebMgr"] = new KxWebApplicationStudent(this, status);
                    }
                    return ((KxWebApplicationStudent) Session["WebMgr"]);
                }
                catch
                {
                    return null;
                }
            }
        }

        #endregion

        #region Public Methods

        public string ResolveFullURL(string relativeUrl)
        {
            return new Uri(Page.Request.Url, ResolveClientUrl(relativeUrl)).AbsoluteUri;
        }

        #endregion

        #region Protected Methods

        protected virtual void InputSessionEnd()
        {
            if (WebApp.WorkflowInterface.WebUser != null && WebApp.WorkflowInterface.WebUser.Loaded)
            {
                int timeout = Session.Timeout;
                if (Session.Timeout > 2)
                {
                    timeout = timeout - 1;
                }

                Page.Header.Controls.Add(new LiteralControl(String.Format("<meta http-equiv='refresh' content='{0};url={1}'>", timeout * 60, KxDbConfiguration.GetConfiguration("HomePageUrl") + "?wf=SessionEnd")));
            }
        }

        protected virtual void CheckSessionStatus()
        {
            if (UIPathName.ToLower().Contains("sessionend"))
            {
                KxLogger.LogActivity("KCF", "Session End", "Session has been killed for user due to timeout", "Userguid:" + WebApp.WorkflowInterface.WebUser.UserGuid.ToString());
                Session.Abandon();
            }
            else if (UIPathName.ToLower().Contains("logout"))
            {
                KxLogger.LogActivity("KCF", "User Logout", "Session has been killed for user due to logout", "Userguid:" + WebApp.WorkflowInterface.WebUser.UserGuid.ToString());
                Session.Abandon();
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            UIPathName = Path.GetFileName(Request.CurrentExecutionFilePath);

            if (PaymentProcessor.PaymentProcessor.IsPaymentReceived)
            {
                Session["Payinbound"] = "1";
                StudentPaymentProcessor pp = new StudentPaymentProcessor();
                try
                {
                    status = pp.ProcessPayment();
                }
                catch
                {
                }
            }

            InputSessionEnd();
            CheckSessionStatus();

            try
            {
                string resume = HttpContext.Current.Request.QueryString["sessionresume"];
                string ResumeFull = (resume ?? "") + HttpContext.Current.Request.QueryString["UID"];


                if (!string.IsNullOrWhiteSpace(resume) && HttpContext.Current.Session["sessionresume" + ResumeFull] != null)
                {
                    var builder = new UriBuilder(HttpContext.Current.Request.Url);
                    var currentParams = HttpUtility.ParseQueryString(HttpContext.Current.Request.QueryString.ToString());
                    currentParams.Remove("sessionresume");
                    currentParams.Remove("UID");
                    builder.Query = currentParams.ToString();

                    HttpContext.Current.Session.Remove("sessionresume" + ResumeFull);

                    Response.Redirect(builder.ToString());
                }
            }
            catch (ThreadAbortException)
            {
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, "Page_Load", "Error redirecting to remove sessionresume", ex.Message);
            }

            try
            {
                WebApp.ProcessPage(this, status);
            }
            catch (Exception ex)
            {
                KxLogger.LogActivity(this, ToString(), ex.Message, ex.StackTrace);
            }
        }

        protected override void Render(HtmlTextWriter writer)
        {
            StringBuilder sb = new StringBuilder();
            StringWriter sw = new StringWriter(sb);

            HtmlTextWriter hWriter = new HtmlTextWriter(sw);

            base.Render(hWriter);

            try
            {
                ((KxWebApplicationStudent) Session["WebMgr"]).ProcessRendering(this, sb.ToString());
            }
            catch
            {
            }

            writer.Write(sb.ToString());
        }

        protected void Page_PreInit(object sender, EventArgs e)
        {
            string mp = null;
            if (!string.IsNullOrWhiteSpace(Request["mp"]))
            {
                mp = string.Format("~/Templates/{0}.master", Request["mp"]);
            }
            else if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["MasterPage"]))
            {
                mp = ConfigurationManager.AppSettings["MasterPage"];
            }
            mp = (mp ?? "").Replace("-lang-", Request["lang"] ?? ConfigurationManager.AppSettings["DefaultLanguage"]);
            if (!string.IsNullOrWhiteSpace(mp))
            {
                this.MasterPageFile = mp;
            }
        }

        #endregion
    }
}