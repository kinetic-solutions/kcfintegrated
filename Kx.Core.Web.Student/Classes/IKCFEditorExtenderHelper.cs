﻿#region copyright

// =======================================================
// Product:   KCF.Student
// Project:   Kx.Core.Web.Student
// File:      IKCFEditorExtenderHelper.cs
// Created:   21/11/2016
// By:        Steve Ashley
// Modified:  21/11/2016
// 
// Copyright © Kinetic Solutions Ltd 2016
// All rights reserved.
// 
// No portion of this file may be re-created, copied or otherwise
// used without the express permission of Kinetic Solutions Ltd.
// =======================================================

#endregion

#region usings

using System.Collections.Generic;

#endregion

namespace Kx.Core.Web.Student.Classes
{
    public interface IKCFEditorExtenderHelper
    {
        #region Public Methods

        string PageControls();
        IEnumerable<string> PageResources();
        IEnumerable<string> EmailTemplates();

        string PageResource(string page);
        string EmailTemplate(string templateName);

        string DescriptiveHelp();

        #endregion
    }
}