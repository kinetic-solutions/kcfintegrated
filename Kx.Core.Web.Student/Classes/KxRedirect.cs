﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Web;
using Kx.Core.Configuration;
using Kx.Core.Utility;

namespace Kx.Core.Web.Student.Classes
{
    public class KxRedirect
    {
        private const String queryString = "?wf=";

        public bool Redirect(string url)
        {
            bool result = false;

            if ((HttpContext.Current != null) && (HttpContext.Current.Response != null))
            {
                // pha
                if (System.Configuration.ConfigurationManager.AppSettings["DebugRedirect"] == "True") 
                    new Data.KxData().LogDebug("Redirect", url); // pha
                HttpContext.Current.Response.Redirect(url);
                result = true;
            }

            return result;
        }


        /// <summary>
        /// Changes the executing workflow from the current definition to another
        /// </summary>
        /// <param name="newWorkflowName">The new workflow to start</param>
        /// <returns>True when successful</returns>
        public bool ChangeWorkflow(string newWorkflowName)
        {
            Kx.Core.Web.Providers.AuditProvider.ConfiguredAddToAudit("KxWorkflowExtender", "ChangeWorkflow", "Workflow redirect, changed to: " + newWorkflowName);

            String homeUrl = KxDbConfiguration.GetConfiguration("HomePageUrl");

            var queryString = string.Empty;
            if (HttpContext.Current.Request.RawUrl.Contains("?"))
            {
                queryString = string.Join(string.Empty,
                    HttpContext.Current.Request.RawUrl.Substring(HttpContext.Current.Request.RawUrl.IndexOf('?'))
                        .Split('?'));
            }

            var currentParams = HttpUtility.ParseQueryString(queryString);

            currentParams["wf"] = newWorkflowName;
            currentParams.Remove("start");

            var builder = new UriBuilder(homeUrl);
            builder.Query = currentParams.ToString();

            Kx.Core.Web.Providers.AuditProvider.ConfiguredAddToAudit("KxWorkflowExtender", "ChangeWorkflow", string.Format("New params: {0}", currentParams.ToString()));
            Kx.Core.Web.Providers.AuditProvider.ConfiguredAddToAudit("KxWorkflowExtender", "ChangeWorkflow", string.Format("Workflow redirected to: {0}", builder.ToString()));


            return Redirect(builder.ToString());
        }

        /// <summary>
        /// Changes the executing workflow from the current definition to another
        /// </summary>
        /// <param name="newWorkflowName">The new workflow to start</param>
        /// <returns>True when successful</returns>
        public bool ChangeWorkflowDeletingParams(string newWorkflowName)
        {
            Kx.Core.Web.Providers.AuditProvider.ConfiguredAddToAudit("KxWorkflowExtender", "ChangeWorkflow", "Workflow redirect, changed to: " + newWorkflowName);

            String homeUrl = KxDbConfiguration.GetConfiguration("HomePageUrl");

            var queryString = string.Empty;

            var currentParams = HttpUtility.ParseQueryString(queryString);

            currentParams["wf"] = newWorkflowName;
            currentParams.Remove("start");

            var builder = new UriBuilder(homeUrl);
            builder.Query = currentParams.ToString();

            Kx.Core.Web.Providers.AuditProvider.ConfiguredAddToAudit("KxWorkflowExtender", "ChangeWorkflow", string.Format("New params: {0}", currentParams.ToString()));
            Kx.Core.Web.Providers.AuditProvider.ConfiguredAddToAudit("KxWorkflowExtender", "ChangeWorkflow", string.Format("Workflow redirected to: {0}", builder.ToString()));

            return Redirect(builder.ToString());
        }

        /// <summary>
        /// Changes the executing workflow from the current definition to another
        /// </summary>
        /// <param name="newWorkflowName">The new workflow to start</param>
        /// <returns>True when successful</returns>
        public bool ChangeWorkflowWithStartStep(string newWorkflowName, string startStep)
        {
            Kx.Core.Web.Providers.AuditProvider.ConfiguredAddToAudit("KxWorkflowExtender", "ChangeWorkflow", "Workflow redirect, changed to: " + newWorkflowName);

            String homeUrl = KxDbConfiguration.GetConfiguration("HomePageUrl");

            var queryString = string.Empty;
            if (HttpContext.Current.Request.RawUrl.Contains("?"))
            {
                queryString = string.Join(string.Empty,
                    HttpContext.Current.Request.RawUrl.Substring(HttpContext.Current.Request.RawUrl.IndexOf('?'))
                        .Split('?'));
            }

            var currentParams = HttpUtility.ParseQueryString(queryString);

            currentParams["wf"] = newWorkflowName;
            currentParams["start"] = startStep;

            var builder = new UriBuilder(homeUrl);
            builder.Query = currentParams.ToString();

            Kx.Core.Web.Providers.AuditProvider.ConfiguredAddToAudit("KxWorkflowExtender", "ChangeWorkflow", string.Format("New params: {0}", currentParams.ToString()));
            Kx.Core.Web.Providers.AuditProvider.ConfiguredAddToAudit("KxWorkflowExtender", "ChangeWorkflow", string.Format("Workflow redirected to: {0}", builder.ToString()));

            return Redirect(builder.ToString());
        }

        /// <summary>
        /// Changes the executing workflow from the current definition to another
        /// </summary>
        /// <param name="newWorkflowName">The new workflow to start</param>
        /// <returns>True when successful</returns>
        public bool ChangeWorkflowWithStartStepDeletingParams(string newWorkflowName, string startStep)
        {
            Kx.Core.Web.Providers.AuditProvider.ConfiguredAddToAudit("KxWorkflowExtender", "ChangeWorkflow", "Workflow redirect, changed to: " + newWorkflowName);

            String homeUrl = KxDbConfiguration.GetConfiguration("HomePageUrl");

            var queryString = string.Empty;

            var currentParams = HttpUtility.ParseQueryString(queryString);

            currentParams["wf"] = newWorkflowName;
            currentParams["start"] = startStep;

            var builder = new UriBuilder(homeUrl);
            builder.Query = currentParams.ToString();

            Kx.Core.Web.Providers.AuditProvider.ConfiguredAddToAudit("KxWorkflowExtender", "ChangeWorkflow", string.Format("New params: {0}", currentParams.ToString()));
            Kx.Core.Web.Providers.AuditProvider.ConfiguredAddToAudit("KxWorkflowExtender", "ChangeWorkflow", string.Format("Workflow redirected to: {0}", builder.ToString()));

            return Redirect(builder.ToString());
        }
    }
}
