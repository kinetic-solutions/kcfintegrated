﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kx.Core.Web.Student.Classes
{
    public class DefaultExtenderValueAttribute : Attribute
    {
        public string ValueString { get; set; }

        public DefaultExtenderValueAttribute()
        {
            
        }

        public DefaultExtenderValueAttribute(string valueString)
        {
            ValueString = valueString;
        }
    }
}
