﻿#region copyright

// =======================================================
// Product:   KCF.Student
// Project:   Kx.Core.Web.Student
// File:      WorkflowMasterPage.cs
// Created:   23/11/2016
// By:        Steve Ashley
// Modified:  23/11/2016
// 
// Copyright © Kinetic Solutions Ltd 2016
// All rights reserved.
// 
// No portion of this file may be re-created, copied or otherwise
// used without the express permission of Kinetic Solutions Ltd.
// =======================================================

#endregion

#region usings

using System;

#endregion

namespace Kx.Core.Web.Student.Classes
{
    public class WorkflowMasterPage : System.Web.UI.MasterPage
    {
        #region Fields and Properties

        public string UIPageName
        {
            get { return System.IO.Path.GetFileName(Request.CurrentExecutionFilePath).Replace(".aspx", ""); }
        }

        #endregion

        #region Public Methods

        public string ResolveFullURL(string relativeUrl)
        {
            return new Uri(Page.Request.Url, ResolveClientUrl(relativeUrl)).AbsoluteUri;
        }

        #endregion

        #region Protected Methods

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        #endregion
    }
}